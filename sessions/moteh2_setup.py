
from bliss.setup_globals import *

load_script("moteh2.py")

print("")
print("Welcome to your new 'moteh2' BLISS session !! ")
print("")
print("You can now customize your 'moteh2' session by changing files:")
print("   * /moteh2_setup.py ")
print("   * /moteh2.yml ")
print("   * /scripts/moteh2.py ")
print("")