print("... beamline_configuration/sessions/eh2_setup.py")

from bliss.setup_globals import *
try:
    from bliss import setup_globals as SG
except:
    print ("WARNING SG not imported")
    SG = None
import math
import numpy as np
import json
import os
from os import path

from bliss.common import logtools as lotoo
from bliss import current_session
import time
from pprint import *
from datetime import datetime

from bliss.scanning.chain import ChainPreset
load_script("preset_uopiom2.py")
uopiom2_preset = Preset_uopiom2()
DEFAULT_CHAIN.add_preset(uopiom2_preset)

log_stdout(fdir='/users/opid13/logs',fname='blisslog_eh2_' + datetime.today().strftime('%Y%m%d') + '.log')

#load_script("preset_eiger_hardware_saving")
#eiger_hws = EigerHardwareSavingPreset()
#DEFAULT_CHAIN.add_preset(eiger_hws)

# eiger hardwre saving

def eigerhws(state):
    if state:
        eiger.saving._managed_mode = "HARDWARE"
        eiger.saving.file_format = "HDF5"
        SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}"
    else:
        eiger.saving._managed_mode = "SOFTWARE"
        eiger.saving.file_format = "HDF5BS"
        SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"

def set_eiger_beam_trimming():
    mgeig()
    eigerhws(False)
    eiger.roi_counters.set('trim', (1161, 1387, 180, 180))
    MG_EH2.enable('eiger:roi_counters:trim_avg')

def set_eiger_beam_hws():
    mgeig()
    eiger.roi_counters.clear()
    eigerhws(True)




# ============= KMAP

#print("+++ ID01.GIT: Loading kmap (id01.git/id01/scripts/kmap.py) ...")
#from id01.scripts import kmap

# Now using local id13 version of kmaps
print("+++ ID13.GIT: Loading kmap (id13.git/id13/scripts/kmap.py) ...")
from id13.scripts import kmap
print("+++ Config kmap")
kmap.setup(kmap_config_eh2)



# The SENTINEL thingy is necessry in order to use "newmicropx" and have it gathering the
# SCAN_SAVING information
#
#

load_script("loadscript_sentinel.py")
try:
    THE_SENTINEL = SENTINEL
except Exception:
    print("load_script does not work - skipping entire eh2_setup!")
    THE_SENTINEL = 0

THE_SENTINEL = 1

if THE_SENTINEL:
    print ("SENTINEL IS TRUE")



    #
    # This is the regular setup ...
    #
    
    print("... beamline_configuration/sessions/scripts/eh2.py")
    load_script("eh2.py")
    
    from id13.scripts.user_script import *
    #print("... local/id13.git/id13/scripts/kmap.py")
    #from id13.scripts import kmap
    
    print("")
    print("Welcome to your new 'eh2' BLISS session !! ")
    print("")
    print("You can now customize your 'eh2' session by changing files:")
    print("   * /eh2_setup.py ")
    print("   * /eh2.yml ")
    print("   * /scripts/eh2.py ")
    print("")
    
    #print("... doing sync()")
    #sync()
    #wa()
    
    	
    #ufsh.set_external_control(fseh2_set_open, fseh2_set_close, fseh2_is_opened)
    #ufsh.init()
    
    #ufsh.mode = ufsh.EXTERNAL
    #ufsh.mode
  
    # try:
    #     eiger.proxy.saving_index_format="%06d"
    #     eiger.proxy.saving_format="HDF5BS"
    # except:
    #     print ("cannot access eiger ... sorry giving up!")
    #     sleep(2)

    # try:
    #     print ("setting saving index format for mpxeh2he ...")
    #     mpxeh2he.proxy.saving_index_format="%06d"
    #     mpxeh2he.proxy.saving_format="HDF5BS"
    # except:
    #     print ("cannot access mpxeh2he ... sorry giving up!")
    #     sleep(2)

    # try:
    #     print ("setting saving index format for mpxeh2cdte ...")
    #     mpxeh2cdte.proxy.saving_index_format="%06d"
    #     mpxeh2cdte.proxy.saving_format="HDF5BS"
    # except:
    #     print ("cannot access mpxeh2cdte ... sorry giving up!")
    #     sleep(2)

    # try:
    #     if True:
    #         print ("setting saving index format for uvlm2 ...")
    #         uvlm2.proxy.saving_index_format="%06d"
    #         uvlm2.proxy.saving_format="HDF5BS"
    #     else:
    #         print ("uvlm2 ... not to be used;)")
    # except:
    #     print ("cannot access uvlm2 ... sorry giving up!")
    #     sleep(2)


    # Set default chain to p201 trigger / eiger
    default_chain_eh2 = config.get("default_chain_eh2")
    DEFAULT_CHAIN.set_settings(default_chain_eh2["chain_config"])
    print(DEFAULT_CHAIN.get({"count_time":1}, [])._tree)

    #====== metadata
    SCAN_SAVING.scan_number_format = '%05d'
    #SCAN_SAVING.images_prefix = '{img_acq_device}_{scan_number}_'
    #SCAN_SAVING.images_prefix = '{img_acq_device}/{sample}_{dataset}_{scan_number}_data_'
    SCAN_SAVING.images_prefix = '{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_'
    print(f"SCAN_SAVING.images_prefix = {SCAN_SAVING.images_prefix}")
    SCAN_SAVING.date_format = '%Y-%m-%d'
    #SCAN_SAVING.technique = 'KMAP'
    
    #============================= (metadata)
    # it is possible to switch to ESRF data policy, even in the
    # test session, using current_session.enable_esrf_data_policy()
    # and to come back to the default (no policy) with
    # current_session.disable_esrf_data_policy()
    #
    # disable -> can't work with metadata
    # enable -> metadata ON
    
    current_session.enable_esrf_data_policy()
    #current_session.disable_esrf_data_policy()
    
    #_mgr = current_session.scan_saving.metadata_manager
    #_exp = current_session.scan_saving.metadata_experiment
    
    
    
    # ### mcb generic ...
    #

    # limits
    
    def set_lm(mot, lolim=None, hilim=None):
        try:
            mot.position
        except AttributeError:
            printr("error: arg1 to set_lm probably not a valid motor/stage instance!")
            print("nothing done ...")
            return
        l,h = mot.limits
        _l = min(l, h)
        _h = max(l, h)
        l = _l
        h = _h
        if None is lolim:
            sl = l
        else:
            sl = float(lolim)
        if None is hilim:
            sh = h
        else:
            sh = float(hilim)
        fsl = min(sl, sh)
        fsh = max(sl, sh)
        mot.limits = (fsl, fsh)
        mot.settings_to_config(velocity=False, acceleration=False, limits=True)
        lm(mot)
        return

    def shcag(mot):
        pos = mot.position
        l,h = mot.limits
        print("cage: %8s" % (mot.name,),'    lo range:', ("%10.5f" % (l - pos,)), '    hi range:', ("%10.5f" % (h - pos,)))

    def shsampcag():
        print("=== ustr... cage:")
        shcag(ustrx)
        shcag(ustry)
        shcag(ustrz)


    def lcag(mot, locag):
        cage(mot, locag=locag, hicag=None)
    def hcag(mot, hicag):
        cage(mot, locag=None, hicag=hicag)

    def xcag(locag, hicag):
        cage(ustrx, locag=locag, hicag=hicag)
    def lxcag(locag):
        cage(ustrx, locag=locag, hicag=None)
    def hxcag(hicag):
        cage(ustrx, locag=None, hicag=hicag)

    def ycag(locag, hicag):
        cage(ustry, locag=locag, hicag=hicag)
    def lycag(locag):
        cage(ustry, locag=locag, hicag=None)
    def hycag(hicag):
        cage(ustry, locag=None, hicag=hicag)

    def zcag(locag, hicag):
        cage(ustrz, locag=locag, hicag=hicag)
    def lzcag(locag):
        cage(ustrz, locag=locag, hicag=None)
    def hzcag(hicag):
        cage(ustrz, locag=None, hicag=hicag)

    def cage(mot, locag=None, hicag=None):
        pos = mot.position
        l,h = mot.limits
        lolim = hilim = None
        if not None is locag:
            sl = float(locag)
            lolim = pos + sl
        if not None is hicag:
            sh = float(hicag)
            hilim = pos + sh
        set_lm(mot, lolim=lolim, hilim=hilim)

    def sycag(mot, cag):
        cage(mot, -cag, cag)

    def mvrcage(mot, locag=None, hicag=None):
        pos = mot.position
        l,h = mot.limits
        if None is locag:
            sl = l
        else:
            sl = l + float(locag)
        if None is hicag:
            sh = h
        else:
            sh = h + float(hicag)
        lolim = sl
        hilim = sh
        set_lm(mot, lolim=lolim, hilim=hilim)

    def symvrcag(mot, cag):
        mvrcage(mot, -cag, cag)

    # misc
    
    def yesno(quetxt, genindent=""):
        ans = input("%s%s (y/n)? " % (genindent,quetxt))
        if "y" == ans:
            return True
        else:
            return False
    
    def qgoto_cen(ctr, mot):
        global SCANS
        s = SCANS[-1]
        s.goto_cen(ctr, axis=mot)
    
    def qgoto_peak(ctr, mot):
        global SCANS
        s = SCANS[-1]
        s.goto_peak(ctr, axis=mot)
    
    
    def qgoto_pt(idx):
        global SCANS
    
        s = SCANS[-1]
        d = s.get_data()
        ax_keys = [k for k in d.keys() if k.startswith("axis")]
    
    
        todo = []
    
        print ("\npreview:")
        for k in ax_keys:
            a,nam = k.split(":")
            mot = getattr(SG, nam)
            pos = d[k][idx]
            todo.append((mot, pos))
            print ("    %s.move(%f)" % (mot.name, pos))
    
        print ("\nnow we will be moving things ...")
        for (m,p) in todo:
            print ("\n    %s.move(%f)" % (m.name, p))
            ans = yesno("move there", genindent="    ")
            if ans:
                m.move(p)
            else:
                print("    okay - nothing done ...")
    
    def get_glob_scans():
        return SCANS
    
    
    # ### more specific

    p201 = p201_eh2_0
    ct22 = p201_eh2_0.counters.ct22
    ct24 = p201_eh2_0.counters.ct24
    ct25 = p201_eh2_0.counters.ct25

    
    DIODE_CTR = ct22
    ION_CTR = ct24
    
    SH = sh2
    
    def sh_is_closed(sh):
        return 'CLOSED' in str(sh.state)
    
    def sh_is_open(sh):
        return 'OPEN' in str(sh.state)
    
    def so():
        if sh_is_open(SH):
            print ("shutter", SH, "probably already open.")
            return True
        while True:
            try:
                print("trying to open", SH, "...")
                SH.open()
                if sh_is_open(SH):
                    print ("shutter", SH, "probably open.")
                    return True
                else:
                    print ("shutter state is", SH.state)
                    time.sleep(1.0)
            except:
                if sh_is_open(SH):
                    print ("shutter", SH, "probably open.")
                    return True
                time.sleep(1.0)
    
    def sc():
        if sh_is_closed(SH):
            print ("shutter", SH, "probably already closed.")
            return True
        while True:
            try:
                SH.close()
                if sh_is_closed(SH):
                    print ("shutter", SH, "probably closed.")
                    return True
                elif sh_is_open(SH):
                    print ("shutter", SH, "probably open.")
                else:
                    print ("shutter", SH, "in fuzzy state - giving up:")
                    print ("state was:",  SH.state)
                    return
    
            except:
                try:
                    if sh_is_closed(SH):
                        print ("shutter", SH, "probably closed.")
                        return True
                    elif sh_is_open(SH):
                        print ("shutter", SH, "probably open.")
                    else:
                        print ("shutter", SH, "in fuzzy state - giving up.")
                        print ("state was:",  SH.state)
                        return
                except:
                    print ("shutter", SH, "error getting state - giving up.")
                    return
    
            print("sleeping - you can type  to interrupt ...")
            time.sleep(1.0)
    
    
    
    
    def goc(mot):
        qgoto_cen(DIODE_CTR, mot)
    
    def gop(mot):
        qgoto_peak(DIODE_CTR, mot)
    
    def gopt(idx):
        qgoto_pt(idx)

    # MG and CHAIN management
    #
    #
    
    ## # measurement groups and chains
    ## #
    
    def mgd():
    ##     # diode (p201) only
    ##     #DEFAULT_CHAIN.set_settings(default_chain_eh3a['chain_config'])
        MG_P201.set_active()
    
    ## def mgpco():
    ##     # pco camera
    ##     DEFAULT_CHAIN.set_settings(chain_pco['chain_config'])
    ##     MG_PCO.set_active()
    
    def rmgeig():
        mgeig()
        try:
            MG_EH2.enable('*r:r*')
        except:
            import traceback
            print(traceback.format_exc())

    def rmgeig_x():
        rmgeig()
        MG_EH2.enable('*xmap2*')

    def mgeig():
        #photon_energy = 12920
        #photon_energy = 13000
        ###photon_energy = 13036
        photon_energy = int(ccmo.get_energy()*1000)
        #photon_energy = 17000
        # eiger detector and diode
        # DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])
        # for Pb (Marine)
        #print ("trying eiger proxy ...")
        eiger.proxy.saving_index_format="%06d"
        eiger.saving.file_format="HDF5BS"
        #eiger.saving.file_format="HDF5"

        auto_summation = True
        print(f"Eiger optimized software-managed configuration (auto_summation={auto_summation}) ...")

        #eiger.camera.photon_energy=17000
        # photon_energy = 13200
        eiger.camera.photon_energy=photon_energy

        print(f'setting: eiger.camera.photon_energy={photon_energy}')
        eiger.saving._managed_mode = "SOFTWARE"
        eiger.camera.auto_summation = "ON" if auto_summation else "OFF"
        eiger.camera.compression_type = "BSLZ4"
        #eiger.camera.dynamic_pixel_depth = True
        ##eiger.proxy.saving_use_hw_comp = True

        # print('setting: eiger.camera.photon_energy=17000')
        # eiger.camera.threshold_energy=12000
        # print('setting: eiger.threshold_energy=12000')
        fshtrigger()
        MG_EH2.set_active()
    
    def mgmpxcdte():
        fshtrigger()
        MG_EH2.disable("*eiger*")
        #MG_EH2.disable("*fre*")
        #MG_EH2.disable("*xmap*")
        #MG_EH2.disable("*pco*")
        #MG_EH2.disable("*mpxeh2*")
        #MG_EH2.disable("*mpxeh2he*")
        #MG_EH2.disable("*uvlm2*")
        MG_EH2.enable("*mpxeh2cdte*")
        MG_EH2.set_active()
    
    def mgmpxcdte_x():
        fshtrigger()
        MG_EH2.disable("*eiger*")
        MG_EH2.disable("*fre*")
        #MG_EH2.enable("*xmap2*")
        MG_EH2.disable("*pco*")
        MG_EH2.disable("*mpxeh2*")
        MG_EH2.disable("*mpxeh2he*")
        MG_EH2.disable("*uvlm2*")
        MG_EH2.enable("*mpxeh2cdte*")
        MG_EH2.set_active()
    
    def mgmpxhe():
        fshtrigger()
        MG_EH2.disable("*eiger*")
        MG_EH2.disable("*fre*")
        MG_EH2.disable("*xmap*")
        MG_EH2.enable("*mpxeh2he*")
        MG_EH2.set_active()
    
    def mgmpxhe_x():
        fshtrigger()
        MG_EH2.disable("*eiger*")
        MG_EH2.enable("*mpxeh2he*")
        MG_EH2.enable("*xmap2*")
        MG_EH2.set_active()
        
    def mgfluox():
        fshtrigger()
        MG_EH2.disable("*eiger*")        
        MG_EH2.disable("*mpxeh2cdte*")
        MG_EH2.enable("*xmap2*")
        MG_EH2.set_active()     
    
    def mgfre():
        # frelon detector and diode
        fshtrigger()
        default_chain_eh2f=config.get('default_chain_eh2f')
        DEFAULT_CHAIN.set_settings(default_chain_eh2f["chain_config"])
        MG_EH2.set_active()
    
    ## def mgvlm():
    ##     # vlm2 microscope camera
    ##     DEFAULT_CHAIN.set_settings(default_chain_eh3vlm2['chain_config'])
    ##     MG_VLM.set_active()
    
    #
    #
    
    def wps():
        wm(phg, pho, pvg, pvo)
    
    def wbet():
        wm(wbety, wbetz)
    
    def wnnp():
        wm(nnp1,nnp2, nnp3)
    
    def wdet():
        wm(udetx, udety, udetz)
    
    def set_outgain(g):
        print(umoco.comm_ack("outbeam %g" % g))
        print(umoco.info())
    
    def set_ingain(g):
        print(umoco.comm_ack("inbeam %g" % g))
        print(umoco.info())
    
    # shutter
    # fsh
    
    SHUTTER_STATE = {
        True  : 'OPEN',
        False : 'CLOSED'
    }
    
    def fshtrigger():
        ufsh.mode=ufsh.EXTERNAL
        umux.switch("FSH_MODE", "P201")
        umux.switch("FS_ENABLE", "ENABLE")
    
    def fshclose():
        ufsh.mode=ufsh.MANUAL
        ufsh.close()
    
    def fshopen():
        ufsh.mode=ufsh.MANUAL
        ufsh.open()
    
    def fshstate():
        print("shutter info:")
        print("    shutter state (ufsh):      %s" % SHUTTER_STATE[ufsh.is_open])
        print("    shutter mode (ufsh):       %s" % ufsh.mode)
        print("    umux FSH_MODE:             %s" % umux.getOutputStat('FSH_MODE'))
        print("    umux TRIG_MODE (detector): %s" % umux.getOutputStat('TRIG_MODE'))
        print("    umux FS_ENABLE:            %s\n" % umux.getOutputStat('FS_ENABLE'))
    
    #
    # mono managment
    #
    load_script("ccmono.py")
    try:
        ccmo = CCMono()
    except:
        pass
    
    # apertures
    def sapalign():
        dscan(usapy,-0.05,0.05,50,0.1)
        goc(usapy)
        sleep(1)
        where()
        sleep(1)
        dscan(usapz,-0.05,0.05,50,0.1)
        goc(usapz)
        where()

    def bapalign():
        dscan(ufreyab,-0.05,0.05,50,0.1)
        sleep(1)
        goc(ufreyab)
        where()
        sleep(1)
        dscan(ufrebz,-0.05,0.05,50,0.1)
        goc(ufrebz)
        where()

    def bapalign2():
        dscan(ufreay,-0.3,0.3,60,0.1)
        sleep(1)
        goc(ufreay)
        where()
        sleep(1)
        dscan(ufreaz,-0.3,0.3,60,0.1)
        goc(ufreaz)
        where()

    def strange_bsalign():
        ypos = ubsy.position
        dscan(ubsy,-0.7,0.7,70,0.05)
        goc(ubsy)
        where()
        sleep(1)
        new_ypos = ubsy.position

        if math.fabs(new_ypos-ypos) < 0.1:
             ubsy.position = 0
        else:
            print("warning: decrepancy %g in ubsy > 0.1" % new_ypos) 

    def std_bsalign():
        ypos = ubsy.position
        zpos = ubsz.position
        dscan(ubsy,-0.4,0.4,80,0.1)
        goc(ubsy)
        where()
        sleep(1)
        dscan(ubsz,-0.4,0.4,80,0.1)
        goc(ubsz)
        where()
        sleep(1)
        new_ypos = ubsy.position
        new_zpos = ubsz.position

        if math.fabs(new_ypos-ypos) < 0.02:
             ubsy.position = 0
        else:
            print("warning: decrepancy %g in ubsy > 0.02" % new_ypos) 

        if math.fabs(new_zpos-zpos) < 0.02:
             ubsz.position = 0
        else:
            print("warning: decrepancy %g in ubsz > 0.02" % new_zpos) 

    def large_bsalign():
        ypos = ubsy.position
        zpos = ubsz.position
        dscan(ubsy,-0.8,0.8,40,0.05)
        goc(ubsy)
        where()
        sleep(1)
        dscan(ubsz,-0.8,0.8,40,0.05)
        goc(ubsz)
        where()
        sleep(1)
        new_ypos = ubsy.position
        new_zpos = ubsz.position

        if math.fabs(new_ypos-ypos) < 0.1:
             ubsy.position = 0
        else:
            print("warning: decrepancy %g in ubsy > 0.1" % new_ypos) 

        if math.fabs(new_zpos-zpos) < 0.1:
             ubsz.position = 0
        else:
            print("warning: decrepancy %g in ubsz > 0.1" % new_zpos) 

    bsalign = std_bsalign

    def alignmuch_old():
        plotinit(ct22)
        mgd()
        fshtrigger()
        so()
        sleep(2)
        sapalign()
        bapalign()
        mv(ubsy,0)
        large_bsalign()
        sct(1)

    def alignmuch():
        plotinit(ct22)
        dqmgeig()
        mgd()
        fshtrigger()
        so()
        sleep(2)
        #sapalign()
        mv(ubsy,0)
        bsalign()
        sct(1)

    def dqmgeigx():
        mgeig()
        MG_EH2.disable('*')
        MG_EH2.enable('*acq_time_2*')
        MG_EH2.enable('*ct22*')
        MG_EH2.enable('*ct24*')
        MG_EH2.enable('*ger:im*')
        MG_EH2.enable("*xmap2*")
        MG_EH2.enable("*ger:roi*")

    def old_alignmuch():
        plotinit(ct22)
        mgd()
        fshtrigger()
        so()
        sleep(2)
        sapalign()
        bapalign()
        #bapalign()
        mvr(ufreyab, -0.005)
        mv(ubsy,0)
        bsalign()
        sct(1)

    # kmap dscan
    #kmap.akmap_lut(ustry,11.95000-0.06,11.95000+0.06, 60,.01,ustrz,np.linspace(-1.737-0.03,-1.737+0.03,20)) 
    def dkmapyz(ll1,ul1,nitv1,ll2,ul2,nitv2,exptime, frames_per_file=2000):
        try:
            ustry.velocity = 0.05
            if exptime < 0.0014:
                raise ValueError("error: exptime %f too low")
            pos1 = ustry.position
            pos2 = ustrz.position
            np1 = nitv1 + 1
            np2 = nitv2 + 1
            slow_lsp = np.linspace(pos2+ll2, pos2+ul2, np2)
            kmap.akmap_lut(ustry, pos1+ll1, pos1+ul1, np1, exptime, ustrz, slow_lsp, frames_per_file=frames_per_file)
        finally:
            ustry.velocity = 5


    def dkmapyz_2(ll1,ul1,nitv1,ll2,ul2,nitv2,exptime, frames_per_file=2000, retveloc=0.25, round=False, debug=False):
        try:
            ustry.velocity = retveloc
            if exptime < 0.0014:
                raise ValueError("error: exptime %f too low")
            pos1 = ustry.position
            pos2 = ustrz.position
            np1 = nitv1 + 1
            np2 = nitv2 + 1
            slow_lsp = np.linspace(pos2+ll2, pos2+ul2, np2)
            if round:
                slow_lsp = np.round(slow_lsp, round)
            if debug:
                print(slow_lsp)
            kmap.akmap_lut(ustry, pos1+ll1, pos1+ul1, np1, exptime, ustrz, slow_lsp, frames_per_file=frames_per_file)
        finally:
            ustry.velocity = 5

    #
    # logfile
    #

    lotoo.logbook_on = False
    def inilog_scanno(cmt="no comment"):
        dmyscan()
        log_scanno()

    def lmarkp(mark, cmt='no comment'):
        lprint('[_%s_]\n%s' % (mark, cmt))

    def lip(cmt='no comment'):
        lmarkp('INTENT', cmt=cmt)

    def lpos(cmt='no comment'):
        lmarkp('POSITIONS', cmt=cmt)
        wa()
        #ladd()

    def lsampo(cmt='no comment'):
        lmarkp('POSITIONS:SAMPLE', cmt=cmt)
        wm(ustrx,ustry,ustrz)
        #ladd()

    def lcrypo(cmt='no comment'):
        lmarkp('POSITIONS:CRYO', cmt=cmt)
        wm(ucryox,ucryoy)
        #ladd()

    def lhexpo(cmt='no comment'):
        lmarkp('POSITIONS:HEX', cmt=cmt)
        wm(uhextx, uhexty, uhextz, uhexrx, uhexry, uhexrz)
        #ladd()

    def lohpo(cmt='no comment'):
        lmarkp('POSITIONS:OH', cmt=cmt)
        wm(pho, phg, pvo, pvg)
        wm(wbetz, wbety)
        #ladd()

    def lflupo(cmt='no comment'):
        lmarkp('POSITIONS:FLUO', cmt=cmt)
        wm(ufluox,ufluoy,ufluoz)
        #ladd()

    def ldetpo(cmt='no comment'):
        lmarkp('POSITIONS:DET', cmt=cmt)
        wm(udetx,udety,udetz)
        #ladd()

    def lbsappo(cmt='no comment'):
        lmarkp('POSITIONS:BSTOP_APERT', cmt=cmt)
        wm(ubsx,ubsy,ubsz)
        wm(usapx,usapy,usapz,ufreyab,ufrebz)
        #ladd()

    def lmocopo(cmt='no comment'):
        lmarkp('POSITIONS:MOCO', cmt=cmt)
        umoco
        #ladd()

    def lenergy(cmt='no comment'):
        lmarkp('POSITIONS:ENERGY', cmt=cmt)
        ccmo.show()
        #ladd()

    def help_log():
        print("logfile tools:\n    log_scanno inilog_scanno lmarkp lit lpos\n    lsampo lcrypo lhexpo lohpo lflupo ldetpo lbsappo lmocopo lmocopo")

    def lm(x):
        wm(x)

    def log_scanno(cmt="no comment", shutterstate='sc', last_scanno=None):
        try:
            last_scan = SCANS[-1]
            last_scanno_str = str(last_scan.scan_number)
        except IndexError:
            lprint ("""TODO
last_scannumber was: %s

comment:
%s

""" % (last_scanno_str, cmt))

    def dkpatchmesh(ptch_step1,nitv1,ptch_step2,nitv2,exptime,nptch1,nptch2, retveloc=0.25):
        pos1 = ustry.position
        pos2 = ustrz.position
        stp1 = ptch_step1/nitv1
        stp2 = ptch_step2/nitv2
        try:
            #origin_arr1 = np.zeros((nptch2, nptch1), dtype=np.float64)
            #origin_arr2 = np.zeros((nptch2, nptch1), dtype=np.float64)
            dc_2d = {}
            pos_list_1d = []
            idx_list_1d = []
            list_1d = []
            for i in range(nptch2):
                z_offset = pos2 + i*(ptch_step2 + stp2)
                for j in range(nptch1):
                    y_offset = pos1 + j*(ptch_step1 + stp1)
                    yz_tup = (y_offset, z_offset)
                    idx_tup = (j,i)
                    dc_2d[idx_tup] = yz_tup
                    pos_list_1d.append(yz_tup)
                    idx_list_1d.append(idx_tup)
            print ('\nscan info: loacl sanno, (yidx, zidx), (y_oripos, z_oripos)')
            print ('==========================================================')
            for (k,x) in enumerate(zip(idx_list_1d, pos_list_1d)):
                (idx,yzt) = x
                print (k, idx, yzt)
    
            for i,(y,z) in enumerate(pos_list_1d):
                print ("measuring patch number:", i)
                mv(ustry, y)
                mv(ustrz, z)
                log_scanno("dkpatchmesh: patch index = %s" % i)
                dkmapyz_2(0, ptch_step1, nitv1, 0, ptch_step2, nitv2, exptime, retveloc=retveloc)
        finally:
                mv(ustry, pos1)
                mv(ustrz, pos2)
            
        return
        #return (dc_2d, idx_list_1d, pos_list_1d)

    # logbook convenience
    def muli():
        lprint("place for inteded manual (multiline) comment")

    #
    # exposure time check
    #
    def expocheck(tstart=10, tdelta=0.01, nexpt=10, npts=10, sleep_time=0.1):
        for n in range(nexpt):
            exptime = tstart + nexpt*tdelta
            print("checkking exptime: %g", exptime)
            loopscan(npts, exptime, sleep_time=sleep_time)

    # mcb reload
    print ("Loading extended mcb infrastructure for eh2")
    load_script("mcb_test_eh2.py")

    #
    # VOLATILE CONFIGURATIONS
    #

    def posfn_to_posname(s):
        if s.endswith('.json'):
            return s
        if s.endswith('._pos'):
            return s[:-5]
        else:
            return s

    def posname_to_posfn(s):
        if s.endswith('.json'):
            return s
        if s.endswith('._pos'):
            return s
        else:
            return "%s._pos"

    def pos_is_sane(s):
        return not pos_is_not_sane(s)

    def pos_is_not_sane(s):
        if s.endswith('_pos'):
            cs = posfn_to_posname(s)
        elif s.endswith('.json'):
            cs = s[:-5]
        else:
            cs = s
        jfn = '%s.json' % cs
        pfn = '%s._pos' % cs
        jflg = path.exists(jfn)
        pflg = path.exists(pfn)
        if (jflg and pflg): return 1
        if (not jflg and not pflg): return -1
        return 0

    def no_such_pos(s):
        return pos_is_not_sane(s) == -1

    def pos_exists(s):
        return not no_such_pos(s)

    def jfn_to_pfn(jfn):
        return ("%s._pos" % jfn[:-5])

    def pfn_to_jfn(pfn):
        return ("%s._pos" % jfn[:-5])

    def get_ptipe(s):
        if s.endswith('.json'):
            return 'j'
        else:
            return 'p'

    def is_of_ptipe(pt, s):
        if not pt in ('j','p'):
            raise ValueError
        return get_ptipe(s) == pt

    def to_other_pfn(s):
        if s.endswith('.json'):
            return s[:-5] + '._pos'
        else: 
            return s[:-5] + '.json'

    def stp(s, force=False):
        if no_such_pos(s):
            pass
        else:
            if pos_exists(s):
                if force and pos_is_sane(s):
                    pass
                else:
                    raise IOError('sorry - cannot overwrite %s' % s)
        
        s = posfn_to_posname(s)
        
        dc = dict(
            x = ustrx.position,
            y = ustry.position,
            z = ustrz.position
        )
        with open("%s._pos" % s, 'w') as f:
            json.dump(dc, f)

    def stonamp(pos, s, force=False):
        if no_such_pos(s):
            pass
        else:
            if pos_exists(s):
                if force and pos_is_sane(s):
                    pass
                else:
                    raise IOError('sorry - cannot overwrite %s' % s)
        
        s = posfn_to_posname(s)
        
        dc = dict(
            x = pos['x'],
            y = pos['y'],
            z = pos['z']
        )
        with open("%s._pos" % s, 'w') as f:
            json.dump(dc, f)

    def readp():
        dc = dict(
            x = ustrx.position,
            y = ustry.position,
            z = ustrz.position
        )
        return dc

    def diffp(t,s=None):
        if not None is s:
            s = posfn_to_posname(s)
        t = posfn_to_posname(t)
        if None is s:
            sp = readp()
        else:
            sp = loadp(s)
        tp = loadp(t)
        diffdc = {}
        print('diff_pos:')
        for k in ('x','y','z'):
            diffdc['d%s' % k] = tp[k] - sp[k]
        for k in ('x','y','z'):
            print('    d%s = %10.5f' % (k, diffdc['d%s' % k]))
        return diffdc

    def interp(t,s=None, fac=0.5, name=None):
        if not None is s:
            s = posfn_to_posname(s)
        t = posfn_to_posname(t)
        if None is s:
            sp = readp()
        else:
            sp = loadp(s)
        tp = loadp(t)
        diffdc = {}
        newpos = {}
        print('diff_pos:')
        for k in ('x','y','z'):
            diffdc['d%s' % k] = tp[k] - sp[k]
        print('difference: source pos - target pos')
        for k in ('x','y','z'):
            print('    d%s = %10.5f' % (k, diffdc['d%s' % k]))
        for k in ('x','y','z'):
            newpos[k] = sp[k] + fac*diffdc['d%s' % k]
        if not None is name:
            stonamp(newpos, name)
        return sp, tp, newpos, diffdc

    def appldiffp(diffdc):
        umvr(ustrx, diffdc['x'])
        umvr(ustry, diffdc['y'])
        umvr(ustrz, diffdc['z'])

    def loadp(s):
        if no_such_pos(s):
            raise IOError('pos %s does not exist' % s)
        if pos_is_not_sane(s):
            raise ValueError('pos %s is not sane' % s)

        if s.endswith('json'):
            with open("%s" % s, 'r') as f:
                jdc = json.load(f)
                dc = {}
                dc['x'] = jdc['motors']['ustrx']
                dc['y'] = jdc['motors']['ustry']
                dc['z'] = jdc['motors']['ustrz']
            return dc
        else:
            s = posfn_to_posname(s)
            with open("%s._pos" % s, 'r') as f:
                dc = json.load(f)
            return dc

    def ls():
        os.system('ls')

    def lspos():
        ll = os.listdir('.')
        ll = sorted(ll)
        print("============== stp:")
        for x in ll:
            if x.endswith('._pos'):
                print(x[:-5])
        print("============== stp:")
        for x in ll:
            if x.endswith('.json'):
                print(x)
        #return ll
        #return ll
        return None

    def gopxy(s, show_only=False):
        s = posfn_to_posname(s)
        dc = loadp(s)
        print("current pos:")
        shp()
        if show_only:
            print('found pos:')
            print(ustry.name, dc['y'])
            print(ustrz.name, dc['z'])
            return
        mv(ustry, float(dc['y']))
        mv(ustrz, float(dc['z']))
        print("new pos:")
        shp()

    def gopos(s, show_only=False):
        s = posfn_to_posname(s)
        print("gopos:s =", s)
        dc = loadp(s)
        print("current pos:")
        shp()
        if show_only:
            print('found pos:')
            print(ustrx.name, dc['x'])
            print(ustry.name, dc['y'])
            print(ustrz.name, dc['z'])
            return
        mv(ustrx, float(dc['x']))
        mv(ustry, float(dc['y']))
        mv(ustrz, float(dc['z']))
        print("new pos:")
        shp()

    rstp = gopos

    def shpos(s):
        s = posfn_to_posname(s)
        gopos(s, show_only=True)


    def ppos(m):
        print (m.name, ":", m.position)

    def shp():
        ppos(ustrx)
        ppos(ustry)
        ppos(ustrz)

    def send_img(name, ext=None):
        if None is ext:
            fname = name
        else:
            fname = "%s.%s" % (name, ext)
        with open(fname, "rb") as f:
            data = f.read()
            data = b"data:image/png;base64," + base64.b64encode(data)
            SCAN_SAVING._icat_proxy.metadata_manager.proxy.uploadBase64(data)
        
    def slil(x):
        #uvolpi.intensity=x
        uvolpi5.intensity=x

    def dmyscan():
        dscan (ustry,-0.012345,0.06789,1,0.042)


#
# intolerant towards tolerance: ...
#

    print("ustrz.tolerance =", ustrz.tolerance)
    if ustrz.tolerance < .001:
        print("WARNING: ustrz tolerance problem and also with the tolerance of ID13 staff tolerating unexplicable changes of the micros yml config file ;\)"*1)
        print('\n\n\n\nplease fix this immediately!!!!!!!!!\n\n\n')
    else:
        print("ustrz.tolerance seems to be okay - you can calm down Manfred!")
        
    print("ustrz.tolerance =", ustrz.tolerance)

    def check_micos():
        if ustrz.tolerance < 0.001:
            print('ustrz is corrupted!')
        else:
            print('ustrz okay')
        if ustrx.tolerance < 0.001:
            print('ustrx is corrupted!')
        else:
            print('ustrx okay')

    def fix_micos():
        ustrz.apply_config(reload=True)
        ustrx.apply_config(reload=True)
        check_micos()

    def user_mode():
        set_lm(umicroz,-1,1)

    def beamline_mode():
        set_lm(umicroz,-3,192)


#############################################3
# DO logbook infra structure
load_script('plotexport')

dooc = elog_print
dooa = elog_add

def doops(cmt=None):
    # export current scatterplot to logbook
    elog_plot(scatter=True)

def doopc(cmt=None):
    # export current curve to logbook
    elog_plot(scatter=False)

def doom(*p, cmt=None):
    ll = []
    ll.append(f'==== POSITION:')
    if not None is cmt:
        ll.append(f'position_comment {cmt}')
    for m in p:
        ll.append(f'position_log: {m.name} {m.position}')
    dooc('\n'.join(ll))

def dooi(s):
    # intention
    elog_print(f'==== INTENTION:\n{s}')

# nanocalorimeter - temporary
def nanocal_run(nframes, expt, frames_per_file=100):
    old_fpf = eiger.saving.frames_per_file
    try:
        load_script('preset_eiger_software')
        ueiger = config.get("ueiger")
        ueiger.hws(True)
        eiger.saving.frames_per_file=frames_per_file
        eiger_software_scan(nframes,expt)
    finally:
        eiger.saving.frames_per_file = old_fpf
        ueiger.hws(False)

#
# timeseries versions without hardwareseaving
# 
#
def do_nanocal_run(nframes, expt):
    eiger.roi_counters.clear()
    print("Warning! Eiger rois have been cleared!")
    ueiger = config.get('ueiger')
    eiger.saving.frames_per_file=500
    fshtrigger()
    ueiger.ttl_software_scan_hws(nframes, expt, user_trigger=True)
    fshclose()

def do_timeseries_run(nframes, expt):
    ueiger = config.get('ueiger')
    ueiger.ttl_software_scan(nframes, expt, user_trigger=False)
    
def do_timeseries_run_new(nframes, expt):
    ueiger = config.get('ueiger')
    eiger.saving.frames_per_file=100#500
    fshtrigger()
    ueiger.ttl_software_scan_hws(nframes, expt, user_trigger=False)
    fshtrigger()

def dqmgeig():
    mgeig()
    MG_EH2.disable('*')
    MG_EH2.enable('*acq_time_2*')
    MG_EH2.enable('*ct22*')
    MG_EH2.enable('*ct24*')
    MG_EH2.enable('*ger:im*')

def mgvlm_eh2x():
    # vlm2 microscope camera
    DEFAULT_CHAIN.set_settings(default_chain_eh2vlm1['chain_config'])
    MG_VLM_EH2x.set_active()

def mgvlm_eh2y():
    # vlm2 microscope camera
    DEFAULT_CHAIN.set_settings(default_chain_eh2vlm2['chain_config'])
    MG_VLM_EH2y.set_active()

# aliases
dhamster_2 = dkmapyz_2

# load_script('genstp')
# load_script('adet_roi')
def resume_eiger():
    mgeig()
    print (eiger)


#
# technology under development to do a cold restart of bliss
# including external eiger server restart
#


# The thing is currently inactive
EIGER_RESTART=False
if EIGER_RESTART:
    print ('type  to interrupt restart procedure ...')
    import time
    from os import path
    try:
        while True:
            t = time.time()
            if path.exists('./eiger_restart_start.ev') or\
                path.exists('./eiger_new.ev'):
                try:
                    os.remove('./eiger_restart_start.ev')
                except:
                    print('error: ./eiger_restart_start.ev (resume)')
                try:
                    os.remove('./eiger_new.ev')
                except:
                    print('error: ./eiger_new.ev (resume)')
                try:
                   resume_eiger()
                except:
                    print('error: resume_eiger()')
                    with open('./eiger_restart.asy', 'w') as eigraf:
                        eigraf.write('a')
                    print('waiting 30 sec...')
                    time.sleep(30)
                    break
                while True:
                    try:
                        print('do something with eiger ...')
                        ssss = eiger.acquisition.trigger_mode
                        print(ssss)
                        if 'ERROR' in ssss:
                            raise RuntimeError()
                        time.sleep(1)
                    except:
                        print('some error happened')
                        with open('./eiger_restart.asy', 'w') as eigraf:
                            eigraf.write('a')
                        print('waiting 30 sec...')
                        time.sleep(30)
                        exit()
            else:
                print('no incentives ... continuing ...')
                time.sleep(5)
                continue
    finally:
        try:
            os.remove('./eiger_restart_start.ev')
        except:
            print('error: ./eiger_restart_start.ev')
        try:
            os.remove('./eiger_new.ev')
        except:
            print('error: ./eiger_new.ev')
        exit()


XRF = 'ABC' 

# the "unfortunate ans incomprehensible" rounder problem

# import bliss.shell.standard as blshell
# _x_rounder = blshell.rounder
# def myrounder(a,b):
#     if a > 0.001:
#         a = 0.001
#     return _x_rounder(a,b)
# blshell.rounder = myrounder


FEB24_1 = True
if FEB24_1:
    print("Load mcb function script: feb24_1")
    load_script('feb24_1')

# Workflow triggering (Loic Huder, 28/05/24)    
from blissoda.id13.xrpd_processor import Id13XrpdProcessor as _XrpdProcessor
xrpd_processor = _XrpdProcessor(enable_plotter=False)


CH7040 = True
if CH7040:
    print("Load proposal script: ch7040")
    load_script('ch7040')
