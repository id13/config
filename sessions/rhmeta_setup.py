
from bliss.setup_globals import *
from bliss.setup_globals import *
from bliss import current_session
import time
import pprint

load_script("rhmeta.py")

from id13.scripts.user_script import *

print("")
print("Welcome to your new 'rh' BLISS session !! ")
print("")
print("You can now customize your 'rh' session by changing files:")
print("   * /rh_setup.py ")
print("   * /rh.yml ")
print("   * /scripts/rh.py ")
print("")

from bliss import setup_globals

print("+ set the default chain ...")
#default_chain = setup_globals.default_chain_rh
#DEFAULT_CHAIN.set_settings(chain_rh['chain_config'])
DEFAULT_CHAIN.set_settings(chain_sim_cam1['chain_config'])

#load_script("preset_fsh.py")
#ufsh_preset = Preset_ufsh()
#DEFAULT_CHAIN.add_preset(ufsh_preset)


#====== metadata
SCAN_SAVING.scan_number_format = '%05d'
SCAN_SAVING.images_prefix = '{img_acq_device}_{scan_number}_'
SCAN_SAVING.date_format = '%Y-%m-%d'
#SCAN_SAVING.technique = 'KMAP'


#============================= (metadata)
# it is possible to switch to ESRF data policy, even in the
# test session, using current_session.enable_esrf_data_policy()
# and to come back to the default (no policy) with
# current_session.disable_esrf_data_policy()
#
# disable -> can't work with metadata
# enable -> metadata ON

current_session.enable_esrf_data_policy()
#current_session.disable_esrf_data_policy()

#_mgr = current_session.scan_saving.metadata_manager
#_exp = current_session.scan_saving.metadata_experiment

