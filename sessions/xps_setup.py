
from bliss.setup_globals import *

load_script("xps.py")

print("")
print("Welcome to your new 'xps' BLISS session !! ")
print("")
print("You can now customize your 'xps' session by changing files:")
print("   * /xps_setup.py ")
print("   * /xps.yml ")
print("   * /scripts/xps.py ")
print("")

#ERROR_REPORT.expert_mode=True
