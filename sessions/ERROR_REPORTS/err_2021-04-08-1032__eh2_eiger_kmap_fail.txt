#
# BLISS_CMD = 'dkpatchmesh(0.4,800,0.4,800,0.004,1,1, retveloc=0.1)'
#
# ==========> bliss output
EH2 [1]: dkpatchmesh(0.4,800,0.4,800,0.004,1,1, retveloc=0.1)                                                                        

scan info: loacl sanno, (yidx, zidx), (y_oripos, z_oripos)
==========================================================
0 (0, 0) (-0.2955000000000041, 11.197101)
measuring patch number: 0
WARNING: message is send but use 'elog_print' instead of 'lprint' in the future
started the scan at :
['ustry', 'ustrz']
[-0.2955000000000041, 11.197101]
WARNING 2021-04-08 10:30:47,931 bliss.scripts.kmap: 
... with CAMERA enabled_device_name[eiger] mg_name[MG_EH2]
ERROR: reading lima property saturated_cblevel (Exception: Accumulation threshold plugins not loaded)
WARNING 2021-04-08 10:30:48,104 bliss.scripts.kmap: 
... with CAMERA: LimaAcquisitionMaster()
... device.name[eiger] device.type[E-08-0106] saving_format[HDF5BS] frames_per_file[2000]
... nb_points[641601] cam_expo_time[0.003990000000252621] acq_mode[SINGLE] acq_trigger_mode[EXTERNAL_TRIGGER_MULTI]
... prepare_once[True] start_once[True]
... adding p201 [p201_eh2_0]
... PRINT BUILDER:


-->|__ CT2AcquisitionMaster( p201_eh2_0 ) |
      |
      |__ CT2CounterAcquisitionSlave( ct2_counters_controller ) ( ct22, ct24, acq_time_2 ) |


-->|__ LimaAcquisitionMaster( eiger ) ( image ) |


... detectors [{<bliss.controllers.lima.lima_base.Lima object at 0x7ff250904c50>}]
... mcas [set()]
 !!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
 !!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
ERROR 2021-04-08 10:31:51,035 bliss.scans: Exception caught in eiger.wait_ready (Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state)
!!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
ERROR 2021-04-08 10:31:51,042 bliss.scans: Exception caught in umusst_master.wait_ready (GreenletExit)
!!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
 ERROR 2021-04-08 10:31:51,072 bliss.scans: Exception caught in eiger.wait_reading (Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state)
!!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
!!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
 det[eiger] img[2838/641601]   ustry_position 4768/641601      
Moving ustry from 0.105 to -0.2955
Moving ustrz from 11.2 to 11.197
moved back to:
['ustry', 'ustrz']
[-0.2955000000000041, 11.197101]
!!! === RuntimeError: Device DeviceProxy(id13/limaccds/eiger1) (eiger) is in Fault state === !!! ( for more details type cmd 'last_error' )
EH2 [2]:  

# ==========> eiger limaCCDs output (lbs131, was freshly initialized)
^C(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/07 10:47:07.794017] 7fb28afe7700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/07 11:15:35.803386] 7fb2237f6700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
Traceback (most recent call last):
  File "/users/blissadm/conda/miniconda3/envs/eiger_devel/lib/python3.6/site-packages/Lima/Server/LimaCCDs.py", line 348, in imageStatusChanged
    _acqstate2string(status))
PyTango.DevFailed: DevFailed[
DevError[
    desc = Not able to acquire serialization (dev, class or process) monitor
  origin = TangoMonitor::get_monitor
  reason = API_CommandTimedOut
severity = ERR]
]
Traceback (most recent call last):
  File "/users/blissadm/conda/miniconda3/envs/eiger_devel/lib/python3.6/site-packages/Lima/Server/LimaCCDs.py", line 348, in imageStatusChanged
    _acqstate2string(status))
PyTango.DevFailed: DevFailed[
DevError[
    desc = Not able to acquire serialization (dev, class or process) monitor
  origin = TangoMonitor::get_monitor
  reason = API_CommandTimedOut
severity = ERR]
]
[2021/04/07 17:37:39.279174] 7fb2237f6700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
^CError: destroying non-empty Allocator
(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/07 17:39:50.231986] 7faed74a9700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/07 17:41:36.175491] 7fae6bfff700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
[2021/04/07 17:42:10.672767] 7fae8d9d4700                                         *Control*Control::Control::_checkOverrun (/users/blissadm/Git/Lima/control/src/CtControl.cpp:1306)-Error: m_status=<AcquisitionStatus=AcqRunning, ImageCounters=<LastImageAcquired=2722, LastBaseImageReady=1214, LastImageReady=1214, LastImageSaved=1214, LastCounterReady=-1>, error_code=Soft Processing overrun
^CError: destroying non-empty Allocator
(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/07 17:54:07.810298] 7fd779102700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/07 17:54:30.857591] 7fd710ff9700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
[2021/04/07 18:07:02.859155] 7fd736ffd700                                         *Control*Control::Control::_checkOverrun (/users/blissadm/Git/Lima/control/src/CtControl.cpp:1306)-Error: m_status=<AcquisitionStatus=AcqRunning, ImageCounters=<LastImageAcquired=94216, LastBaseImageReady=92708, LastImageReady=92708, LastImageSaved=92708, LastCounterReady=-1>, error_code=Soft Processing overrun
^CError: destroying non-empty Allocator
(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/07 18:16:22.756137] 7f343f917700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/07 18:16:57.618634] 7f33d3fff700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
^CError: destroying non-empty Allocator
^C(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/08 10:20:48.438428] 7f21347d8700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/08 10:23:36.475843] 7f20c7fff700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
[2021/04/08 10:24:07.141874] 7f20ea7fc700                                         *Control*Control::Control::_checkOverrun (/users/blissadm/Git/Lima/control/src/CtControl.cpp:1306)-Error: m_status=<AcquisitionStatus=AcqRunning, ImageCounters=<LastImageAcquired=1732, LastBaseImageReady=224, LastImageReady=224, LastImageSaved=224, LastCounterReady=-1>, error_code=Soft Processing overrun
^CError: destroying non-empty Allocator
(eiger_devel) lbs131:~ % LimaCCDs eiger1
[2021/04/08 10:30:20.729508] 7f02b08e4700 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid13/lima_eiger1.cfg : default 
[2021/04/08 10:30:48.057289] 7f0243fff700 *Application*Lima.Server.LimaCCDs::LimaCCDs::read_acc_saturated_cblevel-Error: Accumulation threshold plugins not loaded
[2021/04/08 10:31:49.625319] 7f02667fc700                                         *Control*Control::Control::_checkOverrun (/users/blissadm/Git/Lima/control/src/CtControl.cpp:1306)-Error: m_status=<AcquisitionStatus=AcqRunning, ImageCounters=<LastImageAcquired=4345, LastBaseImageReady=2837, LastImageReady=2837, LastImageSaved=2837, LastCounterReady=-1>, error_code=Soft Processing overrun

