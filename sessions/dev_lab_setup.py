
from bliss.setup_globals import *

load_script("dev_lab.py")

print("")
print("Welcome to your new 'dev_lab' BLISS session !! ")
print("")
print("You can now customize your 'dev_lab' session by changing files:")
print("   * /dev_lab_setup.py ")
print("   * /dev_lab.yml ")
print("   * /scripts/dev_lab.py ")
print("")


def dlvlm_hw_trigger():
    DEFAULT_CHAIN.set_settings(chain_dlvlm_hw['chain_config'])