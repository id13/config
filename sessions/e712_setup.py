
from bliss.setup_globals import *

load_script("e712.py")

print("")
print("Welcome to your new 'e712' BLISS session !! ")
print("")
print("You can now customize your 'e712' session by changing files:")
print("   * /e712_setup.py ")
print("   * /e712.yml ")
print("   * /scripts/e712.py ")
print("")