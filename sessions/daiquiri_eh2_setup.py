
from bliss.setup_globals import *

load_script("daiquiri_eh2.py")

print("")
print("Welcome to your new 'daiquiri_eh2' BLISS session !! ")
print("")
print("You can now customize your 'daiquiri_eh2' session by changing files:")
print("   * /daiquiri_eh2_setup.py ")
print("   * /daiquiri_eh2.yml ")
print("   * /scripts/daiquiri_eh2.py ")
print("")

#SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"
default_chain_eh2 = config.get("default_chain_eh2")
DEFAULT_CHAIN.set_settings(default_chain_eh2["chain_config"])

from blissoda.id13.daiquiri_xrpd_processor import DaiquiriXrpdProcessor as _XrpdProcessor
xrpd_processor = _XrpdProcessor(enable_plotter=False)
