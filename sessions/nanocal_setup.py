
from bliss.setup_globals import *

load_script("nanocal.py")

print("")
print("Welcome to your new 'nanocal' BLISS session !! ")
print("")
print("You can now customize your 'nanocal' session by changing files:")
print("   * /nanocal_setup.py ")
print("   * /nanocal.yml ")
print("   * /scripts/nanocal.py ")
print("")

nanocontrol = make_nanocontrol()
print("NanoControl instance class was created...")

scan_saving_path = '/data/id13/inhouse12/alexey/nanocal_bliss_test/'

