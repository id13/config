from bliss.setup_globals import *
from bliss.setup_globals import *
from bliss import current_session
import time
import pprint
import numpy as np

WORKING_EH = "EH3"

load_script("rhkmap.py")

print("")
print("Welcome to your new 'rhkmap' BLISS session !! ")
print("")
print("You can now customize your 'rhkmap' session by changing files:")
print("   * /rhkmap_setup.py ")
print("   * /rhkmap.yml ")
print("   * /scripts/rhkmap.py ")
print("")

print("+++ ID01.GIT: Loading kmap (id01.git/id01/scripts/kmap.py) ...")
from id01.scripts import kmap
from id01.scripts import musst_tools

print(f"+++ Configuring for [{WORKING_EH}]")
if WORKING_EH == "EH2":
  print("+++ Config kmap")
  kmap.setup(kmap_config_eh2)

  print("+++ MG ....")
  MG_KMAP_EH2.set_active() 
  MG_KMAP_EH2.disable_all()                                                                                                  
  MG_KMAP_EH2.enable("p201_eh2_0:ct2_counters_controller:acq_time_2")                                                        
  #MG_KMAP_EH2.enable("sim_cam1:image")                                                                                       
  MG_KMAP_EH2.enable("eiger:image")                                                                                       
  MG_KMAP_EH2.enable("xmap2*")                                                                                       
  MG_KMAP_EH2                                                                                                                

if WORKING_EH == "EH3":
  print("+++ Config kmap")
  kmap.setup(kmap_config_eh3)

  print("+++ MG ....")
  MG_KMAP_EH3.set_active() 
  MG_KMAP_EH3.disable_all()                                                                                                  
  MG_KMAP_EH3.enable("p201_eh3_0:ct2_counters_controller:acq_time_3")                                                        
  #MG_KMAP_EH2.enable("sim_cam1:image")                                                                                       
  #MG_KMAP_EH3.enable("eiger:image")                                                                                       
  MG_KMAP_EH3.enable("xmap3*")  
  MG_KMAP_EH3.enable("mpxeh3*")  
                                                                                       
  MG_KMAP_EH3                                                                                                                


print("+++ SAVING path....")
SCAN_SAVING.base_path = "/data/id13/inhouse12/rh/tmp/"

#current_session.disable_esrf_data_policy()
current_session.enable_esrf_data_policy()

print("+++ END SETUP")
