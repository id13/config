import sys, os
import base64
print("... beamline_configuration/sessions/eh3_setup.py")

from bliss.common.standard import *
from bliss import setup_globals as SG
import numpy as np
import time
import json
from bliss.common import logtools as lotoo
from bliss.shell.cli.repl import BlissRepl
from bliss import current_session

from bliss.setup_globals import *
# print("+++ ID01.GIT: Loading kmap (id01.git/id01/scripts/kmap.py) ...")
# from id01.scripts import kmap
# from id01.scripts import musst_tools

print("+++ ID13.GIT: Loading kmap (id13.git/id01/scripts/kmap.py) ...")
from id13.scripts import kmap
from id13.scripts.MultiplexerScanPreset import MultiplexerScanPreset 
from id13.scripts import musst_tools
from datetime import datetime

log_stdout(fdir='/users/opid13/logs',fname='blisslog_eh3_' + datetime.today().strftime('%Y%m%d') + '.log')


#load_script("eh3.py")
load_script("preset_fsh.py")


from bliss import current_session
#current_session.disable_esrf_data_policy()
#current_session.enable_esrf_data_policy()

print("")
print("Welcome to your new 'eh3' BLISS session !! ")
print("")
print("You can now customize your 'eh3' session by changing files:")
print("   * /eh3_setup.py ")
print("   * /eh3.yml ")
print("   * /scripts/eh3.py ")
print("")
#
#
#
print("run id13 staff definitions:")

load_script("ccmono.py")
try:
    ccmo = CCMono()
except:
    pass
# x-ray energy in keV, to be used in detector configurations etc
#ID13_XRAY_ENERGY = 9.317
ID13_XRAY_ENERGY = ccmo.get_energy()
#DEFAULT_CHAIN.set_settings(default_chain_eh3a['chain_config'])

# intervention: line removed by (mcb) while debugging eiger
#DEFAULT_CHAIN.set_settings(chain_pco['chain_config'])
# end intervention

def pcolive(*p, **kw): 
    print("please use test_pco2k")
    return
    try: 
        fshopen() 
        limatake(*p, **kw) 
        fshclose() 
    finally: 
        fshclose()         

def pcoct(expt):
    # is also in nt2 simple - cleanup needed ...
    try: 
        fshopen() 
        time.sleep(0.05)
        #sct(expt, pco, mpxeh3he)
        sct(expt, pco, eiger)
        fshclose() 
        elog_print(f'pco image: #S {SCANS[-1].scan_number}')
    finally: 
        fshclose()

def pcodscan(*p):
    ll = list(p)
    ll.append(pco)
    #ll.append(mpxeh3he)
    ll.append(eiger)

    try: 
        fshopen() 
        time.sleep(0.05)
        dscan(*ll)
        elog_print(f'pco scan: #S {SCANS[-1].scan_number}')
    finally: 
        fshclose()

#def pcoloopscan(*p): 
#    try: 
#        fshopen() 
#        loopscan(*p) 
#        fshclose() 
#    finally: 
#        fshclose()         

p201 = p201_eh3_0
ct32 = p201_eh3_0.counters.ct32
ct33 = p201_eh3_0.counters.ct33
ct34 = p201_eh3_0.counters.ct34

#====== metadata
SCAN_SAVING.scan_number_format = '%05d'
#SCAN_SAVING.images_prefix = '{img_acq_device}_{scan_number}_'
print ("modifying SCANSAVING ?")
SCAN_SAVING.images_prefix = '{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_'
print (SCAN_SAVING.images_prefix)
SCAN_SAVING.date_format = '%Y-%m-%d'
#SCAN_SAVING.technique = 'KMAP'

#============================= (metadata)
# it is possible to switch to ESRF data policy, even in the
# test session, using current_session.enable_esrf_data_policy()
# and to come back to the default (no policy) with
# current_session.disable_esrf_data_policy()
#
# disable -> can't work with metadata
# enable -> metadata ON

current_session.enable_esrf_data_policy()
#current_session.disable_esrf_data_policy()

#_mgr = current_session.scan_saving.metadata_manager
#_exp = current_session.scan_saving.metadata_experiment

lotoo.logbook_on = True
def la(): elog_add()
def lp(x): elog_print(x)
def log_scanno(cmt="no comment"):
        try:
            last_scan = SCANS[-1]
            last_scanno_str = str(last_scan.scan_number)
        except IndexError:
            last_scanno_str = "sorry - no scan since last bliss start"
        lp ("""
last_scannumber was: %s

comment:
%s

""" % (last_scanno_str, cmt))



## KMAP configuration EH3
print("+++ Config kmap")
kmap.setup(kmap_config_eh3)
print("+++ Add kmap shutter preset")
mpx_preset = MultiplexerScanPreset(nmux, [['SHUTTER', 'OPEN', 'CLOSED']])
kmap.add_scan_preset(mpx_preset)

print("+++ MG ....")
#MG_KMAP_EH3.set_active()
#MG_KMAP_EH3.disable_all()                                                                                                  
#MG_KMAP_EH3.enable("p201_eh3_0:ct2_counters_controller:acq_time_3")                                                        
##MG_KMAP_EH2.enable("sim_cam1:image")                                                                                       
#MG_KMAP_EH3.enable("mgeig:image")                                                                                       
##MG_KMAP_EH3.enable("xmap3*")                                                                                       
#MG_KMAP_EH3                                                                                                                

##print('\n\n\nATTENTION!!!!\neiger and regular maxipix are not setup ...\n\n\n')
# EIGER
try:
    print ("trying eiger proxy ...")
    eiger.proxy.saving_index_format="%06d"
    eiger.saving.file_format="HDF5BS"
    #eiger.saving.file_format="HDF5"

    auto_summation = True
    print(f"Eiger optimized software-managed configuration (auto_summation={auto_summation}) ...")
    eiger.saving._managed_mode = "SOFTWARE"
    #eiger.camera.auto_summation = "ON" if auto_summation else "OFF"
    #eiger.camera.compression_type = "BSLZ4"
    #eiger.camera.dynamic_pixel_depth = True
    #eiger.proxy.saving_use_hw_comp = True
    #We put that here to enable debugging, TG+AH 15/09/23
    eiger.debug.types.on("Fatal", "Error","Warning")
except:
    print ("cannot access eiger ... sorry giving up!")
    #raise
    sleep(10)




# MAXIPIX
#print('maxipix not mounted')
try:
    mpxeh3.proxy.saving_index_format="%06d"
    mpxeh3.proxy.saving_format="HDF5BS"
except:
    print ("cannot access mpxeh3 ... sorry giving up!")
#    sleep(10)

# MAXIPIX GaAs
print('maxipix eh3he  mounted')
try:
    print('trying to configure saving for mpxeh3he ...')
    mpxeh3he.proxy.saving_index_format="%06d"
    mpxeh3he.proxy.saving_format="HDF5BS"
    print('done - configure saving for mpxeh3he.')
except:
    print ("cannot access mpxeh3 ... sorry giving up!")
#    sleep(10)



cam = "mpxeh3"
if hasattr(SG, cam):
  print("... [%s] is USED" % (cam,))
  try:
    mpxeh3
    mpxthleh3 = SoftAxis(
        "mpxthleh3", mpxeh3.camera, position="energy_threshold", move="energy_threshold"
    )
    DEFAULT_CHAIN.set_settings(chain_mpxeh3['chain_config'])
    print("... [%s] DEFAULT CHAIN PASSED" % (cam,))
  except:
    print("... [%s] EXCEPT ERROR" % (cam,))
    pass
else:
  print("... [%s] is NOT USED" % (cam,))

#####################################################
# PSI_EIGER_500K
#print('maxipix not mounted')
try:
    psi_eiger_500k.proxy.saving_index_format="%06d"
    psi_eiger_500k.proxy.saving_format="HDF5BS"
except:
    print ("cannot access psi_eiger_500k ... sorry giving up!")
#    sleep(10)

cam = "psi_eiger_500k"
if hasattr(SG, cam):
  print("... [%s] is USED" % (cam,))
  try:
    psi_eiger_500k
    DEFAULT_CHAIN.set_settings(chain_psi_eiger_500k['chain_config'])
    print("... [%s] DEFAULT CHAIN PASSED" % (cam,))
  except:
    import traceback
    print(traceback.format_exc())
    print("... [%s] EXCEPT ERROR" % (cam,))
    pass
else:
  print("... [%s] is NOT USED" % (cam,))


# xxx delme





# ### generic ...


# limits -------------------------------------------------------------------------
def lm(x):
    wm(x)

def set_lm(mot, lolim=None, hilim=None):
    try:
        mot.position
    except AttributeError:
        printr("error: arg1 to set_lm probably not a valid motor/stage instance!")
        print("nothing done ...")
        return
    l,h = mot.limits
    _l = min(l, h)
    _h = max(l, h)
    l = _l
    h = _h
    if None is lolim:
        sl = l
    else:
        sl = float(lolim)
    if None is hilim:
        sh = h
    else:
        sh = float(hilim)
    fsl = min(sl, sh)
    fsh = max(sl, sh)
    mot.limits = (fsl, fsh)
    mot.settings_to_config(velocity=False, acceleration=False, limits=True)
    lm(mot)
    return

def shcag(mot):
    pos = mot.position
    l,h = mot.limits
    print("cage: %8s" % (mot.name,),'    lo range:', ("%10.5f" % (l - pos,)), '    hi range:', ("%10.5f" % (h - pos,)))

def shsampcag():
    print("=== ustr... cage:")
    shcag(nnx)
    shcag(nny)
    shcag(nnz)

def lcag(mot, locag):
    cage(mot, locag=locag, hicag=None)
def hcag(mot, hicag):
    cage(mot, locag=None, hicag=hicag)

def xcag(locag, hicag):
    cage(nnx, locag=locag, hicag=hicag)
def lxcag(locag):
    cage(nnx, locag=locag, hicag=None)
def hxcag(hicag):
    cage(nnx, locag=None, hicag=hicag)

def ycag(locag, hicag):
    cage(nny, locag=locag, hicag=hicag)
def lycag(locag):
    cage(nny, locag=locag, hicag=None)
def hycag(hicag):
    cage(nny, locag=None, hicag=hicag)

def zcag(locag, hicag):
    cage(nnz, locag=locag, hicag=hicag)
def lzcag(locag):
    cage(nnz, locag=locag, hicag=None)
def hzcag(hicag):
    cage(nnz, locag=None, hicag=hicag)

def cage(mot, locag=None, hicag=None):
    pos = mot.position
    l,h = mot.limits
    lolim = hilim = None
    if not None is locag:
        sl = float(locag)
        lolim = pos + sl
    if not None is hicag:
        sh = float(hicag)
        hilim = pos + sh
    set_lm(mot, lolim=lolim, hilim=hilim)

def sycag(mot, cag):
    cage(mot, -cag, cag)

def mvrcage(mot, locag=None, hicag=None):
    pos = mot.position
    l,h = mot.limits
    if None is locag:
        sl = l
    else:
        sl = l + float(locag)
    if None is hicag:
        sh = h
    else:
        sh = h + float(hicag)
    lolim = sl
    hilim = sh
    set_lm(mot, lolim=lolim, hilim=hilim)

def symvrcag(mot, cag):
    mvrcage(mot, -cag, cag)


# misc -------------------------------------------------------------------------

def yesno(quetxt, genindent=""):
    print("%s%s? " % (genindent,quetxt))
    print('%splease enter "y" or "n" below accordingly and confirm with :' % genindent)
    ans = input()
    if "y" == ans:
        return True
    else:
        return False

def slil(x):
   nvolpi5.intensity=x


def send_img(name, ext=None):
    if None is ext:
        fname = name
    else:
        fname = "%s.%s" % (name, ext)
    with open(fname, "rb") as f:
        data = f.read()
        data = b"data:image/png;base64," + base64.b64encode(data)
        SCAN_SAVING._icat_proxy.metadata_manager.proxy.uploadBase64(data)


#  -------------------------------------------------------------------------

def posfn_to_posname(s):
    if s.endswith('.json'):
        return s
    if s.endswith('._pos'):
        return s[:-5]
    else:
        return s

def posname_to_posfn(s):
    if s.endswith('.json'):
        return s
    if s.endswith('._pos'):
        return s
    else:
        return "%s._pos"

def pos_is_sane(s):
    return not pos_is_not_sane(s)

def pos_is_not_sane(s):
    if s.endswith('_pos'):
        cs = posfn_to_posname(s)
    elif s.endswith('.json'):
        cs = s[:-5]
    else:
        cs = s
    jfn = '%s.json' % cs
    pfn = '%s._pos' % cs
    jflg = path.exists(jfn)
    pflg = path.exists(pfn)
    if (jflg and pflg): return 1
    if (not jflg and not pflg): return -1
    return 0

def no_such_pos(s):
    return pos_is_not_sane(s) == -1

def pos_exists(s):
    return not no_such_pos(s)

def jfn_to_pfn(jfn):
    return ("%s._pos" % jfn[:-5])

def pfn_to_jfn(pfn):
    return ("%s._pos" % jfn[:-5])

def get_ptipe(s):
    if s.endswith('.json'):
        return 'j'
    else:
        return 'p'

def is_of_ptipe(pt, s):
    if not pt in ('j','p'):
        raise ValueError
    return get_ptipe(s) == pt

def to_other_pfn(s):
    if s.endswith('.json'):
        return s[:-5] + '._pos'
    else:
        return s[:-5] + '.json'


def stp(s, force=False):
    if no_such_pos(s):
        pass
    else:
        if pos_exists(s):
            if force and pos_is_sane(s):
                pass
            else:
                raise IOError('sorry - cannot overwrite %s' % s)

    s = posfn_to_posname(s)

    dc = dict(
        x = nnx.position,
        y = nny.position,
        z = nnz.position
    )
    with open("%s._pos" % s, 'w') as f:
        json.dump(dc, f)

def readp():
    dc = dict(
        x = nnx.position,
        y = nny.position,
        z = nnz.position
    )
    return dc

def diffp(t,s=None):
    if not None is s:
        s = posfn_to_posname(s)
    t = posfn_to_posname(t)
    if None is s:
        sp = readp()
    else:
        sp = loadp(s)
    tp = loadp(t)
    diffdc = {}
    print('diff_pos:')
    for k in ('x','y','z'):
        diffdc['d%s' % k] = tp[k] - sp[k]
    for k in ('x','y','z'):
        print('    d%s = %10.5f' % (k, diffdc['d%s' % k]))
    return diffdc

def appldiffp(diffdc):
    umvr(nnx, diffdc['x'])
    umvr(nny, diffdc['y'])
    umvr(nnz, diffdc['z'])

def loadp(s):
    if no_such_pos(s):
        raise IOError('pos %s does not exist' % s)
    if pos_is_not_sane(s):
        raise ValueError('pos %s is not sane' % s)

    if s.endswith('json'):
        with open("%s" % s, 'r') as f:
            jdc = json.load(f)
            dc = {}
            dc['x'] = jdc['motors']['nnx']
            dc['y'] = jdc['motors']['nny']
            dc['z'] = jdc['motors']['nnz']
        return dc
    else:
        s = posfn_to_posname(s)
        with open("%s._pos" % s, 'r') as f:
            dc = json.load(f)
        return dc
def ls():
    os.system('ls')

def lspos():
    ll = os.listdir('.')
    ll = sorted(ll)
    print("============== stp:")
    for x in ll:
        if x.endswith('._pos'):
            print(x[:-5])
    print("============== stp:")
    for x in ll:
        if x.endswith('.json'):
            print(x)
    #return ll
    #return ll
    return None

def gopxy(s, show_only=False):
    s = posfn_to_posname(s)
    dc = loadp(s)
    print("current pos:")
    shp()
    if show_only:
        print('found pos:')
        print(nny.name, dc['y'])
        print(nnz.name, dc['z'])
        return
    mv(nny, float(dc['y']))
    mv(nnz, float(dc['z']))
    print("new pos:")
    shp()

def gopos(s, show_only=False):
    s = posfn_to_posname(s)
    print("gopos:s =", s)
    dc = loadp(s)
    print("current pos:")
    shp()
    if show_only:
        print('found pos:')
        print(nnx.name, dc['x'])
        print(nny.name, dc['y'])
        print(nnz.name, dc['z'])
        return
    mv(nnx, float(dc['x']))
    mv(nny, float(dc['y']))
    mv(nnz, float(dc['z']))
    print("new pos:")
    shp()

rstp = gopos

def shpos(s):
    s = posfn_to_posname(s)
    gopos(s, show_only=True)


def ppos(m):
    print (m.name, ":", m.position)

def shp():
    ppos(nnx)
    ppos(nny)
    ppos(nnz)













###############################################################
###############################################################
###############################################################
def qgoto_cen(ctr, mot):
    global SCANS
    s = SCANS[-1]
    s.goto_cen(ctr, axis=mot)

def qgoto_peak(ctr, mot):
    global SCANS
    s = SCANS[-1]
    s.goto_peak(ctr, axis=mot)

def qgoto_pt(idx):
    global SCANS

    s = SCANS[-1]
    d = s.get_data()
    ax_keys = [k for k in d.keys() if k.startswith("axis")]


    todo = []

    print ("\npreview:")
    for k in ax_keys:
        a,nam = k.split(":")
        mot = getattr(SG, nam)
        pos = d[k][idx]
        todo.append((mot, pos))
        print ("    %s.move(%f)" % (mot.name, pos))

    print ("\nnow we will be moving things ...")
    for (m,p) in todo:
        print ("\n    %s.move(%f)" % (m.name, p))
        ans = yesno("move there", genindent="    ")
        if ans:
            m.move(p)
        else:
            print("    okay - nothing done ...")



def get_glob_scans():
    return SCANS


# ### more specific

DIODE_CTR = ct32
ION_CTR = ct34

def sh_is_closed(sh):
    return 'CLOSED' in str(sh.state)

def sh_is_open(sh):
    return 'OPEN' in str(sh.state)

def so():
    if sh_is_open(sh3):
        print ("shutter", sh3, "probably already open.")
        return True
    while True:
        try:
            print("trying to open", sh3, "...")
            sh3.open()
            if sh_is_open(sh3):
                print ("shutter", sh3, "probably open.")
                return True
            else:
                print ("shutter state is", sh3.state)
                time.sleep(1)
        except:
            if sh_is_open(sh3):
                print ("shutter", sh3, "probably open.")
                return True
            time.sleep(1.0)

def sc():
    if sh_is_closed(sh3):
        print ("shutter", sh3, "probably already closed.")
        return True
    while True:
        try:
            sh3.close()
            if sh_is_closed(sh3):
                print ("shutter", sh3, "probably closed.")
                return True
            elif sh_is_open(sh3):
                print ("shutter", sh3, "probably open.")
            else:
                print ("shutter", sh3, "in fuzzy state - giving up:")
                print ("state was:",  sh3.state)
                return
                
        except:
            try:
                if sh_is_closed(sh3):
                    print ("shutter", sh3, "probably closed.")
                    return True
                elif sh_is_open(sh3):
                    print ("shutter", sh3, "probably open.")
                else:
                    print ("shutter", sh3, "in fuzzy state - giving up.")
                    print ("state was:",  sh3.state)
                    return
            except:
                print ("shutter", sh3, "error getting state - giving up.")
                return
                
        print("sleeping - you can type  to interrupt ...")
        time.sleep(1.0)


def goc(mot):
    qgoto_cen(DIODE_CTR, mot)

def gop(mot):
    qgoto_peak(DIODE_CTR, mot)

def gopt(idx):
    qgoto_pt(idx)
def gnnp(py, pz):
    print (f'movong nnp2, nnp3 ...')
    print (f'    old: {nnp2.position}    {nnp3.position}')
    tpy = nnp2.dial2user(py)
    tpz = pz
    mv(nnp2, tpy)
    mv(nnp3, tpz)
    print (f'    new: {nnp2.position}    {nnp3.position}')

def get_gnnp(idx, *p):
    m1, ll1, ul1, n1, m2, ll2, ul2, n2, expt = p
    yp = m1.position
    zp = m2.position
    (i2,i1) = divmod(idx, n1)
    stp1 = (ul1-ll1)/n1
    stp2 = (ul2-ll2)/n2
    d1 = i1*stp1
    d2 = i2*stp2
    return yp + d1 + ll1, zp + d2 + ll2

def goto_gnnp(idx, *p):
    print('moving nnp2 nnp3')
    wnnp()
    (y,z) = get_gnnp(idx, *p)
    mv(nnp2, y)
    mv(nnp3, z)
    wnnp()
    



# MG and CHAIN management
#
#

# measurement groups and chains
#

#def mgd_old():
#    # diode (p201) only
#    #DEFAULT_CHAIN.set_settings(default_chain_eh3a['chain_config'])
#    MG_EH3a.set_active()
  
#def mgeig_old():
#    # eiger detector and diode
#    DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])
#    MG_EH3.set_active()
    
def mgd():
    mgsw('d')
    
def mgeig():
    photon_energy = ID13_XRAY_ENERGY*1000.0
    #eiger.camera.photon_energy=photon_energy
    print(f'setting: eiger.camera.photon_energy={photon_energy}')
    eiger.camera.photon_energy=photon_energy
    mgsw('e')
    
def rmgeig():
    mgeig()
    MG_EH3a.enable("*r:r*")
    
def mgpsie():
    print('setting up psi_eiger_500k ...')
    # &&&
    #
    # psi eiger usere mpx trigger cable as it is mounted on the arm
    #

    # default chain command has to be set after mgd()!
    MG_EH3a.disable('*')
    mgd()
    psi_eiger_500k
    psi_eiger_500k.proxy.saving_index_format="%06d"

    DEFAULT_CHAIN.set_settings(chain_psi_eiger_500k['chain_config'])

    #nmux.switch('O5_MPX_POL','NORMAL')
    nmux.switch('O5_MPX_POL','INVERTED')
    photon_energy = ID13_XRAY_ENERGY*1000.0
    print(f'psi_eiger_500k photon_energy: {photon_energy}')
    threshold_energy = 0.5*photon_energy
    psi_eiger_500k.camera.threshold_energy = threshold_energy
    MG_EH3a.enable('psi_eiger_500k:image')
    MG_EH3a.set_active()
    print('psi_eiger_500k setup done.')

def mgeig_x():
    mgeig()
    MG_EH3a.enable('*xmap3*')

def mgmpx():
    mgsw('m')

def mgmpx_x():
    mgsw('mx')

#def mgeig_x():
#    mgsw('ex')
    
def mgpco():
    # pco camera
    DEFAULT_CHAIN.set_settings(chain_pco['chain_config'])
    MG_PCO.set_active()

def mgvlm():
    # vlm2 microscope camera
    DEFAULT_CHAIN.set_settings(default_chain_eh3vlm2['chain_config'])
    MG_VLM.set_active()

def mgd_x():
    mgd()
    MG_EH3a.enable('*xmap3*')

# MG_EH3a config switching
def mgsw(s=''):
    DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])
    MG_EH3a.disable('*')
    if 'x' in s:
        MG_EH3a.enable('*xmap3*')
    MG_EH3a.enable('*ct32*')
    MG_EH3a.enable('*ct34*')
    #MG_EH3a.enable('*ct36*')
    #MG_EH3a.enable('*force_real*')
    #MG_EH3a.enable('*r:r*avg')
    MG_EH3a.enable('*er:acq*')
    if s[0] == 'd':
        pass
    elif s[0] == 'e':
        MG_EH3a.enable('*eiger:i*')
    elif s[0] == 'm':
        MG_EH3a.enable('*3:im*')
    elif s[0] == 'em':
        MG_EH3a.enable('*3:im*')
        MG_EH3a.enable('*eiger:i*')
    else:
        raise ValueError
    MG_EH3a.set_active()

# change defaiult chain e.g. between PCO and EIGER
PSEUDO_DOC = """
EH3 [1]: DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])                                                                                        
EH3 [2]: DEFAULT_CHAIN.set_settings(chain_pco['chain_config'])                                                                                                
EH3 [3]: DEFAULT_CHAIN._settings.keys                                                                                                                         
Out [3]: 

EH3 [4]: DEFAULT_CHAIN._settings                                                                                                                              
Out [4]: {: {'acquisition_settings': filename:,plugin:'default',{'acq_trigger_mode': 'INTERNAL_TRIGGER', 'prepare_once': False, 'start_once': False}, 'master':  at 0x7f3970eec390 with factory functools.partial(, 'tcp://lid133:8909')>}}

EH3 [5]: DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])                                                                                        
EH3 [6]: DEFAULT_CHAIN._settings                                                                                                                              
Out [6]: {: {'acquisition_settings': filename:,plugin:'default',{'acq_trigger_mode': 'EXTERNAL_TRIGGER_MULTI', 'prepare_once': True, 'start_once': True}, 'master':  at 0x7f3970eec390 with factory functools.partial(, 'tcp://lid133:8909')>}}
"""

#
#
#def mge():
#    #MG_EIGER.set_active()
#    pass

#def mgp():
#    MG_PCO.set_active()

def wps():
    wm(phg, pho, pvg, pvo)

def wbet():
    wm(wbety, wbetz)

def wss6():
    wm(ns6hg, ns6ho, ns6vg, ns6vo)

def wnnp():
    wm(nnp1,nnp2, nnp3)


def wdet():
    wm(ndetx, ndety, ndetz)

def zeronnp():
    wm(nnp1,nnp2, nnp3)
    mv(nnp1, 125.0)
    mv(nnp2, 125.0)
    mv(nnp3, 125.0)
    wm(nnp1,nnp2, nnp3)

def wmars():
    wm(nnp4,nnp5, nnp6)

def zeromars():
    wm(nnp4,nnp5, nnp6)
    mv(nnp4, 50.0)
    mv(nnp5, 50.0)
    mv(nnp6, 50.0)
    wm(nnp4,nnp5, nnp6)

def whx():
    wm(nnx, nny, nnz)

def set_outgain(g):
    print(nmoco.comm_ack("outbeam %g" % g))
    #print(nmoco.info())

def set_ingain(g):
    print(nmoco.comm_ack("inbeam %g" % g))
    #print(nmoco.info())

def apalign():
    print("where are we:")
    wm(nt2t, nt2j)
    print(1)
    dscan(nt2t, -0.03,0.03,60,0.03, DIODE_CTR)
    goc(nt2t)
    where()
    sleep(0.5)
    dscan(nt2j, -0.03,0.03,60,0.03, DIODE_CTR)
    goc(nt2j)
    where()
    print("where are we now:")
    wm(nt2t, nt2j)

def nt4align():
    print("aligning aperture mounted on nt4:")
    print("where are we:")
    wm(nt4t, nt4j)
    dscan(nt4t, -0.03,0.03,60,0.03, DIODE_CTR)
    goc(nt4t)
    where()
    sleep(0.5)
    dscan(nt4j, -0.03,0.03,60,0.03, DIODE_CTR)
    goc(nt4j)
    where()
    print("where are we now:")
    wm(nt4t, nt4j)

def nt2alignmll():
    raise('no!')
    print("aligning aperture mounted on nt5 (for MLLs):")
    print("where are we:")
    wm(nt2t, nt2j)
    dscan(nt2t, -0.05,0.05,50,0.05, DIODE_CTR)
    goc(nt2t)
    where()
    sleep(0.5)
    dscan(nt2j, -0.05,0.05,50,0.05, DIODE_CTR)
    goc(nt2j)
    where()
    print("where are we now:")
    wm(nt2t, nt2j)

# ID13 scans
#
load_script('spiralscan')
# shutter
# fsh

SHUTTER_STATE = {
    True  : 'OPEN',
    False : 'CLOSED'
}
SHUTTER_ZPOS = {
    35  : 'SHUTTER IN',
    -15 : 'SHUTTER OUT'
}

def sign(x):
    if x < 0:
        return -1
    else:
        return 1

def get_shutter_inout():
    zpos = ntubez.position
    #x = int(zpos*10+sign(zpos)*0.5)
    x = round(zpos*10)
    try:
        return SHUTTER_ZPOS[x]
    except KeyError:
        return 'SHUTTER Z NOT ALIGNED'


def fshtrigger():
    nfsh.mode=nfsh.EXTERNAL
    #nmux.switch("FSH_MODE", "P201")
    nmux.switch("SHUTTER", "P201")
    
def fshclose():
    nfsh.mode=nfsh.MANUAL
    nfsh.close()

def fshopen():
    nfsh.mode=nfsh.MANUAL
    nfsh.open()

def fshout():
    mv(ntubez, -1.5)

def fshin():
    mv(ntubez, 3.5)

def fshstate():
    print("shutter info:")
    print("    shutter in/out:           %s" % get_shutter_inout())
    print("    shutter state (nfsh):     %s" % SHUTTER_STATE[nfsh.is_open])
    print("    shutter mode (nfsh):      %s" % nfsh.mode)
    print("    nmux SHUTTER:             %s" % nmux.getOutputStat('SHUTTER'))
    print("    nmux DETECTOR:            %s\n" % nmux.getOutputStat('DETECTOR'))

#
# mono managment
#
load_script("ccmono.py")
try:
    ccmo = CCMono()
except:
    pass

Thefollowingisobviouslytoonaive = """
try:
    MG_EH3.disable("*eig*")
except:
    print ('error occurred MG_EH3.disable("*eig*")')
try:
    MG_EH3.disable("*roi*")
except:
    print ('error occurred MG_EH3.disable("*roi*")')
try:
    MG_EH3.enable("*image*")
except:
    print ('error occurred MG_EH3.enable("*image*")')
"""

# area detector utilities
def troi_to_blissroi(x):
    (b,a),(d,c) = x 
    return (a,b,c,d) 



# ### KMAP


# ### Roberto's tests
#
#import sys
#print(sys.path)

#print("Executing ID13-EH3 setup (beamline_configuration/sessions/eh3.py) ...")
#default_chain = setup_globals.default_chain_eh3a

#print("Loading kmap (id13.git/id13/scripts/kmap.py) ...")
#from id13.scripts import kmap

print("MGD ...")
mgd()

print('post MGD')
plotinit(ct32)

def relo():
    load_script("mcb_test_eh3")
    load_script("edgeinfra1")

relo()

SCAN_SAVING.images_prefix      = '{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_' 
SCAN_SAVING.scan_number_format = '%05d'

def akmap(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time, frames_per_file = 400):
    try:
      kmap.akmap(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time, frames_per_file = frames_per_file)
    finally:
      s1 = ('kmap.akmap(%s ,%f ,%f ,%d ,%s ,%f ,%f ,%d ,%f ,frames_per_file=%d)' %(fast_mot.name, xmin, xmax, x_nb_points, slow_mot.name, ymin, ymax, y_nb_points, expo_time, frames_per_file))
      s2 = ('start time: %s' %SCANS[-1].scan_info['start_time_str']) 
      s3 = ('metafile name: %s' %SCANS[-1].scan_info['filename']) 
      s4 = ('scan number: %d' %(SCANS[-1].scan_info['scan_nb'])) 
      lotoo.elog_info('ID13log: \n' + s1 +  '\n' + s2 + '\n' + s3 + '\n' + s4)
      zeronnp()
      
def dmeshlog(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time):
    try:
        s1 = ('dmesh(%s ,%f ,%f ,%d ,%s ,%f ,%f ,%d ,%f) started' %(fast_mot.name, xmin, xmax, x_nb_points, slow_mot.name, ymin, ymax, y_nb_points, expo_time))
        lotoo.elog_info('ID13log: \n' + s1)
        dmesh(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time)
    finally:
        s1 = ('dmesh(%s ,%f ,%f ,%d ,%s ,%f ,%f ,%d ,%f) finished' %(fast_mot.name, xmin, xmax, x_nb_points, slow_mot.name, ymin, ymax, y_nb_points, expo_time))
        s2 = ('start time: %s' %SCANS[-1].scan_info['start_time_str']) 
        s3 = ('metafile name: %s' %SCANS[-1].scan_info['filename']) 
        s4 = ('scan number: %d' %(SCANS[-1].scan_info['scan_nb'])) 
        lotoo.elog_info('ID13log: \n' + s1 +  '\n' + s2 + '\n' + s3 + '\n' + s4)
        zeronnp()
      
def logoutput():
    lotoo.elogbook.info('ID13log: \n' + BlissRepl().app.output[-1])
    
# to initialize SCANS[-1]
#loopscan(2,0.005)
    
def status_table():
    print("input (wheel/break - pco/eiger/arm):")
    print(wcid13f.get("wpco","weiger","warm","bpco","beiger","barm"))# verify input
    print("status pressure:")
    print(wcid13f.get("pwpco","pweiger","pwarm","pbpco","pbeiger","pbarm"))# verify pressure directly

def syncwheels():
    nwpco.controller.raw_write("85:ESYNC")
    nwpco.sync_hard()
    nweiger.controller.raw_write("86:ESYNC")
    nweiger.sync_hard()
    nwarm.controller.raw_write("87:ESYNC")
    nwarm.sync_hard()

def break_pco_active():
    wcid13f.set("bpco",1)

def break_pco_deactive():
    wcid13f.set("bpco",0)
    
def break_arm_active():
    wcid13f.set("barm",1)

def break_arm_deactive():
    wcid13f.set("barm",0)
    
def break_eiger_active():
    wcid13f.set("beiger",1)

def break_eiger_deactive():
    wcid13f.set("beiger",0)
    
    
# ##### function for bs align

def bsalign():
    print("where are we:")
    wm(nbsy,nbsz)
    
    #dscan(nbsy,-0.5,0.5,50,0.02,ct32)
    #goc(nbsy)
    #where() 
    #sleep(0.5)   
    #dscan(nbsz,-0.5,0.5,50,0.02,ct32)
    #goc(nbsz)
    #where()
    
    #sleep(0.5) 
       
    dscan(nbsy,-0.35,0.35,70,0.1,ct32)
    goc(nbsy)
    where()
    sleep(0.5)
    dscan(nbsz,-0.35,0.35,70,0.1,ct32)
    goc(nbsz)
    where()
    
    print("where are we now:")
    wm(nbsy,nbsz)

def close_bsalign():
    print("where are we:")
    wm(nbsy,nbsz)
    
    dscan(nbsy,-0.7,0.7,35,0.02,ct32)
    goc(nbsy)
    where()
    sleep(0.5)
    dscan(nbsz,-0.7,0.7,35,0.02,ct32)
    goc(nbsz)
    where()
    
    print("where are we now:")
    wm(nbsy,nbsz)

def dmyscan():
    dscan(nnp2, 1,1,1,0.01)

#
# det lateral wheel move  managment
#
load_script("eh3dettable")
try:
    eh3dett = ett = EH3DetTable(nweiger, nwarm, nwpco)
    mot_eiger = ett.create_softaxis('eiger')
    set_lm(mot_eiger, -10,1128)
    mot_pco   = ett.create_softaxis('pco')
    mot_arm   = ett.create_softaxis('arm')
except:
    print('Warning - could not initialize EH3DetTable ...')
load_script("eh3detarm")

#print('two theta arm functionality not nitialized!')
try:
    print('ATTENTION !!! trying to initialize the two theta arm ...')
    eh3arm = EH3Arm()
    print('ATTENTION !!! two theta arm functionality initialized !!!')
except:
     print('Warning - could not initialize EH3Arm ...')

#
# map goto
#

def vmap(cmd_str, mot0, ll0, ul0, npts0, mot1, ll1, ul1, npts1, expt, exec_scan=True):
    print('====[INFO]===')
    astart_0 = mot0.position + ll0
    astart_1 = mot1.position + ll1
    stp_0    = (ul0-ll0)/(npts0-1)
    stp_1    = (ul1-ll1)/(npts1-1)
    ncols    = npts0
    print(f'    frameno_to_pos:\n        {mot0.name}, {mot1.name}, {astart_0:12.6f}, {astart_1:12.6f}, {stp_0:12.6f}, {stp_1:12.6f}, {ncols}')
    if exec_scan:
        if 'dmesh' == cmd_str:
            dmesh(mot0, ll0, ul0, npts0-1, mot1, ll1, ul1, npts1-1, expt)
        elif 'dkmap' == cmd_str:
            kmap.dkmap(mot0, ll0, ul0, npts0, mot1, ll1, ul1, npts1, expt)
        else:
            raise ValueError(f'illegal scan type: {cmd_str}')
            

def frameno_to_pos(astart_0, astart_1, stp_0, stp_1, ncols, frame_no):
    (r,c) = divmod(frame_no, ncols)
    pos_0 = astart_0 + c*stp_0
    pos_1 = astart_1 + r*stp_1
    return (pos_0, pos_1)


def frameno_goto(mot0, mot1, astart_0, astart_1, stp_0, stp_1, ncols, frame_no):
    (p0, p1) = frameno_to_pos(astart_0, astart_1, stp_0, stp_1, ncols, frame_no)
    print(f'target: {mot0.name}, {mot1.name}, {p0:12.6f}, {p1:12.6f}')
    ans = yesno('go there?')
    if ans:
        umv(mot0, p0)
        umv(mot1, p1)
    else:
        print(f'[{ans}]')

#def ls2950_setup():
#    print('performing ls2950 setup ...')
#    eiger.camera.photon_energy=13000
#    MG_EH3a.enable('*saxs*')
#    print('done.')

#ls2950_setup()


def eigerhws(state):
    if state:
        eiger.saving._managed_mode = "HARDWARE"
        eiger.saving.file_format = "HDF5"
        SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}"
    else:
        eiger.saving._managed_mode = "SOFTWARE"
        eiger.saving.file_format = "HDF5BS"
        SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"
        
from contextlib import contextmanager

@contextmanager
def eigerhws_manager():
    eigerhws(True)
    try:
        yield
    finally:
        eigerhws(False)

# indenter
INDA_FLG = False
if INDA_FLG:
    load_script("indenter_a")
    inda = make_inda()
    inda_pos_motor = inda.create_posaxis('inda_pos')
    inda_force = inda.create_forcecounter('inda_force')
    inda_raw_force = inda.create_raw_forcecounter('inda_raw_force')

# ls3044 infra structure ...

TRIM_CTR = 'eiger:roi_counters:trim_avg'

def set_eiger_beam_trimming():
    mgeig()
    eigerhws(False)
    eiger.roi_counters.set('trim', (1161, 1387, 180, 180))
    MG_EH3a.enable('eiger:roi_counters:trim_avg')

def set_eiger_beam_hwsx():  
    mgeig_x()
    eiger.roi_counters.clear()
    eigerhws(True)

def trim_goc(mot):
    qgoto_cen(TRIM_CTR, mot)

def trim_gop(mot):
    qgoto_peak(TRIM_CTR, mot)

def trim_dscan(*p):
    plotinit('eiger:roi_counters:trim_avg')
    dscan(*p)


#############################################3
# DOO logbook infra structure
load_script('plotexport')


def doops():
    # export current scatterplot to logbook
    elog_plot(scatter=True)

def doopc():
    # export current curve to logbook
    elog_plot(scatter=False)

dooc = elog_print
dooa = elog_add

def doomp(*p, cmt=None):
    if not None is cmt:
        dooc(f'position_comment {cmt}')
    for m in p:
        dooc(f'position_log: {m.name} {m.position}')

def doom(*p, cmt=None):
    if not None is cmt:
        dooc(f'position_comment {cmt}')
    ll = []
    for m in p:
        ll.append(f'position_log: {m.name} {m.position}')
    dooc('\n'.join(ll))

def dooi(s):
    # intention
    elog_print(f'==== INTENTION:\n{s}')

def doos(s):
    # marked
    elog_print(f'==== SAY:\n{s}')

load_script('siemens_series')
load_script('nt2simple')
#bsalign = largebsalign

# print('loading things "en dur" for es1244 ...')
#
# Virginie's and Tilman's speckle plate management
#
#load_script('es1244_static')
#es1244_static_setup()
#load_script('sc5407_center')
#sc5407_motdc = make_sc5407_motdc()
#ggg = GenStorePos(sc5407_motdc, ALLMOTNAMES, SUBSETS)
#load_script('ls3155_infra')
#load_script('mi1355')
try:
    print('trying to load "eigfix_interface" ...')
    load_script('eigfix_interface')
    print('loading "eigfix_interface" done')
except:
    print('\n!!!\n!!! loading "eigfix_interface" failed\n!!!')
try:
    print('trying to load "sc5337_infra" ...')
    load_script('sc5337_infra')
    print('loading "sc5337_infra" done')
except:
    print('\n!!!\n!!! loading "sc5337_infra" failed\n!!!')


#load_script('ma5733_infra')

#try:
#    print(".. reset piezo proxy  ???!!! ...")
#    try:
#        reset_piezo_proxy()
#    except ValueError:
#        print("done...")
#finally:
# happy rounding ...

# import bliss.shell.standard as blshell
# _x_rounder = blshell.rounder
# def myrounder(a,b):
#     if a > 0.001:
#         a = 0.001
#     return _x_rounder(a,b)
# blshell.rounder = myrounder

def reset_mars_proxy():
    for motor_name in ["nnp4"]:
        motor = getattr(current_session.setup_globals, motor_name)
        motor.controller.sock.kill_proxy_server()
        motor.enable()
        motor.sync_hard()
    for motor_name in ["nnp4"]:
        motor.enable()
    zeromars()

    
def reset_hera_proxy():
    for motor_name in ["nnp1", "nnp2", "nnp3"]:
        motor = getattr(current_session.setup_globals, motor_name)
        motor.controller.sock.kill_proxy_server()
    for motor_name in ["nnp1", "nnp2", "nnp3"]:
        motor.enable()
        motor.sync_hard()
    for motor_name in ["nnp1", "nnp2", "nnp3"]:
        motor.enable()
        motor.sync_hard()
    zeronnp()

    
#print(".. enable mars piezo axes !!! ...")
# reset_mars_proxy()
# for mot in (nnp4, nnp5, nnp6):
#    print ('before: sync hard', mot.name, '.disabled =', mot.disabled)
#    mot.sync_hard()
#    print ('before:', mot.name, '.disabled =', mot.disabled)
#    if mot.disabled:
#        mot.enable()
#    print ('after :', mot.name, '.disabled =', mot.disabled)

#print(".. enable hera piezo axes !!! ...")
#reset_hera_proxy()
# for mot in (nnp1, nnp2, nnp3):
#     print ('before: sync hard', mot.name, '.disabled =', mot.disabled)
#     #mot.sync_hard()
#     print ('before:', mot.name, '.disabled =', mot.disabled)
#     if mot.disabled:
#         mot.enable()
#     print ('after :', mot.name, '.disabled =', mot.disabled)


def gopi(x,y):
    mv(nnp2, x, nnp3, y)

def loff_kmap(m1, ll1, ul1, n1, m2, ll2, ul2, n2, expt, frames_per_file=None,
    loff1=-5.0, loff2=-5.0):
    if None is frames_per_file:
        frames_per_file = n1
    assert m1.name in ('nnp1','nnp2','nnp3')
    assert m2.name in ('nnp1','nnp2','nnp3')
    ini_m1_pos = m1.position
    ini_m2_pos = m2.position
    try:
        mv(m1, ini_m1_pos + ll1 + loff1)
        mv(m2, ini_m2_pos + ll2 + loff2)
        kmap.dkmap(m1, -loff1, -ll1-loff1+ul1, n1, m2, -loff2, -ll2-loff2+ul2, n2, expt,
            frames_per_file=frames_per_file)
    finally:
        print('returning motors to initial position:', ini_m1_pos, ini_m2_pos)
        mv(m1, ini_m1_pos)
        mv(m2, ini_m2_pos)
        print('setting fastshutter to fshtrigger')

load_script('gotoclick_thingy')
def eiger_rois_on():
    MG_EH3a.enable("*ger:r*")


def amgd():
    mgd()
    MG_EH3a.enable("*33*")
def amgdx():
    amgd()
    MG_EH3a.enable("*fx8*")

def rmgeig():
    mgeig()
    eiger_rois_on()
    
def rmgeigx():
    mgeig()
    eiger_rois_on()
    MG_EH3a.enable("*xmap3*")

def mgeigx():
    mgeig()
    MG_EH3a.enable("*xmap3*")

    
BLOSSOM_ON = False
if BLOSSOM_ON:
    sys.path.append('/users/opid13/mcb_feb24/lib')

GENERIC_INFRA = True
if GENERIC_INFRA == True:
    load_script('generic_infra_eh3')

ES1404=False
if ES1404:
    load_script('es1404_infra')
    emg = ES1404Infra(15.0)

CH6831 = False
if CH6831:
    load_script('ch6831')
    
MA6190 = False
if MA6190:
    load_script('ma6190_infra')

SC5471 = False
if SC5471:
    load_script('sc5471_infra')
    
SC5436 = False
if SC5436:
    load_script('sc5436_infra')
# Workflow triggering (Loic Huder, 28/05/24)    
from blissoda.id13.xrpd_processor import Id13XrpdProcessor as _XrpdProcessor
xrpd_processor = _XrpdProcessor(enable_plotter=False)

def set_r3roi(name, r3inp):
    ((a,b),(c,d)) =r3inp
    eiger.roi_counters.set(name, (b,a,d,c))


load_script('scanpool_infra1')

# Temp setup indenter in eh3 session
# load_script("indenter_a")
# inda = make_inda()
# inda_pos_motor = inda.create_posaxis('inda_pos_motor')
# inda_pos_counter = inda.create_positioncounter('inda_pos_counter')
# inda_force = inda.create_forcecounter('inda_force')
# inda_raw_force = inda.create_raw_forcecounter('inda_raw_force')
