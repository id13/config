from bliss.setup_globals import *
from bliss import setup_globals as SG
from os import path

from bliss import current_session
import time
import pprint



load_script("loadscript_sentinel.py")
try:
    THE_SENTINEL = SENTINEL
except:
    print("load_script does not work - skipping entire ebsmar1_setup!")
    THE_SENTINEL = 0

if THE_SENTINEL:
    print ("ebsmar1 setup is on the right track ...")
    load_script("ebsmar1.py")
    #load_script("ebsmar1_scratch.py")
    load_script("gapscan_alternative.py")
    load_script("meta_data_access.py")
    load_script("preset_fsh.py")
    
    print("")
    print("Welcome to your new 'ebsmar1' BLISS session !! ")
    print("")
    print("You can now customize your 'ebsmar1' session by changing files:")
    print("   * /ebsmar1_setup.py ")
    print("   * /ebsmar1.yml ")
    print("   * /scripts/ebsmar1.py ")
    print("")
    print("")
    print("run id13 staff definitions:")
    p201 = p201_eh2_0
    ct22 = p201_eh2_0.counters.ct22
    ct24 = p201_eh2_0.counters.ct24
    ct25 = p201_eh2_0.counters.ct25
    
    
    #ufsh_preset = Preset_ufsh()
    #DEFAULT_CHAIN.add_preset(ufsh_preset)
    
    print("+ set the default chain ...")
    DEFAULT_CHAIN.set_settings(default_chain_eh2['chain_config'])
    
    
    
    ct_str_dc = dict(
        ct22= 'p201_eh2_0:ct2_counters_controller:ct22',
        ct24= 'p201_eh2_0:ct2_counters_controller:ct24',
        ct25= 'p201_eh2_0:ct2_counters_controller:ct25'
    )
    ct_str_shortnames_dc = {
        ct22:'ct22',
        ct24:'ct24',
        ct25:'ct25'
    }
    
    
    def qsc(t):
        return ct(t, p201_eh2_0)
    
    def qsd(mot, ll, ul, n, t):
        return dscan(mot, ll, ul, n, t, p201_eh2_0)
    
    def qgoto_cen(ctr, mot):
        global SCANS
        s = SCANS[-1]
        s.goto_cen(ctr, axis=mot)
    
    def qgoto_peak(ctr, mot):
        global SCANS
        s = SCANS[-1]
        s.goto_peak(ctr, axis=mot)
    
    def qgoto_pt(idx):
        global SCANS
    
        s = SCANS[-1]
        d = s.get_data()
        ax_keys = [k for k in d.keys() if k.startswith("axis")]
    
    
        todo = []
    
        print ("\npreview:")
        for k in ax_keys:
            a,nam = k.split(":")
            mot = getattr(SG, nam)
            pos = d[k][idx]
            todo.append((mot, pos))
            print ("    %s.move(%f)" % (mot.name, pos))
    
        print ("\nnow we will be moving things ...")
        for (m,p) in todo:
            print ("\n    %s.move(%f)" % (m.name, p))
            ans = yesno("move there", genindent="    ")
            if ans:
                m.move(p)
            else:
                print("    okay - nothing done ...")
        
    
    def confirmed_move(posset_key, posdc):
        mot_keys = POSSET_KEY_SETS[posset_key].split()
    
        print ("\nmove preview:")
        for mk in mot_keys:
            print("    %s to %f" % (mk, posdc[mk]))
    
        print ("\nnow we will be moving things ...")
        for (mk) in mot_keys:
            m = getattr(SG, mk)
            mn = m.name
            pos = posdc[mn]
            print ("\n    %s.move(%f)" % (mn, pos))
            ans = yesno("move there", genindent="    ")
            if ans:
                m.move(pos)
            else:
                print("    okay - nothing done ...")
    
        
    def pos_show(posset_key, posdc):
        mot_keys = POSSET_KEY_SETS[posset_key].split()
    
        print ("\npos:")
        for mk in mot_keys:
            print("    %s to %f" % (mk, posdc[mk]))
    
    
    def qshow_scan_start(scanno, posset_key):
    
    
        fname = "%s.h5" % (SG.SCAN_SAVING.data_filename,)
        adpth = path.join(SG.SCAN_SAVING.base_path, 'ebsmar1')
        dh5 = DataH5(fname, adpth=adpth)
        try:
            #print("opening dh5 ...")
            dh5.open_rb()
            #print("find sk ...")
            sk = dh5.find_scankey(scanno)
            #print("get scan pos ...")
            dc = dh5.get_scanpositions(sk)
            #print("getting done ...")
        finally:
            #print("closing dh5 ...")
            dh5.close()
            #print("closing done ...")
        pos_show(posset_key, dc)
    
    def qgoto_scan_start(scanno, posset_key):
    
        print ( "\nconfimred move - POSSET : %s - scan number : %d" % (posset_key,scanno))
        fname = "%s.h5" % (SG.SCAN_SAVING.data_filename,)
        adpth = path.join(SG.SCAN_SAVING.base_path, 'ebsmar1')
        dh5 = DataH5(fname, adpth=adpth)
        try:
            dh5.open_rb()
            sk = dh5.find_scankey(scanno)
            dc = dh5.get_scanpositions(sk)
        finally:
            dh5.close()
            pass
    
        confirmed_move(posset_key,dc)
    
    
    def qgoto_pt(idx):
        global SCANS
    
        s = SCANS[-1]
        d = s.get_data()
        ax_keys = [k for k in d.keys() if k.startswith("axis")]
    
    
        todo = []
    
        print ("\npreview:")
        for k in ax_keys:
            a,nam = k.split(":")
            mot = getattr(SG, nam)
            pos = d[k][idx]
            todo.append((mot, pos))
            print ("    %s.move(%f)" % (mot.name, pos))
    
        print ("\nnow we will be moving things ...")
        for (m,p) in todo:
            print ("\n    %s.move(%f)" % (m.name, p))
            ans = yesno("move there", genindent="    ")
            if ans:
                m.move(p)
            else:
                print("    okay - nothing done ...")
    
    """
    def gotoms(sp, backw=-1, mnam=None):
        s = SCANS[backw]
        if mnam:
            key = "axis:%s" % mnam
        else:
            key = "axis"
        d = s.get_data()
        arr = d[key]
        pos = arr[sp]
    """
    
    def so():
        sh1.open()
        sh2.open()
        if sh1.is_open and sh2.is_open:
            return True
        else:
            return False
    
    
    def sc():
        sh1.close()
        sh2.close()
        if sh1.is_closed and sh2.is_closed:
            return True
        else:
            return False
    
    print("""\
    alias_like:
        default p201 count: qsc    # q standard ct
        default p201 count: qsd    # q standard dscan
    """)
    print("id13 staff done.")
    print("before:ustrz.tolerance =", ustrz.tolerance)
    ustrz.config.set("tolerance", 0.0005)
    print("after :ustrz.tolerance =", ustrz.tolerance)
    print("")
    print("")
    def fix_micos(x,z):
        if not (x.name == 'ustrx' and z.name == 'ustrz'):
            print( "illegal motors")
            return
        x.config.set("tolerance", 0.0005)
        z.config.set("tolerance", 0.0005)
