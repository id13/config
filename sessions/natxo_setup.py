
from bliss.setup_globals import *
import sys
import os

load_script("natxo.py")

print("")
print("Welcome to your new 'natxo' BLISS session !! ")
print("")
print("You can now customize your 'natxo' session by changing files:")
print("   * /natxo_setup.py ")
print("   * /natxo.yml ")
print("   * /scripts/natxo.py ")
print("")
