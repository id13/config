
from bliss.setup_globals import *
from bliss import current_session
import time
import pprint

#load_script("rh.py")
#load_script("rhmeta.py")
#load_script("preset_fsh.py")

#from id13.scripts.user_script import *
from id13.scripts.fastmon import *


print("")
print("Welcome to your new 'rh' BLISS session !! ")
print("")
print("You can now customize your 'rh' session by changing files:")
print("   * /rh_setup.py ")
print("   * /rh.yml ")
print("   * /scripts/rh.py ")
print("")

from bliss import setup_globals

print("+ set the default chain ...")
#default_chain = setup_globals.default_chain_rh
#DEFAULT_CHAIN.set_settings(chain_rh['chain_config'])
#DEFAULT_CHAIN.set_settings(chain_sim_cam1['chain_config'])


#====== metadata
SCAN_SAVING.scan_number_format = '%05d'
SCAN_SAVING.images_prefix = '{img_acq_device}_{scan_number}_'
SCAN_SAVING.date_format = '%Y-%m-%d'
#SCAN_SAVING.technique = 'KMAP'

current_session.enable_esrf_data_policy()
#current_session.disable_esrf_data_policy()


#ufsh_preset = Preset_ufsh()
#DEFAULT_CHAIN.add_preset(ufsh_preset)

