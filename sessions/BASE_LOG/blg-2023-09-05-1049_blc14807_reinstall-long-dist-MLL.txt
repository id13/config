# initial with nfshz moved out a bit:

wbetz     wbety       pf        pb       pu      pd    phg     pho    pvg
-------  --------  -------  --------  -------  ------  -----  ------  -----
 6.0000    0.8638   0.7934   -0.4934   0.2179  0.0821   0.30  0.6434   0.30
30.9846  -87.6925  -0.3150  -75.8870  -3.2478  1.5112   0.30  0.6434   0.30

   pvo    ccth[deg]       ccy     ccz    u18     u35      nbsx       nbsy     nbsz
------  -----------  --------  ------  -----  ------  --------  ---------  -------
0.0679      -6.1361   21.7000  4.5000  10.52  150.01  -52.6315  -100.0000   0.0252
0.0679       1.0929  -21.7000  4.5000  10.52  150.01  -58.8465   424.1410  18.2458

  nweiger      ndetx      ndety    ndetz      nwarm       narmx     narmrm    narmrd    narmz
---------  ---------  ---------  -------  ---------  ----------  ---------  --------  -------
 -20.5566  1400.0000    41.3500   3.6800  1512.4961   2849.6053   360.0000    0.0000  -9.7914
 573.6124  -110.0000  -131.0050   3.6800  2106.6651  -1525.7450  -404.7905  212.6110  -0.2441

    nwpco    npcox      npcoy    nfluoax    nfluoay    nfluoaz    nfluobx    nfluoby    nfluobz
---------  -------  ---------  ---------  ---------  ---------  ---------  ---------  ---------
-495.0109   0.0000   -25.3500      *DIS*      *DIS*      *DIS*    16.5290    70.0555   -10.7929
  99.1581   0.0000  -690.6850      *DIS*      *DIS*      *DIS*  -327.7630   -49.7570   -10.9427

  ningant    nougant    nmarble    niony    nionz    ndiodey    nmicroy    nmicroz    ns6hg
---------  ---------  ---------  -------  -------  ---------  ---------  ---------  -------
 -10.0000    54.1190     1.3993   0.5400  -6.2600     0.0000    -0.0074   215.0000   0.1820
 -10.0000    54.1190     1.3993  -9.7007  -3.7645   554.4000    -0.0577     0.5596  11.0202

  ns6ho    ns6vg    ns6vo    dummy    nt1j    nt1t     nt1x     nt2j     nt2t
-------  -------  -------  -------  ------  ------  -------  -------  -------
 2.1494   0.1069  -0.1106   0.0000   *DIS*  3.1059  -2.9883   9.3199   1.0889
19.9048   8.8633  -5.9453   0.0000   *DIS*  3.1059  -2.9883  -9.9680  -0.0886

    nt2x    nt3j    nt3t    nt3x       nt4j     nt4t      nt4x     nt5j     nt5t
--------  ------  ------  ------  ---------  -------  --------  -------  -------
-45.3679   *DIS*   *DIS*   *DIS*  -222.0857  -0.8267  -14.1977   4.5700  -2.5116
-45.3679   *DIS*   *DIS*   *DIS*    60.5927   2.7732  -14.1977  -4.5700  -2.5116

   nt5x    ntfocus    ntubey    ntubez    nfshy    nfshz    nnp1    nnp2    nnp3
-------  ---------  --------  --------  -------  -------  ------  ------  ------
-1.1002     0.0190     *DIS*     *DIS*  -3.5991  -3.1000     125     125     125
-1.1002    -0.7914     *DIS*     *DIS*   3.5991  -3.1000     125     125     125

   nnx    nny    nnz    nnu    nnv     nnw    Theta    smza    norx
------  -----  -----  -----  -----  ------  -------  ------  ------
11.000  0.252  3.462  0.099  0.319  -1.317    0.000    1097   *DIS*
11.000  0.252  3.462  0.099  0.319  -1.317    0.000    1097   *DIS*

  nory    norz    ntrollz    nfshrot
------  ------  ---------  ---------
 *DIS*   *DIS*      *DIS*   -12.0000
 *DIS*   *DIS*      *DIS*   -60.0000
EH3 [19]:
#C diode is about 4 mm to high according to yellow paper

EH3 [20]: ccmo.show()
ID13 channel-cut monochromator:
    calibration:
        calibrated offset:    0.556905
        calibrated constant:  -1.981357
    insertion devices:
        undulator gap1:       u18 = 10.522375
        undulator gap2:       u35 = 150.013000
    monochromator:
        angle:                 ccth = -6.136140
        energy[keV]:          17.000038
        lambda[A]:            0.729322

Out [23]: MOCO
          Name    : nmoco
          Comm.   : Serial[ser2net://lid133:28000/dev/ttyR32]

          MOCO 02.00  -  Current settings:
            NAME "no name"
            ADDR

            OPRANGE -10 10 0
            INBEAM CURR NORM UNIP 5e-09 NOAUTO
            GAIN INBEAM DEFAULT
            OUTBEAM CURR NORM UNIP 1e-05 NOAUTO
            GAIN OUTBEAM DEFAULT

            MODE INTENSITY
            PEAK 1 0.1 0
            SETPOINT 0.8
            TAU 1
            SET LEFT NORMALISE AUTORANGE
            CLEAR RIGHT AUTORUN BEAMCHECK INTERLOCK
            AUTOTUNE OFF
            AUTOPEAK OFF
            BEAMCHECK 0 0.166667 1.024 0
            SRANGE -10 10
            SPEED 2 50
            INHIBIT OFF LOW

