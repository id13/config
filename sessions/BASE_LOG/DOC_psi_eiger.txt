#
psi_eiger_500k._get_proxy('SlsDetectorEiger').setTestTrimBitsPattern(); psi_eiger_500k.camera.dac_threshold = [2300, 2300] 
#
# frame delay 16, 8 bit : 1   4 bit: 2  can be also a value in between
psi_eiger_500k.camera.tx_frame_delay = 1 / (psi_eiger_500k.camera.max_frame_rate * 1e3 / 3) / 6.2e-9 
#
# threshold enery in eV
psi_eiger_500k.camera.threshold_energy
#
# mux

EH3 [48]: nmux                                                                                                                       
Out [48]: Multiplexer Status:                                                                                                        
          Output name                     Output status                                                                              
          SHUTTER                         CLOSED                                                                                     
          DETECTOR                        P201                                                                                       
          O2_XIA_POL                      NORMAL                                                                                     
          O3_EIGER_POL                    NORMAL                                                                                     
          O5_MPX_POL                      INVERTED                                                                                   
          O6_P201CH9_POL                  NORMAL                                                                                     
          O4_FSHUT_POL                    NORMAL              

mcb Sun 12/11/2023 - No scans work only woth inverted polarity of O5_MPX_POL

numx.switch(key, value) changes the value
# guess psi_eiger: O5_MPX_POL = NORMAL but thsi is not the right setting

# burst mode:
# max no of frames in burst mode with rolling virtual buffer 1.0 x real buffer (might be up to 1.5)
# 4 bit : 30000
# 8 bit : 15000
# 16 bit : 7500 (detector has 12-bit physical dynamic range)
#
# sensor-wise min_period (100 Hz safety margin): 1/(psi_eiger_500k.camera.max_frame_rate * 1e3 - 100)
# slow_down factor: 1.0 
# a minimum latency of 4 usec must be respected: transfer time to pixel buffer
limatake(<expo_time>, int(30000 * 1.0 * 4 / int(psi_eiger_500k.camera.pixel_depth)), psi_eiger_500k, latency_time=(1/(psi_eiger_500k.camera.max_frame_rate * 1e3 - 100)*1.0 - <expo_time>), save=True)

# data-transfer (rougthly sensor-rate / 3), limited by 2x 10 Gbit links:
# 16-bit: 2 kHz
#  8-bit: 4 kHz
#  4-bit: 8 kHz
# 
# on lid13eigerlima1
supervisorctl tail -f LIMA:psi_eiger_500k 
# supervisorctl --serverurl http://lid13eigerlima1:9001 status
# import gevent.subprocess as subprocess
# subprocess.run(capture_output ... example:
EH3 [49]:                                                                                                                            
EH3 [49]: subprocess                                                                                                                 
!!! === NameError: name 'subprocess' is not defined === !!! ( for more details type cmd 'last_error(12)' )                           
EH3 [50]: import gevent.subprocess as subprocess                                                                                     
EH3 [51]: p = subprocess.run(['ls', '-l'], capture_output=True, text=True)                                                           
EH3 [52]: p.stdout                                                                                                                   
Out [52]: 'total 64\n-rw-r--r--   1 opid13 id13 48769 Nov  8 11:07 blc14958_sample.h5\ndrwxr-x--- 256 opid13 id13 16384 Nov  7 16:15 sample_0001\n'                                                                                                                       
                                                                                                                                     
EH3 [53]: p = subprocess.run('ls -l && echo "Hola"', shell=True, capture_output=True, text=True)                                     
EH3 [54]: p.stdout                                                                                                                   
Out [54]: 'total 64\n-rw-r--r--   1 opid13 id13 48769 Nov  8 11:07 blc14958_sample.h5\ndrwxr-x--- 256 opid13 id13 16384 Nov  7 16:15 sample_0001\nHola\n'                                                                                                                 
                                                                                                                                     
EH3 [55]: p.stdout.split('\n')                                                                                                       
Out [55]: ['total 64', '-rw-r--r--   1 opid13 id13 48769 Nov  8 11:07 blc14958_sample.h5', 'drwxr-x--- 256 opid13 id13 16384 Nov  7 16:15 sample_0001', 'Hola', '']                          

# pixel depth:
# e.g.
psi_eiger_500k.camera.pixel_depth = '4' 

#
# laency has to go to kmap command:
kmap.dkmap(nnp2,-2,2.,40,nnp3,-2,2,40,0.02, latency_time=4.0e-6)

#
# all tested with number of frames per file 1000 in the serttings !!!
