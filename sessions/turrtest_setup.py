
from bliss.setup_globals import *

load_script("turrtest.py")

print("")
print("Welcome to your new 'turrtest' BLISS session !! ")
print("")
print("You can now customize your 'turrtest' session by changing files:")
print("   * /turrtest_setup.py ")
print("   * /turrtest.yml ")
print("   * /scripts/turrtest.py ")
print("")