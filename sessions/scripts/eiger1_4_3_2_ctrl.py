print('eiger1 - load 3')
import os
from os import path
from traceback import format_exc
from textwrap import indent as doindent

from collections import OrderedDict as Odict

import gevent, bliss

class MyVirtual(Exception): pass

def eiger_restart():
    dco = DetectorControl()
    print('stopping eiger ...')
    print(dco.stop())
    print('waiting a bit ...')
    gevent.sleep(10)
    print('starting eiger ...')
    print(dco.start())
    print('waiting a bit ...')
    gevent.sleep(30)
    print('mg ...')
    dqmgeigx()

class DetectorControl(object):

    LEGAL_SATUS_VALUES = ('RUNNING', 'STOPPING', 'STARTING', 'STOPPED')

    def __init__(self, server_url='http://lid13eigerlima1:9001', device_s='LIMA:eiger1-4.3.2'):
        self.server_url = server_url
        self.device_s = device_s
        self._last_output = None

    def _parse_supervisorctl_output(self, output):
        wll = output.split(maxsplit=2)
        leng = len(wll)
        if leng == 2:
            device_s, status_s = wll
            date_s = ''
        elif leng == 3:
            device_s, status_s, date_s = wll
        else:
            raise ValueError(f'illegal output found: {str(wll)}')
        if device_s[-1] == ':':
            device_s = device_s[:-1]
        return device_s, status_s, date_s

    def _validate_received_data(self, data, longoutput=False):
        device_s, status_s, date_s = data
        device_ok = self.device_s == device_s
        cmd_ok = status_s in self.LEGAL_SATUS_VALUES
        data_ok = device_ok and cmd_ok
        if longoutput:
            return (data_ok, (device_ok, cmd_ok))
        else:
            return data_ok

    def _send_cmd(self, cmd):
        cmd = f'. /users/blissadm/conda/miniconda/bin/activate blissenv && supervisorctl --serverurl {self.server_url} {cmd} {self.device_s}'
        print(f'cmd = [{cmd}]')
        res=gevent.subprocess.run(cmd, shell=True, capture_output=True, text=True)
        output = res.stdout
        self._last_output = output
        device_s, status_s, date_s = data = self._parse_supervisorctl_output(output)
        return device_s, status_s, date_s

    def start(self):
        self._send_cmd('start')
        status_s = self.getstatus()
        return status_s

    def stop(self):
        self._send_cmd('stop')
        status_s = self.getstatus()
        return status_s

    def getstatus(self):
        device_s, status_s, date_s = data = self._send_cmd('status')
        if not self._validate_received_data(data):
            raise ValueError(f'illegal outputfrom remote cmd: {self._last_output}')
        return status_s


