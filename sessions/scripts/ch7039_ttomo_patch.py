print('version ttomo test ihls3299 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

# enter scanning stepsize here:
stepsize = 0.75
# define fov in asy.com

# insert rotations from generate_rotations.py
# index,kappa,omega,absorption
ORI_INSTRUCT = """

"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,tighti_y,tighti_z) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['tighti_y'],
            sp['tighti_z'] 
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        print('would start now')
        print(tighti_y, tighti_z)
        
        cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        print('move to patch1 (upper left)')
        umvr(nny,-0.0415,nnz,-0.0465)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch1  (upper left)')
                lly_new = lly + tighti_y
                llz_new = llz + tighti_z
                dy_new = uly - lly_new
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)

                kmap.dkmap(
                    nnp5, lly_new, uly ,nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
        
        print('move to patch 2 (upper right')
        umvr(nny,0.083)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch2')
                uly_new = uly-tighti_y
                llz_new = llz+tighti_z
                dy_new = uly_new - lly
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch2 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch3 (lower left)')
        umvr(nny,-0.083, nnz, 0.093)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch3  (lower left)')
                lly_new = lly + tighti_y
                ulz_new = ulz - tighti_z
                dy_new = uly - lly_new
                dz_new = ulz_new- llz
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly_new, uly, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch3 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch4 (lower right)')
        umvr(nny,0.083)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch4  (lower right)')
                uly_new = uly-tighti_y
                ulz_new = ulz-tighti_z
                dy_new = uly_new - lly
                dz_new = ulz_new - llz
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch4 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move back to cen')
        umvr(nny,-0.0415,nnz,0.0465)
        #time.sleep(5)
        

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    print(v)
                    print(w)
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    if 'tighti' == mode:
                        tighti_t = (tighti_y, tighti_z) = tuple(map(float, w[1:]))
                        print(f'process_instructions - {tighti_y}{tighti_z}')
                        print(tighti_t)
                        self.adapt_tighti_scanparams(tighti_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//stepsize)
        nitvz = int(dz//stepsize)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z
        
    def adapt_tighti_scanparams(self, tighti_t):
        print(tighti_t)
        (tighti_y,tighti_z) = tighti_t
        sp = self.scanparams
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z


def ls3299_ttomo_main(zkap, start_key, resume=-1):

    # verify zkap
    print(zkap)

    
    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -45,
        uly = 45,
        nitvy = 180,
        llz = -50,
        ulz = 50,
        nitvz = 200,
        expt = 0.002,
        tighti_y = 0,
        tighti_z = 0
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
