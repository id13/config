print('load 4')
def kmap_rot_scan(kmap_pars, rot_ll, rot_step, rot_nitv):
    start_rot_pos = usrotz.position
    for i in range(0,rot_nitv+1):
        curr_rot_pos = start_rot_pos + rot_ll + i*rot_step
        print('='*50)
        print(f'cycle = {i}: rot position = {curr_rot_pos}')
        mv(usrotz, curr_rot_pos)
        dkmapyz_2(*kmap_pars)
