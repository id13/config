from collections import OrderedDict as Odict

def fine_nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,80,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,80,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,40,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,40,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def fast_nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,10,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,10,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

# +141
# -176
# -59
# -10

#
# y =
def flintp(ax, ay, bx, by, x):
    dx = bx - ax
    dy = by - ay
    m = dx/dy
    y = m*(x - ax) + ay
    return y

# usage from scratch:
#     load_script('ls3359_infra')
#     znedge = ZnEdge(ref_energy=9.667)
#     
# goto a delta-energy relative to ref_energy:
# example: delta_energy = -0.042
# znedge.goto_deltaE(-0.042)


class ZnEdge(object):

    AX = -11.4889
    AY = 15.603
    BX = -11.0939
    BY = 15.993

    def __init__(self, ax=None, ay=None, bx=None, by=None, ref_energy=None):
        t = ax, ay, bx, by
        if ((None,)*4) == t:
            ax = self.AX
            ay = self.AY
            bx = self.BX
            by = self.BY
        else:
            if None in t:
                raise ValueError('gap calib params not specified ...')
        self.calib = ax, ay, bx, by
        self.ref_energy = ref_energy

    def lintp(self, x):
        return flintp(*(self.calib + (x,)))

    def goto_deltaE(self, de, adapt_gap=True):
        ccmo.goto_energy(self.ref_energy + de, force=True)
        ang = ccth.position

        if adapt_gap:
            gap_val = self.lintp(ang)
            mv(u35, gap_val)
