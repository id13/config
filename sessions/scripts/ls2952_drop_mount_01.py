print('ls2952_drop_mount_01 load 2')

STARTKEY = 'a'

def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_drop_mount_01():

    try:
        gop_newds('drop_mount_01_D1_scanstart_0049.json')
        dkpatchmesh(0.65, 325, 2.2, 220, 0.02, 2, 1)
        enddataset()

        gop_newds('drop_mount_01_D1_bis_scanstart_0051.json')
        dkpatchmesh(0.75, 375, 1.7, 170, 0.02, 1, 1)
        enddataset()
    
        gop_newds('drop_mount_01_D2_bis_scanstart_0056.json')
        dkpatchmesh(0.5, 500, 2.5, 250, 0.02, 3, 1)
        enddataset()


    finally:
        sc()
        sc()
