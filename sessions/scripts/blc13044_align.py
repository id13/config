print('blc13044 load 6')

def ybeam_knife_prescan():
    dscan(ustry,-0.15,0.15,50,0.05)
    gofe()

def ybeam_knife_scan():
    dscan(ustry,-0.04,0.04,160,0.1, sleep_time=0.05)
    gofe()

def zbeam_knife_prescan():
    dscan(ustrz,-0.15,0.15,50,0.05)
    gofe()

def zbeam_knife_scan():
    dscan(ustrz,-0.04,0.04,160,0.1, sleep_time=0.05)
    gofe()

def knife_series_y(ds_name,):#psgap):
    #mv(pvg,psgap,phg,psgap)
    newdataset('prealign_yb')
    x_start = -76.77#-78.77#-113.8
    delta_x = 0.2#1#0.5
    n_pts = 11#121
    try:
        mv(ustrx, x_start)
        ybeam_knife_prescan()
        #newdataset('may4_ybeam_series_ps50mu_002')
        newdataset(ds_name)
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(ustrx, x_pos)
            ybeam_knife_scan()
    finally:
        enddataset()

def knife_series_z(ds_name,):# psgap):
    #mv(pvg,psgap,phg,psgap)
    newdataset('prealign_zb')
    x_start = -76.77#-78.77#-113.8
    delta_x = 0.2#1#0.5
    n_pts = 11#121
    try:
        mv(ustrx, x_start)
        zbeam_knife_prescan()
        newdataset(ds_name)
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(ustrx, x_pos)
            zbeam_knife_scan()
    finally:
        enddataset()

def knife_serseries():
    pos = 'cross_center_align1'

    #psgap_tup = [(0.05, '50mu'), (0.1,'100mu'),(0.2,'200mu'),(0.3,'300mu')]
    ustrx_stepsize = '02mm'#'1mm'
    START_KEY = 'd'

    try:
        so()
        gopos(pos)
        mvr(ustry,-0.2)
        #for psgap, sgp in psgap_tup:
        knife_series_z(f'z_serseries_ustrx_step{ustrx_stepsize}_{START_KEY}')#, psgap)
        gopos(pos)
        mvr(ustrz,-0.2)
        #for psgap, sgp in psgap_tup:
        knife_series_y(f'y_serseries_ustrx_step{ustrx_stepsize}_{START_KEY}')#, psgap)

    finally:
        sc()
        sc()
