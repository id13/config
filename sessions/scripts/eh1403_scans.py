print('es1403_calib - load 7')
START_KEY = 'c'


ES1403_JSC_POS = '''
mount1_JSC_post_t1050_x50_pos1_0021.json
mount1_JSC_post_t1050_x50_pos2_0023.json
mount1_JSC_post_t1050_x50_pos3_0025.json
mount1_JSC_post_t1050_x50_pos4_0027.json
mount1_JSC_post_t1050_x50_pos5_0029.json
mount1_JSC_post_t1050_x5_pos1_0020.json
mount1_JSC_post_t1050_x5_pos2_0022.json
mount1_JSC_post_t1050_x5_pos3_0024.json
mount1_JSC_post_t1050_x5_pos4_0026.json
mount1_JSC_post_t1050_x5_pos5_0028.json
mount1_JSC_post_t1150_x50_pos3_0019.json
mount1_JSC_post_t1150_x50_pos4_0018.json
mount1_JSC_post_t1150_x50_pos5_0017.json
mount1_JSC_post_t1150_x5_pos1_0013.json
mount1_JSC_post_t1150_x5_pos3_0014.json
mount1_JSC_post_t1150_x5_pos4_0015.json
mount1_JSC_post_t1150_x5_pos5_0016.json
mount1_JSC_post_tRT_x50_pos1_0031.json
mount1_JSC_post_tRT_x50_pos2_0033.json
mount0_JSC_post_tRT_x50_pos3_0035.json
mount1_JSC_post_tRT_x50_pos4_0037.json
mount1_JSC_post_tRT_x50_pos5_0039.json
mount1_JSC_post_tRT_x5_pos1_0030.json
mount1_JSC_post_tRT_x5_pos2_0032.json
mount1_JSC_post_tRT_x5_pos3_0034.json
mount1_JSC_post_tRT_x5_pos4_0036.json
mount1_JSC_post_tRT_x5_pos5_0038.json
mount1_JSC_t1150_cen_0007.json
mount1_JSC_t1150_pos1_0006.json
mount1_JSC_t1150_pos2_0008.json
mount1_JSC_t1150_x50_pos1_g1_0010.json
mount1_JSC_t1150_x50_pos1_g2_0011.json
mount1_JSC_t1150_x50_pos1_g3_0012.json
mount1_JSC_t1150_x50_pos2_0009.json
'''

ES1403_JSC_POS_T1050 = '''
# grain
mount1_JSC_post_t1050_x50_pos4_0027.json
'''

ES1403_JSC_POS_OVERVIEW = '''
mount1_JSC_post_t1050_x50_pos1_0021.json
mount1_JSC_post_t1050_x50_pos2_0023.json
mount1_JSC_post_t1050_x50_pos3_0025.json
mount1_JSC_post_t1050_x50_pos5_0029.json
mount1_JSC_post_tRT_x50_pos3_0035.json
mount1_JSC_post_tRT_x50_pos4_0037.json
mount1_JSC_post_tRT_x50_pos5_0039.json
'''

ES1403_JSC_nightlist  = '''
t1150_pos2_nny_m0p2
mount1_JSC_post_t1050_x50_pos2_0023.json
mount1_JSC_post_t1050_x50_pos3_0025.json
mount1_JSC_post_t1050_x50_pos5_0029.json
mount1_JSC_post_tRT_x50_pos3_0035.json
mount1_JSC_post_tRT_x50_pos4_0037.json
mount1_JSC_post_tRT_x50_pos5_0039.json
t1150_pos2_nny_m0p4
mount1_JSC_post_t1050_x50_pos1_0021.json
'''

def es1403_overview_scan():
    loff_kmap(nnp2, -100,100, 500, nnp3, -100,100, 500, 0.02)
    #print("loff_kmap(nnp2, -100,100, 200, nnp3, -100,100, 200, 0.01)")

def es1403_setupos(posname, intent):
    if posname.endswith('.json'):
        _nm = posname[:-5]
        dsname = f'{_nm[:-5]}_{intent}'
    elif posname.endswith('._pos'):
        dsname = f'{_nm[:-5]}_{intent}'
    else:   
        dsname = posname
    
    print ('='*40,dsname)
    rstp(posname)
    newdataset(f'{dsname}_{START_KEY}')

def strtolist(s):
    ll = s.split('\n')
    ll = [l.strip() for l in ll]
    ll = [l for l in ll if l]
    return ll



def es1403_scans_JSC_OVERVIEW_night():

    plist = strtolist(ES1403_JSC_nightlist)
    for p in plist:
        es1403_setupos(p, 'overview')
        es1403_overview_scan()

def es1403_scans_JSC_OVERVIEW():

    plist = strtolist(ES1403_JSC_POS_OVERVIEW)
    for p in plist:
        es1403_setupos(p, 'overview')
        es1403_overview_scan()

def es1403_scans_main():
    try:
        so()
        es1403_scans_JSC_OVERVIEW_night()
    finally:
        sc()
        sc()
        sc()
