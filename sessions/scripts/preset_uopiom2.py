from bliss.scanning.chain import ChainPreset
from time import sleep

class Preset_uopiom2(ChainPreset):
  _shutter_delay = 0.0001
    
  def prepare(self,acq_chain):
    #print("... Preset_uopiom2 - prepare")
    pass

  def set_delay(self, delay):
    self._shutter_delay = delay

  def start(self,acq_chain):
    print("... Preset_uopiom2 - start")
    _exp = acq_chain.timer.count_time
    print(f"... exp_time[{_exp}]")
    #shutter_delay = 0.01
    #print(f"... shutter_delay[{shutter_delay}] - deactivated in preset_uopiom2")
    print(f"... shutter_delay[{self._shutter_delay}] ")
    opiomdelay(1,self._shutter_delay,5,_exp-self._shutter_delay,_exp) 
    opiomdelay(2,self._shutter_delay,5,_exp-self._shutter_delay,_exp) 
    sleep(.1)
    pass

  def stop(self,acq_chain):
    #print("... Preset_uopiom2 - stop")
    pass

def opiomdelay(ch, delay, inchannel, width, exptime):
  freq = 8
  tick = 1.0/(freq * 1.0e6)
  
  ch = int(ch)
  if ((ch < 1) or (ch > 4) ):
    print(f"Error in channel[{ch}] must be 1 ... 4")
    exit
        
  chin = int(inchannel)
  if ((chin < 5) or (chin > 8) ):
    print(f"Error in channelIn[{chin}] must be 5 ... 8")
    exit
        
  if( (delay + width) > exptime):
    print(f"Error delay[{delay}] + width[{width}] > exptime[{exptime}]")
    exit
    

  if( (delay < 0) or  (width <= 0) or (exptime <= 0) ):
    print(f"Error delay[{delay}] (<0)  width[{width}] (<=0)exptime[{exptime}](<=0)")
    exit


  ticks_delay = int(delay / tick)
  if ticks_delay == 0:
    ticks_delay = 1
    
  ticks_width = int(width / tick)
  if ticks_width == 0:
    ticks_width = 1
  
  #cmd = "CNT %d CLK%d RISE D32 START RISE I8 PULSE %d %d 1" % (ch, freq, ticks_width, ticks_delay) 
  cmd = "CNT %d CLK%d RISE D32 START RISE I%d PULSE %d %d 1" % (ch, freq, inchannel, ticks_width, ticks_delay) 
  uopiom2.comm(cmd)

  print(cmd)


