load_script('edfsupport')
from scipy import ndimage as ndi
def test_001():
    collection_name = cn = ''
    dsname = dsn = ''

    ldp = get_local_data_dapth(cn, dsn)
    fupth = get_local_data_fupth(cn, dsn)
    fapth = get_local_data_fapth(cn, dsn)

    print(f'local_data_dapth = {ldp}')
    print(f'local_data_fupth = {fupth}')
    print(f'local_data_fapth = {fapth}')

    ldp = get_current_data_dapth()
    fupth = get_current_data_fupth()
    fapth = get_current_data_fapth()

    print(f'current_data_dapth = {ldp}')
    print(f'current_data_fupth = {fupth}')
    print(f'current_data_fapth = {fapth}')

def test_002(scanno, sctrll, subno=1):
    scn_name = f'{scanno:1d}.{subno:1d}'
    fh5 = H5FileReadonly(get_current_data_fapth())
    scn = fh5.fh5['2.1']
    res_dc = mesh_read_counter_data(scn, sctrll)
    return res_dc

def parse_pillar_map_v1(map_arr, mlimits, initial_motpos, threshold, sum_thresh,
    reflength, outfxpath_tpl):
    keys = "raw med thresh anno".split()
    ecks = EDFClosedKeySet_RW(outfxpath_tpl, keys)
    s0, s1 = map_arr.shape
    md_arr = ndi.median_filter(map_arr, 7)
    th_arr = np.where(md_arr > threshold, 1, 0)
    anno_arr = th_arr.copy()
    ecks['raw'] = map_arr
    ecks['med'] = md_arr
    ecks['thresh'] = th_arr
    hpix_arr = np.arange(s1)


    top_hit = -1
    idx_ll = []
    th_thesum_ll = []
    raw_themean_ll = []
    raw_thevar_ll = []
    found_flg = False
    for i in range(s0):
        idx_ll.append(i)
        th_thesum = th_arr[i].sum()
        raw_themean = map_arr[i].mean()
        raw_thevar = map_arr[i].var()
        th_thesum_ll.append(th_thesum)
        raw_themean_ll.append(raw_themean)
        raw_thevar_ll.append(raw_thevar)
        if not found_flg:
            if th_thesum > sum_thresh:
                top_hit = i
                found_flg = True
                hor_cog = compute_cog_1d(hpix_arr, th_arr)
    th_thesum_arr = np.array(th_thesum_ll, dtype=np.int32)
    th_themean_arr = np.array(th_thesum_ll, dtype=np.float64)
    th_thevar_arr = np.array(th_thesum_ll, dtype=np.float64)
    #pprint(list(zip(thesum_ll, themean_ll, thevar_ll)))
    search_arr = th_thesum_arr[0: top_hit+reflength]
    vert_max = search_arr.max()
    vert_argmax = search_arr.argmax()
    refline=th_arr[vert_argmax]
    print('top_hit =', top_hit, '  hor_cog=', hor_cog)
    print('vert_argmax =', vert_argmax, '  vert_max=', vert_max)
    samp_indices = np.compress(refline, hpix_arr)
    print(samp_indices)
    low_hor_idx = samp_indices[0]
    hi_hor_idx = samp_indices[-1]
    print('low_hor_idx =', low_hor_idx, '  hi_hor_idx=', hi_hor_idx)
    anno_arr[top_hit,int(hor_cog)]  += 2
    anno_arr[vert_argmax,low_hor_idx]  += 2
    anno_arr[vert_argmax,hi_hor_idx]  += 2
    ecks['anno'] = anno_arr

    return top_hit, hor_cog, vert_argmax, vert_max, low_hor_idx, hi_hor_idx

def pillar_test(scanno, tg, el, subno=1, ith=5, wth=5, h=10):
    scn_name = f'{scanno:1d}.{subno:1d}'
    with H5FileReadonly(get_current_data_fapth()) as h5fro:
        scn = h5fro[scn_name]
    k=f'fx1_det0_{el}'
    sctrll = [k]
    res_dc = mesh_read_counter_data(scn, sctrll)
    tpl = '%s_{}.edf' % el
    map_arr = res_dc[0][k]
    dc = res_dc[1]
    return parse_pillar_map_v1(map_arr, dc['limits'], dc['initial_motpos'], ith, wth, h, tpl)



XXX = '''
s3ctrl3_tt_tomo_d_022_022_0_00039p60_diff
s3ctrl3_tt_tomo_d_023_023_0_00041p40_diff
s3ctrl3_tt_tomo_d_024_024_0_00043p20_diff
s3ctrl3_tt_tomo_d_025_025_0_00045p00_diff
s3ctrl3_tt_tomo_d_026_026_0_00046p80_diff
s3ctrl3_tt_tomo_d_027_027_0_00048p60_diff
s3ctrl3_tt_tomo_d_028_028_0_00050p40_diff
s3ctrl3_tt_tomo_d_029_029_0_00052p20_diff
s3ctrl3_tt_tomo_d_030_030_0_00054p00_diff
s3ctrl3_tt_tomo_d_031_031_0_00055p80_diff
s3ctrl3_tt_tomo_d_032_032_0_00057p60_diff
s3ctrl3_tt_tomo_d_033_033_0_00059p40_diff
s3ctrl3_tt_tomo_d_034_034_0_00061p20_diff
s3ctrl3_tt_tomo_d_035_035_0_00063p00_diff
s3ctrl3_tt_tomo_d_036_036_0_00064p80_diff
s3ctrl3_tt_tomo_d_037_037_0_00066p60_diff
s3ctrl3_tt_tomo_d_038_038_0_00068p40_diff
s3ctrl3_tt_tomo_d_039_039_0_00070p20_diff
s3ctrl3_tt_tomo_d_040_040_0_00072p00_diff
s3ctrl3_tt_tomo_d_041_041_0_00073p80_diff
s3ctrl3_tt_tomo_d_042_042_0_00075p60_diff
s3ctrl3_tt_tomo_d_043_043_0_00077p40_diff
s3ctrl3_tt_tomo_d_044_044_0_00079p20_diff
s3ctrl3_tt_tomo_d_045_045_0_00081p00_diff
s3ctrl3_tt_tomo_d_046_046_0_00082p80_diff
s3ctrl3_tt_tomo_d_047_047_0_00084p60_diff
s3ctrl3_tt_tomo_d_048_048_0_00086p40_diff
s3ctrl3_tt_tomo_d_049_049_0_00088p20_diff
s3ctrl3_tt_tomo_d_050_050_0_00090p00_diff
s3ctrl3_tt_tomo_d_051_051_0_00091p80_diff
s3ctrl3_tt_tomo_d_052_052_0_00093p60_diff
s3ctrl3_tt_tomo_d_053_053_0_00095p40_diff
s3ctrl3_tt_tomo_d_054_054_0_00097p20_diff
s3ctrl3_tt_tomo_d_055_055_0_00099p00_diff
s3ctrl3_tt_tomo_d_056_056_0_00100p80_diff
s3ctrl3_tt_tomo_d_057_057_0_00102p60_diff
s3ctrl3_tt_tomo_d_058_058_0_00104p40_diff
s3ctrl3_tt_tomo_d_059_059_0_00106p20_diff
s3ctrl3_tt_tomo_d_060_060_0_00108p00_diff
s3ctrl3_tt_tomo_d_061_061_0_00109p80_diff
s3ctrl3_tt_tomo_d_062_062_0_00111p60_diff
s3ctrl3_tt_tomo_d_063_063_0_00113p40_diff
s3ctrl3_tt_tomo_d_064_064_0_00115p20_diff
s3ctrl3_tt_tomo_d_065_065_0_00117p00_diff
s3ctrl3_tt_tomo_d_066_066_0_00118p80_diff
s3ctrl3_tt_tomo_d_067_067_0_00120p60_diff
s3ctrl3_tt_tomo_d_068_068_0_00122p40_diff
s3ctrl3_tt_tomo_d_069_069_0_00124p20_diff
s3ctrl3_tt_tomo_d_070_070_0_00126p00_diff
s3ctrl3_tt_tomo_d_071_071_0_00127p80_diff
s3ctrl3_tt_tomo_d_072_072_0_00129p60_diff
s3ctrl3_tt_tomo_d_073_073_0_00131p40_diff
s3ctrl3_tt_tomo_d_074_074_0_00133p20_diff
s3ctrl3_tt_tomo_d_075_075_0_00135p00_diff
s3ctrl3_tt_tomo_d_076_076_0_00136p80_diff
s3ctrl3_tt_tomo_d_077_077_0_00138p60_diff
s3ctrl3_tt_tomo_d_078_078_0_00140p40_diff
'''

def make_xxx():
    ll = XXX.split('\n')
    ll = [x for x in ll if x.startswith('s3')]
    return ll

def test_003():
    #data_fapth = '/data/visitor/ls3299/id13/20240215/RAW_DATA/s3ctrl3/s3ctrl3_tt_ttomo_test_a_000_000_0_00000p00_diff/s3ctrl3_tt_ttomo_test_a_000_000_0_00000p00_diff.h5'
    data_fapth_tpl = '/data/visitor/ls3299/id13/20240215/RAW_DATA/s3ctrl3/%s/%s.h5'
    data_fapth_ll = [data_fapth_tpl % (x,x) for x in make_xxx()]
    

    for i, data_fapth in enumerate(data_fapth_ll):
        print('data_fapth:', data_fapth)
        mif = MiniOdakInfra(['fx1_det0_Ca'], data_fapth=data_fapth)
        res = mif.read_scaninfo(1) 
        print (res)
        p2 = Pillar2(250,5,30,3, 'myca_%04d_' % i)
        ndi_objects = p2.seg_parse(1, res, 'fx1_det0_Ca')

    
