# wiggle centering
def wigrot_init(th_mot, delta_th):
    mvr(th_mot, -0.5*delta_th)

def wigrot(th_mot, delta_th):
    mvr(th_mot, delta_th)
    mvr(th_mot, -delta_th)

def wigcomp(comp_mot_cen, comp_mot_ext, delta_comp):
    mvr(comp_mot_cen, delta_comp)
    mvr(comp_mot_ext, -delta_comp)

def wcmpb(comp_mot_cen, comp_mot_ext, delta_comp):
    mvr(comp_mot_cen, delta_comp)
    mvr(comp_mot_ext, delta_comp)

def dir_nfluox_pos():
    mvr(nfluobx, -.1)
    mvr(nfluobx, 0.06)

def dir_nfluox_neg():
    mvr(nfluobx, .1)
    mvr(nfluobx, -0.06)
