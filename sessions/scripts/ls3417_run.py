print('ls3417 v3')


start_key = 'c'


WAXS_list = [
'EC_pos4',
'EC_pos3',
]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t,fast_Z=False):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    if fast_Z:
        kmap.dkmap(nnp3,-1*zrange,zrange,zstep,nnp2,-1*yrange,yrange,ystep,exp_t)
    else:
        kmap.dkmap(nnp2,-1*yrange,yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    rmgeigx()
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    #print(name)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()
    #sleep(2)
    
def gopos_displacement_kmap_scan(pos,nb_d,d,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,d_axis='Y',deltaZ=0):
    so()
    fshtrigger()
    rmgeigx()
    gopos(pos)
    #umvr(nnz,deltaZ*0.001)
    ##umvr(nny,0.2)#this is special case for continue wrong scan
    for i in range(nb_d):
        name = f'{pos}_{start_key}_{i}'
        #print(name)
        newdataset(name)
        kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
        enddataset()
        if d_axis == 'Y':
            umvr(nny,d*0.001)
        else:
            umvr(nnz,d*0.001)
        sleep(15)
        #sleep(2)


def gopos_diagonal_kmap_scan(pos,nb_d,dy,dz,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,):
    so()
    fshtrigger()
    rmgeigx()
    gopos(pos)
    #umvr(nnz,deltaZ*0.001)
    ##umvr(nny,0.2)#this is special case for continue wrong scan
    for i in range(nb_d):
        name = f'{pos}_{start_key}_{i}'
        #print(name)
        newdataset(name)
        kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
        enddataset()
        umvr(nny,dy*0.001)
        umvr(nnz,dz*0.001)
        sleep(15)
        #  sleep(2)


def run_Oct_29th_lunch():
    try:
        #umv(ndetx,-300)
        #for i in WAXS_list:            
        gopos_kmap_scan('E3_pos2',start_key,0,50,50,1,1,0.02)
        sleep(15)
        gopos_kmap_scan('E3_pos3',start_key,0,50,50,1,1,0.05)
        sleep(15)
        gopos_kmap_scan('E3_pos4',start_key,0,50,50,1,1,0.1)
        sleep(15)

        sc()
    except:
        sc()




def run_Oct_29th_night():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,4,200,start_key,0,100,100,0.5,0.5,0.05,d_axis='Y')
            sleep(15)

        
        gopos_kmap_scan('HC_bkgd',start_key,0,25,25,0.5,0.5,0.05)
        sleep(15)
        
        sc()
    except: 
        sc()


def run_Oct_30th_morning():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_diagonal_kmap_scan(i,6,-200,200,start_key,0,100,100,0.5,1,0.02,)
            sleep(15)

        gopos_displacement_kmap_scan('HC_pos8',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        gopos_displacement_kmap_scan('HC_pos9',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        
        sc()
    except: 
        sc()



def run_Oct_30th_night():
    try:
        gopos_diagonal_kmap_scan('2191_Vi_pos1',6,-200,-200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('2191_Vi_pos2',6,-200,-200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)

        gopos_displacement_kmap_scan('2191_CI_pos1',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        gopos_displacement_kmap_scan('2191_CI_pos2',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        
        sc()
    except: 
        sc()
        
def run_Oct_31th_day():
    try:
        gopos_diagonal_kmap_scan('EC_pos1',3,40,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('EC_pos3',3,80,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)

        gopos_displacement_kmap_scan('HC_pos1',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        gopos_displacement_kmap_scan('HC_pos3',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        
        sc()
    except: 
        sc()
        
def run_Oct_31th_night():
    try:
        #gopos('EC_pos1')
        gopos_diagonal_kmap_scan('EC_pos5',3,40,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        #gopos('EC_pos3')
        gopos_diagonal_kmap_scan('EC_pos6',3,80,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        
        gopos_diagonal_kmap_scan('EC_pos4',6,40,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('EC_pos8',6,80,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        
        gopos_kmap_scan('EC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        #gopos('HC_pos1')
        gopos_displacement_kmap_scan('HC_pos7',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        #gopos('HC_pos3')
        gopos_displacement_kmap_scan('HC_pos4',3,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        
        gopos_displacement_kmap_scan('HC_pos6',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        gopos_displacement_kmap_scan('HC_pos5',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(15)
        
        gopos_kmap_scan('HC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        sc()
    except: 
        sc()
        
def run_Nov_1st_night():
    try:
        
        gopos_diagonal_kmap_scan('V1_pos1',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('V1_pos2',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('V1_pos3',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        
        gopos_kmap_scan('V1_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        gopos_diagonal_kmap_scan('Ci_pos1',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('Ci_pos2',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('Ci_pos3',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        
        gopos_kmap_scan('Ci_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        sc()
    except: 
        sc()
        
        
def run_Nov_2nd_day():
    try:
        
        gopos_displacement_kmap_scan('HC_pos1',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(5)
        gopos_displacement_kmap_scan('HC_pos2',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(5)
        
        gopos_kmap_scan('HC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        gopos_diagonal_kmap_scan('EC_pos1',6,200,-90,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        
        gopos_diagonal_kmap_scan('EC_pos2',6,-100,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        
        gopos_kmap_scan('EC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        sc()
    except: 
        sc()
        
        
        
def run_Nov_2nd_night1():
    try:
        
        #gopos_displacement_kmap_scan('HC_pos1',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        #sleep(5)
        #gopos_displacement_kmap_scan('HC_pos2',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        #sleep(5)
        
        #gopos_kmap_scan('HC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        #gopos_diagonal_kmap_scan('EC_pos1',6,200,-90,start_key,0,100,100,0.5,1,0.02,)
        gopos('EC_pos1')
        umvr(nny,0.2*5,nnz,-0.09*5)
        newdataset('EC_pos1_c_5_new')
        kmap_scan(100,100,0.5,1,0.02)
        enddataset()
        sleep(5)
        
        gopos_diagonal_kmap_scan('EC_pos2',6,-100,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        
        gopos_kmap_scan('EC_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        sc()
    except: 
        sc()
        

def run_Nov_2nd_night2():
    try:
        
        gopos_displacement_kmap_scan('HC_pos3',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Y')
        sleep(5)
        gopos_diagonal_kmap_scan('HC_pos4',6,200,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        
        gopos_diagonal_kmap_scan('EC_pos3',6,200,-90,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        gopos_diagonal_kmap_scan('EC_pos4',6,-100,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(5)
        
        sc()
    except: 
        sc()
        
        
def run_Nov_3rd():
    try:
        
        gopos_displacement_kmap_scan('Ci_pos1',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Z')
        sleep(15)
        gopos_displacement_kmap_scan('Ci_pos2',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Z')
        sleep(15)
        gopos_displacement_kmap_scan('Ci_pos3',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Z')
        sleep(15)
        
        gopos_kmap_scan('Ci_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        gopos_diagonal_kmap_scan('V1_pos1',6,200,100,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_diagonal_kmap_scan('V1_pos2',6,100,200,start_key,0,100,100,0.5,1,0.02,)
        sleep(15)
        gopos_displacement_kmap_scan('V1_pos3',6,200,start_key,0,100,100,0.5,1,0.02,d_axis='Z')
        sleep(15)
        
        gopos_kmap_scan('V1_bkgd',start_key,0,25,25,0.5,1,0.02)
        
        sc()
    except: 
        sc()



































