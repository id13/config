print('ls3086 load 1')


start_key = 'a'


WAXS_list = [
'PHG_1971_pos1',
'PHG_1971_pos2',
'PHG_1971_pos3',
'PHG_1971_pos4',
'PHG_1971_pos5',
'PHG_1971_bkgd',
'V1_1971_pos1',
'V1_1971_pos2',
'V1_1971_pos3',
'V1_1971_pos4',
'V1_1971_pos5',
]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig_x()
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def run_Nov_10th_night():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,50,50,0.25,0.25,0.02)
            sleep(10)

        sc()
    except:
        sc()
        sc()






