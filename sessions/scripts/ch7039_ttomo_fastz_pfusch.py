print('version ttomo ch7039')
load_script('test_gen1_a')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

# enter scanning stepsize here:
stepsize = 0.25
# define fov in asy.com

exp_time = 0.002

# insert rotations from generate_rotations.py
# index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 5.143, 0
2, 0, 10.286, 0
3, 0, 15.429, 0
4, 0, 20.571, 0
5, 0, 25.714, 0
6, 0, 30.857, 0
7, 0, 36.000, 0
8, 0, 41.143, 0
9, 0, 46.286, 0
10, 0, 51.429, 0
11, 0, 56.571, 0
12, 0, 61.714, 0
13, 0, 66.857, 0
14, 0, 72.000, 0
15, 0, 77.143, 0
16, 0, 82.286, 0
17, 0, 87.429, 0
18, 0, 92.571, 0
19, 0, 97.714, 0
20, 0, 102.857, 0
21, 0, 108.000, 0
22, 0, 113.143, 0
23, 0, 118.286, 0
24, 0, 123.429, 0
25, 0, 128.571, 0
26, 0, 133.714, 0
27, 0, 138.857, 0
28, 0, 144.000, 0
29, 0, 149.143, 0
30, 0, 154.286, 0
31, 0, 159.429, 0
32, 0, 164.571, 0
33, 0, 169.714, 0
34, 0, 174.857, 0
35, 25, 12.857, 0
36, 25, 37.653, 0
37, 25, 62.449, 0
38, 25, 87.245, 0
39, 25, 112.041, 0
40, 25, 136.837, 0
41, 25, 161.633, 0
42, 25, 186.429, 0
43, 25, 211.224, 0
44, 25, 236.020, 0
45, 25, 260.816, 0
46, 25, 285.612, 0
47, 25, 310.408, 0
48, 25, 335.204, 0
49, 45, 0.000, 0
50, 45, 32.727, 0
51, 45, 65.455, 0
52, 45, 98.182, 0
53, 45, 130.909, 0
54, 45, 163.636, 0
55, 45, 196.364, 0
56, 45, 229.091, 0
57, 45, 261.818, 0
58, 45, 294.545, 0
59, 45, 327.273, 0
60, 35, 15.000, 0
61, 35, 43.750, 0
62, 35, 72.500, 0
63, 35, 101.250, 0
64, 35, 130.000, 0
65, 35, 158.750, 0
66, 35, 187.500, 0
67, 35, 216.250, 0
68, 35, 245.000, 0
69, 35, 273.750, 0
70, 35, 302.500, 0
71, 35, 331.250, 0
72, 15, 0.000, 0
73, 15, 25.714, 0
74, 15, 51.429, 0
75, 15, 77.143, 0
76, 15, 102.857, 0
77, 15, 128.571, 0
78, 15, 154.286, 0
79, 15, 180.000, 0
80, 15, 205.714, 0
81, 15, 231.429, 0
82, 15, 257.143, 0
83, 15, 282.857, 0
84, 15, 308.571, 0
85, 15, 334.286, 0
86, 5, 12.000, 0
87, 5, 35.200, 0
88, 5, 58.400, 0
89, 5, 81.600, 0
90, 5, 104.800, 0
91, 5, 128.000, 0
92, 5, 151.200, 0
93, 5, 174.400, 0
94, 5, 197.600, 0
95, 5, 220.800, 0
96, 5, 244.000, 0
97, 5, 267.200, 0
98, 5, 290.400, 0
99, 5, 313.600, 0
100, 5, 336.800, 0
101, 20, 0.000, 0
102, 20, 25.714, 0
103, 20, 51.429, 0
104, 20, 77.143, 0
105, 20, 102.857, 0
106, 20, 128.571, 0
107, 20, 154.286, 0
108, 20, 180.000, 0
109, 20, 205.714, 0
110, 20, 231.429, 0
111, 20, 257.143, 0
112, 20, 282.857, 0
113, 20, 308.571, 0
114, 20, 334.286, 0
115, 30, 13.846, 0
116, 30, 40.473, 0
117, 30, 67.101, 0
118, 30, 93.728, 0
119, 30, 120.355, 0
120, 30, 146.982, 0
121, 30, 173.609, 0
122, 30, 200.237, 0
123, 30, 226.864, 0
124, 30, 253.491, 0
125, 30, 280.118, 0
126, 30, 306.746, 0
127, 30, 333.373, 0
128, 40, 0.000, 0
129, 40, 30.000, 0
130, 40, 60.000, 0
131, 40, 90.000, 0
132, 40, 120.000, 0
133, 40, 150.000, 0
134, 40, 180.000, 0
135, 40, 210.000, 0
136, 40, 240.000, 0
137, 40, 270.000, 0
138, 40, 300.000, 0
139, 40, 330.000, 0
140, 10, 12.000, 0
141, 10, 35.200, 0
142, 10, 58.400, 0
143, 10, 81.600, 0
144, 10, 104.800, 0
145, 10, 128.000, 0
146, 10, 151.200, 0
147, 10, 174.400, 0
148, 10, 197.600, 0
149, 10, 220.800, 0
150, 10, 244.000, 0
151, 10, 267.200, 0
152, 10, 290.400, 0
153, 10, 313.600, 0
154, 10, 336.800, 0
"""

bailouttime = 400


def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')
        self.init_pfusch()


    def init_pfusch(self):
        self.pfusch_idx = -1
        self.pfusch_out_fupth_tpltpl = 'x_%03d_{}.edf'
        self.pfusch_ctr_name = 'eiger_roi1_avg'
        ini = TT_coral_INI()
    
        self.pfusch_dobj = dobj = TT(ini)
        self.pfusch_out_dapth = out_dapth = dobj.make_outdir('out5')
        try:
            os.mkdir(out_dapth)
        except OSError:
            pass
    
        eval_fapth = path.join(out_dapth, 'eval.json')
        self.pfusch_evdb = EvalDb(eval_fapth)
        

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.pfusch_dsname = f'{self.pfusch_dobj.samp_name}_{dsname}'
            self.perform_scan()
            enddataset()
            sleep(1)
            self.pfusch()
            self.auxlog(' done')

    def pfusch(self):
        print("PFUSCH +++++++++++++++++++")
        pfusch_idx = self.pfusch_idx + 1
        self.pfusch_idx = pfusch_idx
        evdb = self.pfusch_evdb
        out_dapth = self.pfusch_out_dapth
        out_fupth_tpl = self.pfusch_out_fupth_tpltpl % pfusch_idx
        out_fapth_tpl = path.join(out_dapth, out_fupth_tpl)
        bd = self.pfusch_dobj.parse(self.pfusch_dsname)
        bd.ctr_name = ctr_name = self.pfusch_ctr_name
        for i in range(5):
            try:
                minisi = self.pfusch_dobj.spec4_to_minisi(bd.spec4)
                kmi = minisi.get_kmapinfo(1, [ctr_name])
                break
            except:
                sleep(2)
        print(f'{i+1} attempts ...')
        ctr_map_arr = kmi.data[ctr_name]
        print(f'out_fapth_tpl: {out_fapth_tpl}')
        objparser = ObjParser1(ctr_map_arr, out_fapth_tpl)
        check_flg, ibounds2d = objparser.parse()
        print(ctr_map_arr.shape, check_flg, ibounds2d)
        corrinfo = kmi.get_corr_info(ibounds2d)
        print_box(corrinfo)
        evdb.add_record(pfusch_idx, (bd, corrinfo))
        self.zkap.c_corry += corrinfo.dev_2*0.001
        self.zkap.c_corrz += corrinfo.dev_1*0.001
        zkap.show()
        pos_dc = zkap.get_pos_dict()
        read_kap = pos_dc['kap']
        if int(read_kap) != 0:
            wy = min(98, corrinfo.i_width2 + 20.0)
            wz = min(90, corrinfo.i_width1 + 20.0)
        else:
            wy = min(98, corrinfo.i_width2 + 10.0)
            wz = min(90, corrinfo.i_width1 + 10.0)
        lly = -0.5*wy
        uly =  0.5*wy
        llz = -0.5*wz
        ulz =  0.5*wz
        fov_t = (lly, uly, llz, ulz)
        self.adapt_fov_scanparams(fov_t)
        print("PFUSCH +++++++++++++++++++<<")



        

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        cmd = f'kmap.dkmap(nnp6, {llz},{ulz},{nitvz},{expt},nnp5, {lly},{uly},{nitvy} )'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
       #  print('move to patch1')
        with gevent.Timeout(seconds=bailouttime):
            try:
                # put the nano scan ...
                # print('patch1')
                kmap.dkmap(nnp6,llz,ulz,nitvz,nnp5,lly,uly,nitvy,expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                # self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)

        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = stepsize*(lly//stepsize)
        uly = stepsize*(uly//stepsize)
        llz = stepsize*(llz//stepsize)
        ulz = stepsize*(ulz//stepsize)

        dy = uly - lly
        dz = ulz - llz
        
        nitvy = int(dy//stepsize) 
        nitvz = int(dz//stepsize)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz


def ch7039_ttomo_main(zkap, start_key, resume=-1):

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params # don't touch these, define directly in asy.com
    scanparams = dict(
        lly = -49,
        uly = 49,
        nitvy = 200,
        llz = -45,
        ulz = 45,
        nitvz = 180,
        expt = exp_time
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)

    # loop over projections
    ttm.mainloop()


'''
stuff to do:
    clean up input, put stepsize in the object
    make fast axis flexible
    stitching?
    check logging/output
    commenting..
'''
