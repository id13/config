import sys
import h5py
from os import path

def karl_tilman_init():
    sys.path.insert(0, '/data/id13/inhouse3/Manfred/SW/kwolek/SRC/UVZ3/REANIM/mcbeco1/episto/lib')

def get_local_data_dapth(collection_name, dsname):
    scs = SCAN_SAVING
    dir_components = dict(
        proposal_dirname = scs.proposal_dirname,
        beamline = scs.beamline,
        proposal_session_name = scs.proposal_session_name,
        collection_name = collection_name,
        dataset_name = dsname
    )
    dapth_part = scs.template.format(**dir_components)
    return path.join(scs.base_path, dapth_part)

def get_local_data_fupth(collection_name, dsname, ext='h5'):
    scs = SCAN_SAVING
    fn_components = dict(
        collection_name = collection_name,
        dataset_name = dsname
    )
    fn_part = scs.data_filename.format(**fn_components)
    return '.'.join((fn_part, ext))
    
def get_local_data_fapth(collection_name, dsname, ext='h5'):
    dapth = get_local_data_dapth(collection_name, dsname)
    fupth = get_local_data_fupth(collection_name, dsname)
    fapth = path.join(dapth, fupth)
    return fapth


def open_h5_readonly(fpth):
    fraw = open(fpth, 'rb')
    fh5  = h5py.File(fpth, 'r')
    return fh5
