machinfo = tango.DeviceProxy("acs.esrf.fr:10000/fe/master/id13")
from bliss.setup_globals import *
def test_change_energy():
    for e in (13.5,13.25,13.0):
        ccmo.goto_energy(e, force=True)
        ccmo.show()
        mllenv.align_ugap()
        mllenv.align_osa()
        sven_ct(1,5, machinfo.sr_current)
