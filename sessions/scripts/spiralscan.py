import math
from math import pi as PI
import numpy as np
from matplotlib import pyplot as plt

def spiral_pos(spstep, npts, offset):
    x0, y0 = offset
    x = []
    y = []
    s = []
    _sp_th = 0.0
    _sp_r  = 0.0
    x.append(x0)
    y.append(y0)
    #x.append(x0 + 0.9*spstep*math.cos(-1.1))
    #y.append(y0 + 0.9*spstep*math.sin(-1.1))
    #x.append(x0 + 0.8*spstep*math.cos(-2.2))
    #y.append(y0 + 0.8*spstep*math.sin(-2.2))
    #x.append(x0 + 0.7*spstep*math.cos(-3.3))
    #y.append(y0 + 0.7*spstep*math.sin(-3.3))
    for i in range(1,npts):
        x.append(x0 + _sp_r*math.cos(_sp_th))
        y.append(y0 + _sp_r*math.sin(_sp_th))
        _sp_th += 2*PI/math.sqrt(1+_sp_th*_sp_th)
        _sp_r = spstep*_sp_th/(2*PI)
    x_arr = np.array(x, dtype=np.float64)
    y_arr = np.array(y, dtype=np.float64)
    return x_arr, y_arr

def spiralscan(motx, offx, moty, offy, spstep, npts, expt,rep=1, **kw):
    print("version with repeat ...")
    offset = (offx, offy)
    x_arr, y_arr = spiral_pos(spstep, npts, offset)
    x_arr = np.repeat(x_arr, rep)
    y_arr = np.repeat(y_arr, rep)
    x0_pos = motx.position
    y0_pos = moty.position
    try:
        lookupscan([(motx, x_arr),(moty, y_arr)], expt, **kw)
    finally:
        mv(motx, x0_pos)
        mv(moty, y0_pos)

def spiral_dscan(motx, moty, spstep, npts, expt,rep=1, **kw):
    offx = motx.position
    offy = moty.position
    spiralscan(motx, offx, moty, offy, spstep, npts, expt,rep=rep, **kw)
