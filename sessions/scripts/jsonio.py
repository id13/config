from os import path
import json
from collections import OrderedDict as Odict


class SmpJsonData(object):

    def __init__(self, tipe=None, fxpth=None, allow_overwrite=False):
        print('Hui!')
        self.tipe = tipe
        self.fxpth = fxpth
        self.allow_overwrite=allow_overwrite
        self.dc = dict(objtipe='SmpJsonData', tipe=self.tipe, data=Odict())

    def reset(self):
        self.dc['data'].clear()

    def save(self, fxpth=None):
        if None is fxpth:
            fxpth = self.fxpth
        if path.exists(fxpth):
            if not self.allow_overwrite:
                raise FileExistsError(f'writing to existsing file {self.fxpth} not allowed!')
        with open(fxpth, 'w') as f:
            json.dump(self.dc, f)

    def load(self, fxpth=None):
        if None is fxpth:
            fxpth = self.fxpth
        with open(fxpth, 'r') as f:
            dc = json.load(f)
        assert dc['objtipe'] == self.dc['objtipe']
        assert dc['tipe'] == self.dc['tipe']
        self.dc['data'] = dc['data']
