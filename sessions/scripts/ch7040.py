def set_saxs_roi():
    eiger.roi_counters.set('saxs', (1151.5, 1218.6,9.7, 53.3, -301.4, 58.6))
    MG_EH2.enable("*r:r*")

def set_waxs_roi():
    eiger.roi_counters.set('waxs', (1151.5, 1216, 933.8, 982.9, -243.9, -37.9))
    MG_EH2.enable("*r:r*")

def doo_kmap(*p):
    try:
        set_saxs_roi()
        dkmapyz_2(*p, retveloc=5.0)
        
    finally:
        eiger.roi_counters.clear()
        
def doo_dmesh(*p):
    try:
        #set_saxs_roi()
        set_waxs_roi()
        dmesh(*p)
    finally:
        eiger.roi_counters.clear()
        
