print ("mac32")
def the_scan():
    dscan(ustrz,-0.1,0.1,100,0.05)

def the_scan2():
    dscan(ustrz,-0.1,0.1,100,0.02)

def the_mesh():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.05)

def in1111_mac32():
    mgeig()
    for i in range(38,68):
        pos_name = "m1_frame2_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_scan()
        
    for i in range(68,77):
        pos_name = "m1_frame2_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_mesh()
        
    for i in range(77,95):
        pos_name = "m1_frame1_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_scan2()

print ("mac32 end")
