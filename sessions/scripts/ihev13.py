print('Peng macro night1')

def ihev13_night():
    try:
        so()
        mgeig_x()
        stp = 0.2
        steps = 36
        theta_start = 0
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            fshtrigger()
            kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.1)

        sc()
        
    finally:
        sc()
        sc()
        sc()


