print('load 33')

def safety_net():
    for i in range(1,6):
        print(f'{i} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)

    
def postionning(position):
    ggg.ggopos(f'{position}', 'all')
    #newdataset(f'kmap_{position}_{start_key}')
    print('='*40)
    print(f'measuring sample {position}')


def logbook_image(bla):
    sync()
    elog_plot(scatter = True)
    dooc(bla)
    sync()
    
def fluo_kmaps(position, start_key):
    for fluo in range(86, 107, 10):
        newdataset(f'kmap_{position}cont_fluo{fluo}_{start_key}')
        umv(ufluoy, fluo)
        print('='*40)
        print('ufluoy position = ', fluo)
        dkmapyz_2(-0.025, 0.025, 25, -0.05, 0.05, 50, 0.02, retveloc=5)
        logbook_image(f'this is sample {position} with XRF det at {fluo}')
        
def var_kmaps(intv, expt):
    dkmapyz_2(-0.1, 0.1, intv, -0.1, 0.1, intv, expt, retveloc=5)
    
def xposure_series(position, mvt):
    umvr(ustrz, -mvt)
    newdataset(f'{position}_rad_damage_2ms')
    do_timeseries_run(200, 0.002)
    umvr(ustrz, mvt)
    newdataset(f'{position}_rad_damage_5ms')
    do_timeseries_run(2000, 0.005)
    umvr(ustrz, mvt)
    newdataset(f'{position}_rad_damage_10ms')
    do_timeseries_run(2000, 0.01)
    
def sunday_night():
    start_key = 'b'
    for pos_num in range(1,7):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 80, -0.04, 0.04, 80, 0.02, retveloc=5)
        safety_net()
        
    for pos_num in range(7,9):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 40, 0.01, retveloc=5)
        safety_net()
        
    for pos_num in range(9,10):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.06, 0.06, 60, -0.06, 0.06, 60, 0.02, retveloc=5)
        safety_net()
        
    for pos_num in range(10,11):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 40, 0.02, retveloc=5)
        safety_net()

    for pos_num in range(11,12):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.1, 0.1, 200, -0.1, 0.1, 100, 0.01, retveloc=5)
        safety_net()
        
    for pos_num in range(12,15):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        newdataset(f'kmap_pn_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample pn at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 80, 0.01, retveloc=5)
        safety_net()
        

    for pos_num in range(15,16):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'pn_{pos_num}', 'all')
        for i in range(4):
            newdataset(f'kmap_pn_{pos_num}_iteration_{i}_{start_key}')
            print('='*40)
            print(f'measuring sample pn at position {pos_num}, iteration {i}')
            dkmapyz_2(-0.06, 0.06, 120, -0.06, 0.06, 120, 0.01, retveloc=5)
            safety_net()
        
    
    for pos_num in range(1,2):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.05, 0.05, 100, -0.05, 0.05, 100, 0.01, retveloc=5)
        safety_net()

    for pos_num in range(2,3):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.06, 0.06, 120, -0.06, 0.06, 120, 0.02, retveloc=5)
        safety_net()
        
    for pos_num in range(3,4):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.06, 0.06, 60, -0.06, 0.06, 60, 0.01, retveloc=5)
        safety_net()
        
    for pos_num in range(4,5):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 40, 0.02, retveloc=5)
        safety_net()


    for pos_num in range(6,9):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        for i in range(4):
            newdataset(f'kmap_ds_{pos_num}_iteration_{i}_{start_key}')
            print('='*40)
            print(f'measuring sample ds at position {pos_num}, iteration {i}')
            dkmapyz_2(-0.025, 0.025, 50, -0.025, 0.025, 50, 0.01, retveloc=5)
            safety_net()

    for pos_num in range(9,12):
        pos_num = format(pos_num, '02d')
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.03, 0.03, 20, -0.03, 0.03, 20, 0.01, retveloc=5)
        safety_net()

    for pos_num in range(12,15):
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.025, 0.025, 25, -0.025, 0.025, 50, 0.01, retveloc=5)
        safety_net()
        
    for pos_num in range(15,16):
        ggg.ggopos(f'ds_{pos_num}', 'all')
        newdataset(f'kmap_ds_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring sample ds at position {pos_num}')
        dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 80, 0.01, retveloc=5)
        safety_net()
    

def monday_diner():
    start_key = 'b'
    
    for cross in range(2,7,2):
        for scale in range(1,6):
            position = f'pn1_{cross}_{scale}'
            postionning(position)
            fluo_kmaps(position, start_key)
            logbook_image(f'this is sample {position}')
            safety_net()
    
def monday_night():
    start_key = 'b'
    try: 
        for cross in range(2,7):
            for scale in range(1,4):
                position = f'ds1_{cross}_{scale}'
                postionning(position)
                fluo_kmaps(position, start_key)
                safety_net()
    finally:
        sh1.close()
        sh1.close()
        sh1.close()


def wednesday_evening():
    start_key = 'b'
    sample_id = ['pn', 'ds', 'pohyt']
    
    for samp in sample_id:

        for scale in range(1,19):
            scale_num = format(scale, '02d')
            pos_key = f'{samp}rot{scale_num}'
            postionning(pos_key)
            if scale < 7:
                umv(usrotz, 10)
                newdataset(f'kmap_{pos_key}_10degree_{start_key}')
            elif scale > 6 and scale < 13:
                umv(usrotz, 20)
                newdataset(f'kmap_{pos_key}_20degree_{start_key}')
            else:
                umv(usrotz, 30)
                newdataset(f'kmap_{pos_key}_30degree_{start_key}')
            if samp == 'pn' or sample_id == 'ds':
                if scale % 2 == 0:
                    dkmapyz_2(-0.1, 0.1, 200, -0.1, 0.1, 200, 0.01, retveloc=5)
                else:
                    dkmapyz_2(-0.1, 0.1, 100, -0.1, 0.1, 100, 0.02, retveloc=5)
            elif samp == 'pohyt':
                if scale % 2 == 0:
                    dkmapyz_2(-0.1, 0.1, 200, -0.1, 0.1, 200, 0.02, retveloc=5)
                else:
                    dkmapyz_2(-0.1, 0.1, 100, -0.1, 0.1, 100, 0.025, retveloc=5)
            logbook_image(f'this is scale {pos_key}')
            safety_net()

def wednesday_ds():
    start_key = 'a'
    sample_id = ['ds', 'pohyt']
    
    for samp in sample_id:
        if samp == 'ds':    
            for scale in range(1,19):
                scale_num = format(scale, '02d')
                pos_key = f'{samp}rot{scale_num}'
                postionning(pos_key)
                if scale < 7:
                    umv(usrotz, 10)
                    newdataset(f'kmap_{pos_key}_10degree_{start_key}')
                elif scale > 6 and scale < 13:
                    umv(usrotz, 20)
                    newdataset(f'kmap_{pos_key}_20degree_{start_key}')
                else:
                    umv(usrotz, 30)
                    newdataset(f'kmap_{pos_key}_30degree_{start_key}')
                if scale % 2 == 0:
                    dkmapyz_2(-0.1, 0.1, 200, -0.1, 0.1, 200, 0.01, retveloc=5)
                else:
                    dkmapyz_2(-0.1, 0.1, 100, -0.1, 0.1, 100, 0.02, retveloc=5)
        elif samp == 'pohyt':
            for scale in range(7,19):
                scale_num = format(scale, '02d')
                pos_key = f'{samp}rot{scale_num}'
                postionning(pos_key)
                if scale < 7:
                    umv(usrotz, 10)
                    newdataset(f'kmap_{pos_key}_10degree_{start_key}')
                elif scale > 6 and scale < 13:
                    umv(usrotz, 20)
                    newdataset(f'kmap_{pos_key}_20degree_{start_key}')
                else:
                    umv(usrotz, 30)
                    newdataset(f'kmap_{pos_key}_30degree_{start_key}')

                if scale % 2 == 0:
                    dkmapyz_2(-0.1, 0.1, 200, -0.1, 0.1, 200, 0.02, retveloc=5)
                else:
                    dkmapyz_2(-0.1, 0.1, 100, -0.1, 0.1, 100, 0.025, retveloc=5)
            logbook_image(f'this is scale {pos_key}')
            safety_net()

def thursday_early():
    start_key = 'c'
    sample_id = ['po', 'pso', 'pinsul']
    
    for samp in sample_id:

        for scale in range(1,19):
            scale_num = format(scale, '02d')
            pos_key = f'{samp}rot{scale_num}'
            postionning(pos_key)
            if scale < 7:
                umv(usrotz, 10)
                newdataset(f'kmap_{pos_key}_10degree_{start_key}')
            elif scale > 6 and scale < 13:
                umv(usrotz, 20)
                newdataset(f'kmap_{pos_key}_20degree_{start_key}')
            else:
                umv(usrotz, 30)
                newdataset(f'kmap_{pos_key}_30degree_{start_key}')
            if samp == 'pinsul' or samp == 'pso':
                if scale % 2 == 0:
                    dkmapyz_2(-0.04, 0.04, 80, -0.04, 0.04, 80, 0.02, retveloc=5)
                else:
                    dkmapyz_2(-0.04, 0.04, 40, -0.04, 0.04, 40, 0.025, retveloc=5)
            elif samp == 'po':
                if scale % 2 == 0:
                    dkmapyz_2(-0.035, 0.035, 70, -0.035, 0.035, 70, 0.02, retveloc=5)
                else:
                    dkmapyz_2(-0.035, 0.035, 35, -0.035, 0.035, 35, 0.025, retveloc=5)
            logbook_image(f'this is scale {pos_key}')
            safety_net()
        
def thursday_diner():
    start_key = 'a'
    
    for cross in range(8,9):
        for scale in range(5,6):
            position = f'dsxection1_{cross}_{scale}'
            postionning(position)
            fluo_kmaps(position, start_key)
            safety_net()
            
def thursday_night():
    start_key = 'a'
    try:
        for scale in range(1, 15):
            scale_num = format(scale, '02d')
            pos_key = f'popupa_{scale_num}'
            ggg.ggopos(f'{pos_key}', 'all')
            newdataset(f'kmap_{pos_key}_{start_key}')
            print('='*40)
            print(f'measuring sample {pos_key}')
            if scale % 2 == 0:
                dkmapyz_2(-0.25, 0.25, 250, -0.25, 0.25, 250, 0.02, retveloc=5)
            else:
                dkmapyz_2(-0.25, 0.25, 500, -0.25, 0.25, 500, 0.01, retveloc=5)
            safety_net()
            

    finally:
        sc()
        sc()
        sc()
    
            
def thursday_lunch():
    start_key = 'a'
    
    for cross in range(7,10):
        for scale in range(1,5):
            position = f'pnxsection1_{cross}_{scale}'
            postionning(position)
            fluo_kmaps(position, start_key)
            #logbook_image(f'this is sample {position}')
            safety_net()

def rad_damage():
    position = 'damagefull1'
    ggg.ggopos(position, 'all')
    xposure_series(position, 0.007)
    position = 'damagexsection1'
    ggg.ggopos(position, 'all')
    umvr(ustrz, 0.02)
    xposure_series(position, 0.007)
    
def leftsample():
  
    start_key = 'b'
    
    for cross in range(5,6):
        for scale in range(1,5):
            position = f'pnxsection1_{cross}_{scale}'
            postionning(position)
            fluo_kmaps(position, start_key)
            #logbook_image(f'this is sample {position}')
            safety_net()
