import numpy as np
from numpy import poly1d
from scipy.optimize import leastsq

class Virtual(Exception): pass

def make_dev_func(func):
    def curry_dev_func(p, y, x):
        return y - func(p, x)
    return curry_dev_func

def np_poly1d(p, x):
    f = poly1d(p)
    return f(x)

def make_poly1d(order):
    def curry_poly1d(p, x):
        assert len(p) == order + 1
        _f = poly1d(p)
        return _f(x)
    return curry_poly1d

def sci_sin(p, x):
    (a,ph,off) = p
    return a*np.sin(x-ph) + off

def sci_cos(p, x):
    (a,ph,off) = p
    return a*np.cos(x-ph) + off

def make_sci_poly1d_dict(order):
    name = f'sci_poly1d_ord{order}'
    param_names = []
    def_params = []
    for i in range(order+1):
        def_params.append(0.0)
        param_names.append(f'a{i}')
    def_params = tuple(def_params)
    param_names = tuple(param_names)
    func = make_poly1d(order)
    dc = dict(
        name = name,
        func = func,
        dev_func = make_dev_func(func),
        param_names = param_names,
        def_params = def_params
    )
    return dc

SCI_SIN = dict(
    name = 'sci_sin',
    func = sci_sin,
    dev_func = make_dev_func(sci_sin),
    param_names = ('amp','phase','offset'),
    def_params = (1.0, 0.0, 0.0)
)

SCI_COS = dict(
    name = 'sci_cos',
    func = sci_cos,
    dev_func = make_dev_func(sci_cos),
    param_names = ('amp','phase','offset'),
    def_params = (1.0, 0.0, 0.0)
)

class SciFuncEnv1D(object):

    def __init__(self, **func_dict):
        for k,v in func_dict.items():
            setattr(self, k, v)
        self.params = self.def_params
        self.ini_params = None

    def eval_func(self, x):
        return self.func(self.params, x)

    def fit(self, ini_params, y, x):
        if None is ini_params:
            ini_params = self.def_params
        fit_params = leastsq(
                self.dev_func,
                ini_params,
                args=(y,x)
            )
        return fit_params[0]

    def set_params(self, params):
        self.params = params

    def get_params(self):
        return self.params

    def update(self, ini_params, y, x):
        self.x = x
        self.y = y
        if not None is ini_params:
           ini_params = self.params
        self.params = self.fit(ini_params, y, x)
