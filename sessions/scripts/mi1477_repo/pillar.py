from pprint import pprint
import numpy as np
from scipy import ndimage as ndi

from stom_access import scanaccess as sacc
from stom_access import edfsupport

def meshcorr_from_pixsamp(pt0, pt0_ideal, phl, phr, shape, limits):

    (m10_, m11),(m20,m21) = limits
    if m11-m10 < 0:
        correction_sense_1 = -1
    else:
        correction_sense_1 = 1
    if m21-m20 < 0:
        correction_sense_2 = -1
    else:
        correction_sense_2 = 1

    (s0, s1) = shape
    # get scan resolution in mesh order
    stp0, stp1 = pix_to_scanresol(shape, limits)

    # pix coordinates
    # pt0_ideal : ideal numbers of pixels from the top of
    #             the map to the top of the sample
    # pt0       : the top pixel coordinate found
    rel_corr_2 = (pt0_ideal - pt0)*stp1*correction_sense_2
    (phl0, phl1) = phl
    (phr0, phr1) = prl
    pmarg_l0 = phl0*stp1
    pmarg_l1 = s1-1-phl0*stp0
    mean_pmarg = 0.5*(pmarg_l0+pmarg_l1)
    rel_corr_1 = (pmarg_l0-mean_pmarg)*stp0*correction_sense_1
    # you have to check and catch thngs later:
    # 1) mean magin concept breaks down if the sample is
    #    partially out of field i.e. if one of the pmarg values
    #    is 0. Thsi is similar for pt0 ...
    return rel_corr_1, rel_corr_2
    

def parse_pillar_map_v1(map_arr, mlimits, initial_motpos, threshold, sum_thresh,
    reflength, outfxpath_tpl):
    keys = "raw med thresh anno".split()
    ecks = edfsupport.EDFClosedKeySet_RW(outfxpath_tpl, keys)
    s0, s1 = map_arr.shape
    md_arr = ndi.median_filter(map_arr, 7)
    th_arr = np.where(md_arr > threshold, 1, 0)
    anno_arr = th_arr.copy()
    ecks['raw'] = map_arr
    ecks['med'] = md_arr
    ecks['thresh'] = th_arr
    hpix_arr = np.arange(s1)
    

    top_hit = -1 
    idx_ll = []
    th_thesum_ll = []
    raw_themean_ll = []
    raw_thevar_ll = []
    found_flg = False
    for i in range(s0):
        idx_ll.append(i)
        th_thesum = th_arr[i].sum()
        raw_themean = map_arr[i].mean()
        raw_thevar = map_arr[i].var()
        th_thesum_ll.append(th_thesum)
        raw_themean_ll.append(raw_themean)
        raw_thevar_ll.append(raw_thevar)
        if not found_flg:
            if th_thesum > sum_thresh:
                top_hit = i
                found_flg = True
                hor_cog = sacc.compute_cog_1d(hpix_arr, th_arr)
    th_thesum_arr = np.array(th_thesum_ll, dtype=np.int32)
    th_themean_arr = np.array(th_thesum_ll, dtype=np.float64)
    th_thevar_arr = np.array(th_thesum_ll, dtype=np.float64)
    #pprint(list(zip(thesum_ll, themean_ll, thevar_ll)))
    search_arr = th_thesum_arr[0: top_hit+reflength]
    vert_max = search_arr.max()
    vert_argmax = search_arr.argmax()
    refline=th_arr[vert_argmax]
    print('top_hit =', top_hit, '  hor_cog=', hor_cog)
    print('vert_argmax =', vert_argmax, '  vert_max=', vert_max)
    samp_indices = np.compress(refline, hpix_arr)
    print(samp_indices)
    low_hor_idx = samp_indices[0]
    hi_hor_idx = samp_indices[-1]
    print('low_hor_idx =', low_hor_idx, '  hi_hor_idx=', hi_hor_idx)
    anno_arr[top_hit,int(hor_cog)]  += 1
    anno_arr[vert_argmax,low_hor_idx]  += 1
    anno_arr[vert_argmax,hi_hor_idx]  += 1
    ecks['anno'] = anno_arr
    
    return top_hit, hor_cog, vert_argmax, vert_max, low_hor_idx, hi_hor_idx
