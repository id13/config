def vol_pseudo_edge(km):
    qgoto_cen(ct22,km)

def vol_knife(km,khw, n, t):
    dscan(km,-khw,khw,n,t)
    vol_pseudo_edge(km)

def voL_aucau_detailed_1():
    mod = 'y'
    xfocm = ustrx
    kmy = ustry
    kmz = ustrz

    h_y = 13.0
    h_z = 6.44
    v_y = h_y-0.2
    v_z = h_z+0.2

    if 'z' == mod:
        raise ValueError('tolerance problem')
        mv(kmy, v_y)
        mv(kmz, v_z)
        km = ustrz
    elif 'y' == mod:
        mv(kmy, h_y)
        #mv(kmz, h_z)
        km = ustry
    else:
        raise ValueError('illegal mod: %s' % mod)

    multipl = 8

    xstart = -38
    xstep = 2.0/multipl
    xnstp = 14*multipl
    kmhw = 0.05
    kmn = 200
    kmt = 0.05
    mv(xfocm, xstart)
    vol_knife(km,0.3,60,0.05)
    for i in range(xnstp+1):
        xpos = xstart + i*xstep
        print ('='*40)
        print ("i =", i, '    xpos =', xpos)
        
        mv(xfocm, xstart + i*xstep)
        vol_knife(km,kmhw, kmn, kmt)
        

# RUN
# * TF 7,8 in and aligned
# 1: #S 1124-14 to 1124 (log1 saved)
#    hor knife, 13.kev, no prefocus, 14 intv in kmx
# 2: #S last scan 1142 (done)
#    hor knife, 13.kev, no prefocus, 14 intv in kmx
# 3: #S last scan 1164 
#    hor knife, 13.kev, 3L prefocus aligned to TF 7,8 in with diode, 14 intv in kmx
# 4  #S scan 1164 
#    loopscan as stability check over refill, 
# 3: #S first scan 1165
#    hor knife, 13.kev, 3L prefocus, 14*8 intv in kmx
# 6: #S first scan 1280 (just a repeat)
#    hor knife, 13.kev, 3L prefocus, 14*8 intv in kmx
