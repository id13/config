print('version ttomo ch7039')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

# enter scanning stepsize here [um]:
stepsize = 0.75
# define fov in asy.com

# overlap between patches [mm]
ov = 0.0035

t_timeout = 120 # s - has to be slightly longer than 1 scan

# insert rotations from generate_rotations.py
# index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 1.385, 0
2, 0, 2.769, 0
3, 0, 4.154, 0
4, 0, 5.538, 0
5, 0, 6.923, 0
6, 0, 8.308, 0
7, 0, 9.692, 0
8, 0, 11.077, 0
9, 0, 12.462, 0
10, 0, 13.846, 0
11, 0, 15.231, 0
12, 0, 16.615, 0
13, 0, 18.000, 0
14, 0, 19.385, 0
15, 0, 20.769, 0
16, 0, 22.154, 0
17, 0, 23.538, 0
18, 0, 24.923, 0
19, 0, 26.308, 0
20, 0, 27.692, 0
21, 0, 29.077, 0
22, 0, 30.462, 0
23, 0, 31.846, 0
24, 0, 33.231, 0
25, 0, 34.615, 0
26, 0, 36.000, 0
27, 0, 37.385, 0
28, 0, 38.769, 0
29, 0, 40.154, 0
30, 0, 41.538, 0
31, 0, 42.923, 0
32, 0, 44.308, 0
33, 0, 45.692, 0
34, 0, 47.077, 0
35, 0, 48.462, 0
36, 0, 49.846, 0
37, 0, 51.231, 0
38, 0, 52.615, 0
39, 0, 54.000, 0
40, 0, 55.385, 0
41, 0, 56.769, 0
42, 0, 58.154, 0
43, 0, 59.538, 0
44, 0, 60.923, 0
45, 0, 62.308, 0
46, 0, 63.692, 0
47, 0, 65.077, 0
48, 0, 66.462, 0
49, 0, 67.846, 0
50, 0, 69.231, 0
51, 0, 70.615, 0
52, 0, 72.000, 0
53, 0, 73.385, 0
54, 0, 74.769, 0
55, 0, 76.154, 0
56, 0, 77.538, 0
57, 0, 78.923, 0
58, 0, 80.308, 0
59, 0, 81.692, 0
60, 0, 83.077, 0
61, 0, 84.462, 0
62, 0, 85.846, 0
63, 0, 87.231, 0
64, 0, 88.615, 0
65, 0, 90.000, 0
66, 0, 91.385, 0
67, 0, 92.769, 0
68, 0, 94.154, 0
69, 0, 95.538, 0
70, 0, 96.923, 0
71, 0, 98.308, 0
72, 0, 99.692, 0
73, 0, 101.077, 0
74, 0, 102.462, 0
75, 0, 103.846, 0
76, 0, 105.231, 0
77, 0, 106.615, 0
78, 0, 108.000, 0
79, 0, 109.385, 0
80, 0, 110.769, 0
81, 0, 112.154, 0
82, 0, 113.538, 0
83, 0, 114.923, 0
84, 0, 116.308, 0
85, 0, 117.692, 0
86, 0, 119.077, 0
87, 0, 120.462, 0
88, 0, 121.846, 0
89, 0, 123.231, 0
90, 0, 124.615, 0
91, 0, 126.000, 0
92, 0, 127.385, 0
93, 0, 128.769, 0
94, 0, 130.154, 0
95, 0, 131.538, 0
96, 0, 132.923, 0
97, 0, 134.308, 0
98, 0, 135.692, 0
99, 0, 137.077, 0
100, 0, 138.462, 0
101, 0, 139.846, 0
102, 0, 141.231, 0
103, 0, 142.615, 0
104, 0, 144.000, 0
105, 0, 145.385, 0
106, 0, 146.769, 0
107, 0, 148.154, 0
108, 0, 149.538, 0
109, 0, 150.923, 0
110, 0, 152.308, 0
111, 0, 153.692, 0
112, 0, 155.077, 0
113, 0, 156.462, 0
114, 0, 157.846, 0
115, 0, 159.231, 0
116, 0, 160.615, 0
117, 0, 162.000, 0
118, 0, 163.385, 0
119, 0, 164.769, 0
120, 0, 166.154, 0
121, 0, 167.538, 0
122, 0, 168.923, 0
123, 0, 170.308, 0
124, 0, 171.692, 0
125, 0, 173.077, 0
126, 0, 174.462, 0
127, 0, 175.846, 0
128, 0, 177.231, 0
129, 0, 178.615, 0
130, 0, 0.000, 0
131, 10, 8.182, 0
132, 10, 24.174, 0
133, 10, 40.165, 0
134, 10, 56.157, 0
135, 10, 72.149, 0
136, 10, 88.140, 0
137, 10, 104.132, 0
138, 10, 120.124, 0
139, 10, 136.116, 0
140, 10, 152.107, 0
141, 10, 168.099, 0
142, 10, 184.091, 0
143, 10, 200.083, 0
144, 10, 216.074, 0
145, 10, 232.066, 0
146, 10, 248.058, 0
147, 10, 264.050, 0
148, 10, 280.041, 0
149, 10, 296.033, 0
150, 10, 312.025, 0
151, 10, 328.017, 0
152, 10, 344.008, 0
153, 0, 0.000, 0
154, 20, 0.000, 0
155, 20, 17.143, 0
156, 20, 34.286, 0
157, 20, 51.429, 0
158, 20, 68.571, 0
159, 20, 85.714, 0
160, 20, 102.857, 0
161, 20, 120.000, 0
162, 20, 137.143, 0
163, 20, 154.286, 0
164, 20, 171.429, 0
165, 20, 188.571, 0
166, 20, 205.714, 0
167, 20, 222.857, 0
168, 20, 240.000, 0
169, 20, 257.143, 0
170, 20, 274.286, 0
171, 20, 291.429, 0
172, 20, 308.571, 0
173, 20, 325.714, 0
174, 20, 342.857, 0
175, 0, 0.000, 0
176, 30, 9.474, 0
177, 30, 27.922, 0
178, 30, 46.371, 0
179, 30, 64.820, 0
180, 30, 83.269, 0
181, 30, 101.717, 0
182, 30, 120.166, 0
183, 30, 138.615, 0
184, 30, 157.064, 0
185, 30, 175.512, 0
186, 30, 193.961, 0
187, 30, 212.410, 0
188, 30, 230.859, 0
189, 30, 249.307, 0
190, 30, 267.756, 0
191, 30, 286.205, 0
192, 30, 304.654, 0
193, 30, 323.102, 0
194, 30, 341.551, 0
195, 0, 0.000, 0
196, 40, 0.000, 0
197, 40, 21.176, 0
198, 40, 42.353, 0
199, 40, 63.529, 0
200, 40, 84.706, 0
201, 40, 105.882, 0
202, 40, 127.059, 0
203, 40, 148.235, 0
204, 40, 169.412, 0
205, 40, 190.588, 0
206, 40, 211.765, 0
207, 40, 232.941, 0
208, 40, 254.118, 0
209, 40, 275.294, 0
210, 40, 296.471, 0
211, 40, 317.647, 0
212, 40, 338.824, 0
213, 0, 0.000, 0
214, 45, 11.250, 0
215, 45, 33.047, 0
216, 45, 54.844, 0
217, 45, 76.641, 0
218, 45, 98.438, 0
219, 45, 120.234, 0
220, 45, 142.031, 0
221, 45, 163.828, 0
222, 45, 185.625, 0
223, 45, 207.422, 0
224, 45, 229.219, 0
225, 45, 251.016, 0
226, 45, 272.812, 0
227, 45, 294.609, 0
228, 45, 316.406, 0
229, 45, 338.203, 0
230, 0, 0.000, 0
231, 35, 0.000, 0
232, 35, 20.000, 0
233, 35, 40.000, 0
234, 35, 60.000, 0
235, 35, 80.000, 0
236, 35, 100.000, 0
237, 35, 120.000, 0
238, 35, 140.000, 0
239, 35, 160.000, 0
240, 35, 180.000, 0
241, 35, 200.000, 0
242, 35, 220.000, 0
243, 35, 240.000, 0
244, 35, 260.000, 0
245, 35, 280.000, 0
246, 35, 300.000, 0
247, 35, 320.000, 0
248, 35, 340.000, 0
249, 0, 0.000, 0
250, 25, 9.000, 0
251, 25, 26.550, 0
252, 25, 44.100, 0
253, 25, 61.650, 0
254, 25, 79.200, 0
255, 25, 96.750, 0
256, 25, 114.300, 0
257, 25, 131.850, 0
258, 25, 149.400, 0
259, 25, 166.950, 0
260, 25, 184.500, 0
261, 25, 202.050, 0
262, 25, 219.600, 0
263, 25, 237.150, 0
264, 25, 254.700, 0
265, 25, 272.250, 0
266, 25, 289.800, 0
267, 25, 307.350, 0
268, 25, 324.900, 0
269, 25, 342.450, 0
270, 0, 0.000, 0
271, 15, 0.000, 0
272, 15, 17.143, 0
273, 15, 34.286, 0
274, 15, 51.429, 0
275, 15, 68.571, 0
276, 15, 85.714, 0
277, 15, 102.857, 0
278, 15, 120.000, 0
279, 15, 137.143, 0
280, 15, 154.286, 0
281, 15, 171.429, 0
282, 15, 188.571, 0
283, 15, 205.714, 0
284, 15, 222.857, 0
285, 15, 240.000, 0
286, 15, 257.143, 0
287, 15, 274.286, 0
288, 15, 291.429, 0
289, 15, 308.571, 0
290, 15, 325.714, 0
291, 15, 342.857, 0
292, 0, 0.000, 0
293, 5, 8.182, 0
294, 5, 24.174, 0
295, 5, 40.165, 0
296, 5, 56.157, 0
297, 5, 72.149, 0
298, 5, 88.140, 0
299, 5, 104.132, 0
300, 5, 120.124, 0
301, 5, 136.116, 0
302, 5, 152.107, 0
303, 5, 168.099, 0
304, 5, 184.091, 0
305, 5, 200.083, 0
306, 5, 216.074, 0
307, 5, 232.066, 0
308, 5, 248.058, 0
309, 5, 264.050, 0
310, 5, 280.041, 0
311, 5, 296.033, 0
312, 5, 312.025, 0
313, 5, 328.017, 0
314, 5, 344.008, 0
315, 0, 0.000, 0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,tighti_y,tighti_z) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['tighti_y'],
            sp['tighti_z'] 
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        print('would start now')
        print(tighti_y, tighti_z)
        
        shift_y = 0.045 - ov # fast axis y -> fov +-45 um
        shift_z = 0.049 - ov # slow axis z -> fov +-49 um
        mvy_p1 = - shift_y 
        mvz_p1 = - shift_z
        mvy_p2 = + 2 * shift_y
        mvy_p3 = - 2 * shift_y
        mvz_p3 = + 2 * shift_z
        mvy_p4 = + 2 * shift_y
        mvy_p0 = - shift_y
        mvz_p0 = - shift_z
        
        cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        print('move to patch1 (upper left)')
        umvr(nny, mvy_p1, nnz, mvz_p1)
        with gevent.Timeout(seconds=t_timeout):
            try:
                # put the nano scan ...
                print('patch1  (upper left)')
                lly_new = lly + tighti_y
                llz_new = llz + tighti_z
                dy_new = uly - lly_new
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//stepsize)
                nitvz_new = int(dz_new//stepsize)

                kmap.dkmap(
                    nnp5, lly_new, uly ,nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
        
        print('move to patch 2 (upper right')
        umvr(nny, mvy_p2)
        with gevent.Timeout(seconds=t_timeout):
            try:
                # put the nano scan ...
                print('patch2')
                uly_new = uly-tighti_y
                llz_new = llz+tighti_z
                dy_new = uly_new - lly
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//stepsize)
                nitvz_new = int(dz_new//stepsize)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch2 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch3 (lower left)')
        umvr(nny, mvy_p3, nnz, mvz_p3)
        with gevent.Timeout(seconds=t_timeout):
            try:
                # put the nano scan ...
                print('patch3  (lower left)')
                lly_new = lly + tighti_y
                ulz_new = ulz - tighti_z
                dy_new = uly - lly_new
                dz_new = ulz_new- llz
                nitvy_new = int(dy_new//stepsize)
                nitvz_new = int(dz_new//stepsize)
                kmap.dkmap(
                    nnp5, lly_new, uly, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch3 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch4 (lower right)')
        umvr(nny, mvy_p4)
        with gevent.Timeout(seconds=t_timeout):
            try:
                # put the nano scan ...
                print('patch4  (lower right)')
                uly_new = uly-tighti_y
                ulz_new = ulz-tighti_z
                dy_new = uly_new - lly
                dz_new = ulz_new - llz
                nitvy_new = int(dy_new//stepsize)
                nitvz_new = int(dz_new//stepsize)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch4 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move back to cen')
        umvr( nny, mvy_p0, nnz, mvz_p0 )
        #time.sleep(5)
        

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    print(v)
                    print(w)
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    if 'tighti' == mode:
                        tighti_t = (tighti_y, tighti_z) = tuple(map(float, w[1:]))
                        print(f'process_instructions - {tighti_y}{tighti_z}')
                        print(tighti_t)
                        self.adapt_tighti_scanparams(tighti_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = stepsize*(lly//stepsize)
        uly = stepsize*(uly//stepsize)
        llz = stepsize*(llz//stepsize)
        ulz = stepsize*(ulz//stepsize)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//stepsize)
        nitvz = int(dz//stepsize)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z
        
    def adapt_tighti_scanparams(self, tighti_t):
        print(tighti_t)
        (tighti_y,tighti_z) = tighti_t
        sp = self.scanparams
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z


def ch7039_ttomo_main(zkap, start_key, resume=-1):

    # verify zkap
    print(zkap)

    
    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -45,
        uly = 45,
        nitvy = 180,
        llz = -49,
        ulz = 49,
        nitvz = 200,
        expt = 0.002,
        tighti_y = 0,
        tighti_z = 0
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
