INFO = '''
microscope
    z_stage
        name = 'umicroz'
        up_pos = +192.0
        down_pos = 0
    y_stage
        name = 'umicroy'
    in_movement = @t
    change_lightlevel = 'slil(<value>) with value from 0 to 100'
    
regular_beamstop
    stages = 'ubsx ubsy ubsz'
    in_pos = 'ubsy = 0.0'
    beam_align_pos = 'ubsy = -1.0'
    out_pos = 'ubsy = -110.0'
    alignment = 'bsalign()'

big_beamstop
    stages = 'ufluox ufluoy ufluoz'
    in_pos = 'ufluoy = 0.0'
    beam_align_pos = 'ufluoy = +4.0'
    out_pos = 'ufluoy = +120

beam_alignment:
    scheme_along_x = @t
    EIGER beamstop sample bap_aperture sap_aperture transfocator_with Be-CRLs fastshutter safetyshutter EH1 smonochromator
    @-
    
        
# sample lys_c2
# x<photon energy>
# pf<# of prefocusing lenses>
# u18_<gap>'
# dooi('x15_3pf_u18_p8p022_characterization')
# 'Sun Jul 14 12:25:45 2024'
# ===============================================================
# newdataset(''x15_3pf_u18_p8p022')
# aligning gold cross in microscope and chracterizing beam
# ===============================================================
# newdataset('x15_goldcross')
# y-profile documentation at focus 
#     scan number: #7
#     estimated beam size hor: 2.5 micron
# z-profile documentation at focus 
#     scan number: #8
#     estimated beam size vert: 2.6 micron
# z-profile documentation at 40mm downstream from focus 
#     scan number: #9
#     estimated beam size vert: 25 micron
# y-profile documentation at 40mm downstream from focus 
#     scan number: #11
#     estimated beam size hor: 21 micron
# 
#  u18 flux transfer calibration (diode to ion chamber)
# full flux: u18 = 8,02          target ct24 value = 329300 for 1 sec count
# about half flux: u18 = 7.95     target ct24 value = 174350 for 1 sec count
# about 1/5th fluz: u18 = 7.905   target ct24 value = 81380 for 1 sec count
#
# ion dark current = 19400.0
# diode dark current = 100.0
#
# y-profile beamsize (scan #19) at 1/5th: estim 3.4micron
# estimated beamsize: @half vert 3.1 micron
# estimated beamsize: @1/5th hor 3.4 micron

# repeat beam size measurem.
# scan #24: 1/5th hor: 3.5 micron
# scan #25: 1/5th vert: 3.1 micron

# repeat beam size measurem.
# scan #27: 1/2th vert: 3.0 micron
# scan #24: 1/2th hor: 3.2 micron
