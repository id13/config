def testb():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    gdi=50.0
    gdj=30.0
    patchparams = dict(
        kpartial = (
            0,gdi,10,
            0,gdj, 6,
            0.02),
        ni=3    ,  nj=2    ,
        di=gdi*0.001 ,  dj=gdj*0.001,
        start_key='b',
        dsname_tag = 'testbtag_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'pos_testb2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)
