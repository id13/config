print('hg169_day3_2 load 10')
import time

print("=== hg169_day3_2 ===")

DSKEY='q' # to be incremented if there is a crash

class EXPO:

    expt = 0.02

class FLUX(object):

    tag = 'noneFX'

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 2.0

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



def hi_flux():
    FLUX.tag = 'hi6p31FX'
    umv(u18, 6.31)

def lo_flux():
    FLUX.tag = 'lo6p13FX'
    umv(u18, 6.13)

def doloop(s, npts, expt):
    print("\n\n\n======================== doing loop:", s, ' n=', npts, '  expt =', expt)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)

    print("\n[loopscan]:")
    fshtrigger()
    loopscan(npts, expt)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)


def dooul(s, pos_s=None, dsfrag=None):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    if not None is dsfrag:
        s_ds = dsfrag

    dsname = "%s_%s" % (DSKEY, s_ds)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    if None is pos_s:
        gopos(s)
    else:
        gopos(pos_s)

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def raw_hg169_day2_1():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    lo_flux()
    dooul('leti2_S2_ul53_1x1_475x145_950x290_10_0059.json')
    dooul('leti2_S4_ul52_1x1_350x135_700x270_10_0057.json')
    dooul('leti2_S7_ul54_1x1_470x200_940x400_10_0060.json')
    dooul('leti2_S8_ul51_1x1_320x160_640x320_10_0056.json')
    dooul('leti2_S9_ul50_1x1_400x150_800x300_10_0055.json')
    
    
    
def raw_hg169_day3_2():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    hi_flux()
    dooul('bamyian_bmm033_ul01_1x1_200x75_400x150_10_0030.json')
    dooul('bamyian_bmm035_ul01_1x1_75x75_150x150_10_0006.json')
    dooul('bamyian_bmm039_ul01_1x1_175x75_350x150_10_0028.json')
    dooul('bamyian_bmm040_ul01_1x1_275x125_550x250_10_0023.json')
    dooul('bamyian_bmm045_2_ul01_1x1_300x100_600x200_10_0027.json')
    dooul('bamyian_bmm045_ul01_1x1_200x100_400x200_10_0005.json')
    dooul('bamyian_bmm055_ul01_1x1_125x50_250x100_10_0012.json')
    dooul('bamyian_bmm082_ul01_1x1_150x100_300x200_10_0024.json')
    dooul('bamyian_bmm111_ul01_1x1_175x175_350x350_10_0029.json')
    dooul('bamyian_bmm128_ul01_1x1_250x250_500x500_10_0031.json')
    dooul('bamyian_bmm145_ul01_1x1_200x100_400x200_10_0022.json')
    dooul('bamyian_bmm154_ul01_1x1_200x50_400x100_10_0026.json')
    dooul('bamyian_bmm181_1_ul01_1x1_200x75_400x150_10_0003.json')
    dooul('bamyian_bmm181_2_ul01_1x1_225x100_450x200_10_0011.json')
    dooul('bamyian_fdm33_ul01_1x1_350x250_700x500_10_0010.json')
    dooul('bamyian_fdm37_ul01_1x1_300x75_600x150_10_0013.json')
    dooul('bamyian_fdm53_1_ul01_1x1_100x75_200x150_10_0001.json')
    dooul('bamyian_fdm53_2_ul01_1x1_100x50_200x100_10_0004.json')
    dooul('bamyian_fdm59_ul01_1x1_160x90_320x180_10_0025.json')
    
    
def raw_hg169_day3_3():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    hi_flux()
    dooul('bamyian_bmm033_ul01_1x1_400x150_400x150_10_0030.json',
        pos_s='bamyian_bmm033_ul01_1x1_200x75_400x150_10_0030.json')
        
        
    dooul('bamyian_bmm035_ul01_1x1_150x150_150x150_10_0006.json',
        pos_s='bamyian_bmm035_ul01_1x1_75x75_150x150_10_0006.json')
    
    
    dooul('bamyian_bmm039_ul01_1x1_350x150_350x150_10_0028.json',
        pos_s='bamyian_bmm039_ul01_1x1_175x75_350x150_10_0028.json')
    
        
    dooul('bamyian_bmm040_ul01_1x1_550x250_550x250_10_0023.json',
        pos_s='bamyian_bmm040_ul01_1x1_275x125_550x250_10_0023.json')  
    
        
    dooul('bamyian_bmm045_2_ul01_2x1_300x200_300x200_10_0027.json',
        pos_s='bamyian_bmm045_2_ul01_1x1_300x100_600x200_10_0027.json')
    
    
    dooul('bamyian_bmm045_ul01_1x1_400x230_400x230_10_0005.json',
        pos_s='bamyian_bmm045_ul01_1x1_200x100_400x200_10_0005.json')   
    
    
        
    dooul('bamyian_bmm055_ul01_1x1_250x100_250x100_10_0012.json',
        pos_s='bamyian_bmm055_ul01_1x1_125x50_250x100_10_0012.json')   
    
    
    
    dooul('bamyian_bmm082_ul01_1x1_300x200_300x200_10_0024.json',
        pos_s='bamyian_bmm082_ul01_1x1_150x100_300x200_10_0024.json')   
    
    
    
    dooul('bamyian_bmm111_ul01_1x1_350x350_350x350_10_0029.json',
        pos_s='bamyian_bmm111_ul01_1x1_175x175_350x350_10_0029.json')
    
       
    dooul('bamyian_bmm128_ul01_1x1_500x500_500x500_10_0031.json',
        pos_s='bamyian_bmm128_ul01_1x1_250x250_500x500_10_0031.json')   
    
        
    
    dooul('bamyian_bmm145_ul01_1x1_400x220_400x220_10_0022.json',
        pos_s='bamyian_bmm145_ul01_1x1_200x100_400x200_10_0022.json')   
    
            
        
    dooul('bamyian_bmm154_ul01_1x1_400x100_400x100_10_0026.json',
        pos_s='bamyian_bmm154_ul01_1x1_200x50_400x100_10_0026.json')   
    
    
    
    dooul('bamyian_bmm181_1_ul01_1x1_400x200_400x200_10_0003.json',
        pos_s='bamyian_bmm181_1_ul01_1x1_200x75_400x150_10_0003.json')   
    
    
    dooul('bamyian_bmm181_2_ul01_1x1_450x200_450x200_10_0011.json',
         pos_s='bamyian_bmm181_2_ul01_1x1_225x100_450x200_10_0011.json')  
    
    
    
    dooul('bamyian_fdm33_ul01_2x1_350x500_350x500_10_0010.json',
        pos_s='bamyian_fdm33_ul01_1x1_350x250_700x500_10_0010.json')   
    
          
    
    dooul('bamyian_fdm37_ul01_2x1_300x150_300x150_10_0013.json',
        pos_s='bamyian_fdm37_ul01_1x1_300x75_600x150_10_0013.json')   
    
    
        
    dooul('bamyian_fdm53_1_ul01_1x1_200x150_200x150_10_0001.json',
        pos_s='bamyian_fdm53_1_ul01_1x1_100x75_200x150_10_0001.json')   
    
    
        
    dooul('bamyian_fdm53_2_ul01_1x1_200x100_200x100_10_0004.json',
        pos_s='bamyian_fdm53_2_ul01_1x1_100x50_200x100_10_0004.json')   
    
    
       
    dooul('bamyian_fdm59_ul01_1x1_320x220_320x220_10_0025.json', 
        pos_s='bamyian_fdm59_ul01_1x1_160x90_320x180_10_0025.json')     
    
    
    
    
def raw_hg169_day2_3():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    #lo_flux()
    dooul('Pa_plate_PaDrLiW_NA_ul01_2x2_400x400_800x800_10_0086.json')
    
    
def raw_hg169_day2_4():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    #lo_flux()
    # dooul('Pa_plate_PaDrLi_OV_ul01_2x2_400x400_800x800_10_0089.json') 
    #dooul('Pa_plate_PaDrLi_OV_ul01_4x4_200x200_400x400_10_0089.json') 
    #dooul('Pa_plate_PaDrLi_OV_ul01_1x1_500x500_1500x1500_10_0089.json')   
    dooul('Pa_plate_PaDr_OV_ul01_8x8_94x88_188x176_10_0087.json',
        pos_s='Pa_plate_PaDr_OV_ul01_2x2_375x350_750x700_10_0087.json',
        dsfrag='Pa_plate_PaDr_OV')
    #dooul('Pa_plate_PaDr_OV_ul01_1x1_500x500_1500x1500_10_0087.json')     

def raw_hg169_day2_2():

            
    # damage hydrocerussite, cerussite and lead white only 50um sections
    
    lo_flux()
    EXPO.expt = 0.010
    dooul('ermanno_C50_mapt1_ul01_1x1_25x50_50x100_10_0070.json')
    dooul('ermanno_HC50_mapt1_ul01_1x1_25x50_50x100_10_0062.json')
    dooul('ermanno_LW50_mapt1_ul01_1x1_25x50_50x100_10_0078.json')
    
    lo_flux()
    EXPO.expt = 0.10
    dooul('ermanno_C50_mapt2_ul01_1x1_25x50_50x100_100_0071.json')
    dooul('ermanno_HC50_mapt2_ul01_1x1_25x50_50x100_100_0063.json')  
    dooul('ermanno_LW50_mapt2_ul01_1x1_25x50_50x100_100_0079.json')
     
    lo_flux()
    EXPO.expt = 1
    dooul('ermanno_C50_mapt3_ul01_1x1_25x50_50x100_1000_0072.json')
    dooul('ermanno_HC50_mapt3_ul01_1x1_25x50_50x100_1000_0064.json')  
    dooul('ermanno_LW50_mapt3_ul01_1x1_25x50_50x100_1000_0080.json')
    
    hi_flux()
    EXPO.expt = 0.010 
    dooul('ermanno_C50_mapt4_ul01_1x1_25x50_50x100_10_0073.json')
    dooul('ermanno_HC50_mapt4_ul01_1x1_25x50_50x100_10_0065.json')  
    dooul('ermanno_LW50_mapt4_ul01_1x1_25x50_50x100_10_0081.json')
    
    hi_flux()
    EXPO.expt = 0.10 
    dooul('ermanno_C50_mapt5_ul01_1x1_25x50_50x100_100_0074.json')
    dooul('ermanno_HC50_mapt5_ul01_1x1_25x50_50x100_100_0066.json')  
    dooul('ermanno_LW50_mapt5_ul01_1x1_25x50_50x100_100_0082.json')
    
    hi_flux()
    EXPO.expt = 1
    dooul('ermanno_C50_mapt6_ul01_1x1_25x50_50x100_1000_0075.json')
    dooul('ermanno_HC50_mapt6_ul01_1x1_25x50_50x100_1000_0067.json')  
    dooul('ermanno_LW50_mapt6_ul01_1x1_25x50_50x100_1000_0083.json')
    
         
         
    lo_flux()
    doloop('ermanno_C50_point1_0076.json',10000, 0.01)
    doloop('ermanno_HC50_point1_0068.json',10000, 0.01)
    doloop('ermanno_LW50_point1_0084.json',10000, 0.01)   
    
    hi_flux() 
    doloop('ermanno_C50_point2_0077.json',10000, 0.01)
    doloop('ermanno_HC50_point2_0069.json',10000, 0.01)
    doloop('ermanno_LW50_point2_0085.json',10000, 0.01)   
    
    
    

def hg169_day2_2():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_2()
        sc()
    finally:
        sc()
        sc()
        sc()



def hg169_day2_3():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_3()
        sc()
    finally:
        sc()
        sc()
        sc()


def hg169_day2_4():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_4()
        sc()
    finally:
        sc()
        sc()
        sc()


def hg169_day3_2():
    try:
        so()
        time.sleep(1)
        raw_hg169_day3_2()
        sc()
    finally:
        sc()
        sc()
        sc()
   
    
def hg169_day3_3():
    try:
        so()
        time.sleep(1)
        raw_hg169_day3_3()
        sc()
    finally:
        sc()
        sc()
        sc() 
    
    
    
