print('load 1')
import sys
sys.path.insert(0,'/data/visitor/ihsc1724/id13/XXPROCESS')
import os, signal
import time
import pprint
import traceback
from collections import OrderedDict as Odict
from filecommunication import BaseHandler, FileCommunication



class Timeout(object):

    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()

def mgallmpx(rois=True, usexmap=True):
    MG_EH2.set_active()
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:acq_time_2')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct22')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct24')
    MG_EH2.enable('eiger:im*')
    eiger.camera.energy_threshold=13.0*0.5
    eiger.proxy.saving_index_format="%06d"
    eiger.proxy.saving_format="HDF5BS"

    if rois:
        MG_EH2.enable('eiger*:roi_counters:roi1_*')

def get_detstatus(det):
    return _get_detstatus(det)[0]

def _get_detstatus(det):
    pid = os.getpid()
    cmd = 'ps -u opid13 > eiger_python3isamess'
    print(f'executing [{cmd}]')
    os.system(cmd)
    print(f'executed [{cmd}]')
    
    with open('eiger_python3isamess', 'r') as f:
        s = f.read()
    print(f'eiger_python3isamess:\n{s}\n')
    ll = s.split('\n')
    resll = []
    for l in ll:
        if 'python' in l:
            resll.append(l)
    leng = len(resll)
    print(f'found {leng} opid13 pytohn processes')
    if not leng in (1,2):
        raise RunTimeError(f'There should be 1 or 2 python processes! - found {leng}')
    if 2 == leng:
        for l in resll:
            print('x1', l)
            w = l.split()
            found_pid = int(w[0])
            print('x2', found_pid)
            if found_pid == pid:
                pass
            else:
                res_pid = found_pid
        print('RUNNING', res_pid)
        return ('RUNNING', res_pid)
    else:
        print('STOPPED', -1)
        return ('STOPPED', -1)

def start_det(det):
    cmd = f'LimaCCDs eiger1 > eiger_output.log &'
    print(f'executing [{cmd}]')
    os.system(cmd)
    print(f'executing [{cmd}] - after os.system')


def stop_det(det):
    (status, pid) = _get_detstatus(det)
    if 'STOPPED' == status:
        print('nothing found to be stopped ...')
        return True
    else:
        print('okay - trying to stop eiger server ...')
    os.kill(pid, signal.SIGTERM)
    time.sleep(15)
    (status, pid) = _get_detstatus(det)
    if 'STOPPED' == status:
        print('eiger server successfully terminated ...')
        return True
    else:
        print('soft kill did not work - trying again')
    os.kill(pid, signal.SIGKILL)
    time.sleep(35)
    (status, pid) = _get_detstatus(det)
    if 'STOPPED' == status:
        print('eiger server successfully killed ...')
        return True
    else:
        return False
        
    

def reset_eiger():

    det1 = 'eiger'
    stop_flg = stop_det(det1)
    if stop_flg:
        print('reset_eiger: successfully stopped')
        print('reset_eiger: trying to start eiger server ...')
        start_det(det1)
        print('reset_eiger: start attempt finished')
    else:
        print('reset_eiger: stopping failed')
    print('reset_eiger: verifying that eiger server is started ...')
    res_flg = get_detstatus(det1)
    print(f'reset_eiger: eiger server started: {res_flg}')
    return res_flg

class EigerStarter(BaseHandler):

    def __init__(self):
        super().__init__()

    def action(self, instruct):
        print('instruct', instruct)
        for (a,v) in instruct:
            print('instruction:', (a,v))
            if 'noop' == a:
                print('got noop ...')
                pass
            elif 'restart_eiger' == a:
                print(f'ieiger restart - max 2 attempts ...')
                res_flg = reset_eiger()
                print(f'attempt 1: eiger successfully stopped: {res_flg}')
                if res_flg:
                    return
                res_flg = reset_eiger()
                print(f'attempt 2: eiger successfully stopped: {res_flg}')
            else:
                print(f'instruction: {(a,v)} ignored.')

COM_FNAME = '/data/visitor/ihsc1724/id13/XXPROCESS/eiger.asy'

def com_receiver():
    hdlr = EigerStarter()
    fcom = FileCommunication(COM_FNAME)
    fcom.set_receive_handler(hdlr)
    fcom.run_receive_server(5.0)

def com_sender(theid, cmd, value=True):
    fcom = FileCommunication(COM_FNAME)
    instructions = list()
    instructions.append(('id', int(theid)))
    instructions.append((cmd, value))
    print(f'sending instructions:\n{instructions}')
    fcom.send(theid, instructions)


if __name__ == '__main__':
    args = sys.argv[1:]
    if args[0] == 'send':
        com_sender(int(args[1]), args[2])
    elif args[0] == 'rec':
        com_receiver()
    else:
        print (args)

