print('ma6424_infra - load 9')

import os
from os import path
from time import sleep

def dummy_kmap(*p):
    print('dummy_kmap:')
    (mot0, ll0, ul0, n0, mot1, ll1, ul1, n1, expt) = p
    print(f'dummy_kmap: {mot0.name} geo={(ll0, ul0, n0)} {mot1.name} geo={(ll1, ul1, n1)} {expt:7.4f}')
    sleep(1)

def real_kmap(*p):
    kmap.dkmap(*p)
    try:
        sleep(1)
        elog_plot(scatter=True)
    except:
        print('elog_plot failure ...')


# SIMULATION
#KMAP_CMD = dummy_kmap

# REAL THING
KMAP_CMD = real_kmap

# MULTIPLE STARTS BECAUSE OF FAILURE
START_KEY = 'a'


def ma6424_check_stop():
    return path.exists('./STOP')

def ma6424_clear_stop():
    try:
        os.unlink('./STOP')
    except:
        print('STOP is not set.')

def scan_seq_1(corename, n_hires):
    name = f'{START_KEY}_{corename}'
    ma6424_clear_stop()
    initial_nnypos = nny.position
    try:
        print('======== scan_seq_1: {name}')
        stp(name)
        newdataset(name)
        if ma6424_check_stop():
            raise RuntimeError('ma6424_check_stop')
        KMAP_CMD(nnp2, -30, 30, 300, nnp3, 0, 120, 120, 0.01)
        hires_nnypos = initial_nnypos + 0.045
        mv(nny, hires_nnypos)
        for i in range(n_hires):
            print(f'    == hires_nnypos: {name}:{i}')
            mv(nny, hires_nnypos + i*0.012)
            if ma6424_check_stop():
                raise RuntimeError('ma6424_check_stop')
            KMAP_CMD(nnp2, 0, 10, 100, nnp3, 0, 120, 300, 0.01)
    finally:
        print(f'    == moving nny back to initial pos: {initial_nnypos}')
        mv(nny, initial_nnypos)
    print(f'    == done.')


def do_a_lot():
    #for p in ['pos1', 'pos2']:
    for p in ['x50_region1', 'x50_region2', 'x50_region3', 'x50_region4']:
        rstp(p)
        scan_seq_1(p, 2)


def ma6424_main():
    try:
        so()
        do_a_lot()
    finally:
        sc()
        sc()
        sc()
