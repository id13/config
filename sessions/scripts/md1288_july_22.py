print('load md1288 ver 6')
prefix = 'd'
def md1288_july_22_day():
    mgeig()
    fshtrigger()
    so()
    try:
        gopos('K4_scan_start_pos')
        dn = f"K4_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(7.6/2,95*4,6.56/2,82*4,0.01,2,2,5)
        enddataset()
        
        gopos('LD_scan_start_pos')
        dn = f"LD_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(6.16/2,77*4,5.6/2,70*4,0.01,2,2,5)
        enddataset()
        
        gopos('K5_scan_start_pos')
        dn = f"K5_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(8.48/2,106*4,7.2/2,90*4,0.01,2,2,5)
        enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()


step_size = 0.005   
ROI_scan = [
'K4_ROI1','K4_ROI2','K4_ROI3',
'LD_ROI1','LD_ROI2','LD_ROI3',
'K5_ROI1','K5_ROI2','K5_ROI3',
]

ROI_start_pos_coord = [
[12.428,6.79],[10.834,4.59],[16.652,4.59],
[-9.25,-0.27],[-8.693,0.93],[-13.464,0.93],
[-43.985,3.26],[-37.915,2.98],[-44.225,5.14],
]

ROI_ninterval = [
[351,176],[351,200],[255,200],
[239,128],[223,200],[175,200],
[176,208],[351,200],[224,200],
]

roi_prefix = 'a' 

def md1288_july_22_night():
    mgeig()
    fshtrigger()
    so()
    try:
        for num,_ in enumerate(ROI_scan):
            if 'K4' in _:
                gopos('K4_scan_start_pos')
            elif 'LD' in _:
                gopos('LD_scan_start_pos')
            elif 'K5' in _:
                gopos('K5_scan_start_pos')
            else:
                print('focus of sample is not aligned')
                return
             
            umv(ustry,ROI_start_pos_coord[num][0])
            umv(ustrz,ROI_start_pos_coord[num][1])
            dn = f"{_}_{roi_prefix}"
            distance_y = step_size*ROI_ninterval[num][0]
            distance_z = step_size*ROI_ninterval[num][1]
            num_interval1 = ROI_ninterval[num][0]
            num_interval2 = ROI_ninterval[num][1]
                        
            newdataset(dn)
            dkpatchmesh(distance_y,num_interval1,
                        distance_z,num_interval2,
                        0.01,1,1,5)
            enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()
