def doc_dmesh_zrot(mid_fix, nscans, zll, zul, znitv, vyll, vyul, vynitv, expt, rotll, rotul, rotnitv):
    start_key = 'c'
    try:
        for i in range(nscans):
            newdataset(f'{start_key}_perov_{mid_fix}_{i:04d}')
            vrg.dmesh_z_rot_vy(zll, zul, znitv, vyll, vyul, vynitv, expt, rotll, rotul, rotnitv)
    finally:
        sc()
        sc()
        sc()



import time

def doo_log(s, lfn):
    s = f'==== entry: {time.time()}\n{s}'
    with open(lfn, 'a') as f:
        f.write(s)
        print(s)
         
def our_vrg_exec(vrg, mthd, params):
    ps = tuple(params)
    print(f'    executing vrg.{mthd}{ps}')
    print('executing for real ...')
    mthd = getattr(vrg, mthd)
    mthd(*params)
    
def our_dmy_exec(vrg, mthd, params):
    ps = tuple(params)
    print(f'    executing vrg.{mthd}{ps}')
    
use_exec = our_vrg_exec

def make_visual_params(rot_mesh_params):
    (z_ll, z_ul, znitv, y_ll, y_ul, ynitv, expt, r_ll, r_ul, rnitv) = rot_mesh_params
    
    assert y_ll == -y_ul
    yhw = y_ul
    assert z_ul > 0
    assert z_ll == 0
    zw = z_ul
    
    y_nitv_v = (int(yhw / 0.1)+1)*2
    z_nitv_v = int(zw/0.1) + 1
    yhw_v = y_nitv_v * 0.1 * 0.5
    zw_v = z_nitv_v * 0.1
    return (y_nitv_v,z_nitv_v,yhw_v ,zw_v)

def doo_visual(seq_id, vrg, logfile_name, start_key, mid_fix, burn_time, *rot_mesh_params):
    print('starting doo_visual ...')
    sc()
    MG_EH2.enable('*uvlm2:image*')
    MG_EH2.disable('*eiger*')
    seq_fix = 'visual'
    
    vparams = (y_nitv_v,z_nitv_v,yhw_v ,zw_v) = make_visual_params(rot_mesh_params)
    dsname = f'{start_key}_{mid_fix}_{seq_fix}_{seq_id}'
    
    newdataset(dsname)
    mesh_params = (0, zw_v, z_nitv_v, -yhw_v, yhw_v, y_nitv_v, rot_mesh_params[6])
    log_str = f'''\
function: doo_visual:
    start_key = {start_key}
    seq_fix = {seq_fix}
    seq_id = {seq_id}
    new_dataset({dsname})
    rot_mesh_params = {rot_mesh_params}
    mesh_params = {mesh_params}
    y_nitv_v,z_nitv_v,yhw_v ,zw_v = {vparams}
    
'''
    print(log_str)
    doo_log(log_str, logfile_name)
    #vrg.dmesh_z_vy(*mesh_params)
    use_exec(vrg, 'dmesh_z_vy', mesh_params)
    
    doo_log('    scan done.\n', logfile_name)
    
def doo_burn(seq_id, vrg, logfile_name, start_key, mid_fix, burn_time, *rot_mesh_params):
    print('starting doo_burn ...')
    so()
    MG_EH2.disable('*uvlm2:image*')
    MG_EH2.disable('*eiger*')
    seq_fix = 'burn'
    (y_nitv_v,z_nitv_v,yhw_v ,zw_v) = make_visual_params(rot_mesh_params)
    y_nitv_v = y_nitv_v *2
    z_nitv_v = z_nitv_v * 2
    vparams = (y_nitv_v,z_nitv_v,yhw_v ,zw_v)
    dsname = f'{start_key}_{mid_fix}_{seq_fix}_{seq_id}'
    
    newdataset(dsname)
    mesh_params = (0, zw_v, z_nitv_v, -yhw_v, yhw_v, y_nitv_v, burn_time)
    log_str = f'''\
function: doo_burn:
    start_key = {start_key}
    seq_fix = {seq_fix}
    seq_id = {seq_id}
    new_dataset({dsname})
    rot_mesh_params = {rot_mesh_params}
    mesh_params = {mesh_params}
    y_nitv_v,z_nitv_v,yhw_v ,zw_v = {vparams}
    
'''
    print(log_str)
    doo_log(log_str, logfile_name)
    #vrg.dmesh_z_vy(*mesh_params)
    use_exec(vrg, 'dmesh_z_vy', mesh_params)
    
    doo_log('    scan done.\n', logfile_name)
    sc()


    

def doo_xrays(seq_id, vrg, logfile_name, start_key, mid_fix, burn_time, *rot_mesh_params):
    print('starting doo_xrays ...')
    try:
        MG_EH2.disable('*uvlm2:image*')
    except:
        print('cannot disable uvlm2:image')
    MG_EH2.enable('*eiger:i*')
    so()
    seq_fix = 'xrays'
    dsname = f'{start_key}_{mid_fix}_{seq_fix}_{seq_id}'
    
    newdataset(dsname)
    mesh_params = rot_mesh_params
    log_str = f'''\
function: doo_xrays:
    start_key = {start_key}
    seq_fix = {seq_fix}
    seq_id = {seq_id}
    new_dataset({dsname})
    rot_mesh_params = {rot_mesh_params}
    mesh_params = {rot_mesh_params}
    
'''
    print(log_str)
    doo_log(log_str, logfile_name)
    #vrg.dmesh_z_rot_vy(*mesh_params)
    use_exec(vrg, 'dmesh_z_rot_vy', mesh_params)
    
    doo_log('    scan done.\n', logfile_name)
    sc()
    


def everything(vrg, *gen_params):
    logfile_name, start_key, mid_fix, burn_time = gen_params[:4]
    assert not path.exists(logfile_name)
    with open(logfile_name, 'w') as f:
        log_s = f'############################ starting scan sequence: {mid_fix} at {time.time()}\n'
        print(log_s)
        f.write(log_s)
        
    (z_ll, z_ul, znitv, y_ll, y_ul, ynitv, expt, r_ll, r_ul, rnitv) = gen_params[4:]
    
    oper_sequence = enumerate(
       # [doo_visual, doo_xrays, doo_visual, doo_burn, doo_visual]
        [doo_visual, doo_xrays, doo_visual]
    )

    
    for (i, f) in oper_sequence:
        f(i, vrg, *gen_params)
  
    with open(logfile_name, 'a') as f:
        log_s = f'############################ san sequence: {mid_fix} finished at {time.time()}\n'
        print(log_s)
        f.write(log_s)
     


'''
def doc_calib_by_burn(zw, yhw, expt, expt_burn, rot, rot_nitv): 
    y_nitv_v = (int(yhw / 0.1)+1)*2
    z_nitv_v - int(zw) /0.1 + 1
    yhw_v = y_nitv_v * 0.1 - 0.5
    zw_v = z_nitv_v * 0.1
    
    start_key = 'j'
    
    
    for i in range(4):
        newdataset(f'{start_key}_perov_day_scan_id{i}_{i:04d}')
        with open(f'{start_key}_perov_day_scan_id{i}_doc_file_{i:04d}', 'w') as doc:
            
            
                       
        if (i != 1) and (i != 3):
            vrg.dmesh_z_vy(0, zw, z_nitv_v, -yhm, yhm, y_nitv_v, expt)
        elif i == 1:
            so()
            so()
            so()
            vrg.dmesh_z_rot_vy(0, zw_v, z_nitv_v, -yhw_v, yhw_v, y_nitv_v, expt, -int(rot)*0.5, int(theta)*0.5, rot_nitv)
            sc()
            sc()
            sc()
        else:
            so()
            so()
            so()
            vrg.dmesh_z_vy(0, zw, 2, -yhm, yhm, 2, expt_burn)
            sc()
            sc()
            sc()
'''
         
            
    

### DOC 


def doo_one_intensity(logf, vrg, u18tt, intensity, exp_times, start_key):
    u18tt.goto_intensity(intensity)
    
    intens_s = str(int(intensity))
    dsname = f'{start_key}_exposure_series_{intens_s}'
    doo_log(f'''
    dsname: {dsname}
    exptimes: {exp_times}
    intensity: {intensity}
''', logf) 
    print('desname =', dsname)
#    return
    newdataset(dsname)
    for expt in exp_times:
        vrg.mvr_z(0.040)
        MG_EH2.disable('*eiger:i*')
        MG_EH2.enable('*uvlm2:i*')
        print('observing before loopscan')
        loopscan(5, 0.02, sleep_time=1)
        MG_EH2.enable('*eiger:i*')
        MG_EH2.disable('*uvlm2:i*')
        so()
        print('burning')
        loopscan(50*10, expt)
        sc()
        MG_EH2.disable('*eiger:i*')
        MG_EH2.enable('*uvlm2:i*')
        print('observing after burning')
        loopscan(60, 0.02, sleep_time=1)
        vrg.mvr_z(0.030)
        MG_EH2.enable('*eiger:i*')
        MG_EH2.disable('*uvlm2:i*')
        so()
        print('measuring the dmesh')
        dmesh(ustrz, 0, 0.02, 4, ustry, -0.01, 0.01, 2, 10*expt)
        sc()
    doo_log(f'    dsname: {dsname} done.', logf)
    
        

def exposure_series(vrg, u18tt):
    try:
        sc()
        logf = 'exposure_series_002.log'
        start_key = 'f'
        log_s = f'############################ starting exposure sequence at {time.time()}\n'
        with open(logf, 'w') as f:
            f.write(log_s)
    
        exp_times = (0.02, 0.05)
        intensities = (5000, 10000, 15000, 20000, 30000, 50000)
        for intens in intensities:
            doo_one_intensity(logf, vrg, u18tt, intens, exp_times, start_key)
    finally:
        pass
        
        

def sat_scan2():
    dmesh_z_rot_vy('zscan2', 1, 0, 0.03, 1, -0.1, 0.1, 2, 0.02, -0.5, 0.5, 2)
    
    
def sat_scan2_visual():
    dmesh_z_rot_vy('zscan2', 1, 0, 0.03, 1, -0.1, 0.1, 2, 0.02, -0.5, 0.5, 2)


def sat_scan2_visual(vrg):
    #everything(vrg, 'log_sat_scan3_f', 'f', 'sat_scan3', 40, 0, 0.2,40, -0.1, 0.1, 20, 0.02, -0.5,0.5,10)
    
    #everything(vrg, 'log_sat_night_dense_scan4_c', 'c', 'sat_scan4_c', 40, 0, 0.15, 50, -0.2, 0.2, 40, 0.02, -0.5,0.5,10)
    
    vrg.mvr_z(0.2)
    everything(vrg, 'log_sat_night_dense_omscan_scan5_e', 'e', 'sat_scan5_e', 40, 0, 0.15, 50, -0.2, 0.2, 40, 0.02, -1,1,40)
    

def sunday_night(vrg, u18tt):
    try:
        exposure_series(vrg, u18tt)
        so()
        umv(u18, 6.24)
        vrg.mvr_z(0.5)
        everything(vrg, 'log_sun_night_large_scan5_f', 'f', 'sun_scan1_f', 40, 0, 0.6, 200, -0.4, 0.4, 80, 0.02, 0,0.5,1)
    finally:
        sc()
        sc()
        sc()
        

def kmap_rot_scan(kmap_pars, rot_ll, rot_step, rot_nitv):
    start_rot_pos = usrotz.position
    for i in range(0,rot_nitv+1):
        curr_rot_pos = start_rot_pos + rot_ll + i*rot_step
        print('='*50)
        print(f'cycle = {i}: rot position = {curr_rot_pos}')
        mv(usrotz, curr_rot_pos)
        dkmapyz_2(*kmap_pars)
        
def doo_some_scan(params, funcs, posnum, scantipe, param_set_idx, start_key):
    gopos('pos%1d' % posnum)
    newdataset('%s_pos%1d_%s' % (start_key, posnum, scantipe))
    sub_pars = params[scantipe] 
    parset = sub_pars[param_set_idx-1]
    funcs[scantipe](*parset)
    enddataset()
    
def test_oversamp(*p):
    print('    ------------------ oversamp', p)
def test_ome(*p):
    print('    --------------------------------------------------ome', p)
    
def monday_night():
    FUNC_DICT = dict(
        ome = kmap_rot_scan, oversamp = dkmapyz_2
    )
    #    ome = test_ome, oversamp = test_oversamp
    
    
    PARAMS = dict(
        ome = (
            ((-0.06,0.06, 40, 0, 0.12, 40, 0.02, 5.0), -1,0.1,20),
            ((-0.06,0.06, 40, 0, 0.12, 40, 0.02, 5.0), -0.5,0.5,20),
        ), 
        oversamp = (
            (0,0.1, 200, 0, 0.1, 200, 0.01, 5.0),
            (-0.1,0.1, 400, 0, 0.2, 400, 0.01, 5.0),
        )
    )
    start_key = 'c'
    the_input_sets = (
        (5,'ome', 1, start_key),
        (6,'oversamp', 1, start_key),
        (7,'ome', 2, start_key),
        (8,'oversamp', 2, start_key),
        (9,'ome', 1, start_key),
        (10,'oversamp', 1, start_key),
        (11,'ome', 2, start_key),
        (12,'oversamp', 2, start_key),
        (13,'ome', 1, start_key),
        (14,'oversamp', 1, start_key),
        (15,'ome', 2, start_key),
        (16,'oversamp', 2, start_key),
        (17,'ome', 1, start_key),
        (18,'oversamp', 1, start_key),
        (19,'ome', 2, start_key),
    )
    for iset in the_input_sets:
        doo_some_scan(*((PARAMS, FUNC_DICT)+iset))
