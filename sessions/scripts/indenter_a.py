import tango
import numpy as np
from time import sleep

class error(Exception): pass

def make_inda():
    return IndenterA()

class IndenterA(object):

    def __init__(self):
        self._inda = indenter.indenter

    # positioning
    #

    def getpos(self):
        return self._inda.position

    def zeropos(self):
        if not self.isready():
            raise error('indenter actuator not ready')
        self._inda.ZeroPosition()

    def ismoving(self):
        return self._inda.State() == tango.DevState.MOVING

    def isready(self):
        return self._inda.State() == tango.DevState.ON

    def start_move(self, pos):
        pos = float(pos)
        self._inda.GoPosition(pos)

    def sync_move(self, pos, upd_interv=.2):
        try:
            if not self.isready():
                raise error('indenter actuator not ready')
    
            self.start_move(pos)
    
            while self.ismoving():
                # print(f'moving to {pos} - current pos = {self.getpos()}')
                sleep(upd_interv)
    
            if self.isready():
                return True
            else:
                return False
        except KeyboardInterrupt:
            self._inda.stop()
            print('stopped by user')

    def stepin(self):
        if not self.isready():
            raise error('indenter actuator not ready')
        self._inda.stepin()

    def stepout(self):
        if not self.isready():
            raise error('indenter actuator not ready')
        self._inda.stepout()

    # force
    #

    def getforce(self):
        for i in range(10):
            try:
                return self._inda.force
            except:
                print(f'fail - force read cycle {i}')
                sleep(0.05)
        return -100.0

    def safer_getforce(self):
        values = [self.getforce() for i in range(5)]
        res = float(np.median(values))
        return res

    def zeroforce(self):
        if not self.isready():
            raise error('indenter actuator not ready')
        self._inda.zeroforce()

    @property
    def position(self):
        return self.getpos()

    @position.setter
    def position(self, pos):
        self.sync_move(pos)

    @property
    def force(self):
        return self.safer_getforce()

    @property
    def raw_force(self):
        return self.getforce()

    def create_posaxis(self, name):
        return SoftAxis(name, self, position='position', move='position', tolerance=0.25)

    def create_positioncounter(self, name):
        return SoftCounter(self, 'position', name=name)

    def create_forcecounter(self, name):
        return SoftCounter(self, 'force', name=name)

    def create_raw_forcecounter(self, name):
        return SoftCounter(self, 'raw_force', name=name)
