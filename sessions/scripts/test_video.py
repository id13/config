import os
from os import path
from collections import OrderedDict as Odict
from scipy import ndimage as ndi
import h5py
from id13.scripts.video_image import video_image
load_script('udo_scanaux')

#
# utils
#
def lstree(dapth, dirsonly=False):
    if dirsonly:
        _cmd = 'tree -d'
    else:
        _cmd = 'tree'
    cmd = f'{_cmd} {dapth}'
    res=subprocess.run(cmd, shell=True, capture_output=True, text=True)
    return res.stdout

def provide_dir(dapth):
    print(f'trying to crete: {dapth} ...')
    try:
        os.mkdir(dapth)
        print('done.')
    except FileExistsError:
        print(f'directory exists already - nothing done.')

#
# Nomenclature:
#
# pix_<name> : object in image coordinates (x,y)
# (typically x - horizontal, y - vertical)
#
# dqpix_<name> : daiquiri coordinates (dqx, dqy)
# (coordinated from the center of the frame)
#
# pn_<name> : object in numpy coordinates (row, column) of an image matrix
#


#
# coords
#
def vid_dqpix_to_pix(dqpix, shape):
    (dqx, dqy) = dqpix
    return (dqx+shape[1]/2, dqy+shape[0]/2)

def vid_pix_to_dqpix(pix, shape):
    (x, y) = pix
    return (x-shape[1]/2, y-shape[0]/2)

def vid_cooswap(x):
    (a,b) = x
    return (b,a)

vid_pix_to_pn = vid_cooswap
vid_pn_to_pix = vid_cooswap


#
# video
#

def vid_get_image(grayscale=True):
    return video_image(CAMERA_S, grayscale=grayscale)

def vid_get_shape():
    return vid_get_image().shape

#
# defaults
#

CAMERA_S = 'nvlm2'
DQPIX_BEAM = (-25.0, -70.0)
PIX_BEAM = (0,0)
#PIX_BEAM = vid_dqpix_to_pix(DQPIX_BEAM)
#PN_BEAM  = vid_pix_to_pn(PIX_BEAM)

#
# h5
#
def make_h5val(h5group, valname, val=0, dtype=np.float64):
    ds = h5group.create_dataset(valname, (1,), dtype)
    ds[0] = val

def get_h5val(h5group, valname, val=0, dtype=np.float64):
    ds = h5group[valname]
    assert ds.shape == (1,)
    return ds[0]

def set_h5val(h5group, valname, val):
    ds = h5group[valname]
    assert ds.shape == (1,)
    ds[0] = val
    h5group[valname][:] = val

def make_h5frameseries(h5group, name, n, shape, dtype='int64'):
    series_shape = (n, shape[0], shape[1])
    return h5group.create_dataset(name, series_shape, dtype)

def make_h5arr1d(h5group, name, n, dtype='float64'):
    shape = (n,)
    h5group.create_dataset(name, shape, dtype)

class VideoFocusSeriesInMemData(object):

    def __init__(self, iomgr, videoview, pfname, groupname, npoints, fmot_name, other_mot_names,
        hormot_name=None, vertmot_name=None, rotzmot_name=None):
        self.iomgr = iomgr
        self.videoview = videoview
        self.pfname = pfname
        self.groupname = groupname
        self.npoints = npoints
        self.fmot_name = fmot_name
        self.hormot_name = hormot_name
        self.vertmot_name = vertmot_name
        self.rotzmot_name = rotzmot_name
        self.other_mot_names = other_mot_names
        self._initialize()

    def get_ofile_fh5(self):
        return self.iomgr.get_ofile(self.pfname, self.groupname)

    def make_vbuffer_arr(self):
        return np.zeros(self.vdata_shape, dtype=self.vdata_dtype)

    def _initialize(self):
        iomgr = self.iomgr
        vv = self.videoview
        n = self.npoints
        (s0,s1) = vv.shape
        # xxx todo: use the context manager ...
        #
        fh5 = iomgr.make_ofile(self.pfname, self.groupname)
        try:
            vdata = make_h5frameseries(fh5, 'vdata', n, vv.shape)
            self.vdata_shape = vdata.shape
            self.vdata_dtype = vdata.dtype
            eval = fh5.create_group('eval')

            # focmot
            #
            focmot = fh5.create_group('focmot')
            focmot.attrs['fmot_name'] = self.fmot_name
            make_h5val(focmot, 'ini_fpos')
            fmot_posdata = make_h5arr1d(focmot, self.fmot_name, n)

            # hormot
            #
            if not None is self.hormot_name:
                hormot = fh5.create_group('hormot')
                hormot.attrs['hormot_name'] = self.hormot_name
                make_h5val(hormot, 'ini_horpos')
                hormot_posdata = make_h5arr1d(hormot, self.hormot_name, n)
            
            # vertmot
            #
            if not None is self.vertmot_name:
                vertmot = fh5.create_group('vertmot')
                vertmot.attrs['vertmot_name'] = self.vertmot_name
                make_h5val(vertmot, 'ini_vertpos')
                vertmot_posdata = make_h5arr1d(vertmot, self.vertmot_name, n)
            
            # rotzmot
            #
            if not None is self.rotzmot_name:
                rotzmot = fh5.create_group('rotzmot')
                rotzmot.attrs['rotzmot_name'] = self.rotzmot_name
                make_h5val(rotzmot, 'ini_rotzpos')
                rotzmot_posdata = make_h5arr1d(rotzmot, self.rotzmot_name, n)

            for nm in self.other_mot_names:
                make_h5val(fh5, nm)
        finally:
            fh5.close()

class VideoFocusSeries(object):

    LEGAL_MOTNAMES = set(('nnx', 'nny', 'nnz', 'nnp1', 'nnp2', 'nnp3'))

    def __init__(self, focmot, iomgr, videoview, pfname, groupname, npoints, foc_stepsize,
        other_mot_names, hormot=None, vertmot=None, rotzmot=None):
        self.iomgr = iomgr
        self.videoview = videoview
        self.pfname = pfname
        self.groupname = groupname
        self.group_dapth = path.join(iomgr.root_dapth, groupname)
        print(f'trying to crete: {self.group_dapth} ...')
        provide_dir(self.group_dapth)
        self.npoints = npoints
        self.foc_stepsize = foc_stepsize
        self.foc_mot = focmot
        self.hor_mot = hormot
        self.vert_mot = vertmot
        self.rotz_mot = rotzmot
        self.other_mot_names = other_mot_names
        for nm in ('hor_mot', 'vert_mot', 'rotz_mot'):
            mot = getattr(self, nm)
            if None is mot:
                setattr(self, f'{nm}_name', None)
            else:
                setattr(self, f'{nm}_name', mot.name)
        self.inmem_data = VideoFocusSeriesInMemData(
            iomgr, videoview, pfname, groupname, npoints, self.foc_mot.name, other_mot_names,
            hormot_name=self.hor_mot_name, vertmot_name=self.vert_mot_name, rotzmot_name=self.rotz_mot_name)

    def get_eval_fh5(self):
        fh5 = self.inmem_data.get_ofile_fh5()
        eval = fh5['eval']
        return eval, fh5

    def get_imagestack_v_arr(self):
        o_fh5 = self.inmem_data.get_ofile_fh5()
        try:
            vdata = o_fh5['vdata']
            return np.array(vdata, copy=False)
        finally:
            o_fh5.close()

    def acq_series(self):
        pfname = self.pfname
        groupname = self.groupname
        iomgr = self.iomgr
        videoview = self.videoview
        foc_mot = self.foc_mot
        assert foc_mot.name == 'nnx'
        ini_pos = foc_mot.position
        imm = self.inmem_data
        o_fh5 = imm.get_ofile_fh5()
        set_h5val(o_fh5['focmot'], 'ini_fpos', ini_pos)
        try:
            n = self.npoints
            stp = self.foc_stepsize
            foc_offset = -stp*(int(n)//2)
            start_pos = ini_pos + foc_offset
            end_pos   = start_pos + (n-1)*stp
            pos_arr = np.linspace(start_pos, end_pos, n)
            o_fh5['focmot'][foc_mot.name][:] = pos_arr
            vbuffer_arr = imm.make_vbuffer_arr()
            for i in range(n):
                mv(foc_mot, pos_arr[i])
                vframe = videoview.acq()
                vbuffer_arr[i] = vframe
            o_fh5['vdata'][:] = vbuffer_arr
            
        finally:
            o_fh5.close()
            mv(foc_mot, ini_pos)

    def setup_generic_mot(role_name):
        mot = getattr(self, f'{rolename}_mot)')
        assert mot.name in self.LEGAL_MOTNAMES
        ini_pos = mot.position
        imm = self.inmem_data
        o_fh5 = imm.get_ofile_fh5()
        #try:
        #    set_h5val(o_fh5[&&&&&&&'focmot'], 'ini_fpos', ini_pos)


    def setup_generic(self, npoints_generic):
        pfname = self.pfname
        groupname = self.groupname
        iomgr = self.iomgr
        videoview = self.videoview

        foc_mot = self.foc_mot
        assert foc_mot.name == 'nnx'
        ini_pos = foc_mot.position
        imm = self.inmem_data
        o_fh5 = imm.get_ofile_fh5()
        set_h5val(o_fh5['focmot'], 'ini_fpos', ini_pos)
        try:
            n = self.npoints
            stp = self.foc_stepsize
            foc_offset = -stp*(int(n)//2)
            start_pos = ini_pos + foc_offset
            end_pos   = start_pos + (n-1)*stp
            pos_arr = np.linspace(start_pos, end_pos, n)
            o_fh5['focmot'][foc_mot.name][:] = pos_arr
            vbuffer_arr = imm.make_vbuffer_arr()
            for i in range(n):
                mv(foc_mot, pos_arr[i])
                vframe = videoview.acq()
                vbuffer_arr[i] = vframe
            o_fh5['vdata'][:] = vbuffer_arr
            
        finally:
            o_fh5.close()
            mv(foc_mot, ini_pos)


    def acq_point_generic(self, fpos=None, horpos=None, vertpos=None, rotzpos=None):
        # &&&&
        pass
    
class SimpleAutofocusDetect(object):

    def __init__(self, vfocseries, itroi=None):
        self.vfocseries = vfocseries
        self._imvs_arr = None
        self.crop(itroi=itroi)
        self.eval_stacks = Odict()

    def get_imvs_arr(self):
        if None is self._imvs_arr:
            imvs_arr = self.vfocseries.get_imagestack_v_arr()
            self.npoints = imvs_arr.shape[0]
            self._imvs_arr = imvs_arr
        return self._imvs_arr

    def crop(self, itroi=None):
        # xxx todo : chain the cropping by adjusting the offset
        #
        imvs_arr = self.get_imvs_arr()
        if None is itroi:
            self.pn_offset = (0,0)
            return
        else:
            ((r0, c0),(r1, c1)) = itroi
            self.pn_offset = (r0, c0)
            self._imvs_arr = imvs_arr[:,r0:r1,c0:c1]

    def make_eval_default_stack(self, name, eval, dtype='float64'):
        imvs_arr = self.get_imvs_arr()
        arr = np.zeros(imvs_arr.shape, dtype=dtype)
        data = eval.create_dataset(name, arr.shape, arr.dtype)
        self.eval_stacks[name] = arr
        return arr

    def make_eval_param_stack(self, name, eval, dtype='float64'):
        imvs_arr = self.get_imvs_arr()
        arr = np.zeros(imvs_arr.shape[0], dtype=dtype)
        data = eval.create_dataset(name, arr.shape, arr.dtype)
        self.eval_stacks[name] = arr
        return arr

    def saveall_eval_stacks(self, eval):
        for name,arr in self.eval_stacks.items():
            eval[name][:] = arr

    def find_obj_002(self):
        eval, fh5 = self.vfocseries.get_eval_fh5()
        imvs_arr = self.get_imvs_arr()
        tst_arr = self.make_eval_default_stack('tst', eval)
        tst_label_arr = self.make_eval_default_stack('tst_label', eval, dtype='int32')
        tst_th_arr = self.make_eval_default_stack('tst_th', eval, dtype='int32')
        vari_parr = self.make_eval_param_stack('vari', eval)
        maxi_parr = self.make_eval_param_stack('maxi', eval)
        sumi_parr = self.make_eval_param_stack('sumi', eval)
        npts_parr = self.make_eval_param_stack('npts', eval)
        nobj_parr = self.make_eval_param_stack('nobj', eval)
        for i in range(self.npoints):
            print('-'*40)
            print(f'frame {i}:')
            w_arr = imvs_arr[i]
            flt_arr = ndi.gaussian_filter(w_arr, 3.5)
            
            print('flt dtype', flt_arr.dtype)
            tst_arr[i] = sq_arr = np.square(w_arr - flt_arr)
            maxi_parr[i] = sq_arr.max()
            vari_parr[i] = sq_arr.var()
            sumi_parr[i] = sq_arr.sum()
            th_arr = np.where(sq_arr > 0, 1, 0)
            tst_th_arr[i] = th_arr
            #th_arr = ndi.binary_erosion(th_arr, iterations=5).astype('int32')
            npts_parr[i] = th_arr.sum()
            (label_arr, nfeatures) = ndi.label(th_arr)
            tst_label_arr[i] = label_arr
            objects = ndi.find_objects(label_arr)
            nobj_parr[i] = len(objects)
            
        self.saveall_eval_stacks(eval)

    def find_obj_001(self):
        eval, fh5 = self.vfocseries.get_eval_fh5()
        imvs_arr = self.get_imvs_arr()
        tst_arr = self.make_eval_default_stack('tst', eval)
        tst_label_arr = self.make_eval_default_stack('tst_label', eval, dtype='int32')
        tst_th_arr = self.make_eval_default_stack('tst_th', eval, dtype='int32')
        vari_parr = self.make_eval_param_stack('vari', eval)
        maxi_parr = self.make_eval_param_stack('maxi', eval)
        sumi_parr = self.make_eval_param_stack('sumi', eval)
        npts_parr = self.make_eval_param_stack('npts', eval)
        nobj_parr = self.make_eval_param_stack('nobj', eval)
        for i in range(self.npoints):
            print('-'*40)
            print(f'frame {i}:')
            w_arr = imvs_arr[i]
            flt_arr = ndi.gaussian_filter(w_arr, 3.5)
            
            print('flt dtype', flt_arr.dtype)
            tst_arr[i] = sq_arr = np.square(w_arr - flt_arr)
            maxi_parr[i] = sq_arr.max()
            vari_parr[i] = sq_arr.var()
            sumi_parr[i] = sq_arr.sum()
            th_arr = np.where(sq_arr > 0, 1, 0)
            tst_th_arr[i] = th_arr
            #th_arr = ndi.binary_erosion(th_arr, iterations=5).astype('int32')
            npts_parr[i] = th_arr.sum()
            (label_arr, nfeatures) = ndi.label(th_arr)
            tst_label_arr[i] = label_arr
            objects = ndi.find_objects(label_arr)
            nobj_parr[i] = len(objects)
            
        self.saveall_eval_stacks(eval)

    def goto_focus(self, mode='vari'):
        vfs = self.vfocseries
        eval, fh5 = self.vfocseries.get_eval_fh5()
        try:
            assert fh5['focmot'].attrs['fmot_name'] == vfs.foc_mot.name
            pos_arr = np.array(fh5['focmot'][vfs.foc_mot.name])
            detect_arr = self.eval_stacks[mode]
            assert detect_arr.shape == (len(pos_arr),)
            foc_idx = detect_arr.argmax()
            assert vfs.foc_mot.name == 'nnx'
            print(f'moving {vfs.foc_mot.name} to {pos_arr[foc_idx]}')
            mv(vfs.foc_mot, pos_arr[foc_idx])
        finally:
            fh5.close()
        


class VideoView(object):

    def __init__(self, shape=None, pix_beam=PIX_BEAM, camera_s=CAMERA_S):
        self.shape = shape
        self.pix_beam = pix_beam
        self.camera_s = camera_s

    def acq(self):
        return video_image(self.camera_s, grayscale=True)

class IOMgr(object):

    def __init__(self, base_dapth=None, root_drpth='mcbroot'):
        self.base_dapth = base_dapth
        self.root_drpth = root_drpth
        self.root_dapth = path.join(self.base_dapth, self.root_drpth)
        provide_dir(self.root_dapth)

    def show_treelist(self):
        print('root: {self.root_drpth}')
        print(lstree(self.root_drpth))

    def make_group(self, groupname):
        group_dapth = path.join(self.root_dapth, groupname)
        os.makdir(group_dapth)

    def get_fapth(self, pfname, groupname, ext):
        fname = f'{pfname}.{ext}'
        return path.join(self.root_dapth, groupname, fname)

    def make_ofile(self, pfname, groupname):
        ext = 'h5'
        fapth = self.get_fapth(pfname, groupname, ext)
        fh5 = h5py.File(fapth, 'w')
        return fh5

    def get_ofile(self, pfname, groupname):
        ext = 'h5'
        fapth = self.get_fapth(pfname, groupname, ext)
        fh5 = h5py.File(fapth, 'r+')
        return fh5

    def get_infile(self, pfname, groupname):
        ext = 'h5'
        fapth = self.get_fapth(pfname, groupname, ext)
        fh5 = h5py.File(fapth, 'r')
        return fh5
    
