def parse_srois(srois):
    roill = srois.split('\n')
    tp_rois = []
    for l in roill:
        if l.startswith('#'):
            continue
        if not l.strip():
            continue
    
        ctmne, ll, ul = l.split()
        ll = int(ll)
        ul = int(ul)
        tp_rois.append((ctmne, ll, ul))
    return tp_rois

def wmflu():
    wm(nfluoax, nfluoay, nfluoaz, nfluomrx, nfluomry, nfluomrz)

def doomflu():
    wmflu()
    doom(nfluoax, nfluoay, nfluoaz, nfluomrx, nfluomry, nfluomrz)

def log_curr_detpos():
    pass

def nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,40,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,40,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)



SROIS = '''
Si 338 358
S 452 472
Ca 728 748
Ti 892 912
Mn 1170 1190
Fe 1270 1290
Ni 1486 1506
Cu 1600 1620
Zn 1718 1738
Ga 1840 1860
Ge 1968 1988
As 2098 2118
Se 2234 2254
OsL 1772 1792
IrL 1826 1846
PtL 1878 1898
Compt 2920 2940
Rayl 2990 3010
'''
class ES1404Infra(object):

    TAGS = "eroi eig fx1 fx8 all"

    def __init__(self, xray_energy=None):
        if None is xray_energy:
            self.xray_energy = ID13_XRAY_ENERGY
        else:
            self.xray_energy = xray_energy
        self.basic()

    def basic(self):
        mgd()

    def getfx(self, sfx):
        if sfx == 'fx1':
            return fx1
        elif sfx == 'fx8':
            return fx8

    def set_fx_rois(self, sfx, srois, clear=False):
        tp_rois = parse_srois(srois)
        fx = self.getfx(sfx)
        if clear:
            fx.rois.clear()
        for t in tp_rois:
            print('setting:', t)
            fx.rois.set(*t)

    def show_rois(self):
        print ('==== EIGER:')
        print (eiger.roi_counters.__info__())
        print ('\n==== fx1:')
        print (fx1.rois.__info__())
        print ('\n==== fx8:')
        print (fx1.rois.__info__())


    def switchon(self, tags):
        if isinstance(tags, str):
            tags = tuple(tags.split())
        for t in tags:
            assert t in self.TAGS
        tags = tuple(tags)
        for i in tags:
            if 'all' in tags:
                tags = tuple("eroi eig fx1 fx8".split())
        for t in tags:
            m = getattr(self, f"activate_{t}")
            m()
        
    def switchoff(self, tags):
        if isinstance(tags, str):
            tags = tuple(tags.split())
        for t in tags:
            assert t in self.TAGS
        tags = tuple(tags)
        for i in tags:
            if 'all' in tags:
                tags = tuple("eroi eig fx1 fx8".split())
        for t in tags:
            m = getattr(self, f"deactivate_{t}")
            m()

    def activate_eroi(self):
        MG_EH3a.enable('*eiger:r*')

    def deactivate_eroi(self):
        MG_EH3a.disable('*eiger:r*')

    def activate_eig(self):
        eiger_energy = self.xray_energy*1000.0
        print(f'setting: eiger.camera.photon_energy={eiger_energy}')
        eiger.camera.photon_energy=eiger_energy
        print(f'found: eiger.camera.photon_energy={eiger.camera.photon_energy}')
        MG_EH3a.enable('*eiger:im*')

    def deactivate_eig(self):
        MG_EH3a.disable('*eiger:im*')

    def activate_fx8(self):
        MG_EH3a.enable('*fx8:*')
        
    def deactivate_fx8(self):
        MG_EH3a.disable('*fx8:*')
        
    def activate_fx1(self):
        MG_EH3a.enable('*fx1:*')
        
    def deactivate_fx1(self):
        MG_EH3a.disable('*fx1:*')


