import math
import time

def make_new_stinfra(fxpth):
        return StretchInfra(log_fxpth=fxpth, idx='<auto>')

class StretchInfra(object):

    def __init__(self, st_act_name='ustretch',
        force_ct_name='force_real_norm_ctrl:force_real_norm',
        log_fxpth=None, idx=0):
        self.log_fxpth = log_fxpth
        self.provide_log()

        if '<auto>' == idx:
            idxll, stll, forcell, tmll = self.read_log()
            print(idxll)
            if idxll:
                idx = idxll[-1] + 1
            else:
                idx = 0
        self.idx = idx
        assert st_act_name == 'ustretch'
        self.st_act = config.get(st_act_name)

    def provide_log(self):
        if path.exists(self.log_fxpth):
            return
        else:
            with open(self.log_fxpth, 'w') as f:
                f.write('')

    def log(self, idx, st, force, tm=None):
        if None is tm:
            tm = time.time()
        with open(self.log_fxpth, 'a') as f:
            s = f'{idx} {st:10.5f} {force:10.5f} {tm:19.7f}\n'
            f.write(s)
        return s

    def read_vals(self):
        idx = self.idx
        self.idx += 1
        st_val = self.st_act.position
        force, tm = self.measure_force()
        return idx, st_val, force, tm

    def acq(self):
        vals = self.read_vals()
        print(self.log(*vals))

    def read_log(self):
        with open(self.log_fxpth, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = (x.split() for x in ll if x)
        idxll = []
        stll = []
        forcell = []
        tmll = []
        for (a,b,c,d) in ll:
            idxll.append(int(a))
            stll.append(float(b))
            forcell.append(float(c))
            tmll.append(float(d))
        return idxll, stll, forcell, tmll

    def check_force(self, force, ck_t):
        (ck_val_min,ck_val_max) = ck_t
        if force < ck_val_min:
            return('crit',f'val_min passed {force}')
        if force > ck_val_max:
            return('crit',f'val_max exceeded {force}')

        return ('ok', '')

    def goto_force(self, st_incr, max_st_pos=None, min_force=0.0, max_force=0.0):
        initial_st_pos = ustretch.position
        ok_bflg = True
        cyc = 0
        while ok_bflg:
            cyc += 1
            print('======== cyxle:', cyc)
            this_st_pos = cyc*st_incr + initial_st_pos
            if this_st_pos > max_st_pos:
                print(f'max_st = {max_st_pos} : requested_pos = {this_st_pos} bailing out ...')
                return
            try:
                mv(self.st_act, this_st_pos)
            except:
                print('tolerating mmoving error ...')
            idx, read_st_pos, force, tm = self.read_vals()
            self.log(idx, read_st_pos, force, tm)
            (res, reason) = self.check_force(force, (min_force,max_force))
            if not 'ok' == res:
                print(f'check not ok: reason={reason} bailing out ...')
                return

    def measure_force(self):
        sct(0.02)
        tm = time.time()
        s = SCANS[-1]
        data = s.get_data()
        force_data = data['force_real_norm_ctrl:force_real_norm']
        force_value = force_data[0]
        return force_value, tm
