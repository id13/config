import math
import time
from bliss.setup_globals import *
import gevent
from bliss.comm.spec.connection import SpecConnection
spec_conn=SpecConnection("lid13eh21:hummontest")

class Timeout(object):
    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()


class Humidity1(object):

    def __init__(self, timeout = 1000.0, eps=0.25, cntmne='hum_h2',
        spec_conn=None):
        self.timeout = timeout
        self.eps = eps
        self.cntmne = cntmne
        self.spec_conn = spec_conn

    def get_hum_sp(self):
        self.spec_conn.send_msg_cmd(f"ct 0.1")
        reply=spec_conn.send_msg_chan_read("scaler/hum_rsp/value")
        gevent.sleep(2.0)
        return float(reply.getValue())
        self.spec_conn.send_msg_cmd(f"ct 0.1")
        reply=spec_conn.send_msg_chan_read("scaler/hum_rsp/value")
        gevent.sleep(2.0)
        return float(reply.getValue())

    def get_hum(self):
        self.spec_conn.send_msg_cmd(f"ct 0.1")
        reply=spec_conn.send_msg_chan_read(f"scaler/{self.cntmne}/value")
        gevent.sleep(1.0)
        return float(reply.getValue())

    def relatively_safe_get_hum(self, nmaxcyc=10):
        for i in range(nmaxcyc):
            hum = self.get_hum()
            if hum > 0.0 and hum < 100.0:
                return hum

    def set_hum(self, hum):
        umv(uhum_sp,hum)
        gevent.sleep(1.0)
        umv(uhum_sp,hum)
        gevent.sleep(1.0)
        hum_sp = self.get_hum_sp()
        hum_sp = self.get_hum_sp()
        return hum_sp

    def hum_is_reached(self, eps=None):
        if None is eps:
            eps = self.eps
        hum_sp = self.get_hum_sp()
        to = False
        toobj = Timeout(self.timeout)
        toobj.reset()
        (to, delta_t) = toobj.check_timeout()
        while not to:
            hum = self.relatively_safe_get_hum()
            if math.fabs(hum - hum_sp) <= eps:
                print(f'got it!     hum = {hum:8.4f}    delta_t = {delta_t}')
                return True
            gevent.sleep(5.0)
            (to, delta_t) = toobj.check_timeout()
            print(f'current hum = {hum:8.4f}    current sp = {hum_sp:8.4f}    delta_t = {delta_t}')
        return False
