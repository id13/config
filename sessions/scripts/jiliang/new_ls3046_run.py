# this is the script for the tomo experiment of Nacre
print('ls3046 is loaded (1)')
start_key = 'a'

def getpos(pos):
    '''
    go to the top of scanning range
    '''
    pre_name = f'{pos[:]}'
    gopos(pos)
    return pre_name

def block_scan(yrange,zrange,ystep_size,zstep_size,exp_t,retveloc,frames_per_file):
    '''
    the scan separat to multiple blocks in Z direction
    each block size is defined by yrange and zrange
    interval size is defined by ystep_size and zstep_size
    '''
    ypts = int(yrange/ystep_size)
    zpts = int(zrange/zstep_size)
    y_low  = -yrange/2
    y_high = yrange/2
    z_low  = -zrange/2
    z_high = zrange/2 
    #in the eh2 dkmap does not exist
    #kmap.dkmap(ustry,y_low,y_high,ypts,ustrz,z_low,z_high,zpts,exp_t)
    dkmapyz_2(y_low,y_high,ypts,z_low,z_high,zpts,exp_t,
              retveloc=retveloc,frames_per_file=frames_per_file)

def rot_block_scan(rot_step,yrange,zrange,
                   ystep_size,zstep_size,exp_t,
                   retveloc,frames_per_file):
    '''
    for each block, there will be a rotation to collect the projection at different angle
    the rotation always start from -180
    rot_step is the angle resolution of rotation unit is degree
    '''
    rot_ang = np.arange(-180,180+rot_step/2,rot_step)
    for _ in rot_ang:
        print(f'............................ recording angle: {_}')
        try:
            f = open('stop_it')
            enddataset()
            return 'stopit'
        except:
            pass
        block_scan(yrange,zrange,ystep_size,zstep_size,exp_t,
                   retveloc=retveloc,frames_per_file=frames_per_file)

def total_scan(pos,yrange,
               zoffset,zblock_size,zblock_space,
               ystep_size,zstep_size,rot_step,exp_t,
               start_key=start_key,
               retveloc=5,
               frames_per_file=2000, 
               already_done=0):
    '''
    all the transition motors have unit of millimeter
    go to the center of total scan range, yrange and zrange is the size of scan meshgrid
    zblock_size is a list including the information of size of each block in Z direction
    zblock_space is a list including the information of distance between each blocks, length of the list 
    should 1 less than the length of the list of zblock_size
    '''
    try:
        prename = getpos(pos)
        so()
        fshtrigger()
        mgeig()
        z_initial = ustrz.position
        z_start = z_initial - zoffset
        z_curr = z_start
        z_scanposll = []
        for _ in range(len(zblock_size)):
            if _ == 0:
                z_displacement = zblock_size[_]/2
            else:
                z_displacement = (zblock_size[int(_-1)]/2+zblock_space[int(_-1)]+
                                  zblock_size[_]/2)
            z_curr = z_curr + z_displacement
            z_scanposll.append(z_curr)

            
        print(f'start ustrz = {z_start}')
        for _ in range(len(zblock_size)):
            print(f'====================================== processing block: {_}')
            z_run = z_scanposll[_]
            print("z business:")
            print(f'block {_} - initial ustrz = {z_initial};   start ustrz = {z_start};   run ustrz = {z_run}')
            dataset_name = f'{prename}_block{_:03d}_{start_key}'
            if already_done > _:
                print(f'====================================== skipping block: {_}')
            else:
                print(f'====================================== recording block: {_}')
                print(f'moving ustrz to {z_run}')
                umv(ustrz, z_run)
                print(f'ustrz position is now {ustrz.position}')
                
                newdataset(dataset_name)
                if rot_block_scan(rot_step,yrange,zblock_size[_],ystep_size,zstep_size,exp_t,
                               retveloc=retveloc,frames_per_file=frames_per_file) == 'stopit':
                    print('okay - bailing out ...')
                    break
                enddataset()
        sc()
    finally:
        sc()
        sc()
pos = 'tomo_cen'

def run_tomo():
    already_done = 0
    yrange = 1.6
    zoffset = 0.0
    zblock_size  = [0.1 for x in range(9)]
    zblock_space  = [0.01 for x in range(8)]
    #zblock_size  = [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
    #zblock_space = [0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01]
    #zblock_size  = [0.1,0.1]
    #zblock_space = [0.01]
    ystep_size   = 0.01
    zstep_size   = 0.01
    rot_step     = 3
    exp_t        = 0.01
    retveloc     = 5
    frames_per_file = 1771
    total_scan(pos,yrange,zoffset,
               zblock_size,zblock_space,
               ystep_size,zstep_size,
               rot_step,exp_t,start_key=start_key,
               retveloc=retveloc,frames_per_file=frames_per_file,
               already_done=already_done)
