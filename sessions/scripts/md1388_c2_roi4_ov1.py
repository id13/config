print('md1388_c2_ov2 - load 2')
PREFIX = 'roi4_ov1_lac'
PIEZO_POS = '''
#no   p2    p3    npts   hwidth   expt  fpf 
1     184.4 38.2  160    20       0.01  320
2     162.7 87.9  160    20       0.01  320
3     39.2  116.3 160    20       0.01  320
4     50.1  134.7 160    20       0.01  320
5     167.4 162.9 160    20       0.01  320

'''
  	

DRY_RUN = False
START_KEY = 'a'
def doo_scan(p2, p3, npts, hw, expt, fpf):
    mv(nnp2, p2)
    mv(nnp3, p3)
    if DRY_RUN:
        print(f'dry loff_kmap: nnp2, {-hw}, {hw},{npts}, nnp3, {-hw}, {hw}, {npts}, {expt}, {fpf}')
    else:
        loff_kmap(nnp2, -hw,hw,npts,nnp3,-hw,hw,npts,expt,
            frames_per_file=fpf)

def prepscan(prefix, piezo_pos_no):
    dsname = f'{START_KEY}_{prefix}_{piezo_pos_no:1d}'
    if DRY_RUN:
        print(f'dry newdataset: {dsname}')
    else:
        newdataset(dsname)
    
def run_piezo_positions():
    ll = PIEZO_POS.split('\n')
    for l in ll:
        l = l.strip()
        if l.startswith('#') or not l:
            continue
        pno, p2, p3, npts, hw, expt, fpf = l.split()
        pno  = int(pno)
        p2   = float(p2)
        p3   = float(p3)
        npts = int(npts)
        hw   = float(hw)
        expt = float(expt)
        fpf  = int(fpf)
        prepscan(PREFIX, pno)
        doo_scan(p2, p3, npts, hw, expt, fpf)
