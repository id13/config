print('load ihma168 ver 3')
prefix = 'b'
def ihma168_aug_29_day():
    mgeig()
    MG_EH2.enable('*xmap*')
    fshtrigger()
    so()
    try:
        for _ in pos_list:
            gopos(_)
            fn = _[:-5]
            dn = f"{fn}_{prefix}"
            print(dn)
            newdataset(dn)
            #dkpatchmesh(0.5,199,0.5,199,0.05,2,2)#,5)#produce too much data
            dkpatchmesh(0.5,199,0.5,199,0.05,1,1,5)
            enddataset()
        
        
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()

#pos_list = [
#'calibrant_KF3_pos1_0001.json',
#'calibrant_KF3_pos2_0002.json',
#'calibrant_KF5_pos1_0003.json',
#'calibrant_KF5_pos2_0004.json',
#'calibrant_KF9_pos1_0005.json',
#'calibrant_KF9_pos2_0006.json',
#
#]

#pos_list = [
#'mount01_new_z_pos_KF3_pos1_0008.json',
#'mount01_new_z_pos_KF5_pos1_0009.json',
#'mount01_new_z_pos_KF9_pos1_0010.json',
#]

#pos_list = [
#'mount02_KF25_pos1_0012.json',
#'mount02_KF25_pos2_0013.json',
#'mount02_KF32_pos1_0011.json',
#'mount02_WKF19_pos1_0014.json',
#]

pos_list = [
'mount03_WKF115_pos3_0000.json',
'mount03_WKF115_pos4_0001.json',
'mount03_KF16_pos1_0002.json',
'mount03_KF16_pos2_0003.json',
'mount03_X1_pos1_0004.json',
]
