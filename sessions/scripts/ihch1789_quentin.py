def doo_poi(*p):
    poiname = p[0]
    scantag = p[1]
    try:
        rstp(poiname)
        print(f'found pos {poiname}')
    except OSError:
        stp(poiname)
        print(f'stored pos {poiname}')
    scanparams = p[2:]
    dsname = f'{poiname}_{scantag}'
    newdataset(dsname)
    dooi(f'doopoi: {poiname} : {scantag} : {str(scanparams)}')
    dkmapyz_2(*scanparams,retveloc=5)
