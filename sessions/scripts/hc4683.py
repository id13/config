def loop_dmesh(start_nnp2, end_nnp2, steps_nnp2, step_nnp3, line_nums, shots_per_point, exp_time):
    try:
        for i in range(0, line_nums):
            dmesh(nnp1, 0, 0, shots_per_point, nnp2, start_nnp2, end_nnp2, steps_nnp2, exp_time)
            umvr(nnp3, step_nnp3)   
    finally:
        umvr(nnp3, -step_nnp3*line_nums)

def overnight1():
    newdataset('PdCuSi_asdep')
    rstp('holder1_run2_PdCuSi_asdep_pos3_empty_50um_0010.json')
    dscan(nnp1, 0, 0, 50, 1)

    rstp('holder1_run2_PdCuSi_asdep_pos3_0009.json')
    dmesh(nnp2, -1.5, 1.5, 30, nnp3, -1.5, 1.5, 30, 1)    
    dmesh(nnp2, -0.35, 0.35, 70, nnp3, -0.35, 0.35, 70, 1)    
    
    ## 5 lines of 5 points, 500nm/step, 300x1s each point
    #loop_dmesh(-1, 1, 4, 0.5, 5, 300, 1)

    newdataset('CuZr_ger_320C')
    eiger.camera.threshold_energy=10500
    eiger.camera.threshold_energy
    
    rstp('holder1_run2_CuZr_ger_320C_pos1_empty_150um_0002.json')
    dscan(nnp1, 0, 0, 50, 1)
    rstp('holder1_run2_CuZr_ger_320C_pos1_0001.json')
    dmesh(nnp2, -5, 5, 100, nnp3, -5, 5, 100, 1)

    rstp('holder1_run2_CuZr_ger_320C_pos2_empty_60um_0004.json')
    dscan(nnp1, 0, 0, 50, 1)
    rstp('holder1_run2_CuZr_ger_320C_pos2_0003.json')
    dmesh(nnp2, -5, 5, 100, nnp3, -5, 5, 200, 1)

    eiger.camera.threshold_energy=6500    
    eiger.camera.threshold_energy


def overnight2():
    so()
    try:
        print(eiger.camera.threshold_energy)
        rstp('holder5_CuZr_night_pos1_0001.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos2_0002.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos3_0003.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos4_0004.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos5_0005.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos6_0006.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos7_0007.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)
        rstp('holder5_CuZr_night_pos8_0008.json')
        loop_dmesh(-3, 3, 30, 0.2, 25, 5, 1)

    finally:
        sc()
        sc()


def longscans1():
    so()
    try:
        print(eiger.camera.threshold_energy)

        newdataset('CuZr_2016_013ab')
        rstp('holder5a_CuZr_2016_013ab_pos2_empty_50um_0008.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder5a_CuZr_2016_013ab_pos2_0007.json')
        loop_dmesh(-5, 5, 10, 1, 12, 9, 1)

        newdataset('CuZr_n496')
        rstp('holder5a_CuZr_n496_pos1_empty_50um_0011.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder5a_CuZr_n496_pos1_0010.json')
        loop_dmesh(-5, 5, 10, 1, 12, 9, 1)

        newdataset('CuZr_n491')
        rstp('holder5a_CuZr_n491_pos1_empty_50um_0001.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder5a_CuZr_n491_pos1_0000.json')
        loop_dmesh(-5, 5, 10, 1, 12, 9, 1)

    finally:
        sc()
        sc()

def overnight3():
    so()
    print(eiger.camera.threshold_energy)
    try:
        newdataset('CuZr_n491_set1')
        rstp('holder7_new_CuZr_n491_pos1_empty_0001.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n491_pos1_0022.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n495_set1')
        rstp('holder7_new_CuZr_n495_pos1_empty_70um_0005.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n495_pos1_0025.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)
        
        newdataset('CuZr_n496_set1')
        rstp('holder7_new_CuZr_n496_pos1_empty_80um_0011.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n496_pos1_0028.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n497_set1')
        rstp('holder7_new_CuZr_n497_pos1_empty_50um_0017.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n497_pos1_0031.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        
        newdataset('CuZr_n491_set2')
        rstp('holder7_new2_CuZr_n491_pos2_0023.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n495_set2')
        rstp('holder7_new_CuZr_n495_pos2_empty_80um_0007.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n495_pos2_0026.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)
        
        newdataset('CuZr_n496_set2')
        rstp('holder7_new_CuZr_n496_pos2_empty_240um_0013.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n496_pos2_0029.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n497_set2')
        rstp('holder7_new_CuZr_n497_pos2_empty_70um_0019.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n497_pos2_0032.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)


        newdataset('CuZr_n491_set3')
        rstp('holder7_new2_CuZr_n491_pos3_0024.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n495_set3')
        rstp('holder7_new_CuZr_n495_pos3_empty_80um_0009.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n495_pos3_0027.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)
        
        newdataset('CuZr_n496_set3')
        rstp('holder7_new_CuZr_n496_pos3_empty_50um_0015.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n496_pos3_0030.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)

        newdataset('CuZr_n497_set3')
        rstp('holder7_new_CuZr_n497_pos3_empty_70um_0021.json')
        dscan(nnp1, 0, 0, 100, 1)
        rstp('holder7_new2_CuZr_n497_pos3_0033.json')
        loop_dmesh(-5, 5, 20, 0.5, 15, 9, 1)
 
    finally:
        sc()
        sc()


