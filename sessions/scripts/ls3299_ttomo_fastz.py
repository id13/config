print('version ttomo test ihls3299 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

#index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 1.800, 0
2, 0, 3.600, 0
3, 0, 5.400, 0
4, 0, 7.200, 0
5, 0, 9.000, 0
6, 0, 10.800, 0
7, 0, 12.600, 0
8, 0, 14.400, 0
9, 0, 16.200, 0
10, 0, 18.000, 0
11, 0, 19.800, 0
12, 0, 21.600, 0
13, 0, 23.400, 0
14, 0, 25.200, 0
15, 0, 27.000, 0
16, 0, 28.800, 0
17, 0, 30.600, 0
18, 0, 32.400, 0
19, 0, 34.200, 0
20, 0, 36.000, 0
21, 0, 37.800, 0
22, 0, 39.600, 0
23, 0, 41.400, 0
24, 0, 43.200, 0
25, 0, 45.000, 0
26, 0, 46.800, 0
27, 0, 48.600, 0
28, 0, 50.400, 0
29, 0, 52.200, 0
30, 0, 54.000, 0
31, 0, 55.800, 0
32, 0, 57.600, 0
33, 0, 59.400, 0
34, 0, 61.200, 0
35, 0, 63.000, 0
36, 0, 64.800, 0
37, 0, 66.600, 0
38, 0, 68.400, 0
39, 0, 70.200, 0
40, 0, 72.000, 0
41, 0, 73.800, 0
42, 0, 75.600, 0
43, 0, 77.400, 0
44, 0, 79.200, 0
45, 0, 81.000, 0
46, 0, 82.800, 0
47, 0, 84.600, 0
48, 0, 86.400, 0
49, 0, 88.200, 0
50, 0, 90.000, 0
51, 0, 91.800, 0
52, 0, 93.600, 0
53, 0, 95.400, 0
54, 0, 97.200, 0
55, 0, 99.000, 0
56, 0, 100.800, 0
57, 0, 102.600, 0
58, 0, 104.400, 0
59, 0, 106.200, 0
60, 0, 108.000, 0
61, 0, 109.800, 0
62, 0, 111.600, 0
63, 0, 113.400, 0
64, 0, 115.200, 0
65, 0, 117.000, 0
66, 0, 118.800, 0
67, 0, 120.600, 0
68, 0, 122.400, 0
69, 0, 124.200, 0
70, 0, 126.000, 0
71, 0, 127.800, 0
72, 0, 129.600, 0
73, 0, 131.400, 0
74, 0, 133.200, 0
75, 0, 135.000, 0
76, 0, 136.800, 0
77, 0, 138.600, 0
78, 0, 140.400, 0
79, 0, 142.200, 0
80, 0, 144.000, 0
81, 0, 145.800, 0
82, 0, 147.600, 0
83, 0, 149.400, 0
84, 0, 151.200, 0
85, 0, 153.000, 0
86, 0, 154.800, 0
87, 0, 156.600, 0
88, 0, 158.400, 0
89, 0, 160.200, 0
90, 0, 162.000, 0
91, 0, 163.800, 0
92, 0, 165.600, 0
93, 0, 167.400, 0
94, 0, 169.200, 0
95, 0, 171.000, 0
96, 0, 172.800, 0
97, 0, 174.600, 0
98, 0, 176.400, 0
99, 0, 178.200, 0
100, 0, 0.000, 0
101, 10, 8.571, 0
102, 10, 25.306, 0
103, 10, 42.041, 0
104, 10, 58.776, 0
105, 10, 75.510, 0
106, 10, 92.245, 0
107, 10, 108.980, 0
108, 10, 125.714, 0
109, 10, 142.449, 0
110, 10, 159.184, 0
111, 10, 175.918, 0
112, 10, 192.653, 0
113, 10, 209.388, 0
114, 10, 226.122, 0
115, 10, 242.857, 0
116, 10, 259.592, 0
117, 10, 276.327, 0
118, 10, 293.061, 0
119, 10, 309.796, 0
120, 10, 326.531, 0
121, 10, 343.265, 0
122, 0, 0.000, 0
123, 20, 0.000, 0
124, 20, 18.000, 0
125, 20, 36.000, 0
126, 20, 54.000, 0
127, 20, 72.000, 0
128, 20, 90.000, 0
129, 20, 108.000, 0
130, 20, 126.000, 0
131, 20, 144.000, 0
132, 20, 162.000, 0
133, 20, 180.000, 0
134, 20, 198.000, 0
135, 20, 216.000, 0
136, 20, 234.000, 0
137, 20, 252.000, 0
138, 20, 270.000, 0
139, 20, 288.000, 0
140, 20, 306.000, 0
141, 20, 324.000, 0
142, 20, 342.000, 0
143, 0, 0.000, 0
144, 30, 10.000, 0
145, 30, 29.444, 0
146, 30, 48.889, 0
147, 30, 68.333, 0
148, 30, 87.778, 0
149, 30, 107.222, 0
150, 30, 126.667, 0
151, 30, 146.111, 0
152, 30, 165.556, 0
153, 30, 185.000, 0
154, 30, 204.444, 0
155, 30, 223.889, 0
156, 30, 243.333, 0
157, 30, 262.778, 0
158, 30, 282.222, 0
159, 30, 301.667, 0
160, 30, 321.111, 0
161, 30, 340.556, 0
162, 0, 0.000, 0
163, 40, 0.000, 0
164, 40, 22.500, 0
165, 40, 45.000, 0
166, 40, 67.500, 0
167, 40, 90.000, 0
168, 40, 112.500, 0
169, 40, 135.000, 0
170, 40, 157.500, 0
171, 40, 180.000, 0
172, 40, 202.500, 0
173, 40, 225.000, 0
174, 40, 247.500, 0
175, 40, 270.000, 0
176, 40, 292.500, 0
177, 40, 315.000, 0
178, 40, 337.500, 0
179, 0, 0.000, 0
180, 45, 12.000, 0
181, 45, 35.200, 0
182, 45, 58.400, 0
183, 45, 81.600, 0
184, 45, 104.800, 0
185, 45, 128.000, 0
186, 45, 151.200, 0
187, 45, 174.400, 0
188, 45, 197.600, 0
189, 45, 220.800, 0
190, 45, 244.000, 0
191, 45, 267.200, 0
192, 45, 290.400, 0
193, 45, 313.600, 0
194, 45, 336.800, 0
195, 0, 0.000, 0
196, 35, 0.000, 0
197, 35, 21.176, 0
198, 35, 42.353, 0
199, 35, 63.529, 0
200, 35, 84.706, 0
201, 35, 105.882, 0
202, 35, 127.059, 0
203, 35, 148.235, 0
204, 35, 169.412, 0
205, 35, 190.588, 0
206, 35, 211.765, 0
207, 35, 232.941, 0
208, 35, 254.118, 0
209, 35, 275.294, 0
210, 35, 296.471, 0
211, 35, 317.647, 0
212, 35, 338.824, 0
213, 0, 0.000, 0
214, 25, 9.474, 0
215, 25, 27.922, 0
216, 25, 46.371, 0
217, 25, 64.820, 0
218, 25, 83.269, 0
219, 25, 101.717, 0
220, 25, 120.166, 0
221, 25, 138.615, 0
222, 25, 157.064, 0
223, 25, 175.512, 0
224, 25, 193.961, 0
225, 25, 212.410, 0
226, 25, 230.859, 0
227, 25, 249.307, 0
228, 25, 267.756, 0
229, 25, 286.205, 0
230, 25, 304.654, 0
231, 25, 323.102, 0
232, 25, 341.551, 0
233, 0, 0.000, 0
234, 15, 0.000, 0
235, 15, 18.000, 0
236, 15, 36.000, 0
237, 15, 54.000, 0
238, 15, 72.000, 0
239, 15, 90.000, 0
240, 15, 108.000, 0
241, 15, 126.000, 0
242, 15, 144.000, 0
243, 15, 162.000, 0
244, 15, 180.000, 0
245, 15, 198.000, 0
246, 15, 216.000, 0
247, 15, 234.000, 0
248, 15, 252.000, 0
249, 15, 270.000, 0
250, 15, 288.000, 0
251, 15, 306.000, 0
252, 15, 324.000, 0
253, 15, 342.000, 0
254, 0, 0.000, 0
255, 5, 8.571, 0
256, 5, 25.306, 0
257, 5, 42.041, 0
258, 5, 58.776, 0
259, 5, 75.510, 0
260, 5, 92.245, 0
261, 5, 108.980, 0
262, 5, 125.714, 0
263, 5, 142.449, 0
264, 5, 159.184, 0
265, 5, 175.918, 0
266, 5, 192.653, 0
267, 5, 209.388, 0
268, 5, 226.122, 0
269, 5, 242.857, 0
270, 5, 259.592, 0
271, 5, 276.327, 0
272, 5, 293.061, 0
273, 5, 309.796, 0
274, 5, 326.531, 0
275, 5, 343.265, 0
276, 0, 0.000, 0


"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        cmd = f'kmap.dkmap(nnp6, {llz},{ulz},{nitvz},{expt},nnp5, {lly},{uly},{nitvy} )'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        print('move to patch1')
        with gevent.Timeout(seconds=180):
            try:
                # put the nano scan ...
                print('patch1')
                kmap.dkmap(nnp6,llz,ulz,nitvz,nnp5,lly,uly,nitvy,expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)

        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//0.75)
        nitvz = int(dz//0.75)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz


def ls3299_ttomo_main(zkap, start_key, resume=-1):

    
    print('Hi IH-MI_1531!')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -50,
        uly = 50,
        nitvy = 200,
        llz = -45,
        ulz = 45,
        nitvz = 180,
        expt = 0.002
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
