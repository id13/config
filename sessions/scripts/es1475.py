def tuesday_dinner():
	rstp('hol2_new_ul')
	newdataset('lv_lowres')
	dkmapyz_2(0,6,600,0,6,600,0.005,retveloc=5)
	rstp('hol3_new_ul')
	newdataset('ln1_lowres')
	dkmapyz_2(0,9,900,0,6,600,0.005,retveloc=5)
	rstp('hol4_new_ul')
	newdataset('ln2_lowres')
	dkmapyz_2(0,10,1000,0,7,700,0.005,retveloc=5)

def tuesday_night():
	rstp('hol1_ul')
	umv(ustry,2.05,ustrz,-11.4)
	try:
		newdataset('lc_highres')
	except:
		pass
	dkmapyz_2(0,2,800,0,2,800,0.005,retveloc=5)
	rstp('hol2_new_ul')
	umv(ustry,2.8,ustrz,0.95)
	try:
		newdataset('lv_highres')
	except:
		pass
	dkmapyz_2(0,2,800,0,2,800,0.005,retveloc=5)
	rstp('hol3_new_ul')
	umv(ustry,12.1,ustrz,-10.8)
	try:
		newdataset('ln1_highres')
	except:
		pass
	dkmapyz_2(0,4,1600,0,1,400,0.005,retveloc=5)
	rstp('hol4_newnew_ul')
	umv(ustry,-16.1,ustrz,0.2)
	try:
		newdataset('ln2_highres')
	except:
		pass
	dkmapyz_2(0,4,1600,0,1,400,0.005,retveloc=5)


def wednesday_morning():
	rstp('hol1_ul')
	dkmapyz_2(0,9,900,0,2.7,270,0.005,retveloc=5)
	rstp('hol2_ul')
	dkmapyz_2(0,4,400,0,8,800,0.005,retveloc=5)
	rstp('hol3_ul')
	dkmapyz_2(0,6,600,0,6.3,630,0.005,retveloc=5)
	rstp('hol4_new_ul')
	dkmapyz_2(0,5,2000,0,1,400,0.005,retveloc=5)
	
def wednesday_morning_highres():
	rstp('hol4_new_ul')
	dkmapyz_2(0,5,2000,0,0.25,100,0.005,retveloc=5)
	dkmapyz_2(0,5,2000,0.25,0.50,100,0.005,retveloc=5)
	dkmapyz_2(0,5,2000,0.5,0.75,100,0.005,retveloc=5)
	dkmapyz_2(0,5,2000,0.75,1,100,0.005,retveloc=5)

def wednesday_afternoon_highres():
	rstp('hol1_ul')
	umv(ustry,0.6,ustrz,-11.2)
	dkmapyz_2(0,3,1200,0,0.5,200,0.005,retveloc=5)
	dkmapyz_2(0,3,1200,0.5,1,200,0.005,retveloc=5)
	rstp('hol2_ul')
	umv(ustry,-1.1,ustrz,2.2)
	dkmapyz_2(0,2,800,0,0.5,200,0.005,retveloc=5)
	dkmapyz_2(0,2,800,0.5,1,200,0.005,retveloc=5)
	dkmapyz_2(0,2,800,1,1.5,200,0.005,retveloc=5)
	dkmapyz_2(0,2,800,1.5,2,200,0.005,retveloc=5)
	rstp('hol3_ul')
	umv(ustry,-16.4,ustrz,-9.4)
	dkmapyz_2(0,5,2000,0,0.25,100,0.005,retveloc=5)
	dkmapyz_2(0,5,2000,0.25,0.5,100,0.005,retveloc=5)
	
def thursday_night():
	rstp('hol1_ul')
	dkmapyz_2(0,6,600,0,3,300,0.005,retveloc=5)
	dkmapyz_2(0,6,600,3,6,300,0.005,retveloc=5)
	rstp('hol2_ul')
	dkmapyz_2(0,6,600,0,2.5,250,0.005,retveloc=5)
	dkmapyz_2(0,6,600,2.5,5,250,0.005,retveloc=5)
	rstp('hol3_ul')
	dkmapyz_2(0,9,900,0,2.25,225,0.005,retveloc=5)
	dkmapyz_2(0,9,900,2.25,4.5,225,0.005,retveloc=5)
	rstp('hol4_ul')
	dkmapyz_2(0,5,500,0,4,400,0.005,retveloc=5)
	dkmapyz_2(0,5,500,4,8,400,0.005,retveloc=5)
	rstp('hol1_ul')
	umvr(ustrz,3,ustry,0.5)
	dkmapyz_2(0,3,1200,0,0.75,300,0.005,retveloc=5)
	dkmapyz_2(0,3,1200,0.75,1.5,300,0.005,retveloc=5)
	rstp('hol2_ul')
	umvr(ustrz,1.5,ustry,1.5)
	dkmapyz_2(0,3,1200,0,0.75,300,0.005,retveloc=5)
	dkmapyz_2(0,3,1200,0.75,1.5,300,0.005,retveloc=5)
	rstp('hol3_ul')
	umvr(ustrz,3,ustry,1)
	dkmapyz_2(0,4,1600,0,0.5,200,0.005,retveloc=5)
	dkmapyz_2(0,4,1600,0.5,1,200,0.005,retveloc=5)
	rstp('hol4_ul')
	umvr(ustrz,1)
	dkmapyz_2(0,3,1200,0,0.75,300,0.005,retveloc=5)
	dkmapyz_2(0,3,1200,0.75,1.5,300,0.005,retveloc=5)
	rstp('hol4_ul')
	umvr(ustry,1,ustrz,6)
	dkmapyz_2(0,3,1200,0,0.75,300,0.005,retveloc=5)
	dkmapyz_2(0,3,1200,0.75,1.5,300,0.005,retveloc=5)
	
	
