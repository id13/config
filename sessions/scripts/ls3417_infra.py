ROICTR_TPL = 'eiger:roi_counters:roi{:1d}_avg'
def get_roiavg(i, nn=-1, shape=None):
    return SCANS[nn].get_data(ROICTR_TPL.format(i)).reshape(shape).copy()

def backsub_plot(sig, *backs, nn=-1, shape=None):
    nbkg = len(backs)
    sig = get_roiavg(sig, nn=nn, shape=shape)
    brr = sig*0
    for ib in backs:
        brr += get_roiavg(ib, nn=nn, shape=shape)
    brr = brr/float(nbkg)
    return (sig - brr), sig, brr
