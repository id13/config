referencePosition = 0
stageMode = 0 # 0 for Bliss, 1 for Manual

def measureSetZReference():
  global stageMode
  sm = stageMode
  if stageMode == 1:
    modeBliss()
  globals()["referencePosition"] = microz.position
  if sm == 1:
    modeManual()

def measureDeltaZ():
  global referencePosition, stageMode
  sm = stageMode
  if stageMode == 1:
    modeBliss()
  print("Delta Z = {0:.0f} um".format((microz.position - referencePosition)*1000))
  if sm == 1:
    modeManual()

def setResolution(resZ=5, resXY=5):
  if resXY < 3 or resZ < 3 or resXY > 7 or resZ > 7:
    print("Parameter out of range (3..7)")
  else:
    icepaptrack.stage_resolution(2**resXY, 2**resXY, 2**resZ)

def modeBliss():
  globals()['stageMode'] = 0
  icepaptrack.stage_disable()
  #print('BLISS mode')

def modeManual():
  globals()['stageMode'] = 1
  icepaptrack.stage_enable()
  #print('MANUAL mode')

modeBliss()
print('BLISS mode activated')
