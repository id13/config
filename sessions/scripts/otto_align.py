print('load otto_align - 1')

from collections import OrderedDict as Odict
from scipy import ndimage
class NoScanFound(Exception): pass

# legend:
#     binobject : binary object

#
# ALIGN
#

class AxisGeometry(object):

    # ap : absolute motor pos
    # nom_stp : nominal motor step size
    # n : number of intervals or continuous scan sampling points
    # lgeo : line geometry lower limit, upperlimit, npoints
    # bi : bound index

    def __init__(self, lgeo, nom_stp=None):
        self.lgeo = (llap, ulap, n) = lgeo
        self.ap_width = ap_width = ulap - llap
        self.ap_stp = ap_width/(1.0*n)
        self.nom_stp = nom_stp

    def compute_ap_boundgeo(self, bi_lgeo, nom_stp=None):
        if None is nom_stp:
            nom_stp = self.nom_stp
        (llap, ulap, n) = self.lgeo
        ap_stp = self.ap_stp
        (ll_bi, ul_bi) = bi_lgeo
        bap_ll = ap_stp*ll_bi + llap
        bap_ul = ap_stp*ul_bi + llap
        bap_width = bap_ul - bap_ll
        n_fl = round(bap_width/nom_stp)
        print("n_fl", n_fl)
        new_n = int(n_fl) + tend
        new_bap_ll = bap_ll
        new_bap_ul = bap_ll + new_n*nom_stp
        return (new_bap_ll, new_bap_ul, new_n)


class MeshScanGeometry(object):

    def __init__(self, mesh_lgeo, nom_steps):
        lgeo0, lgeo1 = mesh_lgeo
        stp0, stp1 = nom_steps
        self.axgeo0 = AxisGeometry(lgeo0, nom_stp=nom_stp0)
        self.axgeo1 = AxisGeometry(lgeo1, nom_stp=nom_stp1)

class PatchInfra(object):

    def __init__(self):
        pass

class MiniOdakInfra(object):

    def __init__(self, def_ctr_names, data_fapth=None):
        self.def_ctr_names = def_ctr_names
        if None is data_fapth:
            self.data_fpath = get_current_data_fapth()
        else:
            self.data_fapth = data_fapth

    def make_scanname(self, scanno, sub_scanno=1):
        return f'{scanno:1d}.{sub_scanno:1d}'

    def get_last_scanno(self):
        try:
            return int(SCANS[-1].scan_number)
        except IndexError:
            raise NoScanFound()

    def get_last_scan(self):
        try:
            return SCANS[-1]
        except IndexError:
            raise NoScanFound()

    def read_scaninfo(self, scanno, counter_names=None, sub_scanno=1):
        scan_name = self.make_scanname(scanno, sub_scanno)
        if None is counter_names:
            counter_names = self.def_ctr_names
        with H5FileReadonly(self.data_fapth) as h5f:
            scn = h5f[scan_name]
        res_dc = mesh_read_counter_data(scn, counter_names)
        return res_dc

        

#
# PILLAR V1
#


def parse_pillar_map_v1(map_arr, threshold, sum_thresh,
    reflength, outfxpath_tpl):
    keys = "raw med thresh anno".split()
    ecks = EDFClosedKeySet_RW(outfxpath_tpl, keys)
    s0, s1 = map_arr.shape
    md_arr = ndi.median_filter(map_arr, 7)
    th_arr = np.where(md_arr > threshold, 1, 0)
    anno_arr = th_arr.copy()
    ecks['raw'] = map_arr
    ecks['med'] = md_arr
    ecks['thresh'] = th_arr
    hpix_arr = np.arange(s1)


    top_hit = -1
    idx_ll = []
    th_thesum_ll = []
    raw_themean_ll = []
    raw_thevar_ll = []
    found_flg = False
    for i in range(s0):
        idx_ll.append(i)
        th_thesum = th_arr[i].sum()
        raw_themean = map_arr[i].mean()
        raw_thevar = map_arr[i].var()
        th_thesum_ll.append(th_thesum)
        raw_themean_ll.append(raw_themean)
        raw_thevar_ll.append(raw_thevar)
        if not found_flg:
            if th_thesum > sum_thresh:
                top_hit = i
                found_flg = True
                hor_cog = compute_cog_1d(hpix_arr, th_arr)
    th_thesum_arr = np.array(th_thesum_ll, dtype=np.int32)
    th_themean_arr = np.array(th_thesum_ll, dtype=np.float64)
    th_thevar_arr = np.array(th_thesum_ll, dtype=np.float64)
    #pprint(list(zip(thesum_ll, themean_ll, thevar_ll)))
    search_arr = th_thesum_arr[0: top_hit+reflength]
    vert_max = search_arr.max()
    vert_argmax = search_arr.argmax()
    refline=th_arr[vert_argmax]
    print('top_hit =', top_hit, '  hor_cog=', hor_cog)
    print('vert_argmax =', vert_argmax, '  vert_max=', vert_max)
    samp_indices = np.compress(refline, hpix_arr)
    print(samp_indices)
    low_hor_idx = samp_indices[0]
    hi_hor_idx = samp_indices[-1]
    print('low_hor_idx =', low_hor_idx, '  hi_hor_idx=', hi_hor_idx)
    anno_arr[top_hit,int(hor_cog)]  += 2
    anno_arr[vert_argmax,low_hor_idx]  += 2
    anno_arr[vert_argmax,hi_hor_idx]  += 2
    ecks['anno'] = anno_arr

    return top_hit, hor_cog, vert_argmax, vert_max, low_hor_idx, hi_hor_idx

class Pillar1(object):

    def __init__(self, zthresh=5, nwidth_thresh=4, reflength=None,
        out_tag='somedefault'):
        self.zthresh = zthresh
        self.nwidth_thresh = nwidth_thresh
        self.reflength = reflength
        self.out_tag = out_tag

    def seg_parse(self, scanno, res, counter_name):
        out_tpl = '%s_%04d_%s_{}.edf' % (
            self.out_tag, scanno, counter_name)
        map_arr = res[0][counter_name]
        dc = res[1]
        parse_res = parse_pillar_map_v1(map_arr, 
            self.zthresh, self.nwidth_thresh, self.reflength, out_tpl)
        return parse_res

#
# PILLAR V2
#

# direction: 'top', 'bottom', 'left', right'
#            it indicates where the search starts i.e. 'top' means it starts
#            at the top and goes to the bottom

def to_binary_imobj(ximobj, threshold=0):
    return np.where(ximobj > threshold , 1, 0)

def npxbound_clip_binimobj(self, binimobj, minwidth, direction=None):
    s0, s1 = binimobj.shape
    site_idx = None
    
    if 'top' == direction:
        for i in range(s0):
            thesum = binimobj[i,:].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'bottom' == direction:
        for i in range(s0-1, -1,-1):
            thesum = binimobj[i,:].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'left' == direction:
        for i in range(s1):
            thesum = binimobj[:,i].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'right' == direction:
        for i in range(s1-1, -1,-1):
            thesum = binimobj[:,i].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    return (site_idx, thesum)


def parse_pillar_map_v2(map_arr, threshold, sum_thresh, medflt_size,
    reflength, outfxpath_tpl):

    # OUT
    keys = "raw med thresh anno label marked".split()
    ecks = EDFClosedKeySet_RW(outfxpath_tpl, keys)

    

    s0, s1 = map_arr.shape
    md_arr = ndi.median_filter(map_arr, medflt_size)
    th_arr = np.where(md_arr > threshold, 1, 0)
    anno_arr = th_arr.copy()
    (label_arr, nfeatures) = ndi.label(th_arr)
    objects = ndi.find_objects(label_arr)
    n_obj = len(objects)
    print (n_obj, nfeatures)
    pprint(objects)
    
    ecks['raw'] = map_arr
    ecks['med'] = md_arr
    ecks['label'] = label_arr
    ecks['thresh'] = th_arr
    if n_obj == 1:
        arr = map_arr.copy()
        sl0, sl1 = objects[0]
        a0 = sl0.start
        b0 = sl0.stop-1
        a1 = sl1.start
        b1 = sl1.stop-1
        arr[a0,a1:b1] = 1000
        arr[b0,a1:b1] = 1000
        arr[a0:b0,a1] = 1000
        arr[a0:b0,b1] = 1000
        ecks['marked'] = arr
    return objects

COUNTER_DICT = Odict(
    Ca = 'fx1_det0_Ca',
)

class Pillar2(object):

    def __init__(self, zthresh=5, nwidth_thresh=4, reflength=None,
        medflt_size=7, out_tag='somedefault', counter_dict={}):
        self.counter_dict = counter_dict
        self.convctr_dict = [dict(v,k) for (k,v) in counter_dict]
        self.counter_names = [counter_dict[k] for k in counter_dict]
        self.zthresh = zthresh
        self.nwidth_thresh = nwidth_thresh
        self.medflt_size = medflt_size
        self.reflength = reflength
        self.out_tag = out_tag

    #def retrieve_scaninfo(self):
    #    scan_info =
        

    #def get_ndiobjects

    def seg_parse(self, scanno, res, counter_name):
        out_tpl = '%s_%04d_%s_{}.edf' % (
            self.out_tag, scanno, counter_name)
        map_arr = res[0][counter_name]
        dc = res[1]
        parse_res = parse_pillar_map_v2(map_arr, 
            self.zthresh, self.nwidth_thresh, self.medflt_size, self.reflength, out_tpl)
        return parse_res
