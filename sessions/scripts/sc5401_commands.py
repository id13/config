

load_script('karl_patch_scan')

#
# BY106
#

positions = '''
BY106_empty_0006.json
BY106_patch1_0000.json
BY106_patch2_0001.json
BY106_patch3_0002.json
BY106_patch4_0003.json
BY106_patch5_0004.json
BY106_patch6_0005.json
'''

def BY106_test_raddam_macro():
    START_KEY = 'a_darktest'
    POS = 'patch1'
    newdataset(f'{START_KEY}_raddam_{POS}')
    radiation_damage_scan(75, 75, 1, 1, 50, 50, 0.002, timeout='auto',
        repeats=2)
        
def BY106_test2_raddam_macro():
    # INPUT
    START_KEY = 'b_darktest2'
    POS = 'patch1'
    WIDTH = 126.0
    #RESOL = 0.25
    RESOL = 1
    EXPT = 0.002
    REPEATS = 2
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def corr_gopos(pn):
    rstp(pn)
    mvr(nnz, -0.117)
    mvr(nny, 0.117)
    
def BY106_patch1_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch1'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 2
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY106_patch1_0000.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)

def BY106_corr_patch1_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'corr_patch1'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    corr_gopos('BY106_patch1_0000.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
def BY106_corr_patch2_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'corr_patch2'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    corr_gopos('BY106_patch2_0001.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)

def BY106_corr_patch3_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'corr_patch3'
    WIDTH = 100.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    corr_gopos('BY106_patch3_0002.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
def BY106_corr_patch4_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'corr_patch4'
    WIDTH = 100.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    corr_gopos('BY106_patch4_0003.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
def BY106_corr_patch6_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'corr_patch6'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    corr_gopos('BY106_patch6_0005.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)

def MS39_freezedry_patch_scan_night2():
    # INPUT
    START_KEY = 'b'
    DSNAME_TAG = 'night2'
    
    # piezo
    START_P = 75
    DP = 0.25
    NP = 400
    
    #hexa
    DY = 0.1
    DZ = 0.1
    NY = 10
    NZ = 10
    
    EXPT = 0.002
    
    rstp('MS39_window_corner_0010.json')

    start_nny = nny.position
    start_nnz = nnz.position
    
    params = tuple((
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    ))
    print ("params:")
    print(params)
    hexayz_nnp23_patch_scan(
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    )
    
    
def BY112_patch1_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch1'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch1_0001.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
def BY112_patch2_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch2'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch2_0002.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)

def BY112_patch3_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch3'
    WIDTH = 126.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch3_0003.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def BY112_patch4_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch4'
    WIDTH = 100.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch4_0004.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)

def BY112_patch5_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch5'
    WIDTH = 100.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch5_0005.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def BY112_patch6_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch6'
    WIDTH = 120.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY112_patch6_0006.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def BY113_patch1_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch1'
    WIDTH = 100.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch1_0001.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def BY113_patch2_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch2'
    WIDTH = 125.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch2_0002.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        

def BY113_patch3_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch3'
    WIDTH = 125.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch3_0003.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS) 
        
def BY113_patch4_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch4'
    WIDTH = 125.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch4_0004.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS) 
        
def BY113_patch4__resume_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch4'
    WIDTH = 125.0
    RESOL = 0.25
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    #newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch4_0004.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)   


def BY113_patch5_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch5'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    #newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch5_0005.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)       
        
        
def BY113_patch6_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch6'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch6_0006.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS) 
        
       
def BY113_patch7_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch7'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch7_0009.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)          
        

def BY113_patch8_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch8'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch8_0010.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)          


def BY113_patch9_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch9'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch9_0011.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)


def BY113_patch10_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch10'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch10_0012.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
        
def BY113_patch11_raddam_macro():
    # INPUT
    START_KEY = 'a'
    POS = 'patch11'
    WIDTH = 60.0
    RESOL = 0.5
    #RESOL = 1
    EXPT = 0.002
    REPEATS = 50
    
    #
    h_width = WIDTH/2
    start_p = 125-h_width
    np = WIDTH/RESOL
    i_np = int(np)
    if np - i_np:
        raise ValueError(f'stepsize {RESOL} does not fit into {WIDTH}') 
    
    nnp1.sync_hard()
    zeronnp()
    
    newdataset(f'{START_KEY}_raddam_{POS}')
    gopos('BY113_patch11_0013.json')
    
    so()
    sleep(2)

    print('params:')
    print(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT)
    radiation_damage_scan(start_p, start_p, RESOL, RESOL, i_np, i_np, EXPT, timeout='auto',
        repeats=REPEATS)
        
        
def MS34_freezedry_patch_scan_night3():
    # INPUT
    START_KEY = 'b'
    DSNAME_TAG = 'night3'
    
    # piezo
    START_P = 75
    DP = 0.25
    NP = 400
    
    #hexa
    DY = 0.1
    DZ = 0.1
    NY = 10
    NZ = 10
    
    EXPT = 0.002
    
    rstp('MS34_upperleft_0005.json')

    start_nny = nny.position
    start_nnz = nnz.position
    
    params = tuple((
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    ))
    print ("params:")
    print(params)
    hexayz_nnp23_patch_scan(
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    )
    
def MS34_redo_freezedry_patch_scan_night3():
    # INPUT
    START_KEY = 'b'
    DSNAME_TAG = 'night3'
    
    # piezo
    START_P = 75
    DP = 0.25
    NP = 400
    
    #hexa
    DY = 0.1
    DZ = 0.1
    NY = 10
    NZ = 10
    
    EXPT = 0.002
    
    rstp('MS34_upperleft_0005.json')

    start_nny = nny.position
    start_nnz = nnz.position
    
    params = tuple((
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    ))
    print ("params:")
    print(params)
    hexayz_nnp23_patch_scan(
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    )


def MS34_redo2_freezedry_patch_scan_night3():
    # INPUT
    START_KEY = 'b'
    DSNAME_TAG = 'night3'
    
    # piezo
    START_P = 75
    DP = 0.25
    NP = 400
    
    #hexa
    DY = 0.1
    DZ = 0.1
    NY = 10
    NZ = 10
    
    EXPT = 0.002
    
    rstp('MS34_upperleft_0005.json')

    start_nny = nny.position
    start_nnz = nnz.position
    
    params = tuple((
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    ))
    print ("params:")
    print(params)
    hexayz_nnp23_patch_scan(
        DSNAME_TAG,
        start_nny, start_nnz, DY, DZ, NY, NZ,
        START_P, START_P, DP, DP, NP, NP,
        EXPT
    )
