print('karl_patch_scan3')
import gevent,bliss, time
from os import path

def make_patch_scan_dsname(dsname_tag, i, j):
    return f'{dsname_tag}_{i:02}_{j:02}'

def doo_kmap(
    start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout='auto'):
    """
    Function to perfrom a map scan the specified region.
    
    @param start_p2 	start position nnp2 motor
    @param start_p3 	start position nnp3 motor
    @param dp2		step size along nnp2
    @param dp3		step size along nnp3
    @param np2		number of points along nnp2
    @param np3 		number of points along nnp3
    @param expt 	exposure time in s
    @pram timeout 	default: auto, time until an image is exported to the output 	
    """
    mv(nnp2, start_p2)
    mv(nnp3, start_p3)
    width_p2 = dp2*np2
    width_p3 = dp3*np3
    est_timeout = np2*np3*(expt+0.0007)*1.2 + 40
    print(f'estimated timeout = {est_timeout}')
    if 'auto' == timeout:
        timeout = est_timeout
    print(f'patch kmap: timeout={timeout}')
    t0 = time.time()
    try:
        with gevent.Timeout(seconds=timeout):
            loff_kmap(nnp2, 0, width_p2, np2, nnp3, 0, width_p3, np3, expt)
            fshtrigger()
            sync()
            elog_plot(scatter=True)
            return 0
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print(f'caught hanging scan after {time.time() - t0} seconds timeout')
        return 1

class MyAbort(Exception): pass

def check_abort():
    """
    If a scan is aborted an entry ./stopit is created. This function checks for this file
    @raise MyAbort exception if the file ./stopit exists
    """
    if path.exists('./stopit'):
    	raise MyAbort()
	

def wrapper_radiation_damage_scan(dsname, start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout='auto', repeats=1):
    newdataset(dsname)
    for i in range(repeats):
        radiation_damage_scan(dsname, start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout, repeats=1)     
        # Change the timeout from string to real since gevent expects real
        if 'auto' == timeout:
            timeout = 20.0 # s to export the plot
        with gevent.Timeout(seconds=timeout):            
    	    elog_plot(dlvlm1=True)
	
def radiation_damage_scan(dsname_tag, start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout='auto', repeats=1):
    """
    A function that performs a radiation damage scan by repeatedly scan the same area with a map scan.
     
    @param dsname_tag 	name of the dataset, currently without effect
    @param start_p2 	start position nnp2 motor
    @param start_p3 	start position nnp3 motor
    @param dp2		step size along nnp2
    @param dp3		step size along nnp3
    @param np2		number of points along nnp2
    @param np3 		number of points along nnp3
    @param expt 	exposure time in s
    @param timeout 	default: auto, time until an image is exported to the output
    @param repeats 	default: 1, number of repeated scans of the same area  
    
    """
    for i in range(repeats):
        check_abort()
        print(f'==== radiation_damage_scan ================: scan {i+1} of {repeats}')
        if None == timeout:
            loff_kmap(nnp2, 0, width_p2, np2, nnp3, 0, width_p3, np3, expt)
        else:
            with bench():
                doo_kmap(start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout=timeout)
        print('5 seconds to interrupt ...')
        time.sleep(5)
        print('do not interrupt!')

def hexayz_nnp23_patch_scan(dsname_tag,
    start_y, start_z, dy, dz, ny, nz,
    start_p2, start_p3, dp2, dp3, np2, np3,
    expt, timeout='auto', yspc=0.0, zspc=0.0, skip=0):
    """
    This is the movement of the hexapod, right.
    
    @param dsname_tag 	name of the dataset that is newly created in this function
    @param start_y 	start position of y axis
    @param start_z 	start position of z axis
    @param dy		step size y
    @param dz 		step siye z
    @param ny 		number of steps y
    @param nz 		number of steps z
    @param start_p2 	start position piezo motor in p2 direction <-> y direction
    @param start_p3	start position piezo motor in p3 direction <-> z direction
    @param dp2
    @param dp3
    @param np2
    @param np3
    @param expt 	exposure time in s
    @param timeout 	default: auto, time until an image is exported to the output
    @param zspc 	default: 0.0
    @param zspc 	default: 0.0
    @param skip 	default: 0  
    """
    ctr = 0
    for i in range(nz):
        for j in range(ny):
            print('==== new patch ================')
            print('checking abort request')
            check_abort()
            print(f'internal ctr = {ctr}')
            dsname = make_patch_scan_dsname(dsname_tag, i, j)
            if ctr < skip:
                print(f'!!!!!!!!!!: skipping {dsname}')
                ctr += 1
                continue
            newdataset(dsname)
            new_cur_y = start_y + j*(dy + yspc) 
            new_cur_z = start_z + i*(dz + zspc)
            print(f'dsname: {dsname}')
            print(f'requested pos: y =  {new_cur_y}    z = {new_cur_z}\n')
            mv(nny, new_cur_y)
            mv(nnz, new_cur_z)
            print(f'actual pos:    y =  {nny.position}    z = {nnz.position}\n')
            doo_kmap(start_p2, start_p3, dp2, dp3, np2, np3, expt,
                timeout=timeout)
            print('5 sec to interrupt ...')
            time.sleep(5)
            ctr += 1
