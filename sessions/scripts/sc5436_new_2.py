load_script('karl_patch_scan3')



def conv_patch_scan_1(dsname_tag, p_scan_par, h_patch_par):
    nny_pos = nny.position
    nnz_pos = nnz.position
    try:
        args = (dsname_tag,) + (nny_pos, nnz_pos) + h_patch_par + p_scan_par
        hexayz_nnp23_patch_scan(*args)
    finally:
        mv(nny, nny_pos)
        mv(nnz, nnz_pos)

P_SCAN_PARAMS_2 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_2 = (0.1, 0.1, 1, 1)
def oneoff_002():
    conv_patch_scan_1('test2', P_SCAN_PARAMS_2, H_PATCH_PARAMS_2)

    
def radam_003():
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('radam_test',75.0, 75.0, 0.2, 0.2, 500, 500, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)

def radam_004():
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('radam_test',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)
	
P_SCAN_PARAMS_3 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_3 = (0.1, 0.1, 1, 1)
def oneoff_003():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_2, H_PATCH_PARAMS_2)
    
    
P_SCAN_PARAMS_4 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_4 = (0.1, 0.1, 2, 1)
def oneoff_004():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_4, H_PATCH_PARAMS_4)
    

def radam_005():
    rstp('ROI3._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        wrapper_radiation_damage_scan('ROI3',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=1)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)
	
	
P_SCAN_PARAMS_5 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_5 = (0.1, 0.1, 3, 4)
def oneoff_005():
    rstp('ROI4._pos')
    conv_patch_scan_1('ROI4', P_SCAN_PARAMS_5, H_PATCH_PARAMS_5)
    

P_SCAN_PARAMS_6 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_6 = (0.1, 0.1, 4, 3)
def oneoff_006():
    rstp('ROI5._pos')
    conv_patch_scan_1('ROI5', P_SCAN_PARAMS_6, H_PATCH_PARAMS_6)
    
P_SCAN_PARAMS_7 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_7 = (0.1, 0.1, 4, 4)
def oneoff_007():
    rstp('ROI6._pos')
    conv_patch_scan_1('ROI6', P_SCAN_PARAMS_7, H_PATCH_PARAMS_7)
    
P_SCAN_PARAMS_8 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_8 = (0.1, 0.1, 1, 1)  
def oneoff_008():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_8, H_PATCH_PARAMS_8)
    
P_SCAN_PARAMS_9 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_9 = (0.1, 0.1, 1, 1)  
def oneoff_009():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_9, H_PATCH_PARAMS_9)
    
        
P_SCAN_PARAMS_10 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_10 = (0.1, 0.1, 1, 1)  
def oneoff_010():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_10, H_PATCH_PARAMS_10)
    
P_SCAN_PARAMS_11 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_11 = (0.1, 0.1, 1, 1)  
def oneoff_011():
    rstp('ROI4._pos')
    conv_patch_scan_1('ROI4', P_SCAN_PARAMS_11, H_PATCH_PARAMS_11)
    
P_SCAN_PARAMS_12 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_12 = (0.1, 0.1, 1, 1)  
def oneoff_012():
    rstp('ROI5._pos')
    conv_patch_scan_1('ROI5', P_SCAN_PARAMS_12, H_PATCH_PARAMS_12)
        
P_SCAN_PARAMS_13 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_13 = (0.1, 0.1, 1, 1)  
def oneoff_013():
    rstp('ROI6._pos')
    conv_patch_scan_1('ROI6', P_SCAN_PARAMS_13, H_PATCH_PARAMS_13)            


P_SCAN_PARAMS_14 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_14 = (0.1, 0.1, 2, 2)  
def oneoff_014():
    rstp('ROI7._pos')
    conv_patch_scan_1('ROI7', P_SCAN_PARAMS_14, H_PATCH_PARAMS_14) 


P_SCAN_PARAMS_15 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_15 = (0.1, 0.1, 13, 13)  
def oneoff_015():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_15, H_PATCH_PARAMS_15) 
    
P_SCAN_PARAMS_16 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_16 = (0.1, 0.1, 2, 2)  
def oneoff_016():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_16, H_PATCH_PARAMS_16) 


P_SCAN_PARAMS_17 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_17 = (0.1, 0.1, 2, 2)  
def oneoff_017():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_17, H_PATCH_PARAMS_17) 
    
    
P_SCAN_PARAMS_18 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_18 = (0.1, 0.1, 2, 2)  
def oneoff_018():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_18, H_PATCH_PARAMS_18) 

P_SCAN_PARAMS_19 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_19 = (0.1, 0.1, 5, 5)  
def oneoff_019():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_19, H_PATCH_PARAMS_19) 

P_SCAN_PARAMS_20 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.004)
H_PATCH_PARAMS_20 = (0.1, 0.1, 2, 2)  
def oneoff_020():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_20, H_PATCH_PARAMS_20) 

P_SCAN_PARAMS_21 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_21 = (0.1, 0.1, 2, 2)  
def oneoff_021():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_20, H_PATCH_PARAMS_21) 


P_SCAN_PARAMS_22 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_22 = (0.1, 0.1, 2, 2)  
def oneoff_022():
    rstp('ROI4._pos')
    conv_patch_scan_1('ROI4', P_SCAN_PARAMS_22, H_PATCH_PARAMS_22) 

P_SCAN_PARAMS_23 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_23 = (0.1, 0.1, 2, 2)  
def oneoff_023():
    rstp('ROI5._pos')
    conv_patch_scan_1('ROI5', P_SCAN_PARAMS_23, H_PATCH_PARAMS_23)
    
    
    
def radam_006():
    rstp('ROI6._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        wrapper_radiation_damage_scan('ROI6',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)
	 
def radam_007():
    rstp('ROI6._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI6',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=9)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)
	 

def radam_008():
    rstp('ROI7._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI7',75.0, 75.0, 0.2, 0.5, 500, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)
	
	
P_SCAN_PARAMS_24 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_24 = (0.1, 0.1, 5, 5)  
def oneoff_024():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_24, H_PATCH_PARAMS_24) 

	
P_SCAN_PARAMS_25 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_25 = (0.1, 0.1, 5, 5)  
def oneoff_025():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_25, H_PATCH_PARAMS_25) 

	 
P_SCAN_PARAMS_26 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.002)
H_PATCH_PARAMS_26 = (0.1, 0.1, 3, 2)  
def oneoff_026():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_26, H_PATCH_PARAMS_26) 

	 
P_SCAN_PARAMS_27 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_27 = (0.1, 0.1, 3, 2)  
def oneoff_027():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_27, H_PATCH_PARAMS_27) 
    
    
def radam_009():
    rstp('ROI1._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI1',75.0, 75.0, 0.2, 0.5, 500, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)    


def radam_010():
    rstp('ROI2._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI2',75.0, 75.0, 0.2, 0.5, 500, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)    



def radam_011():
    rstp('ROI3._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI3',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)    


def radam_012():
    rstp('ROI4._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI4',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3) 



P_SCAN_PARAMS_28 = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.002)
H_PATCH_PARAMS_28 = (0.1, 0.1, 1, 4)  
def oneoff_028():
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_28, H_PATCH_PARAMS_28) 

P_SCAN_PARAMS_29 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_29 = (0.1, 0.1, 2, 3)  
def oneoff_029():
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_29, H_PATCH_PARAMS_29) 

P_SCAN_PARAMS_30 = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_30 = (0.1, 0.1, 2, 3)  
def oneoff_030():
    rstp('ROI3._pos')
    conv_patch_scan_1('ROI3', P_SCAN_PARAMS_30, H_PATCH_PARAMS_30) 
	 

# First region
P_SCAN_PARAMS_31a = (75.0, 75.0, 0.5, 0.5, 200, 200, 0.004)
H_PATCH_PARAMS_31a = (0.1, 0.1, 6, 5) 
# Second region
P_SCAN_PARAMS_31b = (75.0, 75.0, 0.25, 0.25, 400, 400, 0.004)
H_PATCH_PARAMS_31b = (0.1, 0.1, 2, 2)  
   
def oneoff_031():
    # First region
    rstp('ROI1._pos')
    conv_patch_scan_1('ROI1', P_SCAN_PARAMS_31a, H_PATCH_PARAMS_31a) 
    # Second region
    rstp('ROI2._pos')
    conv_patch_scan_1('ROI2', P_SCAN_PARAMS_31b, H_PATCH_PARAMS_31b) 
    # Thrid region



def radam_014():
    rstp('ROI2._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI2',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)


def radam_015():
    rstp('ROI3._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI3',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)



def radam_016():
    rstp('ROI4._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI4',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)



def radam_017():
    rstp('ROI5._pos')
    nnp2 = nny.position
    nnp3 = nnz.position    
    try:
        radiation_damage_scan('ROI5',75.0, 75.0, 0.5, 0.5, 200, 200, 0.002, timeout='auto',repeats=10)
    finally:
        mv(nny, nnp2)
        mv(nnz, nnp3)






# example    
#def oneoff_multi_001():
#    oneoff_018()
#    oneoff_019()
#    oneoff_020()
	         
def mymgeig():
    mgeig()
    MG_EH3a.enable('*r:r*')







