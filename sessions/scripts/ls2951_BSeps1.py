import time

def dummy(*p):
    print("dummy:",p)

def emul(*p):
    (ph,pv) = p[-2:]
    print("emul:",p)
    mvr(ustry,ph*0.4,ustrz,pv*0.4)

# newdataset = dummy
# enddataset = dummy
# dkpatchmesh = emul
print("=== ls2951_BSeps1 ARMED ===")

def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(ph,pv):
    dkpatchmesh(0.4,200,0.4,200,0.02,ph,pv)



DSKEY='a'

def dooul(s):
    print("\n\n\n======================== doing:", s)
    w = s.split('_')
    (ph,pv) = w[-1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))
    dsname = "%s_%s" % (s, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()
    print("\ndooscan[patch mesh]:")
    dooscan(ph,pv)
    wate2()
    print("\nenddataset:")
    enddataset()

    

def ls2951_BSeps1():


ul01_1x1_100x100_200_20
ul02_1x1_200x200_200_20
ul03_1x1_100x100_1000_5
ul04_1x1_100x100_1000_5
ul05_1x1_100x100_1000_5
ul06_1x1_100x100_1000_5

    dooul('ul01_2x2')
    dooul('ul02_2x2')
    dooul('ul03_2x2')
    dooul('ul04_2x2')
    dooul('ul05_2x2')
    dooul('ul07_3x3')
    dooul('ul08_2x2')
