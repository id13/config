print('hello world1')

def safety_net():
    for i in range(1,5):
        timer = 5 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)

def nighty_night():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('night1_start')
	newdataset('pinna_nanodiffraction_1_b')
	fshtrigger()
	#theta backlash corr
	theta_start = 2
	theta_incr = 0.1
	max_steps = 60
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()
	
def calamari_dreams():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('calamari_nanodiffraction_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -1.
	theta_incr = 0.015
	max_steps = 135
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-5,5,100,nnp3,-5,5,100,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def fishing_pinna():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('night1_start')
	newdataset('pinna_fishing_2')
	fshtrigger()
	#theta backlash corr
	theta_start = -10
	theta_incr = 1
	max_steps = 20
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-100,100,100,nnp3,-50,50,50,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def fishing_pinna3():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('pinna_fishing_3_c')
	fshtrigger()
	#theta backlash corr
	theta_start = -10
	theta_incr = 0.5
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-110,110,100,nnp3,-110,110,100,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()
def fishing_pinna4():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('pinna_fishing_4_b')
	fshtrigger()
	#theta backlash corr
	theta_start = -10
	theta_incr = 0.5
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-110,110,100,nnp3,-80,40,60,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()
	
def do_bragg_pinna_test2():
	so()
	newdataset('test2_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 7.375
	theta_incr = 0.005
	max_steps = 50
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	
def do_bragg_pinna_ref1():
	so()
	newdataset('ref1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 7.395
	theta_incr = 0.005
	max_steps = 50
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,7)
	dscan(Theta,0,1,200,0.05)
	
def do_bragg_pinna_edge1():
	so()
	newdataset('edge1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 7.395
	theta_incr = 0.005
	max_steps = 50
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,7)
	dscan(Theta,0,1,200,0.05)
	
	
def do_bragg_pinna_center1():
	so()
	newdataset('center1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 7.395
	theta_incr = 0.005
	max_steps = 50
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,7)
	dscan(Theta,0,1,200,0.05)


def do_bragg_pinna_corner_science1():
	so()
	newdataset('corner_science1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 7.375
	theta_incr = 0.005
	max_steps = 60
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-1.125,1.125,48,nnp2,-1.125,1.125,48,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,7)
	dscan(Theta,0,1,200,0.05)
	
	
def chalky_dreams():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('chalky_blobb_nanodiffraction_1_c')
	fshtrigger()
	#theta backlash corr
	theta_start = -1.0
	theta_incr = 0.1
	max_steps = 20
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-6,6,60,nnp3,-6,6,60,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def do_bragg_chalk1():
	so()
	newdataset('chalk_1_bragg_a')
	fshtrigger()
	gox2(129.99,153.704)
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.004
	max_steps = 75
	exp_time = 0.05
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	
def do_bragg_chalk2():
	so()
	newdataset('chalk_2_bragg_a')
	fshtrigger()
	gox2(131.34,154.21)
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.004
	max_steps = 75
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	
def do_bragg_chalk3():
	so()
	newdataset('chalk_3_bragg_a')
	fshtrigger()
	gox2(130.96,155.07)
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.004
	max_steps = 75
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	
def do_bragg_chalk4():
	so()
	newdataset('chalk_4_bragg_a')
	fshtrigger()
	gox2(130.18,155.83)
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	#theta backlash corr
	theta_start = -0.1
	theta_incr = 0.004
	max_steps = 100
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
def do_bragg_chalk5():
	so()
	newdataset('chalk_5_bragg_a')
	fshtrigger()
	gox2(130.16,154.95)
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
	#theta backlash corr
	theta_start = -0.1
	theta_incr = 0.004
	max_steps = 100
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.5)
	dscan(Theta,0,1,100,0.05)
def do_all():
	do_bragg_chalk4()
	do_bragg_chalk5()

def do_bragg_now():
	so()
	newdataset('victory_bragg_4_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.2)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.2
	theta_incr = 0.01
	max_steps = 50
	exp_time = 0.3
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.2)
	dscan(Theta,0,0.6,60,0.02)
	
def do_bragg_shoot():
	so()
	newdataset('shootingstar_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.25)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.25
	theta_incr = 0.01
	max_steps = 30
	exp_time = 0.05
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.25)
	dscan(Theta,0,0.6,60,0.02)


def do_bragg_twinkle():
	so()
	newdataset('twinkle_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.25)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.1
	theta_incr = 0.01
	max_steps = 50
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.25)
	dscan(Theta,0,0.6,60,0.02)


def do_bragg_etoile():
	so()
	newdataset('etoile_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.45)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.45
	theta_incr = 0.01
	max_steps = 30
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.45)
	dscan(Theta,0,0.6,60,0.02)
	
def do_bragg_scheisse():
	so()
	newdataset('scheisse_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.3
	theta_incr = 0.01
	max_steps = 30
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.6,60,0.02)
	
def do_bragg_granite():
	so()
	newdataset('granite_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,0)
	dscan(Theta,0,0.6,60,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = 0
	theta_incr = 0.01
	max_steps = 30
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,0)
	dscan(Theta,0,0.6,60,0.02)
	
def do_bragg_for_a_friend():
	so()
	newdataset('friendly_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.2)
	dscan(Theta,0,0.45,45,5)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.2
	theta_incr = 0.01
	max_steps = 45
	exp_time = 5
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.2)
	dscan(Theta,0,0.45,45,5)
	
def tinydancer():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('pos7')
	
	newdataset('tinydancer_pos7_b')
	fshtrigger()
	#theta backlash corr
	theta_start = -3
	theta_incr = 0.1
	max_steps = 60
	exp_time = 0.003
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def triangle():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('pos4')
	gox2(175.35,101.33)
	newdataset('triangle_pos4_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -5.8
	theta_incr = 0.1
	max_steps = 116
	exp_time = 0.003
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()
	
def do_stupid_stuff():
	triangle()
	tinydancer()
	
def do_bragg_for_a_chonk():
	so()
	newdataset('chonk_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.5,50,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.3
	theta_incr = 0.01
	max_steps = 50
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.5,50,0.02)
def do_bragg_for_a_chonk2():
	so()
	newdataset('chonk_bragg_2_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.5,50,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.3
	theta_incr = 0.01
	max_steps = 50
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.3)
	dscan(Theta,0,0.5,50,0.02)
	
def do_bragg_for_a_chalk():
	so()
	newdataset('chalk_prison_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.35)
	dscan(Theta,0,0.55,55,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.35
	theta_incr = 0.01
	max_steps = 55
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.35)
	dscan(Theta,0,0.55,55,0.02)


def do_bragg_killemall():
	so()
	newdataset('hit_bragg_1_a')
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.85)
	dscan(Theta,0,0.4,40,0.02)
	#theta backlash corr
	umv(Theta,-2)
	theta_start = -0.85
	theta_incr = 0.01
	max_steps = 40
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,-0.85)
	dscan(Theta,0,0.4,40,0.02)
	
def walk():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('new_pos1')
	gox2(123.64,117.57)
	newdataset('pos1_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()
	
def apero():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('new_pos3b')
	zeronnp()
	newdataset('pos3b_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def pizza():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('new_pos5a')
	gox2(110.12,117.92)
	newdataset('pos5a_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def dessert():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('new_pos4')
	gox2(35.58,139.40)
	newdataset('pos4b_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def encore():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	rstp('new_pos6')
	gox2(128,120.05)
	newdataset('pos6_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()


def dinner_macro():
	walk()
	apero()
	pizza()
	dessert()
	encore()
	
def blessyou():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('dreic_nanodiffraction_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 0
	theta_incr = 0.1
	max_steps = 50
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-10,10,200,nnp3,-10,10,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()



def do_bragg_mobydick():
	so()
	newdataset('ahab_bragg_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 1.07
	theta_incr = 0.01
	max_steps = 25
	exp_time = 1
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,-2)
	umv(Theta,1.07)
	dscan(Theta,0,0.25,25,1)

def do_bragg_blackswan():
	so()
	newdataset('blackswan_bragg_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 4.05
	theta_incr = 0.01
	max_steps = 25
	exp_time = 0.1
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,3)
	umv(Theta,4.05)
	dscan(Theta,0,0.25,25,1)
	
def do_bragg_greyswan():
	so()
	newdataset('greyswan_bragg_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 3.6
	theta_incr = 0.01
	max_steps = 40
	exp_time = 0.3
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,3)
	umv(Theta,3.6)
	dscan(Theta,0,0.4,40,1)

def huntforredpeak():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()

	newdataset('red_october_b')
	fshtrigger()
	#theta backlash corr
	theta_start = 5
	theta_incr = 0.5
	max_steps = 50
	exp_time = 0.002
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp2,-75,75,50,nnp3,-120,0,60,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

def do_bragg_underpressure():
	so()
	newdataset('underpressure_bragg_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)
	#theta backlash corr
	theta_start = 6.8
	theta_incr = 0.004
	max_steps = 50
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)

def do_bohemian_braggsody():
	so()
	newdataset('bohemian_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)
	#theta backlash corr
	theta_start = 6.8
	theta_incr = 0.004
	max_steps = 50
	exp_time = 0.02
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)

def do_brandnewbragg():
	so()
	newdataset('brandnewbragg_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)
	#theta backlash corr
	theta_start = 6.8
	theta_incr = 0.004
	max_steps = 50
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)

def do_blindedbythebragg():
	so()
	newdataset('blindedbythebragg_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)
	#theta backlash corr
	theta_start = 6.8
	theta_incr = 0.004
	max_steps = 50
	exp_time = 0.01
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)
	
def do_stayinalive_bragg():
	so()
	newdataset('stayinalive_bragg_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 6.85
	theta_incr = 0.004
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		#kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,50,0.02)

def do_withalittlehelp_bragg():
	so()
	newdataset('withalittlehelp_bragg_1_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 6.85
	theta_incr = 0.004
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.2,0.2,8,nnp2,-0.2,0.2,8,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,40,0.02)
	
def do_bragg_onthewildside():
	so()
	newdataset('wildside_bragg_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,40,0.02)
	#theta backlash corr
	theta_start = 6.85
	theta_incr = 0.004
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.2,0.2,8,nnp2,-0.2,0.2,8,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,40,0.02)

def do_bragg_turnthepage():
	so()
	newdataset('turnthepage_bragg_1_a')
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,40,0.02)
	#theta backlash corr
	theta_start = 6.85
	theta_incr = 0.004
	max_steps = 40
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		#kmap.dkmap(nnp3,0.5,1.25,16,nnp2,0.5,1.25,16,exp_time)
		#dmesh(nnp3,-0.2,0.2,7,nnp2,-0.2,0.2,7,exp_time)
		kmap.dkmap(nnp3,-0.2,0.2,8,nnp2,-0.2,0.2,8,exp_time)
	fshtrigger()
	umv(Theta,6)
	umv(Theta,6.8)
	dscan(Theta,0,0.2,40,0.02)

def turnthestage():
	so()
	load_script('karl_safe_scan_example')
	fshtrigger()
	newdataset('turnthestage_nanodiffraction_b')
	fshtrigger()
	#theta backlash corr
	theta_start = 6.5
	theta_incr = 0.05
	max_steps = 23
	exp_time = 0.002
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		safe_kmap(nnp3,-60,20,400,nnp2,-100,-60,200,exp_time)
		safe_kmap(nnp3,-60,20,400,nnp2,-60,-20,200,exp_time)
		safe_kmap(nnp3,-60,20,400,nnp2,-20,20,200,exp_time)
		safe_kmap(nnp3,-60,20,400,nnp2,20,60,200,exp_time)
		#safe_kmap(nnp3,-60,20,400,nnp2,60,100,200,exp_time)
		safety_net()
	fshtrigger()
	sc()
	sc()
	sc()

