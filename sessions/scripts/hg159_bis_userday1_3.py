import time

class EXPO:

    expt = 0.02

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_bis_userday1_1.py ARMED ===")
print('RESUME2')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 1.0

    #if expt < 0.01:
    #    expt = 0.01
    #if expt < 0.02 and nitv < 401:
    #    retveloc = 1.0
    #else:
    #    retveloc = 0.5

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='c' # to be incremented if there is a crash

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_bis_userday1_3():


    EXPO.expt = 0.06

    dooul('C_Pa_thin_NA_PaWeLiLW_ul04_3x1_400x360_400x360_60_0023.json')
    dooul('C_Pa_thin_NA_PaDr_ul08_1x1_600x250_600x250_60_0027.json')
    dooul('C_Pa_thin_NA_PaDrLi_ul07_1x1_600x200_600x200_60_0026.json')
    dooul('Pa_thin_NA_PaWeLi_ul02_1x1_250x100_500x200_60_0020.json')
    dooul('Pa_thin_NA_PaWe_ul01_1x1_100x100_200x200_60_0019.json')
    dooul('Pa_thin_NA_PaWeLW_ul05_1x1_300x125_600x250_60_0024.json')
    dooul('Pa_thin_NA_PaDrLiLW_ul06_1x1_250x100_500x200_60_0025.json')      
    dooul('Pa_thin_NA_PaDrLW_ul03_1x1_100x75_200x150_60_0022.json')















  






   
    




def hg159_day5_1_check_pos():
    pass
    #gopos('')
