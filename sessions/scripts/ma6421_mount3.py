
def patch_t3_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=2    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t3_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t3_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def fine_t3_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=50.0
    hdj=50.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,500,
            -hdj,hdj,500,
            0.02),
        ni=1    ,  nj=1    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'fine_t3_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'fine_b_t3_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def patch_t71_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=3    ,  nj=4    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t71_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t71_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def fine_t71_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=50.0
    hdj=50.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,500,
            -hdj,hdj,500,
            0.02),
        ni=1    ,  nj=1    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'fine_t71_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'fine_t71_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def patch_t68_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t68_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t68_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def fine_t68_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=50.0
    hdj=50.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,500,
            -hdj,hdj,500,
            0.02),
        ni=1    ,  nj=1    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'fine_t68_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'fine_t68_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch_t64_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=3    ,  nj=4    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t64_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t64_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def fine_patch_t64_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=50.0
    hdj=50.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,500,
            -hdj,hdj,500,
            0.02),
        ni=2    ,  nj=2    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'fine_patch_t64_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'fine_patch_t64_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def mount3_main():
    clear_abort()
    so()
    try:
        patch_t3_1()
        fine_t3_1()
        patch_t71_1()
        fine_t71_1()
        patch_t68_2()
        fine_t68_2()
        patch_t64_1()
        fine_patch_t64_1()
        
    finally:
        sc()
        sc()
        sc()



