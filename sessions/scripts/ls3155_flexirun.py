print('ls3155_flexirun load 2')


start_key = 'waxs_bigscan'


#
# 2 means nnp2; 3 means nnp3, motors are not to be specified ...
# (posname, yrange, zrange, ystep, zstep, exp_t)
WAXS_list_loxo = [
#['loxo_m_pos1_50x_Sample_3_right_0042.json', 20.0, 20.0, 0.1, 0.5, 0.02],
#['loxo_m_pos2_50x_Sample_3_right_0043.json', 20.0, 20.0, 0.1, 0.5, 0.02],
#['loxo_m_pos3_50x_Sample_3_right_0044.json', 20.0, 20.0, 0.1, 0.5, 0.03],
#['loxo_m_pos4_50x_Sample_3_right_0045.json', 20.0, 20.0, 0.1, 0.5, 0.03],
#['loxo_m_pos5_50x_Sample_3_right_0046.json', 20.0, 20.0, 0.1, 0.5, 0.04],
#['loxo_m_pos6_50x_Sample_3_right_0047.json', 20.0, 20.0, 0.1, 0.5, 0.04],
#['loxo_m_pos7_50x_Sample_3_right_0048.json', 20.0, 20.0, 0.1, 0.5, 0.05],
#['loxo_m_pos8_50x_Sample_3_right_0049.json', 20.0, 20.0, 0.1, 0.5, 0.05],
#['loxo_m_pos9_50x_Sample_3_right_0050.json', 20.0, 20.0, 0.1, 0.5, 0.075],
#['loxo_m_pos10_50x_Sample_3_right_0051.json', 20.0, 20.0, 0.1, 0.5, 0.075],
#['loxo_m_pos11_50x_Sample_3_right_0052.json', 20.0, 20.0, 0.5, 0.1, 0.1],
#['loxo_m_pos12_50x_Sample_3_right_0053.json', 20.0, 20.0, 0.1, 0.5, 0.1],
['loxo_m_sample2_bigscan_pos1_0081.json', 40.0, 40.0, 0.2, 0.2, 0.02],
['loxo_m_sample2_bigscan_pos2_0083.json', 40.0, 40.0, 0.2, 0.2, 0.02],
['loxo_m_sample2_bigscan_pos3_0085.json', 40.0, 40.0, 0.2, 0.2, 0.05],
['loxo_m_sample2_bigscan_pos4_0086.json', 40.0, 40.0, 0.2, 0.2, 0.05],
]
SAXS_list_loxo = [
['loxo_m_Sample_2_pos15_50x_right_cross_test_0072.json', 20.0, 20.0, 0.1, 0.5, 0.02],
['loxo_m_Sample_2_pos16_50x_right_cross_test_0073.json', 22.5, 22.5, 0.1, 0.5, 0.02],
['loxo_m_Sample_2_pos17_50x_right_cross_test_0074.json', 20.0, 20.0, 0.1, 0.5, 0.02],
['loxo_m_Sample_2_pos18_50x_right_cross_test_0075.json', 22.5, 22.5, 0.1, 0.5, 0.02],
#['loxo_m_Sample_2_pos20_50x_right_cross_test_0077.json', 20.0, 20.0, 0.1, 0.5, 0.05],
#['loxo_m_Sample_2_pos21_50x_right_cross_test_0078.json', 20.0, 20.0, 0.1, 0.5, 0.05],
#['loxo_m_Sample_2_pos22_50x_right_cross_test_0079.json', 20.0, 20.0, 0.1, 0.5, 0.05],
#['loxo_m_Sample_2_pos23_50x_right_cross_test_0080.json', 20.0, 20.0, 0.1, 0.5, 0.05],
]

def verify():
    pprint(WAXS_list)


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,-1*yrange,yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    print ('========= got params:')
    print (pos,start_key,run_key,
    yrange,zrange,ystep_size,
    zstep_size,exp_t)
    if 0:
        return

    so()
    fshtrigger()
    #mgroieiger()
    pos_stub = pos.replace('.', '__')
    name = f'{pos_stub}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def run_Feb18_evening():
    try:
        # WAXS
        # umv(ndetx, -275)
        print('\n\n\nWAXS segment\n\n')
        pprint(WAXS_list_loxo)
        for the_item in WAXS_list_loxo:
            posname = the_item[0]
            last_args = the_item[1:]
            gopos_kmap_scan(posname,start_key, 0, *last_args)
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)
        # SAXS
        # umv(ndetx, 300)
        #print('\n\n\nSAXS segment\n\n')
        #pprint(SAXS_list_loxo)
        #for the_item in SAXS_list_loxo:
            #posname = the_item[0]
            #last_args = the_item[1:]
            #gopos_kmap_scan(posname,start_key, 0, *last_args)
            #print('10 sec left to interrupt ...')
            #sleep(10)
            #print('interrupt time over - do not interrupt ...')#sleep(1)                                        
    finally:
        sc()
        sc()
        sc()

def run_Feb19_afternoon():
    try:
        # SAXS
        # umv(ndetx, 300)
        print('\n\n\nSAXS segment\n\n')
        pprint(SAXS_list_loxo)
        for the_item in SAXS_list_loxo:
            posname = the_item[0] 
            last_args = the_item[1:]
            gopos_kmap_scan(posname,start_key, 0, *last_args)
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)
    finally:
        sc()
        sc()
        sc()

def run_Feb19_night():
    try:
        # WAXS
        # umv(ndetx, -275)
        print('\n\n\nWAXS segment\n\n')
        pprint(WAXS_list_loxo)
        for the_item in WAXS_list_loxo:
            posname = the_item[0]
            last_args = the_item[1:]
            gopos_kmap_scan(posname,start_key, 0, *last_args)
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)
    finally:
        sc()
        sc()
        sc()
