# beam size should be less 100 nm, this is 30 mA 16 bunch mode experiments
print('ihls3461 load 6')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    zeronnp()
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,defocus=False):
    so()
    fshtrigger()
    mgeig_x()
    name = f'{pos[:-5]}_{start_key}'
    umv(nnz,0,nny,0)
    gopos(pos)
    if defocus:
        umvr(nnx,1)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def Feb_21st_run1():
    try:
        umv(ndetx,-250)
        start_key = 'd'
        gopos_kmap_scan('brainball_s13_center_50x_0010.json',start_key,
                        60,60,0.1,0.1,0.025)
        gopos_kmap_scan('brainball_s13_bkgd_50x_0009.json',start_key,
                        25,25,1,1,0.025)
        gopos_kmap_scan('brainball_s12_center_50x_0001.json',start_key,
                        40,40,0.1,0.1,0.025)
        gopos_kmap_scan('brainball_s12_bkgd_50x_0002.json',start_key,
                        25,25,1,1,0.025)
        sc()
    finally:
        sc()
        sc()

