print('ls2952_drop_mount_02 load 5')

STARTKEY = 'e'

def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_drop_mount_02():

    try:
        # gop_newds('drop_mount_02_D3_scanstart_0077.json')
        # dkpatchmesh(0.7, 350, 2.3, 230, 0.02, 2, 1)
        # enddataset()

        # gop_newds('drop_mount_02_D4_scanstart_0082.json')
        # dkpatchmesh(0.8, 400, 2.2, 220, 0.02, 2, 1)
        # enddataset()

        # gop_newds('drop_mount_02_D5_scanstart_0085.json')
        # dkpatchmesh(0.88, 440, 3.0, 300, 0.02, 3, 1)
        # enddataset()

        # gop_newds('drop_mount_02_D6_scanstart_0093.json')
        # dkpatchmesh(0.9, 450, 1.9, 190, 0.02, 1, 1)
        # enddataset()

        # gop_newds('drop_mount_02_D2_scanstart_0076.json')
        # dkpatchmesh(1.0, 500, 2.1, 210, 0.02, 2, 1)
        # enddataset()

        gop_newds('drop_mount_02_D2_mtd03_scanstart_0102.json')
        dkpatchmesh(1.0, 500, 1.8, 180, 0.02, 2, 1)
        enddataset()

    finally:
        sc()
        sc()
