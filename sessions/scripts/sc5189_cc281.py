print('sc5189_cc281 load 3')
START_KEY = 'b'

THE_LIST = '''
cc281_5x_roi01_0111.json 150 150 0.5 0.003
cc281_5x_roi02_0112.json 150 150 0.5 0.01
cc281_5x_roi03_0113.json 150 150 0.5 0.02
cc281_5x_roi04_0114.json 150 150 1.0 0.003
cc281_5x_roi05_0115.json 150 150 1.5 0.003
cc281_5x_roi06_0116.json 150 150 2.0 0.003
cc281_5x_roi07_0117.json 150 150 1.0 0.01
cc281_5x_roi08_0118.json 150 150 1.5 0.01
cc281_5x_roi09_0119.json 150 150 2.0 0.01
'''

def make_posdc(poslst):
    posll = poslst.split('\n')
    dc = dict()
    for l in posll:
        #print (f'l = {l}')
        l = l.strip()
        if not l or l.startswith('#'):
            continue
        w1 = l.split()
        w2 = w1[0].split('_')
        for e in w2:
            #print (f'e = {e}')
            if e.startswith('roi'):
                dc[e] = l
    return dc


def dooone(psdc, roi):
    dsname = f'{roi}_{START_KEY}'
    posname, hw, vw, stp, expt = psdc[roi].split()
    hw, vw, stp, expt = map(float, (hw, vw, stp, expt))
    hll = -0.5*0.001*hw
    hul = 0.5*0.001*hw
    hitv = int(hw/stp)
    vll = -0.5*0.001*vw
    vul = 0.5*0.001*vw
    vitv = int(vw/stp)
    print('\n\n\n                                                                  #####')
    print(' ============================================================-----#####')
    print('                                                                  #####\n')
    print(f'ROI: {roi} - What should happen ...')
    print(f"newdataset('{dsname}')")
    print(f"gopos('{posname}')")
    print(f'dkmapyz_2({hll}, {hul}, {hitv}, {vll}, {vul}, {vitv}, {expt})')
    print('')
    print('What is happening ... Good Luck!')
    newdataset(dsname)
    gopos(posname)
    dkmapyz_2(hll, hul, hitv, vll, vul, vitv, expt)


def sc5189_cc281():
    try:
        posdc = make_posdc(THE_LIST)
        so()
        mgeig()
        dooone(posdc, 'roi01')
        dooone(posdc, 'roi02')
        dooone(posdc, 'roi03')
        dooone(posdc, 'roi04')
        dooone(posdc, 'roi05')
        dooone(posdc, 'roi06')
        dooone(posdc, 'roi07')
        dooone(posdc, 'roi08')
        dooone(posdc, 'roi09')
        dooone(posdc, 'roi01')
        dooone(posdc, 'roi01')
        dooone(posdc, 'roi01')
        dooone(posdc, 'roi01')
        dooone(posdc, 'roi01')
    finally:
        pass
        sc()
        sc()
        sc()
