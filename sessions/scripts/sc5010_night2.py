def the_mesh():
    dkpatchmesh(0.4,200,0.4,200,0.04,3,3) 

def sc5010_night2():
    print('second attempt: first dataset: BB1754')
    newdataset('BB1754_0020')
    gopos('measure_BB1754_5')
    the_mesh()
    enddataset()

    print('second attempt: second dataset: BB1760')
    newdataset('BB1760_0021')
    gopos('measure_BB1760_5')
    the_mesh()
    enddataset()
