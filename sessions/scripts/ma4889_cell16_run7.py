import time, sys
print('cell16_run7 load 1')
START_KEY='q'


class Timeout(object):
    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()

def z_scan():
    # print('dscan(ustrz,0,0.4,200,0.02)')
    dscan(ustrz,0,0.25,125,0.02)

def do_z_scan_set(cycle, cursor):
    newdataset(f'cell16_run7_{cycle:04}_{cursor:04}_{START_KEY}')

    ref_ypos =  -11.144-.5
    ref_zpos =  1.5775-.1-0.05
    delta_y_incr = 0.0025
    delta_y_space = 0.325
    mv(ustrz, ref_zpos)
    for i in range(3):
        new_y_targetpos = ref_ypos + i*delta_y_space + cursor*delta_y_incr
        # print(f'mv(ustry, {new_y_targetpos})')
        mv(ustry, new_y_targetpos)
        z_scan()
    
def do_cell16_run7():

    ncycles            = 10*130
    clock_period       = 0.5
    upd_period         = 10.0
    # upd_period         = 1.0
    cycle_period       = 160.0
    # cycle_period       = 3.6
    cycles_per_set     = 130


    
    glob_to = Timeout(0)

    for i in range(ncycles):
        print(f'=================================== cycle {i:04}');sys.stdout.flush()
        set_cursor = i % cycles_per_set
        print(f'                                    set_cursor {set_cursor:04}');sys.stdout.flush()
        try:
            local_to = Timeout(cycle_period)
            do_z_scan_set(i, set_cursor)

            to_detected = False
            upd_to = Timeout(upd_period)
            while not to_detected:
                (to_detected, spent_time) = local_to.check_timeout()
                (upd_flg, _tmp) = upd_to.check_timeout()

                if upd_flg:
                    print(f'time spent in cycle {set_cursor}: {spent_time}');sys.stdout.flush()
                    print(f'total time spent: {glob_to.get_delta()}');sys.stdout.flush()
                    print('\n');sys.stdout.flush()
                    upd_to.reset()
                time.sleep(clock_period)

        except KeyboardInterrupt:
            break


def dofastmesh():
    for i in range(3):
        print(f"night map {i}") 
        newdataset(f'cell16_night_{i:0>2d}')
        gopos('cell16_after_fail._pos')
        dkpatchmesh(2.5,500,0.5,250,0.01,1,1,retveloc=5.0)
        print("done, sleeping...")
        time.sleep(3600)
    sc()
 
    
        
    
    

def cell16_run7():
    try:
        so()
        do_cell16_run7()
    finally:
        sc()
        sc()
