print('ls3357 load 6')


start_key = 'a'

patch_list = [
'patch1',
'patch2',
'patch3',
'patch4',
]

offset_list = [
[0.075,0.075],
[0.225,0.075],
[0.075,0.225],
[0.225,0.225],
]

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(i,patch_list,offset_list,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    #mgeig_x()
    #mgeig()
    #MG_EH3a.enable('*roi*')
    name = f'{patch_list[i]}_{start_key}'
    #gopos(pos)
    gopos('ul_hr_xrd')
    umvr(nny,offset_list[i][0])
    umvr(nnz,offset_list[i][1])
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

def gopos_kmap_test(i,patch_list,offset_list,start_key,):
    
    so()
    fshtrigger()
    #mgeig_x()
    #mgeig()
    #MG_EH3a.enable('*roi*')
    name = f'{patch_list[i]}_{start_key}'
    print(name)
    #gopos(pos)
    gopos('ul_hr_xrd')
    umvr(nny,offset_list[i][0])
    umvr(nnz,offset_list[i][1])


def run_sept_24th_night():
    try:
        umv(ndetx,-200)
        #gopos('ul_hr_xrd')
        for i in range(3,len(patch_list)):
            gopos_kmap_scan(i,patch_list,offset_list,start_key,75,75,0.15,0.15,0.01)
            #gopos_kmap_test(i,patch_list,offset_list,start_key,)
            sleep(10)

        sc()
    except:
        sc()
        sc()


def run_sept_25th_day():
    try:
        umv(ndetx,-200)
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-99.825,99.825,1331,nnp3,-99.825,99.825,1331,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-50.025,50.025,666,nnp3,-99.825,99.825,1331,0.01)
        sc()
    except:
        sc()
        sc()
        
def run_sept_25th_night():
    try:
        umv(ndetx,-200)
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-99.975,99.975,1333,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-99.975,99.975,1333,0.01)
        
        gopos('roi3')
        newdataset(f'roi3_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-24.975,24.975,333,0.01)
        gopos('roi4')
        newdataset(f'roi4_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-24.975,24.975,333,0.01)
        sc()
    except:
        sc()
        sc()

def run_sept_26th_day():
    try:
        umv(ndetx,-200)
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-99.975,99.975,1333,nnp3,-84.975,84.975,1133,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-49.95,49.95,666,nnp3,-55.05,55.05,734,0.01)
        sc()
    except:
        sc()
        sc()

def run_sept_26th_night():
    try:
        umv(ndetx,-200)
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-75,75,1000,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-75,75,1000,0.01)
        
        gopos('roi3')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-75,75,1000,0.01)
        gopos('roi4')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-75,75,1000,nnp3,-75,75,1000,0.01)
        sc()
    except:
        sc()
        sc()
