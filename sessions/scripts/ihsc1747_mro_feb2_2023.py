print('ihsc1747_mro_feb2_2023 - load 1')
START_KEY = 'd'

MRO_POS = """\
s2_1_0011 -15 15 120 -30 30 240 0.02
s2_2_0012 -30 30 240 -15 15 120 0.02
s2_3_0013 -15 15 120 -40 40 320 0.02
s2_4_0014 -40 40 320 -15 15 120 0.02
s2_5_0015 -15 15 120 -40 40 320 0.02
s2_6_0016 -10 10 80 -40 40 320 0.02
s2_7_0017 -10 10 80 -20 20 160 0.02
s2_8_0018 -30 30 240 -15 15 120 0.02
s2_9_0019 -20 20 160 -50 50 400 0.02
s2_10_0020 -40 40 320 -20 20 160 0.02
s2_11_0021 -10 10 80 -30 30 0 0.02
s2_12_0022 -30 30 240 -15 15 120 0.02
s2_13_0023 -50 50 400 -5 15 80 0.01
s2_14_0024 -20 20 160 -15 35 200 0.01
s2_15_0025 -30 30 240 -15 15 120 0.01
s2_16_0026 -10 10 80 -40 40 320 0.01
s2_17_0027 -15 15 120 -40 40 320 0.01
s2_18_0028 -50 50 400 -15 15 120 0.01
s2_19_0029 -50 50 400 -10 10 80 0.01
s2_20_0030 -10 10 80 -30 30 0 0.02\
"""

def dooscan(*p):
    ll1, ul1, n1, ll2, ul2, n2, expt = p
    args = (nnp2, ll1, ul1, n1, nnp3, ll2, ul2, n2, expt)
    prnargs = (nnp2.name, ll1, ul1, n1, nnp3.name, ll2, ul2, n2, expt)
    print('dooscan', ' '.join(map(str, prnargs)))
    kmap.dkmap(*args)

def dooonepos(pn, *p):
    print('posname:', pn)
    rstp(f'plasc_sym_{pn}.json')
    dsname = f'{START_KEY}_{pn[-5:]}'
    print('dsname:', dsname)
    newdataset(dsname)
    dooscan(*p)

def parse_pos_line(s):
    res = []
    ll = s.split('\n')
    for l in ll:
        w = l.split()
        pn, ll1, ul1, n1, ll2, ul2, n2, expt = w
        n1, n2 = int(n1), int(n2)
        fval = ll1, ul1, ll2, ul2, expt
        fval = map(float, fval)
        ll1, ul1, ll2, ul2, expt = fval
        res.append((pn, ll1, ul1, n1, ll2, ul2, n2, expt))
    return res

def ihsc1747_night():
    for instruct in parse_pos_line(MRO_POS)[1:]:
        dooonepos(*instruct)

def ihsc1747_first():
    instruct = parse_pos_line(MRO_POS)[0]
    dooonepos(*instruct)

