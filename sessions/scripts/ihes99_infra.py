print('ihes99_infra - load 1')
def show_kmap_idxpos(cmdstr, idx):
    mots = dict(nnp1=nnp1, nnp2=nnp2, nnp3=nnp3)
    s = cmdstr.split('(')
    cmd = s[0]
    args = s[1][:-1]
    w = args.split(',')
    w = [x.strip() for x in w]
    # (nnp2,10,-10,100,nnp3,-10,10,100,0.1)
    #  0    1  2   3   4    5   6  7   8
    mot1_nm = w[0]
    mot2_nm = w[4]
    if mot1_nm in mots:
        mot1 = mots[mot1_nm]
    else:
        raise ValueError
    if mot2_nm in mots:
        mot2 = mots[mot2_nm]
    else:
        raise ValueError
    mot2_nm = w[4]
    rows = n2 = int(w[7])
    cols = n1 = int(w[3])
    assert idx >= 0
    assert idx < n1*n2

    ll1 = float(w[1])
    ul1 = float(w[2])
    ll2 = float(w[5])
    ul2 = float(w[6])
    stp1 = (ul1-ll1)/n1
    stp2 = (ul2-ll2)/n2
    (iro, ico) = divmod(idx, cols)
    d1 = ll1 + ico*stp1
    d2 = ll2 + iro*stp2
    target1 = mot1.position + d1
    target2 = mot2.position + d2
    return (mot1, target1), (mot2, target2)

def goidx(cmdstr, idx):
    (mot1, target1), (mot2, target2) = show_kmap_idxpos(cmdstr, idx)
    print(f'moving to pos: {target1} {target2}')
    mv(nnp2, target1)
    mv(nnp3, target2)

