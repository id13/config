def alignscan_ionps_vert():
    nionz_pos = nionz.position
    nionz_posll = np.linspace(-2.5, 2.5, 11)
    try:
        for v in nionz_posll:
            mv(nionz, nionz_pos + float(v))
            dscan(pvo, -0.6, 0.6, 30, 0.1)
    finally:
        mv(nionz, nionz_pos)

def alignscan_ionps_hor():
    niony_pos = niony.position
    niony_posll = np.linspace(-2., 2., 10)
    try:
        for v in niony_posll:
            mv(niony, niony_pos + float(v))
            dscan(pho, -1.5, 1.5, 30, 0.1)
    finally:
        mv(niony, niony_pos)
