print('hg233_infra - load 1')

def hg233_doocalib():
    try:
        so()
        sleep(3)
        for i, ndetx_pos in enumerate(np.linspace(-260.0, -300.0, 5)):
            print('='*40)
            print('='*40)
            print('cycle:', i, 'target ndetx position:', ndetx_pos)
            newdataset_name = f'ndetx_m{-1*int(ndetx_pos)}_a'
            print(ndetx_pos, newdataset_name)
            newdataset(newdataset_name)
            umv(ndetx, ndetx_pos)
            print('detector position:', ndetx.position)
            kmap.dkmap(nnp2, -30,30,60,nnp3, -60,60,120,0.01)
            #kmap.dkmap(nnp2, -40, 40, 80, nnp3, -40, 40, 40, 0.01)
            print(2*'\n')
    finally:
        sc()
        sc()
        sc()
        fshtrigger()
