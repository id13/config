print("sc5471_infra - load 1")
#
START_KEY = 'a'
def doo_distance():
    curr_detpos = int(round(ndetx.position))
    if curr_detpos < 0:
        sig_s = 'm'
    else:
        sig_s = 'p'
    umv(ndetx, curr_detpos)
    dsname = f'{START_KEY}_{sig_s:s}{-curr_detpos:1d}p0'
    print(f'dsname = {dsname} - creating dataset ...')
    newdataset(dsname)
    kmap.dkmap(nnp3,-40,40,40, nnp2, -40,40, 80, 0.01)

def doo_dist_series(lspc):
    for x in lspc:
        umv(ndetx, x)
        doo_distance()

def get_ct32():
    return float(SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'])

def get_ct34():
    return float(SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct34'])

def has_some_beam():
    cts_ct34 = get_ct34()
    return cts_ct34 > 150000

def doo_wbet():
    fshtrigger()
    sct()
    if not has_some_beam():
        print('check incoming beam intensity - bailing out ...')
        return
    ini_cts = get_ct32()
    
    dscan(wbetz, -0.05, 0.05, 100,0.1)
    where()
    sleep(0.5)
    gop(wbetz)
    dscan(wbety, -0.05, 0.05, 50,0.1)
    where()
    sleep(0.5)
    gop(wbety)
    dscan(wbetz, -0.05, 0.05, 100,0.1)
    where()
    sleep(0.5)
    gop(wbetz)

    sct()
    end_cts = get_ct32()
    print('ini_cts, end_cts, gains: ', ini_cts, end_cts, end_cts/ini_cts)
