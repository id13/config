print('load blc14938_29oct23 - 1')

from scipy.ndimage import gaussian_filter 


BUFF = '''
load_script('blc14938_29oct2023')
'''
class TM_KPOOL(object): pass

def tm_isvalid():
    try:
        valid = TM_KPOOL.valid
        return valid
    except AttributeError:
        return False

def tm_status():
    valid = tm_isvalid()
    if not valid:
        return 'invalid'
    sc = SCANS[-1]
    last_scanno = sc.scan_number
    scanno = TM_KPOOL.scanno
    if last_scanno == scanno:
        return 'last'
    elif last_scanno < scanno:
        return 'outdated'
    else:
        return 'strange'

def tm_show():
    print('TM_KPOOL:')
    for k in TM_KEYS:
        try:
            v = getattr(TM_KPOOL, k)
        except AttributeError:
            v = '<not-found>'
        print(f'    {k:12s}  =  {v}')
        
def tm_clear():
    for k in TM_KEYS:
        try:
            delattr(TM_KPOOL, k)
        except:
            print(f'error deleting arrtibute {k} from TM_KPOOL')
    for k in TM_KEYS:
        try:
            getattr(TM_KPOOL, k)
        except AttributeError:
            print(f'arrtibute {k} not found as it should be :)')

TM_KEYS = 'shape pos scanno scanparams motnames valid'.split()

def tm_sykmap(hw, npts, expt):

    tm_clear()
    
    TM_KPOOL.valid = False

    # perform scan
    kmap.dkmap(nnp2, -hw, hw, npts, nnp3, -hw, hw, npts, expt)
    sc = SCANS[-1]
    TM_KPOOL.scanno = sc.scan_number

    # (ll2,ul2,n2,ll3,ul3,n3,expt) = TM_KPOOL.scanparams

    TM_KPOOL.shape = (npts, npts)
    TM_KPOOL.scanparams = -hw, hw, npts, -hw, hw, npts, expt
    TM_KPOOL.pos = (nnp2.position, nnp3.position)
    TM_KPOOL.motnames = nnp2.name, nnp3.name
    TM_KPOOL.valid = True

def tm_get_last_kdiodedata(num=-1):
    status = tm_status()
    if tm_status() != 'last':
        print('tm_get_last_kdiodedata: status = {status}')
        raise ValueError
    shape = TM_KPOOL.shape
    sc = None
    if -1 == num:
        sc = SCANS[-1]
    else:
        for _sc in SCANS:
            if num == sc.scan_number:
                sc = _sc
    if None is sc:
        return None

    arr = sc.get_data('p201_eh3_0:ct2_counters_controller:ct32').reshape(shape)
    
    return arr

def give_gfmin(arr, sigma=2.0, shape=None):
    farr = gaussian_filter(arr, sigma)
    min_idx = farr.argmin()
    print(min_idx)
    (c,r) = divmod(min_idx, shape[1])
    return (farr, (c,r))
    
def align_siem():
    arr = tm_get_last_kdiodedata()
    (farr, cr) = give_gfmin(arr, 2.0, arr.shape)
    print(tm_get_crpos(cr))

def tm_get_crpos(cr):
    c,r = cr
    x3 = r
    x2 = c
    (ll2,ul2,n2,ll3,ul3,n3,expt) = TM_KPOOL.scanparams
    (p2, p3) = TM_KPOOL.pos
    or2 = p2 + ll2
    or3 = p3 + ll3
    w2 = ul2 - ll2
    w3 = ul3 - ll3
    (s2,s3) = TM_KPOOL.shape
    frac2 = (x2/s2)*w2
    frac3 = (x3/s3)*w3
    res_p = (or2 + frac2, or3 + frac3)
    return res_p
