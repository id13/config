print('hg233_example - load 2')

import traceback

import time
import gevent,bliss


START_KEY = 'f'

class HG233ScanPool(object):

    def __init__(self, simmode=True, corry=0.0, corrz=0.0):
        self.simmode = simmode
        self.corry = corry
        self.corrz = corrz
       

    def doo_kmap(self, *p):
        if self.simmode:
            print(f'kmap sim: {p}')
        else:
            print(f'kmap real: {p}')
            try:
                kmap.dkmap(*p)
            except:
                print(traceback.format_exc())
        print('3 sec to interrupt with <CTRL><C> ...')
        sleep(3)

    def doo_position(self, pos_name):
        print(f'restoring {pos_name}')
        rstp(pos_name)
        print(f'applying corrections: {self.corry=}  {self.corrz=}')
        umvr(nny, self.corry)
        umvr(nnz, self.corrz)
        ds_name = f'{pos_name}_{START_KEY}'
        if self.simmode:
            print(f'would create dataset {ds_name}')
        else:
            print(f'creating dataset {ds_name}')
            newdataset(ds_name)
        try:
            print(f'seraching scans: {pos_name} ...')
            scans_funcoid = getattr(self, f'scans_{pos_name}')
            print(f'funcoid name = {scans_funcoid.__name__}')
        except AttributeError:
            print('seraching scans: {pos_name} not found! - nothing done')
            return
        scans_funcoid()
        sleep(0.5)
        with gevent.Timeout(seconds=60):
            try:
                print('senfing elog_plot ...')
                elog_plot(scatter = True)
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                print('WARNING: elog_plot timeout ...')


    def main(self, runlist):
        try:
            if self.simmode:
                pass
            else:
                #pass
                so()
            sleep(3)
            rmgeig()

            for i, pos_name in enumerate(runlist):
                print(f'pos number {i} =========================================')
                try:
                    self.doo_position(pos_name)
                except:
                    print(traceback.format_exc())
        finally:
            sc()
            sc()
            sc()

    def trim_beam(self):
        try:
            if sh3.is_closed:
                print(f'open safety shutter before trimming ... nothing done')
                return
            print('setting diode mode ...')
            mgd()
            fshtrigger()
            (det0, _mon) = self.count()
            print(f'{det0=}')
            if det0 > 20000:
                dscan(wbetz, -0.05, 0.05, 100, 0.1)
                gop(wbetz)
                dscan(wbety, -0.1, 0.1, 50, 0.1)
                gop(wbety)
                dscan(wbetz, -0.05, 0.05, 100, 0.1)
                gop(wbetz)
                (det1, _mon) = self.count()
                gain = (det1-det0)/det0
                print(f'gain: {gain*100.0} %')
                if det1 > 200000:
                    nt3align()
            else:
                print(f'too low counts {det0=} ... nothing done')
                return
        finally:
            print('setting roi eiger mode ...')
            rmgeig()

    def count(self, expt=1):
        loopscan(1, expt)
        ct32_v = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
        ct34_v = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct34'][0]
        return (ct32_v, ct34_v)


class HG233Test(HG233ScanPool):

    def __init__(self, simmode=True, corry=-0.007, corrz=0.045):
        super().__init__(simmode=simmode, corry=corry, corrz=corrz)

    def run(self):
        ll = ['b1_p1_nocorr', 'b2_p1']
        self.main(ll)

    def scans_b2_p1(self):
        self.doo_kmap(nnp2, -30,30,30*2, nnp3, -30,-10,10*2,0.02)
        self.doo_kmap(nnp2, -30,30,60*2, nnp3, -10,10,20*2,0.02)
        self.doo_kmap(nnp2, -30,30,30*2, nnp3, 10,30,10*2,0.02)

    def scans_b1_p1_nocorr(self):
        self.doo_kmap(nnp2, -50,50,50*2, nnp3, -50,-20,15*2,0.02)
        self.doo_kmap(nnp2, -50,50,100*2, nnp3, -20,20,20*2,0.02)
        self.doo_kmap(nnp2, -50,50,50*2, nnp3, 20,50,15*2,0.02)

class HG233Night1(HG233ScanPool):

    def __init__(self, simmode=True):
        super().__init__(simmode=simmode)

    def run(self):
        ll = ['dummy_pos']
        self.main(ll)

    def scans_dummy_pos(self):
        self.doo_kmap(nnp2, -50,50,20, nnp3, -30,30,10,0.02)

class HG233Night1_early(HG233ScanPool):

    def __init__(self, simmode=True, corry=-0.007, corrz=0.045):
        super().__init__(simmode=simmode, corry=corry, corrz=corrz)
	
    def run(self):
        ll = ['b1_p2','b2_p2']
        self.main(ll)

    def scans_b1_p2(self):
        self.doo_kmap(nnp2, -40,40,160, nnp3, -40,40,160,0.02)
	
    def scans_b2_p2(self):
        self.doo_kmap(nnp2, -50,50,200, nnp3, -50,50,200,0.02)
	
class HG233Night1_late(HG233ScanPool):

    def __init__(self, simmode=True, corry=-0.002, corrz=0.045):
        super().__init__(simmode=simmode, corry=corry, corrz=corrz)
	
    def run(self):
        ll = ['a1_p1','a1_p2','a2_p1','a2_p2','a3_p1','a3_p2','a4_p1','a4_p2','b1_p1','b1_p2','b2_p1','b2_p2','b2_p3','b3_p1','b3_p2','b4_p1','b4_p2','b4_p3']
        self.main(ll)

    def scans_a1_p1(self):
        self.doo_kmap(nnp2, -30,30,120, nnp3, -60,-20,80,0.02)
        self.doo_kmap(nnp2, -30,30,300, nnp3, -20,20,200,0.02)
	self.doo_kmap(nnp2, -30,30,120, nnp3, 20,60,80,0.02)
	
    def scans_a1_p2(self):
        self.doo_kmap(nnp2, -30,30,120, nnp3, -50,50,200,0.02)
	
    def scans_a2_p1(self):
        self.doo_kmap(nnp2, -50,50,200, nnp3, -60,60,240,0.02)
	
    def scans_a2_p2(self):
        self.doo_kmap(nnp2, -40,40,400, nnp3, -80,-60,100,0.02)
	self.doo_kmap(nnp2, -40,40,400, nnp3, -60,-40,100,0.02)
	self.doo_kmap(nnp2, -40,40,400, nnp3, -40,-20,100,0.02)
	self.doo_kmap(nnp2, -40,40,400, nnp3, -20,0,100,0.02)
	
    def scans_a3_p1(self):
        self.doo_kmap(nnp2, -15,15,150, nnp3, -15,15,150,0.02)
	
    def scans_a3_p2(self):
        self.doo_kmap(nnp2, -40,40,400, nnp3, -20,0,100,0.02)
	self.doo_kmap(nnp2, -40,40,400, nnp3, 0,20,100,0.02)
	
    def scans_a4_p1(self):
        self.doo_kmap(nnp2, -50,50,200, nnp3, -40,40,160,0.02)
	
    def scans_a4_p2(self):
        self.doo_kmap(nnp2, -30,30,300, nnp3, -30,0,150,0.02)
	self.doo_kmap(nnp2, -30,30,300, nnp3, 0,30,150,0.02)
	
    def scans_b1_p2(self):
        self.doo_kmap(nnp2, -30,30,300, nnp3, -30,0,150,0.02)
	self.doo_kmap(nnp2, -30,30,300, nnp3, 0,30,150,0.02)
    def scans_b1_p1(self):
        self.doo_kmap(nnp2, -50,50,200, nnp3, -50,-10,80,0.02)
	self.doo_kmap(nnp2, -50,50,500, nnp3, -10,10,100,0.02)
	self.doo_kmap(nnp2, -50,50,200, nnp3, 10,50,80,0.02)
	
    def scans_b2_p1(self):
        self.doo_kmap(nnp2, -20,20,200, nnp3, -20,20,200,0.02)
    def scans_b2_p2(self):
        self.doo_kmap(nnp2, -20,20,200, nnp3, -20,20,200,0.02)
    def scans_b2_p3(self):
        self.doo_kmap(nnp2, -20,20,200, nnp3, -20,20,200,0.02)
	
    def scans_b3_p1(self):
        self.doo_kmap(nnp2, -20,20,200, nnp3, -20,20,200,0.02)
    def scans_b3_p2(self):
        self.doo_kmap(nnp2, -20,20,200, nnp3, -20,20,200,0.02)

    def scans_b4_p1(self):
        self.doo_kmap(nnp2, -25,25,250, nnp3, -25,0,125,0.02)
	self.doo_kmap(nnp2, -25,25,250, nnp3, 0,25,125,0.02)
    def scans_b4_p2(self):
        self.doo_kmap(nnp2, -25,25,250, nnp3, -25,0,125,0.02)
	self.doo_kmap(nnp2, -25,25,250, nnp3, 0,25,125,0.02)
    def scans_b4_p3(self):
        self.doo_kmap(nnp2, -25,25,250, nnp3, -25,0,125,0.02)
	self.doo_kmap(nnp2, -25,25,250, nnp3, 0,25,125,0.02)
	



	

	
	
	
	

