IHMI1570_SIM = False
print(f'ihmi1570_infra - load 8 (SIM = {IHMI1570_SIM})')

def rot_scan_series(dsname_base, th_start, th_stop, nintervals, *p):
    curr_th = Theta.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        print('all in one dataset pty scan ...')
        newdataset(dsname_base)
        for i, th in enumerate(th_pos_arr):
            #dsname = f'{dsname_base}_{i:05d}_rotser'
            print(f'==== dsname_base: {dsname_base} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            mv(Theta, th)
            print(f'read Theta pos: {Theta.position}')
            if IHMI1570_SIM:
                print("SIM: ************* loff_kmap", *p)
            else:
                loff_kmap(*p)
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)

def rot_dmesh_series(dsname_base, th_start, th_stop, nintervals, *p):
    curr_th = Theta.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        print('all in one dataset pty scan ...')
        newdataset(dsname_base)
        for i, th in enumerate(th_pos_arr):
            #dsname = f'{dsname_base}_{i:05d}_rotser'
            print(f'==== rot_dmesh dsname_base: {dsname_base} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            mv(Theta, th)
            print(f'read Theta pos: {Theta.position}')
            if IHMI1570_SIM:
                print("SIM: ************* loff_kmap", *p)
            else:
                print("setting: nmux.switch('O5_MPX_POL','NORMAL')")
                nmux.switch('O5_MPX_POL','NORMAL')
                fshtrigger()
                dmesh(*p)
    finally:
        print(f'rot_dmesh: moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)

def yrot_scan_series(dsname_base, th_start, th_stop, ystart, ystop, nintervals, *p):
    curr_th = Theta.position
    curr_y = nnp2.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        y_pos_arr = np.linspace(ystart, ystop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            next_y = y_pos_arr[i]
            print(f'next y pos: {next_y}')
            mv(Theta, th)
            mv(nnp2, next_y)
            print(f'read Theta pos: {Theta.position}')
            print(f'read y pos: {nnp2.position}')
            loff_kmap(*p)
            try:
                sleep(1)
                elog_plot(scatter=True)
            except:
                print('elog_plot failure ...')
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)
        mv(nnp2, curr_y)

def posto(name, poi):
    poiname = f'poi{poi:d}_{name}'
    stp(name)
    stp(poiname)

def rmgeig():
    mgeig()
    MG_EH3a.enable('*r:r*')


def newmicp(name):
    stp(name)
    newdataset(name)

def set_polarity(x):
    if -1 == x:
        nmux.switch('O5_MPX_POL','INVERTED')
    elif 1 == x:
        nmux.switch('O5_MPX_POL','NORMAL')
    else:
        raise ValueError()


def wdet():
    print('### global z:')
    wm (ndetz)
    print('### eiger:')
    wm (ndetx, ndety, mot_eiger)
    print('### pco:')
    wm (npcox, npcoy, mot_pco)
    print('### det arm:')
    wm (narmx, narmrd, narmz, mot_arm)
    


def main_night1():
    so()
    try:
        rot_scan_series('pseudo_st2_pty01', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -.75, 0.75, 30,
                                        nnp2, -2., 2, 20,
                                        0.1)
    finally:
        sc()
        sc()
        sc()

def siem_7b_pty1():
    so()
    try:
        rot_scan_series('siem_7b_pty01_b', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 40,
                                        nnp2, -2., 2, 20,
                                        0.02)
    finally:
        sc()
        sc()
        sc()
        
def siem_7c_pty1():
    so()
    try:
        rot_scan_series('siem_7b_pty01_c', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 40,
                                        nnp2, -2., 2, 20,
                                        0.01)
    finally:
        sc()
        sc()
        sc()
        
def siem_7h_pty1():
    so()
    try:
        # this macro ran also for pty01 and pty02 with the same parameters
        rot_scan_series('siem_7h_pty03', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 50,
                                        nnp2, -2., 2, 25,
                                        0.01)
    finally:
        sc()
        sc()
        sc()

def siem_7c_pty1():
    so()
    try:
        rot_scan_series('siem_7c_pty02', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 40,
                                        nnp2, -2., 2, 20,
                                        0.01)
    finally:
        sc()
        sc()
        sc()

def siem_7f_pty1():
    so()
    try:
        rot_scan_series('siem_7f_pty01', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp2, -2., 2, 20,
                                        nnp3, -2., 2., 80,
                                        0.01)
    finally:
        sc()
        sc()
        sc()
        
def siem_7e_pty1():
    so()
    try:
        rot_dmesh_series('siem_7e_dmpty01_a', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 40,
                                        nnp2, -2., 2, 20,
                                        0.07)
    finally:
        sc()
        sc()
        sc()
        
def siem_7d_pty1():
    so()
    try:
        rot_scan_series('siem_7d_pty01', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -2.0, 2.0, 80,
                                        nnp2, -2.0, 2.0, 20,
                                        0.03)
    finally:
        sc()
        sc()
        sc()

def siem_7g_pty_repeat():
    so()
    try:
        for i in range(1, 6):
            print(f'\n############## ptycho cycle {i=}')
            rot_dmesh_series(f'siem_7g_dmpty{i:02d}', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 40,
                                        nnp2, -2., 2, 20,
                                        0.07)
            kmap.dkmap(nnp2, -3, 3, 30, nnp3, -3, 3, 30,0.01)
    finally:
        sc()
        sc()
        sc()

#
# Sun Oct  6 13:46:14 2024   0 prefocusing lenses
#
        
def pfc0_siem_7i_pty1():
    so()
    try:
        rot_scan_series('pfc0_siem_7i_pty01', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 50,
                                        nnp2, -2., 2, 25,
                                        0.07)
    finally:
        sc()
        sc()
        sc()

def pfc0_siem_1b_pty_a():
    so()
    try:
        rot_dmesh_series(f'pfc0_siem_1b_dmpty01_a', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 50,
                                        nnp2, -2., 2, 25,
                                        0.08)
        kmap.dkmap(nnp2, -3, 3, 30, nnp3, -3, 3, 30,0.02)
    finally:
        sc()
        sc()
        sc()

def pfc0_siem_1c_pty():
    so()
    try:
        rot_dmesh_series(f'pfc0_siem_1c_dmpty01', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 50,
                                        nnp2, -2., 2, 25,
                                        0.08)
        kmap.dkmap(nnp2, -3, 3, 30, nnp3, -3, 3, 30,0.02)
    finally:
        sc()
        sc()
        sc()

def pfc0_siem_1c_pty_a():
    so()
    try:
        # setting correct polarity fordmesh in rot_dmesh_series:
        # nmux.switch('O5_MPX_POL','NORMAL')
        #
        rot_dmesh_series(f'pfc0_siem_1c_dmpty01_a', 104.2987-0.15, 104.2987+0.15, 50,
                                        nnp3, -1., 1., 50,
                                        nnp2, -2., 2, 25,
                                        0.08)
        kmap.dkmap(nnp2, -3, 3, 30, nnp3, -3, 3, 30,0.02)
    finally:
        sc()
        sc()
        sc()


##############################################

def mgpsie_correct():
    print('setting up psi_eiger_500k ...')
    # &&&
    #
    # psi eiger usere mpx trigger cable as it is mounted on the arm
    #

    # default chain command has to be set after mgd()!
    MG_EH3a.disable('*')
    mgd()
    psi_eiger_500k
    psi_eiger_500k.proxy.saving_index_format="%06d"

    DEFAULT_CHAIN.set_settings(chain_psi_eiger_500k['chain_config'])

    # 2024-10-05-2228
    # inverted poarity does not trigger correctly - fractional exposure ...
    #
    nmux.switch('O5_MPX_POL','NORMAL')
    #nmux.switch('O5_MPX_POL','INVERTED')
    photon_energy = ID13_XRAY_ENERGY*1000.0
    print(f'psi_eiger_500k photon_energy: {photon_energy}')
    threshold_energy = 0.5*photon_energy
    psi_eiger_500k.camera.threshold_energy = threshold_energy
    MG_EH3a.enable('psi_eiger_500k:image')
    MG_EH3a.set_active()
    print('psi_eiger_500k setup done.')

def rmgpsie_correct():
    mgpsie_correct()
    MG_EH3a.enable("*psi*roi*")



def exp_scan(expt):
    dscan(nnp3, -4, 4, 80, expt)

def exposure_test(tgcmd):
    fshclose()
    exp_scan(0.005)
    tgcmd()
    tll = [0.005, 0.01, 0.015, 0.02, 0.03, 0.05, 0.07, 0.1]
    for t in tll:
        exp_scan(t)
