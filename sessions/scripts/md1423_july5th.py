print('md1423 load 1')


start_key = 'a'

WAXS_list_July7Eve = [
'pos1',
'pos2',
'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
]

WAXS_list_July7afternoon = [
'pos1',
'pos2',
'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
'pos10',
]

WAXS_list_July6Eve = [
'pos1',
'pos2',
'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
'pos10',
'pos11',
'pos12',
'pos13',
'pos14',
'pos15',
]

WAXS_list_July6Morn = [
#'pos1',
#'pos2',
#'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
'pos10',
]

WAXS_list_July5Eve = [
'pos2',
'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
'pos10',
'pos11',
'pos12',
'pos13',
'pos14',
'pos15',
'pos16',
]

WAXS_list_July8Night = [
'pos1',
'pos2',
'pos3',
'pos4',
'pos5',
'pos6',
'pos7',
'pos8',
'pos9',
'pos10',
'pos11',
'pos12',
'pos13',
'pos14',
'pos15',
'pos16',
'pos17',
]

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig_x()
    #mgeig()
    MG_EH3a.enable('*roi*')
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()




def run_july_5th_night():
    try:
        umv(ndetx,-100)
        for i in WAXS_list_July6Eve:
            gopos_kmap_scan(i,start_key,0,100,100,0.25,0.5,0.005)
            sleep(10)

        sc()
    except:
        sc()
        sc()


def run_july_6th_morning():
    try:
        umv(ndetx,-100)
        for i in WAXS_list_July6Morn:
            gopos_kmap_scan(i,start_key,0,70,70,0.25,0.5,0.005)
            sleep(10)

        sc()
    except:
        sc()
        sc()


def run_july_6th_evening():
    try:
        umv(ndetx,-100)
        for i in WAXS_list_July6Eve:
            gopos_kmap_scan(i,start_key,0,100,100,0.25,0.5,0.005)
            sleep(10)

        sc()
    except:
        sc()
        sc()


def run_july_7th_afternoon():
    try:
        for i in WAXS_list_July7afternoon:
            gopos_kmap_scan(i,start_key,0,25,25,0.25,0.5,0.005)
            sleep(10) 
            gopos_kmap_scan(i,start_key,0,25,25,0.25,0.5,0.05)
        sc()
    except:
        sc()
        sc()


def run_july_7th_Eve():
    try:
        for i in WAXS_list_July7Eve:
            gopos_kmap_scan(i,start_key,0,50,50,0.25,0.5,0.005)
            sleep(10) 
        sc()
    except:
        sc()
        sc()

def run_july_8th_Night():
    try:
        for i in WAXS_list_July8Night:
            gopos_kmap_scan(i,start_key,0,70,70,0.25,0.5,0.005)
            sleep(10) 
        sc()
    except:
        sc()
        sc()
