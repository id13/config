print('es1403_calib - load 10')

# loading omega map capability
load_script('ch6830_infra')
START_KEY = 'b'


ES1403_M_POS = '''
M_t1050_x50_pos1_0047.json
M_t1050_x50_pos2_0049.json
M_t1050_x50_pos3_0051.json
M_t1150_x50_pos1_0041.json
M_t1150_x50_pos2_0043.json
M_t1150_x50_pos3_0045.json
M_tRT_x50_pos1_0053.json
M_tRT_x50_pos2_0055.json
M_tRT_x50_pos3_0057.json
'''

def es1403_overview_scan():
    print('loff_kmap(...)')
    loff_kmap(nnp2, -80,80, 160, nnp3, -80,80, 160, 0.02)

def es1403_omega_scan(label):
    rot_label = f'rot_{label}'
    print('rot_label', rot_label)
    print('rot_scan_series(...)')
    rot_scan_series(rot_label, -1, 1, 10, nnp2, -30,30, 60, nnp3, -20,20,40, 0.02)

def es1403_setupos(posname, intent):
    if posname.endswith('.json'):
        _nm = posname[:-5]
        dsname = f'{_nm[:-5]}_{intent}'
    elif posname.endswith('._pos'):
        dsname = f'{_nm[:-5]}_{intent}'
    else:   
        dsname = posname
    
    print ('='*40,dsname)
    rstp(posname)
    newdataset(f'{dsname}_{START_KEY}')
    return dsname

def strtolist(s):
    ll = s.split('\n')
    ll = [l.strip() for l in ll]
    ll = [l for l in ll if l]
    return ll

def es1403_saturday_lunch():
    plist = strtolist(ES1403_M_POS)
    for p in plist:
        label = es1403_setupos(p, 'onepos')
        es1403_omega_scan(label)
        es1403_overview_scan()

def es1403_scans_main():
    try:
        so()
        es1403_saturday_lunch()
    finally:
        sc()
        sc()
        sc()
