print('load 4')

def safety_net():
    for i in range(5):
        rep = 5-i
        print(f'{rep} sec to interrupt kmap...')
        sleep(1)
    print('TOO LATE! Wait for next kmap...')

def doo_scan_flat():
    start_key = 'a'
    
    for i in range(5, 13):
        gopos = f'flat_x50_pos{i}'
        print(gopos)
        newdataset(f'pos_{i}_{start_key}')
        rstp(gopos)
        if i % 2 == 0:
            kmap.dkmap(nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
        else:
            kmap.dkmap(nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)
        safety_net()
        
        
def doo_scan_textured():
    start_key = 'a'
    #so()
    try:
        for i in range(3, 23):
            gopos = f'pos_{i}_x50'
            print(gopos)
            newdataset(f'pos_{i}_{start_key}')
            rstp(gopos)
            if i % 3 == 0:
                kmap.dkmap(nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
            elif i % 3 == 1:
                kmap.dkmap(nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)
            elif i % 3 == 2:
                kmap.dkmap(nnp2, -10, 10, 400, nnp3, -10, 10, 400, 0.02)
            safety_net()
    finally:
      sc()
      sc()
      sc()