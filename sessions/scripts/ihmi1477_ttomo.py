print('version ttomo test mi1477 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

#index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 0.250, 0
2, 0, 0.500, 0
3, 0, 0.750, 0
4, 0, 1.000, 0
5, 0, 1.250, 0
6, 0, 1.500, 0
7, 0, 1.750, 0
8, 0, 2.000, 0
9, 0, 2.250, 0
10, 0, 2.500, 0
11, 0, 2.750, 0
12, 0, 3.000, 0
13, 0, 3.250, 0
14, 0, 3.500, 0
15, 0, 3.750, 0
16, 0, 4.000, 0
17, 0, 4.250, 0
18, 0, 4.500, 0
19, 0, 4.750, 0
20, 0, 5.000, 0
21, 0, 5.250, 0
22, 0, 5.500, 0
23, 0, 5.750, 0
24, 0, 6.000, 0
25, 0, 6.250, 0
26, 0, 6.500, 0
27, 0, 6.750, 0
28, 0, 7.000, 0
29, 0, 7.250, 0
30, 0, 7.500, 0
31, 0, 7.750, 0
32, 0, 8.000, 0
33, 0, 8.250, 0
34, 0, 8.500, 0
35, 0, 8.750, 0
36, 0, 9.000, 0
37, 0, 9.250, 0
38, 0, 9.500, 0
39, 0, 9.750, 0
40, 0, 10.000, 0
41, 0, 10.250, 0
42, 0, 10.500, 0
43, 0, 10.750, 0
44, 0, 11.000, 0
45, 0, 11.250, 0
46, 0, 11.500, 0
47, 0, 11.750, 0
48, 0, 12.000, 0
49, 0, 12.250, 0
50, 0, 12.500, 0
51, 0, 12.750, 0
52, 0, 13.000, 0
53, 0, 13.250, 0
54, 0, 13.500, 0
55, 0, 13.750, 0
56, 0, 14.000, 0
57, 0, 14.250, 0
58, 0, 14.500, 0
59, 0, 14.750, 0
60, 0, 15.000, 0
61, 0, 15.250, 0
62, 0, 15.500, 0
63, 0, 15.750, 0
64, 0, 16.000, 0
65, 0, 16.250, 0
66, 0, 16.500, 0
67, 0, 16.750, 0
68, 0, 17.000, 0
69, 0, 17.250, 0
70, 0, 17.500, 0
71, 0, 17.750, 0
72, 0, 18.000, 0
73, 0, 18.250, 0
74, 0, 18.500, 0
75, 0, 18.750, 0
76, 0, 19.000, 0
77, 0, 19.250, 0
78, 0, 19.500, 0
79, 0, 19.750, 0
80, 0, 20.000, 0
81, 0, 20.250, 0
82, 0, 20.500, 0
83, 0, 20.750, 0
84, 0, 21.000, 0
85, 0, 21.250, 0
86, 0, 21.500, 0
87, 0, 21.750, 0
88, 0, 22.000, 0
89, 0, 22.250, 0
90, 0, 22.500, 0
91, 0, 22.750, 0
92, 0, 23.000, 0
93, 0, 23.250, 0
94, 0, 23.500, 0
95, 0, 23.750, 0
96, 0, 24.000, 0
97, 0, 24.250, 0
98, 0, 24.500, 0
99, 0, 24.750, 0
100, 0, 25.000, 0
101, 0, 25.250, 0
102, 0, 25.500, 0
103, 0, 25.750, 0
104, 0, 26.000, 0
105, 0, 26.250, 0
106, 0, 26.500, 0
107, 0, 26.750, 0
108, 0, 27.000, 0
109, 0, 27.250, 0
110, 0, 27.500, 0
111, 0, 27.750, 0
112, 0, 28.000, 0
113, 0, 28.250, 0
114, 0, 28.500, 0
115, 0, 28.750, 0
116, 0, 29.000, 0
117, 0, 29.250, 0
118, 0, 29.500, 0
119, 0, 29.750, 0
120, 0, 30.000, 0
121, 0, 30.250, 0
122, 0, 30.500, 0
123, 0, 30.750, 0
124, 0, 31.000, 0
125, 0, 31.250, 0
126, 0, 31.500, 0
127, 0, 31.750, 0
128, 0, 32.000, 0
129, 0, 32.250, 0
130, 0, 32.500, 0
131, 0, 32.750, 0
132, 0, 33.000, 0
133, 0, 33.250, 0
134, 0, 33.500, 0
135, 0, 33.750, 0
136, 0, 34.000, 0
137, 0, 34.250, 0
138, 0, 34.500, 0
139, 0, 34.750, 0
140, 0, 35.000, 0
141, 0, 35.250, 0
142, 0, 35.500, 0
143, 0, 35.750, 0
144, 0, 36.000, 0
145, 0, 36.250, 0
146, 0, 36.500, 0
147, 0, 36.750, 0
148, 0, 37.000, 0
149, 0, 37.250, 0
150, 0, 37.500, 0
151, 0, 37.750, 0
152, 0, 38.000, 0
153, 0, 38.250, 0
154, 0, 38.500, 0
155, 0, 38.750, 0
156, 0, 39.000, 0
157, 0, 39.250, 0
158, 0, 39.500, 0
159, 0, 39.750, 0
160, 0, 40.000, 0
161, 0, 40.250, 0
162, 0, 40.500, 0
163, 0, 40.750, 0
164, 0, 41.000, 0
165, 0, 41.250, 0
166, 0, 41.500, 0
167, 0, 41.750, 0
168, 0, 42.000, 0
169, 0, 42.250, 0
170, 0, 42.500, 0
171, 0, 42.750, 0
172, 0, 43.000, 0
173, 0, 43.250, 0
174, 0, 43.500, 0
175, 0, 43.750, 0
176, 0, 44.000, 0
177, 0, 44.250, 0
178, 0, 44.500, 0
179, 0, 44.750, 0
180, 0, 45.000, 0
181, 0, 45.250, 0
182, 0, 45.500, 0
183, 0, 45.750, 0
184, 0, 46.000, 0
185, 0, 46.250, 0
186, 0, 46.500, 0
187, 0, 46.750, 0
188, 0, 47.000, 0
189, 0, 47.250, 0
190, 0, 47.500, 0
191, 0, 47.750, 0
192, 0, 48.000, 0
193, 0, 48.250, 0
194, 0, 48.500, 0
195, 0, 48.750, 0
196, 0, 49.000, 0
197, 0, 49.250, 0
198, 0, 49.500, 0
199, 0, 49.750, 0
200, 0, 50.000, 0
201, 0, 50.250, 0
202, 0, 50.500, 0
203, 0, 50.750, 0
204, 0, 51.000, 0
205, 0, 51.250, 0
206, 0, 51.500, 0
207, 0, 51.750, 0
208, 0, 52.000, 0
209, 0, 52.250, 0
210, 0, 52.500, 0
211, 0, 52.750, 0
212, 0, 53.000, 0
213, 0, 53.250, 0
214, 0, 53.500, 0
215, 0, 53.750, 0
216, 0, 54.000, 0
217, 0, 54.250, 0
218, 0, 54.500, 0
219, 0, 54.750, 0
220, 0, 55.000, 0
221, 0, 55.250, 0
222, 0, 55.500, 0
223, 0, 55.750, 0
224, 0, 56.000, 0
225, 0, 56.250, 0
226, 0, 56.500, 0
227, 0, 56.750, 0
228, 0, 57.000, 0
229, 0, 57.250, 0
230, 0, 57.500, 0
231, 0, 57.750, 0
232, 0, 58.000, 0
233, 0, 58.250, 0
234, 0, 58.500, 0
235, 0, 58.750, 0
236, 0, 59.000, 0
237, 0, 59.250, 0
238, 0, 59.500, 0
239, 0, 59.750, 0
240, 0, 60.000, 0
241, 0, 60.250, 0
242, 0, 60.500, 0
243, 0, 60.750, 0
244, 0, 61.000, 0
245, 0, 61.250, 0
246, 0, 61.500, 0
247, 0, 61.750, 0
248, 0, 62.000, 0
249, 0, 62.250, 0
250, 0, 62.500, 0
251, 0, 62.750, 0
252, 0, 63.000, 0
253, 0, 63.250, 0
254, 0, 63.500, 0
255, 0, 63.750, 0
256, 0, 64.000, 0
257, 0, 64.250, 0
258, 0, 64.500, 0
259, 0, 64.750, 0
260, 0, 65.000, 0
261, 0, 65.250, 0
262, 0, 65.500, 0
263, 0, 65.750, 0
264, 0, 66.000, 0
265, 0, 66.250, 0
266, 0, 66.500, 0
267, 0, 66.750, 0
268, 0, 67.000, 0
269, 0, 67.250, 0
270, 0, 67.500, 0
271, 0, 67.750, 0
272, 0, 68.000, 0
273, 0, 68.250, 0
274, 0, 68.500, 0
275, 0, 68.750, 0
276, 0, 69.000, 0
277, 0, 69.250, 0
278, 0, 69.500, 0
279, 0, 69.750, 0
280, 0, 70.000, 0
281, 0, 70.250, 0
282, 0, 70.500, 0
283, 0, 70.750, 0
284, 0, 71.000, 0
285, 0, 71.250, 0
286, 0, 71.500, 0
287, 0, 71.750, 0
288, 0, 72.000, 0
289, 0, 72.250, 0
290, 0, 72.500, 0
291, 0, 72.750, 0
292, 0, 73.000, 0
293, 0, 73.250, 0
294, 0, 73.500, 0
295, 0, 73.750, 0
296, 0, 74.000, 0
297, 0, 74.250, 0
298, 0, 74.500, 0
299, 0, 74.750, 0
300, 0, 75.000, 0
301, 0, 75.250, 0
302, 0, 75.500, 0
303, 0, 75.750, 0
304, 0, 76.000, 0
305, 0, 76.250, 0
306, 0, 76.500, 0
307, 0, 76.750, 0
308, 0, 77.000, 0
309, 0, 77.250, 0
310, 0, 77.500, 0
311, 0, 77.750, 0
312, 0, 78.000, 0
313, 0, 78.250, 0
314, 0, 78.500, 0
315, 0, 78.750, 0
316, 0, 79.000, 0
317, 0, 79.250, 0
318, 0, 79.500, 0
319, 0, 79.750, 0
320, 0, 80.000, 0
321, 0, 80.250, 0
322, 0, 80.500, 0
323, 0, 80.750, 0
324, 0, 81.000, 0
325, 0, 81.250, 0
326, 0, 81.500, 0
327, 0, 81.750, 0
328, 0, 82.000, 0
329, 0, 82.250, 0
330, 0, 82.500, 0
331, 0, 82.750, 0
332, 0, 83.000, 0
333, 0, 83.250, 0
334, 0, 83.500, 0
335, 0, 83.750, 0
336, 0, 84.000, 0
337, 0, 84.250, 0
338, 0, 84.500, 0
339, 0, 84.750, 0
340, 0, 85.000, 0
341, 0, 85.250, 0
342, 0, 85.500, 0
343, 0, 85.750, 0
344, 0, 86.000, 0
345, 0, 86.250, 0
346, 0, 86.500, 0
347, 0, 86.750, 0
348, 0, 87.000, 0
349, 0, 87.250, 0
350, 0, 87.500, 0
351, 0, 87.750, 0
352, 0, 88.000, 0
353, 0, 88.250, 0
354, 0, 88.500, 0
355, 0, 88.750, 0
356, 0, 89.000, 0
357, 0, 89.250, 0
358, 0, 89.500, 0
359, 0, 89.750, 0
360, 0, 90.000, 0
361, 0, 90.250, 0
362, 0, 90.500, 0
363, 0, 90.750, 0
364, 0, 91.000, 0
365, 0, 91.250, 0
366, 0, 91.500, 0
367, 0, 91.750, 0
368, 0, 92.000, 0
369, 0, 92.250, 0
370, 0, 92.500, 0
371, 0, 92.750, 0
372, 0, 93.000, 0
373, 0, 93.250, 0
374, 0, 93.500, 0
375, 0, 93.750, 0
376, 0, 94.000, 0
377, 0, 94.250, 0
378, 0, 94.500, 0
379, 0, 94.750, 0
380, 0, 95.000, 0
381, 0, 95.250, 0
382, 0, 95.500, 0
383, 0, 95.750, 0
384, 0, 96.000, 0
385, 0, 96.250, 0
386, 0, 96.500, 0
387, 0, 96.750, 0
388, 0, 97.000, 0
389, 0, 97.250, 0
390, 0, 97.500, 0
391, 0, 97.750, 0
392, 0, 98.000, 0
393, 0, 98.250, 0
394, 0, 98.500, 0
395, 0, 98.750, 0
396, 0, 99.000, 0
397, 0, 99.250, 0
398, 0, 99.500, 0
399, 0, 99.750, 0
400, 0, 100.000, 0
401, 0, 100.250, 0
402, 0, 100.500, 0
403, 0, 100.750, 0
404, 0, 101.000, 0
405, 0, 101.250, 0
406, 0, 101.500, 0
407, 0, 101.750, 0
408, 0, 102.000, 0
409, 0, 102.250, 0
410, 0, 102.500, 0
411, 0, 102.750, 0
412, 0, 103.000, 0
413, 0, 103.250, 0
414, 0, 103.500, 0
415, 0, 103.750, 0
416, 0, 104.000, 0
417, 0, 104.250, 0
418, 0, 104.500, 0
419, 0, 104.750, 0
420, 0, 105.000, 0
421, 0, 105.250, 0
422, 0, 105.500, 0
423, 0, 105.750, 0
424, 0, 106.000, 0
425, 0, 106.250, 0
426, 0, 106.500, 0
427, 0, 106.750, 0
428, 0, 107.000, 0
429, 0, 107.250, 0
430, 0, 107.500, 0
431, 0, 107.750, 0
432, 0, 108.000, 0
433, 0, 108.250, 0
434, 0, 108.500, 0
435, 0, 108.750, 0
436, 0, 109.000, 0
437, 0, 109.250, 0
438, 0, 109.500, 0
439, 0, 109.750, 0
440, 0, 110.000, 0
441, 0, 110.250, 0
442, 0, 110.500, 0
443, 0, 110.750, 0
444, 0, 111.000, 0
445, 0, 111.250, 0
446, 0, 111.500, 0
447, 0, 111.750, 0
448, 0, 112.000, 0
449, 0, 112.250, 0
450, 0, 112.500, 0
451, 0, 112.750, 0
452, 0, 113.000, 0
453, 0, 113.250, 0
454, 0, 113.500, 0
455, 0, 113.750, 0
456, 0, 114.000, 0
457, 0, 114.250, 0
458, 0, 114.500, 0
459, 0, 114.750, 0
460, 0, 115.000, 0
461, 0, 115.250, 0
462, 0, 115.500, 0
463, 0, 115.750, 0
464, 0, 116.000, 0
465, 0, 116.250, 0
466, 0, 116.500, 0
467, 0, 116.750, 0
468, 0, 117.000, 0
469, 0, 117.250, 0
470, 0, 117.500, 0
471, 0, 117.750, 0
472, 0, 118.000, 0
473, 0, 118.250, 0
474, 0, 118.500, 0
475, 0, 118.750, 0
476, 0, 119.000, 0
477, 0, 119.250, 0
478, 0, 119.500, 0
479, 0, 119.750, 0
480, 0, 120.000, 0
481, 0, 120.250, 0
482, 0, 120.500, 0
483, 0, 120.750, 0
484, 0, 121.000, 0
485, 0, 121.250, 0
486, 0, 121.500, 0
487, 0, 121.750, 0
488, 0, 122.000, 0
489, 0, 122.250, 0
490, 0, 122.500, 0
491, 0, 122.750, 0
492, 0, 123.000, 0
493, 0, 123.250, 0
494, 0, 123.500, 0
495, 0, 123.750, 0
496, 0, 124.000, 0
497, 0, 124.250, 0
498, 0, 124.500, 0
499, 0, 124.750, 0
500, 0, 125.000, 0
501, 0, 125.250, 0
502, 0, 125.500, 0
503, 0, 125.750, 0
504, 0, 126.000, 0
505, 0, 126.250, 0
506, 0, 126.500, 0
507, 0, 126.750, 0
508, 0, 127.000, 0
509, 0, 127.250, 0
510, 0, 127.500, 0
511, 0, 127.750, 0
512, 0, 128.000, 0
513, 0, 128.250, 0
514, 0, 128.500, 0
515, 0, 128.750, 0
516, 0, 129.000, 0
517, 0, 129.250, 0
518, 0, 129.500, 0
519, 0, 129.750, 0
520, 0, 130.000, 0
521, 0, 130.250, 0
522, 0, 130.500, 0
523, 0, 130.750, 0
524, 0, 131.000, 0
525, 0, 131.250, 0
526, 0, 131.500, 0
527, 0, 131.750, 0
528, 0, 132.000, 0
529, 0, 132.250, 0
530, 0, 132.500, 0
531, 0, 132.750, 0
532, 0, 133.000, 0
533, 0, 133.250, 0
534, 0, 133.500, 0
535, 0, 133.750, 0
536, 0, 134.000, 0
537, 0, 134.250, 0
538, 0, 134.500, 0
539, 0, 134.750, 0
540, 0, 135.000, 0
541, 0, 135.250, 0
542, 0, 135.500, 0
543, 0, 135.750, 0
544, 0, 136.000, 0
545, 0, 136.250, 0
546, 0, 136.500, 0
547, 0, 136.750, 0
548, 0, 137.000, 0
549, 0, 137.250, 0
550, 0, 137.500, 0
551, 0, 137.750, 0
552, 0, 138.000, 0
553, 0, 138.250, 0
554, 0, 138.500, 0
555, 0, 138.750, 0
556, 0, 139.000, 0
557, 0, 139.250, 0
558, 0, 139.500, 0
559, 0, 139.750, 0
560, 0, 140.000, 0
561, 0, 140.250, 0
562, 0, 140.500, 0
563, 0, 140.750, 0
564, 0, 141.000, 0
565, 0, 141.250, 0
566, 0, 141.500, 0
567, 0, 141.750, 0
568, 0, 142.000, 0
569, 0, 142.250, 0
570, 0, 142.500, 0
571, 0, 142.750, 0
572, 0, 143.000, 0
573, 0, 143.250, 0
574, 0, 143.500, 0
575, 0, 143.750, 0
576, 0, 144.000, 0
577, 0, 144.250, 0
578, 0, 144.500, 0
579, 0, 144.750, 0
580, 0, 145.000, 0
581, 0, 145.250, 0
582, 0, 145.500, 0
583, 0, 145.750, 0
584, 0, 146.000, 0
585, 0, 146.250, 0
586, 0, 146.500, 0
587, 0, 146.750, 0
588, 0, 147.000, 0
589, 0, 147.250, 0
590, 0, 147.500, 0
591, 0, 147.750, 0
592, 0, 148.000, 0
593, 0, 148.250, 0
594, 0, 148.500, 0
595, 0, 148.750, 0
596, 0, 149.000, 0
597, 0, 149.250, 0
598, 0, 149.500, 0
599, 0, 149.750, 0
600, 0, 150.000, 0
601, 0, 150.250, 0
602, 0, 150.500, 0
603, 0, 150.750, 0
604, 0, 151.000, 0
605, 0, 151.250, 0
606, 0, 151.500, 0
607, 0, 151.750, 0
608, 0, 152.000, 0
609, 0, 152.250, 0
610, 0, 152.500, 0
611, 0, 152.750, 0
612, 0, 153.000, 0
613, 0, 153.250, 0
614, 0, 153.500, 0
615, 0, 153.750, 0
616, 0, 154.000, 0
617, 0, 154.250, 0
618, 0, 154.500, 0
619, 0, 154.750, 0
620, 0, 155.000, 0
621, 0, 155.250, 0
622, 0, 155.500, 0
623, 0, 155.750, 0
624, 0, 156.000, 0
625, 0, 156.250, 0
626, 0, 156.500, 0
627, 0, 156.750, 0
628, 0, 157.000, 0
629, 0, 157.250, 0
630, 0, 157.500, 0
631, 0, 157.750, 0
632, 0, 158.000, 0
633, 0, 158.250, 0
634, 0, 158.500, 0
635, 0, 158.750, 0
636, 0, 159.000, 0
637, 0, 159.250, 0
638, 0, 159.500, 0
639, 0, 159.750, 0
640, 0, 160.000, 0
641, 0, 160.250, 0
642, 0, 160.500, 0
643, 0, 160.750, 0
644, 0, 161.000, 0
645, 0, 161.250, 0
646, 0, 161.500, 0
647, 0, 161.750, 0
648, 0, 162.000, 0
649, 0, 162.250, 0
650, 0, 162.500, 0
651, 0, 162.750, 0
652, 0, 163.000, 0
653, 0, 163.250, 0
654, 0, 163.500, 0
655, 0, 163.750, 0
656, 0, 164.000, 0
657, 0, 164.250, 0
658, 0, 164.500, 0
659, 0, 164.750, 0
660, 0, 165.000, 0
661, 0, 165.250, 0
662, 0, 165.500, 0
663, 0, 165.750, 0
664, 0, 166.000, 0
665, 0, 166.250, 0
666, 0, 166.500, 0
667, 0, 166.750, 0
668, 0, 167.000, 0
669, 0, 167.250, 0
670, 0, 167.500, 0
671, 0, 167.750, 0
672, 0, 168.000, 0
673, 0, 168.250, 0
674, 0, 168.500, 0
675, 0, 168.750, 0
676, 0, 169.000, 0
677, 0, 169.250, 0
678, 0, 169.500, 0
679, 0, 169.750, 0
680, 0, 170.000, 0
681, 0, 170.250, 0
682, 0, 170.500, 0
683, 0, 170.750, 0
684, 0, 171.000, 0
685, 0, 171.250, 0
686, 0, 171.500, 0
687, 0, 171.750, 0
688, 0, 172.000, 0
689, 0, 172.250, 0
690, 0, 172.500, 0
691, 0, 172.750, 0
692, 0, 173.000, 0
693, 0, 173.250, 0
694, 0, 173.500, 0
695, 0, 173.750, 0
696, 0, 174.000, 0
697, 0, 174.250, 0
698, 0, 174.500, 0
699, 0, 174.750, 0
700, 0, 175.000, 0
701, 0, 175.250, 0
702, 0, 175.500, 0
703, 0, 175.750, 0
704, 0, 176.000, 0
705, 0, 176.250, 0
706, 0, 176.500, 0
707, 0, 176.750, 0
708, 0, 177.000, 0
709, 0, 177.250, 0
710, 0, 177.500, 0
711, 0, 177.750, 0
712, 0, 178.000, 0
713, 0, 178.250, 0
714, 0, 178.500, 0
715, 0, 178.750, 0
716, 0, 179.000, 0
717, 0, 179.250, 0
718, 0, 179.500, 0
719, 0, 179.750, 0
720, 0, 180.000, 0
720, 0, 0.125, 0
721, 0, 0.375, 0
722, 0, 0.625, 0
723, 0, 0.875, 0
724, 0, 1.125, 0
725, 0, 1.375, 0
726, 0, 1.625, 0
727, 0, 1.875, 0
728, 0, 2.125, 0
729, 0, 2.375, 0
730, 0, 2.625, 0
731, 0, 2.875, 0
732, 0, 3.125, 0
733, 0, 3.375, 0
734, 0, 3.625, 0
735, 0, 3.875, 0
736, 0, 4.125, 0
737, 0, 4.375, 0
738, 0, 4.625, 0
739, 0, 4.875, 0
740, 0, 5.125, 0
741, 0, 5.375, 0
742, 0, 5.625, 0
743, 0, 5.875, 0
744, 0, 6.125, 0
745, 0, 6.375, 0
746, 0, 6.625, 0
747, 0, 6.875, 0
748, 0, 7.125, 0
749, 0, 7.375, 0
750, 0, 7.625, 0
751, 0, 7.875, 0
752, 0, 8.125, 0
753, 0, 8.375, 0
754, 0, 8.625, 0
755, 0, 8.875, 0
756, 0, 9.125, 0
757, 0, 9.375, 0
758, 0, 9.625, 0
759, 0, 9.875, 0
760, 0, 10.125, 0
761, 0, 10.375, 0
762, 0, 10.625, 0
763, 0, 10.875, 0
764, 0, 11.125, 0
765, 0, 11.375, 0
766, 0, 11.625, 0
767, 0, 11.875, 0
768, 0, 12.125, 0
769, 0, 12.375, 0
770, 0, 12.625, 0
771, 0, 12.875, 0
772, 0, 13.125, 0
773, 0, 13.375, 0
774, 0, 13.625, 0
775, 0, 13.875, 0
776, 0, 14.125, 0
777, 0, 14.375, 0
778, 0, 14.625, 0
779, 0, 14.875, 0
780, 0, 15.125, 0
781, 0, 15.375, 0
782, 0, 15.625, 0
783, 0, 15.875, 0
784, 0, 16.125, 0
785, 0, 16.375, 0
786, 0, 16.625, 0
787, 0, 16.875, 0
788, 0, 17.125, 0
789, 0, 17.375, 0
790, 0, 17.625, 0
791, 0, 17.875, 0
792, 0, 18.125, 0
793, 0, 18.375, 0
794, 0, 18.625, 0
795, 0, 18.875, 0
796, 0, 19.125, 0
797, 0, 19.375, 0
798, 0, 19.625, 0
799, 0, 19.875, 0
800, 0, 20.125, 0
801, 0, 20.375, 0
802, 0, 20.625, 0
803, 0, 20.875, 0
804, 0, 21.125, 0
805, 0, 21.375, 0
806, 0, 21.625, 0
807, 0, 21.875, 0
808, 0, 22.125, 0
809, 0, 22.375, 0
810, 0, 22.625, 0
811, 0, 22.875, 0
812, 0, 23.125, 0
813, 0, 23.375, 0
814, 0, 23.625, 0
815, 0, 23.875, 0
816, 0, 24.125, 0
817, 0, 24.375, 0
818, 0, 24.625, 0
819, 0, 24.875, 0
820, 0, 25.125, 0
821, 0, 25.375, 0
822, 0, 25.625, 0
823, 0, 25.875, 0
824, 0, 26.125, 0
825, 0, 26.375, 0
826, 0, 26.625, 0
827, 0, 26.875, 0
828, 0, 27.125, 0
829, 0, 27.375, 0
830, 0, 27.625, 0
831, 0, 27.875, 0
832, 0, 28.125, 0
833, 0, 28.375, 0
834, 0, 28.625, 0
835, 0, 28.875, 0
836, 0, 29.125, 0
837, 0, 29.375, 0
838, 0, 29.625, 0
839, 0, 29.875, 0
840, 0, 30.125, 0
841, 0, 30.375, 0
842, 0, 30.625, 0
843, 0, 30.875, 0
844, 0, 31.125, 0
845, 0, 31.375, 0
846, 0, 31.625, 0
847, 0, 31.875, 0
848, 0, 32.125, 0
849, 0, 32.375, 0
850, 0, 32.625, 0
851, 0, 32.875, 0
852, 0, 33.125, 0
853, 0, 33.375, 0
854, 0, 33.625, 0
855, 0, 33.875, 0
856, 0, 34.125, 0
857, 0, 34.375, 0
858, 0, 34.625, 0
859, 0, 34.875, 0
860, 0, 35.125, 0
861, 0, 35.375, 0
862, 0, 35.625, 0
863, 0, 35.875, 0
864, 0, 36.125, 0
865, 0, 36.375, 0
866, 0, 36.625, 0
867, 0, 36.875, 0
868, 0, 37.125, 0
869, 0, 37.375, 0
870, 0, 37.625, 0
871, 0, 37.875, 0
872, 0, 38.125, 0
873, 0, 38.375, 0
874, 0, 38.625, 0
875, 0, 38.875, 0
876, 0, 39.125, 0
877, 0, 39.375, 0
878, 0, 39.625, 0
879, 0, 39.875, 0
880, 0, 40.125, 0
881, 0, 40.375, 0
882, 0, 40.625, 0
883, 0, 40.875, 0
884, 0, 41.125, 0
885, 0, 41.375, 0
886, 0, 41.625, 0
887, 0, 41.875, 0
888, 0, 42.125, 0
889, 0, 42.375, 0
890, 0, 42.625, 0
891, 0, 42.875, 0
892, 0, 43.125, 0
893, 0, 43.375, 0
894, 0, 43.625, 0
895, 0, 43.875, 0
896, 0, 44.125, 0
897, 0, 44.375, 0
898, 0, 44.625, 0
899, 0, 44.875, 0
900, 0, 45.125, 0
901, 0, 45.375, 0
902, 0, 45.625, 0
903, 0, 45.875, 0
904, 0, 46.125, 0
905, 0, 46.375, 0
906, 0, 46.625, 0
907, 0, 46.875, 0
908, 0, 47.125, 0
909, 0, 47.375, 0
910, 0, 47.625, 0
911, 0, 47.875, 0
912, 0, 48.125, 0
913, 0, 48.375, 0
914, 0, 48.625, 0
915, 0, 48.875, 0
916, 0, 49.125, 0
917, 0, 49.375, 0
918, 0, 49.625, 0
919, 0, 49.875, 0
920, 0, 50.125, 0
921, 0, 50.375, 0
922, 0, 50.625, 0
923, 0, 50.875, 0
924, 0, 51.125, 0
925, 0, 51.375, 0
926, 0, 51.625, 0
927, 0, 51.875, 0
928, 0, 52.125, 0
929, 0, 52.375, 0
930, 0, 52.625, 0
931, 0, 52.875, 0
932, 0, 53.125, 0
933, 0, 53.375, 0
934, 0, 53.625, 0
935, 0, 53.875, 0
936, 0, 54.125, 0
937, 0, 54.375, 0
938, 0, 54.625, 0
939, 0, 54.875, 0
940, 0, 55.125, 0
941, 0, 55.375, 0
942, 0, 55.625, 0
943, 0, 55.875, 0
944, 0, 56.125, 0
945, 0, 56.375, 0
946, 0, 56.625, 0
947, 0, 56.875, 0
948, 0, 57.125, 0
949, 0, 57.375, 0
950, 0, 57.625, 0
951, 0, 57.875, 0
952, 0, 58.125, 0
953, 0, 58.375, 0
954, 0, 58.625, 0
955, 0, 58.875, 0
956, 0, 59.125, 0
957, 0, 59.375, 0
958, 0, 59.625, 0
959, 0, 59.875, 0
960, 0, 60.125, 0
961, 0, 60.375, 0
962, 0, 60.625, 0
963, 0, 60.875, 0
964, 0, 61.125, 0
965, 0, 61.375, 0
966, 0, 61.625, 0
967, 0, 61.875, 0
968, 0, 62.125, 0
969, 0, 62.375, 0
970, 0, 62.625, 0
971, 0, 62.875, 0
972, 0, 63.125, 0
973, 0, 63.375, 0
974, 0, 63.625, 0
975, 0, 63.875, 0
976, 0, 64.125, 0
977, 0, 64.375, 0
978, 0, 64.625, 0
979, 0, 64.875, 0
980, 0, 65.125, 0
981, 0, 65.375, 0
982, 0, 65.625, 0
983, 0, 65.875, 0
984, 0, 66.125, 0
985, 0, 66.375, 0
986, 0, 66.625, 0
987, 0, 66.875, 0
988, 0, 67.125, 0
989, 0, 67.375, 0
990, 0, 67.625, 0
991, 0, 67.875, 0
992, 0, 68.125, 0
993, 0, 68.375, 0
994, 0, 68.625, 0
995, 0, 68.875, 0
996, 0, 69.125, 0
997, 0, 69.375, 0
998, 0, 69.625, 0
999, 0, 69.875, 0
1000, 0, 70.125, 0
1001, 0, 70.375, 0
1002, 0, 70.625, 0
1003, 0, 70.875, 0
1004, 0, 71.125, 0
1005, 0, 71.375, 0
1006, 0, 71.625, 0
1007, 0, 71.875, 0
1008, 0, 72.125, 0
1009, 0, 72.375, 0
1010, 0, 72.625, 0
1011, 0, 72.875, 0
1012, 0, 73.125, 0
1013, 0, 73.375, 0
1014, 0, 73.625, 0
1015, 0, 73.875, 0
1016, 0, 74.125, 0
1017, 0, 74.375, 0
1018, 0, 74.625, 0
1019, 0, 74.875, 0
1020, 0, 75.125, 0
1021, 0, 75.375, 0
1022, 0, 75.625, 0
1023, 0, 75.875, 0
1024, 0, 76.125, 0
1025, 0, 76.375, 0
1026, 0, 76.625, 0
1027, 0, 76.875, 0
1028, 0, 77.125, 0
1029, 0, 77.375, 0
1030, 0, 77.625, 0
1031, 0, 77.875, 0
1032, 0, 78.125, 0
1033, 0, 78.375, 0
1034, 0, 78.625, 0
1035, 0, 78.875, 0
1036, 0, 79.125, 0
1037, 0, 79.375, 0
1038, 0, 79.625, 0
1039, 0, 79.875, 0
1040, 0, 80.125, 0
1041, 0, 80.375, 0
1042, 0, 80.625, 0
1043, 0, 80.875, 0
1044, 0, 81.125, 0
1045, 0, 81.375, 0
1046, 0, 81.625, 0
1047, 0, 81.875, 0
1048, 0, 82.125, 0
1049, 0, 82.375, 0
1050, 0, 82.625, 0
1051, 0, 82.875, 0
1052, 0, 83.125, 0
1053, 0, 83.375, 0
1054, 0, 83.625, 0
1055, 0, 83.875, 0
1056, 0, 84.125, 0
1057, 0, 84.375, 0
1058, 0, 84.625, 0
1059, 0, 84.875, 0
1060, 0, 85.125, 0
1061, 0, 85.375, 0
1062, 0, 85.625, 0
1063, 0, 85.875, 0
1064, 0, 86.125, 0
1065, 0, 86.375, 0
1066, 0, 86.625, 0
1067, 0, 86.875, 0
1068, 0, 87.125, 0
1069, 0, 87.375, 0
1070, 0, 87.625, 0
1071, 0, 87.875, 0
1072, 0, 88.125, 0
1073, 0, 88.375, 0
1074, 0, 88.625, 0
1075, 0, 88.875, 0
1076, 0, 89.125, 0
1077, 0, 89.375, 0
1078, 0, 89.625, 0
1079, 0, 89.875, 0
1080, 0, 90.125, 0
1081, 0, 90.375, 0
1082, 0, 90.625, 0
1083, 0, 90.875, 0
1084, 0, 91.125, 0
1085, 0, 91.375, 0
1086, 0, 91.625, 0
1087, 0, 91.875, 0
1088, 0, 92.125, 0
1089, 0, 92.375, 0
1090, 0, 92.625, 0
1091, 0, 92.875, 0
1092, 0, 93.125, 0
1093, 0, 93.375, 0
1094, 0, 93.625, 0
1095, 0, 93.875, 0
1096, 0, 94.125, 0
1097, 0, 94.375, 0
1098, 0, 94.625, 0
1099, 0, 94.875, 0
1100, 0, 95.125, 0
1101, 0, 95.375, 0
1102, 0, 95.625, 0
1103, 0, 95.875, 0
1104, 0, 96.125, 0
1105, 0, 96.375, 0
1106, 0, 96.625, 0
1107, 0, 96.875, 0
1108, 0, 97.125, 0
1109, 0, 97.375, 0
1110, 0, 97.625, 0
1111, 0, 97.875, 0
1112, 0, 98.125, 0
1113, 0, 98.375, 0
1114, 0, 98.625, 0
1115, 0, 98.875, 0
1116, 0, 99.125, 0
1117, 0, 99.375, 0
1118, 0, 99.625, 0
1119, 0, 99.875, 0
1120, 0, 100.125, 0
1121, 0, 100.375, 0
1122, 0, 100.625, 0
1123, 0, 100.875, 0
1124, 0, 101.125, 0
1125, 0, 101.375, 0
1126, 0, 101.625, 0
1127, 0, 101.875, 0
1128, 0, 102.125, 0
1129, 0, 102.375, 0
1130, 0, 102.625, 0
1131, 0, 102.875, 0
1132, 0, 103.125, 0
1133, 0, 103.375, 0
1134, 0, 103.625, 0
1135, 0, 103.875, 0
1136, 0, 104.125, 0
1137, 0, 104.375, 0
1138, 0, 104.625, 0
1139, 0, 104.875, 0
1140, 0, 105.125, 0
1141, 0, 105.375, 0
1142, 0, 105.625, 0
1143, 0, 105.875, 0
1144, 0, 106.125, 0
1145, 0, 106.375, 0
1146, 0, 106.625, 0
1147, 0, 106.875, 0
1148, 0, 107.125, 0
1149, 0, 107.375, 0
1150, 0, 107.625, 0
1151, 0, 107.875, 0
1152, 0, 108.125, 0
1153, 0, 108.375, 0
1154, 0, 108.625, 0
1155, 0, 108.875, 0
1156, 0, 109.125, 0
1157, 0, 109.375, 0
1158, 0, 109.625, 0
1159, 0, 109.875, 0
1160, 0, 110.125, 0
1161, 0, 110.375, 0
1162, 0, 110.625, 0
1163, 0, 110.875, 0
1164, 0, 111.125, 0
1165, 0, 111.375, 0
1166, 0, 111.625, 0
1167, 0, 111.875, 0
1168, 0, 112.125, 0
1169, 0, 112.375, 0
1170, 0, 112.625, 0
1171, 0, 112.875, 0
1172, 0, 113.125, 0
1173, 0, 113.375, 0
1174, 0, 113.625, 0
1175, 0, 113.875, 0
1176, 0, 114.125, 0
1177, 0, 114.375, 0
1178, 0, 114.625, 0
1179, 0, 114.875, 0
1180, 0, 115.125, 0
1181, 0, 115.375, 0
1182, 0, 115.625, 0
1183, 0, 115.875, 0
1184, 0, 116.125, 0
1185, 0, 116.375, 0
1186, 0, 116.625, 0
1187, 0, 116.875, 0
1188, 0, 117.125, 0
1189, 0, 117.375, 0
1190, 0, 117.625, 0
1191, 0, 117.875, 0
1192, 0, 118.125, 0
1193, 0, 118.375, 0
1194, 0, 118.625, 0
1195, 0, 118.875, 0
1196, 0, 119.125, 0
1197, 0, 119.375, 0
1198, 0, 119.625, 0
1199, 0, 119.875, 0
1200, 0, 120.125, 0
1201, 0, 120.375, 0
1202, 0, 120.625, 0
1203, 0, 120.875, 0
1204, 0, 121.125, 0
1205, 0, 121.375, 0
1206, 0, 121.625, 0
1207, 0, 121.875, 0
1208, 0, 122.125, 0
1209, 0, 122.375, 0
1210, 0, 122.625, 0
1211, 0, 122.875, 0
1212, 0, 123.125, 0
1213, 0, 123.375, 0
1214, 0, 123.625, 0
1215, 0, 123.875, 0
1216, 0, 124.125, 0
1217, 0, 124.375, 0
1218, 0, 124.625, 0
1219, 0, 124.875, 0
1220, 0, 125.125, 0
1221, 0, 125.375, 0
1222, 0, 125.625, 0
1223, 0, 125.875, 0
1224, 0, 126.125, 0
1225, 0, 126.375, 0
1226, 0, 126.625, 0
1227, 0, 126.875, 0
1228, 0, 127.125, 0
1229, 0, 127.375, 0
1230, 0, 127.625, 0
1231, 0, 127.875, 0
1232, 0, 128.125, 0
1233, 0, 128.375, 0
1234, 0, 128.625, 0
1235, 0, 128.875, 0
1236, 0, 129.125, 0
1237, 0, 129.375, 0
1238, 0, 129.625, 0
1239, 0, 129.875, 0
1240, 0, 130.125, 0
1241, 0, 130.375, 0
1242, 0, 130.625, 0
1243, 0, 130.875, 0
1244, 0, 131.125, 0
1245, 0, 131.375, 0
1246, 0, 131.625, 0
1247, 0, 131.875, 0
1248, 0, 132.125, 0
1249, 0, 132.375, 0
1250, 0, 132.625, 0
1251, 0, 132.875, 0
1252, 0, 133.125, 0
1253, 0, 133.375, 0
1254, 0, 133.625, 0
1255, 0, 133.875, 0
1256, 0, 134.125, 0
1257, 0, 134.375, 0
1258, 0, 134.625, 0
1259, 0, 134.875, 0
1260, 0, 135.125, 0
1261, 0, 135.375, 0
1262, 0, 135.625, 0
1263, 0, 135.875, 0
1264, 0, 136.125, 0
1265, 0, 136.375, 0
1266, 0, 136.625, 0
1267, 0, 136.875, 0
1268, 0, 137.125, 0
1269, 0, 137.375, 0
1270, 0, 137.625, 0
1271, 0, 137.875, 0
1272, 0, 138.125, 0
1273, 0, 138.375, 0
1274, 0, 138.625, 0
1275, 0, 138.875, 0
1276, 0, 139.125, 0
1277, 0, 139.375, 0
1278, 0, 139.625, 0
1279, 0, 139.875, 0
1280, 0, 140.125, 0
1281, 0, 140.375, 0
1282, 0, 140.625, 0
1283, 0, 140.875, 0
1284, 0, 141.125, 0
1285, 0, 141.375, 0
1286, 0, 141.625, 0
1287, 0, 141.875, 0
1288, 0, 142.125, 0
1289, 0, 142.375, 0
1290, 0, 142.625, 0
1291, 0, 142.875, 0
1292, 0, 143.125, 0
1293, 0, 143.375, 0
1294, 0, 143.625, 0
1295, 0, 143.875, 0
1296, 0, 144.125, 0
1297, 0, 144.375, 0
1298, 0, 144.625, 0
1299, 0, 144.875, 0
1300, 0, 145.125, 0
1301, 0, 145.375, 0
1302, 0, 145.625, 0
1303, 0, 145.875, 0
1304, 0, 146.125, 0
1305, 0, 146.375, 0
1306, 0, 146.625, 0
1307, 0, 146.875, 0
1308, 0, 147.125, 0
1309, 0, 147.375, 0
1310, 0, 147.625, 0
1311, 0, 147.875, 0
1312, 0, 148.125, 0
1313, 0, 148.375, 0
1314, 0, 148.625, 0
1315, 0, 148.875, 0
1316, 0, 149.125, 0
1317, 0, 149.375, 0
1318, 0, 149.625, 0
1319, 0, 149.875, 0
1320, 0, 150.125, 0
1321, 0, 150.375, 0
1322, 0, 150.625, 0
1323, 0, 150.875, 0
1324, 0, 151.125, 0
1325, 0, 151.375, 0
1326, 0, 151.625, 0
1327, 0, 151.875, 0
1328, 0, 152.125, 0
1329, 0, 152.375, 0
1330, 0, 152.625, 0
1331, 0, 152.875, 0
1332, 0, 153.125, 0
1333, 0, 153.375, 0
1334, 0, 153.625, 0
1335, 0, 153.875, 0
1336, 0, 154.125, 0
1337, 0, 154.375, 0
1338, 0, 154.625, 0
1339, 0, 154.875, 0
1340, 0, 155.125, 0
1341, 0, 155.375, 0
1342, 0, 155.625, 0
1343, 0, 155.875, 0
1344, 0, 156.125, 0
1345, 0, 156.375, 0
1346, 0, 156.625, 0
1347, 0, 156.875, 0
1348, 0, 157.125, 0
1349, 0, 157.375, 0
1350, 0, 157.625, 0
1351, 0, 157.875, 0
1352, 0, 158.125, 0
1353, 0, 158.375, 0
1354, 0, 158.625, 0
1355, 0, 158.875, 0
1356, 0, 159.125, 0
1357, 0, 159.375, 0
1358, 0, 159.625, 0
1359, 0, 159.875, 0
1360, 0, 160.125, 0
1361, 0, 160.375, 0
1362, 0, 160.625, 0
1363, 0, 160.875, 0
1364, 0, 161.125, 0
1365, 0, 161.375, 0
1366, 0, 161.625, 0
1367, 0, 161.875, 0
1368, 0, 162.125, 0
1369, 0, 162.375, 0
1370, 0, 162.625, 0
1371, 0, 162.875, 0
1372, 0, 163.125, 0
1373, 0, 163.375, 0
1374, 0, 163.625, 0
1375, 0, 163.875, 0
1376, 0, 164.125, 0
1377, 0, 164.375, 0
1378, 0, 164.625, 0
1379, 0, 164.875, 0
1380, 0, 165.125, 0
1381, 0, 165.375, 0
1382, 0, 165.625, 0
1383, 0, 165.875, 0
1384, 0, 166.125, 0
1385, 0, 166.375, 0
1386, 0, 166.625, 0
1387, 0, 166.875, 0
1388, 0, 167.125, 0
1389, 0, 167.375, 0
1390, 0, 167.625, 0
1391, 0, 167.875, 0
1392, 0, 168.125, 0
1393, 0, 168.375, 0
1394, 0, 168.625, 0
1395, 0, 168.875, 0
1396, 0, 169.125, 0
1397, 0, 169.375, 0
1398, 0, 169.625, 0
1399, 0, 169.875, 0
1400, 0, 170.125, 0
1401, 0, 170.375, 0
1402, 0, 170.625, 0
1403, 0, 170.875, 0
1404, 0, 171.125, 0
1405, 0, 171.375, 0
1406, 0, 171.625, 0
1407, 0, 171.875, 0
1408, 0, 172.125, 0
1409, 0, 172.375, 0
1410, 0, 172.625, 0
1411, 0, 172.875, 0
1412, 0, 173.125, 0
1413, 0, 173.375, 0
1414, 0, 173.625, 0
1415, 0, 173.875, 0
1416, 0, 174.125, 0
1417, 0, 174.375, 0
1418, 0, 174.625, 0
1419, 0, 174.875, 0
1420, 0, 175.125, 0
1421, 0, 175.375, 0
1422, 0, 175.625, 0
1423, 0, 175.875, 0
1424, 0, 176.125, 0
1425, 0, 176.375, 0
1426, 0, 176.625, 0
1427, 0, 176.875, 0
1428, 0, 177.125, 0
1429, 0, 177.375, 0
1430, 0, 177.625, 0
1431, 0, 177.875, 0
1432, 0, 178.125, 0
1433, 0, 178.375, 0
1434, 0, 178.625, 0
1435, 0, 178.875, 0
1436, 0, 179.125, 0
1437, 0, 179.375, 0
1438, 0, 179.625, 0
1439, 0, 179.875, 0
1440, 0, 180.125, 0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        cmd = f'kmap.dkmap(nnp2, {lly},{uly},{nitvy}, nnp3, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        with gevent.Timeout(seconds=600):
            try:
                # put the nano scan ...
                
                kmap.dkmap(nnp2,lly,uly,nitvy,nnp3,llz,ulz,nitvz,expt,frames_per_file=nitvy*20)
                self.log('scan successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
            
        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//0.1)
        nitvz = int(dz//0.1)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz


def mi1477_ttomo_main(zkap, start_key, resume=-1):

    
    print('Hi mi1477!')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -32,
        uly = 28,
        nitvy = 600,
        llz = -5,
        ulz = 5,
        nitvz = 100,
        expt = 0.002
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
