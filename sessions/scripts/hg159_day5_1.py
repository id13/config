import time

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_day5_1 ARMED ===")
print('RESUME1')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    if expt < 0.01:
        expt = 0.01
    if expt < 0.02 and nitv < 401:
        retveloc = 0.5
    else:
        retveloc = 0.25
    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='d'

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_day5_1():


    dooul('cena3_12_ul01_1x1_400x300_400x300_25_0057.json')
    dooul('cena3_15CT_ul01_1x1_400x400_400x400_25_0058.json')
    dooul('cena3_2V_ul01_2x2_350x350_350x350_25_0059.json')
    dooul('cena3_A10_ul01_1x1_300x400_300x400_25_0056.json')
    dooul('cena3_M_ul01_1x1_300x300_300x300_25_0060.json')
    dooul('cena3_Q_ul01_1x1_300x400_300x400_25_0055.json')

    


def hg159_day5_1_check_pos():
    pass
    #gopos('')
