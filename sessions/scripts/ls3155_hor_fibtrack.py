print ('load - 36')
def search_scan(search_params):
    dscan(nnz, *search_params)

def do_kmap(kmap_params):
    try:
        kmap.dkmap(*kmap_params)
    finally:
        fshtrigger()

def analyze_1

def track_fibre_z_up(start_pos, search_params,
    kmap_params, roi_counter, nsegments, start_segment, dmargin, dwidth, start_z = None):

    gopos(start_pos)
    start_y = nny.position
    if not None is start_z:
        print(f'moving to previouslz aligne nnz position: {start_z}')
        mv(nnz, start_z)
        

    for i in range(start_segment, nsegments):
        print('===========================================================================:)')
        print(f'segment: {i}')
        curr_y = start_y + i*dwidth
        print(f'search: curr_y = {curr_y}')
        mv(nny, curr_y)
        search_scan(search_params)
        #goto_cen(eiger.counters.roi2_avg, nnz)
        nnz_before = nnz.position
        goto_cen(roi_counter, nnz)
        nnz_after = nnz.position
        print(f'nnz: before = {nnz_before}  after: {nnz_after}')
        curr_y = curr_y + dmargin
        print(f'kmap: curr_y = {curr_y}')
        mv(nny, curr_y)
        if not i % 1:
            do_kmap(kmap_params)

def fibretracking_main():
# scarr=sc.get_data('p201_eh3_0:ct2_counters_controller:ct34')
    activate_all()
    fshtrigger()
    newdataset('12_I_mid_a')
    start_pos = '12_I_50x_mid'
    start_segment = 0
    start_z = None   
    nsegments = 3
    search_params = [-0.07,0.07,140,0.02]
    kmap_params = [nnp3, -50,50,100*10, nnp2, 0, -20, 40 , 0.01]
    dwidth = -0.02
    dmargin = -0.01
    roi_counter = eiger.counters.roi2_avg
    track_fibre_z_up(start_pos, search_params,
        kmap_params, roi_counter, nsegments, start_segment, dmargin, dwidth, start_z=start_z)

def activate_all():
    mgeig_x()
    MG_EH3a.enable('*r:r*')
