print('ihsc1629_b')
def ihsc1629():
    try:
        so()
        newdataset('G_S6_b')
        gopos('tripple_G_S6_A_0011.json')
        umvr(ustrz,0.1,ustry,0.1)
        dkpatchmesh(1.5,300,1.5,300,0.02,1,1,retveloc=0.5)
        enddataset()

        newdataset('G_S8_b')
        gopos('tripple_G_S8_A_0020.json')
        umvr(ustrz,0.1,ustry,0.1)
        dkpatchmesh(1.5,300,1.5,300,0.02,1,1,retveloc=0.5)
        enddataset()
        sc()
        
    finally:
        sc()
        sc()
        sc()
