print ("sc5005_night1 loading ... 3")
from bliss.setup_globals import *

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    gopos(x)
    newdataset('DS_a_%s' % s)
    fshtrigger()

def WAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 0.0
    dist2 = 15.0

    mvr(nnp2,-dist1)
    fshtrigger()
    kmap.dkmap(nnp3, -38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2,-dist2)
    fshtrigger()
    dmesh(nnp3, -38, 38, 760 , nnp2, 0, -8, 16, 0.2)
    
def SAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 760 , nnp2, 0, -4, 8, 0.05)
    
def WAXS_nephila(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 0.0
    dist2 = 17.0

    mvr(nnp2, -dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, -dist2)
    fshtrigger()
    dmesh(nnp3,-30, 30, 600 , nnp2, 0, -10, 20, 0.2)
    
def SAXS_nephila(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0
    
    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 600 , nnp2, 0, -5, 10, 0.05)


POS = """
mount01_x50_M1_BM1D_p01_0015.json
mount01_x50_M1_BM1D_p02_0016.json
mount01_x50_M1_BM1N_p01_0013.json
mount01_x50_M1_BM1N_p02_0014.json
mount01_x50_M4NEDN_p01_0009.json
mount01_x50_M4NEDN_p02_0010.json
mount01_x50_M4NIDN_p03_0007.json
mount01_x50_M4NIDN_p04_0008.json
"""


def sc5005_main_night1():

    so()
    try:

        WAXS_nephila('mount01_x50_M4NEDN_p01_0009.json')
        WAXS_nephila('mount01_x50_M4NEDN_p02_0010.json')
        WAXS_nephila('mount01_x50_M4NIDN_p03_0007.json')
        WAXS_nephila('mount01_x50_M4NIDN_p04_0008.json')

        WAXS_bombyx('mount01_x50_M1_BM1D_p01_0015.json')
        WAXS_bombyx('mount01_x50_M1_BM1D_p02_0016.json')
        WAXS_bombyx('mount01_x50_M1_BM1N_p01_0013.json')
        WAXS_bombyx('mount01_x50_M1_BM1N_p02_0014.json')

        umv(ndetx, 300)

        SAXS_nephila('mount01_x50_M4NEDN_p01_0009.json')
        SAXS_nephila('mount01_x50_M4NEDN_p02_0010.json')
        SAXS_nephila('mount01_x50_M4NIDN_p03_0007.json')
        SAXS_nephila('mount01_x50_M4NIDN_p04_0008.json')

        SAXS_bombyx('mount01_x50_M1_BM1D_p01_0015.json')
        SAXS_bombyx('mount01_x50_M1_BM1D_p02_0016.json')
        SAXS_bombyx('mount01_x50_M1_BM1N_p01_0013.json')
        SAXS_bombyx('mount01_x50_M1_BM1N_p02_0014.json')

        sc()
    finally:
        sc()
        sc()
        sc()
