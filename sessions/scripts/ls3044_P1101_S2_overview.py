print('ls3044_P1101_S2_overviews: load 19')
POS_COMMENTS = '''
============== stp:
test_pos1
    scan: kmap.dkmap(nnp2, 20,-20,100, nnp3, -30,30,150,0.01)
============== stp:

'''

def overviewMap(pos):
    gopos(pos)
    mv(nnp2,225,nnp3,25)
    kmap.dkmap(nnp2,0,-200,400,nnp3,0,200,400,0.01,frames_per_file=400)

def makeoverviews():
    try:
        eigerhws(True)
        overviewPositions = [
        'P1101_S2_Osteon1_ul_0011.json',
        'P1101_S2_Osteon2_ul_0012.json',
        'P1101_S2_Osteon3_ul_0013.json',
        'P1101_S2_Osteon4_ul_0008.json',
        'P1101_S2_Lamellar_cen_0010.json']

        overviewNames = [
        'Osteon1_a',
        'Osteon2_a',
        'Osteon3_a',
        'Osteon4_a',
        'Lamellar2_a']

        for j,i in enumerate(overviewPositions):
            newdataset(overviewNames[j])
            print('Now scanning {0}'.format(i))
            overviewMap(i)
    finally:
        sc()
        sc()
        sc()

def makeROIScansTest():
    try:
        eigerhws(True)
        overviewPositions = [
        'P1101_S2_Osteon1_ul_0011.json',
        'P1101_S2_Osteon2_ul_0012.json',
        'P1101_S2_Osteon3_ul_0013.json',
        'P1101_S2_Osteon4_ul_0008.json',
        'P1101_S2_Lamellar_cen_0010.json']

        gopos(overviewPositions[0])
        mv(nnp2,125,nnp3,60)
        kstripes('Osteon1_ROI_test_b', 100, 100, 1, 100, 1, 0.01, frames_per_file=100)

        gopos(overviewPositions[1])
        mv(nnp2,125,nnp3,60)
        mvr(nny,0.025)
        kstripes('Osteon2_ROI_test_b', 100, 100, 1, 100, 1, 0.01, frames_per_file=100)

        gopos(overviewPositions[3])
        mv(nnp2,225,nnp3,175)
        kstripes('Osteon4_ROI_a_test_b', 50, 50, 1, 50, 1, 0.01, frames_per_file=50)
        mv(nnp2,100,nnp3,0)
        kstripes('Osteon4_ROI_b_test_b', 50, 50, 1, 50, 1, 0.01, frames_per_file=50)

        gopos(overviewPositions[4])
        mv(nnp2,220,nnp3,75)
        kstripes('Lamellar_ROI_test_b', 100, 100, 1, 100, 1, 0.01, frames_per_file=100)

    finally:
        sc()
        sc()
        sc()



def makefluoLammelarScan(testRun=False):
    try:
        overviewPositions = [
        'P1101_S2_Osteon1_ul_0011.json',
        'P1101_S2_Osteon2_ul_0012.json',
        'P1101_S2_Osteon3_ul_0013.json',
        'P1101_S2_Osteon4_ul_0008.json',
        'P1101_S2_Lamellar_cen_0010.json']
        zeronnp()

        if testRun:
            gopos(overviewPositions[4])
            mv(nnp2,225,nnp3,100)
            kstripesFluo('Lamellar_fluo_test', 60, 60,  1,200, 1,0.01)
        else:
            gopos(overviewPositions[4])
            mv(nnp2,225,nnp3,100)
            kstripesFluo('Lamellar_fluo', 600, 10, 0.10, 2000, 0.10, 0.2)

    finally:
        sc()
        sc()
        sc()



def makeROIScans():
    try:
        eigerhws(True)
        overviewPositions = [
        'P1101_S2_Osteon1_ul_0011.json',
        'P1101_S2_Osteon2_ul_0012.json',
        'P1101_S2_Osteon3_ul_0013.json',
        'P1101_S2_Osteon4_ul_0008.json',
        'P1101_S2_Lamellar_cen_0010.json']

        #gopos(overviewPositions[0])
        #mv(nnp2,125,nnp3,60)
        #kstripes('Osteon1_ROI_a', 1000, 200, 0.1, 1000, 0.1, 0.01, frames_per_file=1000,stripes_to_skip=1)

        #gopos(overviewPositions[1])
        #mv(nnp2,125,nnp3,60)
        #mvr(nny,0.025)
        #kstripes('Osteon2_ROI', 1000, 200, 0.1, 1000, 0.1, 0.01, frames_per_file=1000)

        #gopos(overviewPositions[3])
        ##mv(nnp2,225,nnp3,175)
        ##kstripes('Osteon4_ROI_a', 500, 100, 0.1, 500, 0.1, 0.01, frames_per_file=1000)
        #mv(nnp2,100,nnp3,25)
        #kstripes('Osteon4_ROI_e', 500, 100, 0.1, 500, 0.1, 0.01, frames_per_file=1000)

        gopos(overviewPositions[4])
        mv(nnp2,220,nnp3,75)
        kstripes('Lamellar_ROI_b', 1000, 200, 0.1, 1000, 0.1, 0.01, frames_per_file=1000, stripes_to_skip=4)

    finally:
        sc()
        sc()
        sc()

def makeOsteocyteScans(testRun=False):
    try:
        eigerhws(True)
        overviewPositions = [
        'P1101_S2_Osteon1_ul_0011.json',
        'P1101_S2_Osteon2_ul_0012.json',
        'P1101_S2_Osteon3_ul_0013.json',
        'P1101_S2_Osteon4_ul_0008.json',
        'P1101_S2_Lamellar_cen_0010.json']

        if testRun:
            gopos(overviewPositions[0])
            mv(nnp2,70,nnp3,95)
            kstripes('Osteon1_Osteocyte_test', 30, 30, 0.75, 40, 0.75, 0.01, frames_per_file=100)

            gopos(overviewPositions[1])
            mv(nnp2,100,nnp3,95)
            mvr(nny,0.025)
            kstripes('Osteon2_Osteocyte_test', 20, 20, 0.75, 30, 0.75, 0.01, frames_per_file=100)

            gopos(overviewPositions[3])
            mv(nnp2,102,nnp3,50)
            kstripes('Osteon4_ROI_e_Osteocyte_test', 30, 30, 0.75, 20, 0.75, 0.01, frames_per_file=100)

            gopos(overviewPositions[4])
            mv(nnp2,160,nnp3,128)
            kstripes('Lamellar_ROI_test', 20, 20, 0.75, 20, 0.75, 0.01, frames_per_file=100)
            mv(nnp2,150,nnp3,75)
            kstripes('Lamellar_Osteocyte_test', 20, 20, 0.75, 20, 0.75, 0.01, frames_per_file=100)
        else:
            gopos(overviewPositions[0])
            mv(nnp2,70,nnp3,95)
            kstripes('Osteon1_Osteocyte', 300, 100, 0.075, 400, 0.075, 0.1, frames_per_file=1000)

            gopos(overviewPositions[1])
            mv(nnp2,100,nnp3,95)
            mvr(nny,0.025)
            kstripes('Osteon2_Osteocyte', 200, 100, 0.075, 300, 0.075, 0.1, frames_per_file=1000)

            gopos(overviewPositions[3])
            mv(nnp2,102,nnp3,50)
            kstripes('Osteon4_ROI_e_Osteocyte', 300, 100, 0.075, 200, 0.075, 0.1, frames_per_file=1000)

            gopos(overviewPositions[4])
            mv(nnp2,160,nnp3,128)
            kstripes('Lamellar_Canaliculi', 200, 100, 0.075, 200, 0.075, 0.1, frames_per_file=1000)
            mv(nnp2,150,nnp3,75)
            kstripes('Lamellar_Osteocyte', 200, 100, 0.075, 200, 0.075, 0.1, frames_per_file=1000)

    finally:
        sc()
        sc()
        sc()


def kstripes(prefix, nlines_tot, nlines_per_stripe, nlines_step, ncols, ncols_step, exptime, frames_per_file=None, stripes_to_skip=0):
    if None is frames_per_file:
        frames_per_file = ncols
    (nstripes, fracs) = divmod(nlines_tot, nlines_per_stripe)
    if fracs != 0:
        raise ValueError('illegal lines_per_stripe')

    start_nnp3 = nnp3.position

    for i in range(stripes_to_skip,nstripes):
        print(f'scanning stripe # {i} ============================================================')
        newdataset(f'{prefix}_{i:03}')
        curr_nnp3 = start_nnp3 + i*nlines_per_stripe*nlines_step
        print(f'moving to "curr_nnp3" {curr_nnp3}')
        mv(nnp3, curr_nnp3)
        print(f'kmap_args: {nnp2.name, 0, -ncols*ncols_step, ncols, nnp3.name, 0, nlines_per_stripe*nlines_step, nlines_per_stripe, exptime, frames_per_file}')
        kmap.dkmap(nnp2, 0, -ncols*ncols_step, ncols, nnp3, 0, nlines_per_stripe*nlines_step, nlines_per_stripe,
            exptime, frames_per_file=frames_per_file)
        enddataset()


def kstripesFluo(prefix, nlines_tot, nlines_per_stripe, nlines_step, ncols, ncols_step, exptime, stripes_to_skip=0):
    (nstripes, fracs) = divmod(nlines_tot, nlines_per_stripe)
    if fracs != 0:
        raise ValueError('illegal lines_per_stripe')

    start_nnp3 = nnp3.position

    for i in range(stripes_to_skip,nstripes):
        print(f'scanning stripe # {i} ============================================================')
        newdataset(f'{prefix}_{i:03}')
        curr_nnp3 = start_nnp3 + i*nlines_per_stripe*nlines_step
        print(f'moving to "curr_nnp3" {curr_nnp3}')
        mv(nnp3, curr_nnp3)
        print(f'kmap_args: {nnp2.name, 0, -ncols*ncols_step, ncols, nnp3.name, 0, nlines_per_stripe*nlines_step, nlines_per_stripe, exptime}')
        kmap.dkmap(nnp2, 0, -ncols*ncols_step, ncols, nnp3, 0, nlines_per_stripe*nlines_step, nlines_per_stripe,
            exptime)
        enddataset()
