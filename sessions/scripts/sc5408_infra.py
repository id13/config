print('sc5408 - load 5')
# wps
# 0.15, 0.15 phg, pvg

# to liad this script ...
# load_script('sc5408_infra')

#
# Detector
#
# initial udetx.position=-783 (setting 1)
# dety = 26.45
#
# setting 2 (beam on the left side looking with the beam):
# after loosing probably steps with dety:
# dety
#
# 

#
# bstop
#
# ubsy can go to -145 (out)
# ubsy for beam tuning is -1 (bapalign, u18, etc ...)
# ubsy aligned is ~0.0
#
# undulator
#
# setting 1: u18 detuned 6.38 (optimum ~6.58)
#

# flight tube
#
# ucryox out for microscope: 251
# cryoy diode = 70.0
# cryoy in = 0.0
# cryox diode = 10.0
# cryox in = 1.0

#
# trans limits
#            ustrx     ustry    ustrz
#--------  -------  --------  -------
#User    
# High      -1.242    5.5790    9.841
# Current   -7.028   -0.1040   -0.094
# Low      -11.242   -4.4210  -13.564
#Offset     -7.294  -39.2020   -1.600
#
#Dial    
# High      -6.052   44.7810   11.441
# Current   -0.266   39.0980    1.505
# Low        3.948   34.7810  -11.964
# set_lm(ustrx, -11.242, -1.242)
#

# scratch
# beam eig pix x 1124  gap 1036

import math

def check_pos(mot, pos, eps=0.1):
    if math.fabs(pos-mot.position) > eps:
        raise RuntimeError(f'check position: mot={mot.name} req_pos={pos} curr_pos={mot.position} eps={eps}')

def sc5408_fltube_to_fltdiode():
    try:
        check_pos(ucryoy, 70)
        check_pos(ucryox, 10)
        return
    except RuntimeError:
        pass
        
    print('moving flight tube to flt_diode position ...')
    check_pos(ucryoy, 0)
    check_pos(ucryox, 1)
    wm(ucryox)
    #ans = yesno('move cryox ...')
    ans = True
    if ans:
        umv(ucryox, 10)
    check_pos(ucryox, 10)
    wm(ucryoy)
    #ans = yesno('move cryoy ...')
    ans = True
    if ans:
        umv(ucryoy, 70)
    check_pos(ucryoy, 70)

def sc5408_fltube_to_fltwin():
    try:
        check_pos(ucryoy, 0)
        check_pos(ucryox, 1)
        return
    except RuntimeError:
        pass
        
    print('moving flight tube to flt_win position ...')
    check_pos(ucryoy, 70)
    check_pos(ucryox, 10)
    wm(ucryoy)
    #ans = yesno('move cryoy ...')
    ans = True
    if ans:
        umv(ucryoy, 0)
    check_pos(ucryoy, 0)
    wm(ucryox)
    #ans = yesno('move cryox ...')
    ans = True
    if ans:
        umv(ucryox, 1)
    check_pos(ucryox, 1)
