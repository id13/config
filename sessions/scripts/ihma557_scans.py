print('ihma557_scans - load 7')
MA6423_SIM = False
START_KEY = 'roi1'

def doo_setup(posname):
    rstp(posname)
    dsprefix = f'{START_KEY}_posname'
    return dsprefix


def doo_setup_hr(posname):
    rstp(posname)
    dsprefix = f'{START_KEY}_{posname}_hr'
    return dsprefix



def doo_fine(dsprefix):
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, -0.6, 0.0, 1, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
def doo_coarse(dsprefix):
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -50,50,200, nnp3, -50, 50, 200, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, -0.6, 0.0, 1, nnp2, -50,50,200, nnp3, -50, 50, 200, 0.02)

def rot_scan_series(dsname_base, th_start, th_stop, nintervals, *p):
    curr_th = Theta.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        #mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            mv(Theta, th)
            print(f'read Theta pos: {Theta.position}')
            if MA6423_SIM:
                print("SIM: ************* loff_kmap", *p)
            else:
                loff_kmap(*p)
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)
    
###############################################
############## ihma557 ########################
###############################################


def doo_day1_a():
    dsprefix = f'{START_KEY}_CsFAPbIBr'
    rot_scan_series(dsprefix, -0.3, 0.3, 6, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)




def doo_radam_loop(START_KEY,expt):
    dsprefix = f'{START_KEY}_CsPbIBr_41_radam_loop'
    dname=f'{dsprefix}_{expt}s'
    newdataset(dname)
    npoints = int(expt//0.005)
    loopscan(npoints,0.005)
    
    mvr(nny,0.1)
    npoints = int( expt//0.01)
    loopscan(npoints,0.01)
    
    mvr(nny,0.1)
    npoints = int(expt//0.02)
    loopscan(npoints,0.02)
    
    mvr(nny,0.1)
    npoints = int(expt//0.05)
    loopscan(npoints,0.05)
    
    
    mvr(nny,0.1)
    npoints = int(expt//0.1)
    loopscan(npoints,0.1)
    


def doo_radam_exp(START_KEY,expt):
    dsprefix = f'{START_KEY}_CsPbIBr_41_radam_expt'
    dname=f'{dsprefix}_{expt}s'
    newdataset(dname)
    
    fshopen()
    sleep(expt)
    sct()
    fshclose()

    

def doo_radam_scan_size(START_KEY):

    dsprefix = f'{START_KEY}_CsPbIBr_41_radam_scan'
    dname=f'{dsprefix}_100nm_step'
    newdataset(dname)
    kmap.dkmap(nnp2,-10,10,200, nnp3,-10,10,200,0.02)
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_200nm_step'
    newdataset(dname)
    kmap.dkmap(nnp2,-10,10,100, nnp3,-10,10,100,0.02)
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_100nm_asym'
    newdataset(dname)
    kmap.dkmap(nnp2,-10,10,200, nnp3,-10,10,100,0.02)
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_100nm_asym_long'
    newdataset(dname)
    kmap.dkmap(nnp2,-5,5,100, nnp3,-10,10,50,0.02)
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_100nm_asym_large'
    newdataset(dname)
    kmap.dkmap(nnp2,-5,5,100, nnp3,-2.5,2.5,25,0.02)
    
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_100nm_short'
    newdataset(dname)
    kmap.dkmap(nnp2,-2.5,2.5,50, nnp3,-2.5,2.5,50,0.01)
    
    
    mvr(nny,0.1)
    
    dname=f'{dsprefix}_100nm_long'
    newdataset(dname)
    kmap.dkmap(nnp2,-2.5,2.5,50, nnp3,-2.5,2.5,50,0.05)
    
    

    
    


def doo_night_1():
    
    so()
    sleep(3)
    START_KEY = 'roi1'
    rstp('roi1._pos')
    dsprefix = f'{START_KEY}_CsPbIBr_40_rocking_'
    dname=f'{dsprefix}'
    newdataset(dname)
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
    START_KEY = 'roi2'
    rstp('roi2._pos')
    dsprefix = f'{START_KEY}_CsPbIBr_40_rocking_'
    dname=f'{dsprefix}'
    newdataset(dname)
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
    
    fshtrigger()
    
    START_KEY = 'roi3'
    rstp('roi3._pos')
    doo_radam_scan_size(START_KEY)
    
    fshtrigger()
    
    START_KEY = 'roi4'
    rstp('roi4._pos')
    for k in range (10):
        umvr(nny,0.1)
        doo_radam_exp(START_KEY,k)
        
        
    
    sc()




def doo_day_2():
    
    so()
    sleep(3)
    START_KEY = 'poi1'
    rstp('poi1._pos')
    dsprefix = f'{START_KEY}_CsPbIBr_41_rocking_'
    dname=f'{dsprefix}'
    newdataset(dname)
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
    START_KEY = 'roi2'
    rstp('poi2._pos')
    dsprefix = f'{START_KEY}_CsPbIBr_41_rocking_'
    dname=f'{dsprefix}'
    newdataset(dname)
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
    
    fshtrigger()
    
    START_KEY = 'poi3'
    rstp('poi3._pos')
    doo_radam_scan_size(START_KEY)
    
    fshtrigger()
    
    START_KEY = 'poi4'
    rstp('poi4._pos')
    for k in range (10):
        umvr(nny,0.1)
        doo_radam_exp(START_KEY,k)
        
        
    
    sc()



    

def any_main():
    try:
        so()
        sleep(3)
        doo_day1_a()
    finally:
        sc()
        sc()
        sc()
