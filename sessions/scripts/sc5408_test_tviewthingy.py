from collections import OrderedDict load_script('sc5408_tview_align')

tv_motdc = make_motdc()
zkap = Zkap(tv_motdc, arm=True)

zkap.c_amp = 0.2277509999999996
zkap.phi_offset = -5.999966999999997
zkap.c_cenx_k0  = -7.7072980000000015
zkap.c_ceny_k0  = 27.073999999999998
zkap.c_cenz_k0  = 6.352996000000001
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.22990400000000122
zkap.phi_offset = 17.100080000000002
zkap.c_cenx_k0  = -7.703002999999999
zkap.c_ceny_k0  = 27.06
zkap.c_cenz_k0  = 6.352996000000001
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.22990400000000122
zkap.phi_offset = 107.10008499999999
zkap.c_cenx_k0  = -7.703002999999999
zkap.c_ceny_k0  = 27.06
zkap.c_cenz_k0  = 6.352996000000001
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.22990400000000122
zkap.phi_offset = 107.10008499999999
zkap.c_cenx_k0  = -7.703002999999999
zkap.c_ceny_k0  = 27.06
zkap.c_cenz_k0  = 6.352996000000001
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

from collections import OrderedDict
load_script('sc5408_tview_align')

tv_motdc = make_motdc()
zkap = Zkap(tv_motdc, arm=True)

# zkap: ######## seond calib of pillar 50
zkap.c_amp = 0.22990400000000122
zkap.phi_offset = 17.100059
zkap.c_cenx_k0  = -7.703002999999999
zkap.c_ceny_k0  = 27.06
zkap.c_cenz_k0  = 6.352996000000001
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0)), (10, (10.000076, 0.5863039999999984, -0.0400999999999998)), (20, (20.000055, 1.140900000000002, -0.17498699999999978)), (-10, (-10.00007, -0.5603029999999976, -0.07409599999999994)), (-20, (-20.000013, -1.1168059999999969, -0.23928800000000017)), (-30, (-30.000146, -1.6277010000000018, -0.5051870000000003)), (-40, (-38.780645, -2.036102999999997, -0.8089900000000005))])


