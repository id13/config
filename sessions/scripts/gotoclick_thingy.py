from bliss.scanning.scan_tools import get_flint
def gcp23():
    f = get_flint(creation_allowed=False, mandatory=False)
    p = f.get_live_plot("default-scatter")
    position = p.select_points(1)

    (pos_nnp2, pos_nnp3) = position[0]
    print('initial nnp3,nnp3:', nnp2.position, nnp3.position)
    print('clicekd          :', pos_nnp2, pos_nnp3)
    print('moving nnp2 ...')
    mv(nnp2, pos_nnp2)
    print('moving nnp3 ...')
    mv(nnp3, pos_nnp3)
    print('new              :', nnp2.position, nnp3.position)

