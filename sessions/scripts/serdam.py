print('serdam - load 4')
import sys
from os import path
import time
import gevent
from time import time as tim
import json

from scipy.interpolate import interp1d

def write_json(fxpth, dc, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'w') as f:
        json.dump(dc, f)

def read_json(fxpth, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'r') as f:
        dc = json.load(f)
    return dc

def flintp(ax, ay, bx, by, x):
    dx = bx - ax
    dy = by - ay
    m = dx/dy
    y = m*(x - ax) + ay
    return y

class Menv(object):

    ALLOWED = ''.split()
    ROLES = dict()

    def __init__(self):
        self.motdc = motdc = dict()
        roles = self. ROLES
        for rn, mn in roles.items():
            mot = config.get(roles[rn])
            assert mot.name in self.ALLOWED
            motdc[rn] = mot
            print(f'role name: {rn}   tobe_motname = {mn} motname: {motdc[rn].name}')

    def get_posdc(self):
        posdc = dict()
        motdc = self.motdc
        for rn in motdc:
            posdc[rn] = motdc[rn].position
        return posdc

    def qmv(self, rn, tp):
        print(f'{(rn, tp)=}')
        mv(self.motdc[rn], tp)
        
    def qmvr(self, rn, tdist):
        mvr(self.motdc[rn], tdist)

    def stp(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        assert not path.exists(fnp)
        alldc = dict(
            allowed = self.ALLOWED,
            roles = self.ROLES,
            posdc = self.get_posdc()
        )
        write_json(fnp, alldc)

    def fnx_to_fnpath(self, fnx):
        assert not '/' in fnx
        if not fnx.endswith('.qj'):
            assert not '.' in fnx
            return f'{fnx}.qj'
        else:
            return fnx
        raise ValueError()

    def fnx_to_sfn(self, fnx):
        assert not '/' in fnx
        if fnx.endswith('.qj'):
            return fnx[:-3]
        else:
            assert not '.' in fnx
            return fnx

    def read_alldc(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        return read_json(fnp)

    def get_current_posdc(self):
        posdc = dict()
        for k, mot in self.motdc.items():
            posdc[k] = mot.position
        return posdc

    def rstp(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        alldc = self.read_alldc(fnp)
        assert self.ALLOWED == alldc['allowed']
        assert self.ROLES == alldc['roles']
        posdc = alldc['posdc']
        self.move_to_posdc(posdc)

    def move_to_posdc(self, posdc):
        for k,v in posdc.items():
            self.qmv(k,v)

    def to_posdc_str(self, posdc):
        for k,v in posdc.items():
            print(f'{k:10} = {v:12.5f}')

    def show_pos(self, obj=None, title='pos:'):
        if None is obj:
            posdc = self.get_current_posdc()
        elif isinstance(obj, str):
            posdc = self.read_alldc(obj)['posdc']
        else:
            posdc = obj
            obj.setdefault
        print(title)
        print(self.to_posdc_str(posdc))
        
        

class StrMenv(Menv):

    ALLOWED = 'ustrx ustry ustrz'.split()
    ROLES = dict(
        x = 'ustrx',
        y = 'ustry',
        z = 'ustrz',
    )



class Trajectory1(object):

    ROLES_LL = ''.split()

    def __init__(self, menv, master_rn, slave_rnll):
        self.master_rn = master_rn
        self.slave_rnll = slave_rnll
        self._refdc = dict(A=None, B=None)
        self.menv = menv
        self.traj = (None,None)
        assert set(menv.ROLES.keys()).issubset(set(self.ROLES_LL))

    def set_ref(self, refk, fnx):
        assert refk in('A', 'B')
        self.menv.stp(fnx)
        self._refdc[refk] = fnx

    def setup_trajectory(self):
        #def flintp(ax, ay, bx, by, x):
        fnx_a = self._refdc['A']
        aposdc = self.menv.read_alldc(fnx_a)['posdc']
        fnx_b = self._refdc['B']
        bposdc = self.menv.read_alldc(fnx_b)['posdc']
        self.traj = (aposdc, bposdc)

    def traj_mv(self, rn, tp):
        assert(rn == self.master_rn)
        (aposdc, bposdc) = self.traj
        ax = aposdc[self.master_rn]
        bx = bposdc[self.master_rn]
        t_posdc = dict()
        for srn in self.slave_rnll:
            ay = aposdc[srn]
            by = bposdc[srn]
            _interp = interp1d((ax,bx),(ay,by), kind='linear')
            #srn_tp = flintp(ax, ay, bx, by, tp)
            srn_tp = float(_interp(tp))
            t_posdc[srn] = srn_tp
        self.menv.show_pos(obj=t_posdc, title='target pos:')
        self.menv.show_pos(obj=None, title='current pos (before):')
        self.menv.move_to_posdc(t_posdc)
        self.menv.show_pos(obj=None, title='current pos (after):')

class Trajectory1_Xyz(Trajectory1):

    ROLES_LL = 'x y z'.split()


class FastLoop(object):

    def __init__(self, nframes, expt, nfpf=None):
        self.nframes = nframes
        self.expt = expt
        eiger = config.get('eiger')
        if None is nfpf:
            nfpf = eiger.saving.frames_per_file
        assert(nfpf % nfpf == 0)
        self.nfpf = nfpf
        self.ueiger = config.get('ueiger')

    def run(self):
        eiger.saving.frames_per_file = self.nfpf
        self.ueiger.ttl_software_scan_hws(
            self.nframes, self.expt)

def xxx_test(i):
    xyz = StrMenv()
    traj = Trajectory1_Xyz(xyz, 'x', 'y z'.split())
    xyz.rstp('a4')
    traj.set_ref('A', f'a{i}')
    xyz.rstp('b4')
    traj.set_ref('B', f'b{i}')
    traj.setup_trajectory()
    return xyz, traj

VOID_S = 'VOID____VOID'
class SerDam(object):

    LOG_KEYS = 'expt expwin settlewin deltay npoints latency deltaz nlines dsname'
    LOG_KEYS.extend('log_time log_asc_time'.spit())
    LOG_KEYS.extend('start_time start_asc_time'.spit())
    LOG_KEYS.extend('stop_time stop_asc_time'.spit())
    LOG_KEYS.extend('stat_u18 stat_ct22 stat_24 stat_pvg stat_phg stat_energy'.split())


    def __init__(self, expt, expwin, settlewin, deltay, npoints, latency=3.0):

        self.version = '1.1.0'
        for k in self.LOG_KEYS:
            setattr(self, VOID_S)
        self.expt = expt
        self.expwin = expwin
        self.settlewin = settlewin
        self.deltay = deltay
        self.npoints = npoints
        self.latency = latency
        self.staticidc = dict()
        self.operation_key = VOID_S

    def gather_static(self):
        self.stat_time, self.stat_asc_time = self.get_tmpair()
        self.stat_tm = time.time()
        self.stat_u81 = u18.position
        self.stat_ct22, stat['ct24'] = self.diag_count()
        self.stat_pvg = pvg.position
        self.stat_phg = pvg.position
        self.stat_energy = ccmo.get_energy()

    def diag_count(self):
        # ...
        ct22 = -100.0
        ct24 = -100.0
        return ct22, ct24

    def start_log(self, opkey):
        self.operation_key = opkey
        tmp = self.get_tmpair()
        self.start_time, self.start_asc_time = tmp
        self.stop_time = -1
        self.stop_asc_time = -1
        self.log_time, self.log_asc_time = tmp
        logdc = self.make_logdc()

    def get_tmpair(self):
        t = time.time()
        at = time.asctime(time.localtime())
        return t, at

    def make_logdc(self):
        logdc = dict()
        for k in self.LOG_KEYS:
            logdc[k] = getattr(self, k)

    def save_log(self, logdc, key, logname):
        save_fupth = f'LOOP_LOG_{key}_{logname}.serlog'
        write_json(fupth, logdc):

    def load_log(self, key, logname):
        load_fupth = f'LOOP_LOG_{key}_{logname}.serlog'
        return read_json(fupth):

    def get_log_str(self, logdc):
        ll = [f'==== SerDam object [version = {self.version}]']
        for k in self.LOG_KEYS:
            ll.append(f'{k:10s} = {logdc[k]})

    def show_log(self, logdc):
        log_s = self.get_log_str(logdc)
        print(log_s)

    def setup_fastloop(self, innerwin):
        self.innerwin = innerwin
        _nframes = int(innerwin/self.expt) + 100
        nfpf = self.nfpf = _nframes//100
        nframes = self.nframes = 100*nfpf
        self.fl = FastLoop(nframes, self.expt, nfpf=nfpf)

    def run_fastloop(self):
        self.fl.run()

    def setup_slow_loop(self, nframes=None):
        self.slow_nframes = nframes

    def run_slow_loop(self):
        npts = int(0.5*self.slow_nframes)
        loopscan(npts, self.expt)

    def slow_timing(self):
        exptll = []
        tmll = []
        self.setup_slow_loop(200)
        for i in range(20):
            self.expt = 0.0005*(i+1)
            exptll.append(self.expt)
            tmll.append(self.timethis(self.run_slow_loop))
        print(exptll)
        print(tmll)
        plot(tmll)

    def timethis(self, m):
        t = tim()
        m()
        return tim()-t

    def run_outer_section(self):
        self.outer = gevent.spawn(self.run_fastloop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section)
        cont = 5
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    def run_outer_section_2(self):
        self.outer = gevent.spawn(self.run_fastloop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section_2)
        cont = 2
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    def run_outer_section_3(self):
        self.outer = gevent.spawn(self.run_slow_loop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section_2)
        cont = 2
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    def upd(self):
        print(f'{bool(self.outer)=}  {bool(self.inner)=}')

    def run_inner_section(self):
        ini_ypos = ustry.position
        try:
            deltay = self.deltay
            settlewin = self.settlewin
            expwin = self.expwin
            for i in range(self.npoints):
                mv(ustry, ini_ypos + i*deltay)
                sleep(settlewin)
                fshopen()
                sleep(expwin)
                fshclose()
        finally:
            mv(ustry, ini_ypos)

    # fshopen fshclose : 0.1195 s
    # >> mv stry to delta 0.01: 0.0605

    def run_inner_section_2(self):
        outer = self.outer
        ini_ypos = ustry.position
        try:
            deltay = self.deltay
            settlewin = self.settlewin
            expwin = self.expwin
            for i in range(self.npoints):
                print(f'{i=}')
                if not outer:
                    self.final_i = i
                    return i
                mv(ustry, ini_ypos + i*deltay)
                sleep(settlewin)
                fshopen()
                sleep(expwin)
                fshclose()
        finally:
            mv(ustry, ini_ypos)


    def multiexp_dataset_loop(self, dsname, delta_z, nlines):
        stp(f'LOOP_POS_{dsname}')
        newdataset(dsname)
        ini_zpos = ustrz.position
        for i in range(nlines):
            zpos = i*delta_z + ini_zpos
            print(f'cycle: {i=}   {zpos=}')
            mv(ustrz, zpos)
            self.run_outer_section_2()
