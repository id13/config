print('hg169_day2_2 load 6')
import time

print("=== hg169_day2_2 ===")

DSKEY='l' # to be incremented if there is a crash

class EXPO:

    expt = 0.02

class FLUX(object):

    tag = 'noneFX'

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 2.0

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



def hi_flux():
    FLUX.tag = 'hi6p31FX'
    umv(u18, 6.31)

def lo_flux():
    FLUX.tag = 'lo6p13FX'
    umv(u18, 6.13)

def doloop(s, npts, expt):
    print("\n\n\n======================== doing loop:", s, ' n=', npts, '  expt =', expt)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)

    print("\n[loopscan]:")
    fshtrigger()
    loopscan(npts, expt)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)


def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def raw_hg169_day2_1():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    lo_flux()
    dooul('leti2_S2_ul53_1x1_475x145_950x290_10_0059.json')
    dooul('leti2_S4_ul52_1x1_350x135_700x270_10_0057.json')
    dooul('leti2_S7_ul54_1x1_470x200_940x400_10_0060.json')
    dooul('leti2_S8_ul51_1x1_320x160_640x320_10_0056.json')
    dooul('leti2_S9_ul50_1x1_400x150_800x300_10_0055.json')
    
def raw_hg169_day2_3():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    #lo_flux()
    dooul('Pa_plate_PaDrLiW_NA_ul01_2x2_400x400_800x800_10_0086.json')
    
    
def raw_hg169_day2_4():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    #lo_flux()
    # dooul('Pa_plate_PaDrLi_OV_ul01_2x2_400x400_800x800_10_0089.json') 
    #dooul('Pa_plate_PaDrLi_OV_ul01_4x4_200x200_400x400_10_0089.json') 
    #dooul('Pa_plate_PaDrLi_OV_ul01_1x1_500x500_1500x1500_10_0089.json')   
    dooul('Pa_plate_PaDr_OV_ul01_2x2_375x350_750x700_10_0087.json')
    #dooul('Pa_plate_PaDr_OV_ul01_1x1_500x500_1500x1500_10_0087.json')     

def raw_hg169_day2_2():

            
    # damage hydrocerussite, cerussite and lead white only 50um sections
    
    lo_flux()
    EXPO.expt = 0.010
    dooul('ermanno_C50_mapt1_ul01_1x1_25x50_50x100_10_0070.json')
    dooul('ermanno_HC50_mapt1_ul01_1x1_25x50_50x100_10_0062.json')
    dooul('ermanno_LW50_mapt1_ul01_1x1_25x50_50x100_10_0078.json')
    
    lo_flux()
    EXPO.expt = 0.10
    dooul('ermanno_C50_mapt2_ul01_1x1_25x50_50x100_100_0071.json')
    dooul('ermanno_HC50_mapt2_ul01_1x1_25x50_50x100_100_0063.json')  
    dooul('ermanno_LW50_mapt2_ul01_1x1_25x50_50x100_100_0079.json')
     
    lo_flux()
    EXPO.expt = 1
    dooul('ermanno_C50_mapt3_ul01_1x1_25x50_50x100_1000_0072.json')
    dooul('ermanno_HC50_mapt3_ul01_1x1_25x50_50x100_1000_0064.json')  
    dooul('ermanno_LW50_mapt3_ul01_1x1_25x50_50x100_1000_0080.json')
    
    hi_flux()
    EXPO.expt = 0.010 
    dooul('ermanno_C50_mapt4_ul01_1x1_25x50_50x100_10_0073.json')
    dooul('ermanno_HC50_mapt4_ul01_1x1_25x50_50x100_10_0065.json')  
    dooul('ermanno_LW50_mapt4_ul01_1x1_25x50_50x100_10_0081.json')
    
    hi_flux()
    EXPO.expt = 0.10 
    dooul('ermanno_C50_mapt5_ul01_1x1_25x50_50x100_100_0074.json')
    dooul('ermanno_HC50_mapt5_ul01_1x1_25x50_50x100_100_0066.json')  
    dooul('ermanno_LW50_mapt5_ul01_1x1_25x50_50x100_100_0082.json')
    
    hi_flux()
    EXPO.expt = 1
    dooul('ermanno_C50_mapt6_ul01_1x1_25x50_50x100_1000_0075.json')
    dooul('ermanno_HC50_mapt6_ul01_1x1_25x50_50x100_1000_0067.json')  
    dooul('ermanno_LW50_mapt6_ul01_1x1_25x50_50x100_1000_0083.json')
    
         
         
    lo_flux()
    doloop('ermanno_C50_point1_0076.json',10000, 0.01)
    doloop('ermanno_HC50_point1_0068.json',10000, 0.01)
    doloop('ermanno_LW50_point1_0084.json',10000, 0.01)   
    
    hi_flux() 
    doloop('ermanno_C50_point2_0077.json',10000, 0.01)
    doloop('ermanno_HC50_point2_0069.json',10000, 0.01)
    doloop('ermanno_LW50_point2_0085.json',10000, 0.01)   
    
    
    

def hg169_day2_2():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_2()
        sc()
    finally:
        sc()
        sc()
        sc()



def hg169_day2_3():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_3()
        sc()
    finally:
        sc()
        sc()
        sc()


def hg169_day2_4():
    try:
        so()
        time.sleep(1)
        raw_hg169_day2_4()
        sc()
    finally:
        sc()
        sc()
        sc()


    
    
    
    
    
    
    
