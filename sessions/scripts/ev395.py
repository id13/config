print('hello carlos')
import time
def ev395_night1():
    so()
    newdataset('pos_1_take1')
    umv(nnx,0.703)
    umv(nny,-2.8937)
    umv(nnz,-0.8594)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        dmesh(nnp2,30,-30,60,nnp3,-30,30,60,0.02)
    newdataset('pos_4_take1')
    umv(nnx,0.723)
    umv(nny,-2.8537)
    umv(nnz,-2.2294)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        dmesh(nnp2,30,-30,60,nnp3,-30,30,60,0.02)
    sc()
    sc()


def ev395_night1_take2():
    so()
    newdataset('pos_1_take2')
    umv(nnx,0.703)
    umv(nny,-2.8937)
    umv(nnz,-0.8594)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        dmesh(nnp2,30,-30,60,nnp3,-30,30,60,0.02)
    newdataset('pos_4_take2')
    umv(nnx,0.723)
    umv(nny,-2.8537)
    umv(nnz,-2.2294)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        dmesh(nnp2,30,-30,60,nnp3,-30,30,60,0.02)
    
    sc()
    sc()
    
def ev395_day2_take3():
    # using kamap now
    so()
    newdataset('pos_1_take3')
    umv(nnx,0.703)
    umv(nny,-2.8937)
    umv(nnz,-0.8594)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02)

    newdataset('pos_4_take3')
    umv(nnx,0.723)
    umv(nny,-2.8537)
    umv(nnz,-2.2294)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02)
    
    sc()
    sc()
def ev395_day2_fresh3_restart():
    # using kamap now
    so()
    newdataset('freshpos_3_take1_restart')
    rstp('fresh_pos3')
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02,frames_per_file=360)
    
    sc()
    sc()
    
def ev395_day2_take4():
    # using kamap now
    so()
    newdataset('pos_1_take4')
    umv(nnx,0.703)
    umv(nny,-2.8937)
    umv(nnz,-0.8594)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02,frames_per_file=360)

    newdataset('pos_4_take4')
    umv(nnx,0.723)
    umv(nny,-2.8537)
    umv(nnz,-2.2294)
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02,frames_per_file=360)
    
    sc()
    sc()
    
def ev395_day2_dinner():
    # using kamap now
    so()
    newdataset('wonderland2_take1')
    rstp('wonderland2')
    stp = 0.2
    steps = 20
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,30,-30,60,nnp3,-30,30,60,0.02,frames_per_file=360)
    
    sc()
    sc()
    
def ev395_day2_largefov():
    # using kamap now
    so()
    newdataset('wonderland2_FOV_patched')
    rstp('wonderland2')
    
    kmap.dkmap(nnp2,0,-40,80,nnp3,0,40,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,0,-40,80,nnp3,-40,0,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,40,0,80,nnp3,0,40,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,40,0,80,nnp3,-40,0,80,0.01,frames_per_file=360)
    
    kmap.dkmap(nnp2,0,-40,80,nnp3,-80,-40,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,40,0,80,nnp3,-80,-40,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,0,-40,80,nnp3,40,80,80,0.01,frames_per_file=360)
    kmap.dkmap(nnp2,40,0,80,nnp3,40,80,80,0.01,frames_per_file=360)
    
    sc()
    sc()

def ev395_day2_night():
    # using kamap now
    so()
    newdataset('bonnie_retake2')
    rstp('bonnie._pos')
    stp = 0.2
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)
	
    mv (Theta, theta_start -theta_back)
    newdataset('small_bone_retake2')
    rstp('small_bone._pos')
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)
    	
    mv (Theta, theta_start -theta_back)
    newdataset('point_retake2')
    rstp('point._pos')
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)
    sc()
    sc()
    
def ev395_day3_morning():
    # using kamap now
    so()
    newdataset('hope')
    rstp('hope._pos')
    stp = 0.2
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)    

def ev395_day3_lunch():
    # using kamap now
    so()
    newdataset('friday_lunch_take2')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
    time.sleep(1800)
    newdataset('friday_lunch_take3')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
    time.sleep(1800)
    newdataset('friday_lunch_take4')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
def ev395_test():
    print('asdf')
    time.sleep(10)
    print('asdf +10')

def ev395_day3_afternoon():
    # using kamap now
    so()
    newdataset('friday_lunch_take5_rock')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)  
    	
def ev395_day3_radtest30m():
    # using kamap now
    so()
    newdataset('friday_spot2_take2')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
    time.sleep(1800)
    newdataset('friday_lunch_take3')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
    time.sleep(1800)
    newdataset('friday_lunch_take4')
    umv(Theta,21)
    kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.01,frames_per_file=360)
    
def ev395_day3_consentful_macro():
    so()
    newdataset('little')
    rstp('little._pos')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)  
    newdataset('big')
    rstp('big._pos')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)   
    newdataset('big2')
    rstp('big2._pos')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360) 
    newdataset('infilling')
    rstp('infilling._pos')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)   
    mv(Theta,10)
    newdataset('calc_fish')
    rstp('fishing_start1_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)
    rstp('fishing_start2_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)    
    mv(Theta,9)
    newdataset('calc_fish')
    rstp('fishing_start1_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)
    rstp('fishing_start2_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)  
    
    sc()
    sc()
    
def ev_395_savethenight():
    so
    umv(Theta,10)
    newdataset('calc_fish')
    rstp('fishing_start1_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)
    rstp('fishing_start2_definitiv')
    kmap.dkmap(nnp3,-100,100,50,nnp2,14,-14,120,0.1,frames_per_file=360)  
    
def ev_395_savethenight_filling():
    so()
    newdataset('infilling')
    rstp('infilling._pos')
    stp = 0.1
    steps = 60
    theta_peak = 18
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv(Theta,20)
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,20,-20,80,nnp3,-20,20,80,0.02,frames_per_file=360)      


def ev_395_saturday_cooking():
    so()
    newdataset('dead_tissue_restart')
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    
    
    sc()
def ev_395_saturday_dead_tissue():
    so()
    newdataset('dead_tissue_restart')
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    
    
    sc()
def ev_395_saturday_dead_zwei():
    so()
    newdataset('dead_tissue_zwei')
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    mv(Theta,10)
    kmap.dkmap(nnp3,-30,30,120,nnp2,6.25,-6.25,50,0.0015,frames_per_file=360)
    mv(Theta,0)
    time.sleep(300)
    
    
    sc()


def ev395_saturday_dinner():
    so()
    mv(nnp2,132,nnp3,125)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,124.3,nnp3,169)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,136.4,nnp3,165)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,133.4,nnp3,113)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,119.9,nnp3,165)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,129.2,nnp3,169)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,133.6,nnp3,169)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    mv(nnp2,133.9,nnp3,177)
    umv(Theta,7)
    dscan(Theta,1,5,160,0.05)
    
    sc()
    sc()
    
def ev395_saturday_nightfever():
    so()
    newdataset('point_1')
    rstp('point_1')
    stp = 0.1
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)
    newdataset('point_2')
    rstp('point_2')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)   
    newdataset('point_3')
    rstp('point_3')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)
    newdataset('point_4')   
    rstp('point_4')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)
    newdataset('point_6')      
    rstp('point_6')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)
    newdataset('point_7')
    rstp('point_7')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp2,6.4,-6.4,80,nnp3,-20,20,80,0.02,frames_per_file=400)   
    sc()
    sc()
def ev395_sunday_morning():
    so()
    stp = 0.1
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    newdataset('point_4_refoc')   
    rstp('point_4_refoc')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i-1)*30*0.160
        kmap.dkmap(nnp2,12.8,-12.8,160,nnp3,-20,20,80,0.005,frames_per_file=400) 
	
def ev395_sunday_latemorning():
    so()
    stp = 0.1
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    newdataset('point_3_refoc')   
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*1.5
        kmap.dkmap(nnp2,25.6-1.92-off_nnp2,-25.6-1.92-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400) 
def ev395_sunday_lunch():
    so()
    stp = 0.1
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    newdataset('new_point_1_scan')   
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*1.92
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400) 
def ev395_sunday_afterlunch():
    so()
    stp = 0.1
    steps = 40
    theta_peak = 21
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    newdataset('new_point_3_scan')   
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400) 

def ev395_sunday_night():
    so()
    stp = 0.1
    steps = 40
    theta_peak = 20
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    newdataset('disci')
    rstp('disci_zoom_in_20th') 
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400) 
    newdataset('edgi')
    rstp('edgi_zoom_in_th20') 
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400)
    newdataset('insidi')
    rstp('insidi_zoom_in_th20') 
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400)
    newdataset('far_insidi')
    rstp('far_insidi_zoom_in_th20') 
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400)
    newdataset('really_far_insidi')
    rstp('really_far_insidi_zoom_in_th20')
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400)
    sc()
    time.sleep(3600)
    so()
    newdataset('disci_take2')
    rstp('disci_zoom_in_20th') 
    mv (Theta, theta_start -theta_back)
    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        off_nnp2 = (i)*2.56
        kmap.dkmap(nnp2,25.6-off_nnp2,-25.6-off_nnp2,160,nnp3,-20,20,80,0.005,frames_per_file=400) 
    sc()
				
	
		

