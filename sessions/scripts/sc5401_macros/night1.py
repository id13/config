print('karl_patch_scan - load 2')
import gevent,bliss, time

def make_patch_scan_dsname(dsname_tag, i, j):
    return f'{dsname_tag}_{i:02}_{j:02}'

def doo_kmap(
    start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout='auto'):
    mv(nnp2, start_p2)
    mv(nnp3, start_p3)
    width_p2 = dp2*np2
    width_p3 = dp3*np3
    est_timeout = np2*np3*(expt+0.0007)*1.2 + 40
    print(f'estimated timeout = {est_timeout}')
    if 'auto' == timeout:
        timrout = est_timeout
    print(f'patch kmap: timeout={timeout}')
    t0 = time.time()
    try:
        with gevent.Timeout(seconds=timeout):
            kmap.dkmap(nnp2, 0, width_p2, np2, nnp3, 0, width_p3, np3, expt)
            fshtrigger()
            return 0
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print(f'caught hanging scan after {time.time() - t0} seconds timeout')
        return 1


def radiation_damage_scan(start_p2, start_p3, dp2, dp3, np2, np3, expt, timeout='auto', repeats=1):
    if None == timeout:
        for 
        kmap.dkmap(nnp2, 0, width_p2, np2, nnp3, 0, width_p3, np3, expt)

def hexayz_nnp23_patch_scan(dsname_tag,
    start_y, start_z, dy, dz, ny, nz,
    start_p2, start_p3, dp2, dp3, np2, np3,
    expt, timeout='auto', yspc=0.0, zspc=0.0):
    ctr = 0
    for i in range(nz):
        for j in range(ny):
            ctr += 1
            print('==== new patch ================')
            print(f'internal ctr = {ctr}')
            dsname = make_patch_scan_dsname(dsname_tag, i, j)
            if ctr < 4:
                print(f'!!!!!!!!!!: skipping {dsname}')
                continue
            newdataset(dsname)
            new_cur_y = start_y + j*(dy + yspc) 
            new_cur_z = start_z + i*(dz + zspc)
            print(f'dsname: {dsname}')
            print(f'requested pos: y =  {new_cur_y}    z = {new_cur_z}\n')
            mv(nny, new_cur_y)
            mv(nnz, new_cur_z)
            print(f'actual pos:    y =  {nny.position}    z = {nnz.position}\n')
            doo_kmap(start_p2, start_p3, dp2, dp3, np2, np3, expt,
                timeout=timeout)
            print('5 sec to interrupt ...')
            time.sleep(5)
