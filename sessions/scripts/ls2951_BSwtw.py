import time

def dummy(*p):
    print("dummy:",p)

def emul(*p):
    (rng,nitv,_tmp1,_tmp2,expt,ph,pv) = tuple(p)
    print("emul:",p)
    mvr(ustry,ph*rng,ustrz,pv*rng)

# newdataset = dummy
# enddataset = dummy
# dkpatchmesh = emul
print("=== ls2951_BSiwtw ARMED ===")

def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng,nitv,expt,ph,pv):
    if expt < 0.01:
        expt = 0.01
    if expt < 0.02 and nitv < 401:
        retveloc = 1.0
    else:
        retveloc = 0.25
    dkpatchmesh(rng,nitv,rng,nitv,expt,ph,pv,retveloc=retveloc)



DSKEY='g'

def dooul(s):
    print("\n\n\n======================== doing:", s)

    w = s.split('_')

    (ph,pv) = w[-4].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv,_tmp) = w[-3].split('x')

    nitv = int(nitv)
    expt = float(w[-1])/1000.0
    rng = float(w[-2])/1000.0

    dsname = "%s_%s" % (s, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng,nitv,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def ls2951_BSwtw():


    #dooul('ul01_1x1_100x100_200_20')
    #dooul('ul02_1x1_100x100_200_20')
    #dooul('ul03_1x1_100x100_1000_5')
    ##dooul('ul04_1x1_100x100_1000_5')
    ##dooul('ul05_1x1_100x100_1000_5')
    ##dooul('ul06_1x1_100x100_1000_5')
    dooul('ul07_1x1_100x100_1000_5')
    dooul('ul08_1x1_100x100_1000_5')
    dooul('ul09_1x1_100x100_200_20')
    dooul('ul10_1x1_100x100_200_20')
    dooul('ul11_1x1_100x100_200_20')


