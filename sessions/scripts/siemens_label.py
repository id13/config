print('siemens_label - load 1')
from numpy import random

class SiemensLabel(object):

    def __init__(self):
        pass

    def get_corr_transfer(self):
        pp2 = nnp2.position
        pp3 = nnp3.position
        d_nn2 = 0.001*(pp2-125.0)
        d_nn3 = 0.001*(pp3-125.0)
        return (d_nn2, d_nn3)

    def apply_corr_transfer(self, corr):
        assert max(corr) < 0.2 and min(corr) > -0.2
        d_nn2, d_nn3 = corr
        mvr(nny, d_nn2, nnz, d_nn3)
        mv(nnp2, 125.0, nnp3, 125.0)

    def do_corr_transfer(self):
        corr = self.get_corr_transfer()
        self.apply_corr_transfer(corr)
	
    def mapped_scan(self,amplitude, pixels):
        kmap.dkmap(nnp2, -amplitude/2,amplitude/2,pixels,nnp3, -amplitude/2,amplitude/2,pixels,0.01)

    def default_scan(self):
        kmap.dkmap(nnp2, -2.5,2.5,25,nnp3, -2.5,2.5,25,0.01)

    def refinement_scan(self):
        kmap.dkmap(nnp2, -0.7,0.7,30,nnp3, -0.7, 0.7, 30,0.01)

    def label_run1(self, dsname, nn1_step=0.1, nn1_samples=3, nrand_samples=10):
        stp(f'ini_{dsname}')
        newdataset(dsname)
        zeronnp()
        nn_pos_list = []
        for i in range(nn1_samples):
            if i:
                mvr(nnx, nn1_step)
            self.default_scan()
            gcp23()
            self.refinement_scan()
            gcp23()
            self.default_scan()
            self.do_corr_transfer()
            self.default_scan()
            nn_pos_list.append((nnx.position, nny.position, nnz.position))

        cyc = 0
        for (p_nn1, p_nn2, p_nn3) in nn_pos_list:
            mv(nnx, p_nn1)
            deviations_y = 0.0025*(random.random(nrand_samples)-0.5)/0.5
            deviations_z = 0.0025*(random.random(nrand_samples)-0.5)/0.5
            pixels = random.randint(20,30,nrand_samples)
            amplitude = random.randint(5,10,nrand_samples)
            for (dev_2, dev_3, pixel, amp ) in zip(deviations_y, deviations_z, pixels, amplitude):
                print('cyc =', cyc)
                cyc += 1
                tgt_nn2 = p_nn2 + dev_2
                tgt_nn3 = p_nn3 + dev_3
                print(f'    dev:    dev_2 = {dev_2}; dev_3 = {dev_3}')
                print(f'    target: p_nn1 = {p_nn1}; tgt_nn2 = {tgt_nn2}; tgt_nn3 = {tgt_nn3}')
                mv(nny, tgt_nn2)
                mv(nnz, tgt_nn3)
                self.mapped_scan(amp, pixel)
		

