print('ls3297_serdam_full - load 1')
START_KEY = 'b' #change this letter at each trial

# beloz sswitch to pass for test, and to with bench for actual dat collection
def night3_action(sd):
    #pass
    with bench(): sd.run_flymesh()



    
def ls3297_night3():

    rstp('night3_pos1')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf-fifth_night3_v50_1')
    sd.setup_flymesh(0, 20, 500, 0, 4, 200, 0.0014, retveloc=15)
    sd.setup_fly_speed(50)
    sd.show_log()
    night3_action(sd)
    

    rstp('night3_pos2')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf-fifth_night3_v40_2')
    sd.setup_flymesh(0, 20, 500, 0, 4, 200, 0.0014, retveloc=15)
    sd.setup_fly_speed(40)
    sd.show_log()
    night3_action(sd)


    rstp('night3_pos3')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf-fifth_night3_v30_3')
    sd.setup_flymesh(0, 20, 500, 0, 4, 200, 0.0014, retveloc=15)
    sd.setup_fly_speed(30)
    sd.show_log()
    night3_action(sd)


    rstp('night3_pos4')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf-fifth_night3_v20_4')
    sd.setup_flymesh(0, 20, 500, 0, 4, 200, 0.0014, retveloc=15)
    sd.setup_fly_speed(20)
    sd.show_log()
    night3_action(sd)


def ls3297_main():
    try:
        so()
        ls3297_night3()
    finally:
        sc()
        sc()
        sc()
