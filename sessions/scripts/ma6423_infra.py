MA6423_SIM = False
print(f'ma6423_infra - load 1 (SIM = {MA6423_SIM})')

def rot_scan_series(dsname_base, th_start, th_stop, nintervals, *p):
    curr_th = Theta.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            mv(Theta, th)
            print(f'read Theta pos: {Theta.position}')
            if MA6423_SIM:
                print("SIM: ************* loff_kmap", *p)
            else:
                loff_kmap(*p)
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)

def yrot_scan_series(dsname_base, th_start, th_stop, ystart, ystop, nintervals, *p):
    curr_th = Theta.position
    curr_y = nnp2.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        y_pos_arr = np.linspace(ystart, ystop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            next_y = y_pos_arr[i]
            print(f'next y pos: {next_y}')
            mv(Theta, th)
            mv(nnp2, next_y)
            print(f'read Theta pos: {Theta.position}')
            print(f'read y pos: {nnp2.position}')
            loff_kmap(*p)
            try:
                sleep(1)
                elog_plot(scatter=True)
            except:
                print('elog_plot failure ...')
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)
        mv(nnp2, curr_y)

def posto(name, poi):
    poiname = f'poi{poi:d}_{name}'
    stp(name)
    stp(poiname)

def rmgeig():
    mgeig()
    MG_EH3a.enable('*r:r*')


def indipal1_dx(zpos=None):
    ref_zpos = -2.7513
    pitch = 0.05364238410596027
    if None is zpos:
        zpos = nnz.position
    dx = (zpos - ref_zpos)*pitch
    return dx

def ibdipal1_x(zpos=None):
    ref_xpos = 3.457
    dx = indipal1_dx(zpos=zpos)
    print(f'{dx=}')
    return ref_xpos + dx
    
def newmicp(name):
    stp(name)
    newdataset(name)
