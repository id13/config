def convenience_grid(start_pos0, start_pos1, stp0, stp1, n0, n1):
    lsp0 = np.linspace(start_pos0, start_pos0 + n0*stp0, n0+1)
    lsp1 = np.linspace(start_pos1, start_pos1 + n1*stp1, n1+1)
    ilsp0 = np.arange(n0+1)
    ilsp1 = np.arange(n1+1)
    p0, p1 = np.meshgrid(lsp0, lsp1)
    ip0, ip1 = np.meshgrid(ilsp0, ilsp1)
    
    return(p0,p1,ip0,ip1)


def psi_test_det():
    try:
        fshclose()
        res = bbb_loffkmap.run(nnp2, -1, 1, 10, nnp3, -1, 1, 10, 0.01)
        if res[1] == 'error':
            print('restart recommended!')
    finally:
        fshtrigger()


class ConvGridi2D(object):

    def __init__(self,start_pos0, stp0, n0, start_pos1, stp1=None, n1=None, mot0=None, mot1=None):

        # prep

        self.motors = (mot0, mot1)

        if '<current>' == start_pos0:
            start_pos0 = mot0.position
        if '<current>' == start_pos1:
            start_pos1 = mot1.position


        # symmetric?
        if None is stp1:
            stp1 = stp0
        if None is n1:
            n1 = n0

        # boiler plate
        attnames = 'start_pos0, stp0, n0, start_pos1, stp1, n1'
        values = start_pos0, stp0, n0, start_pos1, stp1, n1
        for (an,v) in zip(attnames, values):
            setattr(self, an, v)

        self.shape = (s0, s1) = ((n0+1),(n1+1))

        self.idxrr = np.arange(s0*s1)
        self.grid = (p0,p1,ip0,ip1) = convenience_grid(start_pos0, start_pos1, stp0, stp1, n0, n1)
        self.flat_grid = list(map(np.ravel, self.grid))

    def get_pos(self, idx):
        (fp0,fp1,fip0,fip1) = self.flat_grid
        return((fp0[idx],fp1[idx]),(fip0[idx],fip1[idx]))

    def goto_pos(self, idx):
        (mot0, mot1) = self.motors
        thepos = (mpos, rccoords) = self.get_pos(idx)
        assert mot0.name in ('nny','nnz','nnp1','nnp2','nnp3')
        mv(mot0, mpos[0])
        mv(mot1, mpos[1])

class Night14Nov(object):

    ACQ_PARAMS_S_1 = '''
        16      2500     0.01
        16      2500     0.001
        16      2500     0.0005
        16      2500     0.0002
         8      5000     0.01
         8      5000     0.001
         8      5000     0.0005
         8      5000     0.0002
         4     10000     0.01
         4     10000     0.001
         4     10000     0.0005
         4     10000     0.0002
    '''
    GRID_PARAMS_1 = ('<current>', 0.05, 5, '<current>')
    #def __init__(self,start_pos0, stp0, n0, start_pos1, stp1=None, n1=None, mot0=None, mot1=None, operation=None):

    

    def __init__(self):
        self._pix_depth = None

    def set_pix_depth(self, pix_depth):
        last_pd = self.pix_depth
        if None is last_bd or last_bd != pix_depth:
            psie_set_pixdepth(pix_depth)

    def _parse_params(self, params_s):
        parll = []
        ll = params_s.split('\n')
        for l in ll:
            l = l.strip()
            if '' == l:
                continue
            pd, nframes, expt = l.split()
            parll.append((int(pd), int(nframes), float(expt)))
        return parll

    def run(self):
        pass
        
