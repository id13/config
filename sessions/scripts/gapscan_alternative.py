import numpy as np
from time import sleep
from math import fabs
from matplotlib import pyplot as plt

counterdict = {
    p201_eh2_0.counters.ct22 : 'p201_eh2_0:ct2_counters_controller:ct22',
    p201_eh2_0.counters.ct25 : 'p201_eh2_0:ct2_counters_controller:ct25'
    
}
def fake_scan(mot, ll, ul, ni, t, ctr1,ctr2, fname="bla.txt", scans=None):
    print ("############################################")
    pos_rr = np.zeros((ni+1,), dtype=np.float64)
    bpos_rr = np.zeros((ni+1,), dtype=np.float64)
    c1_rr = np.zeros((ni+1,), dtype=np.float64)
    c2_rr = np.zeros((ni+1,), dtype=np.float64)
    believed_pos = 100000000
    ntries = 10
    with open(fname, "w") as f:
        stp = (ul-ll)/ni
        for i in range(ni+1):
            flag = False
            mvctr = 0
            print( i)
            pos = ll + i*stp
            outerflg = False
            for k in range(5):
                if outerflg and not flag:
                    print ("ADJUST FAR ==================")
                    mvr(u18, 0.2)
                    sleep(0.02)
                    mvr(u18, -0.2)
                else:
                    pass
                if flag:
                    continue
                else:
                    for j in range(ntries):
                        crit = fabs(believed_pos-pos)
                        print (crit)
                        if crit < 0.004:
                            flag = True
                            continue
                        else:
                            print ("inner move **", j)
                            mv(mot,pos)
                            sleep(0.4)
                            sync()
                            believed_pos = mot.position
                            mvctr += 1
                    print("j =", j)
                    if j > ntries -2:
                        outerflg = True
            ct(t, ctr1,ctr2)
            x = scans[-1]
            data = x.get_data()
            ctr1_val = float(data[counterdict[ctr1]][0])
            ctr2_val = float(data[counterdict[ctr2]][0])
            print(pos, believed_pos, ctr1_val, ctr2_val)
            bpos_rr[i] = believed_pos
            c1_rr[i] = ctr1_val
            f.write("%f %f %f %f\n" % (pos, believed_pos, ctr1_val, ctr2_val))
            plt.plot(bpos_rr, c1_rr)
    plt.plot(bpos_rr, c1_rr)
    plt.show()
    return
