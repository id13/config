print('load 3')

from collections import OrderedDict as ODict


def xcomp(x): umvr(umcenx, x, ustrx, -x)


def ycomp(y): umvr(umceny, y, ustry, -y)

def rotprob(dphi): mvr(usrotz, dphi);mvr(usrotz, -dphi)

def make_xyzmxy_motdc():
    dc = ODict(
        ustrx = ustrx,
        ustry = ustry,
        ustrz = ustrz,
        umcenx = umcenx,
        umceny = umceny,
    )
    print (dc)
    for mn, mot in dc.items():
        assert mot.name == mn
    return dc

def make_ggg():
    mdc = make_xyzmxy_motdc()
    ggg = GenStorepos(mdc, list(mdc.keys()),
            dict(
                none = [],
                all = 'ustrx ustry ustrz umcenx umceny'.split(),
                oxy = 'ustrx ustry ustrz'.split(),
                cenxy = 'umcenx umceny'.split()
            ))
    return ggg


def mymv(motobj, pos):
    motobj.move(pos)
     



class GenStorepos(object):

    POS_NAME_EXT = 'gjs'
    POS_FNAME_TPL = "{{}}.{}".format(POS_NAME_EXT)

    def __init__(self, motdc, allmotnames, subsets):
        self.motdc = motdc
        self.allmotnames = allmotnames
        self.subsets = subsets

    def make_fname(self, poskey):
        if poskey.endswith(self.POS_NAME_EXT):
            fname =  poskey
        else:
            fname = self.POS_FNAME_TPL.format(poskey)
        return fname

    def load_pos(self, poskey):
        fname = self.make_fname(poskey)
        with open(fname, 'r') as f:
            _posdc = json.load(f)
        posdc = ODict()
        for mn in self.allmotnames:
            posdc[mn] = float(_posdc[mn])
        return posdc

    def print_pos(self, posdoc):
        for mn, pos in posdc.items():
            print(f"    {mn:10} = {pos}")

    def show_curr_pos(self):
        print('current:')
        for mn, mot in self.motdc.items():
            print(f'    {mn:10} = {mot.position}')

    def show_pos(self, poskey):
        self.show_curr_pos()
        posdc = self.load_pos(poskey)
        print(f'{poskey}:')
        self.print_pos(posdc)

    def lspos(self):
        ll = os.listdir()
        ll.sort()
        for fname in ll:
            if fname.endswith(self.POS_NAME_EXT):
               print(fname)

    def save_pos(self, poskey, posdc):
        fname = self.make_fname(poskey)
        if path.exists(fname):
            raise IOError(f'illegal attempt to overwrite posfile {fname}')
        with open(fname, 'w') as f:
            json.dump(posdc, f)

    def gstp(self, poskey):
        posdc = ODict()
        for mn, mot in self.motdc.items():
            posdc[mn] = mot.position
        self.save_pos(poskey, posdc)

    def ggopos(self, poskey, subsetkey="none"):
        posdc = self.load_pos(poskey)
        subset = self.subsets[subsetkey]
        for mn in subset:

            mymv(self.motdc[mn], posdc[mn])
 
    def ginterpolate(self,pos1,pos2):
        
        pos1 =self.load_pos(pos1)
        pos2 =self.load_pos(pos2)
        
        for mn, mot in pos1.items():
            
            if mn== 'umcenx':
                umcenx_1 = mot

            elif mn== 'umceny':
                umceny_1 = mot

            elif 'ustrz' in mn :
                ustrz_1 = mot

            

        for mn, mot in pos2.items():
            if mn== 'umcenx':
                umcenx_2 = mot
            elif mn== 'umceny':
                umceny_2 = mot
            elif 'ustrz' in mn :
                ustrz_2 = mot
        
        for mn, mot in self.motdc.items():
            
            if 'ustrz' in mn :
                current_z = mot.position
                print(mn, current_z)
        
        
        ratio_z= (ustrz_1-ustrz_2)/(current_z-ustrz_1)
        
        new_pos_x = (umcenx_1-umcenx_2)/ratio_z+umcenx_1
        new_pos_y = (umceny_1-umceny_2)/ratio_z+umceny_1        
        
        print(f' x1 position {umcenx_1} and x2 position {umcenx_2}')
        print(f'Estimated new x position {new_pos_x} and y position {new_pos_y}')
        
        return new_pos_x, new_pos_y
        
    def move_to_new(self, pos1, pos2):
        
        new_pos_x, new_pos_y = self.ginterpolate(pos1,pos2)
        
        umv(umcenx,new_pos_x )
        umv(umceny,new_pos_y )
        return 
    
    def scans_along_z(self,start_pos, end_pos):
        
        
        pos1 =self.load_pos(start_pos)
        pos2 =self.load_pos(end_pos)
        
        for mn, mot in pos1.items():
            if 'ustrz' in mn :
                ustrz_1 = mot


        for mn, mot in pos2.items():
            if 'ustrz' in mn :
                ustrz_2 = mot
        
        height = abs(ustrz_2-ustrz_1)
        
        print(f'range = {height}')
        
        steps = int(height/0.01)
        umv(ustrz,max(ustrz_2,ustrz_1)+0.01)
        print(f'steps = {steps}')
        self.move_to_new(start_pos, end_pos)
        
        print(f'number of steps will be {steps}')
        umv(usrotz, 0)
        for st in range (steps):
            #umvr(ustrz, -0.01)
            umvr(ustrz, -0.03)
            umv(usrotz, 0) 
            self.move_to_new( start_pos, end_pos)
            for phi in range (-40,40,1):
                umv(usrotz, phi)  
                print('scanning')
                dkmapyz_2(-0.1, 0, 50, -0.01, 0.01, 2, 0.1, retveloc=5)
                #dscan(ustry,-0.07, 0.03, 50,0.1)
                
        
        return 
    
def scan_rot():
            
    for phi in range (-20,20,1):
        umv(usrotz, phi)  
        print('scanning')
        dkmapyz_2(-0.04, 0.04, 32, -0.05, 0.05, 20, 0.1, retveloc=5)
    
    return
    
def scan_rot_branch():
            
    for phi in range (-8,15,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.050, 0.05, 25, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return
        
            
def scan_rot_branch_fine():
            
    for phi in range (-10,12,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.06, 0.06, 40, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return


def scan_rot_main_shaft():
            
    for phi in range (-10,25,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.225, 0.225, 150, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return
    
    
    
def scan_rot_sec():
            
    for phi in range (-20,20,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.08, 0.08, 40, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return
    
def scan_rot_first():
            
    for phi in range (-10,20,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.06, 0.06, 40, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return


def scan_rot_main():
            
    for phi in range (-10,25,1):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.05, 0.05, 40, -0.11, 0.11, 44, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return


def scan_rot_half():
            
    for phi in range (-40,30,2):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.05, 0.05, 40, -0.35, 0.35, 14, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return

def scan_rot_half_top():
            
    for phi in range (-40,30,2):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.05, 0.05, 40, -0.5, 0.5, 20, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return


def scan_rot_full():
            
    for phi in range (-40,30,2):
        
        umvr(usrotz, phi)  
        print(phi/10)
        dkmapyz_2(-0.04, 0.04, 32, -0.85, 0.85, 85, 0.1, retveloc=5)
        umvr(usrotz, -phi) 
    
    return
