print(" - load 23")

UNDULATOR_CALIB = '''
u18     counts
6.48    126500-200
6.38    52300-200
6.28    9246-200
6.18    1430-200
'''

def linewise_kmap(ll1, ul1, n1, nlines2, stepsize2, expt):
    
    curr_nnp3 = nnp3.position
    print(f'linewise_kmap: curr_nnp3 = {curr_nnp3}')
    try:
        for i in range(nlines2):
            run_nnp3 = curr_nnp3 + i * stepsize2
            umv(nnp3, run_nnp3)
            print('#\n'*3)
            print(f'linewise_kmap: active_nnp3 = {run_nnp3}  cycle: {i} =====\n')
            sleep(2)
            kmap.dkmap(nnp2, ll1, ul1, n1, nnp3, 0, 0, 1, expt)
    finally:
        umv(nnp3, curr_nnp3)
        fshtrigger()

def safe_interrupt():
    print('4 sec left to interrupt with <CTRL><C> ...')
    sleep(4)
    print('interrupt time over - do not interrupt ...')
    sleep(1)

def serialgrid(nrows, ncols, row_step=0.25, col_step=0.25, skip=None):
    try:
        if None is skip:
            skip = []
        mgroieiger()
        so()
        sleep(2)
        curr_y = nny.position
        curr_z = nnz.position
        try:
            ctr = 0
            for i in range(nrows):
                run_nnz = curr_z + row_step*i
                umv(nnz, run_nnz)
                for j in range(ncols):
                    run_nny = curr_y + col_step*j
                    umv(nny, run_nny)
                    print('#\n'*5)
                    print('=============== cycle: i', i, 'j',j,'nny:', run_nny, 'nnz:', run_nnz,'\n')
                    if ctr in skip:
                        print('serialgrid: skipping entry:', ctr, '(no data will be recorded for this index)')
                    else:
                        linewise_kmap(0, 200, 400, 41, 5, 0.0014)
                    ctr += 1
                    safe_interrupt()
        finally:
            umv(nny, curr_y)
            umv(nnz, curr_z)
    finally:
        sc()
        sc()
        sc()

