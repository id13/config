from bliss.scanning.chain import ChainPreset

class Preset_ufsh(ChainPreset):
  def prepare(self,acq_chain):
    #print("... preset Preparing")
    pass
  def start(self,acq_chain):
    #print("... preset Starting, Opening the shutter")
    #ufsh.mode=1
    umux_fsh.set("p201")
  def stop(self,acq_chain):
    #print("... preset Stopped, closing the shutter")
    umux_fsh.set("closed")


def ufsh_status():
  info = ufsh.__info__()
  print(info)
  
def ufsh_open():
  ufsh.mode = ufsh.MANUAL
  ufsh.open()
  
def ufsh_close():
  ufsh.mode = ufsh.MANUAL
  ufsh.close()
  
def ufsh_trig():
  umux.switch("FSH_MODE","P201")                                                                                          
  umux.switch("FS_ENABLE","ENABLE")                                                                                       
  umux.switch("FSHUTTER_POL","NORMAL")
  ufsh.mode = ufsh.EXTERNAL
  
  
