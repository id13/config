print('ls3219 - load 9')


def scan_a():
    loff_kmap(nnp2, -30, 30, 30, nnp3, -30,30,30, 0.01)

def scan_b():
    loff_kmap(nnp2, -50, 50, 200, nnp3, -50,50,200, 0.05)

def scan_c():
    loff_kmap(nnp2, -50, 50, 200, nnp3, -50,50,200, 0.05)

def test_do_patchmap(ny, nz, dy, dz, scan_func):
    print('test:', ny, nz, dy, dz, scan_func)

def do_patchmap(ny, nz, dy, dz, scan_func):
    nny_start = nny.position
    nnz_start = nnz.position
    try:
        for i in range(nz):
            nnz_curr = nnz_start + i*dz
            mv(nnz, nnz_curr)
            for j in range(ny):
                print(f'==== {i} {j}')
                nny_curr = nny_start + j*dy
                mv(nny, nny_curr)
                scan_func()
    finally:
        mv(nny, nny_start)
        mv(nnz, nnz_start)

# new sample 



def admin_goto(posname, start_key, det_pos):
    assert posname.endswith('.json')
    rstp(posname)
    ds_name = f'{start_key}_{det_pos}_{posname[13:-5]}'
    print(f'position: {posname}')
    print(f'ds_name: {ds_name}')
    newdataset(ds_name)

def bliss_mount3_meas001():
    start_key = 'a'

    #
    # close
    #
    
    det_pos = 'close'
    mv(ndetx, -240.0)

# A MG
    admin_goto('bliss_mount3_A_p1_0009.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('bliss_mount3_A_p2_0010.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

# B
    admin_goto('bliss_mount3_B_p5_0004.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('bliss_mount3_B_p6_0006.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)


    #
    # far
    #
    det_pos = 'far'
    mv(ndetx, -40.0)
# A
    admin_goto('bliss_mount3_A_p3_0011.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('bliss_mount3_A_p4_0012.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

# B
    admin_goto('bliss_mount3_B_p7_0005.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)



def bliss_mount4_meas001():
    start_key = 'b'

    #
    # close
    #
    
    det_pos = 'close'
    mv(ndetx, -240.0)

    # A SaOS2_CTRL
    
    #admin_goto('SaoS_4_A_p1_0017.json', start_key, det_pos)
    #do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_A_p2_0018.json', start_key, det_pos)
    do_patchmap(2,2, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_A_p3_0019.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)
    
    #
    # far
    #
    det_pos = 'far'
    mv(ndetx, -40.0)

    admin_goto('SaoS_4_A_p4_0020.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_A_p3_0019.json', start_key, det_pos)
    do_patchmap(1,1, 0.11, 0.11, scan_b)



def bliss_mount4_meas002():
    start_key = 'a'

    #
    # close
    #
    
    det_pos = 'close'
    mv(ndetx, -240.0)

    # B SaOS2_TREATED
    
    #admin_goto('SaoS_4_A_p1_0017.json', start_key, det_pos)
    #do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_B_p1_0021.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_B_p2_0022.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)
    
    #
    # far
    #
    det_pos = 'far'
    mv(ndetx, -40.0)

    admin_goto('SaoS_4_B_p3_0023.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)

    admin_goto('SaoS_4_B_p4_0024.json', start_key, det_pos)
    do_patchmap(3,1, 0.11, 0.11, scan_b)




 # far
#
#   det_pos = 'far'
#    mv(ndetx, -40.0)#
#
#    admin_goto('bliss_mount3_B_p8_0007.json', start_key, det_pos)
#    do_patchmap(3, 1, 0.11, 0.11, scan_c)
#
#    admin_goto('bliss_mount3_B_p9_0008.json', start_key, det_pos)
#    do_patchmap(3, 1, 0.11, 0.11, scan_c)
#
#    admin_goto('bliss_mount3_A_p5_0013.json', start_key, det_pos)
#    do_patchmap(3, 1, 0.11, 0.11, scan_c)
#
#    admin_goto('bliss_mount3_A_p6_0014.json', start_key, det_pos)
#    do_patchmap(3, 1, 0.11, 0.11, scan_c)
