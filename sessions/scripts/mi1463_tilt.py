print('mi1463_tilt - load 2')
from math import fabs

EPS = 0.02
def isnear(x,y):
    return fabs(x-y) < EPS

class NnvTiltMgr(object):

    def __init__(self, xoffset, xpitch):
        self.xoffset = xoffset
        self.xpitch = xpitch

    def get_vnnxpos(self, nnvpos):
        return self.xoffset - nnvpos*self.xpitch

    def check_nnvpos(self):
        nnvpos = nnv.position
        vnnxpos = self.get_vnnxpos(nnvpos)
        if not isnear(vnnxpos, nnx.position):
            raise RuntimeError(f'inconsistency - nnv {nnv.position}, nnx {nnx.position}')

    def nnv_mv(self, nnvpos):
        print(f'nnv_mv: target_positon = {nnvpos}')
        curr_nnvpos = nnv.position
        if fabs(nnvpos - curr_nnvpos) > 0.5:
            raise RuntimeError(f'too far displacement of nnv: {nnvpos - curr_nnvpos}')
        self.check_nnvpos()
        nnxpos = self.get_vnnxpos(nnvpos)
        mv(nnv, nnvpos)
        mv(nnx, nnxpos)
