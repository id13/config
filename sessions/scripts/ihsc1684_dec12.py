print('load script ihsc1684')
# so()
#sct(0.01)
#load_script('ch6158_1st_night')
#load_script('ihsc1684_tolman')
#bch2 = setitup()
#load_script('ihsc1684_dec12')
#ch615819_night('name',starting_angle)
#ucorrsrotz = make_corrsrotz(bch2)

prefix = 'd'


#def set_bch_class():
    #load_script('ch6158_tolman')
#bch2 = setitup()
#ucorrsrotz = make_corrsrotz(bch2)
    
def set_lm_sample_running():
    set_lm(ustrx,-61.000,-59.347)
    set_lm(ustry,-10.000,-13.410)
    set_lm(usrotz,29.5,135.5)

def set_lm_sample_change():
    sc()
    umv(udetx,-64)
    umvr(ubsx,10)
    umv(ubsy,-100)
    umv(usrotz,90)
    set_lm(ustrx,-61.000,30.000)
    #set_lm(ustry,-10.000,-13.410)
    #set_lm(usrotz,29.5,135.5)

class CorrectedUsrotz(object):

    def __init__(self, bch):
        self.bch = bch

    @property
    def cr_position(self):
        return usrotz.position

    @cr_position.setter
    def cr_position(self, pos):
        mv(usrotz, pos)
        self.bch.correct(pos, ask=False)

def make_corrsrotz(bch):
    cr = CorrectedUsrotz(bch)
    sftax = SoftAxis('corrsrotz', cr, position='cr_position', move='cr_position', tolerance=0.02)
    return sftax


def go_rot_cent(deg):
    umv(usrotz,deg)
    if np.abs(usrotz.position-deg)<0.1:
        bch2.correct(deg,ask=False)
    else:
        raise RuntimeError(f'user interrupt, want to {deg}, go to {usrotz.position}')

def angular_raster_scan(deg,ll1,ul1,nitv1,ll2,ul2,nitv2,exp_t,retveloc=5):
    try:
        f = open('stopit')
        enddataset()
        return 'stopit'
    except:
        pass
    
    go_rot_cent(deg)
    dkmapyz_2(ll1,ul1,nitv1,ll2,ul2,nitv2,exp_t,retveloc=retveloc)
    
def tomo_scan(sample_name,start_ang,z_pos):
    mgeig()
    fshtrigger()
    so()
    ll1 = -0.2
    ul1 =  0.2
    nitv1 = int(0.4/0.005)
    ll2 = -0.2
    ul2 =  0.2
    nitv2 = int(0.4/0.005)
    exp_t = 0.03

    once_flg = True
    try:
        ang1 = np.arange(start_ang,180.5,1.5)
        print(f'ang1 = {ang1}\n')
        #ans = yesno('okay? continue?')
        #if not ans:
        #    raise RuntimeError('user interrupt')
        # dataset inlucde 50 to 135 degree with step size of 1 degree
        newdataset(f'{sample_name}_{prefix}')
        for deg in ang1:
            umv(ustrz,z_pos)
            print(f'ang1 series: thets = {deg}')
            #if once_flg:
            #    ans = yesno('okay? continue?')
            #    if not ans:
            #        raise RuntimeError('user interrupt')
            #    once_flg = False
            # fly scan for each angle   
            #go_rot_cent(deg) 
            angular_raster_scan(deg,ll1,ul1,nitv1,
                                    ll2,ul2,nitv2,exp_t)
        enddataset()
            
        sc()
    finally:
        sc()
        sc()


def tomo_run():
    tomo_scan('tomo_WAXS',0,1.57)
    umv(udetx,-65)
    tomo_scan('tomo_SAXS',0,1.78)
