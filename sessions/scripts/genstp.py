print('genstp: load 1')
import os
from os import path
from collections import OrderedDict as ODict
import json
# json.dump(dc, f)
# json.load(f)

# define interface
class Virtual(Exception): pass


EXAMPLE = '''
ALLMOTNAMES = "ustrx ustry ustrz umcenx umceny".split()
SUBSETS = Odict(
    none = [],
    all = ALLMOTNAMES,
    xyz = "ustrx ustry ustrz".split(),
    xy  = "ustrx ustry".split(),
    mxy = "mcenx umceny",
)
mystorepos = GenStorepos(
    motdc, ALLMOTNAMES, SUBSETS
)
def make_xyzmxy_motdc():
    dc = ODict(
        ustrx = ustrx,
        ustry = ustry,
        ustrz = ustrz,
        umcenx = umcenx,
        umceny = umceny,
    )
    print (dc)
    for mn, mot in dc.items():
        assert mot.name == mn
    return dc

def make_xyzmxy_subsets():
    dc = ODict(
        none = [],
        all = 'ustrx ustry ustrz umcenx umceny'.split(),
        mxy = 'umcenx umceny'.split(),
        xyz = 'ustrx ustry ustrz'.split(),
        yz = 'ustry ustrz'.split(),
        xy = 'ustrx ustry'.split(),
    )
    return dc

def make_xyzmxy_allmotnames():
    return 'ustrx ustry ustrz umcenx umceny'.split()

xyzmxy_allmotnames = make_xyzmxy_allmotnames()
xyzmxy_motdc = make_xyzmxy_motdc()
xyzmxy_subsets = make_xyzmxy_subsets()
xymxy = GenStorepos(xyzmxy_motdc, xyzmxy_allmotnames, xyzmxy_subsets)





mystorepos.ggopos('bla', 'xy')
dmm = make_dummies("a b c".split())
mymv(dmm['a'], 42.0)
mymv(dmm['b'], 123.0)
mymv(dmm['c'], 12.02)
mystorepos = GenStorepos(dmm, 'a b c'.split(), dict(xy = "a b".split(), all="a b c".split(), none=[]))
'''
class DummyMotObj(object):

    def __init__(self, name, pos=0.0):
        self.name = name
        self.position = 0.0

    def move(self, pos):
        self.position = pos

    def setpos(self, pos):
        self.position = pos

def make_dummies(motnamelist):
    dc = ODict()
    for mn in motnamelist:
        dc[mn] = DummyMotObj(mn)
    return dc

def mymv(motobj, pos):
    motobj.move(pos)

def mymvr(motobj, dist):
    pos = motonbj.position
    newpos = pos + dist
    motobj.mv(newpos)

class GenStorepos(object):

    POS_NAME_EXT = 'gjs'
    POS_FNAME_TPL = "{{}}.{}".format(POS_NAME_EXT)

    def __init__(self, motdc, allmotnames, subsets):
        self.motdc = motdc
        self.allmotnames = allmotnames
        self.subsets = subsets

    def make_fname(self, poskey):
        if poskey.endswith(self.POS_NAME_EXT):
            fname =  poskey
        else:
            fname = self.POS_FNAME_TPL.format(poskey)
        return fname

    def load_pos(self, poskey):
        fname = self.make_fname(poskey)
        with open(fname, 'r') as f:
            _posdc = json.load(f)
        posdc = ODict()
        for mn in self.allmotnames:
            posdc[mn] = float(_posdc[mn])
        return posdc

    def print_pos(self, posdc):
        for mn, pos in posdc.items():
            print(f"    {mn:10} = {pos}")
            
    def show_curr_pos(self):
        print('current:')
        for mn, mot in self.motdc.items():
            print(f'    {mn:10} = {mot.position}')

    def show_pos(self, poskey):
        self.show_curr_pos()
        posdc = self.load_pos(poskey)
        print(f'{poskey}:')
        self.print_pos(posdc)

    def lspos(self):
        ll = os.listdir()
        ll.sort()
        for fname in ll:
            if fname.endswith(self.POS_NAME_EXT):
               print(fname)

    def save_pos(self, poskey, posdc):
        fname = self.make_fname(poskey)
        if path.exists(fname):
            raise IOError(f'illegal attempt to overwritr posfile {fname}')
        with open(fname, 'w') as f:
            json.dump(posdc, f)

    def gstp(self, poskey):
        posdc = ODict()
        for mn, mot in self.motdc.items():
            posdc[mn] = mot.position
        self.save_pos(poskey, posdc)

    def ggopos(self, poskey, subsetkey="none"):
        posdc = self.load_pos(poskey)
        subset = self.subsets[subsetkey]
        for mn in subset:
            mymv(self.motdc[mn], posdc[mn])

def make_xyzmxy_motdc():
    dc = ODict(
        ustrx = ustrx,
        ustry = ustry,
        ustrz = ustrz,
        umcenx = umcenx,
        umceny = umceny,
    )
    for mn, mot in dc.items():
        assert mot.name == mn
    return dc

def make_xyzmxy_subsets():
    dc = ODict(
        none = [],
        all = 'ustrx ustry ustrz umcenx umceny'.split(),
        mxy = 'umcenx umceny'.split(),
        xyz = 'ustrx ustry ustrz'.split(),
        yz = 'ustry ustrz'.split(),
        xy = 'ustrx ustry'.split(),
    )
    return dc

def make_xyzmxy_allmotnames():
    return 'ustrx ustry ustrz umcenx umceny'.split()

