print('sc5471_night2 - load 5')

NIGHT2_TODO = '''#
# posname width_mu npoints special_flag
SO4_1 100 200 1
SO2_1 100 200 1
BO2_1 100 200 1                                                                                                                                                        
SO3_1 100 200 1 
BO1_1 150 300 1 
Single_Crystal_Mat_2 100 200 1
Single_Crystal_Mat_1 100 200 1 
ainur_8_1_1 100 200 1
ainur_8_2_1 100 200 1
Single_Crystal_Mat_3 100 200 1                                                                                                                                                    
PBTTT_011_1 150 300 1                                                                                                                                                        
PBTTT_08_1 100 200 1                                                                                                                                                                                                                                                                                                                
PBTTT_05_1 100 200 1
air 20 40 1
kapton_scotch_1 20 40 1                                                                                                                                                                                                                                                                                                            
#'''



# nnp2 was originally at 115
# moved it to regular position 126
# correction in nny is therefore 0.006-0.01 instead of just 0.006
#
START_KEY = 'b'

SC5471_SIM = False

def xxx_kmap(*p):
    if SC5471_SIM:
        print(f'SIM: loff_kmap {str(p)}')
    else:
        print(f'REAL: loff_kmap {str(p)}')
        loff_kmap(*p)

def xxx_newdataset(dsname):
    if SC5471_SIM:
        print(f'SIM: newdataset: {dsname}')
    else:
        print(f'REAL newdataset: {dsname}')
        newdataset(dsname)

def doo_setup(posname):
    rstp(posname)
    if SC5471_SIM:
        sleep(1.5)
    #mvr(nnz, 0.0115)
    #mvr(nny, 0.006-0.01)
    mvr(nnz, 0.0115)
    mvr(nny, 0.006)

    if SC5471_SIM:
        sleep(1.5)
    dsname = f'{START_KEY}_{posname}'
    xxx_newdataset(dsname)

def doo_scan(hw, nstp, flg=1):
    #mvr(nny, 0.001*hw)
    #mvr(nnz, 0.001*hw)
    xxx_kmap(nnp2, -hw, hw, nstp, nnp3, -hw, hw, nstp, 0.05)
    if flg == 2:
        print('FLG is active! doing second scan ...')
        mvr(nny, 0.001*2*hw)
        xxx_kmap(nnp2, -hw, hw, nstp, nnp3, -hw, hw, nstp, 0.05)

def parse_line(l):
    l = l.strip()
    if not l:
        raise ValueError()
    if not l.startswith('#'):
        print(f'line: {l}')
        w = l.split()
        posname = w[0]
        hw = 0.5*float(w[1])
        nstp = int(w[2])
        flg = int(w[3])
        return (posname, hw, nstp, flg)
    else:
        return None
    

def sc5471_night2():
    ll = NIGHT2_TODO.split('\n')
    for l in ll:
        res = parse_line(l)
        if None is res:
            continue
        else:
            posname, hw, nstp, flg = res
            print(f'==========================================:  {posname}')
            print(f'data = {(hw, nstp, flg)}')
            doo_setup(posname)
            doo_scan(hw, nstp, flg=flg)

def sc5471_main():
    try:
        so()
        sleep(3)
        sc5471_night2()
    finally:
        sc()
        sc()
        sc()
