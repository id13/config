print('asdf')
def lunch_scan():
    so()
    newdataset('lunch_roi2')
    umv(nnx,2.970,nny,-2.320, nnz, -0.233)
    umv(Theta,-6)
    for ii in range(-5,5,1):
        print(ii)
        umv(Theta,ii)
        loff_kmap(nnp2,-5,5,100,nnp3,-5,5,100,0.02)
    newdataset('lunch_roi3')
    umv(nnx,2.970,nny,-2.295,nnz,-0.222)
    umv(Theta,-6)
    for ii in range (-5,5,1):
        print(ii)
        umv(Theta,ii)
        loff_kmap(nnp2,-5,5,100,nnp3,-5,5,100,0.02)
    sc()


def afternoon_scan():
    so()
    newdataset('roi4_rock')
    umv(Theta,-4)
    ang_list = np.linspace(-4,-2,41)
    for ii in range(0,len(ang_list)):
        curr_ang = ang_list[ii]
        umv(Theta,curr_ang)
        loff_kmap(nnp2,-3,3,120,nnp3,-3,3,120,0.01)


def night_scan():
    so()
    newdataset('roi3_rock')
    rstp('roi3._pos')
    umv(Theta,-3)
    ang_list = np.linspace(-3,3,61)
    for ii in range(0,len(ang_list)):
        curr_ang = ang_list[ii]
        umv(Theta,curr_ang)
        loff_kmap(nnp2,-5,5,200,nnp3,-5,5,200,0.003)
        
    newdataset('roi2_rock')
    rstp('roi2._pos')
    umv(Theta,-3)
    ang_list = np.linspace(-3,3,61)
    for ii in range(0,len(ang_list)):
        curr_ang = ang_list[ii]
        umv(Theta,curr_ang)
        loff_kmap(nnp2,-15,15,300,nnp3,-15,15,300,0.003)
        newdataset('roi2_rock')
    rstp('roi1._pos')
    umv(Theta,-3)
    ang_list = np.linspace(-3,3,61)
    for ii in range(0,len(ang_list)):
        curr_ang = ang_list[ii]
        umv(Theta,curr_ang)
        loff_kmap(nnp2,-15,15,300,nnp3,-15,15,300,0.003)
    sc()
    sc()
