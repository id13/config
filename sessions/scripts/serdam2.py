print('serdam - load 5')
import sys
from os import path
import math
import time
import gevent
from time import time as tim
import json

from scipy.interpolate import interp1d

def write_json(fxpth, dc, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'w') as f:
        json.dump(dc, f)

def read_json(fxpth, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'r') as f:
        dc = json.load(f)
    return dc

def flintp(ax, ay, bx, by, x):
    dx = bx - ax
    dy = by - ay
    m = dx/dy
    y = m*(x - ax) + ay
    return y

class Menv(object):

    ALLOWED = ''.split()
    ROLES = dict()

    def __init__(self):
        self.motdc = motdc = dict()
        roles = self. ROLES
        for rn, mn in roles.items():
            mot = config.get(roles[rn])
            assert mot.name in self.ALLOWED
            motdc[rn] = mot
            print(f'role name: {rn}   tobe_motname = {mn} motname: {motdc[rn].name}')

    def get_posdc(self):
        posdc = dict()
        motdc = self.motdc
        for rn in motdc:
            posdc[rn] = motdc[rn].position
        return posdc

    def qmv(self, rn, tp):
        print(f'{(rn, tp)=}')
        mv(self.motdc[rn], tp)
        
    def qmvr(self, rn, tdist):
        mvr(self.motdc[rn], tdist)

    def stp(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        assert not path.exists(fnp)
        alldc = dict(
            allowed = self.ALLOWED,
            roles = self.ROLES,
            posdc = self.get_posdc()
        )
        write_json(fnp, alldc)

    def fnx_to_fnpath(self, fnx):
        assert not '/' in fnx
        if not fnx.endswith('.qj'):
            assert not '.' in fnx
            return f'{fnx}.qj'
        else:
            return fnx
        raise ValueError()

    def fnx_to_sfn(self, fnx):
        assert not '/' in fnx
        if fnx.endswith('.qj'):
            return fnx[:-3]
        else:
            assert not '.' in fnx
            return fnx

    def read_alldc(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        return read_json(fnp)

    def get_current_posdc(self):
        posdc = dict()
        for k, mot in self.motdc.items():
            posdc[k] = mot.position
        return posdc

    def rstp(self, fnx):
        fnp = self.fnx_to_fnpath(fnx)
        alldc = self.read_alldc(fnp)
        assert self.ALLOWED == alldc['allowed']
        assert self.ROLES == alldc['roles']
        posdc = alldc['posdc']
        self.move_to_posdc(posdc)

    def move_to_posdc(self, posdc):
        for k,v in posdc.items():
            self.qmv(k,v)

    def to_posdc_str(self, posdc):
        for k,v in posdc.items():
            print(f'{k:10} = {v:12.5f}')

    def show_pos(self, obj=None, title='pos:'):
        if None is obj:
            posdc = self.get_current_posdc()
        elif isinstance(obj, str):
            posdc = self.read_alldc(obj)['posdc']
        else:
            posdc = obj
            obj.setdefault
        print(title)
        print(self.to_posdc_str(posdc))
        
        

class StrMenv(Menv):

    ALLOWED = 'ustrx ustry ustrz'.split()
    ROLES = dict(
        x = 'ustrx',
        y = 'ustry',
        z = 'ustrz',
    )



class Trajectory1(object):

    ROLES_LL = ''.split()

    def __init__(self, menv, master_rn, slave_rnll):
        self.master_rn = master_rn
        self.slave_rnll = slave_rnll
        self._refdc = dict(A=None, B=None)
        self.menv = menv
        self.traj = (None,None)
        assert set(menv.ROLES.keys()).issubset(set(self.ROLES_LL))

    def set_ref(self, refk, fnx):
        assert refk in('A', 'B')
        self.menv.stp(fnx)
        self._refdc[refk] = fnx

    def setup_trajectory(self):
        #def flintp(ax, ay, bx, by, x):
        fnx_a = self._refdc['A']
        aposdc = self.menv.read_alldc(fnx_a)['posdc']
        fnx_b = self._refdc['B']
        bposdc = self.menv.read_alldc(fnx_b)['posdc']
        self.traj = (aposdc, bposdc)

    def traj_mv(self, rn, tp):
        assert(rn == self.master_rn)
        (aposdc, bposdc) = self.traj
        ax = aposdc[self.master_rn]
        bx = bposdc[self.master_rn]
        t_posdc = dict()
        for srn in self.slave_rnll:
            ay = aposdc[srn]
            by = bposdc[srn]
            _interp = interp1d((ax,bx),(ay,by), kind='linear')
            #srn_tp = flintp(ax, ay, bx, by, tp)
            srn_tp = float(_interp(tp))
            t_posdc[srn] = srn_tp
        self.menv.show_pos(obj=t_posdc, title='target pos:')
        self.menv.show_pos(obj=None, title='current pos (before):')
        self.menv.move_to_posdc(t_posdc)
        self.menv.show_pos(obj=None, title='current pos (after):')

class Trajectory1_Xyz(Trajectory1):

    ROLES_LL = 'x y z'.split()


class FastLoop(object):

    def __init__(self, nframes, expt, nfpf=None):
        self.nframes = nframes
        self.expt = expt
        eiger = config.get('eiger')
        if None is nfpf:
            nfpf = eiger.saving.frames_per_file
        assert(nfpf % nfpf == 0)
        self.nfpf = nfpf
        self.ueiger = config.get('ueiger')

    def run(self):
        eiger.saving.frames_per_file = self.nfpf
        self.ueiger.ttl_software_scan_hws(
            self.nframes, self.expt)

def xxx_test(i):
    xyz = StrMenv()
    traj = Trajectory1_Xyz(xyz, 'x', 'y z'.split())
    xyz.rstp('a4')
    traj.set_ref('A', f'a{i}')
    xyz.rstp('b4')
    traj.set_ref('B', f'b{i}')
    traj.setup_trajectory()
    return xyz, traj

VOID_S = 'VOID____VOID'

class SerDam(object):

    LOG_KEYS = 'expt expwin settlewin deltay npoints latency deltaz nlines'.split()
    LOG_KEYS.extend('dsname operation_key'.split())
    LOG_KEYS.extend('log_time log_asc_time'.split())
    LOG_KEYS.extend('start_time start_asc_time'.split())
    LOG_KEYS.extend('stop_time stop_asc_time'.split())
    LOG_KEYS.extend('fly_ll1 fly_ul1 fly_nitv1 fly_ll2 fly_ul2 fly_nitv2'.split())
    LOG_KEYS.extend('fly_expt fly_frames_per_file fly_retveloc'.split())
    LOG_KEYS.extend('stat_u18 stat_ct22 stat_ct24 stat_pvg stat_phg stat_energy'.split())
    LOG_KEYS.extend('stat_udetx stat_transfoc stat_ustrx stat_ustry stat_ustrz'.split())
    LOG_KEYS.extend('stat_diode_gain stat_ion_gain'.split())
    LOG_KEYS.extend('stat_eigerenergy stat_flyspeed'.split())


    def __init__(self, operation_key, dsname):
        self.version = '1.2.0'
        for k in self.LOG_KEYS:
            setattr(self, k, VOID_S)
        self.dsname = dsname
        self.operation_key = operation_key
        self.menv = StrMenv()

    #############################################################
    # FLY_MESH

    def setup_flymesh(self, ll1, ul1, nitv1, ll2, ul2, nitv2, exptime, frames_per_file=2000, retveloc=5.0):
        assert self.operation_key == 'FLY'
        self.fly_ll1 = ll1
        self.fly_ul1 = ul1
        self.fly_nitv1 = nitv1
        self.fly_ll2 = ll2
        self.fly_ul2 = ul2
        self.fly_nitv2 = nitv2
        self.fly_expt = exptime
        self.fly_frames_per_file = frames_per_file
        self.fly_retveloc = retveloc

    def run_flymesh(self):
        self.start_log()
        try:
            assert self.operation_key == 'FLY'
            fshtrigger()
            dkmapyz_2(self.fly_ll1, self.fly_ul1, self.fly_nitv1, self.fly_ll2, self.fly_ul2, self.fly_nitv2,
                        self.fly_expt, retveloc=self.fly_retveloc)
        finally:
            self.finish_log()
        
    def get_fly_speed(self):
        dll = math.fabs(self.fly_ul1-self.fly_ll1)
        return dll/(self.fly_expt*self.fly_nitv1)

    def setup_fly_speed(self, fly_speed, mode='nitv'):
        dll = math.fabs(self.fly_ul1-self.fly_ll1)
        if mode == 'nitv':
            nitv_new = int(round(dll/(self.fly_expt*fly_speed)))
            self.fly_nitv1 = nitv_new
            self.setup_fly_speed(fly_speed, mode='yrange')
        elif 'yrange' == mode:
            self.fly_ul1 = self.fly_ll1 + fly_speed*self.fly_nitv1*self.fly_expt


    #############################################################
    # MULTI_LOOP

    def setup_multiinner(self, expt, expwin, settlewin, deltay, npoints, latency=3.0):
        assert self.operation_key == 'MULTI'
        self.expt = expt
        self.expwin = expwin
        self.settlewin = settlewin
        self.deltay = deltay
        self.npoints = npoints
        self.latency = latency

    def setup_fastloop(self, innerwin, deltaz, nlines):
        assert self.operation_key == 'MULTI'
        self.innerwin = innerwin
        self.deltaz = deltaz
        self.nlines = nlines
        _nframes = int(innerwin/self.expt) + 100
        nfpf = self.nfpf = _nframes//100
        nframes = self.nframes = 100*nfpf
        self.fl = FastLoop(nframes, self.expt, nfpf=nfpf)

    def run_multiloop(self):
        self.start_log()
        try:
            assert self.operation_key == 'MULTI'
            deltaz = self.deltaz
            nlines = self.nlines
            ini_zpos = ustrz.position
            for i in range(nlines):
                zpos = i*deltaz + ini_zpos
                print(f'cycle: {i=}   {zpos=}')
                mv(ustrz, zpos)
                self.run_outer_section_2()
        finally:
            self.finish_log()

    def run_fastloop(self):
        self.fl.run()

    def upd(self):
        print(f'{bool(self.outer)=}  {bool(self.inner)=}')

    def run_inner_section_2(self):
        assert self.operation_key == 'MULTI'
        outer = self.outer
        ini_ypos = ustry.position
        try:
            deltay = self.deltay
            settlewin = self.settlewin
            expwin = self.expwin
            for i in range(self.npoints):
                print(f'{i=}')
                if not outer:
                    self.final_i = i
                    return i
                mv(ustry, ini_ypos + i*deltay)
                sleep(settlewin)
                fshopen()
                sleep(expwin)
                fshclose()
        finally:
            mv(ustry, ini_ypos)

    def run_outer_section_2(self):
        assert self.operation_key == 'MULTI'
        self.outer = gevent.spawn(self.run_fastloop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section_2)
        cont = 2
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    #############################################################
    # INFRA


    # timing ####

    def timethis(self, m):
        t = tim()
        m()
        return tim()-t

    def get_tmpair(self):
        t = time.time()
        at = time.asctime(time.localtime())
        return t, at

    # generic acq ####

    def diag_count(self, countpos=None):
        # ...
        ini_posdc = self.menv.get_posdc()
        ct22 = -100.0
        ct24 = -100.0
        return ct22, ct24
        try:
            if not None is countpos:
                rstp(countpos)
            sc = ct(1.0)
            ct22 = sc.get_data()['p201_eh2_0:ct2_counters_controller:ct22'][0]
            ct24 = sc.get_data()['p201_eh2_0:ct2_counters_controller:ct24'][0]
        finally:
            self.menv.move_to_posdc(ini_posdc)
        return ct22, ct24

    def diag_count_netsig(self):
        diode_raw, ion_raw = self.diag_count()
        diode_sig = self.get_diode_netsig(diode_raw)
        ion_sig = self.get_ion_netsig(diode_raw)
        return diode_sig, ion_sig

    def gather_static(self):
        self.stat_time, self.stat_asc_time = self.get_tmpair()
        self.stat_tm = time.time()
        self.stat_u18 = u18.position
        self.stat_udetx = udetx.position
        self.stat_ustrx = ustrx.position
        self.stat_ustry = ustry.position
        self.stat_ustrz = ustrz.position
        self.stat_transfoc = transfocator1.status_read()
        self.stat_ct22, self.stat_ct24 = self.diag_count()
        self.stat_pvg = pvg.position
        self.stat_phg = pvg.position
        self.stat_eigerenergy = eiger.camera.photon_energy
        self.stat_energy = ccmo.get_energy()
        try:
            self.stat_diode_gain = self.read_moco('outbeam')
        except:
            self.stat_diode_gain = -100.0
        try:
            self.stat_ion_gain = self.read_moco('inbeam')
        except:
            self.stat_ion_gain = -100.0
        if 'FLY' == self.operation_key:
            self.stat_flyspeed = self.get_fly_speed()

    def read_moco(self, beam):
        _beam = f'?{beam}'
        res = umoco.comm(_beam)
        (nix0, nix1, nix2, val_s, nix4) = res.split()
        val = float(val_s)
        return val

    def get_diode_netsig(self, raw):
        return raw-100.0

    def get_ion_netsig(self, raw):
        return raw-19404.0

    def goto_flux_fraction(self, ref_ion_netsig, frac):
        # 15 keV diode gain 1e-3 ion gain 1e-7
        # diode 896583 is ion 330040
        # dark diode is 100.0 dark ion is 19404
        pass
        



    # logging ####

    def start_log(self):
        opkey = self.operation_key
        dsname = self.dsname
        stp(f'SERDAM_START_POS_{dsname}')
        newdataset(dsname)
        self.operation_key = opkey
        tmp = self.get_tmpair()
        self.start_time, self.start_asc_time = tmp
        self.stop_time = -1
        self.stop_asc_time = -1
        self.log_time, self.log_asc_time = tmp
        self.gather_static()
        logdc = self.make_logdc()
        self.save_log(logdc, f'START_{opkey}', dsname)

    def finish_log(self):
        stp(f'SERDAM_END_POS_{self.dsname}')
        tmp = self.get_tmpair()
        self.stop_time, self.stop_asc_time = tmp
        self.log_time, self.log_asc_time = tmp
        self.gather_static()
        logdc = self.make_logdc()
        self.save_log(logdc, f'FIN_{self.operation_key}', self.dsname)

    def make_logdc(self):
        logdc = dict()
        for k in self.LOG_KEYS:
            logdc[k] = getattr(self, k)
        return logdc

    def save_log(self, logdc, key, logname):
        save_fupth = f'SERDAM_LOG_{key}_{logname}.serlog'
        write_json(save_fupth, logdc)

    def load_log(self, key, logname):
        load_fupth = f'SERDAM_LOG_{key}_{logname}.serlog'
        return read_json(load_fupth)

    def get_log_str(self, logdc):
        ll = [f'==== SerDam object [version = {self.version}]']
        for k in self.LOG_KEYS:
            ll.append(f'{k:12s} = {logdc[k]}')
        return '\n'.join(ll)

    def show_log(self, logdc=None):
        if None is logdc:
            logdc = self.make_logdc()
        log_s = self.get_log_str(logdc)
        print(log_s)

    #############################################################
    # OLD

    def setup_slow_loop(self, nframes=None):
        self.slow_nframes = nframes

    def run_slow_loop(self):
        npts = int(0.5*self.slow_nframes)
        loopscan(npts, self.expt)

    def slow_timing(self):
        exptll = []
        tmll = []
        self.setup_slow_loop(200)
        for i in range(20):
            self.expt = 0.0005*(i+1)
            exptll.append(self.expt)
            tmll.append(self.timethis(self.run_slow_loop))
        print(exptll)
        print(tmll)
        plot(tmll)

    def run_outer_section(self):
        self.outer = gevent.spawn(self.run_fastloop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section)
        cont = 5
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    def run_outer_section_3(self):
        self.outer = gevent.spawn(self.run_slow_loop)
        sleep(self.latency)
        self.inner = gevent.spawn(self.run_inner_section_2)
        cont = 2
        while cont:
            try:
                state = bool(self.inner) or bool(self.outer)
                if not state:
                    cont -= 1
                self.upd()
                sleep(0.5)
            except KeyboardInterrupt:
                break

    def run_inner_section(self):
        ini_ypos = ustry.position
        try:
            deltay = self.deltay
            settlewin = self.settlewin
            expwin = self.expwin
            for i in range(self.npoints):
                mv(ustry, ini_ypos + i*deltay)
                sleep(settlewin)
                fshopen()
                sleep(expwin)
                fshclose()
        finally:
            mv(ustry, ini_ypos)

    # fshopen fshclose : 0.1195 s
    # >> mv stry to delta 0.01: 0.0605

