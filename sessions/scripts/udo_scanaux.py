import sys
import h5py
from os import path
import numpy as np

# utils
#

def compute_cog_1d(xrr, arr):
    momrr = xrr*arr
    momsum = momrr.sum()
    arrsum = arr.sum()
    cog = momsum/arrsum
    return cog

def address_to_meshcoords(address, scanwidth):
    pm1,pm0 = divmod(address, scanwidth)
    return pm0,pm1

def meshcoords_to_address(meshcoords, scanwidth):
    pm0,pm1 = meshcoords
    address = pm1*scanwidth + pm0
    return address

def address_to_pixcoords(address, scanwidth):
    p0, p1 = divmod(address, scanwidth)
    return p0, p1

def pixcoords_to_address(pixcoords, scanwidth):
    p0,p1 = pixcoords
    address = p0*scanwidth + p1
    return address

def pix_to_motors_contscan(pixcoords, shape, limits):
    # coords: hor first, vert second
    # according to mesh syntax
    stp0, stp1 = pix_to_scanresol(shape, limits)
    (pm1,pm0) = pix
    lm0, lm1 = limits
    mpos0 = (pm0+0.5)*stp0 + lm0[0]
    mpos1 = (pm1+0.5)*stp1 + lm1[0]
    return mpos0, mpos1, stp0, stp1

def pix_to_scanresol(shape, limits):
    # mesh coords: hor first, vert second
    # according to mesh syntax
    (sm1,sm0) = shape
    meshp = (pm0, pm1)
    lm0, lm1 = limits
    dm0 = lm0[1] - lm0[0]
    dm1 = lm1[1] - lm1[0]
    stp0 = dm0/sm0
    stp1 = dm1/sm1
    return stp0, stp1

# scans
#

def preprocess_sctitle(sctitle):
    s = sctitle.strip()
    wa = s.split('(')
    wb = wa[1].split(')')
    wc = wb[0].split(',')
    scan_tipe = wa[0].strip()
    str_scan_params = tuple(x.strip() for x in wc)
    return scan_tipe, str_scan_params


# meshlike scans akamap, akamap_lut, dmesh, amesh

MESHARGS_TYPES = (str, float, float, int, str, float,float, int, float)
ALLOWED_MESHTIPES = ('dmesh', 'amesh', 'akmap_lut', 'akamap')

def to_mesh_params(str_scan_params):
    (mn1,ll1,ul1,n1,mn2,ll2,ul2,n2,mexpt) =\
        tuple(tp(val) for tp,val in zip(MESHARGS_TYPES, str_scan_params))
    limits = ((ll1,ul1), (ll2,ul2))
    dims = (n1,n2)
    motnames =(mn1, mn2)
    return dims, limits, motnames

def mesh_read_counter_data(scn, str_counters):
    #sctitle = str.encode(scn['title'].asstr()[()])
    sctitle = scn['title'].asstr()[()]
    scan_tipe, str_scan_params = preprocess_sctitle(sctitle)
    if not scan_tipe in ALLOWED_MESHTIPES:
        sys.stdout.flush()
        raise ValueError(f'illegal meshtipe: {scan_tipe}')
    dims, limits, motnames = to_mesh_params(str_scan_params)
    (s1, s0) = dims
    shape = (s0, s1)
    inst = scn['instrument']
    posi = inst['positioners']
    initial_motpos = (
            np.array(posi[motnames[0]]),
            np.array(posi[motnames[1]])
        )
    print('initial_motpos', initial_motpos)

    scanpar_dc = dict(scan_tipe=scan_tipe, dims=dims, limits=limits,
        motnames=motnames, initial_motpos=initial_motpos, shape=shape)


    meas = scn['measurement']
    data_dc = dict()
    for scou in str_counters:
        arr = np.array(meas[scou])
        arr = arr.reshape(shape)
        data_dc[scou] = arr
    return data_dc, scanpar_dc

# h5 io
#
def h5path_get(h5item, tppath):
    res = h5item
    for k in tppath:
        res = res[k]
    return res

class H5FileReadonly(object):

    def __init__(self, h5_fxpth):
        self.h5_fxpth = h5_fxpth
        self.fraw = None
        self.fh5  = None
        self.fraw = open(self.h5_fxpth, 'rb')
        self.fh5  = h5py.File(self.h5_fxpth, 'r')

    def __enter__(self):
        return self.fh5

    def __exit__(self, tipe, value, traceback):
        if not None is self.fraw:
            self.fraw.close()


# bliss session
#

def get_current_data_dapth():
    scs = SCAN_SAVING
    dir_components = dict(
        proposal_dirname = scs.proposal_dirname,
        beamline = scs.beamline,
        proposal_session_name = scs.proposal_session_name,
        collection_name = scs.collection_name,
        dataset_name = scs.dataset_name)
    dapth_part = scs.template.format(**dir_components)
    return path.join(scs.base_path, dapth_part)

def get_current_data_fupth():
    scs = SCAN_SAVING
    return get_local_data_fupth(scs.collection_name, scs.dataset_name)

def get_current_data_fapth():
    dapth = get_current_data_dapth()
    fupth = get_current_data_fupth()
    fapth = path.join(dapth, fupth)
    return fapth

def get_local_data_dapth(collection_name, dsname):
    scs = SCAN_SAVING
    dir_components = dict(
        proposal_dirname = scs.proposal_dirname,
        beamline = scs.beamline,
        proposal_session_name = scs.proposal_session_name,
        collection_name = collection_name,
        dataset_name = dsname
    )
    dapth_part = scs.template.format(**dir_components)
    return path.join(scs.base_path, dapth_part)

def get_local_data_fupth(collection_name, dsname):
    return f'{collection_name:s}_{dsname:s}.h5'

def get_local_data_fapth(collection_name, dsname):
    dapth = get_local_data_dapth(collection_name, dsname)
    fupth = get_local_data_fupth(collection_name, dsname)
    fapth = path.join(dapth, fupth)
    return fapth
