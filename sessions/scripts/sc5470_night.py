print('sc5470 - Yin load 2')
l_10ms = [
#'PNxsec2um_PN1cuticle1_0017.json',
#'PNxsec2um_PN1cuticle2_0018.json',
#'PNxsec2um_PN1ori_0007.json',
#'PNxsec2um_PN1ori_0008.json',
#'PNxsec2um_PN1ori_0009.json',
'PNxsec2um_PN1ori_0010.json',
'PNxsec2um_PN1ori2_0011.json',
'PNxsec2um_PN1ori2_0012.json',
]
l_20ms = [
'PNxsec2um_PN1ori_0001.json',
'PNxsec2um_PN1ori_0002.json',
'PNxsec2um_PN1ori_0003.json',
'PNxsec2um_PN1ori_0004.json',
'PNxsec2um_PN1ori_0005.json',
'PNxsec2um_PN1ori2_0013.json',
'PNxsec2um_PN1ori2_0014.json',
'PNxsec2um_PN1ori2_0015.json',
'PNxsec2um_PN1ori2_0016.json',
]


def sc5470_night():
    so()
    mgeig_x()
    eigerhws(True)

    START_KEY = 'b'
    #print('\n test \n')
    for _ in l_10ms:
        #print(f'name for 10 ms is {_[:-5]}')
        newdataset(f'{START_KEY}_{_[:-5]}_10ms')
        rstp(_)
        kmap.dkmap(nnp2, -60,60,240, nnp3, -60,60,240,0.01, frames_per_file=240*10)
    #print('\n test \n')
    for _ in l_20ms:
        #print(f'name for 20 ms is {_[:-5]}')
        newdataset(f'{START_KEY}_{_[:-5]}_20ms')
        rstp(_)
        kmap.dkmap(nnp2, -60,60,240, nnp3, -60,60,240,0.02, frames_per_file=240*10)


def sc5470_night_main():
    try:
        sc5470_night()
        sc()
    finally:
        sc()
        sc()
