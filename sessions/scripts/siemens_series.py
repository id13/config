# load_script('siemens_series')

print('siemens series load - 5')
import numpy as np
import tango

def siemens_series(xmot, xstart, xstep, xnpts, *p):
    doos('siemens_series')
    for i in range(xnpts):
        print('3 sec to interrupt ...')
        sleep(3)
        next_xpos = xstart + i * xstep
        if xmot.name in ['nnp1', 'nnp5']:
            mv(xmot, next_xpos)
        else:
            raise ValueError(f'illegal motor {xmot} ({xmot.name})')
            return
        try:
            scanno = SCANS[-1].scan_number
            scanno = int(scanno)

        except IndexError:
            scanno = -1
        except:
            scanno = -2
        s = f'xindex: {i}    nominal_xpos {next_xpos}    xpos = {xmot.position}   #S{scanno} ========='
        print(s)
        doos(s)
        doos(s)
        kmap.dkmap(*p)
        sync()
        elog_plot(scatter=True)
        sync()

#def doop(*args, **kw):
#    print(*args, **kw)
#    dooc(*args, **kw)

def sven_getflux(nmaxvalues=10, background=0, ringcurrent=0):
    # omit 1st point, since this seems to have too less intensity
    acq_time = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:acq_time_3'][1:]
    the_data = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][1:]
    the_data = np.array(the_data) - background
    
    s = nmoco.comm_ack('?inbeam')
    w = s.split()
    gain = float(w[3])
    wbz = wbetz.position
    ll = []
    ll.append(f'==== FLUX (#{SCANS[-1].scan_number})')
    ll.append('nmoco gain: {0:.2e},  acq_time: {1:.2f} s,  energy: {2:.2f} keV'.format(gain, acq_time[0], ccmo.get_energy()))
    ll.append(f'ns6vg: {ns6vg.position:.3f}, ns6hg: {ns6hg.position:.3f}')

    if wbz > 3:
      ll.append('no prefocusing lens')
    elif wbz > -12:
      ll.append('1 prefocusing lens')
    elif wbz > -30:
      ll.append('2 prefocusing lenses')
    else:
      ll.append('3 prefocusing lenses')
    
    if ringcurrent > 1:
        ll.append('ring current: {0:.2f} mA'.format(ringcurrent))
    gain_fac = gain*1000.0*2.15e+6/acq_time
    sven_flux_arr = the_data*gain_fac
    ll.append(f'Total factor: {np.mean(gain_fac):.3e},  background: {background}')
    #ll.append(f'printing max {nmaxvalues} values ...')
    #for v in sven_flux_arr[:nmaxvalues]:
    #    ll.append("%g" % v)
    if len(the_data) > 1:
        ll.append(f'mean det  [cps]:  {np.mean(the_data):g}')
        ll.append(f'mean flux [ph/s]: {np.mean(sven_flux_arr):g}')
        if ringcurrent > 1:
            ll.append(f'mean flux [ph/s]: {np.mean(sven_flux_arr)*200./ringcurrent:g} (at 200 mA)')
    ll.append('\n')
    res = '\n'.join(ll)
    print(res)
    dooc(res)
    

def sven_ct(expt, n=1, ringcurrent='auto'):
    if 'auto' == ringcurrent:
        machinfo = tango.DeviceProxy("acs.esrf.fr:10000/fe/master/id13")
        ringcurrent = machinfo.SR_Current
    # measure background
    # background is currently always 0!!!
    #fshclose()
    #sct(expt)
    #background = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
    background = 0.0
    print(f'Background: {background}')
    
    #fshtrigger()
    fshopen()
    if n > 1:
        loopscan(n, expt)
        sven_getflux(n, background, ringcurrent)
    else:
        sct(expt)
        sven_getflux(background=background, ringcurrent=ringcurrent)
    fshtrigger()

def pcomvr(motor, delta, dt=0.1):
    mvr(motor, delta)
    pcoct(dt)

def pcomv(motor, delta, dt=0.1):
    mv(motor, delta)
    pcoct(dt)

def pcokemvr(dy, dz, dt=0.1):
    mvr(nt2j, (dy+dz)/1.41, nt2t, (-dy+dz)/1.41)
    pcoct(dt)

def pcomvrpcoviewarea(dy, dz, dt=0.1):
    umvr(ns6vo, -dz, nt5t, dz, ndetz, dz)
    umvr(ns6ho, -dy, nt5j, -dy, mot_pco, dy, nfshy, -dy)
    pcoct(dt)

# Example for deflection to top-right in rotated PCO image in Flint: s6mll(0.00, 0.05)  , with dirH = -1, dirV = -1
def s6mll(start, end, tPCO=-1, tEiger=-1, doLog=True):
    hg0 = 0.105
    ho0 = 0.915
    vg0 = 0.105
    vo0 = -0.013
    maxAperture = 0.105
    dirH = -1
    dirV = 1
    do = (start+end)/2-maxAperture/2
    da = maxAperture - (end-start)
    nextScan = -1
    if SCANS is not None and len(SCANS)>0:
        nextScan = int(SCANS[-1].scan_number)+1

    mv(ns6vg, vg0-da, ns6hg, hg0-da, ns6ho, ho0-do*dirH, ns6vo, vo0-do*dirV)  # both directions
    #mv(ns6hg, hg0-da, ns6ho, ho0-do*dirH)  # only hor. focusing lens
    if doLog:
        dooi("Set MLL aperture from {0:.0f} to {1:.0f} um. Next Scan is #{2:d}".format(start*1000, end*1000, nextScan))
        #dooi("Set MLL hor. aperture from {0:.0f} to {1:.0f} um. Next Scan is #{2:d}".format(start*1000, end*1000, nextScan))
    fshtrigger()
    sleep(2)
    if tPCO > 0.001:
      pcoct(tPCO)
    if tEiger > 0.001:
      sct(tEiger)

def sven_efficiency_scan():
    maxAperture = 0.05
    s6mll(-0.005, maxAperture+0.005)
    sven_ct(1,10)
    sleep(5)
    for ap in range(0, 16, 5):
        s6mll(0.000+ap/1000, maxAperture-ap/1000)
        sven_ct(1,10)
        sleep(5)
    # additional range, asymmetric:
    #for ap in range(110, 151, 10):
    #    s6mll(0.000, ap/1000)
    #    sven_ct(1,10)
    #    sleep(5)
    s6mll(-0.005, maxAperture+0.005)
    sven_ct(1,10)

def sven_ptycho_series():
    maxAperture = 0.105
    for ap in range(-5, 31, 5):
        s6mll(0.000+ap/1000, maxAperture-ap/1000)
        sleep(5)
        kmap.dkmap(nnp2, -0.75,0.75,60, nnp3,-0.75,0.75,60,0.04)
        sleep(5)
    sc()


def sven_test():
    machinfo = tango.DeviceProxy("acs.esrf.fr:10000/fe/master/id13")
    ringcurrent = machinfo.SR_Current
    lines = 'ring current: {0:.2f} mA\n'.format(ringcurrent)

    s = nmoco.comm_ack('?inbeam')
    w = s.split()
    gain = float(w[3])
    lines = lines + 'nmoco gain: {0:.2e},  energy: {1:.2f} keV\n\n'.format(gain, ccmo.get_energy())


    position = SCANS[-1].get_data()['ns6vg'][1:]
    the_data = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][1:]
    lines = lines + 'Last positions:'
    for x in position:
        lines = lines + '\n{0:.4f}'.format(x)
    lines = lines + '\n\nLast counter values:'
    for x in the_data:
        lines = lines + '\n{0:.1f}'.format(x)
    print(lines)
    dooi(lines)

