print('load 1')
import traceback
import time
def set_condition(idx, attempt):
    # newdataset
    print('\n\n\n======================== setting condition:')
    print(f'    condition index = {idx}')
    print(f'    condition attempt = {attempt}')


def x_check_dets():
    mgallmpx()
    th1 = eiger.camera.energy_threshold
    if th1 < 0.5*12.8:
        raise RuntimeError()


def exit_door():
    print('Finished kmap. You can control+c within 5s.')
    sleep(1)
    print('Finished kmap. You can control+c within 4s.')
    sleep(1)
    print('Finished kmap. You can control+c within 3s.')
    sleep(1)
    print('Finished kmap. You can control+c within 2s.')
    sleep(1)
    print('Finished kmap. You can control+c within 1s.')
    sleep(1)
    print('Too late! Wait for after the next map, in 1min 4s...')    

'''
POSLIST_LIST = [
'pos._pos',
'pos2._pos',
...
]

SCAN_LIST = [
(01..),
(01..),
]
'''
PREFIX = 'alexey'
def doo_scan(idx, attempt):
    newdataset(f'{PREFIX}_{idx:04d}_{attempt}')
    gopos(POS_LIST[idx]
    dkmapyz_2(*SCAN_LIST[idx]


class THEID(object):

    CMD_ID = 580

def det1starter_macro(theidobj, idx, attempt, maxattempts=3):
    if attempt > maxattempts:
        raise RuntimeError(f'maxattempts reached (attempt {attempt} maxattempts {maxattempts}')
    else:
        try:
            doo_scan(idx, attempt)
        except:
            exit_door()
            print(traceback.format_exc())
            print(f'failure {idx} attempt {attempt}')
            THEID.CMD_ID = THEID.CMD_ID+1
            com_sender(THEID.CMD_ID, 'restart_both')
            try:
                time.sleep(20)
                x_check_dets()
            except:
                time.sleep(40)
                x_check_dets()
            det1starter_macro(theidobj, idx, attempt+1, maxattempts=maxattempts)

def det1starter_test_macro():

    for i in range(20000):
        det1starter_macro(THEID, i, 1, maxattempts=5)
        exit_door()
