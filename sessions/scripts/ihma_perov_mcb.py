import bisect
import numpy as np

def readxy(fname, dname=None, skip=0):
    if dname:
        fname = os.path.join(dname, fname)
    with open(fname, "r") as f:
        ll = f.readlines()
    ll = ll[skip:]
    ll = [x.strip() for x in ll]
    ll = [x for x in ll if x and not x.startswith('#')]
    x = np.zeros((len(ll),), dtype=np.float64)
    y = np.zeros((len(ll),), dtype=np.float64)
    for (i,l) in enumerate(ll):
        (x[i],y[i]) = tuple(map(float, l.split()))
    return (x,y)

def writexy(xarr,yarr, fname, dname=None):
    ll = []
    for (x,y) in zip(xarr,yarr):
        ll.append(f'{x} {y}')
    s = '\n'.join(ll)
    with open(fname, "w") as f:
        f.write(s)

TABLE = '''
0 5007.755790350664 6.2011249999999905
1 5851.026705862792 6.210500000000025
2 6851.4597146526885 6.2205000000000155
3 8072.106012244831 6.229874999999993
4 9286.54036939121 6.239874999999984
5 10885.493840130232 6.249875000000031
6 12647.510747570492 6.259250000000009
7 15023.57796807428 6.2692499999999995
8 17251.80100596894 6.2792499999999905
9 20570.530189117046 6.288625000000025
10 23513.126378274286 6.2986250000000155
11 27790.97916624797 6.307999999999993
12 32804.32570299982 6.317999999999984
13 39600.80974470751 6.328000000000031
14 47191.17977538354 6.337375000000009
15 56709.114926379356 6.3473749999999995
16 70958.68511491177 6.361750000000029
17 81032.89953282554 6.37175000000002
18 97622.197090254 6.381750000000011
19 113397.73044928772 6.391124999999988
20 126996.91047314888 6.401124999999979
21 146324.74197609135 6.410500000000013
22 157122.95805295862 6.420500000000004
23 165288.86436593445 6.430499999999995
24 169043.9823653973 6.439875000000029
25 170000.0 6.44987500000002
'''


def linint_calc(x,a,b,ya,yb):
    m = (yb-ya)/(b-a)
    t = ya
    return m*(x-a) + t

def xcp_linint(x, table):
    (tx,ty) = table
    leng = len(tx)
    i = bisect.bisect_left(tx, x)
    print ('bisect_index', i)
    if i > 0 and i < leng:
        return linint_calc(x, tx[i-1], tx[i], ty[i-1], ty[i])
    else:
        raise ValueError('illegal attempt to extrapolate ...')

def get_inmem_scandata(scn, counter_str):
    return scn.get_data()[counter_str]

def indent(s, indent='   '):
    ll = s.split('\n')
    ill = []
    for l in ll:
        ill.append(f'{indent}{l}')
    return '\n'.join(ill)


class MyPlot(object):

    def __init__(self, plot_key):
        self.plot_key = plot_key

    def get_flint_plot(self):
        try:
            return self.flint_curve_plot
        except AttributeError:
            f = flint()
            self.flint_curve_plot = plt = f.get_plot('curve', name=self.plot_key)
        return plt

class Curve(object):

    def __init__(self, myplot, curve_key):
        self.myplot = myplot
        self.curve_key = curve_key
        self.ini_flg = False

    def install_data(self, xy):
        self.xy = xy

    def plot(self):
        x,y = self.xy
        plt = self.myplot.get_flint_plot()
        if self.ini_flg:
            plt.remove_item(legend=self.curve_key)
        plt.add_curve(x, y, legend=self.curve_key)



class U18tuningTable(object):

    def __init__(self, table_fupth, mode='raw', stastoni=None, counter_str=None, verbose=1):
        self.myplot = MyPlot('U18TT')
        self.curves = cvdc = dict()
        self.tuning_key = tunk = 'tuning'
        cvdc[tunk] = Curve(self.myplot, tunk)
        self.intensity_key = intenk = 'intensity'
        cvdc[intenk] = Curve(self.myplot, intenk)
        
        self.table_fupth = table_fupth
        self.verbose = verbose
        self.stastoni = stastoni
        self.counter_str = counter_str

        if 'create' == mode:
            self.create_tuningtable()
        elif 'read' == mode:
            tuning_table = self.read_tuning_table(table_fupth)
            self.install_tuning_table(tuning_table)
        elif 'raw' == mode:
            pass
        else:
            raise ValueError('illegal u18 tuning table mode: {mode}')

    def setup_intensity_table(self, scale):
        self.intensity_scale = scale
        x,y = self.tuning_table
        self.max_y = max(list(y))
        intensity = self.intensity_scale*y/self.max_y
        self.intensity_table = x, intensity
        self.lookup_table = intensity, x

    def tun2int(self, tuning_counter_value):
        intensity_value = self.intensity_scale*tuning_counter_value/self.max_y
        return intensity_value

    def int2tun(self, intensity_value):
        tuning_counter_value = self.max_y*intensity_value/self.intensity_scale
        return tuning_counter_value

    def lookup_intensity(self, req_gap):
        intensity_value = xcp_linint(req_gap, self.intensity_table)
        return intensity_value

    def lookup_counter_value(self, req_gap):
        counter_value = xcp_linint(req_gap, self.tuning_table)
        return counter_value

    def lookup_gap(self, req_intensity):
        gap_value = xcp_linint(req_intensity, self.lookup_table)
        return gap_value

    def goto_intensity(self, intensity):
        gap_val = self.lookup_gap(intensity)
        self.mv_u18(gap_val)
        actual_gap_val = u18.position
        actual_nominal_intensity = self.lookup_intensity(actual_gap_val)
        expected_counter_value = self.lookup_counter_value(actual_gap_val)
        print(f'req. gav_val           = {gap_val}')
        print(f'req. intensity         = {intensity}')
        print(f'actual nom. intensity  = {actual_nominal_intensity}')
        print(f'actual exp. ctr_val    = {expected_counter_value}')
        return (actual_gap_val, actual_nominal_intensity, expected_counter_value)

    def store_tuning_table(self, tutab, fxpth):
        xarr,yarr = self.tuning_table
        writexy(xarr,yarr, fxpth)

    def read_tuning_table(self, fxpth):
        return readxy(fxpth)

    def install_tuning_table(self, tutab):
        self.tuning_table = tutab

    def plot_tuning_table(self):
        curve_key = self.tuning_key
        crv = self.curves[curve_key]
        crv.install_data(self.tuning_table)
        crv.plot()

    def plot_intensity_table(self):
        curve_key = self.intensity_key
        crv = self.curves[curve_key]
        crv.install_data(self.intensity_table)
        crv.plot()

    def plot_table(self, curve_key, table):
        crv = self.curves[curve_key]
        crv.install_data(table)
        crv.plot()

    def get_dummy_table(self):
        x = np.linspace(6.2,6.6,81, dtype=np.float64)
        y = x*np.sin(20*x)
        return (x,y)

    def add_curve(self, curve_key):
        if curve_key in self.curves:
            raise KeyError(f'curve named {curve_key} is already installed!')
        self.curves[curve_key] = Curve(self.myplot, curve_key)

    def acq_tuningtable(self):
        (a,b,n) = self.stastoni
        d = (b-a)/n
        x = []
        y = []
        for i in range(n):
            print(f'==== cycle: {i}')
            target_gap = a + d*i
            (uu18, err) = self.mv_u18(target_gap)
            print(f'    target_gap = {target_gap:3.6f}; uu18 = {uu18:3.6f}; err = {err:3.6f}')
            x.append(uu18)
            cts = self.acquire(5,0.1)
            print(f'    counts = {cts};')
            y.append(cts)
        return (x,y)

    def create_tuningtable(self):
        tuning_table = self.acq_tuningtable()
        self.tuning_table = tuning_table
        return tuning_table

    def mv_u18(self, target_gap, eps=0.005):
        delta = 1.0
        cyc = 0
        limit = 20
        lim_ctr = 0
        while delta > eps:
            try:
                mv(u18, target_gap)
            except RuntimeError:
                print('got the silly tolerance RuntimeError again - sigh ...')
                cyc -= 1
            
            u18pos = u18.position
            print(u18.position)
            delta =math.fabs(target_gap-u18pos)
            if cyc > 3:
                break
            if lim_ctr > limit:
                raise RuntimeError('too many u18 movement errors - giving up ...')
            cyc += 1
            lim_ctr +=1
        uu18 = u18.position
        return uu18, target_gap

    def acquire(self, n, expt):
        the_scan = loopscan(n, expt)
        arr = get_inmem_scandata(the_scan,self.counter_str)
        if self.verbose > 1:
            print(f'acq arr: {arr}')
        return float(np.mean(arr))
