def es1319_roiset_001():
    xmap3.rois.set('Si', 325, 376)
    xmap3.rois.set('S', 389, 539)
    xmap3.rois.set('K', 622, 690)
    xmap3.rois.set('Ca', 700, 772)
    xmap3.rois.set('Ti', 870, 936)
    xmap3.rois.set('Mn', 1141, 1210)
    xmap3.rois.set('Fe', 1223, 1335)
    xmap3.rois.set('Ni', 1467, 1508)
    xmap3.rois.set('Cu', 1561, 1646)
    xmap3.rois.set('Zn', 1678, 1789)
    xmap3.rois.set('Se', 2172, 2322)
    xmap3.rois.set('Br', 2312, 2462)
    xmap3.rois.set('Rb', 2617, 2719)
    xmap3.rois.set('Sr', 2756, 2873)
    xmap3.rois.set('Y', 2910, 3045)
    xmap3.rois.set('Zr', 3061, 3215)
    xmap3.rois.set('Nb', 3243, 3364)
    xmap3.rois.set('Mo', 3449, 3558)
    xmap3.rois.set('Rayl', 3763, 3870)
    xmap3.rois.set('Compt', 3608, 3742)













