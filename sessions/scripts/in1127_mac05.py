print ("mac05 start")
def the_scan():
    dscan(ustrz,-0.1,0.1,100,0.05)

def the_mesh1():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.05)
    
def the_mesh2():
    dmesh(ustrz,-0.1,0.1,50,ustry,-0.2,0.2,100,0.02)
    
def the_mesh3():
    dmesh(ustrz,0,1,500,ustry,-0.2,0.2,2,0.05)
    
def in1127_mac05():
    so()
    fshtrigger()
    mgeig()
    for i in range(64,65):
        pos_name = "s_fr03_01_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        #newdataset(pos_name[3:])      
        the_mesh3()
        
        
print ("mac05 end")
