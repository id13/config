print('ihsc1816_augusto - load 5')
START_KEY = 'f'

def parse_wstp(w, stp):
    n = int(w/stp) +1
    nll = n//2
    nul = n - nll
    ll = stp*nll
    ul = stp*nul
    return -ll, ul, n

def doo_scan(*p):
    print('scanparameters:', *p)
    dkmapyz_2(*p, retveloc=5.0)

def doo_raddam(sampkey, poskey):
    pos_name = f'{sampkey}_{poskey}'
    dsname = f'{START_KEY}_RADDAM_{sampkey}_{poskey}'
    print('======== measuring dadiation damage:', sampkey, ' pos =', poskey)
    print('dataset:', dsname)
    rstp(pos_name)
    newdataset(dsname)

    # print('loopscan ...')
    loopscan(500, 0.1)

def doo_position(sampkey, poskey, w, wstp, h, hstp, expt):
    wll, wul, wn = parse_wstp(w, wstp)
    hll, hul, hn = parse_wstp(h, hstp)
    params = (wll, wul, wn, hll, hul, hn, expt)
    pos_name = f'{sampkey}_{poskey}'
    dsname = f'{START_KEY}_{sampkey}_{poskey}'
    print('======== measuring:', sampkey, ' pos =', poskey)
    print('dataset:', dsname)
    rstp(pos_name)
    newdataset(dsname)
    doo_scan(*params)


def ihsc1816_run_1():
    raise runtimeError('invalid')
    so()
    sleep(5)
    try:
        with bench():
            doo_raddam('S1', 'p11')

        with bench():
            doo_position('S1', 'p10', 2.0, 0.005, 0.50, 0.020, 0.025)

        with bench():
            doo_position('S3', 'p1', 0.2, 0.002, 0.10, 0.002, 0.05)

        with bench():
            doo_position('S3', 'p2', 2.0, 0.005, 0.50, 0.020, 0.025)
    finally:
        sc()
        sc()
        sc()

def doo_ribbon(sampkey, air=False):
    print(f'****\n*********************** doing ribbon {sampkey}\n****')
    doo_raddam(sampkey, 'p1')
    doo_position(sampkey, 'p2', 2.0, 0.005, 0.50, 0.020, 0.025)
    doo_position(sampkey, 'p3', 0.6, 0.002, 0.10, 0.002, 0.05)
    if air:
        doo_position(f'air_{sampkey}', 'p1', 0.1, 0.005, 0.01, 0.01, 0.025)
        doo_position(f'air_{sampkey}', 'p2', 0.1, 0.002, 0.01, 0.01, 0.05)

def doo_fibres(sampkey):
    print(f'####\n####################### doing fibres {sampkey}\n####')
    doo_raddam(sampkey, 'p1')

    # junction
    doo_position(sampkey, 'p2', 0.6, 0.005, 0.60, 0.005, 0.025)
    doo_position(sampkey, 'p3', 0.6, 0.005, 0.60, 0.005, 0.025)

    # fibres
    doo_position(sampkey, 'p4', 0.4, 0.002, 0.40, 0.020, 0.05)
    doo_position(sampkey, 'p5', 0.4, 0.002, 0.20, 0.002, 0.05)

def ihsc1816_run_2():
    so()
    sleep(5)
    try:
        doo_ribbon('S7', air=True)
        doo_ribbon('S8', air=False)
        doo_fibres('F8')
        doo_fibres('F12')
        doo_fibres('F18')
        doo_fibres('F14')
        doo_fibres('F22')

    finally:
        sc()
        sc()
        sc()
