from numpy import *
from matplotlib import pyplot as plt
import scifitfunc as sff
from pprint import pprint



def _test():
    sinff = sff.SciFuncEnv1D(**sff.SCI_SIN)
    #pprint(sff.make_sci_poly1d_dict(2))
    #sinff = sff.SciFuncEnv1D(**sff.make_sci_poly1d_dict(2))
    x = linspace(0.0, 360.0, 181)
    xr = x*pi/180.0
    print(len(x))
    #y = 30.473**sin(xr)*(1.0+0.002*xr)
    y = 1.0*sin(xr)
    e = random.random(len(y))*0.2 - 0.1
    print (e)
    y = y+e
    yy = y*0
    de = 5
    for i in range(len(x)-de):
        #fit = polyfit(x[i:i+de], y[i:i+de],1)
        #f = poly1d(fit)
        l = max(i-20*de, 0)
        p = sinff.update(None, y[l:i+de], xr[l:i+de])
        
        #print(fit, f(x[i+de]))
        #yy[i+de] = f(x[i+de])
        yy[i+de] = sinff.eval_func(xr[i+de])
    #print(x,y,yy)
    plt.plot(x,y, 'green')
    plt.plot(x,yy, 'red')
    plt.show()

if __name__ == '__main__':
    _test()
