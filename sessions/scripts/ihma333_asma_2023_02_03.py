print('ihma333_asma_2023_02_03 - load 2')
def ihma333_yesno(quetxt=''):
    print(quetxt)
    print('please answer below with "y" or "n":')
    ans = input()
    if "y" == ans:
        return True
    else:
        return False

CALIB_START_KEY = 'a'
def ihma333_calibration():
    quetxt = '''
This routine contains dangerous detector movments
only proceed with "y" when you are fully aware of the situation and
when you are sure there is no problem!
'''
    ans = ihma333_yesno(quetxt)
    if ans:
        umv(ndetx, -250)
        for i in range(260, 370, 10):
            print('========================== iteration item:', i)
            dsname = f'{CALIB_START_KEY}_calib_al2o3_ndetx_m{i}p0'
            print(dsname)
            newdataset(dsname)
            target_pos = float(-1*i)
            print(f'target_pos = {target_pos}')
            umv(ndetx, target_pos)

            kmap.dkmap(nnp2, -40, 40, 20, nnp3, -80, 80, 80, 0.02)
