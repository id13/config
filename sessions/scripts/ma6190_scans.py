print('ma6190_scans - load 9')
START_KEY = 'b5'

def doo_setup(posname):
    rstp(posname)
    dsprefix = f'{START_KEY}_posname'
    return dsprefix


def doo_setup_hr(posname):
    rstp(posname)
    dsprefix = f'{START_KEY}_{posname}_hr'
    return dsprefix



def doo_fine(dsprefix):
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, -0.6, 0.0, 1, nnp2, -10,10,200, nnp3, -10, 10, 200, 0.02)
    
def doo_coarse(dsprefix):
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -50,50,200, nnp3, -50, 50, 200, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, -0.6, 0.0, 1, nnp2, -50,50,200, nnp3, -50, 50, 200, 0.02)

def night1():
    pref = doo_setup('x50_roi1')
    doo_fine(pref)
    pref = doo_setup('x50_roi2')
    doo_coarse(pref)
    pref = doo_setup('x50_roi3')
    doo_fine(pref)
    pref = doo_setup('x50_roi4')
    doo_coarse(pref)


def night3():
    pref = doo_setup('1x50')
    doo_fine(pref)
    pref = doo_setup('2x50')
    doo_coarse(pref)
    pref = doo_setup('3x50')
    doo_fine(pref)
    pref = doo_setup('4x50')
    doo_coarse(pref)

################################################################
# night 2

def doo_topupper():

    # upper region in top sample

    dsprefix = f'{START_KEY}_topupper'

    rot_scan_series(dsprefix, 1.5-0.6, 1.5+0.6, 12, nnp2, -4, 4, 80, nnp3, -20, 40, 600, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, 1.5-0.6, 1.5+0.0, 1, nnp2, -4, 4, 80, nnp3, -20, 40, 600, 0.02)
    
def doo_toplower():

    # lower region in top sample

    dsprefix = f'{START_KEY}_toplower'

    rot_scan_series(dsprefix, 1.5-0.6, 1.5+0.6, 24, nnp2, -4, 4, 80, nnp3, 40, 100, 600, 0.02)
    check_dsprefix = f'{dsprefix}_check'
    rot_scan_series(check_dsprefix, 1.5-0.6, 1.5+0.0, 1, nnp2, -4, 4, 80, nnp3, 40, 100, 600, 0.02)

    

def night2():
    doo_topupper()
    doo_toplower()


################################################################
# day1_a

def doo_day1_a():

    dsprefix = f'{START_KEY}_bp122_a_1x50'
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
    dsprefix = f'{START_KEY}_bp122_a_1x50_check'
    rot_scan_series(dsprefix, -0.6, 0, 1, nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
    


################################################################
# day1_b

def doo_day1_b():

    dsprefix = f'{START_KEY}_bp122_a_3x50'
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
    dsprefix = f'{START_KEY}_bp122_a_3x50_check'
    rot_scan_series(dsprefix, -0.6, 0, 1, nnp2, -5, 5, 100, nnp3, -5, 5, 100, 0.02)
    
################################################################
# day1_c thin stripes agains secondary raddam

def doo_day1_c():

    dsprefix = f'{START_KEY}_bp122_a_3x50_c'
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -5, 5, 100, nnp3, 0, 0.9, 10, 0.01)
    dsprefix = f'{START_KEY}_bp122_a_3x50_c_check'
    rot_scan_series(dsprefix, -0.6, 0.6, 1, nnp2, -5, 5, 100, nnp3, 0, 0.9, 10, 0.01)
    
def doo_day1_c2():

    dsprefix = f'{START_KEY}_bp122_a_3x50_c2'
    rot_scan_series(dsprefix, -0.6, 0.6, 12, nnp2, -5, 5, 50, nnp3, 0, 0.9, 5, 0.01)
    dsprefix = f'{START_KEY}_bp122_a_3x50_c2_check'
    rot_scan_series(dsprefix, -0.6, 0.6, 1, nnp2, -5, 5, 50, nnp3, 0, 0.9, 5, 0.01)

def doo_day1_c3():

    curr_nnp3 = nnp3.position
    nnp3_stp = 1.0
    curr_th = Theta.position
    try:
        for i in range(30):
            print(f"=============================== cycle: {i}")
            mv(nnp3, curr_nnp3 + i*nnp3_stp)
            dsprefix = f'{START_KEY}_bp122_a_3x50_c3_z{i:03d}'
            rot_scan_series(dsprefix, -0.5, 0.5, 10, nnp2, -5, 5, 50, nnp3, 0, 0.8, 5, 0.01)

        print(f"=============================== cycle: check")
        dsprefix = f'{START_KEY}_bp122_a_3x50_c3_check'
        rot_scan_series(dsprefix, -0.5, 0, 1, nnp2, -5, 5, 50, nnp3, 0, 0.8, 5, 0.01)
    finally:
        print("returning Theta and nnp3 ...")
        mv(nnp3, curr_nnp3)
        mv(Theta, curr_th)



def doo_day1_c4():

    curr_nnp3 = nnp3.position
    nnp3_stp = 1.0
    curr_th = Theta.position
    try:
        for i in range(30):
            mv(nnp3, curr_nnp3 + i*nnp3_stp)
            if i < 17:
                continue
            print(f"=============================== cycle: {i}")
            
            dsprefix = f'{START_KEY}_bp122_c_2x50_c3_z{i:03d}'
            rot_scan_series(dsprefix,
                 curr_th-0.5, curr_th+0.5, 10, nnp2, -5, 5, 50, nnp3, 0, 0.8, 5, 0.01)

        print(f"=============================== cycle: check")
        dsprefix = f'{START_KEY}_bp122_c_2x50_c3_check'
        rot_scan_series(dsprefix, curr_th-0.5, curr_th, 1, nnp2, -5, 5, 50, nnp3, 0, 0.8, 5, 0.01)
    finally:
        print("returning Theta and nnp3 ...")
        mv(nnp3, curr_nnp3)
        mv(Theta, curr_th)
	


################################################################
# day4

def doo_day4_a():

    dsprefix = f'{START_KEY}_cigs_cell_1x50_hr'
    rot_scan_series(dsprefix, -0.3, 0.3, 6, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)

def doo_day4_b():

    dsprefix = f'{START_KEY}_cigs_cell_2x50_hr'
    rot_scan_series(dsprefix, -0.3, 0, 1, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)

def doo_day4_c():

    dsprefix = f'{START_KEY}_cigs_cell_2x50_hr'
    rot_scan_series(dsprefix, -0.9, -0.6, 1, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)

def doo_day4_d():

    dsprefix = f'{START_KEY}_cigs_cell_2x50_hr'
    rot_scan_series(dsprefix, 0.3, 0.9, 2, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)


def doo_hr(dsprefix):
    rot_scan_series(dsprefix, -1.5, 1.5, 1, nnp2, -2.5, 2.5, 100, nnp3, -2.5, 2.5, 100, 0.02)

    


def doo_day4_e():

    pref = doo_setup_hr('2x50')
    doo_hr(pref)



def any_main():
    try:
        so()
        sleep(3)
        #night1()
        #night2()
        #doo_day1_a()
        #doo_day1_b()
        #doo_day1_c()
        #doo_day1_c2()
        #doo_day1_c3()
        #doo_day1_c4()
        #night3()
        #doo_day4_a()
        #doo_day4_b()
        #doo_day4_c()
        #doo_day4_d()
        doo_day4_e()
    finally:
        sc()
        sc()
        sc()
