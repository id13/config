SEQ_1 = [
    ('scn', 2, 0.0),
    ('press', 100.0),
    ('scn', 10, 0.0),
    ('scn', 1000, 300.0)
]
SEQ_3 = [
    ('scn', 1000, 0.0)
]
SCAN_PARAMS_1 = (-0.5, 0.5, 200, -4, 2,60, 0.01)

SEQ_2 = [
    ('press', 50.0),
    ('scn', 2, 0.0),
    ('press', 20.0),
    ('scn', 2, 10.0),
    ('scn', 1, 20.0),
    ('press', 100.0),
]
SCAN_PARAMS_2 = (-0.5, 0.5, 200, -4, 2,6, 0.01)

SP_DMY = (-0.5, 0.5, 200, -0.005, 0.005, 1, 0.01)
SP_LINE = (-0.5, 0.5, 200, -0.005, 0.005, 1, 0.01)

SEQ_PRESS_100 = [
    ('press', 100.0)
]
SEQ_PRESS_0 = [
    ('press', 0.0)
]

def press_on():
    psyrun(SP_DMY, SEQ_PRESS_100)

def press_off():
    psyrun(SP_DMY, SEQ_PRESS_0)
    
def make_asymap(nitv, dz, n=1, slp=0.0):
    ul = dz*nitv
    part = SP_LINE[:3]
    sp = part + (0.0, ul, nitv, SP_LINE[-1])
    
    seq = [('scn', n, slp)]
    psyrun(sp, seq)


def psyrun(sp, seq):
    ll0, ul0, n0, ll1, ul1, n1, expt = sp
    syringerun(ll0, ul0, n0, ll1, ul1, n1, expt, seq)
    
def syringerun(ll0, ul0, n0, ll1, ul1, n1, expt, program_seq):
    for instruct in program_seq:
        cmd = instruct[0]
        if 'scn' == cmd:
            # 'scn', nscans, sleeptime
            #
            nscans = instruct[1]
            sleeptime = instruct[2]
            for i in range(nscans):
                print(f'sleeping {sleeptime} ...')
                sleep(sleeptime)
                dkmapyz_2(ll0, ul0, n0, ll1, ul1, n1, expt, retveloc=5)
                sleep(0.2)
                elog_plot(scatter=True)
        elif 'press' == cmd:
            # 'press', pressure_value
            #
            pressure_value = instruct[1]
            nanodac_loop2.output.set_value(pressure_value)

def runit(scanparams, seq):
    try:
        so()
        sleep(5)
        ll0, ul0, n0, ll1, ul1, n1, expt = scanparams
        syringerun(ll0, ul0, n0, ll1, ul1, n1, expt, seq)
    finally:
        sc()
        sc()
        sc()

def ihsc1863_main():
    runit(SCAN_PARAMS_2, SEQ_2)
