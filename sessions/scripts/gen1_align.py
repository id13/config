from os import path
import json
from collections import OrderedDict as Odict
import numpy as np
from scipy import ndimage as ndi
class NoScanFound(Exception): pass

class Virtual(Exception): pass


KMAP_MOTORS = (nnp4, nnp5, nnp6)
#KMAP_MOTORS = (ustry, ustrz)
KMAP_MOTOR_DC = Odict(((m.name, m) for m in KMAP_MOTORS))

def write_json(fxpth, dc):
    if not fxpth.endswith('.json'):
        raise ValueError(f'SAFETY: illegal path for write_json {fxpth}')
    with open(fxpth, 'w') as f:
        json.dump(dc, f)

def read_json(fxpth):
    with open(fxpth, 'r') as f:
        dc = json.load(f)
    return dc





class EvalDb(object):

    def __init__(self, fxpth):
        self.fxpth = fxpth
        self.dc = dict()

    def add_record(self, idx, record):
        evalbox, corrbox = record
        edc = box_to_dict(evalbox)
        cdc = box_to_dict(corrbox)
        self.dc[idx] = (edc, cdc)

    def get_record(self, idx):
        (edc, cdc) = self.dc[idx]
        evalbox = dict_to_box(edc)
        corrbox = dict_to_box(cdc)
        return evalbox, corrbox

    def save(self):
        write_json(self.fxpth, self.dc)

    def load(self):
        self.dc = read_json(self.fxpth)


class ObjParser(object):

    KEYS = "ori".split()

    def __init__(self, ctr_map_arr=None, out_tpl=None):
        if out_tpl:
            self.edflogger = EDFClosedKeySet_RW(out_tpl, self.KEYS)
        else:
            self.edflogger = None
        
        self.ctr_map_arr = ctr_map_arr

    def frame(self, arr, ibounds2d, val=7, mode='add', dtype='int16', endd=1):
        brr = np.zeros(arr.shape, dtype)
        (a,b),(c,d) = ibounds2d
        (a,b),(c,d) = (a,b-endd),(c,d-endd)
        brr[a:b,c] += 1
        brr[a:b,d] += 1
        brr[a,c:d] += 1
        brr[b,c:d] += 1
        return np.where(brr, val, 0).astype(dtype)

    def corners(self, arr, ibounds2d, val=7, mode='add', dtype='int16', endd=1):
        brr = np.zeros(arr.shape, dtype)
        (a,b),(c,d) = ibounds2d
        (a,b),(c,d) = (a,b-endd),(c,d-endd)
        brr[a,c] = 1
        brr[b,c] = 1
        brr[a:d] = 1
        brr[b:d] = 1
        return np.where(brr, val, 0).astype(dtype)

    def edflog(self, k, arr):
        if None is self.edflogger:
            return
        else:
            self.edflogger[k] = arr

    def parse(self, **kw):
        raise Virtual()

    def check_ibounds2d(self, ibounds2d):
        (a,b),(c,d) = ibounds2d
        (a,b,c,d) = map(int, (a,b,c,d))
        return min(a,b,c,d) >= 0

    def naive_th_dsearch(self, arr1d, thd, direction=1):
        for i, v in enumerate(arr1d[::direction]):
            if v > thd:
                if -1 == direction:
                    return len(arr1d) - i - 1
                else:
                    return i
        return -10

class ObjParser1(ObjParser):

    KEYS = "ori medflt thd framed".split()

    def __init__(self, ctr_map_arr=None, out_tpl=None):
        # order = PYTHON
        super().__init__(ctr_map_arr=ctr_map_arr, out_tpl=out_tpl)

    def parse(self, thd=3.7, proj_thd=4, med_width=9):
        map_arr = self.ctr_map_arr
        self.edflog('ori', map_arr)
        md_arr = ndi.median_filter(map_arr, med_width)
        self.edflog('medflt', md_arr)
        th_arr = np.where(md_arr > thd, 1, 0)
        self.edflog('thd', th_arr)

        along_ax0_proj = np.sum(th_arr,axis=0)
        print(along_ax0_proj)
        ax1_low = self.naive_th_dsearch(along_ax0_proj, proj_thd, 1)
        ax1_hi = self.naive_th_dsearch(along_ax0_proj, proj_thd, -1) + 1
        along_ax1_proj = np.sum(th_arr,axis=1)
        print(along_ax1_proj)
        ax0_low = self.naive_th_dsearch(along_ax1_proj, proj_thd, 1)
        ax0_hi = self.naive_th_dsearch(along_ax1_proj, proj_thd, -1) + 1

        ibounds2d = ((ax0_low, ax0_hi),(ax1_low, ax1_hi))
        check_flg = self.check_ibounds2d(ibounds2d)
        if check_flg:
            frame_rr = self.frame(th_arr, ibounds2d).astype(map_arr.dtype) + map_arr
            self.edflog('framed', frame_rr)
            
        return(check_flg, ibounds2d)

class MiniScanInfra(object):

    def __init__(self, def_ctr_names, data_fapth=None):
        self.def_ctr_names = def_ctr_names
        if None is data_fapth:
            self.data_fapth = get_current_data_fapth()
        else:
            self.data_fapth = data_fapth

    def make_scanname(self, scanno, sub_scanno=1):
        return f'{scanno:1d}.{sub_scanno:1d}'

    def get_last_scanno(self):
        try:
            return int(SCANS[-1].scan_number)
        except IndexError:
            raise NoScanFound()

    def get_last_scan(self):
        try:
            return SCANS[-1]
        except IndexError:
            raise NoScanFound()

    def read_raw_kmapinfo(self, scanno, counter_names=None, sub_scanno=1):
        scan_name = self.make_scanname(scanno, sub_scanno)
        if None is counter_names:
            counter_names = self.def_ctr_names
        with H5FileReadonly(self.data_fapth) as h5f:
            scn = h5f[scan_name]
        res_dc = mesh_read_counter_data(scn, counter_names)
        return res_dc

    def get_kmapinfo(self, scanno, counter_names=None, sub_scanno=1):
        if None is counter_names:
            counter_names = self.def_ctr_names
        raw_info = self.read_raw_kmapinfo(scanno, counter_names,
                                    sub_scanno=sub_scanno)
        return KmapInfo(raw_info)

    def spec_mgeo_to_kmap_params(self, motnames, spec_mgeo, expt, dkmapyz=False):
        # SPEC : from here on
        mg1, mg2 = spec_mgeo
        mname1, mname2 = motnames
        mot1 = KMAP_MOTOR_DC[mname1]
        mot2 = KMAP_MOTOR_DC[mname2]
        if dkmapyz:
            return (mg1.astuple() + mg2.astuple() + (expt,))
        else:
            return ((mot1,) + mg1.astuple() + (mot2,) + mg2.astuple() + (expt,))

class Box(object): pass

def box_to_dict(box):
    dc = dict()
    for k in dir(box):
        if '__' in k:
            pass
        else:
            dc[k] = getattr(box, k)
    return dc

def dict_to_box(dc):
    box = Box()
    for k,v in dc.items():
        setattr(box, k, v)
    return box

def print_box(box):
    print('box:')
    for k in dir(box):
        if '__' in k:
            pass
        else:
            print(f'    {k} = {getattr(box, k)}')

class KmapInfo(object):

    def __init__(self, raw_info):
        self.data, self.params = data, params = raw_info
        limits = params['limits']
        dims = params['dims']
        self.spec_mgeo = (  ContAxGeo(limits[0], dims[0]),
                            ContAxGeo(limits[1], dims[1])
                         )

    def map_py_ibounds_to_specmgeo(self, py_ibounds2d):
        # PY: py_ibounds2d
        # SPEC : from here on
        (ib2, ib1) = py_ibounds2d
        (mg1, mg2) = self.spec_mgeo
        sub_mg = (
                    mg1.make_sub_geo(ib1),
                    mg2.make_sub_geo(ib2)
                  )
        return sub_mg

    def get_corr_info(self, ibounds2d):
        o_mg1, o_mg2 = self.spec_mgeo
        i_mg1, i_mg2 = self.map_py_ibounds_to_specmgeo(ibounds2d)

        box = Box()
        box.o_cen1 = o_cen1 = 0.5*(o_mg1.ll + o_mg1.ul)
        box.o_cen2 = o_cen2 = 0.5*(o_mg2.ll + o_mg2.ul)
        box.o_width1 = (o_mg1.ul + o_mg1.ll)
        box.o_width2 = (o_mg2.ul + o_mg2.ll)
        box.i_cen1 = i_cen1 = 0.5*(i_mg1.ll + i_mg1.ul)
        box.i_cen2 = i_cen2 = 0.5*(i_mg2.ll + i_mg2.ul)
        box.i_width1 = (i_mg1.ul - i_mg1.ll)
        box.i_width2 = (i_mg2.ul - i_mg2.ll)
        box.dev_1 = i_cen1 - o_cen1
        box.dev_2 = i_cen2 - o_cen2
        return box
        


class ContAxGeo(object):

    def __init__(self, limits, nsweeps):
        ll = self.ll = limits[0]
        ul = self.ul = limits[1]
        n  = self.n = nsweeps
        self.width = w = ul - ll
        self.step_width = w/n

    def astuple(self):
        return (self.ll, self.ul, self.n)

    def make_sub_geo(self, ibounds):
        (istart, istop) = ibounds
        step_width = self.step_width
        sub_ll = self.ll + istart*step_width
        sub_ul = self.ll + istop*step_width
        nsweeps = istop - istart
        return self.__class__((sub_ll, sub_ul), nsweeps)
