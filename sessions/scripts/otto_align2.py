print('load otto_align2 - 1')

from os import path
from collections import OrderedDict as Odict
from scipy import ndimage
class NoScanFound(Exception): pass

# legend:
#     binobject : binary object


# direction: 'top', 'bottom', 'left', right'
#            it indicates where the search starts i.e. 'top' means it starts
#            at the top and goes to the bottom

def dim_to_py(x):
    (a,b) = x
    return (b,a)

def py_to_dim(x):
    (a,b) = x
    return (b,a)

def to_binary_imobj(ximobj, threshold=0):
    return np.where(ximobj > threshold , 1, 0)

def npxbound_clip_binimobj(self, binimobj, minwidth, direction=None):
    s0, s1 = binimobj.shape
    site_idx = None
    
    if 'top' == direction:
        for i in range(s0):
            thesum = binimobj[i,:].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'bottom' == direction:
        for i in range(s0-1, -1,-1):
            thesum = binimobj[i,:].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'left' == direction:
        for i in range(s1):
            thesum = binimobj[:,i].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    elif 'right' == direction:
        for i in range(s1-1, -1,-1):
            thesum = binimobj[:,i].sum()
            site_idx = i
            if thesum >= minwidth:
                break
    return (site_idx, thesum)


class AxisGeometry(object):

    # ap : absolute motor pos
    # nom_stp : nominal motor step size
    # n : number of intervals or continuous scan sampling points
    # lgeo : line geometry lower limit, upperlimit, npoints
    # bi : bound index

    def __init__(self, lgeo, nom_stp):
        self.lgeo = (llap, ulap, n) = lgeo
        self.ap_width = ap_width = ulap - llap
        self.ap_stp = ap_width/(1.0*n)
        self.nom_stp = nom_stp

    def compute_ap_boundgeo(self, bi_lgeo):
        nom_stp = self.nom_stp
        (llap, ulap, n) = self.lgeo
        ap_stp = self.ap_stp
        (ll_bi, ul_bi) = bi_lgeo
        bap_ll = ap_stp*ll_bi + llap
        bap_ul = ap_stp*ul_bi + llap
        bap_width = bap_ul - bap_ll
        n_fl = round(bap_width/nom_stp)
        print("n_fl", n_fl)
        new_n = int(n_fl) + tend
        new_bap_ll = bap_ll
        new_bap_ul = bap_ll + new_n*nom_stp
        return (new_bap_ll, new_bap_ul, new_n)

    def transform_to_diff(self, ap_lgeo, offset):
        (l,u,n) = ap_lgeo
        l = l - offset
        u = u - offset
        return ((l, u, n), offset)

class MeshScanGeometry(object):

    def __init__(self, mesh_lgeo, nom_steps):
        lgeo0, lgeo1 = mesh_lgeo
        stp0, stp1 = nom_steps
        self.axgeo = (
                        AxisGeometry(lgeo0, nom_stp=nom_stp0),
                        AxisGeometry(lgeo1, nom_stp=nom_stp1)
                     )

    def compute_ap_boundgeo(self, mesh_bi_lgeo):
        bi_lgeo0, bi_lgeo1 = mesh_bi_lgeo
        ax0, ax1 = self.axgeo
        new_bap0 = ax0.compute_ap_boundgeo(bi_lgeo0)
        new_bap1 = ax1.compute_ap_boundgeo(bi_lgeo1)
        return (new_bap0, new_bap1)

    def transform_to_diff(self, mesh_ap_lgeo, mesh_offset):
        ap_lgeo0, ap_lgeo1 = mesh_ap_lgeo
        ax0, ax1 = self.axgeo
        offset0, offset1 = mesh_offset
        new_diff0 = ax0.compute_ap_boundgeo(ap_lgeo0)
        new_diff1 = ax1.compute_ap_boundgeo(ap_lgeo1)
        return (new_diff0, new_diff1)



COUNTER_DICT = Odict(
    Ca = 'fx1_det0_Ca',
)

class Counters(object):

    def __init__(self, counter_dc):
        self.counter_dc = counter_dc
        self.genctr_dc = Odict(((v,k) for (k,v) in counter_dc.items()))
        self.counter_name_list = list(counter_dc.keys())

    def gen_to_counter_names(self, genctr_list):
        genctr_dc = self.genctr_dc
        return [genctr_dc[k] for in genctr_list]


class PostScanInfo(object):

# {'scan_tipe': 'akmap', 'dims': (180, 120), 'limits'
# : ((5.0, 95.0), (5.0, 65.0)), 'motnames': ('nnp5', 'nnp6'), 'initial_motpos': (50.0, 5
# 0.0), 'shape': (120, 180)})      

    def __init__(self, scanno, res, counters=None):
        self.dim_to_py = dim_to_py
        if None is counters:
            self.counters = Counters(COUNTER_DICT)
        else:
            self.counters = counters
        self.scanno = scanno
        self.res = res

    def get_map_arr(self, genctr_name):
        counter_name = self.counters[genctr_name]
        data = self.res[0]
        map_arr = data[counter_name]
        return map_arr

    def get_shape(self)
        return (self.res[1]['shape'])

    def get_intial_motpos(self)
        return dim_to_py(self.res[1]['initial_motpos'])

    def get_limits(self):
        return dim_to_py(self.res[1]['limits'])

    def get_motnames(self):
        return dim_to_py(self.res[1]['motnames'])

    def scan_tipe(self):
        return dim_to_py(self.res[1]['scan_tipe'])

class PreScanInfo(object):

    def __init__(self, scan_tipe, mesh_bap_lgeo, expt, keywords):
        self.mesh_bap_lgeo = mesh_bap_lgeo
        self.expt = expt
        pass
        ...

class StaticAttKey(object):

    __slots__ = ('sakkeys')

    def __init__(self, **kw):
        slots = self.sak_getslots()
        for k in slots:
            setattr(self, k, None)
        self.sakkeys = [k for k in slots if not k.startswith('sak_')]
        self.sak_loaddict(kw)

    def sak_toodict():
        odc = Odict()
        for k in self.sak_getslots():
            odc[k] = getattr(self, k)
        return odc

    def sak_getslots(self):
        return self.__slots__

    def sak_dictupdate(self, dc):
        for k,v in dc.items():
            setattr(self, k, v)

    def sak_todict(self):
        oc = dict()
        for k in self.sak_getslots():
            dc[k] = getattr(self, k)
        return dc
        
    

class TTomoDir(StaticAttKey):

    __slots__ = StaticAttKey.__slots__ + tuple(
            "ds_dapth raw_list tomo_list ttll errorll"
        )

    def __init__(self, ds_dapth=None):
        super().__init__(ds_dapth=ds_dapth)

    def update(self)
        ll = os.listdir(self.ds_dapth)
        ll = [x for x in ll if x.startswith(samplename) and x.endswith('_diff')]
        self.ttll = ttll = []
        self.errorll = errorll = []
        for x in ll:
            try:
                tt_item = TTomoItem(samplename=samplename)
                tt_item.set_tt_tomo_name(x)
                ttll.append((x, tt_item))
            except:
                errorll.append(x)


class TTomoItem(StaticAttKey):

    __slots__ = StaticAttKey.__slots__ + tuple(
            "ttn samplename start_key pnum1 pnum2 kappa phi".split()
        )

    def __init__(self, samplename=None):
        super().__init__(samplename=samplename)
        self.samplename = samplename


    def set_tt_tomo_name(self, ttn):
        _samplename, info = ttn.split('_tt_tomo_')
        assert _samplename == self.samplename
        (start_key, pnum1, pnum2, kappa, phi, diff) = info.split()
        assert diff == 'diff'
        self.start_key = start_key
        self.pnum1 = int(pnum1)
        self.pnum2 = int(pnum2)
        self.kappa = float(kappa)
        self.phi = float(phi.replace('p', ','))
        self.ttn = ttn


class MiniOdakInfra(object):

    def __init__(self, counters, data_fapth=None):
        if None is counters:
            self.counters = Counters(COUNTER_DICT)
        else:
            self.counters = counters
        self.def_ctr_names = def_ctr_names
        if None is data_fapth:
            self.data_fpath = get_current_data_fapth()
        else:
            self.data_fapth = data_fapth

    def make_scanname(self, scanno, sub_scanno=1):
        return f'{scanno:1d}.{sub_scanno:1d}'

    def get_last_scanno(self):
        try:
            return int(SCANS[-1].scan_number)
        except IndexError:
            raise NoScanFound()

    def make_post_scaninfo(self, scanno, genctr_list=None, sub_scanno=1):
        scan_name = self.make_scanname(scanno, sub_scanno)
        if None is genctr_list:
            counter_names = self.counters.counter_name_list
        else:
            counter_names = self.counters.gen_to_counter_names(genctr_list)
        with H5FileReadonly(self.data_fapth) as h5f:
            scn = h5f[scan_name]
        res = mesh_read_counter_data(scn, counter_names)
        post_scaninfo = PostScanInfo(scanno, res, counters=self.counters)
        return post_scaninfo


class EDFOutLocation(object):

    def __init__(self, dxpth):
        self.dxpth = dxpth

    def make_ecks(self, out_tpl, keys):
        out_fxpth_tpl = os.path.join(self.dxpth, out_tpl)
        ecks = EDFClosedKeySet_RW(out_fxpth_tpl, keys)
        return ecks

class EDFSakObjAlignA01(StaticAttKey):

    OUT_KEYS = &&&&
    &&&&

    __slots__ = StaticAttKey.__slots__ + tuple(
            "edfoutloc out_tpl".split()
        ) + tuple (
            "raw med thresh label marked"
        )

    def __init__(self, **kw):
        super().__init__(**kw)

    def setup_ecks(self):
        self.edfoutloc.make_ecks(self.out_tpl, self.out_keys)
        


class AlignA01Params(StaticAttKey):

    __slots__ = StaticAttKey.__slots__ + tuple(
            "".split()
        )

    def __init__(self, **kw):
        super().__init__(**kw)


class AlignA01(object):

    def __init__(self, moif):




########################
########################
########################
########################
########################



        out_tpl = '%s_%04d_%s_{}.edf' % (
            self.out_tag, scanno, counter_name)
def parse_pillar_map_v2(map_arr, threshold, sum_thresh, medflt_size,
    reflength, outfxpath_tpl):

    # OUT
    keys = "raw med thresh anno label marked".split()
    ecks = EDFClosedKeySet_RW(outfxpath_tpl, keys)

    

    s0, s1 = map_arr.shape
    md_arr = ndi.median_filter(map_arr, medflt_size)
    th_arr = np.where(md_arr > threshold, 1, 0)
    anno_arr = th_arr.copy()
    (label_arr, nfeatures) = ndi.label(th_arr)
    objects = ndi.find_objects(label_arr)
    n_obj = len(objects)
    print (n_obj, nfeatures)
    pprint(objects)
    
    ecks['raw'] = map_arr
    ecks['med'] = md_arr
    ecks['label'] = label_arr
    ecks['thresh'] = th_arr
    if n_obj == 1:
        arr = map_arr.copy()
        sl0, sl1 = objects[0]
        a0 = sl0.start
        b0 = sl0.stop-1
        a1 = sl1.start
        b1 = sl1.stop-1
        arr[a0,a1:b1] = 1000
        arr[b0,a1:b1] = 1000
        arr[a0:b0,a1] = 1000
        arr[a0:b0,b1] = 1000
        ecks['marked'] = arr
    return objects

class Pillar2(object):

    def __init__(self, zthresh=5, nwidth_thresh=4, reflength=None,
        medflt_size=7, out_tag='somedefault', counter_dict):
        self.counter_dict = counter_dict
        self.convctr_dict = Odict((v,k) for (k,v) in counter_dict]
        self.counter_names = [counter_dict[k] for k in counter_dict]
        self.zthresh = zthresh
        self.nwidth_thresh = nwidth_thresh
        self.medflt_size = medflt_size
        self.reflength = reflength
        self.out_tag = out_tag

    def retrieve_scaninfo(self):
        scan_info =
        

    def get_ndiobjects

    def seg_parse(self, scanno, res, counter_name):
        out_tpl = '%s_%04d_%s_{}.edf' % (
            self.out_tag, scanno, counter_name)
        map_arr = res[0][counter_name]
        dc = res[1]
        parse_res = parse_pillar_map_v2(map_arr, 
            self.zthresh, self.nwidth_thresh, self.medflt_size, self.reflength, out_tpl)
        return parse_res
