# ihsc1687 29/01/22-30/01/22 eh3
# pcl bottlebrush from IDA

def do_meshes():
    fshtrigger()
    rstp('small')
    dkmapyz_2(0, 2.5, 500, 0, 2.5, 500, 0.01, retveloc=5)
     
    newdataset('medium')
    rstp('medium')
    dkmapyz_2(0, 2.5, 500, 0, 2.5, 500, 0.01, retveloc=5)

    newdataset('large')
    rstp('large')
    dkmapyz_2(0, 2.5, 500, 0, 2.5, 500, 0.01, retveloc=5)

    sc()
    sc()


print(SCAN_SAVING.data_path)
