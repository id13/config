print('es1403_calib - load 1')
START_KEY = 'a'

def calib_scan():
    loff_kmap(nnp2, -40,40, 80, nnp3, -40,40, 80, 0.01)

def es1403_setupcalib(ndetx_pos):
    s_pos = f'{ndetx_pos:8.3f}'
    s_pos = s_pos.replace('-','m')
    s_pos = s_pos.replace('.','p')
    s_pos = s_pos.replace(' ','')
    print(ndetx_pos)
    print(s_pos)
    umv(ndetx, ndetx_pos)
    newdataset(f'calib_{s_pos}_{START_KEY}')

def es1403_calib():

    for ndetx_pos in np.linspace(-340.0, -300.0, 5):
        es1403_setupcalib(ndetx_pos)
        calib_scan()

    for ndetx_pos in np.linspace(-280.0, -240.0, 3):
        es1403_setupcalib(ndetx_pos)
        calib_scan()

def es1403_calib_main():
    try:
        so()
        es1403_calib()
    finally:
        sc()
        sc()
        sc()
