import sys
import h5py
from os import path

MESHARGS_TYPES = (str, float, float, int, str, float,float, int, float)

def parse_title_akmap_lut(sctitle):
    s = sctitle.strip()
    wa = s.split('(')
    wb = wa[1].split(')')
    wc = wb[0].split(',')
    we = tuple(x.strip() for x in wc)
    
    (mn1,ll1,ul1,n1,mn2,ll2,ul2,n2,mexpt) = tuple(tp(val) for tp,val in zip(MESHARGS_TYPES, we))
    limits = ((ll1,ul1), (ll2,ul2))
    dims = (n1,n2)
    motnames =(mn1, mn2)
    return dict(stype='akamp_lut', dims=dims, limits=limits, motnames=motnames)


#def karl_tilman_init():
#    sys.path.insert(0, '/data/id13/inhouse3/Manfred/SW/kwolek/SRC/UVZ3/REANIM/mcbeco1/episto/lib')

def get_kmap_scaninfo(fapth):
    pass


def get_local_data_dapth(collection_name, dsname):
    scs = SCAN_SAVING
    dir_components = dict(
        proposal_dirname = scs.proposal_dirname,
        beamline = scs.beamline,
        proposal_session_name = scs.proposal_session_name,
        collection_name = collection_name,
        dataset_name = dsname
    )
    dapth_part = scs.template.format(**dir_components)
    return path.join(scs.base_path, dapth_part)

def get_local_data_fupth(collection_name, dsname, ext='h5'):
    scs = SCAN_SAVING
    fn_components = dict(
        collection_name = collection_name,
        dataset_name = dsname
    )
    fn_part = scs.data_filename.format(**fn_components)
    return '.'.join((fn_part, ext))
    
def get_local_data_fapth(collection_name, dsname, ext='h5'):
    dapth = get_local_data_dapth(collection_name, dsname)
    fupth = get_local_data_fupth(collection_name, dsname)
    fapth = path.join(dapth, fupth)
    return fapth

def open_h5_readonly(fpth):
    fraw = open(fpth, 'rb')
    fh5  = h5py.File(fpth, 'r')
    return fh5

def _test1():
    sctitle = 'akmap_lut( ustry, 0.37050000000000266, 0.5205000000000026, 76, ustrz, -0.20440599999999998, 0.01559400000000001, 111, 0.005 )'
    print(parse_title_akmap_lut(sctitle))


if __name__ == '__main__':
    _test1()
