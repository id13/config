print('sc5122_fine_ver1cb823.py load 2')

from math import *

def shortmesh(theta,lsr,hsr):
  
  theta_rad = theta * pi/180 
  wf = cos(fabs(theta_rad))
  
  c_lsr = lsr * wf
  c_hsr = hsr * wf
  
  start = lsr - c_lsr
  end = start + c_lsr + c_hsr
  
  images = 200
  
  exptime = 0.01 * wf
  
  print('scanning from %f to %f with %f exptime' %(start,end,exptime))
  dkmapyz_2(start,end,images-1,0,0.05,10,exptime,frames_per_file=200, retveloc=0.5)




  
def theta_scan(bunch,lsr,hsr):

  theta = 0
  umv(usrotz,theta)
  print('fine_bb0%s_th%02d' %(bunch,theta))
  newdataset('fine_bb0%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  

  
  umv(usrotz,0)

  
def sample_mesh(startcoord,lsr,hsr,steps):
  
  try:
    so()
    eigerhws(True)
    mgeig()
    rstp(startcoord)

    #print('partially scanning bunch 9')
    #partial_theta_scan(57,lsr,hsr)
    #umvr(ustrz,0.275)

    for i in range(steps):
      print('scanning bunch %i' %i)
      theta_scan(i,lsr,hsr)
      umvr(ustrz,0.055)   #0.22 would skips the actual last line because we start to count from 0
    enddataset()
    sc()
        
  finally:
    sc()
    sc()
    sc()
  
  
  
