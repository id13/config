def patch1_t50_3():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False,
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_t50_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_t50_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_t50_3():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_t50_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_t50_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch3_t50_3():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch3_t50_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch3_t50_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch4_t50_3():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch4_t50_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch4_t50_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch1_t50_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_t50_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_t50_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_t50_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_t50_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_t50_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch3_t50_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch3_t50_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch3_t50_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch4_t50_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch4_t50_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch4_t50_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch1_t50_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_t50_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_t50_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_t50_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_t50_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_t50_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch3_t50_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch3_t50_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch3_t50_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch4_t50_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch4_t50_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch4_t50_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch1_t50_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_t50_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_t50_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_t50_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_t50_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_t50_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch3_t50_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch3_t50_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch3_t50_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch4_t50_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch4_t50_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch4_t50_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch_t61_1():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=3    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t61_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t61_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch_t54_1_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_t54_1_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t54_1_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def mount5_main():
    clear_abort()
    so()
    try:
        patch1_t50_3()
        patch2_t50_3()
        patch3_t50_3()
        patch4_t50_3()
        
        patch1_t50_2()
        patch2_t50_2()
        patch3_t50_2()
        patch4_t50_2()

        patch1_t50_4()
        patch2_t50_4()
        patch3_t50_4()
        patch4_t50_4()

        patch1_t50_1()
        patch2_t50_1()
        patch3_t50_1()
        patch4_t50_1()

        patch_t61_1()
        patch_t54_1_2()
        
    finally:
        sc()
        sc()
        sc()



