def patch_U_ref_5():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=7    ,  nj=6    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_U_ref_5_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_U_ref_5'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)



def patch1_U_ref_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_U_ref_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_U_ref_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_U_ref_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_U_ref_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_U_ref_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch3_U_ref_4():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch3_U_ref_4_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch3_U_ref_4'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch_U_ref_3():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch_U_ref_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_U_ref_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch1_U_ref_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch1_U_ref_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch1_U_ref_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch2_U_ref_2():

    #clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=3    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'patch2_U_ref_2_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch2_U_ref_2'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def mount6_main():
    clear_abort()
    so()
    try:
        patch_U_ref_5()
        
        patch1_U_ref_4()
        patch2_U_ref_4()
        patch3_U_ref_4()

        patch_U_ref_3()

        patch1_U_ref_2()
        patch2_U_ref_2()        
    finally:
        sc()
        sc()
        sc()



