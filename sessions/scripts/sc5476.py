print("sc5476.mac v11")

import gevent,bliss
prefix = 'damage'
key    = 'n1'

datset_list = [
'roi79763_pos1',
'roi79762_pos1',
'roi79761_pos1',
#'roi79653_pos1',
#'roi79650_pos1',
#'roi79652_pos1',
#'roi79646_pos1',
#'roi79647_pos1',
#'roi79604_pos1',
#'roi79606_pos1_new',
#'roi79603_pos1',
#'continue_pos6',
#'roi79598_pos1',
#'roi79599_pos1',
#'roi79523_pos1',
#'roi79530_pos1',
#'roi79532_pos1',
#'roi79482_pos1',
#'roi79323_pos1',
#'roi79322_pos1',
#'roi79356_pos1',
#'roi79321_pos1',
]


step_list  = [
[23,18],
[8,19],
[11,19],
#[20,20],
#[20,16], 
#[27,6],
#[4,12],
#[4,6],
#[3,9],
#[4,10],
#[20,15],
#[20,7],
#[14,12],
#[6,12],
#[4,14],
#[22,12],
#[18,18],
#[40,30],
#[38,30],
#[13,19],
#[7,23],
#[44,28],
]

sample_list = [
'callus_6Lb',
'callus_4Lb',
'callus_4Lb',
#'callus_7L',
#'callus_8Lb',
#'callus_8Lb',
#'cortex_5R',
#'cortex_5R',
#'cortex_1R',
#'cortex_1R',
#'callus_1L',
#'callus_8L',
#'callus_8L',
#'cortex_6R',
#'callus_4L',
#'callus_6L',
#'tensile',
#'callus_2L',
#'cortex_4R',
#'cortex_4R',
#'callus_3L',
]

def damage100(datset_list,setp_list,sample_list):
    so()
    dqmgeigx()
    fshtrigger()
    
    for _ in range(len(sample_list)):
        gopos(datset_list[_])
        newsample(sample_list[_])
        datset_name = f'{prefix}_{datset_list[_]}_{key}'
        #print(datset_name,step_list[_][1],step_list[_][0])
        newdataset(datset_name)
        y_start = ustry.position
        #print(y_start)
        
        for row in range(0,step_list[_][1]): #py/10
            umv(ustry,y_start)
            #for line in range(0,step_list[_][0]): #px/10
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05)
            for line in range(0,step_list[_][0]):
                #do_timeseries_run_new(100,0.02)
                #try:
                #    with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.1) 
                #except bliss.common.greenlet_utils.killmask.BlissTimeout:
                #    print('caughting timeout')
            umvr(ustrz,0.1)

def damage100_continue(num,origin_pos,continue_pos,step,sample):
    so()
    dqmgeigx()
    fshtrigger()
    line_start = int(num%step[0])
    row_start  = int(num/step[0])
    gopos(origin_pos)
    newsample(sample)
    datset_name = f'{prefix}_{origin_pos}_conitnue_{key}'
    #print(datset_name,step_list[_][1],step_list[_][0])
    newdataset(datset_name)
    y_start = ustry.position
    #print(y_start)
    gopos(continue_pos)
    
    i = 0
    #print(line_start,row_start)
    for row in range(row_start,step[1]):
        if i == 0:
            #for line in range(line_start,step[0]):
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05) 
            for line in range(line_start,step[0]):
                #do_timeseries_run_new(100,0.02)
            #    try:
            #        with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.1) 
             #   except bliss.common.greenlet_utils.killmask.BlissTimeout:
             #       print('caughting timeout')
        else:
            umv(ustry,y_start)
            #for line in range(0,step[0]):
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05)
            for line in range(0,step[0]):
                #do_timeseries_run_new(100,0.02)
            #    try:
            #        with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.1) 
             #   except bliss.common.greenlet_utils.killmask.BlissTimeout:
             #       print('caughting timeout')
        i = 1
        umvr(ustrz,0.1)

def damage50(datset_list,setp_list,sample_list):
    so()
    dqmgeigx()
    fshtrigger()
    
    for _ in range(len(sample_list)):
        gopos(datset_list[_])
        newsample(sample_list[_])
        datset_name = f'{prefix}_{datset_list[_]}_{key}'
        #print(datset_name,step_list[_][1],step_list[_][0])
        newdataset(datset_name)
        y_start = ustry.position
        #print(y_start)
        
        for row in range(0,step_list[_][1]): #py/10
            umv(ustry,y_start)
            #for line in range(0,step_list[_][0]): #px/10
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05)
            for line in range(0,step_list[_][0]):
                #do_timeseries_run_new(100,0.02)
                #try:
                #    with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.05) 
                #except bliss.common.greenlet_utils.killmask.BlissTimeout:
                #    print('caughting timeout')
            umvr(ustrz,0.05)


def damage50_continue(num,origin_pos,continue_pos,step,sample):
    so()
    dqmgeigx()
    fshtrigger()
    line_start = int(num%step[0])
    row_start  = int(num/step[0])
    gopos(origin_pos)
    newsample(sample)
    datset_name = f'{prefix}_{origin_pos}_conitnue_{key}'
    #print(datset_name,step_list[_][1],step_list[_][0])
    newdataset(datset_name)
    y_start = ustry.position
    #print(y_start)
    gopos(continue_pos)
    
    i = 0
    #print(line_start,row_start)
    for row in range(row_start,step[1]):
        if i == 0:
            #for line in range(line_start,step[0]):
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05) 
            for line in range(line_start,step[0]):
                #do_timeseries_run_new(100,0.02)
            #    try:
            #        with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.05) 
             #   except bliss.common.greenlet_utils.killmask.BlissTimeout:
             #       print('caughting timeout')
        else:
            umv(ustry,y_start)
            #for line in range(0,step[0]):
            #    #do_timeseries_run_new(100,0.02)
            #    loopscan(50,0.04)
            #    sleep(0.1)
            #    umvr(ustry,0.05)
            for line in range(0,step[0]):
                #do_timeseries_run_new(100,0.02)
            #    try:
            #        with gevent.Timeout(seconds=10):
                        loopscan(50,0.04,sleep_time=0.005)
                        sleep(0.2)
                        umvr(ustry,0.05) 
             #   except bliss.common.greenlet_utils.killmask.BlissTimeout:
             #       print('caughting timeout')
        i = 1
        umvr(ustrz,0.05)
    
def damage50_continue_roi79321_patch(num,origin_pos,continue_pos,step,sample):
    so()
    dqmgeigx()
    fshtrigger()
    line_start = int(num%step[0])
    row_start  = int(num/step[0])
    gopos(origin_pos)
    newsample(sample)
    datset_name = f'{prefix}_{origin_pos}_conitnue_{key}'
    #print(datset_name,step_list[_][1],step_list[_][0])
    newdataset(datset_name)
    y_start = ustry.position
    #print(y_start)
    gopos(continue_pos)
    
    #print(line_start,row_start)
    for row in range(row_start,step[1]):
        umv(ustry,y_start+(0.05*line_start))
        for line in range(0,step[0]-line_start):
            loopscan(50,0.04,sleep_time=0.005)
            sleep(0.2)
            umvr(ustry,0.05) 
        umvr(ustrz,0.05)


def test_Timeout():
    slp_tm = [2,5,2,10,3]
    for _ in range(5):
        try:
            with gevent.Timeout(seconds=4):
                sleep(slp_tm[_])
                print(slp_tm[_])
        except: 
            bliss.common.greenlet_utils.killmask.BlissTimeout

        
def run_damage_loopscan_new():
    #print(len(datset_list),len(step_list),len(sample_list))
    #damage50_continue(64,'roi79321_pos1','continue_pos3',[44,28],'callus_3L')
    #damage50_continue_roi79321_patch(64,'roi79321_pos1','continue_pos3',[44,28],'callus_3L')
    #damage50(datset_list,step_list,sample_list)#April 6 change to 100 um step size
    #damage100_continue(47+51+43,'roi79650_pos1','continue_pos9',[20,16],'callus_8Lb')
    damage100(datset_list,step_list,sample_list)
    
def run_damage_loopscan_continue():
    damage50_continue(174,'roi79603_pos1','continue_pos6',[0,15],'callus_1L')
