print('load sc5252_grid 47')
START_KEY = 'a'
START_KEY_POS = f'p{START_KEY}'
def dkmapyz_2(ll2,ul2,n2,ll3,ul3,n3,expt):
    print(ll2,ul2,n2,ll3,ul3,n3,expt)
    kmap.dkmap(nnp2,ll2,ul2,n2,nnp3,ll3,ul3,n3,expt)

FULL_PARAM_SETS_centered = '''
set2b -50 50 400 -50 50 200 0.01
set3b -50 50 400 -50 50 200 0.005
set5b -50 50 500 -45 45 300 0.005
set6b -50 50 500 -50 50 250 0.003
set7b -50 50 500 -45 45 300 0.003
'''

FULL_PARAM_SETS_centered_iso = '''
set3ci -50 50 400 -50 50 400 0.005
set5ci -50 50 500 -50 50 500 0.005
set7ci -50 50 500 -50 50 500 0.003
set9ci -50 50 400 -50 50 400 0.003
'''

FULL_PARAM_SETS_centered_iso_half = '''
set3cih -25 25 200 -50 50 400 0.005
set5cih -25 25 250 -50 50 500 0.005
set7cih -25 25 250 -50 50 500 0.003
set9cih -25 25 200 -50 50 400 0.003
'''

FULL_PARAM_SETS_gridstyle = '''
set2b 0 100 400 0 100 200 0.01
set3b 0 100 400 0 100 200 0.005
set5b 0 100 500 0 90 300 0.005
set6b 0 100 500 0 100 250 0.003
set7b 0 100 500 0 90 300 0.003
'''

FULL_PARAM_SETS_gridstyle_iso = '''
set3i 0 100 400 0 100 400 0.005
set5i 0 100 500 0 100 500 0.005
set7i 0 100 500 0 100 500 0.003
set9i 0 100 400 0 100 400 0.003
set10i 0 100 200 0 100 200 0.003
set11i 0 100 200 0 100 200 0.005
'''

PARAM_SETS_manual = '''
set18s 0 35 350 0 35 350 0.003
set19s 0 35 350 0 35 350 0.005
set20s 0 35 350 0 35 350 0.01
set21s 0 35 500 0 35 240 0.003
set22s 0 35 500 0 35 240 0.005
set23s 0 35 500 0 35 240 0.01
'''

#put in here your parameter set of choice, please
PARAM_SETS = PARAM_SETS_manual

#put in positions AND sets  manually if not via grid
POSITIONS = '''
gridpos_yz_pa_0000_00_00_0_sent set18s
gridpos_yz_pa_0001_01_00_1_sent set19s
gridpos_yz_pa_0002_02_00_2_sent set20s
gridpos_yz_pa_0003_03_00_3_sent set21s
gridpos_yz_pa_0004_04_00_4_sent set22s
gridpos_yz_pa_0005_05_00_5_sent set23s
gridpos_yz_pa_0006_06_00_6_sent set18s
gridpos_yz_pa_0007_07_00_7_sent set19s
gridpos_yz_pa_0008_08_00_8_sent set20s
gridpos_yz_pa_0009_09_00_9_sent set21s
gridpos_yz_pa_0010_10_00_10_sent set22s
gridpos_yz_pa_0011_11_00_11_sent set23s
gridpos_yz_pa_0012_12_00_12_sent set18s
gridpos_yz_pa_0013_13_00_13_sent set19s
gridpos_yz_pa_0014_14_00_14_sent set20s
gridpos_yz_pa_0015_15_00_15_sent set21s
gridpos_yz_pa_0016_16_00_16_sent set22s
gridpos_yz_pa_0017_17_00_17_sent set23s
gridpos_yz_pa_0018_18_00_18_sent set18s
gridpos_yz_pa_0019_19_00_19_sent set19s
gridpos_yz_pa_0020_00_01_20_sent set20s
gridpos_yz_pa_0021_01_01_21_sent set21s
gridpos_yz_pa_0022_02_01_22_sent set22s
gridpos_yz_pa_0023_03_01_23_sent set23s
gridpos_yz_pa_0024_04_01_24_sent set18s
gridpos_yz_pa_0025_05_01_25_sent set19s
gridpos_yz_pa_0026_06_01_26_sent set20s
gridpos_yz_pa_0027_07_01_27_sent set21s
gridpos_yz_pa_0028_08_01_28_sent set22s
gridpos_yz_pa_0029_09_01_29_sent set23s
gridpos_yz_pa_0030_10_01_30_sent set18s
gridpos_yz_pa_0031_11_01_31_sent set19s
gridpos_yz_pa_0032_12_01_32_sent set20s
gridpos_yz_pa_0033_13_01_33_sent set21s
gridpos_yz_pa_0034_14_01_34_sent set22s
gridpos_yz_pa_0035_15_01_35_sent set23s
gridpos_yz_pa_0036_16_01_36_sent set18s
gridpos_yz_pa_0037_17_01_37_sent set19s
gridpos_yz_pa_0038_18_01_38_sent set20s
gridpos_yz_pa_0039_19_01_39_sent set21s
gridpos_yz_pa_0040_00_02_40_sent set22s
gridpos_yz_pa_0041_01_02_41_sent set23s
gridpos_yz_pa_0042_02_02_42_sent set18s
gridpos_yz_pa_0043_03_02_43_sent set19s
gridpos_yz_pa_0044_04_02_44_sent set20s
gridpos_yz_pa_0045_05_02_45_sent set21s
gridpos_yz_pa_0046_06_02_46_sent set22s
gridpos_yz_pa_0047_07_02_47_sent set23s
gridpos_yz_pa_0048_08_02_48_sent set18s
gridpos_yz_pa_0049_09_02_49_sent set19s
gridpos_yz_pa_0050_10_02_50_sent set20s
gridpos_yz_pa_0051_11_02_51_sent set21s
gridpos_yz_pa_0052_12_02_52_sent set22s
gridpos_yz_pa_0053_13_02_53_sent set23s
gridpos_yz_pa_0054_14_02_54_sent set18s
gridpos_yz_pa_0055_15_02_55_sent set19s
gridpos_yz_pa_0056_16_02_56_sent set20s
gridpos_yz_pa_0057_17_02_57_sent set21s
gridpos_yz_pa_0058_18_02_58_sent set22s
gridpos_yz_pa_0059_19_02_59_sent set23s
gridpos_yz_pa_0060_00_03_60_sent set18s
gridpos_yz_pa_0061_01_03_61_sent set19s
gridpos_yz_pa_0062_02_03_62_sent set20s
gridpos_yz_pa_0063_03_03_63_sent set21s
gridpos_yz_pa_0064_04_03_64_sent set22s
gridpos_yz_pa_0065_05_03_65_sent set23s
gridpos_yz_pa_0066_06_03_66_sent set18s
gridpos_yz_pa_0067_07_03_67_sent set19s
gridpos_yz_pa_0068_08_03_68_sent set20s
gridpos_yz_pa_0069_09_03_69_sent set21s
gridpos_yz_pa_0070_10_03_70_sent set22s
gridpos_yz_pa_0071_11_03_71_sent set23s
gridpos_yz_pa_0072_12_03_72_sent set18s
gridpos_yz_pa_0073_13_03_73_sent set19s
gridpos_yz_pa_0074_14_03_74_sent set20s
gridpos_yz_pa_0075_15_03_75_sent set21s
gridpos_yz_pa_0076_16_03_76_sent set22s
gridpos_yz_pa_0077_17_03_77_sent set23s
gridpos_yz_pa_0078_18_03_78_sent set18s
gridpos_yz_pa_0079_19_03_79_sent set19s
gridpos_yz_pa_0080_00_04_80_sent set20s
gridpos_yz_pa_0081_01_04_81_sent set21s
gridpos_yz_pa_0082_02_04_82_sent set22s
gridpos_yz_pa_0083_03_04_83_sent set23s
gridpos_yz_pa_0084_04_04_84_sent set18s
gridpos_yz_pa_0085_05_04_85_sent set19s
gridpos_yz_pa_0086_06_04_86_sent set20s
gridpos_yz_pa_0087_07_04_87_sent set21s
gridpos_yz_pa_0088_08_04_88_sent set22s
gridpos_yz_pa_0089_09_04_89_sent set23s
gridpos_yz_pa_0090_10_04_90_sent set18s
gridpos_yz_pa_0091_11_04_91_sent set19s
gridpos_yz_pa_0092_12_04_92_sent set20s
gridpos_yz_pa_0093_13_04_93_sent set21s
gridpos_yz_pa_0094_14_04_94_sent set22s
gridpos_yz_pa_0095_15_04_95_sent set23s
gridpos_yz_pa_0096_16_04_96_sent set18s
gridpos_yz_pa_0097_17_04_97_sent set19s
gridpos_yz_pa_0098_18_04_98_sent set20s
gridpos_yz_pa_0099_19_04_99_sent set21s
gridpos_yz_pa_0100_00_05_100_sent set22s
gridpos_yz_pa_0101_01_05_101_sent set23s
gridpos_yz_pa_0102_02_05_102_sent set18s
gridpos_yz_pa_0103_03_05_103_sent set19s
gridpos_yz_pa_0104_04_05_104_sent set20s
gridpos_yz_pa_0105_05_05_105_sent set21s
gridpos_yz_pa_0106_06_05_106_sent set22s
gridpos_yz_pa_0107_07_05_107_sent set23s
gridpos_yz_pa_0108_08_05_108_sent set18s
gridpos_yz_pa_0109_09_05_109_sent set19s
gridpos_yz_pa_0110_10_05_110_sent set20s
gridpos_yz_pa_0111_11_05_111_sent set21s
gridpos_yz_pa_0112_12_05_112_sent set22s
gridpos_yz_pa_0113_13_05_113_sent set23s
gridpos_yz_pa_0114_14_05_114_sent set18s
gridpos_yz_pa_0115_15_05_115_sent set19s
gridpos_yz_pa_0116_16_05_116_sent set20s
gridpos_yz_pa_0117_17_05_117_sent set21s
gridpos_yz_pa_0118_18_05_118_sent set22s
gridpos_yz_pa_0119_19_05_119_sent set23s
gridpos_yz_pa_0120_00_06_120_sent set18s
gridpos_yz_pa_0121_01_06_121_sent set19s
gridpos_yz_pa_0122_02_06_122_sent set20s
gridpos_yz_pa_0123_03_06_123_sent set21s
gridpos_yz_pa_0124_04_06_124_sent set22s
gridpos_yz_pa_0125_05_06_125_sent set23s
gridpos_yz_pa_0126_06_06_126_sent set18s
gridpos_yz_pa_0127_07_06_127_sent set19s
gridpos_yz_pa_0128_08_06_128_sent set20s
gridpos_yz_pa_0129_09_06_129_sent set21s
gridpos_yz_pa_0130_10_06_130_sent set22s
gridpos_yz_pa_0131_11_06_131_sent set23s
gridpos_yz_pa_0132_12_06_132_sent set18s
gridpos_yz_pa_0133_13_06_133_sent set19s
gridpos_yz_pa_0134_14_06_134_sent set20s
gridpos_yz_pa_0135_15_06_135_sent set21s
gridpos_yz_pa_0136_16_06_136_sent set22s
gridpos_yz_pa_0137_17_06_137_sent set23s
gridpos_yz_pa_0138_18_06_138_sent set18s
gridpos_yz_pa_0139_19_06_139_sent set19s
gridpos_yz_pa_0140_00_07_140_sent set20s
gridpos_yz_pa_0141_01_07_141_sent set21s
gridpos_yz_pa_0142_02_07_142_sent set22s
gridpos_yz_pa_0143_03_07_143_sent set23s
gridpos_yz_pa_0144_04_07_144_sent set18s
gridpos_yz_pa_0145_05_07_145_sent set19s
gridpos_yz_pa_0146_06_07_146_sent set20s
gridpos_yz_pa_0147_07_07_147_sent set21s
gridpos_yz_pa_0148_08_07_148_sent set22s
gridpos_yz_pa_0149_09_07_149_sent set23s
gridpos_yz_pa_0150_10_07_150_sent set18s
gridpos_yz_pa_0151_11_07_151_sent set19s
gridpos_yz_pa_0152_12_07_152_sent set20s
gridpos_yz_pa_0153_13_07_153_sent set21s
gridpos_yz_pa_0154_14_07_154_sent set22s
gridpos_yz_pa_0155_15_07_155_sent set23s
gridpos_yz_pa_0156_16_07_156_sent set18s
gridpos_yz_pa_0157_17_07_157_sent set19s
gridpos_yz_pa_0158_18_07_158_sent set20s
gridpos_yz_pa_0159_19_07_159_sent set21s
gridpos_yz_pa_0160_00_08_160_sent set22s
gridpos_yz_pa_0161_01_08_161_sent set23s
gridpos_yz_pa_0162_02_08_162_sent set18s
gridpos_yz_pa_0163_03_08_163_sent set19s
gridpos_yz_pa_0164_04_08_164_sent set20s
gridpos_yz_pa_0165_05_08_165_sent set21s
gridpos_yz_pa_0166_06_08_166_sent set22s
gridpos_yz_pa_0167_07_08_167_sent set23s
gridpos_yz_pa_0168_08_08_168_sent set18s
gridpos_yz_pa_0169_09_08_169_sent set19s
gridpos_yz_pa_0170_10_08_170_sent set20s
gridpos_yz_pa_0171_11_08_171_sent set21s
gridpos_yz_pa_0172_12_08_172_sent set22s
gridpos_yz_pa_0173_13_08_173_sent set23s
gridpos_yz_pa_0174_14_08_174_sent set18s
gridpos_yz_pa_0175_15_08_175_sent set19s
gridpos_yz_pa_0176_16_08_176_sent set20s
gridpos_yz_pa_0177_17_08_177_sent set21s
gridpos_yz_pa_0178_18_08_178_sent set22s
gridpos_yz_pa_0179_19_08_179_sent set23s
gridpos_yz_pa_0180_00_09_180_sent set18s
gridpos_yz_pa_0181_01_09_181_sent set19s
gridpos_yz_pa_0182_02_09_182_sent set20s
gridpos_yz_pa_0183_03_09_183_sent set21s
gridpos_yz_pa_0184_04_09_184_sent set22s
gridpos_yz_pa_0185_05_09_185_sent set23s
gridpos_yz_pa_0186_06_09_186_sent set18s
gridpos_yz_pa_0187_07_09_187_sent set19s
gridpos_yz_pa_0188_08_09_188_sent set20s
gridpos_yz_pa_0189_09_09_189_sent set21s
gridpos_yz_pa_0190_10_09_190_sent set22s
gridpos_yz_pa_0191_11_09_191_sent set23s
gridpos_yz_pa_0192_12_09_192_sent set18s
gridpos_yz_pa_0193_13_09_193_sent set19s
gridpos_yz_pa_0194_14_09_194_sent set20s
gridpos_yz_pa_0195_15_09_195_sent set21s
gridpos_yz_pa_0196_16_09_196_sent set22s
gridpos_yz_pa_0197_17_09_197_sent set23s
gridpos_yz_pa_0198_18_09_198_sent set18s
gridpos_yz_pa_0199_19_09_199_sent set19s
gridpos_yz_pa_0200_00_10_200_sent set20s
gridpos_yz_pa_0201_01_10_201_sent set21s
gridpos_yz_pa_0202_02_10_202_sent set22s
gridpos_yz_pa_0203_03_10_203_sent set23s
gridpos_yz_pa_0204_04_10_204_sent set18s
gridpos_yz_pa_0205_05_10_205_sent set19s
gridpos_yz_pa_0206_06_10_206_sent set20s
gridpos_yz_pa_0207_07_10_207_sent set21s
gridpos_yz_pa_0208_08_10_208_sent set22s
gridpos_yz_pa_0209_09_10_209_sent set23s
gridpos_yz_pa_0210_10_10_210_sent set18s
gridpos_yz_pa_0211_11_10_211_sent set19s
gridpos_yz_pa_0212_12_10_212_sent set20s
gridpos_yz_pa_0213_13_10_213_sent set21s
gridpos_yz_pa_0214_14_10_214_sent set22s
gridpos_yz_pa_0215_15_10_215_sent set23s
gridpos_yz_pa_0216_16_10_216_sent set18s
gridpos_yz_pa_0217_17_10_217_sent set19s
gridpos_yz_pa_0218_18_10_218_sent set20s
gridpos_yz_pa_0219_19_10_219_sent set21s
gridpos_yz_pa_0220_00_11_220_sent set22s
gridpos_yz_pa_0221_01_11_221_sent set23s
gridpos_yz_pa_0222_02_11_222_sent set18s
gridpos_yz_pa_0223_03_11_223_sent set19s
gridpos_yz_pa_0224_04_11_224_sent set20s
gridpos_yz_pa_0225_05_11_225_sent set21s
gridpos_yz_pa_0226_06_11_226_sent set22s
gridpos_yz_pa_0227_07_11_227_sent set23s
gridpos_yz_pa_0228_08_11_228_sent set18s
gridpos_yz_pa_0229_09_11_229_sent set19s
gridpos_yz_pa_0230_10_11_230_sent set20s
gridpos_yz_pa_0231_11_11_231_sent set21s
gridpos_yz_pa_0232_12_11_232_sent set22s
gridpos_yz_pa_0233_13_11_233_sent set23s
gridpos_yz_pa_0234_14_11_234_sent set18s
gridpos_yz_pa_0235_15_11_235_sent set19s
gridpos_yz_pa_0236_16_11_236_sent set20s
gridpos_yz_pa_0237_17_11_237_sent set21s
gridpos_yz_pa_0238_18_11_238_sent set22s
gridpos_yz_pa_0239_19_11_239_sent set23s
gridpos_yz_pa_0240_00_12_240_sent set18s
gridpos_yz_pa_0241_01_12_241_sent set19s
gridpos_yz_pa_0242_02_12_242_sent set20s
gridpos_yz_pa_0243_03_12_243_sent set21s
gridpos_yz_pa_0244_04_12_244_sent set22s
gridpos_yz_pa_0245_05_12_245_sent set23s
gridpos_yz_pa_0246_06_12_246_sent set18s
gridpos_yz_pa_0247_07_12_247_sent set19s
gridpos_yz_pa_0248_08_12_248_sent set20s
gridpos_yz_pa_0249_09_12_249_sent set21s
gridpos_yz_pa_0250_10_12_250_sent set22s
gridpos_yz_pa_0251_11_12_251_sent set23s
gridpos_yz_pa_0252_12_12_252_sent set18s
gridpos_yz_pa_0253_13_12_253_sent set19s
gridpos_yz_pa_0254_14_12_254_sent set20s
gridpos_yz_pa_0255_15_12_255_sent set21s
gridpos_yz_pa_0256_16_12_256_sent set22s
gridpos_yz_pa_0257_17_12_257_sent set23s
gridpos_yz_pa_0258_18_12_258_sent set18s
gridpos_yz_pa_0259_19_12_259_sent set19s
gridpos_yz_pa_0260_00_13_260_sent set20s
gridpos_yz_pa_0261_01_13_261_sent set21s
gridpos_yz_pa_0262_02_13_262_sent set22s
gridpos_yz_pa_0263_03_13_263_sent set23s
gridpos_yz_pa_0264_04_13_264_sent set18s
gridpos_yz_pa_0265_05_13_265_sent set19s
gridpos_yz_pa_0266_06_13_266_sent set20s
gridpos_yz_pa_0267_07_13_267_sent set21s
gridpos_yz_pa_0268_08_13_268_sent set22s
gridpos_yz_pa_0269_09_13_269_sent set23s
gridpos_yz_pa_0270_10_13_270_sent set18s
gridpos_yz_pa_0271_11_13_271_sent set19s
gridpos_yz_pa_0272_12_13_272_sent set20s
gridpos_yz_pa_0273_13_13_273_sent set21s
gridpos_yz_pa_0274_14_13_274_sent set22s
gridpos_yz_pa_0275_15_13_275_sent set23s
gridpos_yz_pa_0276_16_13_276_sent set18s
gridpos_yz_pa_0277_17_13_277_sent set19s
gridpos_yz_pa_0278_18_13_278_sent set20s
gridpos_yz_pa_0279_19_13_279_sent set21s
gridpos_yz_pa_0280_00_14_280_sent set22s
gridpos_yz_pa_0281_01_14_281_sent set23s
gridpos_yz_pa_0282_02_14_282_sent set18s
gridpos_yz_pa_0283_03_14_283_sent set19s
gridpos_yz_pa_0284_04_14_284_sent set20s
gridpos_yz_pa_0285_05_14_285_sent set21s
gridpos_yz_pa_0286_06_14_286_sent set22s
gridpos_yz_pa_0287_07_14_287_sent set23s
gridpos_yz_pa_0288_08_14_288_sent set18s
gridpos_yz_pa_0289_09_14_289_sent set19s
gridpos_yz_pa_0290_10_14_290_sent set20s
gridpos_yz_pa_0291_11_14_291_sent set21s
gridpos_yz_pa_0292_12_14_292_sent set22s
gridpos_yz_pa_0293_13_14_293_sent set23s
gridpos_yz_pa_0294_14_14_294_sent set18s
gridpos_yz_pa_0295_15_14_295_sent set19s
gridpos_yz_pa_0296_16_14_296_sent set20s
gridpos_yz_pa_0297_17_14_297_sent set21s
gridpos_yz_pa_0298_18_14_298_sent set22s
gridpos_yz_pa_0299_19_14_299_sent set23s
gridpos_yz_pa_0300_00_15_300_sent set18s
gridpos_yz_pa_0301_01_15_301_sent set19s
gridpos_yz_pa_0302_02_15_302_sent set20s
gridpos_yz_pa_0303_03_15_303_sent set21s
gridpos_yz_pa_0304_04_15_304_sent set22s
gridpos_yz_pa_0305_05_15_305_sent set23s
gridpos_yz_pa_0306_06_15_306_sent set18s
gridpos_yz_pa_0307_07_15_307_sent set19s
gridpos_yz_pa_0308_08_15_308_sent set20s
gridpos_yz_pa_0309_09_15_309_sent set21s
gridpos_yz_pa_0310_10_15_310_sent set22s
gridpos_yz_pa_0311_11_15_311_sent set23s
gridpos_yz_pa_0312_12_15_312_sent set18s
gridpos_yz_pa_0313_13_15_313_sent set19s
gridpos_yz_pa_0314_14_15_314_sent set20s
gridpos_yz_pa_0315_15_15_315_sent set21s
gridpos_yz_pa_0316_16_15_316_sent set22s
gridpos_yz_pa_0317_17_15_317_sent set23s
gridpos_yz_pa_0318_18_15_318_sent set18s
gridpos_yz_pa_0319_19_15_319_sent set19s
gridpos_yz_pa_0320_00_16_320_sent set20s
gridpos_yz_pa_0321_01_16_321_sent set21s
gridpos_yz_pa_0322_02_16_322_sent set22s
gridpos_yz_pa_0323_03_16_323_sent set23s
gridpos_yz_pa_0324_04_16_324_sent set18s
gridpos_yz_pa_0325_05_16_325_sent set19s
gridpos_yz_pa_0326_06_16_326_sent set20s
gridpos_yz_pa_0327_07_16_327_sent set21s
gridpos_yz_pa_0328_08_16_328_sent set22s
gridpos_yz_pa_0329_09_16_329_sent set23s
gridpos_yz_pa_0330_10_16_330_sent set18s
gridpos_yz_pa_0331_11_16_331_sent set19s
gridpos_yz_pa_0332_12_16_332_sent set20s
gridpos_yz_pa_0333_13_16_333_sent set21s
gridpos_yz_pa_0334_14_16_334_sent set22s
gridpos_yz_pa_0335_15_16_335_sent set23s
gridpos_yz_pa_0336_16_16_336_sent set18s
gridpos_yz_pa_0337_17_16_337_sent set19s
gridpos_yz_pa_0338_18_16_338_sent set20s
gridpos_yz_pa_0339_19_16_339_sent set21s
gridpos_yz_pa_0340_00_17_340_sent set22s
gridpos_yz_pa_0341_01_17_341_sent set23s
gridpos_yz_pa_0342_02_17_342_sent set18s
gridpos_yz_pa_0343_03_17_343_sent set19s
gridpos_yz_pa_0344_04_17_344_sent set20s
gridpos_yz_pa_0345_05_17_345_sent set21s
gridpos_yz_pa_0346_06_17_346_sent set22s
gridpos_yz_pa_0347_07_17_347_sent set23s
gridpos_yz_pa_0348_08_17_348_sent set18s
gridpos_yz_pa_0349_09_17_349_sent set19s
gridpos_yz_pa_0350_10_17_350_sent set20s
gridpos_yz_pa_0351_11_17_351_sent set21s
gridpos_yz_pa_0352_12_17_352_sent set22s
gridpos_yz_pa_0353_13_17_353_sent set23s
gridpos_yz_pa_0354_14_17_354_sent set18s
gridpos_yz_pa_0355_15_17_355_sent set19s
gridpos_yz_pa_0356_16_17_356_sent set20s
gridpos_yz_pa_0357_17_17_357_sent set21s
gridpos_yz_pa_0358_18_17_358_sent set22s
gridpos_yz_pa_0359_19_17_359_sent set23s
gridpos_yz_pa_0360_00_18_360_sent set18s
gridpos_yz_pa_0361_01_18_361_sent set19s
gridpos_yz_pa_0362_02_18_362_sent set20s
gridpos_yz_pa_0363_03_18_363_sent set21s
gridpos_yz_pa_0364_04_18_364_sent set22s
gridpos_yz_pa_0365_05_18_365_sent set23s
gridpos_yz_pa_0366_06_18_366_sent set18s
gridpos_yz_pa_0367_07_18_367_sent set19s
gridpos_yz_pa_0368_08_18_368_sent set20s
gridpos_yz_pa_0369_09_18_369_sent set21s
gridpos_yz_pa_0370_10_18_370_sent set22s
gridpos_yz_pa_0371_11_18_371_sent set23s
gridpos_yz_pa_0372_12_18_372_sent set18s
gridpos_yz_pa_0373_13_18_373_sent set19s
gridpos_yz_pa_0374_14_18_374_sent set20s
gridpos_yz_pa_0375_15_18_375_sent set21s
gridpos_yz_pa_0376_16_18_376_sent set22s
gridpos_yz_pa_0377_17_18_377_sent set23s
gridpos_yz_pa_0378_18_18_378_sent set18s
gridpos_yz_pa_0379_19_18_379_sent set19s
gridpos_yz_pa_0380_00_19_380_sent set20s
gridpos_yz_pa_0381_01_19_381_sent set21s
gridpos_yz_pa_0382_02_19_382_sent set22s
gridpos_yz_pa_0383_03_19_383_sent set23s
gridpos_yz_pa_0384_04_19_384_sent set18s
gridpos_yz_pa_0385_05_19_385_sent set19s
gridpos_yz_pa_0386_06_19_386_sent set20s
gridpos_yz_pa_0387_07_19_387_sent set21s
gridpos_yz_pa_0388_08_19_388_sent set22s
gridpos_yz_pa_0389_09_19_389_sent set23s
gridpos_yz_pa_0390_10_19_390_sent set18s
gridpos_yz_pa_0391_11_19_391_sent set19s
gridpos_yz_pa_0392_12_19_392_sent set20s
gridpos_yz_pa_0393_13_19_393_sent set21s
gridpos_yz_pa_0394_14_19_394_sent set22s
gridpos_yz_pa_0395_15_19_395_sent set23s
gridpos_yz_pa_0396_16_19_396_sent set18s
gridpos_yz_pa_0397_17_19_397_sent set19s
gridpos_yz_pa_0398_18_19_398_sent set20s
gridpos_yz_pa_0399_19_19_399_sent set21s
'''

def make_grid_posname(cellname, cycle, i, j):
    posname = f'gridpos_yz_{START_KEY_POS}_{cycle:04d}_{i:02d}_{j:02d}_{cellname}_sent'
    return posname

#function to call with grid parameters to create grid. WATCH dy, dz in mm!!!
#position sample at top left corner of grid before creating the grid!
def generate_pos_grid(dy, dz, ny, nz):
    curr_y = nny.position
    curr_z = nnz.position
    cycle = 0
    for j in range(nz):
        next_z = curr_z + j*dz
        mv(nnz, next_z)
        for i in range(ny):
            print('='*20)
            next_y = curr_y + i*dy
            print(f'cycle={cycle};next_y={next_y};next_z={next_z};i={i};j={j}')
            mv(nny, next_y)
            cellname = str(cycle)
            posname = make_grid_posname(cellname, cycle, i, j)
            stp(posname)
            cycle += 1

def make_positions_string(ny, nz):
    leng = ny*nz
    paramsets = [x for x in PARAM_SETS.split('\n') if x.strip()]
    paramsets = paramsets*(ny*nz)
    paramsets = [x.split()[0] for x in paramsets if x.strip()]
    posnames = []
    cycle = 0
    for j in range(nz):
        for i in range(ny):
            cellname = str(cycle)
            pn = make_grid_posname(cellname, cycle, i, j)
            posnames.append(pn)
            cycle += 1
    positions_prep = list(zip(posnames, paramsets))
    lines = []
    for ps in positions_prep:
        p,s = ps
        lines.append(f'{p} {s}')
    lines.append('')
    positions = '\n'.join(lines)
    return positions

#function to call after creating grid to get scan positions together with scan settings
#to be copied to POSITIONS 
def print_positions_string(ny, nz):
    print(make_positions_string(ny, nz))

def extract_cellname(posname):
    w = posname.split('_')
    cellname = w[-2]
    return cellname

#function to run the scans
def sc5252_generic1():
    try:
        pdict = dict()
        ll = PARAM_SETS.split('\n')
        for l in ll:
            l = l.strip()
            if l:
                w = l.split()
                print(w)
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = w
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = (
                    setk,int(ll2),int(ul2),int(n2),
                    int(ll3),int(ul3),int(n3),float(expt)
                )
                pdict[setk] = (ll2,ul2,n2,ll3,ul3,n3,expt)

        ll = POSITIONS.split('\n')
        measlist = []
        for l in ll:
            l = l.strip()
            if l:
                w = l.split()
                print(w)
                posname, setk = w
                measlist.append((posname, setk))
        so()
        mgeig()

        for i, (posname, setk) in enumerate(measlist):
            cellname = extract_cellname(posname)
            roiname = f'{cellname}_{setk}'

            newdataset(f'{roiname}_{START_KEY}')
            gopos(posname)
            dkmapyz_2(*pdict[setk])

    finally:
        sc()
        sc()
        sc()
