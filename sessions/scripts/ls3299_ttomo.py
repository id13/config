print('version ttomo test ihls3299 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

#index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 1.500, 0
2, 0, 3.000, 0
3, 0, 4.500, 0
4, 0, 6.000, 0
5, 0, 7.500, 0
6, 0, 9.000, 0
7, 0, 10.500, 0
8, 0, 12.000, 0
9, 0, 13.500, 0
10, 0, 15.000, 0
11, 0, 16.500, 0
12, 0, 18.000, 0
13, 0, 19.500, 0
14, 0, 21.000, 0
15, 0, 22.500, 0
16, 0, 24.000, 0
17, 0, 25.500, 0
18, 0, 27.000, 0
19, 0, 28.500, 0
20, 0, 30.000, 0
21, 0, 31.500, 0
22, 0, 33.000, 0
23, 0, 34.500, 0
24, 0, 36.000, 0
25, 0, 37.500, 0
26, 0, 39.000, 0
27, 0, 40.500, 0
28, 0, 42.000, 0
29, 0, 43.500, 0
30, 0, 45.000, 0
31, 0, 46.500, 0
32, 0, 48.000, 0
33, 0, 49.500, 0
34, 0, 51.000, 0
35, 0, 52.500, 0
36, 0, 54.000, 0
37, 0, 55.500, 0
38, 0, 57.000, 0
39, 0, 58.500, 0
40, 0, 60.000, 0
41, 0, 61.500, 0
42, 0, 63.000, 0
43, 0, 64.500, 0
44, 0, 66.000, 0
45, 0, 67.500, 0
46, 0, 69.000, 0
47, 0, 70.500, 0
48, 0, 72.000, 0
49, 0, 73.500, 0
50, 0, 75.000, 0
51, 0, 76.500, 0
52, 0, 78.000, 0
53, 0, 79.500, 0
54, 0, 81.000, 0
55, 0, 82.500, 0
56, 0, 84.000, 0
57, 0, 85.500, 0
58, 0, 87.000, 0
59, 0, 88.500, 0
60, 0, 90.000, 0
61, 0, 91.500, 0
62, 0, 93.000, 0
63, 0, 94.500, 0
64, 0, 96.000, 0
65, 0, 97.500, 0
66, 0, 99.000, 0
67, 0, 100.500, 0
68, 0, 102.000, 0
69, 0, 103.500, 0
70, 0, 105.000, 0
71, 0, 106.500, 0
72, 0, 108.000, 0
73, 0, 109.500, 0
74, 0, 111.000, 0
75, 0, 112.500, 0
76, 0, 114.000, 0
77, 0, 115.500, 0
78, 0, 117.000, 0
79, 0, 118.500, 0
80, 0, 120.000, 0
81, 0, 121.500, 0
82, 0, 123.000, 0
83, 0, 124.500, 0
84, 0, 126.000, 0
85, 0, 127.500, 0
86, 0, 129.000, 0
87, 0, 130.500, 0
88, 0, 132.000, 0
89, 0, 133.500, 0
90, 0, 135.000, 0
91, 0, 136.500, 0
92, 0, 138.000, 0
93, 0, 139.500, 0
94, 0, 141.000, 0
95, 0, 142.500, 0
96, 0, 144.000, 0
97, 0, 145.500, 0
98, 0, 147.000, 0
99, 0, 148.500, 0
100, 0, 150.000, 0
101, 0, 151.500, 0
102, 0, 153.000, 0
103, 0, 154.500, 0
104, 0, 156.000, 0
105, 0, 157.500, 0
106, 0, 159.000, 0
107, 0, 160.500, 0
108, 0, 162.000, 0
109, 0, 163.500, 0
110, 0, 165.000, 0
111, 0, 166.500, 0
112, 0, 168.000, 0
113, 0, 169.500, 0
114, 0, 171.000, 0
115, 0, 172.500, 0
116, 0, 174.000, 0
117, 0, 175.500, 0
118, 0, 177.000, 0
119, 0, 178.500, 0
120, 0, 0.000, 0
121, 5, 0.750, 0
122, 5, 13.580, 0
123, 5, 26.411, 0
124, 5, 39.241, 0
125, 5, 52.071, 0
126, 5, 64.902, 0
127, 5, 77.732, 0
128, 5, 90.562, 0
129, 5, 103.393, 0
130, 5, 116.223, 0
131, 5, 129.054, 0
132, 5, 141.884, 0
133, 5, 154.714, 0
134, 5, 167.545, 0
135, 5, 180.375, 0
136, 5, 193.205, 0
137, 5, 206.036, 0
138, 5, 218.866, 0
139, 5, 231.696, 0
140, 5, 244.527, 0
141, 5, 257.357, 0
142, 5, 270.188, 0
143, 5, 283.018, 0
144, 5, 295.848, 0
145, 5, 308.679, 0
146, 5, 321.509, 0
147, 5, 334.339, 0
148, 5, 347.170, 0
149, 0, 0.000, 0
150, 10, 0.000, 0
151, 10, 12.857, 0
152, 10, 25.714, 0
153, 10, 38.571, 0
154, 10, 51.429, 0
155, 10, 64.286, 0
156, 10, 77.143, 0
157, 10, 90.000, 0
158, 10, 102.857, 0
159, 10, 115.714, 0
160, 10, 128.571, 0
161, 10, 141.429, 0
162, 10, 154.286, 0
163, 10, 167.143, 0
164, 10, 180.000, 0
165, 10, 192.857, 0
166, 10, 205.714, 0
167, 10, 218.571, 0
168, 10, 231.429, 0
169, 10, 244.286, 0
170, 10, 257.143, 0
171, 10, 270.000, 0
172, 10, 282.857, 0
173, 10, 295.714, 0
174, 10, 308.571, 0
175, 10, 321.429, 0
176, 10, 334.286, 0
177, 10, 347.143, 0
178, 0, 0.000, 0
179, 15, 6.429, 0
180, 15, 19.056, 0
181, 15, 31.684, 0
182, 15, 44.311, 0
183, 15, 56.939, 0
184, 15, 69.566, 0
185, 15, 82.194, 0
186, 15, 94.821, 0
187, 15, 107.449, 0
188, 15, 120.077, 0
189, 15, 132.704, 0
190, 15, 145.332, 0
191, 15, 157.959, 0
192, 15, 170.587, 0
193, 15, 183.214, 0
194, 15, 195.842, 0
195, 15, 208.469, 0
196, 15, 221.097, 0
197, 15, 233.724, 0
198, 15, 246.352, 0
199, 15, 258.980, 0
200, 15, 271.607, 0
201, 15, 284.235, 0
202, 15, 296.862, 0
203, 15, 309.490, 0
204, 15, 322.117, 0
205, 15, 334.745, 0
206, 15, 347.372, 0
207, 0, 0.000, 0
208, 20, 0.000, 0
209, 20, 12.857, 0
210, 20, 25.714, 0
211, 20, 38.571, 0
212, 20, 51.429, 0
213, 20, 64.286, 0
214, 20, 77.143, 0
215, 20, 90.000, 0
216, 20, 102.857, 0
217, 20, 115.714, 0
218, 20, 128.571, 0
219, 20, 141.429, 0
220, 20, 154.286, 0
221, 20, 167.143, 0
222, 20, 180.000, 0
223, 20, 192.857, 0
224, 20, 205.714, 0
225, 20, 218.571, 0
226, 20, 231.429, 0
227, 20, 244.286, 0
228, 20, 257.143, 0
229, 20, 270.000, 0
230, 20, 282.857, 0
231, 20, 295.714, 0
232, 20, 308.571, 0
233, 20, 321.429, 0
234, 20, 334.286, 0
235, 20, 347.143, 0
236, 0, 0.000, 0
237, 25, 6.429, 0
238, 25, 20.027, 0
239, 25, 33.626, 0
240, 25, 47.225, 0
241, 25, 60.824, 0
242, 25, 74.423, 0
243, 25, 88.022, 0
244, 25, 101.621, 0
245, 25, 115.220, 0
246, 25, 128.819, 0
247, 25, 142.418, 0
248, 25, 156.016, 0
249, 25, 169.615, 0
250, 25, 183.214, 0
251, 25, 196.813, 0
252, 25, 210.412, 0
253, 25, 224.011, 0
254, 25, 237.610, 0
255, 25, 251.209, 0
256, 25, 264.808, 0
257, 25, 278.407, 0
258, 25, 292.005, 0
259, 25, 305.604, 0
260, 25, 319.203, 0
261, 25, 332.802, 0
262, 25, 346.401, 0
263, 0, 0.000, 0
264, 30, 0.000, 0
265, 30, 15.000, 0
266, 30, 30.000, 0
267, 30, 45.000, 0
268, 30, 60.000, 0
269, 30, 75.000, 0
270, 30, 90.000, 0
271, 30, 105.000, 0
272, 30, 120.000, 0
273, 30, 135.000, 0
274, 30, 150.000, 0
275, 30, 165.000, 0
276, 30, 180.000, 0
277, 30, 195.000, 0
278, 30, 210.000, 0
279, 30, 225.000, 0
280, 30, 240.000, 0
281, 30, 255.000, 0
282, 30, 270.000, 0
283, 30, 285.000, 0
284, 30, 300.000, 0
285, 30, 315.000, 0
286, 30, 330.000, 0
287, 30, 345.000, 0
288, 0, 0.000, 0
289, 35, 7.500, 0
290, 35, 22.188, 0
291, 35, 36.875, 0
292, 35, 51.562, 0
293, 35, 66.250, 0
294, 35, 80.938, 0
295, 35, 95.625, 0
296, 35, 110.312, 0
297, 35, 125.000, 0
298, 35, 139.688, 0
299, 35, 154.375, 0
300, 35, 169.062, 0
301, 35, 183.750, 0
302, 35, 198.438, 0
303, 35, 213.125, 0
304, 35, 227.812, 0
305, 35, 242.500, 0
306, 35, 257.188, 0
307, 35, 271.875, 0
308, 35, 286.562, 0
309, 35, 301.250, 0
310, 35, 315.938, 0
311, 35, 330.625, 0
312, 35, 345.312, 0
313, 0, 0.000, 0
314, 40, 0.000, 0
315, 40, 16.364, 0
316, 40, 32.727, 0
317, 40, 49.091, 0
318, 40, 65.455, 0
319, 40, 81.818, 0
320, 40, 98.182, 0
321, 40, 114.545, 0
322, 40, 130.909, 0
323, 40, 147.273, 0
324, 40, 163.636, 0
325, 40, 180.000, 0
326, 40, 196.364, 0
327, 40, 212.727, 0
328, 40, 229.091, 0
329, 40, 245.455, 0
330, 40, 261.818, 0
331, 40, 278.182, 0
332, 40, 294.545, 0
333, 40, 310.909, 0
334, 40, 327.273, 0
335, 40, 343.636, 0
336, 0, 0.000, 0
337, 45, 8.182, 0
338, 45, 25.773, 0
339, 45, 43.364, 0
340, 45, 60.955, 0
341, 45, 78.545, 0
342, 45, 96.136, 0
343, 45, 113.727, 0
344, 45, 131.318, 0
345, 45, 148.909, 0
346, 45, 166.500, 0
347, 45, 184.091, 0
348, 45, 201.682, 0
349, 45, 219.273, 0
350, 45, 236.864, 0
351, 45, 254.455, 0
352, 45, 272.045, 0
353, 45, 289.636, 0
354, 45, 307.227, 0
355, 45, 324.818, 0
356, 45, 342.409, 0
357, 0, 0.000, 0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        print('move to patch1')
        umvr(nny,-0.0425)
        with gevent.Timeout(seconds=140):
            try:
                # put the nano scan ...
                print('patch1')
                kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
        print('move to patch 2')
        umvr(nny,0.085)
        with gevent.Timeout(seconds=140):
            try:
                # put the nano scan ...
                print('patch2')
                kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch2 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
        print('move back to cen')
        umvr(nny,-0.0425)
        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//0.75)
        nitvz = int(dz//0.75)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz


def ls3299_ttomo_main(zkap, start_key, resume=-1):

    
    print('Hi IH-MI_1531!')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -45,
        uly = 45,
        nitvy = 120,
        llz = -49.875,
        ulz = 48.875,
        nitvz = 133,
        expt = 0.002
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
