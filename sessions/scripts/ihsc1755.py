print('ihsc1755.py - load 1')

def safety_net():
    for i in range(1,2):
        timer = 2 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)


# list section begin
#
#THE_LIST = list(range(0, 81, 1))
#THE_LIST.extend(list(range(100, 271, 1)))

# for startkey d (morning check):
#THE_LIST = list(range(0, 21, 1))

# list peanut
THE_LIST = list(range(0, 81, 1))
THE_LIST.extend(list(range(120, 261, 1)))

print(len(THE_LIST))
print(THE_LIST)

#START_KEY = 'd'
def tomo_r4_p3():
    try:
        start_time = time.time()
        so()
        newdataset(f'tomot_r4_p3_{START_KEY}')
        for i in THE_LIST:
            start_time_cyc = time.time()
            cmg.move_to_cen(-53+i)
            print('===============')
            print(i)
            print('===============')
            align_particle(10, 0.5, 0.01, excessv=0.02)

            # main scan
            kmap.dkmap(nnp2, -2, 2, 80, nnp3, -2, 2, 80, 0.01)

            # elog fromtime to time
            try:
                if not i % 5:
                    elog_plot(scatter=True)
            except:
                print("elog_plot error - wasn't that important anyway ...")
            safety_net()
            t = time.time()
            print('******** done cycle', i, 'over all time', t - start_time, 'cyc time', t - start_time_cyc)
    finally:
        sc()
        sc()
        sc()



START_KEY = 'a'

def tomo_peanuts():
    try:
        start_time = time.time()
        so()
        newdataset(f'tomo_r2_p2_{START_KEY}')
        for i in THE_LIST:
            start_time_cyc = time.time()
            cmg.move_to_cen(110-180+i)
            print('===============')
            print(i)
            print('===============')
            align_particle(20, 0.5, 0.01, excessv=0.02)

            # main scan
            kmap.dkmap(nnp2, -3, 3, 120, nnp3, -2, 2, 80, 0.01)

            # elog fromtime to time
            try:
                if not i % 5:
                    elog_plot(scatter=True)
            except:
                print("elog_plot error - wasn't that important anyway ...")
            safety_net()
            t = time.time()
            print('******** done cycle', i, 'over all time', t - start_time, 'cyc time', t - start_time_cyc)
    finally:
        sc()
        sc()
        sc()
