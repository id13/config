print('ls3357 load 6')


start_key = 'a'

patch_list = [
'patch1',
'patch2',
'patch3',
'patch4',
]

offset_list = [
[0.075,0.075],
[0.225,0.075],
[0.075,0.225],
[0.225,0.225],
]

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(i,patch_list,offset_list,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    rmgeigx()
    name = f'{patch_list[i]}_{start_key}'
    #gopos(pos)
    gopos('ul_hr_xrd')
    umvr(nny,offset_list[i][0])
    umvr(nnz,offset_list[i][1])
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

def gopos_kmap_test(i,patch_list,offset_list,start_key,):
    
    so()
    fshtrigger()
    name = f'{patch_list[i]}_{start_key}'
    print(name)
    #gopos(pos)
    #gopos('ul_hr_xrd')
    umvr(nny,offset_list[i][0])
    umvr(nnz,offset_list[i][1])


def run_Nov_10th_day():
    try:
        umv(ndetx,-200)
        so()
        fshtrigger()
        rmgeigx()
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-100.05,100.05,1334,nnp3,-100.05,100.05,1334,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-50.025,50.025,667,nnp3,-100.05,100.05,1334,0.01)
        sc()
    except:
        sc()
        sc()

def run_Nov_10th_night():
    try:
        umv(ndetx,-200)
        so()
        fshtrigger()
        rmgeigx()
        gopos('roi1')
        newdataset(f'roi1b_{start_key}')
        kmap.dkmap(nnp2,-100.05,100.05,1334,nnp3,-100.05,100.05,1334,0.01)
        gopos('roi2b')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-55.05,55.05,734,nnp3,-100.05,100.05,1334,0.01)
        gopos('roi3')
        newdataset(f'roi3_{start_key}')
        kmap.dkmap(nnp2,-100.05,100.05,1334,nnp3,-30,30,400,0.01)
        gopos('roi4')
        newdataset(f'roi4_{start_key}')
        kmap.dkmap(nnp2,-55.05,55.05,734,nnp3,-30,30,400,0.01)
        sc()
    except:
        sc()
        sc()
        
def run_Nov_11th_night():
    try:
        umv(ndetx,-200)
        so()
        fshtrigger()
        rmgeigx()
        gopos('roi1')
        newdataset(f'roi1_{start_key}')
        kmap.dkmap(nnp2,-100.05,100.05,1334,nnp3,-100.05,100.05,1334,0.01)
        gopos('roi2')
        newdataset(f'roi2_{start_key}')
        kmap.dkmap(nnp2,-100.05,100.05,1334,nnp3,-37.5,37.5,500,0.01)
        sc()
    except:
        sc()
        sc()
