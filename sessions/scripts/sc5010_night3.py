def sc5010_night3():

    # 1 ###########################################

    print('newdataset(BB1755_1)')
    newdataset('BB1755_1')

    gopos('measure_BB1755')
    umv(ufluox, -5.0)
    dkpatchmesh(0.25,125,0.338,169,0.04,2,4) 

    enddataset()

    # 2 ###########################################
    print('newdataset(BB1761_1)')
    newdataset('BB1761_1')

    gopos('measure_BB1761')
    umv(ufluox, -6.5)
    dkpatchmesh(0.35,175,0.35,175,0.04,2,1) 

    enddataset()

    # 3 ###########################################
    print('newdataset(BB1772_1)')
    newdataset('BB1772_1')

    gopos('measure_BB1772')
    umv(ufluox, -5.0)
    dkpatchmesh(0.368,184,0.334,167,0.04,3,3) 

    enddataset()

