print('blc13044_eh3 load 9')

def ybeam_knife_prescan():
    dscan(nnp2,75,-75,50,0.05)
    gofe()

def ybeam_knife_scan():
    dscan(nnp2,5,-5,400,0.1, sleep_time=0.05)
    gofe()

def zbeam_knife_prescan():
    dscan(nnp3,-75,75,50,0.05)
    gofe()

def zbeam_knife_scan():
    dscan(nnp3,-5,5,400,0.1, sleep_time=0.05)
    gofe()

def knife_series_y(ds_name,):#psgap):
    newdataset('prealign_ya')
    pos = "initial_y_edge1"
    gopos(pos)
    x_start = 1#0.061
    delta_x = 0.2
    n_pts = 6
    try:
        mv(nnx, x_start)
        ybeam_knife_prescan()
        newdataset(ds_name)
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(nnx, x_pos)
            ybeam_knife_scan()
    finally:
        enddataset()

def knife_series_z(ds_name,):# psgap):
    newdataset('prealign_za')
    pos = "initial_z_edge1"
    gopos(pos)
    x_start = 1#0.061
    delta_x = 0.2
    n_pts = 6
    try:
        mv(nnx, x_start)
        zbeam_knife_prescan()
        newdataset(ds_name)
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(nnx, x_pos)
            zbeam_knife_scan()
    finally:
        enddataset()

def knife_series_liu():

    nnx_stepsize = '02mm'#'02mm'
    START_KEY = 'a'
    num_lens  = '3'
    try:
        so()
        knife_series_z(f'z_serseries_nnx_{num_lens}bet_step{nnx_stepsize}_{START_KEY}')
        
        knife_series_y(f'y_serseries_nnx_{num_lens}bet_step{nnx_stepsize}_{START_KEY}')

    finally:
        sc()
        sc()
