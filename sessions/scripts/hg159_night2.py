import time

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_night2 ARMED ===")
print('RESUME1')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    if expt < 0.01:
        expt = 0.01
    if expt < 0.02 and nitv < 401:
        retveloc = 0.5
    else:
        retveloc = 0.25
    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='c'

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_night2():



    dooul('nati_CTGF_ul01_1x1_150x200_150x200_25_0014.json')
    dooul('nati_Fe2Ca_ul01_1x1_200x300_200x300_25_0015.json')
    dooul('nati_Fe2Mg_ul01_2x1_250x200_250x200_25_0016.json')
    dooul('nati_G110B_ul01_1x1_300x300_300x300_25_0020.json')
    dooul('nati_G110B_ul02_1x1_300x300_300x300_25_0021.json')
    dooul('nati_G11A_ul01_2x1_250x400_250x400_25_0018.json')
    dooul('nati_G11A_ul02_1x1_300x300_300x300_25_0019.json')
    dooul('nati_G11B_ul01_1x1_300x400_300x400_25_0023.json')
    dooul('nati_GMg1_ul01_1x1_400x400_400x400_25_0013.json')
    dooul('nati_GMg2_ul01_1x1_400x200_400x200_25_0017.json')



def hg159_night2_check_pos():
    pass
    #gopos('')
