print('es1403_calib - load OG5')

# loading omega map capability
load_script('ch6830_infra')
START_KEY = 'd'


ES1403_OG_POS = '''
OG_t1050_x50_pos1_0093.json
OG_t1050_x50_pos2_0095.json
OG_t1050_x50_pos3_0097.json
OG_t1050_x50_pos4_0099.json
OG_t1050_x50_pos5_0101.json
OG_t1150_x50_pos1_0083.json
OG_t1150_x50_pos2_0085.json
OG_t1150_x50_pos3_0087.json
OG_t1150_x50_pos4_0089.json
OG_t1150_x50_pos5_0091.json
OG_tRT_x50_pos1_0103.json
OG_tRT_x50_pos2_0105.json
OG_tRT_x50_pos3_0107.json
OG_tRT_x50_pos4_0109.json
OG_tRT_x50_pos5_0111.json
'''

ES1403_OG_POS_2 = '''
OG_t1050_x50_pos1_0093.json
OG_t1050_x50_pos2_0095.json
OG_t1150_x50_pos1_0083.json
OG_t1150_x50_pos2_0085.json
OG_tRT_x50_pos1_0103.json
OG_tRT_x50_pos2_0105.json
OG_t1050_x50_pos3_0097.json
OG_t1050_x50_pos4_0099.json
OG_t1050_x50_pos5_0101.json
OG_t1150_x50_pos3_0087.json
OG_t1150_x50_pos4_0089.json
OG_t1150_x50_pos5_0091.json
OG_tRT_x50_pos3_0107.json
OG_tRT_x50_pos4_0109.json
OG_tRT_x50_pos5_0111.json
'''

def es1403_overview_scan():
    print('overview: loff_kmap(...)')
    loff_kmap(nnp2, -80,80, 160, nnp3, -80,80, 160, 0.02)

def es1403_overview_scan_2():
    print('overview2: loff_kmap(...)')
    loff_kmap(nnp2, -80,80, 320, nnp3, -80,80, 320, 0.02)

def es1403_omega_scan(label):
    rot_label = f'rot_{label}'
    print('rot_label', rot_label)
    print('rot_scan_series(...)')
    rot_scan_series(rot_label, -1, 1, 10, nnp2, -30,30, 60, nnp3, -20,20,40, 0.02)

def es1403_setupos(posname, intent):
    if posname.endswith('.json'):
        _nm = posname[:-5]
        dsname = f'{_nm[:-5]}_{intent}'
    elif posname.endswith('._pos'):
        dsname = f'{_nm[:-5]}_{intent}'
    else:   
        dsname = posname
    
    print ('='*40,dsname)
    rstp(posname)
    newdataset(f'{dsname}_{START_KEY}')
    return dsname

def strtolist(s):
    ll = s.split('\n')
    ll = [l.strip() for l in ll]
    ll = [l for l in ll if l]
    return ll

def es1403_saturday_night():
    plist = strtolist(ES1403_OG_POS_2)
    for p in plist:
        label = es1403_setupos(p, 'onepos')
        es1403_omega_scan(label)
        es1403_overview_scan()
    plist = strtolist(ES1403_OG_POS_2)
    for p in plist:
        label = es1403_setupos(p, 'onepos2')
        es1403_overview_scan_2()

def es1403_scans_main():
    try:
        so()
        es1403_saturday_night()
    finally:
        sc()
        sc()
        sc()
