# -*- coding: utf-8 -*-

print('psieiger - load 3')
import os
from os import path
from traceback import format_exc
from textwrap import indent as doindent

from collections import OrderedDict as Odict

import gevent, bliss

class MyVirtual(Exception): pass

def psie_restart():
    dco = DetectorControl()
    print('stopping psi eiger ...')
    print(dco.stop())
    print('waiting a bit ...')
    gevent.sleep(10)
    print('starting psi eiger ...')
    print(dco.start())
    print('waiting a bit ...')
    gevent.sleep(30)
    print('mgpsie ...')
    mgpsie()

def psie_set_pixdepth(int_depth):
    int_depth = int(int_depth)
    assert int_depth in (4,8,16)
    str_depth = f'{int_depth:1d}'
    
    if int_depth in (8,16):
        delay = 0
    else:
        max_data_rate = 8e3 
        delay_time_unit = 6.2e-9
        delay = 2 * (1 / max_data_rate) / delay_time_unit
    psi_eiger_500k.camera.tx_frame_delay = delay
    psi_eiger_500k.camera.pixel_depth = str_depth

def psie_get_pixdepth():
    return  psi_eiger_500k.camera.pixel_depth
    
def psie_take(expt, nframes, save=True, run=True):
    max_nframes = int(30000 * 1.0 * 4 / int(psi_eiger_500k.camera.pixel_depth))
    latency_time = 1/(psi_eiger_500k.camera.max_frame_rate * 1e3 - 100)*1.0 - expt
    print(f'max_nframes = {max_nframes}   latency_time = {latency_time}')
    #if nframes > max_nframes:
    #    raise ValueError(f'number of frames is too large: {nframes}')
    return limatake(expt, nframes, psi_eiger_500k, latency_time=latency_time, save=save, run=run)

def asy_fsh_gate(delay, duration):
    gevent.sleep(delay)
    fshopen()
    gevent.sleep(duration)
    fshclose()

def asy_psie_take(expt, nframes, save=True, delay=0.0, duration=0.0):
    _pt = psie_take(expt, nframes, save=save, run=False)
    gevent.spawn(asy_fsh_gate, delay, duration)
    with bench():
        _pt.run()
    fshtrigger()

def psie_loff_kmap(m1, ll1, ul1, n1, m2, ll2, ul2, n2, expt, frames_per_file=None, latency_time=1e-5,
    loff1=-5.0, loff2=-5.0):
    #if None is frames_per_file:
    #    frames_per_file = n1
    assert m1.name in ('nnp1','nnp2','nnp3')
    assert m2.name in ('nnp1','nnp2','nnp3')
    ini_m1_pos = m1.position
    ini_m2_pos = m2.position
    try:
        mv(m1, ini_m1_pos + ll1 + loff1)
        mv(m2, ini_m2_pos + ll2 + loff2)
        kmap.dkmap(m1, -loff1, -ll1-loff1+ul1, n1, m2, -loff2, -ll2-loff2+ul2, n2, expt,
            frames_per_file=frames_per_file, latency_time=latency_time)
    finally:
        print('returning motors to initial position:', ini_m1_pos, ini_m2_pos)
        mv(m1, ini_m1_pos)
        mv(m2, ini_m2_pos)
    fshtrigger()



class McbLogger(object):

    def __init__(self, log_prefix):
        self.detail_logfupth = f'mcb_{log_prefix}_detail.log'
        self.summary_logfupth = f'mcb_{log_prefix}_summary.log'
        self._last_id = None
        self.reset()

    def get_new_id(self):
        lid = self._last_id
        if None is lid:
            if path.exists(self.summary_logfupth):
                ll = self._get_summary()
                if 0:
                    try:
                        print(f'[{ll[-1]}]')
                        print(f'[{ll[-2]}]')
                        print(f'[{ll[-3]}]')
                    except IndexError:
                        pass
                x = ll[-1][:9]
                lid = int(x)
            else:
                lid = -1
        new_id = self._last_id = lid + 1
        return lid

    def reset(self):
        self._summary_ll = None

    def log(self, msg, details_dc=None):
        if '\n' in msg:
            print('warning: newline character found in msg [{msg}]')
        lid = self.get_new_id()
        new_id = lid + 1
        summary_line = f'{new_id:08d}  {gevent.time.time()}  {gevent.time.asctime()}   {msg}\n'
        with open(self.summary_logfupth, 'a') as f:
            f.write(summary_line)

        ll = []
        for (k,v) in details_dc.items():
            ll.append(f'k = {k}')
            ll.append(doindent(str(v), '    '))
        ll.append('')
        detail_s = '\n'.join(ll)
        detail_s = doindent(detail_s, '    ')
        with open(self.detail_logfupth, 'a') as f:
            f.write(summary_line)
            f.write(detail_s)

    def _get_summary(self):
        with open(self.summary_logfupth) as f:
            ll = f.readlines()
        return ll

class BlissCommand(object):

    FUNC = (print,)
    NAME = 'print'

    def __init__(self, timeout=None, further_details=None):
        self.timeout = timeout
        self.logger = McbLogger('georg')
        if None is further_details:
            self.further_details = Odict()

    def log(self,msg, details_dc=None):
        self.logger.log(msg, details_dc=details_dc)

    def estimate_timeout(self):
        raise MyVirtual()

    def timeout_exc_handler(self, tb, msg, elapsed, details):
        print('Timeout:')
        print(tb)
        details['traceback'] = tb
        msg = f'{msg};terminated=timeout;elapsed={elapsed}'
        print(msg)
        return msg

    def generic_exc_handler(self, tb, msg, elapsed, details):
        print('Error:')
        print(tb)
        details['traceback'] = tb
        msg = f'{msg};terminated=error;elapsed={elapsed}'
        print(msg)
        return msg

    def _clean_params(self, p, kw):
        pp = []
        for x in p:
            try:
                name = x.name
            except AttributeError:
                name = x
            pp.append(name)

        kwkw = dict()
        for k,v in kw.items():
            try:
                name = v.name
            except AttributeError:
                name = v
            kwkw[k] = name
        return pp, kwkw

    def run(self, *p, **kw):
        self.p = p
        self.kw = kw
        details = Odict()
        details['timeout'] = self.timeout
        pp, kwkw = self._clean_params(p, kw)
        msg = f'name={self.NAME};p={pp};kw={kwkw};timeout={self.timeout}'
        excp_flg = 'new'
        try:
            try:
                t0 = gevent.time.time()
                with gevent.Timeout(seconds=self.timeout):
                    self.FUNC[0](*p, **kw)
                elapsed = gevent.time.time()-t0
                msg = f'{msg};terminated=regular;elapsed={elapsed}'
                print(msg)
                details['excp_flg'] = excp_flg = 'ok'
                tb = ''
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                tb = format_exc()
                elapsed = gevent.time.time()-t0
                msg = self.timeout_exc_handler(tb, msg, elapsed, details)
                details['excp_flg'] = excp_flg = 'timeout'
            except:
                tb = format_exc()
                elapsed = gevent.time.time()-t0
                msg = self.generic_exc_handler(tb, msg, elapsed, details)
                details['excp_flg'] = excp_flg = 'error'
        finally:
            details['elapsed'] = elapsed
            try:
                details.update(self.further_details)
            except AttributeError():
                pass
            self.log(msg, details_dc=details)
        return elapsed, excp_flg, details, tb




class AsyLimatake(BlissCommand):

    # asy_psie_take(expt, nframes, save=True, delay=0.0, duration=0.0)
    FUNC = (asy_psie_take,)
    NAME = 'asy_psie_take'

class PsieRestart(BlissCommand):

    FUNC = (psie_restart,)
    NAME = 'psie_restart'

class PsieLoffKmap(BlissCommand):

    # psie_loff_kmap(m1, ll1, ul1, n1, m2, ll2, ul2, n2, expt, frames_per_file=None,
    #                loff1=-5.0, loff2=-5.0)
    FUNC = (psie_loff_kmap,)
    NAME = 'psie_loff_kmap'


def open_header(*p, **kw):
    pass

def close_header(*p, **kw):
    pass

def comment(*p, **kw):
    pass


class OpenHeader(BlissCommand):

    FUNC = (open_header,)
    NAME = 'open_header'

class CloseHeader(BlissCommand):

    FUNC = (open_header,)
    NAME = 'open_header'

class Comment(BlissCommand):

    FUNC = (comment,)
    NAME = 'comment'


def bbbc(s):
    bbb_comment = Comment(timeout=100)
    bbb_comment.further_details['comment'] = s
    bbb_comment.run()
    
BUFFER = '''
bbb_asytake = AsyLimatake(timeout=1000)
bbb_restart = PsieRestart(timeout=1000)
bbb_loffkmap = PsieLoffKmap(timeout=1000)
bbb_openheader = OpenHeader(timeout=100)
bbb_closeheader = CloseHeader(timeout=100)
'''


class CtrlAttemptEnv(object):

    def __init__(self, detctrl_obj=None):
        self.dco = detctrl


class DetectorControl(object):

    LEGAL_SATUS_VALUES = ('RUNNING', 'STOPPING', 'STARTING', 'STOPPED')

    def __init__(self, server_url='http://lid13eigerlima1:9001', device_s='LIMA:psi_eiger_500k'):
        self.server_url = server_url
        self.device_s = device_s
        self._last_output = None

    def _parse_supervisorctl_output(self, output):
        wll = output.split(maxsplit=2)
        leng = len(wll)
        if leng == 2:
            device_s, status_s = wll
            date_s = ''
        elif leng == 3:
            device_s, status_s, date_s = wll
        else:
            raise ValueError(f'illegal output found: {str(wll)}')
        if device_s[-1] == ':':
            device_s = device_s[:-1]
        return device_s, status_s, date_s

    def _validate_received_data(self, data, longoutput=False):
        device_s, status_s, date_s = data
        device_ok = self.device_s == device_s
        cmd_ok = status_s in self.LEGAL_SATUS_VALUES
        data_ok = device_ok and cmd_ok
        if longoutput:
            return (data_ok, (device_ok, cmd_ok))
        else:
            return data_ok

    def _send_cmd(self, cmd):
        cmd = f'. /users/blissadm/conda/miniconda/bin/activate blissenv && supervisorctl --serverurl {self.server_url} {cmd} {self.device_s}'
        print(f'cmd = [{cmd}]')
        res=gevent.subprocess.run(cmd, shell=True, capture_output=True, text=True)
        output = res.stdout
        self._last_output = output
        device_s, status_s, date_s = data = self._parse_supervisorctl_output(output)
        return device_s, status_s, date_s

    def start(self):
        self._send_cmd('start')
        status_s = self.getstatus()
        return status_s

    def stop(self):
        self._send_cmd('stop')
        status_s = self.getstatus()
        return status_s
    
    def getstatus(self):
        device_s, status_s, date_s = data = self._send_cmd('status')
        if not self._validate_received_data(data):
            raise ValueError(f'illegal outputfrom remote cmd: {self._last_output}')
        return status_s


