print('ls2952_fib_mount_05 load 7')

STARTKEY = 'b'

def gop_newds(pos, prefix=None):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    if not None is prefix:
        ds_name = '{}_{}'.format(prefix, ds_name)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_fib_mount_05():

    try:
       mode = 'SAXS'
 
       if 'SAXS' == mode:
           # full 1.2 x 2.3  horxvert
           gop_newds('fib_mount_05_A1_ter_scanstart_0106.json', prefix=mode)
           dkpatchmesh(0.6, 300, 2.0, 200, 0.02, 2, 1)
           enddataset()

           # full 1.2 x 1.5  horxvert
           gop_newds('fib_mount_05_A2_bis_scanstart_0110.json', prefix=mode)
           dkpatchmesh(0.6, 300, 1.2, 120, 0.02, 2, 1)
           enddataset()

           # full 1.0 x 2.2  horxvert
           gop_newds('fib_mount_05_A3_quater_scanstart_0113.json', prefix=mode)
           dkpatchmesh(1.0, 500, 1.9, 190, 0.02, 1, 1)
           enddataset()

           # full 0.5 x 0.9  horxvert
           gop_newds('fib_mount_05_C4_bis_scanstart_0117.json', prefix=mode)
           dkpatchmesh(0.5, 250, 0.45, 45, 0.02, 1, 1)
           enddataset()

           #
           #gop_newds('fib_mount_05_behenate_scanstart_0118.json')


       elif 'WAXS' == mode:
           # full 1.2 x 2.3  horxvert
           gop_newds('fib_mount_05_A1_ter_scanstart_0106.json', prefix=mode)
           mvr(ustrz, 2.3-0.2)
           dkpatchmesh(0.6, 300, 0.2, 20, 0.02, 2, 1)

           # full 1.2 x 1.5  horxvert
           gop_newds('fib_mount_05_A2_bis_scanstart_0110.json', prefix=mode)
           mvr(ustrz, 1.5-0.2)
           dkpatchmesh(0.6, 300, .2, 20, 0.02, 2, 1)

           # full 1.0 x 2.2  horxvert
           gop_newds('fib_mount_05_A3_quater_scanstart_0113.json', prefix=mode)
           mvr(ustrz, 2.2-0.2)
           dkpatchmesh(1.0, 500, 0.2, 20, 0.02, 1, 1)

           # full 0.5 x 0.9  horxvert
           gop_newds('fib_mount_05_C4_bis_scanstart_0117.json', prefix=mode)
           mvr(ustrz, 0.4)
           dkpatchmesh(0.5, 250, 0.3, 30, 0.02, 1, 1)


    finally:
        sc()
        sc()
