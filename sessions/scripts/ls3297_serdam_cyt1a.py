print('ls3297_serdam_full - load 1')
START_KEY = 'b' #change this letter at each trial

# beloz sswitch to pass for test, and to with bench for actual dat collection
def full_action(sd):
    #pass
    with bench(): sd.run_flymesh()

    
def ls3297_full():

    rstp('pos1')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_full_v50_1')
    sd.setup_flymesh(0, 20, 500, 0, 4.5, 225, 0.0014, retveloc=15)
    sd.setup_fly_speed(50)
    sd.show_log()
    full_action(sd)
    
    rstp('pos2')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_full_v20_2')
    sd.setup_flymesh(0, 20, 500, 0, 4.5, 225, 0.0014, retveloc=15)
    sd.setup_fly_speed(20)
    sd.show_log()
    full_action(sd)


def ls3297_main():
    try:
        so()
        ls3297_full()
    finally:
        sc()
        sc()
        sc()

