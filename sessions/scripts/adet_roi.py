import math


class EigerRoiMgr(object):

    def __init__(self, energ, cenp, dist, pix_size=0.075):
        self.cenp = cenp
        self.energy = energ
        self.lamb = 12.3985/energ
        self.dist = dist
        self.pix_size = pix_size

    def dspc_to_th(self, dspc):
        q = 2*math.pi/dspc
        th = math.asin(q*self.lamb/(4*math.pi))
        return th

    def th_to_Rpix(self, th):
        R = math.tan(2*th)*self.dist
        Rpix = R/self.pix_size
        return Rpix
        
    def set_qroi(self, rcname, dspc, deltad=0.1, azim=(-180.0,180.0)):
        ceny, cenz = self.cenp
        azim_low, azim_hi = azim
        d_max = dspc+0.5*deltad
        d_min = dspc-0.5*deltad

        # low
        th_low = self.dspc_to_th(d_max)
        Rpix_low = self.th_to_Rpix(th_low)

        # hi
        th_hi = self.dspc_to_th(d_min)
        Rpix_hi = self.th_to_Rpix(th_hi)

        print("""
dspc = {}
cenp = {}
Rpix_low = {}
Rpix_hi = {}
""".format(dspc, self.cenp, Rpix_low, Rpix_hi))
        try:
            eiger
        except:
            print(rcname,(ceny, cenz, Rpix_low,Rpix_hi, azim_low, azim_hi))
            return
        eiger.roi_counters.set(rcname,(ceny, cenz, Rpix_low,Rpix_hi, azim_low, azim_hi))


def _test():
    erm = EigerRoiMgr(13.0, (1200,1500), 140.0)
    erm.set_qroi('qqq', 2.0, 0.05)

if __name__ == '__main__':
    _test()
