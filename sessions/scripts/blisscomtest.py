import coderepo
from qma.qma_jfcom2 import *

class TestTwoway(object):

    def __init__(self, args):
        self.args = args
        self.comid_m = int(args[0])
        self.comid_s = int(args[1])

    def run_slave(self):
        fcfg = FcomCfg('erwin.fcom')
        fjs = FJSlave(fcfg, timeout=10000000.0, pause=0.02,
            comid_m = self.comid_m,
            comid_s = self.comid_s)
        while True:
            msg = fjs.process()

    def run_master(self):
        fcfg = FcomCfg('erwin.fcom')
        fjm = FJMaster(fcfg, timeout=10000000.0, pause=0.02,
            comid_m = self.comid_m,
            comid_s = self.comid_s)
        i_a = int(self.args[2])
        i_e = i_a + int(self.args[3])
        for i in range(i_a, i_e):
            msg_obj = FJMessage('cmd1', dict(instruct_id=i))
            fjm.feed(msg_obj)

class JLog(object):

    def __init__(self, host_dxpth, jlog_dupth, nsegsize=100):
        self.host_dapth = path.abspath(host_dxpth)
        self.dapath = path.join(self.host_dapth, jlog_dupth)
        try:
            os.mkdir(self.dapth)
        except OSError:
            pass

    def log(self, payload): &&&

class BlissAccess(object):

    MOT_INFO = '''
        x:ustrx y:ustry z:ustrz'
    '''

    CTR_INFO = ''

    def __init__(self, blissconfig):
        self.blissconfig = blissconfig
        self.mot_dc = dict()

    def _parse_motinfo(self, mot_info):
        mot_dc = self.mot_dc
        bc = self.blissconfig
        mot_info = mot_info.replace('\n',' ')
        ll = mot_info.split()
        for x in ll:
            role_name, mot_name = x.split(':')
            mot_dev = bc.get(mot_name)
            mot_dc[role_name] = mot_dev
            

    def dscan(self, motname, *p):
        dscan(self.mot_dc[motname], *p)

TestScanProcessor(DefaultProcessor):

    def __init__(self):
        self.mot_dc = dict(
            ustry = ustry,
            ustrz = ustrz
            )
        


def run_slave():
    ttw = TestTwoway([0,0])
    ttw.run_slave()
