print('ma6421_infra - load 2')

import os
from os import path


class MyAbort(Exception): pass

def check_abort():
    if path.exists('./stopit'):
        raise MyAbort()

def clear_abort():
    if path.exists('./stopit'):
        os.unlink('./stopit')

def xxx_kmap_partial(kpartial, sim_mode):
    (ll0, ul0, n0, ll1, ul1, n1, expt) = kpartial
    rfp = (nnp2, ll0, ul0, n0, nnp3, ll1, ul1, n1, expt)
    sfp = (nnp2.name, ll0, ul0, n0, nnp3.name, ll1, ul1, n1, expt)
    if sim_mode['kmap']:
        print(f'SIM: kmap {str(sfp)}')
        sleep(3)
    else:
        print(f'REAL: kmap {str(sfp)}')
        kmap.dkmap(*rfp)

def xxx_newdataset(dsname, sim_mode):
    if sim_mode['dset']:
        print(f'SIM: newdataset: {dsname}')
    else:
        print(f'REAL newdataset: {dsname}')
        newdataset(dsname)

def xxx_rstp(posname, sim_mode):
    if sim_mode['rstp']:
        print(f'SIM: rstp: {posname}')
    else:
        print(f'REAL: rstp: {posname}')
        rstp(posname)

# i :  horizontal
# j :  vertical

def make_patch_scan_dsname(dsname_tag, i, j, start_key='a'):
    return f'{start_key}_{dsname_tag}_{i:02}_{j:02}'

def doo_patch_scan(kpartial=None, ni=None, nj=None, di=None, dj=None,
    start_key=None, dsname_tag=None, sim_mode=None):
    ini_nny_pos = nny.position
    ini_nnz_pos = nnz.position
    try:
        print(f'{nj=}')
        for j in range(nj):
            target_nnz_pos = ini_nnz_pos + j*dj

            print(f'{target_nnz_pos=}')
            mv(nnz, target_nnz_pos)
            read_nnz_pos = nnz.position
            print(f'{read_nnz_pos=}')

            for i in range(ni):
                target_nny_pos = ini_nny_pos + i*di
                print(f'{target_nny_pos=}')
                mv(nny, target_nny_pos)
                read_nny_pos = nny.position
                print(f'{read_nny_pos=}')

                check_abort()
                dsname = make_patch_scan_dsname(dsname_tag, i, j,
                            start_key)
                xxx_newdataset(dsname, sim_mode)
                xxx_kmap_partial(kpartial, sim_mode)

    finally:
        mv(nny, ini_nny_pos)
        mv(nnz, ini_nnz_pos)

def doo_one_coarse(**kw):
    xxx_rstp(kw['posname'], kw['sim_mode'])
    print(kw['patchparams'])
    doo_patch_scan(**kw['patchparams'])
