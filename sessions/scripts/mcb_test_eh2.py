import sys
sys.path.insert(0, '/users/opid13/mcb/test_pypath')

from os import path
import numpy as np

import baseinfra

import metadata
import datah5ta
import sres
import xprofiles as xp
import dataanalysis1 as da1

import eh2_opinfo

from bliss import setup_globals as BGLO
from bliss.setup_globals import *

class Benv(object):

    def __init__(self):
        self.gscans = get_glob_scans()
        brico_dname = "/data/id13/inhouse12/DATAPOLICY_I12_1/eh2/inhouse/blc12302/id13"
        brico_fname = "blc12302_id13.h5"
        inimdata = dict(
            dname = brico_dname,
            fname = brico_fname
        )
        print (inimdata)
        self.metad = metadata.BlissScanFile1(inimdata=inimdata)
        self.meta = metadata.make_mdview(self.metad)
        self.opcfg = eh2_opinfo.make_opcfg()
        self.scanres = sres.StdScanTa(self.metad, self.opcfg)
        self.blissscans = da1.BlissScans(self.gscans)
        self.blissprofiles = da1.BlissProfiles_F1(
            self.scanres,
            det = self.get_det(),
            mon = self.get_mon(),
            blissscans = self.blissscans
        )

        self.scanres.open()
        

    def get_det(self):
        return "p201_eh2_0:ct2_counters_controller:ct22"

    def get_mon(self):
        return "p201_eh2_0:ct2_counters_controller:ct24"

    def get_detobj(self):
        return p201_eh2_0.counters.ct22

    def get_monobj(self):
        return p201_eh2_0.counters.ct24

    def get_last_bliss_scanno(self):
        scanno= self.gscans[-1].scan_number
        scanno = int(scanno)
        return scanno

class KnifeSuite(object):

    def __init__(self, fname):
        self.benv = Benv()

    def load_prf(self, gsels, gctr=None, gaxis=0, bbcon=True):
        bprofs = self.benv.blissprofiles
        if bbcon:
            (p,) = bprofs.bbcon_load_single_profiles(gsels, gctr=gctr, gaxis=0)
        else:
            raise NotImplemented
        return p

    def detect_edge(self, prf, thresh_lev=0.5, edge_tipe=-1):
        seg = xp.ScanSegmentation1D(prf)
        if edge_tipe in (-1,'falling'):
            edge_i = seg.get_first_falling()
        elif edge_tipe in (1,'rising'):
            edge_i = seg.get_first_rising()
        else:
            raise ValueError('illegal edge mode: %s' % edge-tipe)
        new_xpos = seg.interpolate_x(edge_i)
        return (new_xpos)

    def analyze_beam(self, prf):
        beaman = xp.BeamAnalysis1(prf)
        fwhm = beaman.process()
        return fwhm

def goe(edge_tipe):
    ks = KnifeSuite('erwin')
    p = ks.load_prf(-1)
    pos = ks.detect_edge(prf=p, edge_tipe=edge_tipe)
    sc = ks.benv.blissscans.get_scan(-1)
    relev = ks.benv.blissscans.get_relevant_scaninfo(sc)
    print (relev.single, pos)
    mot = getattr(BGLO, relev.single)
    if mot in (ustry,ustrz):
        mv(mot, pos)
        where()

def gofe():
    goe('falling')

def gore():
    goe('rising')

    


def testks():
    gscans = get_glob_scans()
    print (gscans)
    ks = KnifeSuite()
    p = ks.load_prf(-1)
    print (p.xrr)
    print (p.yrr)
    print (p.idxrr)
