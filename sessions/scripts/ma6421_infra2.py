print('ma6421_infra - load 2')

import os
from os import path


class MyAbort(Exception): pass

def check_abort():
    if path.exists('./stopit'):
        raise MyAbort()

def clear_abort():
    if path.exists('./stopit'):
        os.unlink('./stopit')

def xxx_kmap_partial(kpartial, sim_mode):
    (ll0, ul0, n0, ll1, ul1, n1, expt) = kpartial
    rfp = (nnp2, ll0, ul0, n0, nnp3, ll1, ul1, n1, expt)
    sfp = (nnp2.name, ll0, ul0, n0, nnp3.name, ll1, ul1, n1, expt)
    if sim_mode['kmap']:
        print(f'SIM: kmap {str(sfp)}')
        sleep(3)
    else:
        print(f'REAL: kmap {str(sfp)}')
        kmap.dkmap(*rfp)

def xxx_newdataset(dsname, sim_mode):
    if sim_mode['dset']:
        print(f'SIM: newdataset: {dsname}')
    else:
        print(f'REAL newdataset: {dsname}')
        newdataset(dsname)

def xxx_rstp(posname, sim_mode):
    if sim_mode['rstp']:
        print(f'SIM: rstp: {posname}')
    else:
        print(f'REAL: rstp: {posname}')
        rstp(posname)

# i :  horizontal
# j :  vertical

def make_patch_scan_dsname(dsname_tag, i, j, start_key='a'):
    return f'{start_key}_{dsname_tag}_{i:02}_{j:02}'

def doo_patch_scan(kpartial=None, ni=None, nj=None, di=None, dj=None,
    start_key=None, dsname_tag=None, sim_mode=None):
    ini_nny_pos = nny.position
    ini_nnz_pos = nnz.position
    try:
        print(f'{nj=}')
        for j in range(nj):
            target_nnz_pos = ini_nnz_pos + j*dj

            print(f'{target_nnz_pos=}')
            mv(nnz, target_nnz_pos)
            read_nnz_pos = nnz.position
            print(f'{read_nnz_pos=}')

            for i in range(ni):
                target_nny_pos = ini_nny_pos + i*di
                print(f'{target_nny_pos=}')
                mv(nny, target_nny_pos)
                read_nny_pos = nny.position
                print(f'{read_nny_pos=}')

                check_abort()
                dsname = make_patch_scan_dsname(dsname_tag, i, j,
                            start_key)
                xxx_newdataset(dsname, sim_mode)
                xxx_kmap_partial(kpartial, sim_mode)

    finally:
        mv(nny, ini_nny_pos)
        mv(nnz, ini_nnz_pos)

def doo_one_coarse(**kw):
    xxx_rstp(kw['posname'], kw['sim_mode'])
    print(kw['patchparams'])
    doo_patch_scan(**kw['patchparams'])


def call_one_coarse(**kw):
    assert set(kw.keys()) == set(

    # these parameters are required:
    '''

    sim_mode start_key dsname_tag posname
    hdi hdj kni knj ni nj expt


    '''.split())
    )

    print(I#### call_one_coarse params found:')
    pprint(kw)

    clear_abort()

    # sim_mode
    sim_mode = kw['sim_mode']

    # admin
    start_key = kw['start_key']
    dsname_tag = kw['dsname_tag']

    # start pos name
    posname = kw['posname']

    #### kmap
    # half width of the scan
    hdi = kw['hdi']
    hdj = kw['hdj']
    # shape info of kmap
    kni = kw['kni']
    knj = kw['knj']

    # patch matrix
    ni = kw['ni']
    nj = kw['nj']


    # exposuretime
    expt = kw['expt']
    

    patchparams = dict(
        kpartial = (
            -hdi,hdi,kni,
            -hdj,hdj,knj,
            expt),
        ni = ni ,  nj = nj    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key=start_key,
        dsname_tag =adsname_tag, 
        sim_mode=sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = posname
    )

def run_position(defaults_dc, ni = None, nj = None, posname = None, **kw)
     
    my_params = dict()
    my_params.update(defaults_dc)

    my_params.update(defaults_dc)
    my_params.update(
        ni = ni, nj = nj, posname = posname, dsname_tag = posname
    )
    my_params.update(**kw)


# ##################################################################
# example/template:
# 
def run_my_positions():
    raise ValueError('example only!')

    defaults_dc = dict(
        sim_mode = dict(
            kmap = False,
            dset = False,
            rstp = False
        ),
        start_key = 'a',

        kni = 150,
        knj = 150,
        hdi = 75.0,
        hdj = 75.0,
        expt = 0.02
    )
    run_position(defaults_dc,          posname = 'nopos_aaa',
                                       ni = 3, nj = 2
                )
    run_position(defaults_dc,          posname = 'nopos_bbb',
                                       ni = 4, nj = 5
                )
    run_position(defaults_dc,          posname = 'nopos_ccc',
                                       ni = 42, nj = 4242
                )
