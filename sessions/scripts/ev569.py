print("ev569 - load 10")

from os import path

# vigo_l3_3

#g1
START_KEY = "a"
POS_LIST = '''
a2 hv4
a3 hv4
a4 hv4
b1 hv4
b2 hv4
b3 hv4
'''

class Bailout(Exception): pass

def make_poslist():
    ll = POS_LIST.split('\n')
    ll = [x for x in ll if x.strip()]
    ll = [tuple(x.split()) for x in ll]
    return ll

def noop(*p, **kw):
    pass

def check_bailout():
    if path.exists('./stopit'):
        raise Bailout()

# SIMUL

def doo_realscan(*p):
    check_bailout()
    kmap.dkmap(*p)

#doo_scan = print
doo_scan = doo_realscan

#doo_rstp = print
doo_rstp = rstp

def ev569_mgeigx():
    mgd()
    mgeig()
    MG_EH3a.enable("*r:r*")
    MG_EH3a.enable("*xmap3*det0*")

def ev569_mgd():
    mgd()
    fshtrigger()

def ev569_roison():
    eiger.roi_counters.set('roi1', (1124.7, 1189.6, 9.3, 22.4, -132.8, 227.2))

def ev569_mvthings_out():
    print('ATTENTION!')
    print('would do the following:\n    umv(nfluoby, 120, nfluoay, 180, nbsy, -100, ndetx, 300)')
    ans = yesno('proceed?')
    if ans:
        umv(nfluoby, 120, nfluoay, 180, nbsy, -100, ndetx, 300)

def ev569_mvthings_in():
    print('ATTENTION!')
    print('would do the following:\n    umv(nfluoby, 57, nfluoay, 0, nbsy, -2)')
    ans = yesno('proceed?')
    if ans:
        umv(nfluoby, 57, nfluoay, 0, nbsy, -2)


def ev569_roisoff():
    eiger.roi_counters.clear()

class DoScans(object):

    def do_scans_hv4(self, pname):
        print(f"============ overview: {pname}")
        doo_scan(nnp2,-80,80,160,nnp3,-80,80,160,0.02)
    
        print(f"============ quad1: {pname}")
        doo_scan(nnp2,-30,0,300,nnp3,-30,0,300,0.02)
    
        print(f"============ quad2: {pname}")
        doo_scan(nnp2,0,30,300,nnp3,-30,0,300,0.02)
    
        print(f"============ quad3: {pname}")
        doo_scan(nnp2,-30,0,300,nnp3,0,30,300,0.02)
    
        print(f"============ quad4: {pname}")
        doo_scan(nnp2,0,30,300,nnp3,0,30,300,0.02)
    
    
    def do_scans_v2(self, pname):
    
        print(f"============ overview: {pname}")
        doo_scan(nnp2,-80,80,160,nnp3,-80,80,160,0.02)
    
        print(f"============ vert1: {pname}")
        doo_scan(nnp2,-15,15,300,nnp3,-30,0,300,0.02)
    
        print(f"============ vert2: {pname}")
        doo_scan(nnp2,-15,15,300,nnp3,0,30,300,0.02)

    def do_scans_cen(self, pname):
    
        print(f"============ overview: {pname}")
        doo_scan(nnp2,-80,80,160,nnp3,-80,80,160,0.02)
    
        print(f"============ cen: {pname}")
        doo_scan(nnp2,-15,15,300,nnp3,-15,15,300,0.02)
    
def do_scans(pname, mode):
    ds = DoScans()
    meth = getattr(ds, f'do_scans_{mode}')
    meth(pname)

def prep_pos(pname):
    if pname.endswith('._pos'):
        pname = pname[:-5]
    newdataset(f"{START_KEY}_{pname}")
    print(f"========================================== restoring: {pname}")
    doo_rstp(pname)

def ev569_main():
    try:
        so()
        ll = make_poslist()
        print(ll)

        try:
            for pname, mode in ll:
                prep_pos(pname)
                do_scans(pname, mode)
        except Bailout:
            print("okay bailing out ...")
    
    finally:
        sc()
        sc()
        sc()
