import gevent,bliss
key = 'm'
print(f'load sc5337 run ver 12{key}')

FRAMES_PER_FILE=200

load_script('mi1355')
load_script('eigfix_interface')


def patch_run_debug_june15_night2(patch_num=0):
    try:
        pos_list = [
                'CL2S1_13_scan_pos1_0030.json',
                'CL2S1_13_scan_pos2_0031.json',
                'CL2S1_13_scan_pos3_0032.json',
                'CL2S1_13_scan_pos4_0033.json',
                'CL2S1_13_scan_pos5_0034.json',

                    ]
        so()
        fshtrigger()

        # lunchfail
        #mgeig_hws_x()

        mgeig()
        eigerhws(True)
        MG_EH3a.enable('*xmap*')
        #gopos('empty')
        #newdataset(f'bkgd_20ms_b_{key}')
        #umvr(nny,0.005,nnz,0.04)
        #zeronnp()
        #kmap.dkmap(nnp2, -25, 25, 100, 
        #           nnp3, -25, 25, 100, 0.02,
        #           frames_per_file=FRAMES_PER_FILE)    
        for i in range(len(pos_list)):
            gopos(pos_list[i])
            umvr(nny,0.005,nnz,0.04)
            for ii in range(4):
                for iii in range(4):
                    if (i==0) and ((ii*4+iii) < patch_num):
                        pass
                    else:
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        try:
                            with gevent.Timeout(seconds=400):
                                kmap.dkmap(nnp2, 0, 50, 100, 
                                   nnp3, 0, 50, 100, 0.02,
                                   frames_per_file=FRAMES_PER_FILE)    

                        # lunch fail
                        #eig_hws_kmap(nnp2, 0, 50, 100, 
                        #                     nnp3, 0, 50, 100, 0.02,
                        #                     probeonly=False)    

                        except bliss.common.greenlet_utils.killmask.BlissTimeout:
                            print('caught the timeout ...')

                        enddataset()
        sc()
    finally:
        sc()
        sc()

def patch_run_debug_june15_night1(patch_num=0):
    try:
        pos_list = [
                'CL2S2_3_scan_pos1_0026.json',
                'CL2S2_3_scan_pos2_0027.json',
                ]
        so()
        fshtrigger()

        # lunchfail
        #mgeig_hws_x()

        mgeig()
        eigerhws(True)
        MG_EH3a.enable('*xmap*')
        gopos('empty')
        newdataset(f'bkgd_20ms_b_{key}')
        zeronnp()
        kmap.dkmap(nnp2, -25, 25, 100, 
                   nnp3, -25, 25, 100, 0.02,
                   frames_per_file=FRAMES_PER_FILE)    
        for i in range(len(pos_list)):
            gopos(pos_list[i])
            for ii in range(4):
                for iii in range(4):
                    if (i==0) and ((ii*4+iii) < patch_num):
                        pass
                    else:
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        try:
                            with gevent.Timeout(seconds=400):
                                kmap.dkmap(nnp2, 0, 50, 100, 
                                   nnp3, 0, 50, 100, 0.02,
                                   frames_per_file=FRAMES_PER_FILE)    

                        # lunch fail
                        #eig_hws_kmap(nnp2, 0, 50, 100, 
                        #                     nnp3, 0, 50, 100, 0.02,
                        #                     probeonly=False)    

                        except bliss.common.greenlet_utils.killmask.BlissTimeout:
                            print('caught the timeout ...')

                        enddataset()
        sc()
    finally:
        sc()
        sc()


def patch_run(pos_list,patch_num=0):
    try:
        so()
        fshtrigger()
        eig_hws()
        mgeig()
        MG_EH3a.enable('xmap3:*')
        for i in range(len(pos_list)):
            gopos(pos_list[i])
            for ii in range(4):
                for iii in range(4):
                    if (ii*4+iii) < patch_num:
                        pass
                    else:
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        klines(0,50,100,0,50,100,0.02)
                        enddataset()
        sc()
    finally:
        sc()
        sc()

def june14_run():
    try:
        so()
        fshtrigger()
        eig_hws()
        mgeig()
        MG_EH3a.enable('xmap3:*')
        for i in range(len(pos_list)):
            if i == 0:
                gopos(pos_list[i])
                umvr(nnz,0.2)
                for ii in range(4):
                    for iii in range(4):
                        #
                        if (ii*4+iii) < 15:
                            pass
                        else:
                            umv(nnp2,iii*50+25)
                            umv(nnp3,ii*50+25)
                            fn = f'{pos_list[i][:-10]}_lower_patch{ii*4+iii}_{key}'
                            #fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                            newdataset(fn)
                            klines(0,50,100,0,50,100,0.02)
                            enddataset()
            else:
                gopos(pos_list[i])
                umvr(nnz,0.2)
                for ii in range(4):
                    for iii in range(4):
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_lower_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        klines(0,50,100,0,50,100,0.02)
                        enddataset()
        sc()
    finally:
        sc()
        sc()
