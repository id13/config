print('mi1463_xrd - load 5')
load_script('mi1463_tilt')
from math import fabs
def evening_1():
    try:
        kmap.dkmap(nnp2, -30,50,1600,nnp3, -110, 110,41, 0.02)
    finally:
        sc()
        sc()
        sc()

def whexa():
    wm(nnx, nny, nnz, nnu, nnv, nnw)

EPS = 0.02
def isnear(x,y):
    return fabs(x-y) < EPS


def get_vnnxpos(nnvpos):
    # sample 1
    # vnnxpos = 1.365 - nnvpos*1.5669
    
    # snmx
    vnnxpos = 1.399 - nnvpos*1.5425
    
    # snma
    #vnnxpos = 1.382 - nnvpos*1.59875
    return vnnxpos

def check_nnvpos():
    nnvpos = nnv.position
    vnnxpos = get_vnnxpos(nnvpos)
    if not isnear(vnnxpos, nnx.position):
        print(vnnxpos, nnx.position)
        raise RuntimeError(f'inconsistency - nnv {nnv.position}, nnx {nnx.position}')

def nnv_mv(nnvpos):
    curr_nnvpos = nnv.position
    if fabs(nnvpos - curr_nnvpos) > 0.5:
        raise RuntimeError(f'too far displacement of nnv: {nnvpos - curr_nnvpos}')
    check_nnvpos()
    nnxpos = get_vnnxpos(nnvpos)
    mv(nnv, nnvpos)
    mv(nnx, nnxpos)

# nnv -1.6 d nnz 55.0
# nnv 1.6 d nnz 10.0

def vknif_series():
    dooi('vknif_series()')
    for v in np.linspace(0,1.4,7):
        nnv_mv(v)
        kmap.dkmap(nnp2, -15, 10, 25, nnp3, -8,8,320,0.01)
        print('...')
        sleep(3)
        elog_plot(scatter=True)
        print('...i')
        sleep(3)


def vline_series():
    print(-.4,1.2,33)
    dooi('vline_series()')
    for v in np.linspace(-.4,1.2,33):
        nnv_mv(v)
        dscan(nnp3, -15,5,400,0.02)
        print('...i')
        sleep(3)

def sat_night():
    start_key = 'n'

    if True:
        print('SNMX ==================')
        rstp('snmx_final')
        mv(nnv, 0)
        zeronnp()
        snmx_tm=NnvTiltMgr(1.399, 1.5425)
        snmx_tm.nnv_mv(0.0)
        snmx_tm.nnv_mv(0.2)
        snmx_tm.nnv_mv(0.4)
        newdataset(f'{start_key}_snmx_meas')
        umv(Theta, 1.8)
        #kmap.dkmap(nnp2, -23, 27, 50, nnp3, -10,40,50, 0.02)
        kmap.dkmap(nnp2, -23, 27, 1000, nnp3, -10,40,1000, 0.02)
        sleep(3)
        elog_plot(scatter=True)
        snmx_tm.nnv_mv(0.2)
        snmx_tm.nnv_mv(0.0)

    if True:
        print('SNMA ==================')
        rstp('snma_final')
        zeronnp()
        snma_tm=NnvTiltMgr(1.382, 1.59875)
        snma_tm.nnv_mv(-0.2)
        snma_tm.nnv_mv(-0.4)
        snma_tm.nnv_mv(-0.6)
        snma_tm.nnv_mv(-0.9)
        newdataset(f'{start_key}_snma_meas')
        umv(Theta, 0.8)
        #kmap.dkmap(nnp2, -56-2, -31+8, 35, nnp3, 55-8,80+2,35, 0.02)
        kmap.dkmap(nnp2, -56-2, -31+8, 700, nnp3, 55-8,80+2,700, 0.02)
        sleep(3)
        elog_plot(scatter=True)
        snma_tm.nnv_mv(-0.6)
        snma_tm.nnv_mv(-0.4)
        snma_tm.nnv_mv(-0.2)
        snma_tm.nnv_mv(0.0)
    
def sat_night_main():
    try:
        so()
        sleep(4)
        sat_night()
    finally:
        sc()
        sc()
        sc()
