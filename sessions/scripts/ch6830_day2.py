print('load - 1')

START_KEY = 'a'
load_script('ch6830_infra')
def t_r(*p):
    print('rot_scan_series')
    print(f'    {p}')

def t_kmap(*p):
    print('kmap.dkmap')
    print(f'    {p}')
# TEST
def mykmap(*p):
    kmap.dkmap(*p)
#rot_scan_series = t_r
#mykmap = t_kmap

def ch6830_day2():
    # POS A
    rstp('sp1_pos2_0007.json')
    rot_scan_series(f'rot_day2_B_{START_KEY}',
                    -3.0, 3.0, 24,
                    nnp2, -10, 10, 80, nnp3, -10, 10, 80, 0.02)


def ch6830_night1():
    # POS A
    #rstp('ybg22f_pos2_0004.json')
    rstp('ybg22f_pos2_0004.json')
    rot_scan_series(f'rot_night1_A_{START_KEY}',
                    -3.0, 3.0, 120,
                    nnp2, -10, 10, 80, nnp3, -10, 10, 80, 0.02)

    # POS A
    rstp('ybg22f_pos1_0003.json')
    rot_scan_series(f'rot_night1_B_{START_KEY}',
                    -1.5, 1.5, 60,
                    nnp2, -15, 15, 120, nnp3, -15, 15, 120, 0.02)
    # BIG_SCAN C
    newdataset(f'night1_big_scan_C_{START_KEY}')
    mykmap(nnp2, -100, 100, 400, nnp3, -100, 100, 400, 0.02)

    # BIG_SCAN D
    # rstp('ybg22f_pos2_0004.json')
    # newdataset(f'night1_big_scan_D_{START_KEY}')
    # mykmap(nnp2, -100, 100, 400, nnp3, -100, 100, 400, 0.04)

def ch6830_day2_main():
    try:
        so()
        ch6830_day2()
    finally:
        sc()
        sc()
        sc()
