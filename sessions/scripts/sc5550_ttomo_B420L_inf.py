print('version ttomo test sc5550 - 2')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

TEST_ORI_INSTRUCT = """
1,0,0,0
2,0,4.3,0
4,0,0,1
5,0,4.3,1
"""

#index,kappa,omega,absorption
ORI_INSTRUCT = """
"""

#index,kappa,omega,absorption
POS_KAPPA_ORI_INSTRUCT = """
1,0,0,0
2,0,4.3,0
3,0,8.6,0
4,0,12.9,0
5,0,17.2,0
6,0,21.5,0
7,0,25.8,0
8,0,30.1,0
9,0,34.4,0
10,0,38.7,0
11,0,43,0
12,0,47.3,0
13,0,51.6,0
14,0,55.9,0
15,0,60.2,0
16,0,64.5,0
17,0,68.8,0
18,0,73.1,0
19,0,77.4,0
20,0,81.7,0
21,0,86,0
22,0,90.3,0
23,0,94.6,0
24,0,98.9,0
25,0,103.2,0
26,0,107.5,0
27,0,111.8,0
28,0,116.1,0
29,0,120.4,0
30,0,124.7,0
31,0,129,0
32,0,133.3,0
33,0,137.6,0
34,0,141.9,0
35,0,146.2,0
36,0,150.5,0
37,0,154.8,0
38,0,159.1,0
39,0,163.4,0
40,0,167.7,0
41,0,172,0
42,0,176.3,0
43,8,4.15,0
44,8,12.45,0
45,8,20.75,0
46,8,29.05,0
47,8,37.35,0
48,8,45.65,0
49,8,53.95,0
50,8,62.25,0
51,8,70.55,0
52,8,78.85,0
53,8,87.15,0
54,8,95.45,0
55,8,103.75,0
56,8,112.05,0
57,8,120.35,0
58,8,128.65,0
59,8,136.95,0
60,8,145.25,0
61,8,153.55,0
62,8,161.85,0
63,8,170.15,0
64,8,178.45,0
65,8,186.75,0
66,8,195.05,0
67,8,203.35,0
68,8,211.65,0
69,8,219.95,0
70,8,228.25,0
71,8,236.55,0
72,8,244.85,0
73,8,253.15,0
74,8,261.45,0
75,8,269.75,0
76,8,278.05,0
77,8,286.35,0
78,8,294.65,0
79,8,302.95,0
80,8,311.25,0
81,8,319.55,0
82,8,327.85,0
83,8,336.15,0
84,8,344.45,0
85,8,352.75,0
86,16,0,0
87,16,8.5,0
88,16,17,0
89,16,25.5,0
90,16,34,0
91,16,42.5,0
92,16,51,0
93,16,59.5,0
94,16,68,0
95,16,76.5,0
96,16,85,0
97,16,93.5,0
98,16,102,0
99,16,110.5,0
100,16,119,0
101,16,127.5,0
102,16,136,0
103,16,144.5,0
104,16,153,0
105,16,161.5,0
106,16,170,0
107,16,178.5,0
108,16,187,0
109,16,195.5,0
110,16,204,0
111,16,212.5,0
112,16,221,0
113,16,229.5,0
114,16,238,0
115,16,246.5,0
116,16,255,0
117,16,263.5,0
118,16,272,0
119,16,280.5,0
120,16,289,0
121,16,297.5,0
122,16,306,0
123,16,314.5,0
124,16,323,0
125,16,331.5,0
126,16,340,0
127,16,348.5,0
128,24,4.5,0
129,24,13.5,0
130,24,22.5,0
131,24,31.5,0
132,24,40.5,0
133,24,49.5,0
134,24,58.5,0
135,24,67.5,0
136,24,76.5,0
137,24,85.5,0
138,24,94.5,0
139,24,103.5,0
140,24,112.5,0
141,24,121.5,0
142,24,130.5,0
143,24,139.5,0
144,24,148.5,0
145,24,157.5,0
146,24,166.5,0
147,24,175.5,0
148,24,184.5,0
149,24,193.5,0
150,24,202.5,0
151,24,211.5,0
152,24,220.5,0
153,24,229.5,0
154,24,238.5,0
155,24,247.5,0
156,24,256.5,0
157,24,265.5,0
158,24,274.5,0
159,24,283.5,0
160,24,292.5,0
161,24,301.5,0
162,24,310.5,0
163,24,319.5,0
164,24,328.5,0
165,24,337.5,0
166,24,346.5,0
167,24,355.5,0
168,32,0,0
169,32,9.6,0
170,32,19.2,0
171,32,28.8,0
172,32,38.4,0
173,32,48,0
174,32,57.6,0
175,32,67.2,0
176,32,76.8,0
177,32,86.4,0
178,32,96,0
179,32,105.6,0
180,32,115.2,0
181,32,124.8,0
182,32,134.4,0
183,32,144,0
184,32,153.6,0
185,32,163.2,0
186,32,172.8,0
187,32,182.4,0
188,32,192,0
189,32,201.6,0
190,32,211.2,0
191,32,220.8,0
192,32,230.4,0
193,32,240,0
194,32,249.6,0
195,32,259.2,0
196,32,268.8,0
197,32,278.4,0
198,32,288,0
199,32,297.6,0
200,32,307.2,0
201,32,316.8,0
202,32,326.4,0
203,32,336,0
204,32,345.6,0
205,40,5.35,0
206,40,16.05,0
207,40,26.75,0
208,40,37.45,0
209,40,48.15,0
210,40,58.85,0
211,40,69.55,0
212,40,80.25,0
213,40,90.95,0
214,40,101.65,0
215,40,112.35,0
216,40,123.05,0
217,40,133.75,0
218,40,144.45,0
219,40,155.15,0
220,40,165.85,0
221,40,176.55,0
222,40,187.25,0
223,40,197.95,0
224,40,208.65,0
225,40,219.35,0
226,40,230.05,0
227,40,240.75,0
228,40,251.45,0
229,40,262.15,0
230,40,272.85,0
231,40,283.55,0
232,40,294.25,0
233,40,304.95,0
234,40,315.65,0
235,40,326.35,0
236,40,337.05,0
237,40,347.75,0
238,0,0,0
239,0,0,1
240,0,4.3,1
241,0,8.6,1
242,0,12.9,1
243,0,17.2,1
244,0,21.5,1
245,0,25.8,1
246,0,30.1,1
247,0,34.4,1
248,0,38.7,1
249,0,43,1
250,0,47.3,1
251,0,51.6,1
252,0,55.9,1
253,0,60.2,1
254,0,64.5,1
255,0,68.8,1
256,0,73.1,1
257,0,77.4,1
258,0,81.7,1
259,0,86,1
260,0,90.3,1
261,0,94.6,1
262,0,98.9,1
263,0,103.2,1
264,0,107.5,1
265,0,111.8,1
266,0,116.1,1
267,0,120.4,1
268,0,124.7,1
269,0,129,1
270,0,133.3,1
271,0,137.6,1
272,0,141.9,1
273,0,146.2,1
274,0,150.5,1
275,0,154.8,1
276,0,159.1,1
277,0,163.4,1
278,0,167.7,1
279,0,172,1
280,0,176.3,1
"""


def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log',
            resume=-1, start_key="zzzz", absorb=False, test_mode=True, sleep_time=10.0):
        self.absorb = absorb
        self.test_mode= test_mode
        self.sleep_time= sleep_time
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb and self.absorb:
            # no absorption sacns
            self.log('absorption scan active ...')
            dsname = newdataset_base + '_absorb'
        elif (not absorb) and (not self.absorb):
            self.log('diffraction scan active ...')
            dsname = newdataset_base + '_diff'
        else:
            self.log(f'skipping item as list absorb does not match global absorb flag: list={absorb}, global={self.absorb}')
            return

        if self.test_mode:
            self.log(f'skipping dataset creation: {dsname}')
        else:
            newdataset(dsname)
        self.perform_scan()

    def perform_scan(self):
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        print(f'FOV (YxZ): {uly-lly} x {ulz-llz}')

        if self.test_mode:
            self.log('TEST_MODE is active')
            print('scan params:')
            pprint(sp)
            print(f'FOV (YxZ): {uly-lly} x {ulz-llz}')
            print(f'sleeping {self.sleep_time} ...')
            sleep(self.sleep_time)
            return

        lly *= 0.001
        uly *= 0.001
        llz *= 0.001
        ulz *= 0.001
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        t0 = time.time()
        with gevent.Timeout(seconds=180):
            try:
                dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
                self.log('scan successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
            
        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 3*(lly//3)
        uly = 3*(uly//3)
        llz = 3*(llz//3)
        ulz = 3*(ulz//3)

        dy = uly - lly
        dz = ulz - llz

        nitvy = dy//3
        nitvz = dz//3

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def sc5550_main(zkap, start_key, resume=-1, absorb=False, test_mode=True, sleep_time=10.0):
    print('Hi SC-5550! - B_434R_inf_2')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(POS_KAPPA_ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -75,
        uly = 75,
        nitvy = 75,
        llz = -110,
        ulz = 110,
        nitvz = 110,
        expt = 0.005,
        retveloc = 5.0
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key,
        absorb=absorb, test_mode=test_mode, sleep_time=sleep_time)

    # loop over projections
    ttm.mainloop()
