import time

class EXPO:

    expt = 0.02

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_bis_inhday1_1.py ARMED ===")
print('RESUME3')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 1.0

    #if expt < 0.01:
    #    expt = 0.01
    #if expt < 0.02 and nitv < 401:
    #    retveloc = 1.0
    #else:
    #    retveloc = 0.5

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='b' # to be incremented if there is a crash

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_bis_inhday1_1():


    EXPO.expt = 0.06

    #dooul('Pa_plate_OV_PaDr_OV_ul01_1x1_200x250_800x1000_25_0018.json')
    #dooul('C_Pa_plate_OV_PaDr_OV_ul01_1x1_400x500_800x1000_25_0018.json')

    dooul('Pa_plate_OV_PaDrLi_ul01_1x1_400x300_1600x1200_25_1000.json')
    #dooul('C_Pa_plate_OV_PaDrLi_ul01_1x1_800x600_1600x1200_25_1000.json')

    dooul('Pa_plate_OV_Re_LO_Li_ul01_1x1_500x500_2000x2000_25_1002.json')
    #dooul('C_Pa_plate_OV_Re_LO_Li_ul01_1x1_1000x1000_2000x2000_25_1002.json')

    dooul('Pa_plate_OV_PaWeLi_ul01_1x1_300x200_1200x800_25_1003.json')

    dooul('Pa_plate_OV_PaWe_ul01_1x1_300x200_1200x800_25_1001.json')

    dooul('Pa_plate_OV_ReLOLW_ul01_1x1_250x250_1000x1000_25_1004.json')



def hg159_day5_1_check_pos():
    pass
    #gopos('')
