print('load 19')
def safety_net():
	for i in range(1,5):
		timer = 5 - i
		print(f'{timer} sec to interrupt')
		sleep(1)
		print('TOO LATE - wait for next kmap')
		print('='*40)
		
def activate_all():
	mgeig_x()
	MG_EH3a.enable('*r:r*')

def ref_overnight_scan1():
	try:
		#position for ovi_2_50x
		y_move_list = [0.180,0.060,-0.060,-0.180]
		z_move_list = [0.130,0,-0.130]
		y_name_list = [1,2,3,4]
		z_name_list = [1,2,3]
	
		for i in range(0,4):
			for j in range(0,3):
				print("tile_yz="+str(i)+str(j))
				activate_all()
				so()
				fshtrigger()
				newdataset("ovi_2__"+str(y_name_list[i])+"_"+str(z_name_list[j]))
				gopos('ovi_2_50x')
				umvr(nny,y_move_list[i])
				umvr(nnz,z_move_list[j])
				kmap.dkmap(nnp2,-60,60,60,nnp3,-65,65,65,0.05)
				sc()
				safety_net()
		
				
	finally:
		sc()
		sc()

		
def overnight_scan1():

	try:
		#ovi_1_50x
		print("starting ovi_1 scan")
		y_move_list_1 = [0.065,-0.065]
		z_move_list_1 = [0.065,-0.065]
		y_name_list_1 = [1,2]
		z_name_list_1 = [1,2]
			
	
		for i in range(0,2):
			for j in range(0,2):
				print("tile_yz="+str(i)+str(j))
				activate_all()
				so()
				fshtrigger()
				newdataset("ovi_1__"+str(y_name_list_1[i])+"_"+str(z_name_list_1[j]))
				gopos('ovi_1_50x')
				umvr(nny,y_move_list_1[i])
				umvr(nnz,z_move_list_1[j])
				kmap.dkmap(nnp2,-65,65,65,nnp3,-65,65,65,0.05)
				sc()
				safety_net()
				
	except:
		print("ovi_1_50x scanning failed, continue with ovi_41")
		
		
		
	try:	
		
		#ovi_41_50x
		print("starting ovi_41 scan")
		y_move_list_41 = [0.150,0,-0.150]
		z_move_list_41 = [0.130,0,-0.130]
		y_name_list_41 = [1,2,3]
		z_name_list_41 = [1,2,3]
	
		for i in range(0,3):
			for j in range(0,3):
				print("tile_yz="+str(i)+str(j))
				activate_all()
				so()
				fshtrigger()
				newdataset("ovi_41__"+str(y_name_list_41[i])+"_"+str(z_name_list_41[j]))
				gopos('ovi_41_50x')
				umvr(nny,y_move_list_41[i])
				umvr(nnz,z_move_list_41[j])
				kmap.dkmap(nnp2,-75,75,75,nnp3,-65,65,65,0.05)
				sc()
				safety_net()
				
	except:
		print("ovi_41 failed to scan, continue with ovi_42")
		
		
		
		
	try:	
				
		#ovi_42_50x
		print("starting ovi_42 scan")
		y_move_list_42 = [0.260,0.130,0,-0.130,-0.260]
		z_move_list_42 = [0.140,0,-0.140]
		y_name_list_42 = [1,2,3,4,5]
		z_name_list_42 = [1,2,3]
	
		for i in range(0,5):
			for j in range(0,3):
				print("tile_yz="+str(i)+str(j))
				activate_all()
				so()
				fshtrigger()
				newdataset("ovi_42__"+str(y_name_list_42[i])+"_"+str(z_name_list_42[j]))
				gopos('ovi_42_50x')
				umvr(nny,y_move_list_42[i])
				umvr(nnz,z_move_list_42[j])
				kmap.dkmap(nnp2,-65,65,65,nnp3,-70,70,70,0.05)
				sc()
				safety_net()
				
	except:
		print("ovi_42 scan failed, continue with ovi_43")
		
				
		
	try:
		
		#ovi_43_50x
		print("starting ovi_43 scan")
		y_move_list_43 = [0.130,0,-0.130]
		z_move_list_43 = [0.15,0,-0.150]
		y_name_list_43 = [1,2,3]
		z_name_list_43 = [1,2,3]
	
		for i in range(0,3):
			for j in range(0,3):
				print("tile_yz="+str(i)+str(j))
				activate_all()
				so()
				fshtrigger()
				newdataset("ovi_43__"+str(y_name_list_43[i])+"_"+str(z_name_list_43[j]))
				gopos('ovi_43_50x')
				umvr(nny,y_move_list_43[i])
				umvr(nnz,z_move_list_43[j])
				kmap.dkmap(nnp2,-65,65,65,nnp3,-75,75,75,0.05)
				sc()
				safety_net()
	except:
		print("ovi_43 scan failed, script finishes")
				
	finally:
		sc()
		sc()


def overnight_scan_2():
	print("getting ready to scan hair cross-sections")
	xsection_positions = ["thx_5L1_50x","thx_4L_50x"]
	
	for i in xsection_positions:
		print(i)
		try:
			print("sample "+i)
			activate_all()
			so()
			fshtrigger()
			newdataset(i)
			gopos(i)
			kmap.dkmap(nnp2,-20,30,50*2,nnp3,-15,25,40*2,0.05)
			sc() 
						
			
			
		except:
			print("failed scan /n")
			print("bad scan"+i)
			sc()
			
		finally:
			sc()
			sc()
			
			
		safety_net()
			
			
			
