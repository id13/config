import gevent,bliss
key = 'j'
print(f'load sc5337 run ver 10 {key}')

load_script('mi1355')
load_script('eigfix_interface')

#pos_list = [
#'CL1S1_4_N_pos1_0001.json',
#'CL1S1_4_N_o_pos2_0002.json',
#'CL1S1_4_N_o_pos3_0003.json',
#'CL1S1_4_N_p_pos6_0006.json',
#'CL1S1_4_N_p_pos5_0005.json',
#'CL1S1_4_N_p_pos6_0006.json',
#]

def patch_run_debug_notimeout(patch_num=0):
    try:
        pos_list = [
                #'CL2S3_5_scan_pos1_0011.json',
                #'CL2S3_5_scan_pos2_0012.json',
                #'CL2S3_5_scan_pos3_0013.json',
                #'CL2S3_5_scan_pos5_0015.json',
                #'CL2S3_5_scan_pos6_0016.json',
                #'CL2S3_5_scan_pos7_0017.json',
                'CL2S3_5_scan_pos8_0018.json',
                'CL2S3_5_scan_pos9_0019.json',
                'CL2S3_5_scan_pos10_0020.json',
                ]
        so()
        fshtrigger()

        # lunchfail
        #mgeig_hws_x()

        mgeig()
        MG_EH3a.enable('*xmap*')
        gopos('CL2S3_5_empty_0024.json')
        newdataset(f'bkgd_20ms{key}')
        zeronnp()
        eig_hws_kmap(nnp2, -25, 25, 100, 
                     nnp3, -25, 25, 100, 0.02,
                     probeonly=False)    
        for i in range(len(pos_list)):
            gopos(pos_list[i])
            for ii in range(4):
                for iii in range(4):
                    if (i==0) and ((ii*4+iii) < patch_num):
                        pass
                    else:
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        #try:
                            #with gevent.Timeout(seconds=600):
                        eig_hws_kmap(nnp2, 0, 50, 100, 
                                             nnp3, 0, 50, 100, 0.02,
                                             frames_per_file=200)    

                        # lunch fail
                        #eig_hws_kmap(nnp2, 0, 50, 100, 
                        #                     nnp3, 0, 50, 100, 0.02,
                        #                     probeonly=False)    

                        #except bliss.common.greenlet_utils.killmask.BlissTimeout:
                        #    print('caught the timeout ...')

                        enddataset()
        gopos('CL2S3_5_empty_0024.json')
        newdataset(f'bkgd_20ms_after{key}')
        zeronnp()
        eig_hws_kmap(nnp2, -25, 25, 100, 
                     nnp3, -25, 25, 100, 0.02,
                     probeonly=False)    
        newdataset(f'bkgd_10ms{key}')
        eig_hws_kmap(nnp2, -25, 25, 100, 
                     nnp3, -25, 25, 100, 0.01,
                     probeonly=False)   
        gopos('CL2S3_5_rad_test1_0021.json')
        newdataset(f'rad_test1_10ms{key}')
        oneline(-0.05,0.05,100,0.01) 
        gopos('CL2S3_5_rad_test2_0022.json')
        newdataset(f'rad_test2_10ms{key}')
        oneline(-0.05,0.05,100,0.01) 
        sc()
    finally:
        sc()
        sc()


def patch_run(pos_list,patch_num=0):
    try:
        so()
        fshtrigger()
        eig_hws()
        mgeig()
        MG_EH3a.enable('xmap3:*')
        for i in range(len(pos_list)):
            gopos(pos_list[i])
            for ii in range(4):
                for iii in range(4):
                    if (ii*4+iii) < patch_num:
                        pass
                    else:
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        klines(0,50,100,0,50,100,0.02)
                        enddataset()
        sc()
    finally:
        sc()
        sc()

def june14_run():
    try:
        so()
        fshtrigger()
        eig_hws()
        mgeig()
        MG_EH3a.enable('xmap3:*')
        for i in range(len(pos_list)):
            if i == 0:
                gopos(pos_list[i])
                umvr(nnz,0.2)
                for ii in range(4):
                    for iii in range(4):
                        #
                        if (ii*4+iii) < 15:
                            pass
                        else:
                            umv(nnp2,iii*50+25)
                            umv(nnp3,ii*50+25)
                            fn = f'{pos_list[i][:-10]}_lower_patch{ii*4+iii}_{key}'
                            #fn = f'{pos_list[i][:-10]}_patch{ii*4+iii}_{key}'
                            newdataset(fn)
                            klines(0,50,100,0,50,100,0.02)
                            enddataset()
            else:
                gopos(pos_list[i])
                umvr(nnz,0.2)
                for ii in range(4):
                    for iii in range(4):
                        umv(nnp2,iii*50+25)
                        umv(nnp3,ii*50+25)
                        fn = f'{pos_list[i][:-10]}_lower_patch{ii*4+iii}_{key}'
                        newdataset(fn)
                        klines(0,50,100,0,50,100,0.02)
                        enddataset()
        sc()
    finally:
        sc()
        sc()
