print ("mac11 start")
def the_scan():
    dscan(ustrz,-0.1,0.1,80,0.1)

def the_mesh1():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.1)
    
def the_mesh2():
    dmesh(ustrz,-0.1,0.1,80,ustry,-0.2,0.2,2,0.1)
    
def the_mesh3():
    dmesh(ustrz,0,1,500,ustry,-0.2,0.2,2,0.1)
    
def in1127_mac11():
    so()
    fshtrigger()
    mgeig()
    for i in range(3,32):
        pos_name = "nov2202_f01_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[8:])      
        the_mesh2()
        
        
print ("mac11 end")
