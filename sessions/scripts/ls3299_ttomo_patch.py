print('version ttomo test ihls3299 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

#index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 1.800, 0
2, 0, 3.600, 0
3, 0, 5.400, 0
4, 0, 7.200, 0
5, 0, 9.000, 0
6, 0, 10.800, 0
7, 0, 12.600, 0
8, 0, 14.400, 0
9, 0, 16.200, 0
10, 0, 18.000, 0
11, 0, 19.800, 0
12, 0, 21.600, 0
13, 0, 23.400, 0
14, 0, 25.200, 0
15, 0, 27.000, 0
16, 0, 28.800, 0
17, 0, 30.600, 0
18, 0, 32.400, 0
19, 0, 34.200, 0
20, 0, 36.000, 0
21, 0, 37.800, 0
22, 0, 39.600, 0
23, 0, 41.400, 0
24, 0, 43.200, 0
25, 0, 45.000, 0
26, 0, 46.800, 0
27, 0, 48.600, 0
28, 0, 50.400, 0
29, 0, 52.200, 0
30, 0, 54.000, 0
31, 0, 55.800, 0
32, 0, 57.600, 0
33, 0, 59.400, 0
34, 0, 61.200, 0
35, 0, 63.000, 0
36, 0, 64.800, 0
37, 0, 66.600, 0
38, 0, 68.400, 0
39, 0, 70.200, 0
40, 0, 72.000, 0
41, 0, 73.800, 0
42, 0, 75.600, 0
43, 0, 77.400, 0
44, 0, 79.200, 0
45, 0, 81.000, 0
46, 0, 82.800, 0
47, 0, 84.600, 0
48, 0, 86.400, 0
49, 0, 88.200, 0
50, 0, 90.000, 0
51, 0, 91.800, 0
52, 0, 93.600, 0
53, 0, 95.400, 0
54, 0, 97.200, 0
55, 0, 99.000, 0
56, 0, 100.800, 0
57, 0, 102.600, 0
58, 0, 104.400, 0
59, 0, 106.200, 0
60, 0, 108.000, 0
61, 0, 109.800, 0
62, 0, 111.600, 0
63, 0, 113.400, 0
64, 0, 115.200, 0
65, 0, 117.000, 0
66, 0, 118.800, 0
67, 0, 120.600, 0
68, 0, 122.400, 0
69, 0, 124.200, 0
70, 0, 126.000, 0
71, 0, 127.800, 0
72, 0, 129.600, 0
73, 0, 131.400, 0
74, 0, 133.200, 0
75, 0, 135.000, 0
76, 0, 136.800, 0
77, 0, 138.600, 0
78, 0, 140.400, 0
79, 0, 142.200, 0
80, 0, 144.000, 0
81, 0, 145.800, 0
82, 0, 147.600, 0
83, 0, 149.400, 0
84, 0, 151.200, 0
85, 0, 153.000, 0
86, 0, 154.800, 0
87, 0, 156.600, 0
88, 0, 158.400, 0
89, 0, 160.200, 0
90, 0, 162.000, 0
91, 0, 163.800, 0
92, 0, 165.600, 0
93, 0, 167.400, 0
94, 0, 169.200, 0
95, 0, 171.000, 0
96, 0, 172.800, 0
97, 0, 174.600, 0
98, 0, 176.400, 0
99, 0, 178.200, 0
100, 0, 180.000, 0
101, 0, 0.000, 0
102, 5, 0.900, 0
103, 5, 17.264, 0
104, 5, 33.627, 0
105, 5, 49.991, 0
106, 5, 66.355, 0
107, 5, 82.718, 0
108, 5, 99.082, 0
109, 5, 115.445, 0
110, 5, 131.809, 0
111, 5, 148.173, 0
112, 5, 164.536, 0
113, 5, 180.900, 0
114, 5, 197.264, 0
115, 5, 213.627, 0
116, 5, 229.991, 0
117, 5, 246.355, 0
118, 5, 262.718, 0
119, 5, 279.082, 0
120, 5, 295.445, 0
121, 5, 311.809, 0
122, 5, 328.173, 0
123, 5, 344.536, 0
124, 5, 360.900, 0
125, 0, 0.000, 0
126, 10, 0.000, 0
127, 10, 16.364, 0
128, 10, 32.727, 0
129, 10, 49.091, 0
130, 10, 65.455, 0
131, 10, 81.818, 0
132, 10, 98.182, 0
133, 10, 114.545, 0
134, 10, 130.909, 0
135, 10, 147.273, 0
136, 10, 163.636, 0
137, 10, 180.000, 0
138, 10, 196.364, 0
139, 10, 212.727, 0
140, 10, 229.091, 0
141, 10, 245.455, 0
142, 10, 261.818, 0
143, 10, 278.182, 0
144, 10, 294.545, 0
145, 10, 310.909, 0
146, 10, 327.273, 0
147, 10, 343.636, 0
148, 10, 360.000, 0
149, 0, 0.000, 0
150, 15, 8.182, 0
151, 15, 24.545, 0
152, 15, 40.909, 0
153, 15, 57.273, 0
154, 15, 73.636, 0
155, 15, 90.000, 0
156, 15, 106.364, 0
157, 15, 122.727, 0
158, 15, 139.091, 0
159, 15, 155.455, 0
160, 15, 171.818, 0
161, 15, 188.182, 0
162, 15, 204.545, 0
163, 15, 220.909, 0
164, 15, 237.273, 0
165, 15, 253.636, 0
166, 15, 270.000, 0
167, 15, 286.364, 0
168, 15, 302.727, 0
169, 15, 319.091, 0
170, 15, 335.455, 0
171, 15, 351.818, 0
172, 15, 368.182, 0
173, 0, 0.000, 0
174, 20, 0.000, 0
175, 20, 16.364, 0
176, 20, 32.727, 0
177, 20, 49.091, 0
178, 20, 65.455, 0
179, 20, 81.818, 0
180, 20, 98.182, 0
181, 20, 114.545, 0
182, 20, 130.909, 0
183, 20, 147.273, 0
184, 20, 163.636, 0
185, 20, 180.000, 0
186, 20, 196.364, 0
187, 20, 212.727, 0
188, 20, 229.091, 0
189, 20, 245.455, 0
190, 20, 261.818, 0
191, 20, 278.182, 0
192, 20, 294.545, 0
193, 20, 310.909, 0
194, 20, 327.273, 0
195, 20, 343.636, 0
196, 20, 360.000, 0
197, 0, 0.000, 0
198, 25, 8.182, 0
199, 25, 26.182, 0
200, 25, 44.182, 0
201, 25, 62.182, 0
202, 25, 80.182, 0
203, 25, 98.182, 0
204, 25, 116.182, 0
205, 25, 134.182, 0
206, 25, 152.182, 0
207, 25, 170.182, 0
208, 25, 188.182, 0
209, 25, 206.182, 0
210, 25, 224.182, 0
211, 25, 242.182, 0
212, 25, 260.182, 0
213, 25, 278.182, 0
214, 25, 296.182, 0
215, 25, 314.182, 0
216, 25, 332.182, 0
217, 25, 350.182, 0
218, 25, 368.182, 0
219, 0, 0.000, 0
220, 30, 0.000, 0
221, 30, 18.000, 0
222, 30, 36.000, 0
223, 30, 54.000, 0
224, 30, 72.000, 0
225, 30, 90.000, 0
226, 30, 108.000, 0
227, 30, 126.000, 0
228, 30, 144.000, 0
229, 30, 162.000, 0
230, 30, 180.000, 0
231, 30, 198.000, 0
232, 30, 216.000, 0
233, 30, 234.000, 0
234, 30, 252.000, 0
235, 30, 270.000, 0
236, 30, 288.000, 0
237, 30, 306.000, 0
238, 30, 324.000, 0
239, 30, 342.000, 0
240, 30, 360.000, 0
241, 0, 0.000, 0
242, 35, 9.000, 0
243, 35, 29.000, 0
244, 35, 49.000, 0
245, 35, 69.000, 0
246, 35, 89.000, 0
247, 35, 109.000, 0
248, 35, 129.000, 0
249, 35, 149.000, 0
250, 35, 169.000, 0
251, 35, 189.000, 0
252, 35, 209.000, 0
253, 35, 229.000, 0
254, 35, 249.000, 0
255, 35, 269.000, 0
256, 35, 289.000, 0
257, 35, 309.000, 0
258, 35, 329.000, 0
259, 35, 349.000, 0
260, 35, 369.000, 0
261, 0, 0.000, 0
262, 40, 0.000, 0
263, 40, 20.000, 0
264, 40, 40.000, 0
265, 40, 60.000, 0
266, 40, 80.000, 0
267, 40, 100.000, 0
268, 40, 120.000, 0
269, 40, 140.000, 0
270, 40, 160.000, 0
271, 40, 180.000, 0
272, 40, 200.000, 0
273, 40, 220.000, 0
274, 40, 240.000, 0
275, 40, 260.000, 0
276, 40, 280.000, 0
277, 40, 300.000, 0
278, 40, 320.000, 0
279, 40, 340.000, 0
280, 40, 360.000, 0
281, 0, 0.000, 0
282, 45, 10.000, 0
283, 45, 32.500, 0
284, 45, 55.000, 0
285, 45, 77.500, 0
286, 45, 100.000, 0
287, 45, 122.500, 0
288, 45, 145.000, 0
289, 45, 167.500, 0
290, 45, 190.000, 0
291, 45, 212.500, 0
292, 45, 235.000, 0
293, 45, 257.500, 0
294, 45, 280.000, 0
295, 45, 302.500, 0
296, 45, 325.000, 0
297, 45, 347.500, 0
298, 45, 370.000, 0
299, 0, 0.000, 0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,tighti_y,tighti_z) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['tighti_y'],
            sp['tighti_z'] 
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        print('would start now')
        print(tighti_y, tighti_z)
        
        cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        print('move to patch1 (upper left)')
        umvr(nny,-0.0415,nnz,-0.0465)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch1  (upper left)')
                lly_new = lly + tighti_y
                llz_new = llz + tighti_z
                dy_new = uly - lly_new
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)

                kmap.dkmap(
                    nnp5, lly_new, uly ,nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch1 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
        
        print('move to patch 2 (upper right')
        umvr(nny,0.083)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch2')
                uly_new = uly-tighti_y
                llz_new = llz+tighti_z
                dy_new = uly_new - lly
                dz_new = ulz - llz_new
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz_new, ulz, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch2 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch3 (lower left)')
        umvr(nny,-0.083, nnz, 0.093)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch3  (lower left)')
                lly_new = lly + tighti_y
                ulz_new = ulz - tighti_z
                dy_new = uly - lly_new
                dz_new = ulz_new- llz
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly_new, uly, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch3 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move to patch4 (lower right)')
        umvr(nny,0.083)
        with gevent.Timeout(seconds=150):
            try:
                # put the nano scan ...
                print('patch4  (lower right)')
                uly_new = uly-tighti_y
                ulz_new = ulz-tighti_z
                dy_new = uly_new - lly
                dz_new = ulz_new - llz
                nitvy_new = int(dy_new//0.5)
                nitvz_new = int(dz_new//0.5)
                kmap.dkmap(
                    nnp5, lly, uly_new, nitvy_new,
                    nnp6, llz, ulz_new, nitvz_new,
                    expt, frames_per_file = nitvz*10)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan patch4 successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
                
        print('move back to cen')
        umvr(nny,-0.0415,nnz,0.0465)
        #time.sleep(5)
        

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    print(v)
                    print(w)
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    if 'tighti' == mode:
                        tighti_t = (tighti_y, tighti_z) = tuple(map(float, w[1:]))
                        print(f'process_instructions - {tighti_y}{tighti_z}')
                        print(tighti_t)
                        self.adapt_tighti_scanparams(tighti_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//0.5)
        nitvz = int(dz//0.5)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z
    def adapt_tighti_scanparams(self, tighti_t):
        print(tighti_t)
        (tighti_y,tighti_z) = tighti_t
        sp = self.scanparams
        sp['tighti_y'] = tighti_y
        sp['tighti_z'] = tighti_z


def ls3299_ttomo_main(zkap, start_key, resume=-1):

    
    print('Hi IH-MI_1531!')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -45,
        uly = 45,
        nitvy = 180,
        llz = -50,
        ulz = 50,
        nitvz = 200,
        expt = 0.002,
        tighti_y = 0,
        tighti_z = 0
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
