import h5py
import numpy as np
import matplotlib.pyplot as plt
import time

def read_scan(path,scan_no,counter):
	f = h5py.File(path,mode='r')
	dat_str = '/%d.1/measurement/%s'%(scan_no,counter)
	dim0_str = '/%d.1/technique/dim0'%(scan_no) 
	dim1_str = '/%d.1/technique/dim1'%(scan_no)
	print(dat_str)
	dat_back = f[dat_str][...]
	dim0=f[dim0_str][...]
	dim1=f[dim1_str][...]
	dat_ret = np.reshape(dat_back,(dim1,dim0))
	return dat_ret
	

def do_stupid():
	kmap.dkmap(nnp2,-50,50,50,nnp3,-50,50,45,0.01)
	time.sleep(1)
	dat_return = read_scan(SCAN_SAVING.filename,4,'xmap3_det0_Ca')
	plt.matshow(dat_return)
	plt.show()
	
def do_nanodiff_zombie():
	so()
	newdataset('nanodiffraction_rock_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 16.5
	theta_incr = 0.04
	max_steps = 75
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-120,120,240,nnp3,-120,120,240,exp_time)


def do_nanodiff_tiny():
	so()
	newdataset('bm_fu_c')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 300
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_tiny2():
	so()
	newdataset('bm_fu_roi3_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 480
	rep = 15
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_tiny3():
	so()
	newdataset('nobm_fu_roi3_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 480
	rep = 15
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_bighope1():
	so()
	newdataset('bm_fu_roi1_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 480
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_bighope2():
	so()
	newdataset('bm_fu_ippolype_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 480
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_nohope():
	so()
	newdataset('bm_fu_mutant_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 600
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()


def do_nanodiff_nohope2():
	so()
	newdataset('bm_fu_mutant_roi2_a')
	fshtrigger()
	exp_time = 0.005
	sleep_time = 300
	rep = 10
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-30,30,120,nnp3,-30,30,120,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
def do_nanodiff_nohope3():
	so()
	newdataset('bm_fu_b_splinter_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 30
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()

def do_nanodiff_nohope4():
	so()
	newdataset('bm_fu_b_splinter_b')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 600
	rep = 60
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_nohope5():
	so()
	newdataset('bm_fu_spiderman_a')
	fshtrigger()
	exp_time = 0.004
	sleep_time = 300
	rep = 60
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_smarter():
	so()
	newdataset('bm_fu_feynman_a')
	fshtrigger()
	exp_time = 0.002
	sleep_time = 300
	rep = 30
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_smarter2():
	so()
	newdataset('bm_fu_musk_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 30
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()

def do_nanodiff_smarter3():
	so()
	newdataset('bm_fu_bezos_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 30
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()

def do_nanodiff_rockingchair():
	so()
	newdataset('rocking_chair_marie_a')
	fshtrigger()
	#theta backlash corr
	theta_start = 16.5
	theta_incr = 0.04
	max_steps = 75
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-120,120,240,nnp3,-60,60,240,exp_time)
	sc()
	sc()
	sc()

def do_nanodiff_dirtyharry2():
	so()
	#newdataset('zia_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()


def do_nanodiff_dirtyharry3():
	so()
	#newdataset('zia_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_mcb():
	so()
	newdataset('aj_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()

def do_nanodiff_mcb2():
	so()
	#newdataset('aj_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 300
	rep = 20
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	sc()
	
def do_nanodiff_albertII():
	so()
	newdataset('caroline_a')
	fshtrigger()
	exp_time = 0.0015
	sleep_time = 600
	rep = 30
	for ii in range (0,rep):
		print('---- cycle number ---')
		print(ii)
		kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,exp_time)
		time.sleep(sleep_time)
	fshtrigger()
	mgeig_x()
	kmap.dkmap(nnp2,-20,20,80,nnp3,-20,20,80,0.005)
	newdataset('stephanie_a')
	umvr(nny,-0.2)
	kmap.dkmap(nnp2,-120,120,480,nnp3,-60,60,240,0.025)
	umvr(nny,0.2)
	kmap.dkmap(nnp2,-120,120,480,nnp3,-60,60,240,0.025)
	umvr(nny,0.2)
	kmap.dkmap(nnp2,-120,120,480,nnp3,-60,60,240,0.025)
	umvr(nny,0.2)
	kmap.dkmap(nnp2,-120,120,480,nnp3,-60,60,240,0.025)
	sc()
