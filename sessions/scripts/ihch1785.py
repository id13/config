print('ihsc1785 (cathy clay  - load 1)')

# loading omega map capability
load_script('ch6830_infra')
START_KEY = 'a'


_POS = '''
'''

def ihsc1785_overview_scan():
    print('overview: loff_kmap(...)')
    loff_kmap(nnp2, -80,80, 160, nnp3, -80,80, 160, 0.02)

def ihsc1785_overview_scan_2():
    print('overview2: loff_kmap(...)')
    loff_kmap(nnp2, -80,80, 320, nnp3, -80,80, 320, 0.02)

def ihsc1785_omega_scan(label):
    rot_label = f'rot_{label}'
    print('rot_label', rot_label)
    print('rot_scan_series(...)')
    rot_scan_series(rot_label, -1, 1, 10, nnp2, -30,30, 60, nnp3, -20,20,40, 0.02)

def ihsc1785_setupos(posname, intent):
    if posname.endswith('.json'):
        _nm = posname[:-5]
        dsname = f'{_nm[:-5]}_{intent}'
    elif posname.endswith('._pos'):
        dsname = f'{_nm[:-5]}_{intent}'
    else:   
        dsname = posname
    
    print ('='*40,dsname)
    rstp(posname)
    newdataset(f'{dsname}_{START_KEY}')
    return dsname

def strtolist(s):
    ll = s.split('\n')
    ll = [l.strip() for l in ll]
    ll = [l for l in ll if l]
    return ll

def ihsc1785_saturday_night():
    plist = strtolist(ES1403_OG_POS_2)
    for p in plist:
        label = ihsc1785_setupos(p, 'onepos')
        ihsc1785_omega_scan(label)
        ihsc1785_overview_scan()
    plist = strtolist(ES1403_OG_POS_2)
    for p in plist:
        label = ihsc1785_setupos(p, 'onepos2')
        ihsc1785_overview_scan_2()


def dark_scan():
    newdataset(f'empty_{START_KEY}')
    for expt in np.linspace(0.005, .055, 11):
        loff_kmap(nnp2, -10,10, 25, nnp3, -10,10, 25, expt)

def empty_scan():
    newdataset(f'empty_{START_KEY}')
    for expt in np.linspace(0.005, .055, 11):
        loff_kmap(nnp2, -10,10, 100, nnp3, -10,10, 100, expt)

def ihsc1785_pixcalib_main():
    try:
        sc()
        dark_scan()
        so()
        sleep(10)
        empty_scan()
    finally:
        sc()
        sc()
        sc()
