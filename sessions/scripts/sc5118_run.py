print('load sc5118 ver 3')
prefix = 'g'

def sc5118_oct11_overnight():
    mgeig()
    fshtrigger()
    so()
    try:
        fn = 'mount05_VL321_RT1_0002.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()

        fn = 'mount05_VL352_107C_0001.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()

        fn = 'mount05_VL321_RT_0000.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()
        
        
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()

def sc5118_oct10_overnight():
    mgeig()
    fshtrigger()
    so()
    try:
        fn = 'mount04_VL_352_1_0000.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()

        fn = 'mount04_VL_321_best_0004.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,8,8,5)
        enddataset()

        fn = 'mount04_VL_352_0003.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,8,8,5)
        enddataset()
        
        
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()

def sc5118_oct9_overnight():
    mgeig()
    fshtrigger()
    so()
    try:
        fn = 'mount03_TPOT_1_0007.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,9,4,5)
        enddataset()

        fn = 'mount03_DAPOT_0005.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()

        fn = 'mount03_TPAT_0004.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()
        
        fn = 'mount03_ADPAT_0003.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()
        
        fn = 'mount03_DAPAT_0002.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,99,0.3,99,0.02,7,7,5)
        enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()

def sc5118_overnight():
    mgeig()
    fshtrigger()
    so()
    try:
        fn = 'mount02_BB13_0000.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.48,159,0.48,159,0.01,5,2,5)
        enddataset()

        fn = 'mount02_PC13_0012.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.48,159,0.48,159,0.02,6,2,5)
        enddataset()

        fn = 'mount02_PC11_0025.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.48,159,0.48,159,0.01,4,2,5)
        enddataset()
        
        fn = 'mount02_PBA_0037.json'
        gopos(fn)
        dn = f"{fn[:-10]}_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.48,159,0.48,159,0.02,5,2,5)
        enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()


def sc5118_afternoon():
    mgeig()
    fshtrigger()
    so()
    try:
        gopos('DAPOT_FRESH_RT_1_sub1_0000.json')
        dn = f"DAPOT_FRESH_RT_1_sub1_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.05,24,0.02,9,0.02,1,1,5)
        enddataset()

        gopos('DAPOT_FRESH_RT_1_sub2_0001.json')
        dn = f"DAPOT_FRESH_RT_1_sub2_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.3,149,0.3,149,0.02,1,1,5)
        enddataset()
        
        gopos('DAPOT_FRESH_RT_1_sub3_0002.json')
        dn = f"DAPOT_FRESH_RT_1_sub3_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(0.1,49,0.04,19,0.02,1,1,5)
        enddataset()
        
        gopos('DAPOT_FRESH_RT_1_sub4_0003.json')
        for __ in range(10):
            dn = f"DAPOT_FRESH_RT_1_sub4_{prefix}_{__}"
            print(dn)
            newdataset(dn)
            dkpatchmesh(0.05,24,0.05,24,0.02,1,1,5)
            enddataset()

        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()


