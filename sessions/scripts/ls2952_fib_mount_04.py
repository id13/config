print('ls2952_fib_mount_04 load 2')

STARTKEY = 'a'

def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_fib_mount_04():

    try:

        gop_newds('fib_mount_04_A3_bis_scanstart_0063.json')
        dkpatchmesh(0.9, 450, 2.0, 200, 0.02, 1, 1)
        enddataset()

        gop_newds('fib_mount_04_A3_ter_scanstart_0067.json')
        dkpatchmesh(1.0, 500, 3.0, 300, 0.02, 1, 1)
        enddataset()

        gop_newds('fib_mount_04_A4_bis_scanstart_0069.json')
        dkpatchmesh(0.7, 350, 1.0, 100, 0.02, 1, 1)
        enddataset()

        gop_newds('fib_mount_04_A1_bis_scanstart_0070.json')
        dkpatchmesh(0.6, 300, 2.2, 110, 0.02, 2, 1)
        enddataset()


    finally:
        sc()
        sc()
