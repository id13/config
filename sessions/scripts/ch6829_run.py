print('ch6829 v1')


start_key = 'a'


WAXS_list = [
#'position2',
#'position3',
#'position4',
#'position5',
#'position1',
#'position2',
#'position3',

#'position3',
#'position4',

#'position3',
#'position4',
#'position5',
#'position1',
#'position2',
#'position3',

#'position1',
#'position2',

#'position3',
#'position4',
#'position5',
#'position6',

#'position1',
#'position2',
#'position3',
#'position4',

#'position5',

#'position1',

#'position3',
#'position4',

#'position1',

#'position1',


#'position1',

#'position1',
#'position2',
#'position3',
#'position4',

#'position1',
#'position2',

#'position1',

'position1',


]

List = [
#'position2',

#'position2',

#'position2',

'position2',

]

vertical = [
#'position1',
#'position2',
#'position3',

]

horizontal_short = [

#'position4',

]

horizontal_long = [

#'position5',
#'position6',

]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t,fast_Z=False):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    if fast_Z:
        kmap.dkmap(nnp3,-1*zrange,zrange,zstep,nnp2,-1*yrange,yrange,ystep,exp_t)
    else:
        kmap.dkmap(nnp2,-1*yrange,yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    rmgeigx()
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    #print(name)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()
    #sleep(2)
    
def gopos_displacement_kmap_scan(pos,nb_d,d,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,d_axis='Y',deltaZ=0):
    so()
    fshtrigger()
    rmgeigx()
    gopos(pos)
    umvr(nnz,deltaZ*0.001)
    for i in range(nb_d):
        name = f'{pos}_{start_key}_{i}'
        #print(name)
        newdataset(name)
        kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
        enddataset()
        if d_axis == 'Y':
            umvr(nny,d*0.001)
        else:
            umvr(nnz,d*0.001)
        sleep(15)
        #sleep(2)


def run_Oct_24th_afternoon():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:            
            gopos_kmap_scan(i,start_key,0,50,50,2,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()
        
def run_Oct_24th_night():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
                gopos_displacement_kmap_scan(i,21,200,start_key,0,100,100,2,2,0.05)
                sleep(15)

        sc()
    except:
        sc()
        
def run_Oct_25th_morning():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,19,200,start_key,0,100,100,5,5,0.05,d_axis='Z')
            sleep(15)
        gopos_kmap_scan('healed',start_key,0,100,100,5,5,0.05)
        sleep(15)
        gopos_kmap_scan('healed2',start_key,0,100,100,5,5,0.05)
        sleep(15)

        sc()
    except:
        sc()
        
def run_Oct_25th_morning2():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,4,200,start_key,0,50,100,.5,1,0.03,d_axis='Z')
            sleep(15)
        gopos_kmap_scan('bkg',start_key,0,20,20,.5,1,0.03)
        sleep(15)

        sc()
    except:
        sc()
        
def run_Oct_25th_evening():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,1,200,start_key,0,50,100,.5,1,0.03,d_axis='Z')
            sleep(15)
        sc()
    except:
        sc()
        
def run_Oct_25th_night():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,2,200,start_key,0,100,100,1,1,0.05,d_axis='Z')
            sleep(15)
        sc()
    except:
        sc()
        
def run_Oct_26th_morning():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,100,100,1,0.5,0.03)
            sleep(15)
        sc()
    except:
        sc() 
        
def run_Oct_26th_noon():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,25,25,1,0.5,0.03)
            sleep(15)
        sc()
    except:
        sc() 


def run_Oct_26th_afternoon():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,1,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
            sleep(15)
        #umv(ndetx,-300)
        for i in List:
            gopos_displacement_kmap_scan(i,2,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
            sleep(15)
            
        gopos_kmap_scan('empty',start_key,0,25,25,1,0.5,0.03)
        sleep(15)
        
        sc()
    except:
        sc()
        
        
def run_Oct_26th_afternoon2():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,50,100,1,0.5,0.03)
            sleep(15)
        sc()
    except:
        sc() 
        
        
def run_Oct_26th_evening():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,4,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)
        #umv(ndetx,-300)
        for i in List:
            gopos_displacement_kmap_scan(i,5,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)
            
        gopos_kmap_scan('empty1',start_key,0,25,25,1,0.5,0.03)
        sleep(15)
        
        sc()
    except:
        sc()



def run_Oct_26th_night():
    try:
        #umv(ndetx,-300)
        for i in vertical:
                
            gopos_displacement_kmap_scan(i,2,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
            sleep(15)
        
        gopos_kmap_scan('empty',start_key,0,25,25,1,0.5,0.03)
        sleep(15)
    
        for i in horizontal_short:
            gopos_displacement_kmap_scan(i,4,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)
        for i in horizontal_long:
            gopos_displacement_kmap_scan(i,7,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)

        gopos_kmap_scan('empty2',start_key,0,25,25,1,0.5,0.03)
        sleep(15)    

        
        sc()
    except:
        sc()
        
        
def run_Oct_26th_night2():
    try:
        #umv(ndetx,-300)
        for i in vertical:
            
            if i=='position1':
                
                gopos_displacement_kmap_scan(i,1,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z',deltaZ=200)
                sleep(15)
            else:
                
                gopos_displacement_kmap_scan(i,2,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
                sleep(15)
        
        gopos_kmap_scan('empty',start_key,0,25,25,1,0.5,0.03)
        sleep(15)
        
        #umv(ndetx,-300)
        for i in horizontal_short:
            gopos_displacement_kmap_scan(i,4,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)
        for i in horizontal_long:
            gopos_displacement_kmap_scan(i,7,200,start_key,0,100,50,1,0.5,0.03,d_axis='Y')
            sleep(15)

        gopos_kmap_scan('empty2',start_key,0,25,25,1,0.5,0.03)
        sleep(15)    

        
        sc()
    except:
        sc()
        
        
def run_Oct_27th_morning():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,100,100,1,0.5,0.07)
            sleep(15)
            
            
        gopos_kmap_scan('roi1',start_key,0,100,75,1,0.5,0.07)
        sleep(15)
        
        gopos_kmap_scan('roi2',start_key,0,50,50,1,0.5,0.07)
        sleep(15)
        
        gopos_kmap_scan('empty',start_key,0,25,25,1,0.5,0.07)
        sleep(15)
            
            
        sc()
    except:
        sc() 


def run_Oct_27th_noon():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,3,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
            sleep(15)
        #umv(ndetx,-300)
        for i in List:
            gopos_displacement_kmap_scan(i,2,200,start_key,0,50,100,1,0.5,0.03,d_axis='Z')
            sleep(15)
            
        gopos_kmap_scan('empty',start_key,0,25,25,1,0.5,0.03)
        sleep(15)
        
        sc()
    except:
        sc()


def run_Oct_27th_afternoon():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,2,150,start_key,0,75,75,1,1,0.03,d_axis='Z')
            sleep(15)
            
        gopos_kmap_scan('empty',start_key,0,25,25,1,1,0.03)
        sleep(15)
        
        sc()
    except:
        sc()


def run_Oct_27th_evening():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,2,200,start_key,0,100,100,1,1,0.03,d_axis='Z')
            sleep(15)
            
        gopos_kmap_scan('empty',start_key,0,25,25,1,1,0.03)
        sleep(15)
        
        sc()
    except:
        sc()



def run_Oct_27th_night():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,6,100,start_key,0,50,50,1,1,0.2,d_axis='Y')
            sleep(15)
             
        gopos_kmap_scan('empty',start_key,0,25,25,1,1,0.2)
        sleep(15)
        
        sc()
    except:
        sc()


def run_Oct_27th_night2():
    try:
        #umv(ndetx,-300)
        for i in WAXS_list:
            gopos_displacement_kmap_scan(i,3,200,start_key,0,100,100,1,1,0.1,d_axis='Z')
            sleep(15)
        
        for i in List:
            gopos_displacement_kmap_scan(i,2,200,start_key,0,100,100,1,1,0.1,d_axis='Y')
            sleep(15)
        
        gopos_kmap_scan('empty',start_key,0,25,25,1,1,0.1)
        sleep(15)
        
        sc()
    except: 
        sc()






































