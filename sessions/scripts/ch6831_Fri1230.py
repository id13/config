print("ch6831 : load - 14")
import numpy as np

START_KEY = 'd'

SIM_BFLG = False

EXPT = 0.02

XX = 10

def init_one(posname):
    print(f'initializing: {posname}')
    rstp(posname)
    if SIM_BFLG:
        print(f'newdataset: {START_KEY}_{posname}')
    else:
        newdataset(f'{START_KEY}_{posname}')

def tip_scan():
    if SIM_BFLG:
        print('tip_scan')
    else:
        kmap.dkmap(nnp2, -7,7, 14*XX, nnp3, -5, 15, 20*XX, EXPT)

def shaft_scan():
    if SIM_BFLG:
        print('shaft scan...')
    else:
        kmap.dkmap(nnp2, -20,20, 40*XX, nnp3, -15, 15, 30*XX, EXPT)

def big_scan():
    if SIM_BFLG:
        print('big scan ...')
    else:
        kmap.dkmap(nnp2, -35, 31, 33*XX, nnp3, -30, 50, 40*XX, EXPT)

POS_LIST_day_1 = '''
Y_ht1_tip
Y_ht1_mid
Y_ht2_tip
Y_ht2_mid
Y_ht3_tip
Y_ht3_mid
Y_hb1_bottom
Y_hb1_mid
Y_hb2_bottom
Y_hb2_mid
'''

POS_LIST = '''
Y_ht1_mid
Y_ht1_tip
Y_ht2_mid
Y_ht2_tip
Y_hb1_bottom
Y_hb1_mid
Y_hb2_bottom
Y_hb2_mid
Y_hb3_connect
Y_hb3_mid
Y_hb3_socket
'''


def make_poslist():
    ll = POS_LIST.split('\n')
    ll = [x.strip() for x in ll if x]
    return ll

def ch6831_multi():
    posll = make_poslist()
    for p in posll:
        init_one(p)
        if 'tip' in p:
            print('========',p,'tip')
            tip_scan()
        elif 'mid' in p or 'bottom' in p:
            print('========',p,'shaft')
            shaft_scan()
        elif 'connect' in p or 'socket' in p:
            print('========',p,'big')
            big_scan()
        else:
            raise ValueError(p)

def rotseries_j9_6_mid_lam():
    for th in np.linspace(-20.0,10.0,31):
        print('==========:', th)
        mv(Theta, th)
        kmap.dkmap(nnp2, -15,15,60, nnp3, -11,13, 48,0.02)


def hex_recover():
    nnx.sync_hard()
    nny.sync_hard()
    nnz.sync_hard()
    nnu.sync_hard()
    nnv.sync_hard()
    nnw.sync_hard()

def ch6831_main():
    try:
        so()
        sleep(2)
        #ch6831_day2_1()
        ch6831_multi()
        #rotseries_j9_6_mid_lam()
    finally:
        sc()
        sc()
        sc()
