print('Martin simple template - load 1')
def martin_example():
    try:
        # Sample 1
        umv(nnz,0)
        umv(nny,0)
        
        
        newdataset('S1_301123_pos1')
        rstp('S1_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S1_301123_pos2')
        rstp('S1_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -100, 100, 400, 0.02)
        
        newdataset('S1_301123_pos5')
        rstp('S1_pos5')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -20, 20, 80, 0.02)
        
        newdataset('S1_301123_pos3')
        rstp('S1_pos3')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)
        
        newdataset('S1_301123_pos4')
        rstp('S1_pos4')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)       
        
        # Sample 2
        umv(nnz,0)
        umv(nny,0)
        
        newdataset('S2_301123_pos1')
        rstp('S2_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S2_301123_pos2')
        rstp('S2_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -10, 10, 40, 0.02)
        
        newdataset('S2_301123_pos3')
        rstp('S2_pos3')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)
        
        newdataset('S2_301123_pos4')
        rstp('S2_pos4')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)             
        
        # Sample 3
        umv(nnz,0)
        umv(nny,0)
        newdataset('S3_301123_pos1')
        rstp('S3_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)
        
        newdataset('S3_301123_pos2')
        rstp('S3_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S3_301123_pos3')
        rstp('S3_pos3')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        #newdataset('S3_301123_pos4')
        #rstp('S3_pos4')
        #loff_kmap(nnp2, -35, 35, 700, nnp3, -20, 20, 80, 0.02)        
        
     
        # template
        # pollute with commands below ...
        # print('Hallo Martin!')
        # kmap.dkmap(nnp2, -1,1,10, nnp3, -2,3,30,0.0142)
        # rstp('willi')
        # loff_kmap(nnp2, -1,1,10, nnp3, -2,3,30,0.0142)
        # rstp('Milowisch')
        # rstp('erwin')
        # mvr(nny, 0.02)
        # rstp('Erwin_Huber')
        # end pollution
    
    
    finally:
       sc()
       sc()
       sc()

def martin_lastnight():
    try:
 
        # Sample 3
        umv(nnz,0)
        umv(nny,0)
        
        newdataset('S3_011223_pos3')
        rstp('S3_pos3')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)
               
        newdataset('S3_011223_pos2')
        rstp('S3_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S3_011223_pos1')
        rstp('S3_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -100, 100, 400, 0.02) 
 
 
        # Sample 1
        umv(nnz,0)
        umv(nny,0)
        
        newdataset('S1_011223_pos1')
        rstp('S1_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)
        
        newdataset('S1_011223_pos2')
        rstp('S1_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S1_011223_pos3')
        rstp('S1_pos3')
        loff_kmap(nnp2, -45, 45, 900, nnp3, -70, 70, 280, 0.02)
        
        newdataset('S1_011223_pos4')
        rstp('S1_pos4')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)       
        
        # Sample 2
        umv(nnz,0)
        umv(nny,0)
        
        newdataset('S2_011223_pos1')
        rstp('S2_pos1')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S2_011223_pos2')
        rstp('S2_pos2')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -35, 35, 140, 0.02)
        
        newdataset('S2_011223_pos3')
        rstp('S2_pos3')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -20, 20, 80, 0.02)
        
        newdataset('S2_011223_pos4')
        rstp('S2_pos4')
        loff_kmap(nnp2, -35, 35, 700, nnp3, -70, 70, 280, 0.02)             
        
      


    
    
    finally:
       sc()
       sc()
       sc()
