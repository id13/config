def patch_t63():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=5    ,  nj=2    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='b',
        dsname_tag = 't63_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t63'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def patch_t36_1():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=1    ,  nj=4    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='b',
        dsname_tag = 't36_1_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t36_1'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)


def patch_t65_3():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=3    ,  nj=4    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='b',
        dsname_tag = 't65_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t65_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def patch_t69():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,150,
            -hdj,hdj,150,
            0.02),
        ni=2    ,  nj=4    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='b',
        dsname_tag = 't69_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'patch_t69'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def fine_patch_t65_3():

    clear_abort()
    the_sim_mode = dict(
        kmap = False,
        dset = False,
        rstp = False
    )

    # half width of the scan
    hdi=75.0
    hdj=75.0

    patchparams = dict(
        kpartial = (
            -hdi,hdi,600,
            -hdj,hdj,600,
            0.02),
        ni=1    ,  nj=1    ,
        di=2*hdi*0.001 ,  dj=2*hdj*0.001,
        start_key='a',
        dsname_tag = 'fine_t65_3_hv',
        sim_mode=the_sim_mode
    )
    coarse_params = dict(
        sim_mode = the_sim_mode,
        patchparams = patchparams,
        posname = 'fine_patch_t65_3'
    )
    print('='*20)
    print(coarse_params)
    print('='*20)
    doo_one_coarse(**coarse_params)

def mount2_main():
    try:
        #patch_t63()
        #patch_t36_1()
        #patch_t65_3()
        #patch_t69()
        fine_patch_t65_3()
    finally:
        sc()
        #sc()
        #sc()



