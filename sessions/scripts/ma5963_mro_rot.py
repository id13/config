import numpy as np


print('mro_rot - load 1')
START_KEY = 'b'

def doo_generic_nnpscan(nnpfast_mot, nnpslow_mot, scandir_tag):
    print('==== checking things within doo_generic_nnpscan ...')
    try:
        if scandir_tag == 'vert':
            assert nnpfast_mot.name == 'nnp2' and nnpslow_mot.name == 'nnp3'
        elif scandir_tag == 'hor':
            assert nnpfast_mot.name == 'nnp3' and nnpslow_mot.name == 'nnp2'
        else:
            print(f'ERROR: illegal config:\nfastmot=nnpfast_mot.name, slowmot=nnpslow_mot.name, scandir_tag={scandir_tag}')
    except AssertionError:
         print('ERROR: fast', nnpfast_mot.name, 'slow:', nnpslow_mot.name, 'direction:', scandir_tag)
         raise RuntimeError()

    # ############### scan params 
    ll0 = 10.0
    ul0 = 160.0
    n0 = 160*2
    ll1 = 0.0
    ul1 = 5.0
    n1 = 6
    expt = 0.05

    print (f'==== performing scan:\n    {(nnpfast_mot.name, ll0, ul0, n0, nnpslow_mot.name, ll1, ul1, n1, 0.05)}')
    kmap.dkmap(nnpfast_mot, ll0, ul0, n0, nnpslow_mot, ll1, ul1, n1, expt)


def dooone_pos(posname, scandir_tag):
    print(f'==== going to position: {posname}')
    rstp(posname)

    dsname = f'{scandir_tag}_{posname}_{START_KEY}'
    print(f'==== creating dataset: {dsname}')
    newdataset(dsname)


    if 'vert' == scandir_tag:
        nnp_fast = nnp2
        nnp_slow = nnp3
    elif 'hor' == scandir_tag:
        nnp_fast = nnp3
        nnp_slow = nnp2
    else:
        print(f'ERROR: illegal scandir_tag {scandir_tag} - nothing done')
        return


    print(f'==== fast {nnp_fast.name}   slow: {nnp_slow.name}')
    th_start = -10
    th_stop = 10
    n_th = 11
    th_list = np.linspace(th_start, th_stop, n_th)

    nnp_slow_start = 75.0
    nnp_slow_stop  = 175.0
    nnp_fast_start = 40
    nnp_slow_list = np.linspace(nnp_slow_start, nnp_slow_stop, n_th)

    print('==== th_list:')
    pprint(th_list)
    print('==== nnp_slow_list:')
    pprint(nnp_slow_list)

    print('==== starting theta scan loop ...')
    for i in range(n_th):

        print(f'==== cycle {i}:   moving ...')
        mv(Theta, th_list[i])
        mv(nnp_fast, nnp_fast_start)
        mv(nnp_slow, nnp_slow_list[i])

        print(f'==== cycle {i}:   Theta={th_list[i]}')
        print(f'==== cycle {i}:   Theta={Theta.position}')
        print(f'==== cycle {i}:   nnpfast: {nnp_fast.name} = {nnp_fast.position}')
        print(f'==== cycle {i}:   nnpslow: {nnp_slow.name} = {nnp_slow.position}')

        doo_generic_nnpscan(nnp_fast, nnp_slow, scandir_tag)


######################## main section

POS_STR = '''\
horz1
horz2
horz3
horz4
vert1
vert2
vert3
vert4
vert5
vert6'''


TEST_POS_STR = '''\
horz1
vert6'''


def get_scandir_tag(posname):
    if posname.startswith('hor'):
        return 'hor'
    elif posname.startswith('vert'):
        return 'vert'
    else:
        raise ValueError(f'ERROR: illegal posname {posname}')

def mro_rot_main():
    try:
        posll = POS_STR.split('\n')
        #posll = TEST_POS_STR.split('\n')
        print(posll)
        print([get_scandir_tag(x) for x in posll])

        for posname in posll:
            print(f'\n############ processing pos {posname} ...')
            scandir_tag = get_scandir_tag(posname)
            dooone_pos(posname, scandir_tag)
    finally:
        sc()
        sc()
        sc()















