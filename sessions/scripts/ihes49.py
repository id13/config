print("ihes49_night1 load 1")

START_KEY = 'k'

def ihes49_night1():
    try:
        newdataset(f'tubsM_{START_KEY}')
        gopos('mount_01_tubsM_scanstart_0001.json')
        dkpatchmesh(0.8,400,0.8,400,0.02,3,2, retveloc=5.0)
    
        newdataset(f'tubsT_{START_KEY}')
        gopos('mount_01_tubsT_scanstart_0004.json')
        dkpatchmesh(0.8,400,0.8,400,0.02,3,2, retveloc=5.0)
    
        enddataset()

    finally:
        sc()
        sc()
        
