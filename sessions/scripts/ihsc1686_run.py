print('insc1686 load for 3')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size) + 1
    zstep = int(2*zrange/zstep_size) + 1
    print(f'nnp2,{yrange},{-1*yrange},{ystep},nnp3,{-1*zrange},{zrange},{zstep},{exp_t}')
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig()
    name = f'{pos[:-5]}_{start_key}'
    gopos(pos)
    newdataset(name)
    print(f'\n=======\n{name}')
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

#saxs_pos = [
#'mount01_Ion_pos2_0009.json',
#'mount01_Ion_pos4_0011.json',
#'mount01_vis_pos2_0001.json',
#'mount01_vis_pos4_0003.json',
#]

waxs_pos1 = [
'mount01_Vis_pos1_new_0015.json',
'mount01_Ion_pos1_new_0021.json',
]
waxs_pos2 = [
'mount01_Vis_pos2_new_0016.json',
'mount01_Ion_pos2_new_0022.json',
]
TW_waxs_pos1 = [
'mount01_AC_TW_pos1_new_0017.json',
]
TW_waxs_pos2 = [
'mount01_AC_TW_pos2_new_0018.json',
]
TW_saxs_pos1 = [
'mount01_AC_TW_pos3_new_0019.json',
]
TW_saxs_pos2 = [
'mount01_AC_TW_pos4_new_0020.json',
]

# basing radiation damage test, 0.05 sec could give enough signal to noise ratio
# start_key with b has been aborted, due to 100 nm step size cause sameple damaged, not pattern after several scan.
def jan30_night_run():
    try:
        start_key = 'c'
        umv(ndetx,-300)
        for _ in TW_waxs_pos2:
            gopos_kmap_scan(_,start_key,
                            25,25,0.2,0.2,0.03)
        
        for _ in waxs_pos1:
            gopos_kmap_scan(_,start_key,
                            12.5,12.5,0.2,0.2,0.03)
        
        umv(ndetx,300)
        for _ in TW_saxs_pos1:
            gopos_kmap_scan(_,start_key,
                            25,25,0.2,0.2,0.03)
        
        umv(ndetx,-300)
        #for _ in TW_waxs_pos2:
        #    gopos_kmap_scan(_,start_key,
        #                    25,25,0.1,0.1,0.05)
        
        for _ in waxs_pos2:
            gopos_kmap_scan(_,start_key,
                            12.5,12.5,0.2,0.2,0.03)
        
        #umv(ndetx,300)
        for _ in TW_saxs_pos2:
            gopos_kmap_scan(_,start_key,
                            25,25,0.2,0.2,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()
