print('ls2995 v.36')
launch_prefix = 'a'
def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-10]
    s.replace('-','_')
    nl  = s.split('_')
    sn  = launch_prefix + '_' + nl[1]+'_'+nl[2] #x[:-17]#sample name
    rsl = nl[-2] #resolution
    if rsl == 'lr':
        patch_size = 1 
    if rsl == 'hr':
        patch_size = 0.4
    nn = nl[-1].split('x')
    pr  = int(nn[0])  #patch_row
    pc  = int(nn[1]) #patch_col
    print('\n\n',sn,patch_size,pr,pc,'\n\n')
    gopos(x)
    sleep(1.0)
    
    newdataset(sn)
    fshtrigger()
    return patch_size,pr,pc


def run_patch_scan(json_file):
    try:
        #pc is column and pr is row of scan, which correlate step at y and z in path scan
        so()
        patch_size,pr,pc = tgopos(json_file)
        mgeig()
        dkpatchmesh(patch_size,200,patch_size,200,0.02,pc,pr,retveloc=0.5)
        enddataset()
        
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()


def ls2995_wtb4():

    kapton_pos = 'empty'
    gopos(kapton_pos)
    newdataset('kapton_bkg')
    mgeig()
    loopscan(200,0.02)
    enddataset()    
    
    dam_pos = 'WTB4_dam1_wtb4lr2_0014.json'
    gopos(dam_pos)
    mgeig()
    newdataset('dam1')
    for i in range(4):
        loopscan(200,0.02)
        umvr(ustry,0.03)
    enddataset()
    
    l = [
    'WTB4_mac1_wtb4lr1_lr_1x1_0000.json',
    'WTB4_mac1_wtb4lr2_lr_1x1_0005.json',
    ]
    for i in l:
        run_patch_scan(i)
