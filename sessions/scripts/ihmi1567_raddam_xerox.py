def xerox_goto_nominal():
    rstp('exposed')
    umv(u18, 6.46)

def doo_one_spot(k, i, dy=-0.05, du18=-0.025):
    mvr(ustry, dy)
    mvr(u18, du18)
    name = f'{k}_{i:03d}'
    newdataset(name)
    stp(name)
    ueiger.ttl_software_scan(1000, 0.002)
    sleep(10)
    fshtrigger()
    loopscan(3,1)

def ser_spot(k, n, dy=-0.05, du18=-0.025):
    for j in range(n):
        doo_one_spot(k, j, dy=dy, du18=du18)

def posto(name, poi):
    poiname = f'poi{poi:d}_{name}'
    stp(name)
    stp(poiname)

def symtop_kmap(w, ny, h, nz, expt):
    ini_ypos = ustry.position
    try:
        mvr(ustry, -0.5*w)
        dkmapyz_2(0, w, ny, 0, h, nz, expt, retveloc=5)
    finally:
        umv(ustry, ini_ypos)

def one_fib_series_scan(w,ny,h,nz,expt):
    symtop_kmap(w, ny-1, h, nz-1, expt)

def z_offset(i, zstart, nz, zmargin, zstepseq):
    zoff = zstart
    for j in range(i):
        zoff += nz*zstepseq[j] + zmargin
    return zoff

def do_fib_series(run_key, pos_prefix, zmargin, w, ny, nz,zstepseq,expt):
    ini_pos_name = f'fib_series_{pos_prefix}_{run_key}_ini'
    zstart = ustrz.position
    stp(ini_pos_name)
    newdataset(ini_pos_name)

    for i, zstep in enumerate(zstepseq):
        print(f'CYCLE {i} =============================================================!!!')
        zoff = z_offset(i, zstart, nz, zmargin, zstepseq)
        umv(ustrz, zoff)
        stp(f'fib_series_{pos_prefix}_{run_key}_zone{i:03d}')
        h = nz*zstep
        print(f'new params: {i=}  {zoff=}  params: {(w, ny, h, nz, expt)=}')
        one_fib_series_scan(w, ny, h, nz, expt)
        stp(f'fib_series_{pos_prefix}_{run_key}_zone{i:03d}_done')

def doc_last_scanno():
    try:
        dooc(f'last scanno was: {str(SCANS[-1].scan_number):s}')
    except IndexError:
        dooc(f'sorry - start from fresh ...')
    
    
# DOC_RUNS ############
# [73]: do_fib_series('a', 'raddam1', 0.02, 0.1, 50, 20, (0.0005, 0.001, 0.0015, 0.0), 0.01)
# okay
def hair_left_raddam2():
    zstepseq = (0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004, 0.0045, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010)
    zmargin = 0.02
    w = 0.2
    ny = 100
    nz = 30
    expt = 0.01
    do_fib_series('a', 'hair_left_raddam2', zmargin, w, ny, nz, zstepseq, expt)
    # interrupted during zone 7

def hair_left_raddam3():
    zstepseq = (0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004, 0.0045, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010)
    zmargin = 0.04
    w = 0.25
    ny = 500
    nz = 40
    expt = 0.005
    do_fib_series('a', 'hair_left_raddam3', zmargin, w, ny, nz, zstepseq, expt)

def CF00L_raddam1():
    zstepseq = (0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004, 0.0045, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010)
    zmargin = 0.04
    w = 0.25
    ny = 500
    nz = 40
    expt = 0.01
    do_fib_series('a', 'CF00L_raddam1', zmargin, w, ny, nz, zstepseq, expt)

def CXxxL_raddamx():
    zstepseq = (0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004, 0.0045, 0.005)
    zmargin = 0.04
    w = 0.4
    ny = 800
    nz = 40
    expt = 0.01

    poslist = 'PF00L PF35L PF50L PF65L CF30L CF50L CF65L'.split()

    for pos in poslist:
        print(f'=\n= {pos}        ======================\n=')
        rstp(pos)
        do_fib_series('b', f'{pos}_raddamx', zmargin, w, ny, nz, zstepseq, expt)
