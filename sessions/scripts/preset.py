from bliss.scanning.scan import ScanPreset
import sys

class ThisPreset(ScanPreset):

    def __init__(self):
        super().__init__()

    def prepare(self, scan):
        #self.connect_data_channels([p201_eh2_0.counters.ct22, eiger.roi_counters], self.doit)
        self.connect_data_channels([p201_eh2_0.counters.ct22], self.doit)
        print('###===')
        #raise ValueError()

    def doit(self, counter, channel_name, data):
        print('===')
        if counter is p201_eh2_0.counters.ct22:
            print('Christmas')
        print(counter, type(counter))
        print(channel_name)
        if channel_name == 'eiger:roi_counters:roi1_avg':
            print(data)
        print('done')

def myscan(*p):
    sc = dscan(*p, run=False)
    p = ThisPreset()
    sc.add_preset(p)
    return sc
