print('load posinfra - 1')
from collections import OrderedDict as Odict
from os import path
from jsonio import SmpJsonData

class BlissPosMgr(object):


    def __init__(self, actuator_list):
        print('mgr')
        self.actuatordc = acdc = Odict()
        for ac in actuator_list:
            acdc[ac.name] = ac

    def get_actuator_names(self):
        return self.actuatordc.keys()

    def stp(self, posname, tags=None):
        pos_sjd = SmpJsonData(tipe='BLISS_POS2', fxpth=f'{posname}.bp2')
        acdc = self.actuatordc
        data = pos_sjd.dc['data']
        data['pos'] = self.make_posdc()
        pos_sjd.save()


    def make_posdc(self):
        acdc = self.actuatordc
        posdc = Odict()
        for k,v in acdc.items():
            posdc[k] = v.position
        return posdc

    def rstp(self, posname, actuator_names):
        pos_sjd = SmpJsonData(tipe='BLISS_POS2', fxpth=f'{posname}.bp2')
        pos_sjd.load()
        data = pos_sjd.dc['data']
        posdc = data['pos']
        self.posdc_goto(posdc, actuator_names)

    def posdc_goto(self, posdc, actuator_names):
        if 'all' == actuator_names:
            actuator_names = self.get_actuator_names()
        if isinstance(actuator_names, str):
            actuator_names = actuator_names.split()
        acdc = self.actuatordc
        for k in actuator_names:
            assert k in acdc
            ac = acdc[k]
            mv(acdc[k], posdc[k])

def print_pos(header, posdc, **kw):
    print(header)
    for k,v in kw.items():
        print(f'  {k:10}:  {v}')

    for k,v in posdc.items():
        print(f'    {k:10}:  {v}')


class BlissPosList(object):

    TIPE = 'BLISS_POSLIST2'

    def __init__(self, actuator_list, name, tag_list):
        self.name = name
        self.actuator_list = actuator_list
        self.tag_list = tag_list
        fxpth = self.make_fxpth(name)
        self.sjd = SmpJsonData(tipe=self.TIPE, fxpth=fxpth)
        self.reset()

    def reset(self):
        self.sjd.reset()
        self.data = data = self.sjd.dc['data']
        self.max_idx = -1
        data['pos'] = Odict()
        for t in self.tag_list:
            data[t] = Odict()
        self.bpmgr = BlissPosMgr(self.actuator_list)

    def __len__(self):
        return len(self.data)

    def str_item(self, idx, header=None):
        ll = []
        if None is header:
            header = f'bpl_item: {idx}'
        ll.append(header)
        for k in self.tag_list:
            ll.append(f'  {k:10}:  {self.data[k][idx]}')
        ll.append('  pos:')
        posdc = self.data['pos'][idx]
        for k,v in posdc.items():
            ll.append(f'    {k:10}:  {v}')
        return('\n'.join(ll))

    def print_item(self, idx):
        print(self.str_item(idx))

    def print_all(self):
        print(f'== BlissPosList: {self.name}')
        for idx in self.data['pos']:
            self.print_item(idx)

    def make_fxpth(self, name):
        return f'{name}.bpli2'

    def add_pos(self, idx='next', **kw):
        if 'next' == idx:
            idx = self.max_idx = self.max_idx +1
        data = self.data
        assert 'pos' in kw
        for k,v in kw.items():
            data[k][idx] = v

    def add_currentpos(self, idx='next', **kw):
        data = self.data
        assert 'pos' not in kw
        kw['pos'] = self.bpmgr.make_posdc()
        self.add_pos(idx=idx, **kw)

    def goto_pos(self, idx, actuator_names=None):
        posdc = self.data['pos'][idx]
        self.bpmgr.posdc_goto(posdc, actuator_names)
 
    def load(self, name=None):
        if None is name:
            name = self.name
        fxpth = self.make_fxpth(name)
        self.sjd.load(fxpth=fxpth)
        assert self.sjd.dc['tipe'] == self.TIPE
        self.data = data = self.sjd.dc['data']
        data['idx']
        data['pos']
        if len(data['idx']):
            self.max_idx = max(data['idx'])
        else:
            self.max_idx = -1

    def store(self, name=None):
        if None is name:
            name = self.name
        fxpth = self.make_fxpth(name)
        self.sjd.save(fxpth=fxpth)
