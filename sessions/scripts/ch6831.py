print("ch6831 : load - 27")
import numpy as np
from os import path

# modify start key if you have to start several times or restart
START_KEY = 'd'

# If True it will only test if it can go to the positions and
# whether the basic logic is sound. It won't acquire scans and
# it won't create new datastes
# SIM_BFLG = True

SIM_BFLG = False


# full resolution
EXPT = 0.02
XX = 10

# Quick:
#EXPT = 0.01
#XX   = 1

# BSTOP
# -51  0        0
# -31  0.1045  -0.0591
# -11  0.2075  -0.1095

load_script('ch6831_bstop')


def init_one(posname):
    print(f'initializing: {posname}')
    rstp(posname)
    if SIM_BFLG:
        print(f'newdataset: {START_KEY}_{posname}')
    else:
        newdataset(f'{START_KEY}_{posname}')

def tip_scan():
    if SIM_BFLG:
        print('tip_scan')
    else:
        kmap.dkmap(nnp2, -14,0, 14*XX, nnp3, 0, 30, 30*XX, EXPT)

def shaft_scan():
    if SIM_BFLG:
        print('shaft scan...')
    else:
        kmap.dkmap(nnp2, -20,20, 40*XX, nnp3, -15, 15, 30*XX, EXPT)

def big_scan():
    if SIM_BFLG:
        print('big scan ...')
    else:
        kmap.dkmap(nnp2, -35, 31, 33*XX, nnp3, -30, 50, 40*XX, EXPT)


POS_LIST = '''
Y_ht2_mid                                                                                                                   
Y_ht2_tip                                                                                                                   
Y_hb2_base
Y_hb2_mid                                                                                                                   
Y_ht3_mid                                                                                                                   
Y_ht3_tip                                                                                                                   
Y_hb3_base                                                                                                                  
Y_hb3_mid                                                                                                                   
Y_hb3_2_base                                                                                                                
Y_hb3_2_mid                                                                                                                 
Y_ht5_mid                                                                                                                   
Y_ht5_tip                                                                                                                  
'''


def make_poslist():
    ll = POS_LIST.split('\n')
    ll = [x.strip() for x in ll if x.strip()]
    return ll
    
# scan options to be activated in ch6831_main

def ch6831_multi():
    posll = make_poslist()
    for p in posll:
        if path.exists('STOP'):
            break
        init_one(p)
        if 'tip' in p:
            print('========',p,'tip')
            tip_scan()
        elif 'mid' in p or 'base' in p:
            print('========',p,'shaft')
            shaft_scan()
        elif 'connect' in p or 'socket' in p:
            print('========',p,'big')
            big_scan()
        else:
            raise ValueError(p)

def ch6831_rotseries():
    # edit the linspace for the Theta values to be visited
    for th in np.linspace(-20.0,10.0,31):
        print('==========:', th)
        mv(Theta, th)
        kmap.dkmap(nnp2, -15,5,100, nnp3, -2,12,70,0.02)

def ch6831_rotseries_fancy():
    # edit the linspace for the Theta values to be visited
    npts = 31
    th_spc = np.linspace(-20.0,10.0,npts)
    nnp2_spc = np.linspace(120.0,130.0,npts)
    for i in range(npts):
        th = th_spc[i]
        nnp = nnp2_spc[i]
        mv(Theta, th)
        mv(nnp2, nnp)
        print('==========: th', th, ' nnp2 ', nnp2.position)
        kmap.dkmap(nnp2, -15,15,60, nnp3, -11,13, 48,0.02)


def hex_recover():
    nnx.sync_hard()
    nny.sync_hard()
    nnz.sync_hard()
    nnu.sync_hard()
    nnv.sync_hard()
    nnw.sync_hard()

def ch6831_main():
    try:
        so()
        sleep(2)
        ch6831_multi()
        #ch6831_rotseries()
	#ch6831_rotseries_fancy()
    finally:
        sc()
        sc()
        sc()
