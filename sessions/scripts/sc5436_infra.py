print('sc5436_infra - load 1')
from time import sleep
from id13.scripts.onaxis_vlm import restore_live,stop_live
from bliss.flint.client.proxy import get_flint                                              

class LiveMic(object):

    ALLOWED_MOTS = 'nnx nny nnz'.split()

    def goto_click_img(self):
        ps_y, ps_z = self.pix_size
        beam_pxy, beam_pxz = self.beam_pixel
        curr_y = nny.position
        curr_z = nnz.position
        f =  get_flint()                                                                            
        p = f.get_live_plot(image_detector="dlvlm1")                                                
        pos = p.select_points(1)
        print(pos)         
        (px_y,px_z) = pos[0]
        print(f'{px_y,px_z=}')
        d_pxy = px_y - beam_pxy
        d_pxz = px_z - beam_pxz
        print(f'{d_pxy,d_pxz=}')
        dy = d_pxy*ps_y
        dz = d_pxz*ps_z
        print(f'{dy,dz=}')
        target_y = curr_y - dy
        target_z = curr_z + dz
        print(f'#    mv(nny, {target_y}, nnz, {target_z})')
        mv(nny, target_y, nnz, target_z)
        self.acq()
        

    def __init__(self):
        self.pix_size = (0.00099, 0.00097)
        self.beam_pixel = (354.87,458.10)
        self.stp = 0.01
        self.expt = 0.001
        self.slil_stp = 1
        self.expt_stp = 0.001
        self.mot = nny
        self.lightlevel = 7

    def apalign(self):
        dscan(nmicroy, -0.04, 0.04, 40, 0.1)
        goc(nmicroy)
        where()
        sleep(1)
        dscan(nmicroz, -0.04, 0.04, 40, 0.1)
        goc(nmicroz)
        where()
        sleep(1)

    def sync(self):
        slil(self.lightlevel)

    def set_tweak(self, mot, stp):
        assert mot.name in self.ALLOWED_MOTS
        assert stp > -1.01 and stp < 1.01
        self.mot = mot
        self.stp = stp

    def acq(self):
        ct(self.expt, dlvlm1)

    def slil_mv(self, ll):
        if ll < 0 or ll > 100:
            raise ValueError(f'illegal light level: {ll}')
        slil(ll)
        self.lightlevel = ll

    def slil_mvr(self, dll):
        ll = self.lightlevel + dll
        if ll < 0 or ll > 100:
            raise ValueError(f'illegal light level: {ll}')
        slil(ll)
        self.lightlevel = ll
    
    def set(self, lightlevel=None, expt=None, slil_stp=1, expt_stp=0.001):
        if not None is lightlevel:
            self.lightlevel = lightlevel
            self.slil_mv(lightlevel)
        if not None is expt:
            self.expt = expt
        if not None is expt_stp:
            self.expt_stp = expt_stp
        if not None is slil_stp:
            self.slil_stp = slil_stp

    def mgd(self):
        mgd()
        MG_EH3a.add(dlvlm1.counters.image)
        MG_EH3a.enable("dlvlm1:image")

    def rmgeig(self):
        rmgeig()
        MG_EH3a.add(dlvlm1.counters.image)
        MG_EH3a.enable("dlvlm1:image")

limic = LiveMic()

def limacq():
    limic.acq()

def ltw(x):
    if x == 0:
        limic.slil_mvr(-limic.slil_stp)
        limic.acq()
    elif x == 1:
        limic.slil_mvr(limic.slil_stp)
        limic.acq()
    
def etw(x):
    if x == 0:
        expt = limic.expt - limic.expt_stp
    elif x == 1:
        expt = limic.expt + limic.expt_stp
    if expt < 0.001 or expt > 0.1:
        raise ValueError(f'illegal light level: {expt}')
    limic.expt = expt
    limic.acq()
    
def mtw(x):
    if x == 0:
        mvr(limic.mot, -limic.stp)
        limic.acq()
    elif x == 1:
        mvr(limic.mot, limic.stp)
        limic.acq()

def ztw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nnz, stp)
    mvr(limic.mot, limic.stp)
    limic.acq()

def nztw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nnz, stp)
    mvr(limic.mot, -limic.stp)
    limic.acq()

def xtw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nnx, stp)
    mvr(limic.mot, limic.stp)
    limic.acq()

def nxtw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nnx, stp)
    mvr(limic.mot, -limic.stp)
    limic.acq()

def ytw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nny, stp)
    mvr(limic.mot, limic.stp)
    limic.acq()

def nytw(stp=None):
    if None is stp:
        stp = limic.stp
    limic.set_tweak(nny, stp)
    mvr(limic.mot, -limic.stp)
    limic.acq()

