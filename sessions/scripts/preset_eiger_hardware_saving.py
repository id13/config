import time
from bliss.scanning.chain import ChainPreset

class EigerHardwareSavingPreset(ChainPreset):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._eiger = config.get("eiger")    
    
    def start(self, acq_chain):
        print(self.__class__.__name__, "Start")        
        #self._eiger._proxy.saving_format = 'HDF5BS'
        self._eiger._proxy.saving_managed_mode = 'HARDWARE'
        self._eiger._proxy.saving_format = 'HDF5'
        self._eiger._proxy.video_active = False
        self._eiger.stopAcq()
        self._eiger.prepareAcq()

    def stop(self, acq_chain):
        print(self.__class__.__name__, "Finished")
        self._eiger._proxy.saving_managed_mode = 'SOFTWARE'
