print('iscanpool_infra1 - load 1')

class Virtual(Exception):  pass

import traceback

import time
import gevent,bliss


def make_trimmer():
    return ID13ScanPool()

class ID13ScanPool(object):

    def __init__(self, simmode=True, corry=0.0, corrz=0.0, default_okay=True, start_key='a'):
        ndiodey.acceleration = 5
        self.start_key = start_key
        self.simmode = simmode
        self.corry = corry
        self.corrz = corrz
        self.default_okay = default_okay

        self.trim_mots = dict(
            nt3j = nt3j,
            nt1t = nt1t,
            wbetz = wbetz,
            wbety = wbety
        )

        self.bstop_imots = dict(
            nbsy = nbsy,
            nbsz = nbsz,
        )

    def doo_kmap(self, *p):
        if self.simmode:
            print(f'kmap sim: {p}')
        else:
            print(f'kmap real: {p}')
            try:
                kmap.dkmap(*p)
            except:
                print(traceback.format_exc())
        print('3 sec to interrupt with <CTRL><C> ...')
        sleep(3)

    def scans__DEFAULT(self):
        raise Virtual()

    def doo_position(self, pos_name):
        print(f'restoring {pos_name}')
        rstp(pos_name)
        print(f'applying corrections: {self.corry=}  {self.corrz=}')
        umvr(nny, self.corry)
        umvr(nnz, self.corrz)
        ds_name = f'{pos_name}_{self.start_key}'
        if self.simmode:
            print(f'would create dataset {ds_name}')
        else:
            print(f'creating dataset {ds_name}')
            newdataset(ds_name)
        try:
            print(f'seraching scans: {pos_name} ...')
            scans_funcoid = getattr(self, f'scans_{pos_name}')
            print(f'funcoid name = {scans_funcoid.__name__}')
        except AttributeError:
            if self.default_okay:
                print('seraching scans: {pos_name} not found! - going for default')
                scan_funcoid = self.scanns__DEFAULT
            else:
                print('seraching scans: {pos_name} not found and no default allowed! - nothing done')
                return

        scans_funcoid()
        sleep(0.5)
        with gevent.Timeout(seconds=60):
            try:
                print('senfing elog_plot ...')
                elog_plot(scatter = True)
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                print('WARNING: elog_plot timeout ...')


    def main(self, runlist):
        try:
            if self.simmode:
                pass
            else:
                #pass
                so()
            sleep(3)
            rmgeig()

            for i, pos_name in enumerate(runlist):
                print(f'pos number {i} =========================================')
                try:
                    self.doo_position(pos_name)
                except:
                    print(traceback.format_exc())
        finally:
            sc()
            sc()
            sc()

    def save_last_trim_pos(self):
        self.trim_pos = dict(
            nt3j = nt3j.position,
            nt1t = nt1t.position,
            wbetz = wbetz.position,
            wbety = wbety.position,
            )

    def save_last_bstop_pos(self):
        self.bstop_pos = dict(
            nbsy = nbsy.position,
            nbsz = nbsz.position,
            )

    def goto_last_trim_pos(self):
        for k, v in self.trim_mots:
            mv(v, self.trim_pos[k])

    def goto_last_bstop_pos(self):
        for k, v in self.bstop_mots:
            mv(v, self.bstop_pos[k])

    def show_last_trim_pos(self):
        for k, v in self.trim_mots:
            print(f'mot: {k}  last_pos = {self.trim_mots[k]}  current_pos = {v.position}')

    def show_last_bstop_pos(self):
        for k, v in self.bstop_mots:
            print(f'mot: {k}  last_pos = {self.trim_mots[k]}  current_pos = {v.position}')

    def trim_beam(self):
        self.save_last_trim_pos()
        try:
            if sh3.is_closed:
                print(f'open safety shutter before trimming ... nothing done')
                return
            print('setting diode mode ...')
            mgd()
            fshtrigger()
            (det0, _mon) = self.count()
            print(f'{det0=}')
            if det0 > 20000:
                dscan(wbetz, -0.05, 0.05, 50, 0.1)
                gop(wbetz)
                dscan(wbety, -0.1, 0.1, 50, 0.1)
                gop(wbety)
                dscan(wbetz, -0.05, 0.05, 100, 0.1)
                gop(wbetz)
                (det1, _mon) = self.count()
                gain = (det1-det0)/det0
                print(f'gain: {gain*100.0} %')
                if det1 > 200000:
                    nt3align()
                (det2, _mon) = self.count()
                if det2 > 200000:
                    print('beam probably okay')
            else:
                print(f'too low counts {det0=} ... nothing done')
                return
        finally:
            print('setting roi eiger mode ...')
            rmgeig()

    def count(self, expt=1):
        loopscan(1, expt)
        ct32_v = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
        ct34_v = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct34'][0]
        return (ct32_v, ct34_v)

class ScanpoolExample(ID13ScanPool):

    def __init__(self, simmode=True, corry=-0.002, corrz=0.030, start_key='zz'):
        super().__init__(simmode=simmode, corry=corry, corrz=corrz)
	
    def run(self):
        ll = ['pos1','pos2','pos3']
        self.main(ll)

    def scans_pos1(self):
        # would handle "pos1"
        self.doo_kmap(nnp2, -40,40,160, nnp3, -40,40,160,0.02)
	
    def scans__DEFAULT(self):
        # would handle "pos2" and "pos3"
        self.doo_kmap(nnp2, -4,4,16, nnp3, -4,4,16,0.02)
