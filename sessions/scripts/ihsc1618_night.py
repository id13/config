import os 
print ('armed rl4...')

DS_KEY = 'd'

def dummy(*p,**kw):
    print('dummy', p, kw)

#newdataset = dummy
#enddataset = dummy
#dmesh = dummy

def ihsc1618_night():
    gopos('c1cs_x5_0016.json')
    mvr(nnz,0.01)
    newdataset('x5_%s' % DS_KEY)
    zeronnp()
    dmesh(nnp2,15,-15,240,nnp3,-15,15,60,0.05) 
    enddataset()

    gopos('c1cs_x5b_0017.json')
    mvr(nnz,0.01)
    newdataset('x5b_%s' % DS_KEY)
    zeronnp()
    dmesh(nnp2,8,-8,128,nnp3,-8,8,32,0.05) 
    enddataset()
    
    gopos('c1cs_x6_0018.json')
    mvr(nnz,0.01)
    newdataset('x6_%s' % DS_KEY)
    zeronnp()
    dmesh(nnp2,15,-15,240,nnp3,-15,15,60,0.1) 
    enddataset()
    
    gopos('c1cs_x7_0019.json')
    mvr(nnz,0.01)
    newdataset('x7_%s' % DS_KEY)
    zeronnp()
    dmesh(nnp2,15,-15,120,nnp3,-15,15,60,0.2) 
    enddataset()
