print('load 10')

import math


OUR_DOC = '''

EH2 [288]: wm(umcenx, umceny)

            umcenx     umceny
--------  --------  ---------
User
 High      2.71000    2.94769
 Current   0.17499   -2.20601
 Low      -2.71000   -2.94776
Offset     1.26846   12.55565

Dial
 High      1.44154   -9.60797
 Current  -1.09347  -14.76167
 Low      -3.97846  -15.50341
'''

def ourmv(mot, pos):
    print(f'mv({mot.name}, {pos})')
    mv(mot, pos)


class VirtualReflectionGeo(object):
    
    def __init__(self, ini_pos, rad_correction_angle, ini_z, ini_z_slope):
        try:
            ini_mceny, ini_mcenx = ini_pos
        except ValueError:
            if ini_pos == "here":
                ini_mceny = umceny.position
                ini_mcenx = umcenx.position
                ini_z = ustrz.position
        self.ini_mceny = ini_mceny
        self.ini_mcenx = self.ori_mcenx = ini_mcenx
        self.rad_correction_angle = rad_correction_angle
        self.ini_z = ini_z
        self.ini_z_slope = ini_z_slope
        self.cosphi = math.cos(rad_correction_angle)
        self.sinphi = math.sin(rad_correction_angle)
    
    def __repr__(self):
        rsl = []
        rsl.append(f'VirtualReflectionGeo(({self.ini_mceny:3.6f}, {self.ini_mcenx:3.6f}),')
        rsl.append(f'{self.rad_correction_angle:3.6f},{self.ini_z:3.6f},{self.ini_z_slope:3.6f})')
        rs = ''.join(rsl)
        return rs
        
    def show(self):
        ll = []
        ll.append(self.__repr__())
        r,x,y,z,mx,my,uu18 = (
           usrotz.position,
           ustrx.position,
           ustry.position,
           ustrz.position,
           umcenx.position,
           umceny.position,
           u18.position
           )

        ll.append(f'    r,x,y,z,mx,my,u18 = {r:3.6f},{x:3.6f},{y:3.6f},{z:3.6f},{mx:3.6f},{my:3.6f},{uu18:3.6f}')
        s = '\n'.join(ll)
        return s
    def __str__(self):
        return self.show()
        

    def get_vy(self):
        curr_mceny = umceny.position
        curr_mcenx = umcenx.position
        dmceny = curr_mceny - self.ini_mceny
        dmcenx = curr_mcenx - self.ini_mcenx    
        vy_pos = dmceny / self.cosphi
        return(vy_pos)
        
    def get_mcen_pos(self, vy_pos): 
        dmceny = vy_pos * self.cosphi
        dmcenx = vy_pos * self.sinphi
        mceny_pos = self.ini_mceny + dmceny
        mcenx_pos = self.ini_mcenx + dmcenx
        return(mceny_pos, mcenx_pos)
        
    def mv_vy(self, vy_pos):
        (mceny_pos, mcenx_pos) = self.get_mcen_pos(vy_pos)
        ourmv(umceny, mceny_pos)
        ourmv(umcenx, mcenx_pos) 
    
    def mvr_vy(self, vy_pos_distance):
        curr_vy_pos = self.get_vy()
        new_vy_pos = curr_vy_pos + vy_pos_distance
        self.mv_vy(new_vy_pos)

    def go_to_z(self, z_pos):
        curr_vy_pos = self.get_vy()
        delta_z = z_pos - self.ini_z
        self.ini_mcenx = self.ori_mcenx + self.ini_z_slope * delta_z
        self.mv_vy(curr_vy_pos)
        ourmv(ustrz, z_pos) 
        
    def mvr_z(self, z_pos_distance):
        curr_z_pos = ustrz.position
        new_z_pos = curr_z_pos + z_pos_distance
        self.go_to_z(new_z_pos)    
        
             
    def dmesh_z_vy(self, zll, zul, znitv, vyll, vyul, vynitv, expt):
        start_vy = self.get_vy()
        start_z = ustrz.position
        vy_step = (vyul-vyll)/vynitv
        try:
            for i in range(vynitv+1):
                curr_vy = start_vy + vyll + i*vy_step
                self.mv_vy(curr_vy)
                dscan(ustrz, zll, zul, znitv, expt)
        finally:
            self.mv_vy(start_vy)
            mv(ustrz, start_z)
    
    def dmesh_z_rot_vy(self, zll, zul, znitv, vyll, vyul, vynitv, expt, rotll, rotul, rotnitv):
        start_vy = self.get_vy()
        start_z = ustrz.position
        vy_step = (vyul-vyll)/vynitv
        try:
            for i in range(vynitv+1):
                curr_vy = start_vy + vyll + i*vy_step
                self.mv_vy(curr_vy)
                dmesh(usrotz, rotll, rotul, rotnitv, ustrz, zll, zul, znitv, expt)
        finally:
            self.mv_vy(start_vy)
            mv(ustrz, start_z)
            
