def mgroieiger():
    mgeig()
    MG_EH3a.enable('*r:r*')

def wpz():
    print(f'''
nnp1 : {nnp1.position:8.4f}
nnp2 : {nnp2.position:8.4f}
nnp3 : {nnp3.position:8.4f}
''')

def perfcorr():
    umv(nnp1 , 125.0000)
    umv(nnp2 , 119.4100)
    umv(nnp3 , 136.9700)

def corrzeronnp():
    zeronnp()
    perfcorr()
