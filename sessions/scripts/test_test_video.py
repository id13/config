print('test_test_video - load 1')

def focalize(iomgr, videoview, pfname, npts, stp, itroi):
    vseries = VideoFocusSeries(nnx, iomgr, videoview, pfname, 'foc_test', npts, stp, [])
    vseries.acq_series()
    sad=SimpleAutofocusDetect(vseries, itroi=itroi)
    sad.find_obj_001()
    sad.goto_focus()


def dotest_vid():


    base_dapth = os.getcwd()
    root_drpath = 'videotest'
    iomgr = IOMgr(base_dapth, root_drpath)
    vframe_shape = vid_get_shape()
    print(DQPIX_BEAM)
    print(vid_dqpix_to_pix(DQPIX_BEAM, vframe_shape))
    r_b, c_b = vid_pix_to_pn(vid_dqpix_to_pix(DQPIX_BEAM, vframe_shape))
    r_b = int(r_b)
    c_b = int(c_b)
    itroi = ((r_b-40, c_b-200),(r_b+40, c_b+200))
    #itroi = None
    print(itroi)
    videoview = VideoView(vframe_shape)
    nny_start = nny.position
    nnz_start = nnz.position
    NI = 1
    NJ = 1
    PREFIX = 'beech1_1'
    try:
        for i in range(NI):
            runno = i + 1000
            new_pos_z = nnz_start + i*0.1
            mv(nnz, new_pos_z)
            for j in range(NJ):
                print(f'cycle: {i}  {j}')
                new_pos_y = nny_start + j*0.1
                mv(nny, new_pos_y)
                focalize(iomgr, videoview, f'{PREFIX}_fine_{i:03d}_{j:03d}', 9, 0.05, itroi)
                focalize(iomgr, videoview, f'{PREFIX}_fine_{i:03d}_{j:03d}', 9, 0.001, itroi)
                focalize(iomgr, videoview, f'{PREFIX}_fine_{i:03d}_{j:03d}', 9, 0.002, itroi)
    finally:
        pass
        # slil(0)
    #focalize(iomgr, videoview, f'x_{runno:03d}_01', 5, 0.03, itroi)
    #focalize(iomgr, videoview, f'x_{runno:03d}_01', 5, 1.0, itroi)
    #focalize(iomgr, videoview, f'x_{runno:03d}_02', 5, 0.2, itroi)
    #focalize(iomgr, videoview, f'x_{runno:03d}_03', 5, 0.03, itroi)
    #focalize(iomgr, videoview, f'x_{runno:03d}_04', 5, 0.05, itroi)


def dotest_vid_old():
    base_dapth = os.getcwd()
    root_drpath = 'videotest'
    iomgr = IOMgr(base_dapth, root_drpath)
    vframe_shape = vid_get_shape()
    print(DQPIX_BEAM)
    print(vid_dqpix_to_pix(DQPIX_BEAM, vframe_shape))
    r_b, c_b = vid_pix_to_pn(vid_dqpix_to_pix(DQPIX_BEAM, vframe_shape))
    r_b = int(r_b)
    c_b = int(c_b)
    itroi = ((r_b-100, c_b-200),(r_b+100, c_b+200))
    print(itroi)
    videoview = VideoView(vframe_shape)

    vseries = VideoFocusSeries(nnx, iomgr, videoview, 'ser001', 'foc_test', 20, 0.05, [])
    vseries.acq_series()
    sad=SimpleAutofocusDetect(vseries, itroi=itroi)
    sad.find_obj_001()
    sad.goto_focus()
    return vseries, sad
