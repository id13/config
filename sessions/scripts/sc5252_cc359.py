print('load 1')
START_KEY = 'e'

def dkmapyz_2(ll2,ul2,n2,ll3,ul3,n3,expt):
    print(ll2,ul2,n2,ll3,ul3,n3,expt)
    kmap.dkmap(nnp2,ll2,ul2,n2,nnp3,ll3,ul3,n3,expt)

PARAM_SETS = '''
set2b -50 50 400 -50 50 200 0.01
set3b -50 50 400 -50 50 200 0.005
set7b -50 50 500 -45 45 300 0.003
'''
def sc5252_cc359():
    try:
        pdict = dict()
        ll = PARAM_SETS.split('\n')
        for l in ll:
            l = l.strip()
            if l:
                w = l.split()
                print(w)
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = w
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = (
                    setk,int(ll2),int(ul2),int(n2),
                    int(ll3),int(ul3),int(n3),float(expt)
                )
                pdict[setk] = (ll2,ul2,n2,ll3,ul3,n3,expt)
        so()
        mgeig()

        posname = 'cc359_5x_beforeScan_cell1_0001.json'
        roiname = 'cell1_set7b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set7b'])

        posname = 'cc359_5x_beforeScan_cell2_0002.json'
        roiname = 'cell2_set3b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set2b'])

        posname = 'cc359_5x_beforeScan_cell3_0003.json'
        roiname = 'cell3_set3b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set3b'])

        posname = 'cc359_5x_beforeScan_cell4_0004.json'
        roiname = 'cell4_set7b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set7b'])

        posname = 'cc359_5x_beforeScan_cell5_0005.json'
        roiname = 'cell5_set2b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set2b'])

        posname = 'cc359_5x_beforeScan_cell6_0006.json'
        roiname = 'cell6_set3b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set3b'])

        posname = 'cc359_5x_beforeScan_cell7_0007.json'
        roiname = 'cell7_set7b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set7b'])

        posname = 'cc359_5x_beforeScan_cell8_0008.json'
        roiname = 'cell8_set2b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set2b'])

        posname = 'cc359_5x_beforeScan_cell9_0009.json'
        roiname = 'cell9_set3b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set3b'])

        posname = 'cc359_5x_beforeScan_cell10_0010.json'
        roiname = 'cell10_set7b'

        newdataset(f'{roiname}_{START_KEY}')
        gopos(posname)
        dkmapyz_2(*pdict['set7b'])

    finally:
        sc()
        sc()
        sc()
