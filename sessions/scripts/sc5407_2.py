print('sc5407_2.py - load 1')


def do_calib():
	try:
		newdataset('measurement')
		kmap.dkmap(nnp2, -80, 80, 320, nnp3, -100, 100, 400, 0.02)
	finally:
		sc()
		sc()
		sc()

def safety_net():
    for i in range(1,7):
        timer = 7 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)

def Hpc6_mem_K():
	start_key = 'a'
	so()
	for i in range(1, 6):
		pos_key = f'p{i}_good'
		ggg.grstp(f'{pos_key}', 'xyz')
		print('=================')
		print(f'going to {pos_key}')
		print('=================')
		newdataset(f'{pos_key}_{start_key}')
		kmap.dkmap(nnp2, -7, 7, 280, nnp3, -6, 6, 240, 0.02)
		sync()
		elog_plot(scatter = True)
		dooc(f'this is position {pos_key}')
		sync()
		safety_net()

def Hp1b_mem_2_2():
	start_key = 'a'
	so()
	for i in range(1, 7):
		pos_key = f'p{i}'
		rstp(f'{pos_key}')
		umvr(nnz,-0.009,nny,-0.006)
		print('=================')
		print(f'going to {pos_key}')
		print('=================')
		newdataset(f'{pos_key}_{start_key}')
		kmap.dkmap(nnp2, -7, 7, 280, nnp3, -7, 7, 280, 0.02)
		sync()
		elog_plot(scatter = True)
		dooc(f'this is position {pos_key}')
		sync()
		safety_net()


def Hp1b_mem_2_2_2():

	start_key = 'a'
	so()
	for i in range(2, 7):
		pos_key = f'p{i}_final'
		ggg.grstp(f'{pos_key}', 'trans')
		print('=================')
		print(f'going to {pos_key}')
		print('=================')
		newdataset(f'{pos_key}_{start_key}')
		kmap.dkmap(nnp2, -3, 3, 120, nnp3, -3, 3, 120, 0.02)
		sync()
		elog_plot(scatter = True)
		dooc(f'this is position {pos_key}')
		sync()
		safety_net()

def Heb1_mem_C():

	start_key = 'b'
	try:
		so()
		for i in range(5, 12):
			pos_key = f'p{i}_final'
			ggg.grstp(f'{pos_key}', 'trans')
			print('=================')
			print(f'going to {pos_key}')
			print('=================')
			newdataset(f'{pos_key}_{start_key}')
			kmap.dkmap(nnp2, -3, 3, 120, nnp3, -3, 3, 120, 0.02)
			sync()
			elog_plot(scatter = True)
			dooc(f'this is position {pos_key}')
			sync()
			safety_net()
	finally:
		sc()
		sc()
		sc()


def He1c6_mem_L():

	start_key = 'a'
	try:
		so()
		for i in range(1, 17):
			pos_key = f'p{i}_final'
			ggg.grstp(f'{pos_key}', 'trans')
			print('=================')
			print(f'going to {pos_key}')
			print('=================')
			newdataset(f'{pos_key}_{start_key}')
			kmap.dkmap(nnp2, -3, 3, 120, nnp3, -3, 3, 120, 0.02)
			sync()
			elog_plot(scatter = True)
			dooc(f'this is position {pos_key}')
			sync()
			safety_net()
	finally:
		sc()
		sc()
		sc()


def Hc3c6_mem_A():

	start_key = 'a'
	try:
		so()
		for i in range(1, 14):
			pos_key = f'p{i}_final'
			ggg.grstp(f'{pos_key}', 'trans')
			print('=================')
			print(f'going to {pos_key}')
			print('=================')
			newdataset(f'{pos_key}_{start_key}')
			kmap.dkmap(nnp2, -3, 3, 120, nnp3, -3, 3, 120, 0.02)
			sync()
			elog_plot(scatter = True)
			dooc(f'this is position {pos_key}')
			sync()
			safety_net()
	finally:
		sc()
		sc()
		sc()
