print('load 1')


def safety_net():
    for i in range(1,5):
        timer = 5 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)
    
    
def samedi_soir():
    so()
    load_script('karl_safe_scan_example')
    safe_meig_x()
    start_key = 'a'
    try:        
        for i in range(7, 9):
            pos_key = f'pos{i}'
            rstp(pos_key)
            for j in range(-48, 50, 4):
                angle = j/100
                print('================================')
                print(f'measuring RC at {pos_key} at angle {angle}')
                print('================================')
                umv(Theta, angle)
                newdataset(f'{pos_key}_rocking_Theta_{j}')
                safe_kmap(nnp2, -5,5,100,nnp3, -5,5,100,0.02)
                safety_net()
            umv(Theta, 0)
            print('================================')
            print(f'measuring XRF at {pos_key}')
            print('================================')
            newdataset(f'{pos_key}_XRF_mapping')
            safe_kmap(nnp2, -5,5,100,nnp3, -5,5,100,0.2)
            safety_net()
            
        for i in range(9,11):
            pos_key = f'pos{i}'
            rstp(pos_key)
            for j in range(-48, 50, 4):
                angle = j/100
                print('================================')
                print(f'measuring RC at {pos_key} at angle {angle}')
                print('================================')
                umv(Theta, angle)
                newdataset(f'{pos_key}_rocking_Theta_{j}')
                safe_kmap(nnp2, -2.5,2.5,100,nnp3, -2.5,2.5,100,0.02)
                safety_net()
            umv(Theta, 0)
            print('================================')
            print(f'measuring XRF at {pos_key}')
            print('================================')
            newdataset(f'{pos_key}_XRF_mapping')
            safe_kmap(nnp2, -2.5,2.5,100,nnp3, -2.5,2.5,100,0.2)
            safety_net()
            
        for i in range(5,7):
            pos_key = f'pos{i}'
            rstp(pos_key)
            print('================================')
            print(f'measuring XRF at {pos_key}')
            print('================================')
            newdataset(f'{pos_key}_XRF_mapping')
            safe_kmap(nnp2, -5,5,100,nnp3, -5,5,100,0.2)
            safety_net()
                
    finally:
        sc()
        sc()
        sc()        
                
            
def kmap_for_patch(patch_num):
    print('===================')
    print(f'mapping patch {patch_num}')
    print('====================')
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, -2.5,2.5,100,0.02)
    safety_net()
    

def dimanche_soir():
    so()
    load_script('karl_safe_scan_example')
    safe_meig_x()
    start_key = 'b'
    try:
        for i in range(-44, 46, 4):
            angle = i/100
            umv(Theta, angle)
            print('======================')
            print(f'going to angle {angle}')
            print('=====================')
            newdataset(f'patch_1_theta_{i}_{start_key}')
            kmap_for_patch(1)
            umvr(nny, 0.005)
            newdataset(f'patch_2_theta_{i}_{start_key}')
            kmap_for_patch(2)
            umvr(nnz, 0.005)
            newdataset(f'patch_3_theta_{i}_{start_key}')
            kmap_for_patch(3)
            umvr(nny, -0.005)
            newdataset(f'patch_4_theta_{i}_{start_key}')
            kmap_for_patch(4)
        umv(Theta, 0)
        print('================================')
        print(f'measuring XRF at patch 4')
        print('================================')
        newdataset(f'{patch_num}_XRF_mapping')
        safe_kmap(nnp2, -2.5,2.5,100,nnp3, -2.5,2.5,100,0.2)
        safety_net()        
            
    finally:
        sc()
        sc()
        sc()



def part2_mardi_soir():
    start_key = 'a'
    try:
        so()
        rstp('dmesh_pos1')
        newdataset(f'dmesh_pos1_bis_{start_key}')
        dmesh(nnp2, -100, 100, 200, nnp3, -100, 100, 200, 0.02)
        
        rstp('dmesh_pos2')
        newdataset(f'dmesh_pos2_bis_{start_key}')
        dmesh(nnp2, -100, 100, 200, nnp3, -100, 100, 200, 0.02)

        rstp('dmesh_pos1')
        newdataset(f'dmesh_backto_pos1_bis_{start_key}')
        dmesh(nnp2, -100, 100, 200, nnp3, -100, 100, 200, 0.02)
        
    finally:
        sc()
        sc()
        sc()
        


def mercredi_diner():
    try:
        rstp('new_pos2')
        newdataset('new_pos2_kmap')
        kmap.dkmap
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        
        rstp('new_pos5')
        newdataset('new_pos5_kmap')
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        
        rstp('new_pos8')
        newdataset('new_pos8_kmap')
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        kmap.dkmap(nnp2, -2, 2, 80, nnp3, 5, 15, 200, 0.02)
        
    finally:
        sc()
        sc()
        sc()
        



def large_rocking_kmaps():
    load_script('karl_safe_scan_example')
    umv(Theta, 0)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()
    
    umv(Theta, -0.02)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()
    
    umv(Theta, 0.02)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()
    
    umv(Theta, -0.04)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()
    
    umv(Theta, 0.04)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()

    umv(Theta, 0)
    safe_kmap(nnp2, -2.5,2.5,100,nnp3, 2,7,100,0.02)
    safety_net()

    
def small_rocking_kmaps():
    load_script('karl_safe_scan_example')
    umv(Theta, 0)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    
    umv(Theta, -0.02)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    
    umv(Theta, 0.02)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    
    umv(Theta, -0.04)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    
    umv(Theta, 0.04)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()

    umv(Theta, -0.06)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    
    umv(Theta, 0.06)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()

    umv(Theta, 0)
    safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
    safety_net()
    

    
def mercredi_soir():
    so()
    load_script('karl_safe_scan_example')
    start_key = 'a'   
    try:
        for i in range(3, 7):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_kmap_rocking_{start_key}')
            large_rocking_kmaps()
            
        for i in range(7, 11):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_kmap_rocking_{start_key}')
            small_rocking_kmaps()        
            
        for i in range(11, 13):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_repeat_kmap_{start_key}')
            for i in range(20):
                safe_kmap(nnp2, -1,1,40,nnp3, 2,4,40,0.02)
                safety_net()
                
    finally:
        sc()
        sc()
        sc()
        

def samp6_rocking_kmap():
    so()
    try:
        rstp('pos16')
        newdataset('pos16_kmap_rocking')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
        for i in range(1, 4):
            abs_angle = i * 0.02
            
            mes_angle = abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
            
            mes_angle = - abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
        umv(Theta, 0)
        kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
        
        rstp('pos17')
        newdataset('pos17_kmap_rocking')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
        for i in range(1, 4):
            abs_angle = i * 0.02
            
            mes_angle = abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
            
            mes_angle = - abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
        umv(Theta, 0)
        kmap.dkmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)

        
    finally:
        sc()
        sc()
        sc()
        
        
def samp6_last():
    so()
    try:
        rstp('pos18')
        newdataset('pos18_kmap_rocking')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
        for i in range(1, 4):
            abs_angle = i * 0.02
            
            mes_angle = abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
            
            mes_angle = - abs_angle
            umv(Theta, mes_angle)
            kmap.dkmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
        umv(Theta, 0)
        kmap.dkmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
        

        
    finally:
        sc()
        sc()
        sc()
        
        
def samp6_transmission_last():
    so()
    try:
        rstp('pos16')
        newdataset('back_pos16_transmission')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -3, 3, 60, nnp3, 1, 7, 60, 0.2)

        rstp('pos17')
        newdataset('back_pos17_transmission')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -3, 3, 60, nnp3, 1, 7, 60, 0.2)

        rstp('pos18')
        newdataset('back_pos18_transmission')
        umv(Theta, 0)
        kmap.dkmap(nnp2, -5, 5, 100, nnp3, 1, 11, 100, 0.2)
        
    finally:
        sc()
        sc()
        sc()


def back_samp3_nuit():
    so()
    load_script('karl_safe_scan_example')
    start_key = 'a'
    try:
        for i in range(7, 28):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_kmap_rocking_{start_key}')
            print('======================')
            print(f' measuring at {pos_key} at Theta = 0')
            print('======================')
            umv(Theta, 0)
            safe_kmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
            safety_net()
            for i in range(1, 4):
                abs_angle = i * 0.02
            
                mes_angle = abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
                safety_net()
            
                mes_angle = - abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
                safety_net()
            print('======================')
            print(f' measuring at {pos_key} at Theta = 0 FINAL')
            print('======================')
            umv(Theta, 0)
            safe_kmap(nnp2, -2, 2, 20, nnp3, 2, 6, 20, 0.02)
            safety_net()
        
    finally:
        sc()
        sc()
        sc()
                
        
def samp7_kmap_rocking():
    so()
    load_script('karl_safe_scan_example')
    start_key = 'a'
    try:
        for i in range(4, 25):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_kmap_rocking_{start_key}')
            print('======================')
            print(f' measuring at {pos_key} at Theta = -2 (theta 0)')
            print('======================')
            umv(Theta, -2)
            safe_kmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
            safety_net()
            for i in range(1, 4):
                abs_angle = i * 0.02
            
                mes_angle =  -2 + abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
                safety_net()
            
                mes_angle = -2 - abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
                safety_net()
            print('======================')
            print(f' measuring at {pos_key} at Theta = -2 (theta 0) FINAL')
            print('======================')
            umv(Theta, -2)
            safe_kmap(nnp2, -4, 4, 40, nnp3, 2, 10, 40, 0.02)
            safety_net()
        
    finally:
        sc()
        sc()
        sc()
        
def samp7_transmission_rocking():
    so()
    load_script('karl_safe_scan_example')
    start_key = 'a'
    try:
        for i in range(4, 25):
            pos_key = f'pos{i}'
            rstp(pos_key)
            newdataset(f'{pos_key}_transmission_rocking_{start_key}')
            print('======================')
            print(f' measuring at {pos_key} at Theta = -2 (theta 0)')
            print('======================')
            umv(Theta, -2)
            safe_kmap(nnp2, -5, 5, 100, nnp3, 1, 11, 100, 0.2)
            safety_net()
            
            for j in range(1, 3):
                abs_angle = j * 2
        
                mes_angle =  -2 + abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -5, 5, 100, nnp3, 1, 11, 100, 0.2)
                safety_net()
            
                mes_angle = -2 - abs_angle
                print('======================')
                print(f' measuring at {pos_key} at {mes_angle}')
                print('======================')
                umv(Theta, mes_angle)
                safe_kmap(nnp2, -5, 5, 100, nnp3, 1, 11, 100, 0.2)
                safety_net()
        print('======================')
        print(f' measuring at {pos_key} at Theta = -2 (theta 0)')
        print('======================')
        umv(Theta, -2)
        safe_kmap(nnp2, -5, 5, 100, nnp3, 1, 11, 100, 0.2)
        safety_net()
        
    finally:
        sc()
        sc()
        sc()

