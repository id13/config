def tune_energy(e):
    #mvr(ustry, -.5)
    if e > 18.0 or e <12.5:
        raise ValueError()
    ccmo.goto_energy(e)
    dscan(u18,-0.4,0.4,8,0.1)
    gop(u18)
    #umvr(ustry, 0.5)

def u_slop(e):
    m = (6.52-6.4)/(13.1-12.92)
    u = 6.4 + m*(e-12.92)
    return u

def goto_e(e):
    if e <12.0 or e > 17.1:
        raise ValueError(f'illegal energy: {e}')
    u = u_slop(e)
    ccmo.goto_energy(e, force=True)
    umv (u18, u)
    

    



def knife():
    mvr(ustry,0.5)
    dscan(ustry,-0.03,0.03,120,0.05)
    mvr(ustry,-0.5)

def shortbap():
    dscan(ufreyab,-0.05,0.05,20,0.05)
    goc(ufreyab)
    where()
    dscan(ufrebz,-0.05,0.05,20,0.05)
    goc(ufrebz)
    where()

def shortsap():
    dscan(usapy,-0.05,0.05,20,0.05)
    goc(usapy)
    where()
    dscan(usapz,-0.05,0.05,20,0.05)
    goc(usapz)
    where()


   

def fancy():
    raise RuntimeError
    POS = 'fancy6'
    stp(POS)
        
    mvr (ustry ,-0.1)
    for i in range(1,21):
        loopscan(i, 0.02, sleep_time=3)
        mvr(ustry, 0.01)
    rstp(POS)
    dkmapyz_2(-0.2,0.2,200, -0.1,0.1,100, 0.02, retveloc=5)
 
