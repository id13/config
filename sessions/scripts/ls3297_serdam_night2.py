print('ls3297_serdam_night2 - load 1')
START_KEY = 'b'

def night2_action(sd):
    #pass
    with bench(): sd.run_flymesh()

    
def ls3297_night2():

    rstp('night2_pos1')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_night2_v50_1')
    sd.setup_flymesh(0, 20, 500, 0, 3.5, 175, 0.0014, retveloc=15)
    sd.setup_fly_speed(50)
    sd.show_log()
    night2_action(sd)
    

    rstp('night2_pos2')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_night2_v40_2')
    sd.setup_flymesh(0, 20, 500, 0, 3.5, 175, 0.0014, retveloc=15)
    sd.setup_fly_speed(40)
    sd.show_log()
    night2_action(sd)


    rstp('night2_pos3')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_night2_v30_3')
    sd.setup_flymesh(0, 20, 500, 0, 3.5, 175, 0.0014, retveloc=15)
    sd.setup_fly_speed(30)
    sd.show_log()
    night2_action(sd)


    rstp('night2_pos4')
    sd = SerDam('FLY', f'{START_KEY}_x15keV_3pf_night2_v20_4')
    sd.setup_flymesh(0, 20, 500, 0, 3.5, 175, 0.0014, retveloc=15)
    sd.setup_fly_speed(20)
    sd.show_log()
    night2_action(sd)
