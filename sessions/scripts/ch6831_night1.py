print("ch6831 : load - 1")

def ch6831_night1():

    rstp('Y_h2_mid')
    newdataset('night1_Y_h2_mid')
    kmap.dkmap(nnp2, -20, 20, 400, nnp3, -30,30, 600, 0.02)

    rstp('Y_h2_base')
    newdataset('night1_Y_h2_base')
    kmap.dkmap(nnp2, -20, 20, 400, nnp3, -30,30, 600, 0.02)

    rstp('Y_s1_mid')
    newdataset('night1_Y_s1_mid')
    kmap.dkmap(nnp2, -20, 20, 400, nnp3, -30,30, 600, 0.02)

    rstp('Y_s1_base')
    newdataset('night1_Y_s1_base')
    kmap.dkmap(nnp2, -20, 20, 400, nnp3, -30,30, 600, 0.02)

def hex_recover():
    nnx.sync_hard()
    nny.sync_hard()
    nnz.sync_hard()
    nnu.sync_hard()
    nnv.sync_hard()
    nnw.sync_hard()

def ch6831_main():
    try:
        ch6831_night1()
    finally:
        sc()
        sc()
        sc()
