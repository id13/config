def caps_procedure():
    # return 'inactive'
    mvr(ustry, -0.25)
    mvr(ustrz, -0.25)
    dkmapyz_2(0,0.5,50,0,0.5,50,0.02)

def dummy(x=None):
    print(x)

# newdataset = dummy
# enddataset = dummy

def capsDRY_mt2():
    
    newdataset('empty_c')
    gopos('empty')
    caps_procedure()
    enddataset()
    
    newdataset('spdryD_c')
    gopos('spdryD_p1')
    caps_procedure()
    enddataset()
    
    newdataset('Saltdr_c')
    gopos('Saltdr_p1')
    caps_procedure()
    enddataset()
    
    newdataset('FAdryD_c')
    gopos('FAdryD_p1')
    caps_procedure()
    enddataset()
    
    newdataset('salDNA_c')
    gopos('salDNA_p1')
    caps_procedure()
    enddataset()
    
    newdataset('emptycap_c')
    gopos('emptycap_p1')
    caps_procedure()
    enddataset()
