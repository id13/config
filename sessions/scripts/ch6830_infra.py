def nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,40,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,40,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def fast_nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,10,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,10,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

XXX = '''
def mvr_setx_nt3(delta_ntx, npts):
    curr_ntx = nt1x.position
    positions = np.linspace(curr_ntx, curr_ntx+delta_ntx, npts)
    for i in range(npts):
        mv(nt1x, positions[i])
        fast_nt3align()
    nt3align()
'''


def rot_scan_series(dsname_base, th_start, th_stop, nintervals, *p):
    curr_th = Theta.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            mv(Theta, th)
            print(f'read Theta pos: {Theta.position}')
            kmap.dkmap(*p)
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)

def yrot_scan_series(dsname_base, th_start, th_stop, ystart, ystop, nintervals, *p):
    curr_th = Theta.position
    curr_y = nnp2.position
    try:

        th_pos_arr = np.linspace(th_start, th_stop, nintervals+1)
        y_pos_arr = np.linspace(ystart, ystop, nintervals+1)
        mv(Theta, th_start - 1.0)
        mv(Theta, th_start)
        for i, th in enumerate(th_pos_arr):
            dsname = f'{dsname_base}_{i:05d}_rotser'
            newdataset(dsname)
            print(f'==== dsname: {dsname} ==== cycle: {i}')
            print(f'next Theta pos: {th}')
            next_y = y_pos_arr[i]
            print(f'next y pos: {next_y}')
            mv(Theta, th)
            mv(nnp2, next_y)
            print(f'read Theta pos: {Theta.position}')
            print(f'read y pos: {nnp2.position}')
            kmap.dkmap(*p)
    finally:
        print(f'moving Theta back to initial pos: {curr_th}')
        mv(Theta, curr_th)
        mv(nnp2, curr_y)
