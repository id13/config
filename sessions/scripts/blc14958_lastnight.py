POS = '''

             nnx     nny     nnz
--------  ------  ------  ------
User    
 High     -0.678   4.680   6.000
 Current  -1.533  -0.384  -1.133
 Low      -2.678  -5.320  -6.000
Offset     0.000   0.000   0.000

Dial    
 High     -0.678   4.680   6.000
 Current  -1.533  -0.384  -1.133
 Low      -2.678  -5.320  -6.000

'''
def blc14958_lastnight():
    this_loffkmap = PsieLoffKmap(timeout=160)
    g = ConvGridi2D('<current>', 0.07, 16, '<current>', 0.07, 9, mot0=nny, mot1=nnz)
    try:
        for i in range(17*10):
            print(f'\n\ncycle {i} ===================================')
            g.goto_pos(i)
            this_loffkmap.run(nnp2, 0, 68, 68*5, nnp3, 0, 68, 68*5, 0.0005)
    finally:
        sc()
        sc()
        sc()
    
