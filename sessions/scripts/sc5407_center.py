print("sc5407_center - load 7")
import math as gmth
import os
from os import path
from collections import OrderedDict as Odict
import json
from scipy.optimize import leastsq


RPD = gmth.pi/180.0
DPR = 180.0/gmth.pi


ALLMOTNAMES = "nnx nny nnz smza nnp1 nnp2 nnp3".split()
SUBSETS = Odict(
    none = [],
    all  = ALLMOTNAMES,
    xyz  = "nnx nny nnz".split(),
    rxyz = "nnx nny nnz smza".split(),
    rxy  = "nnx nny smza".split(),
    nn   = "nnp1 nnp2 nnp3".split(),
    n23  = "nnp2 nnp3".split(),
    trans = 'nnx nny nnz nnp2 nnp3'.split(),
    n23r = "nnp2 nnp3 smza".split(),
    r    = "smza"
)
def make_sc5407_motdc():
    dc = Odict(
        nnx = nnx,
        nny = nny,
        nnz = nnz,
        nnp1 = nnp1,
		nnp2 = nnp2,
		nnp3 = nnp3,
        smza = smza
    )
    print (dc)
    for mn, mot in dc.items():
        assert mot.name == mn
    return dc



EXAMPLES = '''
load_script('sc5407_center')
sc5407_motdc = make_sc5407_motdc()
ggg = GenStorePos(sc5407_motdc, ALLMOTNAMES, SUBSETS)
cmg=CenMgr(ggg=ggg)


cmg.setup_from_pair180('a0', 'a1', 'acen')



'''

# centering ######################################

def calc_dist(x0, y0, x1, y1):
    dx = x0 - x1
    dy = y0 - y1
    d = gmth.sqrt(dx*dx + dy*dy)
    return d

def calc_phase(x0, y0, xc, yc):
    q = (y0-yc)/(x0-xc)
    phase = gmth.atan(q)
    return phase


class CenMgr(object):

    def __init__(self, ggg=None):
        self.ggg = ggg

    def setup_from_pair180(self, gposkey_a, gposkey_b, gposkey_cen):
        adc = self.ggg.load_pos(gposkey_a)
        bdc = self.ggg.load_pos(gposkey_b)
        the_dist = self.get_gposdc_dist(adc, bdc)
        self.amp = 0.5* the_dist
        annx = adc['nnx']
        anny = adc['nny']
        bnnx = bdc['nnx']
        bnny = bdc['nny']
        xc = 0.5*(annx + bnnx)
        yc = 0.5*(anny + bnny)
        print(adc, bdc)
        cendc = adc.copy()
        cendc['nnx'] = xc
        cendc['nny'] = yc
        self.center = xc, yc
        self.ggg.save_pos(gposkey_cen, cendc)
        the_phase = self.get_gposdc_phase(adc)
        self.phi_offset = adc['smza']/1000.0 - DPR*the_phase

    def smza_to_phi(self, smza_pos):
        return smza_pos/1000.0 - self.phi_offset

    def phi_to_smza(self, phi_value):
        return 1000.0*(phi_value + self.phi_offset)

    def get_gposdc_dist(self, gpdc_0, gpdc_1):
        nnx_0 = gpdc_0['nnx']
        nny_0 = gpdc_0['nny']
        nnx_1 = gpdc_1['nnx']
        nny_1 = gpdc_1['nny']
        return calc_dist(nnx_0, nny_0, nnx_1, nny_1)

    def get_gposdc_phase(self, gpdc):
        xc, yc = self.center
        nnx_0 = gpdc['nnx']
        nny_0 = gpdc['nny']
        phase = calc_phase(nnx_0, nny_0, xc, yc)
        print (f'phase = {DPR*phase*1000}')
        return phase

    def move_to_cen(self, phi_value, doit=True):
        smza_pos = self.phi_to_smza(phi_value)
        phi_rad = RPD*phi_value
        xc, yc = self.center
        
        xp = -self.amp*gmth.cos(phi_rad) + xc
        yp = -self.amp*gmth.sin(phi_rad) + yc
        print(f'smza, xp, yp = {smza_pos}, {xp}, {yp}')
        if doit:
            umv(smza, smza_pos)
            umv(nnx, xp)
            umv(nny, yp)


# stp business ######################################

class GenStorePos(object):

    POS_NAME_EXT = 'gjs'
    POS_FNAME_TPL = "{{}}.{}".format(POS_NAME_EXT)

    def __init__(self, motdc, allmotnames, subsets):
        self.motdc = motdc
        self.allmotnames = allmotnames
        self.subsets = subsets

    def make_fname(self, poskey):
        if poskey.endswith(self.POS_NAME_EXT):
            fname =  poskey
        else:
            fname = self.POS_FNAME_TPL.format(poskey)
        return fname

    def load_pos(self, poskey):
        fname = self.make_fname(poskey)
        with open(fname, 'r') as f:
            _posdc = json.load(f)
        posdc = Odict()
        for mn in self.allmotnames:
            posdc[mn] = float(_posdc[mn])
        return posdc

    def print_pos(self, posdc):
        for mn, pos in posdc.items():
            print(f"    {mn:10} = {pos}")

    def show_curr_pos(self):
        print('current:')
        for mn, mot in self.motdc.items():
            print(f'    {mn:10} = {mot.position}')

    def show_pos(self, poskey):
        self.show_curr_pos()
        posdc = self.load_pos(poskey)
        print(f'{poskey}:')
        self.print_pos(posdc)

    def lspos(self):
        ll = os.listdir()
        ll.sort()
        for fname in ll:
            if fname.endswith(self.POS_NAME_EXT):
               print(fname)

    def save_pos(self, poskey, posdc):
        fname = self.make_fname(poskey)
        if path.exists(fname):
            raise IOError(f'illegal attempt to overwritr posfile {fname}')
        with open(fname, 'w') as f:
            json.dump(posdc, f)

    def get_curr_posdc(self):
        for mn, mot in self.motdc.items():
            posdc[mn] = mot.position
        return posdc

    def gstp(self, poskey):
        posdc = Odict()
        for mn, mot in self.motdc.items():
            posdc[mn] = mot.position
        self.save_pos(poskey, posdc)

    def grstp(self, poskey, subsetkey="none"):
        posdc = self.load_pos(poskey)
        subset = self.subsets[subsetkey]
        for mn in subset:
            umv(self.motdc[mn], posdc[mn])

    def load_pos_series(tag, numlist, keys):
        resll = dict()
        for k in keys:
            res[k] = list()
        for i in numlist:
            pk = f'{tag}_{i:05}'
            pdc = self.load_pos(pk)
            for k in keys:
                res[k].append(pdc[k])
        resarrs = dict()
        for k in keys:
            resarrs[k] = np.array(resll[k], dtype=np.float64)
        return resarrs

    def sinfit_series(tag, numlist, xmotkey, ymotkey):
        dc = self.load_pos_series(tag, numlist, (xmotkey, ymotkey))
        xarr = dc[xmotkey]
        yarr = dc[ymotkey]



# sin fitting ######################################

def rfsin(p, y, x):
    return y - fsin(x, p)

def fsin(x, p):
    print("fsin x arrg:")
    print(x)
    A,ph,o = tuple(p)
    print("A,Ph,o=", A,ph,o)
    return A*np.sin(x-ph) + o

def sinfit(xarr, yarr, p=None):
    if None is p:
        p = np.array([1.0,0.0,0.0], dtype=np.float64)
    pl = leastsq(rfsin, p, args=(yarr, xarr))
    amp = pl[0][0]
    phase = pl[0][1]
    offset = pl[0][2]
    return (amp, phase, offset)

def get_sinfit_arrays(xarr, yarr, params):
    amp, phase, offset = params

def plotfit(x, y1, y2):
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.show()




