from bliss.config.channels import Channel
import gevent

K_CMD_EH2 = 'LS3301_cmd_FOR_EH2'

def get_channel(k, default=42):
    c = Channel(k, default_value=default)
    return c

def send_cmd(c, cmd):
    c.value = cmd

def ls3301_eh2_cmd_cb(cmd):
    print(f'{[cmd]}')
    w = cmd.split()
    if w[0] == 'domvr':
        fshclose()
        sleeptime = float(w[2])
        print('sleeping %dsec'%sleeptime)
        sleep(sleeptime)
        distance = float(w[1])
        #mot_nstretch = config.get('nstretch')
        mvr(nstretch, distance)
        fshopen()

class CCC(object):

    def __init__(self, ck=K_CMD_EH2, iseh2=False):
        self.c = get_channel(ck)
        if iseh2:
            self.c.register_callback(ls3301_eh2_cmd_cb)

    def send(self, cmd):
        print(f'{[cmd]}')
        send_cmd(self.c, cmd)

