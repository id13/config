print('eh3 dkpatch - load 2')

def dkmap_nnp56(ll1,ul1,nitv1,ll2,ul2,nitv2,exptime):
    
    ll1 = ll1*1000
    ul1 = ul1*1000
    ll2 = ll2*1000
    ul2 = ul2*1000
    np1 = nitv1 + 1
    np2 = nitv2 + 1
    print(nnp5, ll1, ul1, np1, nnp6, ll2, ul2, np2, exptime)
    wmars()
    kmap.dkmap(nnp5, ll1, ul1, np1, nnp6, ll2, ul2, np2, exptime)

def dkpatchmesh(dsprefix, ptch_step1,nitv1,ptch_step2,nitv2,exptime,nptch1,nptch2, ovlp1=0.0, ovlp2=0.0):
    pos1 = nny.position
    pos2 = nnz.position
    stp1 = ptch_step1/nitv1
    stp2 = ptch_step2/nitv2
    try:
        print ('DKPATCHMAP |||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
        msg = f'BEGIN: kpatch dataset with prefix: {dsprefix}'
        print (msg)
        try:
            elog_print(msg)
        except:
            print(f'WARNING: elog_print("{msg}") failed!')

        dc_2d = {}
        pos_list_1d = []
        idx_list_1d = []
        list_1d = []
        for i in range(nptch2):
            z_offset = pos2 + i*(ptch_step2 + stp2) - i*ovlp2
            for j in range(nptch1):
                y_offset = pos1 + j*(ptch_step1 + stp1) - j*ovlp1
                yz_tup = (y_offset, z_offset)
                idx_tup = (j,i)
                dc_2d[idx_tup] = yz_tup
                pos_list_1d.append(yz_tup)
                idx_list_1d.append(idx_tup)
        print ('\nscan info: (yidx, zidx), (y_oripos, z_oripos)')
        print ('-----------------------------------------------')
        for (k,x) in enumerate(zip(idx_list_1d, pos_list_1d)):
            (idx,yzt) = x
            print (k, idx, yzt)

        for i,(y,z) in enumerate(pos_list_1d):
            col = idx_list_1d[i][0]
            row = idx_list_1d[i][1]

            dsname = f'dkp_{dsprefix}_i{i:02}_c{col:02}_r{row:02}'
            msg = f'--------- BEGIN: measuring dkpatch dataset #{i}: {dsname}'
            print (msg)
            try:
                elog_print(msg)
            except:
                print(f'WARNING: elog_print("{msg}") failed!')

            newdataset(dsname)
            mv(nny, y)
            mv(nnz, z)
            zeromars()
            fshtrigger()
            dkmap_nnp56(-0.5*ptch_step1, 0.5*ptch_step1, nitv1, -0.5*ptch_step2, 0.5*ptch_step2, nitv2, exptime)
            enddataset()
            msg = f'-------- END:   measuring dkpatch dataset #{i}: {dsname}'
            print (msg)
            try:
                elog_print(msg)
            except:
                print(f'WARNING: elog_print("{msg}") failed!')
    finally:
            mv(nny, pos1)
            mv(nnz, pos2)
            print ('||||||||||||||| END DKPATCHMAP ')

    return
