print("ch6831_bstop : load - 3")

CH6831_OFFX = -51.0
CH6831_OFFY = 0.0
CH6831_OFFZ = 0.0
#CH6831_MY = 0.2075/40.0
#CH6831_MZ = -0.1095/40.0
CH6831_MY = 0.331/60.0
CH6831_MZ = -0.1442/60.0


class Ch6831Bstop(object):

    def __init__(self, offx=None, offy=None, offz=None,
        my=None, mz=None):
        self.mot_x = config.get('nbsx')
        self.mot_y = config.get('nbsy')
        self.mot_z = config.get('nbsz')
        self.offx = offx
        self.offy = offy
        self.offz = offz
        self.my = my
        self.mz = mz
        self.bs_x_low = -5.1
        self.bs_x_hi = 45.1

    def xmv(self, bs_x):
        tgt_x = bs_x + self.offx
        tgt_y = self.my*bs_x + self.offy
        tgt_z = self.mz*bs_x + self.offz
        umv(self.mot_x, tgt_x, self.mot_y, tgt_y, self.mot_z, tgt_z)

    def get_x(self):
        return self.mot_x.position - self.offx

    def show(self):
        print('current bs_x:', self.get_x())

    def xmvr(self, delta_bs_x):
        curr_bs_x = self.get_x()
        goto_bs_x = curr_bs_x + delta_bs_x
        self.xmv(goto_bs_x)

    def test_bs(self):
        diode_mot = config.get('ndiodey')
        print(diode_mot.position)
        if diode_mot.position < -0.1:
            raise RuntimeError(f'illegal diode position for beamstop test!')
        dscan(nbsy, -0.15,0.15, 30,0.05)
        dscan(nbsz, -0.15,0.15, 30,0.05)

    def bstop_series(self, start, stop, npts, expt):
        bs_x_ll = np.linspace(start, stop, npts)
        for p in bs_x_ll:
            self.xmv(p)

def make_bstop_obj():
    bstop_obj = Ch6831Bstop(
            offx = CH6831_OFFX,
            offy = CH6831_OFFY,
            offz = CH6831_OFFZ,
            my   = CH6831_MY,
            mz   = CH6831_MZ
        )
    return bstop_obj

