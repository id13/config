def wflu():
    wm(nfluomrx, nfluomry, nfluomrz)

def mgfx():
    MG_EH3fx.set_active()

def fx8roiset():
    fx8.rois.set('Si', 325, 376)
    fx8.rois.set('S', 389, 539)
    fx8.rois.set('K', 622, 690)
    fx8.rois.set('Ca', 700, 772)
    fx8.rois.set('Ti', 870, 936)
    fx8.rois.set('Mn', 1141, 1210)
    fx8.rois.set('Fe', 1223, 1335)
    fx8.rois.set('Ni', 1467, 1508)
    fx8.rois.set('Cu', 1561, 1646)
    fx8.rois.set('Zn', 1678, 1789)
    fx8.rois.set('Se', 2172, 2322)
    fx8.rois.set('Br', 2312, 2462)
    fx8.rois.set('Rb', 2617, 2719)
    fx8.rois.set('Sr', 2756, 2873)
    #fx8.rois.set('Y', 2910, 3045)
    #fx8.rois.set('Zr', 3061, 3215)
    #fx8.rois.set('Nb', 3243, 3364)
    #fx8.rois.set('Mo', 3449, 3558)
    #fx8.rois.set('Rayl', 3763, 3870)
    #fx8.rois.set('Compt', 3608, 3742)

def fx1roiset():
    fx1.rois.set('Si', 325, 376)
    fx1.rois.set('S', 389, 539)
    fx1.rois.set('K', 622, 690)
    fx1.rois.set('Ca', 700, 772)
    fx1.rois.set('Ti', 870, 936)
    fx1.rois.set('Mn', 1141, 1210)
    fx1.rois.set('Fe', 1223, 1335)
    fx1.rois.set('Ni', 1467, 1508)
    fx1.rois.set('Cu', 1561, 1646)
    fx1.rois.set('Zn', 1678, 1789)
    fx1.rois.set('Se', 2172, 2322)
    fx1.rois.set('Br', 2312, 2462)
    fx1.rois.set('Rb', 2617, 2719)
    fx1.rois.set('Sr', 2756, 2873)

def fxroiset():
    fx1roiset()
    fx8roiset()
