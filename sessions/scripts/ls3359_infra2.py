print('ls3359_infra2 - load 2')
from collections import OrderedDict as Odict
import json
from math import fabs

def write_json(fxpth, dc, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'w') as f:
        json.dump(dc, f)

def read_json(fxpth, dxpth=None):
    if not None is dxpth:
        path.join(dxpth, fxpth)
    with open(fxpth, 'r') as f:
        dc = json.load(f)
    return dc

def fine_nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,80,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,80,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,40,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,40,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def fast_nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,10,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,10,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

# +141
# -176
# -59
# -10

#
# y =
def flintp(ax, ay, bx, by, x):
    dx = bx - ax
    dy = by - ay
    m = dx/dy
    y = m*(x - ax) + ay
    return y

# usage from scratch:
#     load_script('ls3359_infra')
#     znedge = ZnEdge(ref_energy=9.667)
#     
# goto a delta-energy relative to ref_energy:
# example: delta_energy = -0.042
# znedge.goto_deltaE(-0.042)


# using "hoff"
# load_script('ls3359_infra2')
# hoff = make_hexoff_03()
# hoff.set_pos(*hoff.get_curr_pos())
# hoff.restore_pos(1)
# add the detectors to the measurement group
#MG_EH3a.enable('*xmap3*det0*')
#MG_EH3a.enable('*ct33*')
#MG_EH3a.enable('*eiger*')

# making a hoff calibration means designing a new func
# that makes your hoff ...
# e.g. make_hexoff_03() in this script
# note that you have to use the ekey "0" position twice
# 1. as reference (pos0) to the set_pos command
# 2. as offset calib pos like for the other ekeys
#    subsequnetly

def make_hexoff_01():
    znedge = ZnEdge(ref_energy=9.667)
    hexoff = HexOffset(znedge=znedge, table=TABLE_01)
    return hexoff


TABLE_01 = dict(
    etab = dict(
        {
            0 : 0.141,
            1 : 0.0,
            2 : -0.020,
            3 : -0.059 ,
            4 : -0.176,
            5 :-0.350
        }
    ),
    xtab = dict(
        {
           0 : 0.0,
           1 : 2.056-2.106,
           2 : 1.442-2.106,
           3 : 1.307-2.106,
           4 : 0.939-2.106,
           5 : 0.289-2.106
        }
    ),
    ytab = dict(
        {
           0 : 0.0,
           1 : 0.0,
           2 : 0.0,
           3 : 0.0,
           4 : 0.0,
           5 : 0.0,
        }
    ),
    ztab = dict(
        {
           0 : 0.0,
           1 : 0.0,
           2 : 0.0,
           3 : 0.0,
           4 : 0.0,
           5 : 0.0,
        }
    )
)

STP_CALIB_02 = '''
newfoc_0._pos
newfoc_m176._pos  newfoc_m350._pos
newfoc_141._pos  newfoc_m20._pos   newfoc_m59._pos
'''

def make_hexoff_02():
    znedge = ZnEdge(ref_energy=9.667)
    hexoff = HexOffset(znedge=znedge, table=TABLE_01)
    hexoff.ekey=0
    pos0 = hexoff.read_stp_pos('newfoc_141')
    hexoff.set_pos(*pos0)
    hexoff.store_offsets(0, fxpth='newfoc_141')
    hexoff.store_offsets(1, fxpth='newfoc_0')
    hexoff.store_offsets(2, fxpth='newfoc_m20')
    hexoff.store_offsets(3, fxpth='newfoc_m59')
    hexoff.store_offsets(4, fxpth='newfoc_m176')
    hexoff.store_offsets(5, fxpth='newfoc_m350')
    return hexoff

def make_hexoff_03():
    znedge = ZnEdge(ref_energy=9.667)
    hexoff = HexOffset(znedge=znedge, table=TABLE_01)
    hexoff.ekey=0
    pos0 = hexoff.read_stp_pos('a1_0')
    hexoff.set_pos(*pos0)
    hexoff.store_offsets(0, fxpth='a1_0')
    hexoff.store_offsets(1, fxpth='a1_1')
    hexoff.store_offsets(2, fxpth='a1_2')
    hexoff.store_offsets(3, fxpth='a1_3')
    hexoff.store_offsets(4, fxpth='a1_4')
    hexoff.store_offsets(5, fxpth='a1_5')
    return hexoff




class HexOffset(object):

    TKEYS = 'table pos0 ekey'.split()

    def __init__(self, znedge=None, table=None):
        self.ekey = None
        self.pos0 = None
        self.set_znedge(znedge)
        self.set_table(table)

    #####################
    # setup

    def set_znedge(self, znedge):
        self.znedge = znedge

    def set_table(self, table):
        self.table = table

    def set_pos(self, x0, y0, z0):
        self.pos0 = (x0, y0, z0)

    #####################
    # IO

    def read_stp_pos(self, fxpth):
        if not fxpth.endswith('._pos'):
            fxpth = f'{fxpth}._pos'
        posdc = read_json(fxpth)
        return (posdc['x'], posdc['y'], posdc['z'])

    def load(self, fxpth):
        all_dc = read_json(fxpth)
        for k in all_dc:
            setattr(self, k, all_dc[k])
        
    def save(self, fxpth):
        all_dc = dict()
        for k in self.TKEYS:
            all_dc[k] = getattr(self, k)
        write_json(fxpth, all_dc)

    def store_offsets(self, ekey, fxpth=None):

        if None is fxpth:
            x,y,z = self.get_currpos()
        else:
            x,y,z = self.read_stp_pos(fxpth)

        x0, y0, z0 = self.pos0
        self.table['xtab'][ekey] = x - x0
        self.table['ytab'][ekey] = y - y0
        self.table['ztab'][ekey] = z - z0

    def show(self):
        print('=== current zn denerg')
        print(f'ekey = {self.ekey}')
        denerg = self.ekey_to_denerg(self.ekey)
        print(f'denerg = {denerg}')
        print('=== current pos')
        print(f'pos = {self.pos0}')
        print('=== zn energy table:')
        pprint(self.table)

    #####################
    # bliss actions

    def trhex_goclick(self):
        f = get_flint(creation_allowed=False, mandatory=False)
        p = f.get_live_plot("default-scatter")
        position = p.select_points(1)
        (pos_nnp2, pos_nnp3) = position[0]
        dy = 0.001*(pos_nnp2-125)
        dz = 0.001*(pos_nnp3-125)
        self.trhex_mvr(dy=dy, dz=dz)

    def elem_mv(self, mot, p):
        if None is p:
            return
        else:
            mv(mot, p)

    def trhex_mvr(self, dx=None, dy=None, dz=None):
        (x, y, z) = self.get_curr_pos()
        if None is dx:
            tx = None
        else:
            tx = x + dx
        if None is dy:
            ty = None
        else:
            ty = y + dy
        if None is dz:
            tz = None
        else:
            tz = z + dz
        self.trhex_mv(x=tx, y=ty, z=tz)

    def trhex_mv(self, x=None, y=None, z=None):
        self.elem_mv(nnx, x)
        self.elem_mv(nny, y)
        self.elem_mv(nnz, z)

    def goto_denerg_by_key(self, ekey):
        self.ekey = None
        denerg = self.ekey_to_denerg(ekey)
        self.znedge.goto_deltaE(denerg)
        self.ekey = ekey

    def restore_pos(self, ekey, keep_energy=False):
        dx, dy, dz = self.ekey_to_offset(ekey)
        x0, y0, z0 = self.pos0
        tx = x0 + dx
        ty = y0 + dy
        tz = z0 + dz
        print('delta:', dx, dy, dz)
        print('pos0',  x0, y0, z0)
        print('target', tx, ty, tz)
        print('will go to:', tx, ty, tz)
        self.trhex_mv(x=tx, y=ty, z=tz)
        if not keep_energy:
            self.goto_denerg_by_key(ekey)

    #####################
    # conversions and getters

    def get_curr_pos(self):
        return (
            nnx.position,
            nny.position,
            nnz.position
            )
    
    def get_ptabs(self):
        tb = self.table
        return(tb['xtab'], tb['ytab'], tb['ztab'])

    def convert_pos(self, in_ekey, out_ekey, in_x, in_y, in_z):
        in_off_x, in_off_y, in_off_z  = self.ekey_to_offset(in_ekey)
        out_off_x, out_off_y, out_off_z  = self.ekey_to_offset(out_ekey)
        out_x = in_x - in_off_x + out_off_x
        out_y = in_y - in_off_y + out_off_y
        out_z = in_z - in_off_z + out_off_z
        return out_x, out_y, out_z

    def denerg_to_ekey(self, denerg, eps=0.004):
        etab = self.table['etab']
        for k, v in etab.items():
            if fabs(denerg-v) < eps:
                return k

    def ekey_to_denerg(self, ekey):
        return self.table['etab'][ekey]

    def ekey_to_offset(self, ekey):
        tx, ty, tz = self.get_ptabs()
        return tx[ekey], ty[ekey], tz[ekey]

class ZnEdge(object):

    AX = -11.4889
    AY = 15.603
    BX = -11.0939
    BY = 15.993

    def __init__(self, ax=None, ay=None, bx=None, by=None, ref_energy=None):
        t = ax, ay, bx, by
        if ((None,)*4) == t:
            ax = self.AX
            ay = self.AY
            bx = self.BX
            by = self.BY
        else:
            if None in t:
                raise ValueError('gap calib params not specified ...')
        self.calib = ax, ay, bx, by
        self.ref_energy = ref_energy

    def get_energy_offset(self):
        return ccmo.get_energy() - self.ref_energy

    def lintp(self, x):
        return flintp(*(self.calib + (x,)))

    def goto_deltaE(self, de, adapt_gap=True):
        ccmo.goto_energy(self.ref_energy + de, force=True)
        ang = ccth.position

        if adapt_gap:
            gap_val = self.lintp(ang)
            mv(u35, gap_val)
