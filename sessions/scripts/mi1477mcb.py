print('mi1477mcb - load 1')
import numpy as np

def test_001():
    collection_name = cn = ''
    dsname = dsn = ''

    ldp = get_local_data_dapth(cn, dsn)
    fupth = get_local_data_fupth(cn, dsn)
    fapth = get_local_data_fapth(cn, dsn)

    print(f'local_data_dapth = {ldp}')
    print(f'local_data_fupth = {fupth}')
    print(f'local_data_fapth = {fapth}')

    ldp = get_current_data_dapth()
    fupth = get_current_data_fupth()
    fapth = get_current_data_fapth()

    print(f'current_data_dapth = {ldp}')
    print(f'current_data_fupth = {fupth}')
    print(f'current_data_fapth = {fapth}')

def test_003():
    fapth = get_current_data_fapth()
    sc = SCANS[-1]
    scanno = int(sc.scan_number)
    scanno_s = f'{scanno:1d}.1'
    print(scanno_s)
    data_h5path = (scanno_s, 'measurement', 'eiger_roi1_avg')
    title_h5path = (scanno_s, 'title')
    with H5FileReadonly(fapth) as h5ro:
        title = h5path_get(h5ro, title_h5path)
        title_s = title.asstr()[()]
        print(title_s, type(title_s))
        scan_tipe, spar_s = preprocess_sctitle(title_s)
        print (scan_tipe, spar_s)
        mesh_params = to_mesh_params(spar_s)
        print(mesh_params)

        h5data = h5path_get(h5ro, data_h5path)
        arr = np.array(h5data).reshape(mesh_params[0])
        print(arr)
        print(arr.shape)
    return arr
    
def test_002():
    fapth = '/data/visitor/mi1477/id13/20231031/RAW_DATA/simense_ptyco_test/simense_ptyco_test_defocuse0p12mm_100nm/simense_ptyco_test_defocuse0p12mm_100nm.h5'
    scanno = 3
    scanno_s = f'{scanno:1d}.1'
    itempath = (scanno_s, 'measurement', 'eiger_roi3_avg')
    with H5FileReadonly(fapth) as h5ro:
        h5data = h5path_get(h5ro, itempath)
        arr = np.array(h5data)
        print(arr)

def get_nny_corr(h5ro):
    itempath = (scanno_s, 'measurement', 'eiger_roi3_avg')
    h5data = h5path_get(h5ro, itempath)
    arr = np.array(h5data)
    delta_nny = 0 # :)
    #    ...
    return delta_nny

def sketch():
    fapth = get_current_data_fapth()
    sc = SCANS[-1]
    scanno = int(sc.scan_number)
    scanno_s = f'{scanno:1d}.1'
    with H5FileReadonly(fapth) as h5ro:
        delta_nny = get_nny_corr(h5ro)
