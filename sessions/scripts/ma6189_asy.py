import gevent

def asy_fsh_gate(delay, duration):
    gevent.sleep(delay)
    fshopen()
    gevent.sleep(duration)
    fshclose()

def asy_loopscan(npts, expt, delay, duration):
    fshclose()
    _loopscan = loopscan(npts, expt, run=False)
    gevent.spawn(asy_fsh_gate, delay, duration)
    with bench():
        _loopscan.run()
    fshtrigger()
