from os import path
import numpy as np
import fabio
from fabio.edfimage import EdfImage

class EDFSingle(object):

    def __init__(self, fxpth=None, mode='inp'):
        assert fxpth.endswith('.edf')
        self.mode = mode
        self.fxpath = fxpth

    def __len__(self):
        return 1

    def __getitem__(self, i):
        if self.mode != 'inp':
            raise ValueError(f'illegal mode for output: {mode}')
        if i != 0:
            raise IndexError
        edf = fabio.open(self.fxpath)
        return edf.data

    def __setitem__(self, i, arr):
        if i != 0:
            raise IndexError
        if self.mode != 'out':
            raise ValueError(f'illegal mode for output: {mode}')
        edf = EdfImage()
        edf.data = arr
        edf.write(self.fxpath)

class EDFClosedKeySet_RW(object):

    def __init__(self, fxpth_tpl=None, keys=()):
        assert fxpth_tpl.endswith('.edf')
        self.fxpath_tpl = fxpth_tpl
        self.key_set = set()
        self.add_keys(keys)



    def add_keys(self, keys):
        key_set = self.key_set
        for k in keys:
            key_set.add(k)

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, k):
        if k in self.key_set:
            fxpath = self.fxpath_tpl.format(k)
            edf = fabio.open(fxpath)
            return edf.data
        else:
            raise KeyError(f'illegal key {k} EDFClosedKeySet_RW')

    def __setitem__(self, k, arr):
        if k in self.key_set:
            fxpath = self.fxpath_tpl.format(k)
            edf = EdfImage()
            edf.data = arr
            edf.write(fxpath)
        else:
            raise KeyError(f'illegal key {k} EDFClosedKeySet_RW')

    def __generator(self):
        for k in self.keys:
            fxpath = self.fxpath_tpl.format(k)
            with fabio.open(fxpath) as edf:
                arr = edf.data
            yield arr

    def __iter__(self):
        return self.__generator()

class EDFSimpleFupth_FreeKey_RW(object):

    def __init__(self, fupth_tpl=None, dapth=None):
        if not None in (fupth_tpl, dapth):
            self.set_tpl(fupth_tpl=fupth_tpl, dapth=dapth)

    def set_tpl(self, fupth_tpl=None, dapth=None):
        self.dapth = dapth
        self.fupth_tpl = fupth_tpl
        self.fapth_tpl = path.join(dapth, fupth_tpl)

    def __getitem__(self, k):
        fapath = self.fapath_tpl.format(k)
        edf = fabio.open(fapath)
        return edf.data

    def __setitem__(self, k, arr):
        fapth = self.fapth_tpl.format(k)
        edf = EdfImage()
        edf.data = arr
        edf.write(fapth)

class EDFCumArray_RW(object):
    """
    EDF Varray for samll reduced data series
    For efficiency use ithe file series feature of fabio
    """

    def __init__(self, fxpthlist=None, frameshape=None):
        self.frameshape = frameshape  
        self.fapthlist = [path.abspath(x) for x in fxpthlist]
        self.leng = len(fxpthlist)

    def _check_frameshape(self, arr):
        fs = self.frameshape
        if None is fs:
            self.frameshape = arr.shape
        else:
            if arr.shape != fs:
                raise ValueError(f'shape mismatch: arr.shape = {arr.shape}, frameshape = {fs}')

    def __len__(self):
        return self.leng

    def __getitem__(self, i):
        with fabio.open(self.fapthlist[i]) as edf:
            arr = edf.data
        self._check_frameshape(arr)
        return arr

    def __setitem__(self, i, arr):
        self._check_frameshape(arr)
        edf = EdfImage()
        edf.data = arr
        edf.write(self.fapthlist[i])

    def __generator(self):
        for fp in self.fapthlist:
            with fabio.open(fp) as edf:
                arr = edf.data
            self._check_frameshape(arr)
            yield arr

    def __iter__(self):
        return self.__generator()



def _test1():
    N = 100
    x = np.arange(N*N, dtype=np.uint16).reshape((N,N))
    edf_inp = EDFSingle(fxpth='./test_0000.edf', mode='inp')
    edf_out = EDFSingle(fxpth='./out_test_0000.edf', mode='out')
    arr = edf_inp[0]
    edf_out[0] = arr

def _test2():
    N = 100
    x = np.arange(N*N, dtype=np.uint16).reshape((N,N))
    fxpthlist = [f'x_{i:04}.edf' for i in range(10)]
    eca_w = EDFCumArray_RW(fxpthlist=fxpthlist)
    for i in range(len(eca_w)):
        arr = np.sin(i*0.1*x).astype(np.float32)
        eca_w[i] = arr

    eca_r = EDFCumArray_RW(fxpthlist=fxpthlist)
    for i,arr in enumerate(iter(eca_r)):
        print (i)
        print (arr)


if __name__ == '__main__':
    _test2()
