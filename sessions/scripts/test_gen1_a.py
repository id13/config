# PURP test predicion kit (related to CH7039, Tilman)
print('loading test_gen1_a - load 2')

from os import path

# loading
load_script('gen1_edfsupp')
load_script('gen1_scanaux')
load_script('gen1_align')
load_script('gen1_pillar')


CTR_LL_1 = [
    'ct22',
    'ct24'
]

CTR_LL = CTR_LL_1

class DsnBidon(object):

    __slots__ = "ome_pint ome_pfrac ome_val kap kap_val idxa idxb start_key fulldsn spec4 minisi kmapinfo oy_pos o_zpos ibounds2d".split()

    def __init__(self):
        for k in self.__class__.__slots__:
            setattr(self, k, None)

    def __str__(self):
        ll = []
        for k in self.__class__.__slots__:
            ll.append(f'    {k} = {str(getattr(self, k))}')
        return '\n'.join(ll)

    def to_dict(self):
        dc = dict((k, getattr(self, k)) for k in self.__class__.__slots__)
        dc['minisi'] = '<void>'
        dc['kmapinfo'] = '<void>'
        return dc

    def fromdict(self, dc):
        for k, v in dc.items():
            setattr(self, k, v)

class TT_s1526_3_INI(object):

    def __init__(self):
        self.def_ctr_names = ['eiger_roi1_avg', 'eiger_roi2_avg']
        self.out_data_dapth = '/data/visitor/ch7039/id13/20240501/PROCESSED_DATA/align_test'
        self.raw_data_dapth = '/data/visitor/ch7039/id13/20240501/RAW_DATA'
        self.samp_name = 's1526_3'

class TT_coral_INI(object):

    def __init__(self):
        self.def_ctr_names = ['eiger_roi1_avg', 'eiger_roi2_avg']
        self.out_data_dapth = '/data/visitor/ch7039/id13/20240501/PROCESSED_DATA/align_test'
        self.raw_data_dapth = '/data/visitor/ch7039/id13/20240501/RAW_DATA'
        self.samp_name = 'coral'


class TT(object):

    def __init__(self, ini):
        self.def_ctr_names = ini.def_ctr_names
        self.out_data_dapth = ini.out_data_dapth
        self.raw_data_dapth = ini.raw_data_dapth
        self.samp_name = ini.samp_name
        self.samp_dapth = path.join(self.raw_data_dapth, self.samp_name)

    def make_outdir(self, out_dupth):
        return path.join(self.out_data_dapth, out_dupth)

    def get_fulldsnames(self):
        ll = os.listdir(self.samp_dapth)
        resll =  list(x for x in ll if x.endswith('diff'))
        resll.sort()
        return resll

    def parse(self, fulldsname):
        print(f'parsing: {fulldsname}')
        #res= DsnBidon()
        res = Box()
        w = fulldsname.split('_')
        assert w[-1] == 'diff'
        ome_str = w[-2].replace('m','-')
        (ome_pint, ome_pfrac) = ome_str.split('p')
        res.ome_pint = int(ome_pint)
        res.ome_pfrac = int(ome_pfrac)
        res.ome_val = float(ome_str.replace('p','.'))
        res.kap = int(w[-3])
        res.kap_val = float(res.kap)
        res.idxa = int(w[-5])
        res.idxb = int(w[-4])
        res.start_key = w[-6]
        res.fulldsn = fulldsname
        res.spec4 = self.fulldsn_to_spec4(fulldsname)
        return res

    def get_fulldsn_ll1(self):
        ll = self.get_fulldsnames()
        resll = []
        for fdsn in ll:
            bd = self.parse(fdsn)
            #if bd.idxb < 3 and bd.start_key == 'b':
            if bd.start_key == 'a':
                print(f'fdsn: {fdsn}')
                print(str(bd))
                resll.append(bd)
        return resll

    def fulldsn_to_spec4(self, fulldsn):
        samp_name = self.samp_name
        dsname = fulldsn[len(samp_name)+1:]
        scanno = 1
        det_name = 'eiger'
        return (samp_name, dsname, scanno, det_name)

    def spec4_to_fapth(self, spec4):
        sn, dsn = spec4[:2]
        return path.join(self.raw_data_dapth, sn, f'{sn}_{dsn}', f'{sn}_{dsn}.h5')

    def spec4_to_minisi(self, spec4):
        data_fapth = self.spec4_to_fapth(spec4)
        return MiniScanInfra(self.def_ctr_names, data_fapth=data_fapth)


def test_001():
    out_fupth_tpltpl = 'x_%03d_{}.edf'
    ctr_name = 'eiger_roi1_avg'
    ctr_lst = ['eiger_roi1_avg']
    #ini = TT_s1526_3_INI()
    ini = TT_coral_INI()

    dobj = TT(ini)
    ll1 = dobj.get_fulldsn_ll1()
    out_dapth = dobj.make_outdir('out3')
    try:
        os.mkdir(out_dapth)
    except OSError:
        pass

    eval_fapth = path.join(out_dapth, 'eval.json')
    evdb = EvalDb(eval_fapth)

    for i in range(len(ll1)):
        out_fupth_tpl = out_fupth_tpltpl % i
        out_fapth_tpl = path.join(out_dapth, out_fupth_tpl)
        bd = ll1[i]
        bd.ctr_name = ctr_name
        minisi = dobj.spec4_to_minisi(bd.spec4)
        kmi = minisi.get_kmapinfo(1, [ctr_name])
        ctr_map_arr = kmi.data[ctr_name]
        print(f'out_fapth_tpl: {out_fapth_tpl}')
        objparser = ObjParser1(ctr_map_arr, out_fapth_tpl)
        check_flg, ibounds2d = objparser.parse()
        print(ctr_map_arr.shape, check_flg, ibounds2d)
        corrinfo = kmi.get_corr_info(ibounds2d)
        print_box(corrinfo)
        evdb.add_record(i, (bd, corrinfo))
    evdb.save()

    new_evdb = EvalDb(eval_fapth)
    new_evdb.load()
    keys = list(new_evdb.dc.keys())
    keys.sort()
    for idx in keys:
        (bd, corrinfo) = new_evdb.get_record(idx)
        print('====', idx)
        print_box(bd)
        print_box(corrinfo)

    
    
        

    
    


        

def test_dataacess():
    if 0:
        collection_name = cn = ''
        dsname = dsn = ''
        ldp = get_local_data_dapth(cn, dsn)
        fupth = get_local_data_fupth(cn, dsn)
        fapth = get_local_data_fapth(cn, dsn)

        print(f'local_data_dapth = {ldp}')
        print(f'local_data_fupth = {fupth}')
        print(f'local_data_fapth = {fapth}')

    if 1:

        ldp = get_current_data_dapth()
        fupth = get_current_data_fupth()
        fapth = get_current_data_fapth()

        print(f'current_data_dapth = {ldp}')
        print(f'current_data_fupth = {fupth}')
        print(f'current_data_fapth = {fapth}')

def test_scaninfra():
    sinfra = MiniScanInfra(CTR_LL)
    return sinfra, sinfra.get_kmapinfo(8)
