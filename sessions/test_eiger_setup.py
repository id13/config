
from bliss.setup_globals import *

load_script("test_eiger.py")

print("")
print("Welcome to your new 'test_eiger' BLISS session !! ")
print("")
print("You can now customize your 'test_eiger' session by changing files:")
print("   * /test_eiger_setup.py ")
print("   * /test_eiger.yml ")
print("   * /scripts/test_eiger.py ")
print("")