
from bliss.setup_globals import *
from id13.scripts import icepaptrack

load_script("lab.py")

print("")
print("Welcome to your new 'lab' BLISS session !! ")
print("")
print("You can now customize your 'lab' session by changing files:")
print("   * /lab_setup.py ")
print("   * /lab.yml ")
print("   * /scripts/lab.py ")
print("")