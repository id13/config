
from bliss.setup_globals import *

load_script("michael.py")

print("")
print("Welcome to your new 'michael' BLISS session !! ")
print("")
print("You can now customize your 'michael' session by changing files:")
print("   * /michael_setup.py ")
print("   * /michael.yml ")
print("   * /scripts/michael.py ")
print("")

print("+ set the default chain ...")
DEFAULT_CHAIN.set_settings(chain_sim_cam1['chain_config'])
