
from bliss.setup_globals import *

load_script("optics.py")

print("")
print("Welcome to your new 'oh' BLISS session !! ")
print("")
print("You can now customize your 'oh' session by changing files:")
print("   * /oh_setup.py ")
print("   * /oh.yml ")
print("   * /scripts/oh.py ")
print("")
