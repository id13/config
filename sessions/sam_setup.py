
from bliss.setup_globals import *

load_script("sam.py")

print("")
print("Welcome to your new 'sam' BLISS session !! ")
print("")
print("You can now customize your 'sam' session by changing files:")
print("   * /sam_setup.py ")
print("   * /sam.yml ")
print("   * /scripts/sam.py ")
print("")

DEFAULT_CHAIN.set_settings(chain_pcoedge_hw["chain_config"])