
from bliss.setup_globals import *

load_script("eigerwatch.py")

print("")
print("Welcome to your new 'eigerwatch' BLISS session !! ")
print("")
print("You can now customize your 'eigerwatch' session by changing files:")
print("   * /eigerwatch_setup.py ")
print("   * /eigerwatch.yml ")
print("   * /scripts/eigerwatch.py ")
print("")