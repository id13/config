
from bliss.setup_globals import *

load_script("seb.py")

print("")
print("Welcome to your new 'seb' BLISS session !! ")
print("")
print("You can now customize your 'seb' session by changing files:")
print("   * /seb_setup.py ")
print("   * /seb.yml ")
print("   * /scripts/seb.py ")
print("")
from bliss import setup_globals

default_chain = setup_globals.default_chain_eh3
DEFAULT_CHAIN.set_settings(default_chain['chain_config'])

print("Loading kmap")
from id13.scripts import kmap
