from bliss.setup_globals import *

load_script("manfred.py")

print("")
print("Welcome to your new 'manfred' BLISS session !! ")
print("")
print("You can now customize your 'manfred' session by changing files:")
print("   * /manfred_setup.py ")
print("   * /manfred.yml ")
print("   * /scripts/manfred.py ")
print("")
print("This session was created to showcase a XMLRPC server.")
print("Now it is part of the ID13 contribution to 'The Good, the Bad and the Ugly of BLISS' - 13. Dec 2018")

import sys
sys.path.append('/data/id13/inhouse2/AJ/skript')
import fileIO.datafiles.save_data as odata
import numpy as np


import subprocess
import xmlrpc.client
from SimpleXMLRPCServer import SimpleXMLRPCServer
#s = xmlrpclib.ServerProxy('http://localhost:8020')

LUT = {}


### misc utils
### see /data/id13/inhouse11/THEDATA_I11_1/d_2018-11-13_inh_ihma67_pre/OPERATION/BLISS/aj_utils.py
### for the original, working functions. These here are demonstartion dummies!


def get_lut(fname):
    data,header = odata.open_data(fname)
    lut_dict = {}
    print('loaded lookup for {}'.format(header[0]))
    for i, data_name in enumerate(header):
        print('found positions for {}'.format(data_name))
        lut_dict.update({data_name:data[:,i]})
    return lut_dict

def get_kmap_slow_motorset(sms_dict,smphi_positions):
    kmap_slow=['smphi',smphi_positions]          
    for mot_name in ['smx','smy']:                                                                                                                                    
        kmap_slow.append(mot_name)                                                                                          
        kmap_slow.append(get_lut_positions(mot_name,sms_dict,smphi_positions))  
    return kmap_slow


def get_lut_positions(mot_name, lut_dict, smphi_positions):
    phi = lut_dict['smphi']
    lut_pos = lut_dict[mot_name]
    mot_start_pos = 0
    return np.interp(smphi_positions, phi, lut_pos)


### registered functions
### need a return value!


def mcb_sync():
    sync()
    return "The real thing!"

def server_print(msg):
    print(msg)
    return 0


def lut_mv_phi(position):
    '''
    devides the mv to <position> into small steps so that the lookup correction is 'continiously' aplied 
    '''
    # curr_pos = smphi.position()
    curr_pos = 0
    int_pos = np.linspace(curr_pos,position,int(abs(position-curr_pos))+2)
    # print('intermediate positions: ', int_pos)
    kmap_slow = get_kmap_slow_motorset(LUT, int_pos)

    # move the kmap_slow [mot,pos etc. ] list to pos i in parallel:
    for i in range(len(int_pos)):
        mot_pos_list = []
        for j in range(0,len(kmap_slow),2):
            mot_pos_list.append(kmap_slow[j])
            mot_pos_list.append(kmap_slow[j+1][i])
        print('lut_move {} to {}'.format(mot_pos_list[0],mot_pos_list[1]))
        print('  - move {} to {}'.format(mot_pos_list[2],mot_pos_list[3]))
        print('  - move {} to {}'.format(mot_pos_list[4],mot_pos_list[5]))

    return 0


def load_lookuptable(fname):
    LUT.update(get_lut(fname))
    
    x=LUT['smphi'][:50]
    y1=LUT['smy'][:50]
    y2=LUT['smx'][:50]

    gnuplot = subprocess.Popen(["/usr/bin/gnuplot"], 
                               stdin=subprocess.PIPE)
    gnuplot.stdin.write("set term dumb 79 25\n")
    gnuplot.stdin.write('set xlabel "smphi [degrees]"\n')
    gnuplot.stdin.write("plot '-' using 1:2 title 'smy lookuptable' with linespoints\n")
    for i,j in zip(x,y1):
        gnuplot.stdin.write("%f %f\n" % (i,j))
    gnuplot.stdin.write("e\n")
    gnuplot.stdin.flush()


    gnuplot = subprocess.Popen(["/usr/bin/gnuplot"], 
                               stdin=subprocess.PIPE)
    gnuplot.stdin.write("set term dumb 79 25\n")
    gnuplot.stdin.write('set xlabel "smphi [degrees]"\n')
    gnuplot.stdin.write("plot '-' using 1:2 title 'smx lookuptable' with linespoints \n")
    for i,j in zip(x,y2):
        gnuplot.stdin.write("%f %f\n" % (i,j))
    gnuplot.stdin.write("e\n")
    gnuplot.stdin.flush()


    return 0


def where_is_fluoa():
    wm(fluoax,fluoay,fluoaz)
    
    return (fluoax.position(),fluoay.position(),fluoaz.position())

def start_pihexa_sync_server():
    server = SimpleXMLRPCServer(("lid13eh31", 8020))
    server.register_function(mcb_sync)
    server.register_function(load_lookuptable)
    server.register_function(server_print)
    server.register_function(lut_mv_phi)
    server.register_function(where_is_fluoa)
    

    return server

svr = start_pihexa_sync_server()
svr.serve_forever()



