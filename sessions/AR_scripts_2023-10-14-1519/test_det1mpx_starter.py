print('load 1')
import traceback
import time
def set_condition(idx, attempt):
    # newdataset
    print('\n\n\n======================== setting condition:')
    print(f'    condition index = {idx}')
    print(f'    condition attempt = {attempt}')


def x_check_dets():
    mgallmpx()
    th1 = mpxeh2he.camera.energy_threshold
    th2 = mpxeh2cdte.camera.energy_threshold
    if th1 < 0.5*23.4:
        raise RuntimeError()
    if th2 < 0.5*23.4:
        raise RuntimeError()

def doo_scan(idx, attempt):
    print('\n\n\n======================== setting condition:')
    print(f'    condition index = {idx}')
    print(f'    condition attempt = {attempt}')
    newdataset(f'f_doo_scan_{idx:04d}_{attempt}')
    umv(ustry, 56.55200, ustrz,   -7.46600)
    dkmapyz_2(-0.1,0.1,10,-0.1,0.1,10, 0.01, 5.0)

class THEID(object):

    CMD_ID = 400

def det1starter_macro(theidobj, idx, attempt, maxattempts=3):
    if attempt > maxattempts:
        raise RuntimeError(f'maxattempts reached (attempt {attempt} maxattempts {maxattempts}')
    else:
        try:
            doo_scan(idx, attempt)
        except:
            print(traceback.format_exc())
            print(f'failure {idx} attempt {attempt}')
            THEID.CMD_ID = THEID.CMD_ID+1
            com_sender(THEID.CMD_ID, 'restart_both')
            try:
                time.sleep(20)
                x_check_dets()
            except:
                time.sleep(40)
                x_check_dets()
            det1starter_macro(theidobj, idx, attempt+1, maxattempts=maxattempts)

def det1starter_test_macro():

    for i in range(2):
        det1starter_macro(THEID, i, 1, maxattempts=10)
