import time
print ("version 5")

def defocus_patch():
    npatch = 2
    nnp2_start = 125.0 + npatch*8.0/2 ################## to be checked
    nnp3_start = 125.0 - npatch*8.0/2 ################## to be checked

    mv(nnp2, nnp2_start)
    mv(nnp3, nnp3_start)

    for i in range(npatch):
        print ("===========================------- i cycle:", i)
        nnp3_pos = nnp3_start + i*8.0
        print("nnp3_pos =", nnp3_pos)
        mv(nnp3, nnp3_pos)

        for j in range(npatch):
            print ("==== j cycle:", j, "  (i=%1d)" % i)
            nnp2_pos = nnp2_start - j*8.0
            print("nnp2_pos =", nnp2_pos)
            mv(nnp2, nnp2_pos)
            kmap.dkmap(nnp3,-4,4,160,nnp2,4,-4,40,0.050, frames_per_file=200)
            ################### check the exposure time before run

def infocus_patch():
    npatch = 4
    nnp2_start = 125.0 + npatch*4.0/2 ################## to be checked
    nnp3_start = 125.0 - npatch*4.0/2 ################## to be checked

    mv(nnp2, nnp2_start)
    mv(nnp3, nnp3_start)

    for i in range(npatch):
        print ("===========================------- i cycle:", i)
        nnp3_pos = nnp3_start + i*4.0
        print("nnp3_pos =", nnp3_pos)
        mv(nnp3, nnp3_pos)

        for j in range(npatch):
            print ("==== j cycle:", j, "  (i=%1d)" % i)
            nnp2_pos = nnp2_start - j*4.0
            print("nnp2_pos =", nnp2_pos)
            mv(nnp2, nnp2_pos)
            kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,0.010, frames_per_file=200)
            ################### check the exposure time before run


def infocus_run():
    mgeig_x()
    fshtrigger()
    so()
    time.sleep(5)
    print("running infocus patch ...")
    try:
        infocus_patch()
        zeronnp()
        sc()
    finally:
        zeronnp()
        sc()
        sc()

def defocus_run():
    mgeig_x()
    fshtrigger()
    so()
    time.sleep(5)
    print("running defocus patch ...")
    try:
        defocus_patch()
        zeronnp()
        sc()
    finally:
        zeronnp()
        sc()
        sc()
