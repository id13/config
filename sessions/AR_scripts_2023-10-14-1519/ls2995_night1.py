print('ls2995 v.9')
launch_prefix = 'f'
def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-10]
    s.replace('-','_')
    nl  = s.split('_')
    sn  = launch_prefix + '_' + nl[1]+'_'+nl[2] #x[:-17]#sample name
    rsl = nl[-2] #resolution
    if rsl == 'lr':
        patch_size = 1 
    if rsl == 'hr':
        patch_size = 0.4
    nn = nl[-1].split('x')
    pr  = int(nn[0])  #patch_row
    pc  = int(nn[1]) #patch_col
    print('\n\n',sn,patch_size,pr,pc,'\n\n')
    gopos(x)
    sleep(1.0)
    
    newdataset(sn)
    fshtrigger()
    return patch_size,pr,pc

def strip1lr1_scan2():
    try:
        so()
        path_size,pr,pc = tgopos('WTCS1_mac1_strip1lr1_lr_2x4_0031.json')
        sleep(5)
        umvr(ustry,1)
        sleep(5)
        umvr(ustry,1)
        mgeig()
        dkpatchmesh(1,200,1,200,0.02,2,2,retveloc=0.5)
        enddataset()
                
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()


def run_patch_scan(json_file):
    try:
        #pc is column and pr is row of scan, which correlate step at y and z in path scan
        so()
        patch_size,pr,pc = tgopos(json_file)
        mgeig()
        dkpatchmesh(patch_size,200,patch_size,200,0.02,pc,pr,retveloc=0.5)
        enddataset()
        
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()

def feb_25_afternoon():
    l = [
'WTCS1_mac1_strip1hr1_hr_1x1_0032.json',
'WTCS1_mac1_strip1hr2_hr_1x1_0043.json',
'WTCS1_mac1_strip1lr1_lr_2x4_0031.json',
'WTCS1_mac1_strip2hr1_hr_1x1_0040.json',
'WTCS1_mac1_strip2hr2_hr_1x1_0041.json',
'WTCS1_mac1_strip2hr3_hr_1x1_0042.json',
'WTCS1_mac1_strip2lr1_lr_4x2_0039.json',
'WTCS1_mac1_strip3hr1_hr_2x2_0034.json',
'WTCS1_mac1_strip3hr3_hr_1x1_0037.json',
'WTCS1_mac1_strip3lr1_lr_1x1_0033.json',
'WTCS1_mac1_strip3lr2_lr_1x2_0038.json',

 ]
    for i in l:
        run_patch_scan(i)


def feb_25_night():
    l = [
#'WTCS1_mac1_strip1hr1_hr_1x1_0032.json',
#'WTCS1_mac1_strip1hr2_hr_1x1_0043.json',
#'WTCS1_mac1_strip1lr1_lr_2x4_0031.json',
#'WTCS1_mac1_strip2hr1_hr_1x1_0040.json',
#'WTCS1_mac1_strip2hr2_hr_1x1_0041.json',
#'WTCS1_mac1_strip2hr3_hr_1x1_0042.json',
'WTCS1_mac1_strip2lr1_lr_4x2_0039.json',
'WTCS1_mac1_strip3hr1_hr_2x2_0034.json',
'WTCS1_mac1_strip3hr3_hr_1x1_0037.json',
'WTCS1_mac1_strip3lr1_lr_1x1_0033.json',
'WTCS1_mac1_strip3lr2_lr_1x2_0038.json',

 ]
    for i in l:
        run_patch_scan(i)
#############################
#
#wrong scan coordinate
#b_mac1_strip1hr1 and b_mac1_strip1hr2 is not affected by the transpose
#b_mac1_strip1lr1 scan 1-4 is ok, scan 5 is aborted due to wrong position
#new scan started with prefix c
#c_ma1_strip1lr1 include rest for strip1lr1
#scan info: loacl sanno, (yidx, zidx), (y_oripos, z_oripos)
#==========================================================
#0 (0, 0) (-9.4805, -4.313507)
#1 (1, 0) (-8.4755, -4.313507)
#2 (0, 1) (-9.4805, -3.3085070000000005)
#3 (1, 1) (-8.4755, -3.3085070000000005)
#4 (0, 2) (-9.4805, -2.3035070000000006)
#5 (1, 2) (-8.4755, -2.3035070000000006)
#6 (0, 3) (-9.4805, -1.2985070000000007)
#7 (1, 3) (-8.4755, -1.2985070000000007)
# 
