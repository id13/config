def set_perov_xmap2_rois():
    xmap2.rois.set('ILa', 750, 817)
    xmap2.rois.set('ILb', 817, 883)
    xmap2.rois.set('ILab', 750, 883)
    xmap2.rois.set('BrKa', 2340, 2410)
