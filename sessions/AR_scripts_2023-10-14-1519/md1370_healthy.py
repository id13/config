import numpy as np
import time

def safety_net(tot_sleep):
    for i in range(1,tot_sleep):
        timer = tot_sleep - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)
    
def get_last_scandata():
    sc = SCANS[-1]  
    data = sc.get_data('xmap3:Ca_det0')
    return data
    
def get_Ca_cen(arr, start_pos, ll, stp=1.0):
    p0 = start_pos + ll
    mini = np.min(arr)
    maxi = np.max(arr)
    hr = 0.5*(maxi-mini)
    brr = arr - hr
    crr = np.where(brr > 0, 1, 0)
    idx = np.arange(len(crr))
    cpr = np.compress(crr, idx)
    print('idx =', cpr[0])
    pos = p0 + 0.5*(cpr[0]+cpr[0]-1)*stp
    return pos
    

def align_proj():
    
    fshtrigger()
    umvr(nnp3, 7.5)
    
    print('before align nnp2:',nnp2.position)
    eiger.saving.frames_per_file=81
    dscan(nnp2, -20, 20, 80, 0.02)
    qgoto_cen('Ca_det0', nnp2)
    print('after:',nnp2.position)
    where()

    print('before align:',nnp3.position)
    initial_nnp3 = nnp3.position
    eiger.saving.frames_per_file=161
    dscan(nnp3, -27.5, 12.5, 160, 0.02)
    
    
    arr = get_last_scandata()
    edge_nnp3_pos = get_Ca_cen(arr, initial_nnp3, -27.5, stp=0.25)
    mv(nnp3, edge_nnp3_pos)
    print('after:',nnp3.position)
    where()

def short_scan():
    with bench():
        kmap.dkmap(nnp2,-6.02,6.02,86,nnp3,-1.52,15,118,0.01,frames_per_file = 118*4)
        
 
def long_scan_1():
    with bench():
        kmap.dkmap(nnp2,-6.02,6.02,172,nnp3,-1.52,15,236,0.01,frames_per_file = 236*2)
        
        
def long_scan_2():
    with bench():
        kmap.dkmap(nnp2,-6.02,6.02,172,nnp3,-1.52,13.32,212,0.01,frames_per_file = 212*2)        
    
def do_proj(zkap, ome, iscan):
    zkap.goto_okpos(ome)
    print('=============')
    print(f'aligning the pillar for {ome}')
    print('=============')
    _t0 = time.time()
    align_proj()
    print(f'alignment took: {time.time()-_t0} sec')
    
    print('=============')
    print(f'measuring the pillar for {ome}')
    print('=============')
    _t0 = time.time()
    if iscan<129:
        long_scan_1()
    else:
        long_scan_2()
    print(f'map took: {time.time()-_t0} sec')
    
#OME_LIST = [135, 150, 180]

N = 240
angles_deg = np.linspace(0, 180, N+1)
subset_1   = list(angles_deg[0::4])
subset_2   = list(angles_deg[2::4])
subset_3   = list(angles_deg[1::4])
subset_4   = list(angles_deg[3::4])
scans = subset_1 + subset_2 + subset_3 + subset_4

# First test with 3 finer maps at these angles for healthy_A
#scans = [180.0,90.0,0.0]
#start: 17:35

def md1370_main(zkap, start_idx):
    try:
        so()
        start_key = 'e'
        eigerhws(True)
        mgeig_x()
        for i, ome in enumerate(scans):
            print('====================')
            print(f'now going to {ome}') 
            print('====================')
            if i < start_idx:
                continue
            if i not in [158]:
                continue
                
                        
            newdataset(f'{start_key}_{i:03d}_{int(100*ome):06d}')
            do_proj(zkap, ome, i)
            safety_net(5)
    finally:
        sc()
        sc()
        sc()
