print('ls2950 ttomo - load 5')
import sys
import time
from pprint import pprint
from functools import partial
from collections import OrderedDict as Odict
import numpy as np

class Bailout(Exception): pass


def make_pjparams():
    tilt_step = 40.0/6.0
    ntilts = 1
    tilts = Odict()
    for i in range(ntilts):
        smxa = -float(int((i*tilt_step)*1000))
        tk   = f'tilt{i:02}'
        tilts[tk] = smxa
    tkll = list(tilts.keys())
    tiltorder = tkll[:]
    _philists = Odict()
    comp1 = list(np.linspace(208.0,388.0,76))
    comp2 = list(np.linspace(388.0-1.2,208+1.2,75))
    the_list = comp1 + comp2
    the_list = the_list[1:]
    _philists[tkll[0]] = the_list
    # _philists[tkll[1]] = range(0,180,4)
    # _philists[tkll[2]] = range(0,180,6)
    # _philists[tkll[3]] = range(0,180,10)
    # _philists[tkll[4]] = range(0,180,12)
    # _philists[tkll[5]] = range(0,180,18)
    # _philists[tkll[6]] = range(0,180,20)
    philists = Odict()
    for k,v in _philists.items():
        philists[k] = list(map(float, _philists[k]))
        
    pjparams = Odict(
        tilts = tilts,
        tiltorder = tiltorder,
        philists = philists
    )

    return pjparams

def dummy(*p, **kw):
    print(f'obj: {p[0]}  attr: {p[1]}  args: {p[2:]}  kw: {kw}')

class Dummy(object):

    def __init__(self, *p, **kw):
        self._phi = 0

    def _get_phi(self):
        return self._phi

    def _mv_phi(self, phi):
        self._phi = phi

    def _mvr_phi(self, dphi):
        self._phi = self._phi + dphi

    def __getattr__(self, k):
        if 'mvr_phi' == k:
            return self._mvr_phi
        elif 'mv_phi' == k:
            return self._mv_phi
        if k == 'get_phi':
            return self._get_phi
        else:
            return partial(dummy, self.__class__.__name__, k)

class DummyKmap(Dummy): pass
class DummyBCH(Dummy): pass
class Nnp5(Dummy): pass
class Nnp6(Dummy): pass

SIMULATION = False
if SIMULATION:
    kmap = DummyKmap()
    bch = DummyBCH()
    nnp5 = Nnp5()
    nnp6 = Nnp6()

    
class TTomo(object):

    def __init__(self, bch, pjparams, asyfn='ttomo.asyn', logfn='ttomo.log'):
        self.bch = bch
        self.pjparams = pjparams
        self.asyfn = asyfn
        self.logfn = logfn
        self._id = 0
        self._ictr = 0
        self.meshshape = None
        self.stepsize  = None
        self.exptime   = None
        self.fastaxis  = None
        self.natural_phistep = 5.0

    def set_scanparams(self, meshshape=(80,180), stepsize=0.5,  exptime=0.01, fastaxis='y'):
        self.meshshape = meshshape
        self.stepsize  = stepsize
        self.exptime   = exptime
        self.fastaxis  = fastaxis

    def run(self, start_tiltkey, start_phiidx):
        phll = self.pjparams['philists']
        start_found = False
        for k in phll:
            if not start_found:
                if k != start_tiltkey:
                    continue
                else:
                    start_found = True
                    phiidx_ll = range(len(phll[k]))
                    self.tilt_goto(k)
                    for phiidx in phiidx_ll[start_phiidx:]:
                        self.doo_projection(phiidx)
            else:
                phiidx_ll = range(len(phll[k]))
                self.tilt_goto(k)
                for phiidx in phiidx_ll:
                    self.doo_projection(phiidx)


    def log(self, s):
        with open(self.logfn, 'a') as f:
            f.write(f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n')
            f.write(s)

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                print('new instruction set found - processing ...')
            else:
                print('only old instruction set found - continuing ...')
                return
        else:
            print('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()
                
            elif 'tweak' == a:
                self.bch.show()
                (mod, k, twv) = v.split()
                kw = {f'gen_{k}': float(twv)}
                if 'rel' == mod:
                    self.bch.set_rel_gen_diffpos(**kw)
                elif 'abs' == mod:
                    self.bch.set_abs_gen_diffpos(**kw)
                else:
                    print(f'WARNING: illegal arg {mod} for instruction {a}')
                self.bch.show()
            else:
                print(f'WARNING: instruction {a} ignored')


    def tilt_goto(self, tiltkey):
        self.tiltkey = 'expired'
        self.bch.tilt_goto(tiltkey)
        self.bch.tilt_goto(tiltkey)
        self.bch.apply_corr(tiltkey=tiltkey, auto=True)
        self.tiltkey = tiltkey

    def phi_goto(self, phi):
        curr_phi = bch.get_phi()
        print (phi, curr_phi)
        phirang = phi-curr_phi
        nsteps = int((phi-curr_phi)/self.natural_phistep)
        for i in range(nsteps):
            tmp_phi = curr_phi + i*self.natural_phistep
            self.raw_phi_goto(tmp_phi)
        self.raw_phi_goto(phi)

    def raw_phi_goto(self, phi):
        print(f'ttomo.raw_phi_goto: phi={phi}')
        self.bch.mv_phi(phi)
        self.bch.apply_corr(tiltkey=self.tiltkey, auto=True)

    def doo_projection(self, phiidx):
        fastaxis = self.fastaxis
        meshshape = self.meshshape
        stepsize = self.stepsize
        exptime = self.exptime

        print(f'instruct: {self._ictr}==============================================================')
        self._ictr += 1
        try:
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
        except:
            raise
            print('WARNING: no instructions found')

        phival = self.pjparams['philists'][self.tiltkey][phiidx]
        print (f'taking projection ...')
        print (f'tiltkey: {self.tiltkey}  phiidx: {phiidx}  phi: phival')
        self.phi_goto(phival)

        fast_np = meshshape[1]
        slow_np = meshshape[0]
        if 'y' == fastaxis:
            fastmot = nnp5
            slowmot = nnp6
        elif 'z' == fastaxis:
            fastmot = nnp6
            slowmot = nnp5
        else:
            raise ValueError('illegal fastaxis {fastaxis}')
        fast_ll = -int(0.5*fast_np)*stepsize
        fast_ul = (fast_np-int(0.5*fast_np))*stepsize
        slow_ll = -int(0.5*slow_np)*stepsize
        slow_ul = (slow_np-int(0.5*slow_np))*stepsize
        print(f'''using params:
    fastmot fast_ll fast_ul fast_np: {fastmot.name} {fast_ll:8.3} {fast_ul:8.3} {fast_np}
    slowmot slow_ll slow_ul slow_np: {slowmot.name} {slow_ll:8.3} {slow_ul:8.3} {slow_np}
    exptime: {exptime:10.3}
''')
        time.sleep(2)
        kmap.dkmap(
            fastmot, fast_ll, fast_ul, fast_np,
            slowmot, slow_ll, slow_ul, slow_np,
            exptime
        )

def _test():
    pjparams = make_pjparams()
    pprint(pjparams)
    tt = TTomo(bch, pjparams)
    bch.mvr_phi(0.1)
    tt.tilt_goto('tilt01')
    tt.phi_goto(54.8)
    tt.set_scanparams()
    tt.run('tilt00', 0)

def ttomo_main():
    pjparams = make_pjparams()
    pprint(pjparams)
    ans = yesno('continue?')
    if not ans:
        print('bailing out ...')
        return
    tt = TTomo(bch, pjparams)
    tt.tilt_goto('tilt00')
    tt.set_scanparams()
    tt.run('tilt00', 0)


if __name__ == '__main__':
    _test()
