print('load 1')


def safety_net():
    for i in range(1,5):
        timer = 5 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
        print('TOO LATE - wait for next kmap')
        print('='*40)

def second_afternoon():
    so()
    mgeig_x()
    MG_EH3a.enable('*eiger:roi*')
            
    rstp("pos1")
    newdataset("pos1_bis")
    kmap.dkmap(nnp2, -75,75, 1500, nnp3, -10, 10, 33, 0.05)
    safety_net()
    
                
    rstp("pos2")
    newdataset("pos2")
    kmap.dkmap(nnp2, -95,90, 1850, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos3")
    newdataset("pos3")
    kmap.dkmap(nnp2, -85,75, 1600, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos4")
    newdataset("pos4")
    kmap.dkmap(nnp2, -30,100, 1300, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos5")
    newdataset("pos5")
    kmap.dkmap(nnp2, -95,10, 1050, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos6")
    newdataset("pos6")
    kmap.dkmap(nnp2, -70,70, 1400, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos7")
    newdataset("pos7")
    kmap.dkmap(nnp2, -70,65, 1350, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos8")
    newdataset("pos8")
    kmap.dkmap(nnp2, -70,70, 1400, nnp3, -10, 10, 33, 0.05)
    safety_net()
                
    rstp("pos9")
    newdataset("pos9")
    kmap.dkmap(nnp2, -80,90, 1700, nnp3, -10, 10, 33, 0.05)
    safety_net()
    
    sc()
    sc()
    
def first_night():
    so()
    mgeig_x()
    MG_EH3a.enable('*eiger:roi*')
            
    rstp("pos2")
    newdataset("pos2")
    kmap.dkmap(nnp2, -20,20, 400, nnp3, -60, 60, 300, 0.05)
    safety_net()
    
    rstp("pos3")
    newdataset("pos3")
    kmap.dkmap(nnp2, -60,60, 1200, nnp3, -7.5, 7.5, 25, 0.05)
    safety_net()
    
    rstp("pos4")
    newdataset("pos4")
    kmap.dkmap(nnp2, -80,80, 1600, nnp3, -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos5")
    newdataset("pos5")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos6")
    newdataset("pos6")
    kmap.dkmap(nnp2, -50,50, 1000, nnp3, -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos7")
    newdataset("pos7")
    kmap.dkmap(nnp2, -50,50, 1000, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos8")
    newdataset("pos8")
    kmap.dkmap(nnp2, -80,80, 1600, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos9")
    newdataset("pos9")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos10")
    newdataset("pos10")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos11")
    newdataset("pos11")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos12")
    newdataset("pos12")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos13")
    newdataset("pos13")
    kmap.dkmap(nnp2, -120,120, 2400, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos14")
    newdataset("pos14")
    kmap.dkmap(nnp2, -120,120, 2400, nnp3, -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos15")
    newdataset("pos15")
    kmap.dkmap(nnp2, -120,120, 2400, nnp3,  -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos16")
    newdataset("pos16")
    kmap.dkmap(nnp2, -120,120, 2400, nnp3, -7.5, 7.5,  25, 0.05)
    safety_net()
    
    rstp("pos17")
    newdataset("pos17")
    kmap.dkmap(nnp2, -120,120, 2400, nnp3, -7.5, 7.5,  25, 0.05)
    safety_net()
    
    
    sc()
    sc()
    sc()  


def second_night():
    so()
    mgeig_x()
    MG_EH3a.enable('*eiger:roi*')
             
    rstp("nebo_pos2")
    newdataset("nebo_pos2")
    kmap.dkmap(nnp2, -10,10, 200, nnp3, -50, 80, 1300, 0.05)
    safety_net()
 
              
    rstp("nebo_pos4")
    newdataset("nebo_pos4")
    kmap.dkmap(nnp2, -70,70, 1400, nnp3, -12, 12, 40, 0.05)
    safety_net()
    
                  
    rstp("nebo_pos5")
    newdataset("nebo_pos5")
    kmap.dkmap(nnp2, -110,120, 2300, nnp3, -12, 12, 40, 0.05)
    safety_net()
                  
    rstp("nebo_pos7")
    newdataset("nebo_pos7")
    kmap.dkmap(nnp2, -100,85, 1850, nnp3, -12, 12, 40, 0.05)
    safety_net()
    
    
    sc()
    sc()
    
    
    
    
def third_afternoon():
    so()
    mgeig_x()
    MG_EH3a.enable('*eiger:roi*')
             
    rstp("sample_5_pos2")
    newdataset("sample_5_pos2")
    kmap.dkmap(nnp2, -90,90, 1800, nnp3, -20, 20, 66, 0.05)
    safety_net()
 
              
    rstp("shaft_nebo")
    newdataset("shaft_nebo")
    kmap.dkmap(nnp2, -100,100, 266, nnp3, -50, 50, 133, 0.03)
    safety_net()
    
                  
    rstp("tip_nebo")
    newdataset("tip_nebo")
    kmap.dkmap(nnp2, -60,90, 200, nnp3, -120, 120,320, 0.03)
    safety_net()
    
    rstp("nebo_tip_pos2")
    newdataset("tip_nebo_pos2")
    kmap.dkmap(nnp2, -80,80, 213, nnp3, 0, 110, 146, 0.03)
    safety_net()
                  
    rstp("platnicky_tip")
    newdataset("tip_platnicky")
    kmap.dkmap(nnp2, -70,100, 226, nnp3, -35, 70, 140, 0.03)
    safety_net()
    
    
    sc()
    sc()



    
def third_night():
    so()
    mgeig_x()
    MG_EH3a.enable('*eiger:roi*')
             
    rstp("sample_6_pos1")
    newdataset("sample_6_pos1")
    kmap.dkmap(nnp2, -50,50, 1000, nnp3, -10, 10, 33, 0.05)
    safety_net()
                 
    rstp("sample_6_pos2")
    newdataset("sample_6_pos2")
    kmap.dkmap(nnp2, -65,65, 1300, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
              
    rstp("sample_6_pos3")
    newdataset("sample_6_pos3")
    kmap.dkmap(nnp2, -70,70, 1400, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
              
    rstp("sample_6_pos4")
    newdataset("sample_6_pos4")
    kmap.dkmap(nnp2, -80,80, 1600, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
              
    rstp("sample_6_pos5")
    newdataset("sample_6_pos5")
    kmap.dkmap(nnp2, -100,80, 1800, nnp3, -10, 10, 33, 0.05)
    safety_net()

             
    rstp("sample_8_pos1")
    newdataset("sample_8_pos1")
    kmap.dkmap(nnp2, -50,40, 900, nnp3, -35, 20, 95, 0.05)
    safety_net()
     
               
    rstp("sample_8_pos2")
    newdataset("sample_8_pos2")
    kmap.dkmap(nnp2, -60,60, 1200, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
  
               
    rstp("sample_8_pos3")
    newdataset("sample_8_pos3")
    kmap.dkmap(nnp2, -60,60, 1200, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
  
               
    rstp("sample_8_pos5")
    newdataset("sample_8_pos5")
    kmap.dkmap(nnp2, -60,60, 1200, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
  
               
    rstp("sample_8_pos6")
    newdataset("sample_8_pos6")
    kmap.dkmap(nnp2, -70,70, 1400, nnp3, -10, 10, 33, 0.05)
    safety_net()
 
               
    rstp("sample_8_pos9")
    newdataset("sample_8_pos9")
    kmap.dkmap(nnp2, -50,50, 1000, nnp3, -10, 10, 33, 0.05)
    safety_net()

              
    rstp("sample_6_pos1")
    newdataset("sample_6_pos1_longexp")
    kmap.dkmap(nnp2, -50,50, 1000, nnp3, -30, 120, 30, 0.1)
    safety_net()
 
            
    
    sc()
    sc()
