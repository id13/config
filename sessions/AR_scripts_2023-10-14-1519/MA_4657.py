print('ma4657 loaded v3')

def bsalign():
    print("where are we:")
    wm(nbsy,nbsz)
    
    dscan(nbsy,-0.5,0.5,50,0.02,ct32)
    goc(nbsy)
    where() 
    sleep(0.5)   
    dscan(nbsz,-0.5,0.5,50,0.02,ct32)
    goc(nbsz)
    where()
    
    sleep(0.5) 
       
    dscan(nbsy,-0.3,0.3,60,0.02,ct32)
    goc(nbsy)
    where()
    sleep(0.5)
    dscan(nbsz,-0.3,0.3,60,0.02,ct32)
    goc(nbsz)
    where()
    
    print("where are we now:")
    wm(nbsy,nbsz)
    
def ma4657_night():
    #print('newdataset(T1)')
    #newdataset('T1')
    
    #gopos('mt04_T1_0007.json')
    #umv(Theta,0)
    #fshtrigger()
    #kmap.akmap(nnp2,130,90,400,nnp3,85,125,40,0.05,frames_per_file=400)
    #fshtrigger()
    #kmap.akmap(nnp2,130,90,400,nnp3,125,165,40,0.05,frames_per_file=400)
    
    
    #print('newdataset(T2)')
    #newdataset('T2')
    
    gopos('mt04_T2_0006.json')
    umv(Theta,-1)
    fshtrigger()
    kmap.akmap(nnp2,130,90,400,nnp3,85,125,40,0.05,frames_per_file=400)
    fshtrigger()
    kmap.akmap(nnp2,130,90,400,nnp3,125,165,40,0.05,frames_per_file=400)
    
    newdataset('0002')
  
def ma4657_MOFSET():
    #print('newdataset(T1)')
    
    #newdataset('mosfet')
    #fshtrigger()
    #kmap.akmap(nnp2,130,80,250,nnp3,80,100,100,0.05,frames_per_file=500)
    #fshtrigger()
    #kmap.akmap(nnp2,130,80,250,nnp3,100,120,100,0.05,frames_per_file=500)
    #fshtrigger()
    #kmap.akmap(nnp2,130,80,250,nnp3,120,140,100,0.05,frames_per_file=500)   
    #fshtrigger()
    #kmap.akmap(nnp2,130,80,250,nnp3,140,160,100,0.05,frames_per_file=500)
    ############ aborted half way through the third kmap scan, due to the fast shutter mal-functioning #############
    ############ the first two kmaps are finished corrected ###############
    
    #newdataset('mosfet_02')
    #fshtrigger()
    #kmap.akmap(nnp2,130,80,500,nnp3,120,130,100,0.2,frames_per_file=500)
    ##### aborted after beam loss, but first 2.5um along nnp3 is OK, i.e. the first 25 eiger files are fine ######
    
    #newdataset('mosfet_03')
    #fshtrigger()
    #kmap.akmap(nnp2,125,85,400,nnp3,122.5,132.5,100,0.2,frames_per_file=400)
    #
    ############ the last line of kmaps is missing###############
    
    newdataset('mosfet_04')
    fshtrigger()
    kmap.akmap(nnp2,115,95,200,nnp3,132.5,142.5,100,0.2,frames_per_file=400)
    
    fshtrigger()
    kmap.akmap(nnp2,115,95,200,nnp3,142.5,152.5,100,0.2,frames_per_file=400)
    
    fshtrigger()
    kmap.akmap(nnp2,115,95,200,nnp3,152.5,162.5,100,0.2,frames_per_file=400)
    
    fshtrigger()
    kmap.akmap(nnp2,115,95,200,nnp3,162.5,172.5,100,0.2,frames_per_file=400)
    
    newdataset('0003')

def macro_run():
    try:
        so()
        ma4657_MOFSET()
        sc()
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()
        sc()
