# START
from prompt_toolkit import PromptSession
from prompt_toolkit.key_binding import KeyBindings
from bliss.common.greenlet_utils import asyncio_gevent

def my_prompt_app():
    bindings = KeyBindings()

    @bindings.add("h")
    def _(event):
        event.app.current_buffer.insert_text("Hello, world !")

    @bindings.add("e")
    def _(event):
        event.app.exit()

    session = PromptSession()

    return asyncio_gevent.yield_future(session.prompt_async("> ", key_bindings=bindings))

# END
