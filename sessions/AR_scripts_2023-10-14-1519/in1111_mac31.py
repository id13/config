print ("mac31")
def the_scan():
    #dmesh(ustrz,-0.05,0.05,5,ustry,-0.05,0.05,5,0.01)
    dscan(ustrz,-0.1,0.1,100,0.02)

def in1111_mac31():
    for i in range(1,19):
        pos_name = "m1_frame1_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_scan()
        
    for i in range(20,38):
        pos_name = "m1_frame3_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_scan()

print ("mac31 end")
