# beam size should be less 100 nm, this is 30 mA 16 bunch mode experiments
print('ihsc1749 load 7')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    zeronnp()
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,defocus=False):
    so()
    fshtrigger()
    mgeig()
    name = f'{pos[:-5]}_{start_key}'
    umv(nnz,0,nny,0)
    gopos(pos)
    if defocus:
        umvr(nnx,1)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

RCF_fiber = [
'RCF_HT_50x_fiber3_0012.json',
'RCF_Ion_50x_fiber1_0006.json',
'RCF_Vis_50x_fiber1_0002.json',
]

starch_sample1 = [
'sample1_A2_50x_0017.json',
'sample1_A2_50x_2_0018.json',
'sample1_B_50x_1_0023.json',
'sample1_B_50x_2_0022.json',
'sample1_C_50x_1_0027.json',
'sample1_C_50x_2_0026.json',
]

starch_sample4 = [
'sample4_A_50x_1_0006.json',
'sample4_A_50x_2_0005.json',
'sample4_A_50x_3_0004.json',
'sample4_B_50x_1_0012.json',
'sample4_B_50x_2_0011.json',
'sample4_B_50x_3_0010.json',
'sample4_C_50x_1_0018.json',
'sample4_C_50x_2_0017.json',
'sample4_C_50x_3_0016.json',

]


sample4_new = [
'sample4_250nm_A_50x_pos1_0043.json',
'sample4_250nm_A_50x_pos2_0042.json',
'sample4_250nm_A_50x_pos3_0041.json',
'sample4_250nm_B_50x_pos1_0048.json',
'sample4_250nm_B_50x_pos2_0047.json',
'sample4_250nm_C_50x_pos1_0054.json',
'sample4_250nm_C_50x_pos2_0053.json',
'sample4_250nm_C_50x_pos3_0052.json',
]

sample1_new = [
'sample1_250nm_A_50x_pos1_0061.json',
'sample1_250nm_A_50x_pos2_0060.json',
'sample1_250nm_A_50x_pos3_0059.json',
'sample1_250nm_B_50x_pos1_0067.json',
'sample1_250nm_B_50x_pos2_0066.json',
'sample1_250nm_B_50x_pos3_0065.json',
'sample1_250nm_C_50x_pos1_0073.json',
'sample1_250nm_C_50x_pos2_0072.json',
'sample1_250nm_C_50x_pos3_0071.json',

]

def Feb_6th_night_run():
    try:
        umv(ndetx,-300)
        start_key = 'c'
        for _ in sample1_new:
            gopos_kmap_scan(_,start_key,
                            25,25,0.25,0.25,0.04,defocus=True)
        
        sc()
    finally:
        sc()
        sc()
        enddataset()

def Feb_6th_day_run():
    try:
        umv(ndetx,-300)
        start_key = 'b'
        for _ in sample4_new:
            gopos_kmap_scan(_,start_key,
                            25,25,0.25,0.25,0.04,defocus=True)
        
        sc()
    finally:
        sc()
        sc()
        enddataset()


def Feb_4th_night_run2():
    try:
        umv(ndetx,-300)
        start_key = 'a'
        for _ in starch_sample4:
            gopos_kmap_scan(_,start_key,
                            15,15,0.1,0.1,0.03)
        
        sc()
    finally:
        sc()
        sc()
        enddataset()

def Feb_4th_afternoon_run1():
    try:
        umv(ndetx,-300)
        start_key = 'a'
        for _ in RCF_fiber:
            gopos_kmap_scan(_,start_key,
                            17,17,0.1,0.1,0.04)
        
        sc()
    finally:
        sc()
        sc()
        enddataset()


def march9_daytime_run2():
    try:
        so()
        mgeig()
        fshtrigger()
        #umv(ndetx,0)
        start_key = 'd'
        for _ in bend_film_pos:
            #gopos_kmap_scan(_,start_key,
            #                60,50,0.25,0.5,0.05)
            # new scan for bend film only use width of 50 microns instead 120
            gopos_kmap_scan(_,start_key,
                            25,50,0.25,0.5,0.05)
        
        #umv(ndetx,-360)
        #for _ in clay_waxs_pos:
        #    gopos_kmap_scan(_,start_key,
        #                    100,5,0.25,0.25,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()

def march9_daytime_run():
    try:
        so()
        mgeig()
        fshtrigger()
        #umv(ndetx,0)
        start_key = 'a'
        for _ in straight_film_pos:
            gopos_kmap_scan(_,start_key,
                            60,50,0.25,0.5,0.05)
        
        #umv(ndetx,-360)
        #for _ in clay_waxs_pos:
        #    gopos_kmap_scan(_,start_key,
        #                    100,5,0.25,0.25,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()
