from collections import OrderedDict
load_script('sc5408_tview_align')
tv_motdc = make_motdc()
zkap = Zkap(tv_motdc, arm=True)

load_script('sc5408_infra')
load_script('sc5408_ttomo_b_411R_inf')

pos1: x0
pos2: x0 180
pos3: x0 180 z corr   
pos4: x1
pos5: x2
pos6: x2 180 final 
pos7: y0 270
pos8: y0 270 NEW
pos9: y0 90
pos10: y0 90 NEW
pos11: 180 check
pos12: 180 check corr
pos13: 0 check
pos14: 0 check corr
pos15: z position
pos16: omega 90 kappa 0
pos17: omega 90 kappa -8
pos18: omega 90 kappa -16
pos19: omega 90 kappa -24
pos20: omega 90 kappa -32
pos21: omega 90 kappa -40


# zkap: ########
zkap.c_amp = 0.7602004999999998
zkap.phi_offset = 783.19937
zkap.c_cenx_k0  = -6.2885975
zkap.c_ceny_k0  = -0.1284999999999954
zkap.c_cenz_k0  = -0.11880500000000005
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.7618502500000002
zkap.phi_offset = 783.19937
zkap.c_cenx_k0  = -6.2885975
zkap.c_ceny_k0  = -0.11949999999999861
zkap.c_cenz_k0  = -0.11880500000000005
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.7681002499999998
zkap.phi_offset = 783.19937
zkap.c_cenx_k0  = -6.2885975
zkap.c_ceny_k0  = -0.11949999999999861
zkap.c_cenz_k0  = -0.11880500000000005
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.7759999999999998
zkap.phi_offset = 783.19937
zkap.c_cenx_k0  = -6.2885975
zkap.c_ceny_k0  = -0.11949999999999861
zkap.c_cenz_k0  = -0.11880500000000005
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.7759999999999998
zkap.phi_offset = 783.19937
zkap.c_cenx_k0  = -6.2885975
zkap.c_ceny_k0  = -0.12449999999999861
zkap.c_cenz_k0  = -0.03880299999999992
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0)), (-8, (-8.000186, -0.6707920000000005, -0.015503000000000045)), (-16, (-16.000017, -1.3376010000000003, -0.1295010000000001)), (-24, (-24.000054000000002, -1.979889, -0.331909)), (-32, (-32.000019, -2.5925910000000005, -0.622909)), (-40, (-39.999995000000006, -3.1526949999999996, -0.9895020000000001))])
