print('version ev480_pillar1_ttomo_003 load 2')
import numpy as np, math
import time
import traceback

class Bailout(Exception): pass

ORI_INSTRUCT = """
# kappa,omega
0, 36.43504531722054
0, 34.259818731117825
0, 32.08459214501511
0, 29.909365558912388
0, 27.734138972809667
0, 25.55891238670695
0, 23.38368580060423
0, 21.208459214501513
0, 19.033232628398792
0, 16.858006042296072
0, 14.682779456193353
0, 12.507552870090635
0, 10.332326283987916
0, 8.157099697885197
0, 5.981873111782478
0, 3.8066465256797586
0, 1.6314199395770395
0, 0
"""

TEST_ORI_INSTRUCT = """
# kappa,omega
0, 0.0
0, 3.444976076555024
0, 6.889952153110048
0, 10.334928229665072
0, 13.779904306220097
0, 17.22488038277512
0, 20.669856459330145
0, 24.11483253588517
0, 27.559808612440193
"""

def generate_instruct_string(start_ome, stop_ome, nproj, nstrands):
    eps = 0.0001
    omegall = list(np.linspace(start_ome,stop_ome,nproj))
    #print(omegall)
    llll = []
    for i in range(nstrands):
        sl = omegall[i::nstrands]
        if i % 2:
            sl.reverse()
        llll.append(sl)

    #print(f'llll = {llll}')
    for i in range(nstrands):
        if i % 2:
            if math.fabs(llll[i][-1] - start_ome) < eps:
                pass
            else:
                llll[i].append(start_ome)
        else:
            if math.fabs(llll[i][-1] - stop_ome) < eps:
                pass
            else:
                llll[i].append(stop_ome)
    full = []
    for ll in llll:
        full.extend(ll)
    full_indexed = []
    for tp in enumerate(full):
        i,ome = tp
        full_indexed.append(tp)
    sll = []
    sll.append(f'')
    sll.append(f'# kappa,omega')
    sent = -1
    for tp in full_indexed:
        i, ome = tp
        #print(sent, tp)
        if sent == ome:
            pass
        else:
            sll.append(f'0, {ome}')
        sent = tp[1]
    sll.append(f'')
    s = '\n'.join(sll)
    return s
        


def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (kap,ome) = l.split(',')
            ko = (int(kap), float(ome))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll


def dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=None):
    kmap.dkmap(nnp2,lly,uly,nitvy,nnp3,llz,ulz,nitvz,expt)

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo_eh3.log'):
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(kap,ome)) = instll_item
        self.log('... dummy ko trajectory')
        #self.zkap.trajectory_goto(ome, kap, stp=4)
        self.zkap.goto_okpos(ome)
        self.perform_scan()


    def perform_scan(self): 
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        lly *= 1.000
        uly *= 1.000
        llz *= 1.000
        ulz *= 1.000
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
        time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        indexed_instll = list(enumerate(instll))
        indexed_instll.reverse()
        while(True):
            full_item = indexed_instll.pop()
            cycle, instll_item = full_item
            print(f'\n\n\n\n\n\n\n ============================================ CYCLE {cycle}')
            print(f'KAPPA = {instll_item[0]}  OMEGA = {instll_item[1]}')
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        print('++++++++++++++++++++++')
        print(f'instruct {instruct}')
        print('++++++++++++++++++++++')
        a , v = instruct[0]
        print('++++++++++++++++++++++')
        print(f'processing instruction: {a} {v}')
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t_bds = (lly, uly, llz, ulz) = tuple(map(int,w[1:5]))
                        fov_t_stps = (stpy, stpz) = tuple(map(float,w[5:]))
                        fov_t = fov_t_bds + fov_t_stps
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_corr(c_corr_t)
                    elif 'nnpcor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_nnpcorr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_c_nnpcorr(self, c_corr_t):
        print('applying nnp correction! ======================================== NNP!')
        print('pos before:')
        wnnp()
        (c_corrx, c_corry, c_corrz) = c_corr_t
        x = nnp1.position + c_corrx
        y = nnp2.position + c_corry
        z = nnp3.position + c_corrz
        mv(nnp1, x)
        mv(nnp2, y)
        mv(nnp3, z)
        print('pos after:')
        wnnp()
        self.log(f'nnpcorrection: {c_corr_t}')

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz, stpy, stpz) = fov_t

        lly = stpy*int(lly/stpy)
        uly = stpy*int(uly/stpy)
        llz = stpz*int(llz/stpz)
        ulz = stpz*int(ulz/stpz)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy/stpy)
        nitvz = int(dz/stpz)

        print('d', dy, dz)
        print('st',stpy,stpz)
        print('ni',nitvy,nitvz)
        print('ll',lly, uly, llz, ulz)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def ev480_test_main(zkap):
    print('Hi!')

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    # 117
    # 170
    scanparams = dict(
        lly = -16,
        uly = 16,
        nitvy = 320,
        llz = -10,
        ulz = 10,
        nitvz = 200,
        expt = 0.002,
        retveloc = 0.0
    )
    ttm = TTomo( 'asy.com', zkap, instll, scanparams)



    # loop over projections
    ttm.mainloop()
