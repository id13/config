from collections import OrderedDict
load_script('sc5408_tview_align')
tv_motdc = make_motdc()
zkap = Zkap(tv_motdc, arm=True)

load_script('sc5408_infra')
load_script('sc5408_ttomo_b_411R_sup')
 
pos1: x0
pos2: x0 180
pos3: x0 180 z corr
pos4: x1
pos5: x2
pos6: x2 180 final  
pos7: y0 270
pos8: y0 90
pos9: check 180
pos10: check 180 corr
pos11: check 0 (aready perfect)
pos12: z center
pos13: z center
pos14: omega 90 kappa0
pos15: omega 90 kappa-8
pos16: omega 90 kappa-16
pos17: omega 90 kappa-24
pos18: omega 90 kappa-32
pos19: omega 90 kappa-40


# zkap: ########
zkap.c_amp = 0.48484799999999995
zkap.phi_offset = 1159.499203
zkap.c_cenx_k0  = -6.2878419999999995
zkap.c_ceny_k0  = -0.1355000000000004
zkap.c_cenz_k0  = -1.185806
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.4929240000000006
zkap.phi_offset = 1159.499203
zkap.c_cenx_k0  = -6.2878419999999995
zkap.c_ceny_k0  = -0.11949999999999861
zkap.c_cenz_k0  = -1.185806
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.5010000000000012
zkap.phi_offset = 1159.499203
zkap.c_cenx_k0  = -6.2878419999999995
zkap.c_ceny_k0  = -0.11949999999999861
zkap.c_cenz_k0  = -1.185806
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0))])

# zkap: ########
zkap.c_amp = 0.5010000000000012
zkap.phi_offset = 1159.499203
zkap.c_cenx_k0  = -6.2878419999999995
zkap.c_ceny_k0  = -0.1295
zkap.c_cenz_k0  = -1.105804
zkap.c_corrx    = 0.0
zkap.c_corry    = 0.0
zkap.c_corrz    = 0.0
zkap.kap_corr_table = OrderedDict([(0, (0.0, 0.0, 0.0)), (-8, (-8.000033, -0.5300900000000004, -0.009002999999999872)), (-16, (-15.999966, -1.051895, -0.09069799999999995)), (-24, (-23.999821, -1.5592040000000003, -0.2462009999999999)), (-32, (-31.999996, -2.037002000000001, -0.465897)), (-40, (-39.999939, -2.480194000000001, -0.7501989999999998))])

