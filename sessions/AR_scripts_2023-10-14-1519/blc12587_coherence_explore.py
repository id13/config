import time
import numpy as np
print('ps explore 11')

def blc12587_Wed_night():
    try:
        so()
        gap_vals = [0.15,0.20,0.25,0.30,0.35,0.40]
        
        for gv in gap_vals:
            print('measuring gap value: ', gv, '======================================================')
            sgv = str(gv).replace('.','p')
            newdataset('c_primary_slit_gv_%s' %sgv)
            
            # move primary slits gap
            umv(pvg,gv,phg,gv)
        
            # move sample infocus
            goinfocus()
            measure_infocus()

            # move sample outfocus by 1mm
            godefocus()
            measure_defocus()
        sc()
    finally:
        sc()
        sc()
        sc()

def goinfocus():
    # move sample infocus
    umv(nnx,2.759,nny,0.7201,nnz,0.9670)
    umv(nnp1,125,nnp2,125,nnp3,125)
    time.sleep(30)
    
def godefocus():
    # move sample outfocus by 1mm
    umv(nnx,3.759,nny,0.7142,nnz,0.9583)
    umv(nnp1,125,nnp2,125,nnp3,125)
    time.sleep(30)
    
def measure_infocus():
    # kmap scans
    fshtrigger()
    kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,0.02) # small steps
        
    #fshtrigger()
    #kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,0.02) # small steps repeat
        
    # spiral scans
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.05,4000,0.02) # small steps
        
    #fshtrigger()
    #spiral_dscan(nnp2,nnp3,0.05,4000,0.02) # small steps repeat    

def measure_defocus():
    # kmap scans
    fshtrigger()
    kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,0.02) # small steps
        
    #fshtrigger()
    #kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,0.02) # small steps repeat
        
    fshtrigger()
    kmap.dkmap(nnp3,-4,4,160,nnp2,4,-4,40,0.02) # big steps
        
    #fshtrigger()
    #kmap.dkmap(nnp3,-4,4,160,nnp2,4,-4,40,0.02) # big steps repeat        
        
    # spiral scans
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.05,4000,0.02) # small steps
        
    #fshtrigger()
    #spiral_dscan(nnp2,nnp3,0.05,4000,0.02) # small steps repeat        

    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.20,4000,0.02) # big steps
        
    #fshtrigger()
    #spiral_dscan(nnp2,nnp3,0.20,4000,0.02) # big steps repeat

def BS_calib():
    start_pos = ndety.position
    print('ndety position: %f' %start_pos)
    pos_step = 0.075
    mvr(ndety,-3.75)
    fshtrigger()
    try:
        so()
        for i in range(101):
            mvr(ndety,pos_step)
            time.sleep(1)
            loopscan(100,0.1)
        sc()
        umv(ndety,start_pos)
    finally:
        umv(ndety,start_pos)
        sc()
        sc()
        sc()
