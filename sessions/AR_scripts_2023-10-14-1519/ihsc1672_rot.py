print('ihsc1672 ver 5')
start_key = 'c'

def tomo_scan():
    gopos('rotation_bottom_center')
    rot_ang = [0,15,30,45,60,75,90,5,20,35,50,65,80,95,10,25,40,55,70,85,100]
    pos_list = [15,25,35,45,55,65]
    for i in range(len(pos_list)):
        if i == 0 :
            umvr(ustrz,-0.075*5/4)
        else:
            #sleep(120)
            umvr(ustrz,-0.075*3/2)
        for j in range(len(rot_ang)):
            fn_name = f'pos{pos_list[i]:03d}_azi_{rot_ang[j]:04d}_{start_key}'
            newdataset(fn_name)
            so()
            fshtrigger()
            #dqmgeigx()
            umv(usrotz,rot_ang[j])
            dkmapyz_2(-0.15,0.15,119,-0.0375/2,0.0375/2,14,0.002,retveloc=5)
            enddataset()
    gopos('rotation_top_center')
    rot_ang = [0,15,30,45,60,75,90,5,20,35,50,65,80,95,10,25,40,55,70,85,100]
    pos_list = [1,2,3,4,5,6,7,8,9,10]
    for i in range(len(pos_list)):
        if i == 0 :
            umvr(ustrz,0.075/2)
        else:
            #sleep(120)
            umvr(ustrz,0.075)
        for j in range(len(rot_ang)):
            fn_name = f'fluo_pos{pos_list[i]:03d}_azi_{rot_ang[j]:04d}_{start_key}'
            newdataset(fn_name)
            so()
            fshtrigger()
            #dqmgeigx()
            umv(usrotz,rot_ang[j])
            dkmapyz_2(-0.15,0.15,119,-0.0375,0.0375,29,0.02,retveloc=5)
            enddataset()
