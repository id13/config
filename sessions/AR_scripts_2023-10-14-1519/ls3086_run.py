print('ls3086 load 3.6')


start_key = 'd'


WAXS_list = [
'CIN_1968_bkgd',
'CIN_1968_pos1',
'CIN_1968_pos2',
'CIN_1968_pos3',
'CIN_1968_pos4',
'CIN_1968_pos5',
'CIN_1968_pos6',
'PHG_1968_bkgd',
'PHG_1968_pos1',
'PHG_1968_pos2',
'PHG_1968_pos3',
'PHG_1968_pos4',
#'PHG_1968_pos5',
'PHG_1968_pos6',
'PHG_1968_pos7',

#'V1_2068_bkgd',
#'V1_2068_pos1',
#'V1_2068_pos2',
#'V1_2068_pos3',
#'V1_2068_pos4',
#'V1_2068_pos5',
##'V2_2068_bkgd',
#'V2_2068_pos1',
#'V2_2068_pos2',
#'V2_2068_pos3',
#'V2_2068_pos4',
#'V2_2068_pos5',


#'EC_1968_bkgd',
#'EC_1968_pos1',
#'EC_1968_pos2',
#'EC_1968_pos3',
#'EC_1968_pos4',
#'EC_1968_pos5',
#'HC_1968_bkgd',
#'HC_1968_pos1',
#'HC_1968_pos2',
#'HC_1968_pos3',
#'HC_1968_pos4',
#'HC_1968_pos5',


#'V1_1968_bkgd',
#'V1_1968_pos1',
#'V1_1968_pos2',
#'V1_1968_pos3',
#'V1_1968_pos4',
#'V1_1968_pos5',
#'V1_1968_pos6',
#'V2_1968_bkgd2',
#'V2_1968_nkgd',
#'V2_1968_pos1',
#'V2_1968_pos2',
#'V2_1968_pos3',
#'V2_1968_pos4',
#'V2_1968_pos5',



#'EC_1971_pos1',
#'EC_1971_pos2',
#'EC_1971_pos3',
#'EC_1971_pos4',
#'EC_1971_pos5',
#'HC_1971_pos1',
#'HC_1971_pos2',
#'HC_1971_pos3',
#'HC_1971_pos4',
#'HC_1971_pos5',


#'PHG_2068_CING_pos1',
#'PHG_2068_CING_pos2',
#'PHG_2068_CING_pos3',
#'PHG_2068_CING_pos4',
#'PHG_2068_CING_pos5',
#'PHG_2068_PHG_pos1',
#'PHG_2068_PHG_pos2',
#'PHG_2068_PHG_pos3',
#'PHG_2068_PHG_pos4',
#'PHG_2068_PHG_pos5',
#'PHG_2068_PHG_pos6',


#'EC_2068_bkgd1',
#'EC_2068_bkgd2',
#'EC_2068_pos1',
#'EC_2068_pos2',
#'EC_2068_pos3',
#'EC_2068_pos4',
#'EC_2068_pos5',
#'HC_2068_bkgd',
#'HC_2068_bkgd2',
#'HC_2068_pos1',
#'HC_2068_pos2',
#'HC_2068_pos3',
#'HC_2068_pos4',
#'HC_2068_pos5',


#'EC_1971_pos1',
#'EC_1971_pos2',
#'EC_1971_pos3',
#'EC_1971_pos4',
#'EC_1971_pos5',

#'CING_1971_pos1',
#'CING_1971_pos2',
#'CING_1971_pos3',
#'CING_1971_pos4',
#'CING_1971_pos5',
#'V2_1971_pos1',
#'V2_1971_pos2',
#'V2_1971_pos3',
#'V2_1971_pos4',
#'V2_1971_pos5',


#'PHG_1971_pos1',
#'PHG_1971_pos2',
#'PHG_1971_pos3',
#'PHG_1971_pos4',
#'PHG_1971_pos5',
#'PHG_1971_bkgd',
#'V1_1971_pos1',
#'V1_1971_pos2',
#'V1_1971_pos3',
#'V1_1971_pos4',
#'V1_1971_pos5',
]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig_x()
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def run_Nov_13rd_night():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            if 'bkgd' in i:
                gopos_kmap_scan(i,start_key,0,75,75,3,3,0.02)
            else:
                gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
                #gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()

def run_Nov_13rd_evening():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            #gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()

def run_Nov_13rd_day():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            #gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()

def run_Nov_12nd_night():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            #gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()

        sc()
def run_Nov_12nd_evening():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            #gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()
        sc()

def run_Nov_12nd_day():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            #gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(10)

        sc()
    except:
        sc()
        sc()

def run_Nov_11st_night():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()
        sc()

def run_Nov_11st_evening():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            sleep(15)

        sc()
    except:
        sc()
        sc()

def run_Nov_11st_day():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            #gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            gopos_kmap_scan(i,start_key,0,68,68,0.5,0.5,0.02)
            sleep(10)

        sc()
    except:
        sc()
        sc()

def run_Nov_10th_night():
    try:
        umv(ndetx,-300)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,75,75,0.5,0.5,0.02)
            sleep(10)

        sc()
    except:
        sc()
        sc()






