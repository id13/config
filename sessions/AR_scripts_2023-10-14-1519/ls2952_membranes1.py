print('ls2952_membranes1 load 1')

def ls2952_membranes1():
    STARTKEY = 'a'


    gopos('membranes1_A_0004.json')
    mvr(ustrz, 0.1, ustry, -0.2)
    newdataset(f'main_memA_{STARTKEY}')
    dkpatchmesh(1.2, 600, 1.0, 100, 0.02, 1,1, retveloc=5.0)
    enddataset()


    gopos('membranes1_B_0002.json')
    mvr(ustrz, 0.1, ustry, -0.2)
    newdataset(f'main_memB_{STARTKEY}')
    dkpatchmesh(1.2, 600, 1.0, 100, 0.02, 1,1, retveloc=5.0)
    enddataset()


    gopos('membranes1_C_0003.json')
    mvr(ustrz, 0.1, ustry, -0.2)
    newdataset(f'main_memC_{STARTKEY}')
    dkpatchmesh(1.2, 600, 1.0, 100, 0.02, 1,1, retveloc=5.0)
    enddataset()
