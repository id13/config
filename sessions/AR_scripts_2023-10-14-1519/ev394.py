print('Tilmans macro night3 without tabs (afternoon bis)')
def ev394_night1():
    so()
    newdataset('si_braggpty1_a')
    stp = 0.004
    steps=120
    theta_peak = 116.825
    theta_start = theta_peak - int(steps/2)*stp
    theta_back = 1
    mv (Theta, theta_start -theta_back)
    mv (Theta, theta_peak)
    kmap.dkmap(nnp3,-1.2,1.2,80,nnp2,1.2,-1.2,30,0.08)
    mv (Theta, theta_start -theta_back)
    mv (Theta, theta_start)

    for i in range(steps):
        theta_curr = theta_start + i*stp
        print('Current theta',theta_curr,'cycle = ',i)
        mv (Theta, theta_curr)
        kmap.dkmap(nnp3,-1.2,1.2,80,nnp2,1.2,-1.2,30,0.08)

    mv (Theta, theta_start -theta_back)
    mv (Theta, theta_peak)
    kmap.dkmap(nnp3,-1.2,1.2,80,nnp2,1.2,-1.2,30,0.08)
    sc()

def ev394_night2():
    try:
        so()
        newdataset('branch_b')
        stp = 0.1
        steps = 150
        theta_start = -7.5
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,176)
            umv(nnp3,78)
            kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80,0.005)
            kmap.dkmap(nnp2,0,-24,80,nnp3,-24,0,80,0.005)
            kmap.dkmap(nnp2,24,0,80,nnp3,-12,12,80,0.005)

        newdataset('flower_b')
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,(103+12))
            umv(nnp3,(124-12))
            kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80,0.005)
            kmap.dkmap(nnp2,0,-24,80,nnp3,-24,0,80,0.005)
            kmap.dkmap(nnp2,24,0,80,nnp3,0,24,80,0.005)
            kmap.dkmap(nnp2,24,0,80,nnp3,-24,0,80,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()
def ev394_evening3():
    try:
        so()
        newdataset('63C_k2so4_braggptycho_b')
        stp = 0.01
        steps = 201
        theta_start = -1.0
        theta_back = 1.0
        umv(nnp2,49.0)
        umv(nnp3,58.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2,200,0.07)
        mv(Theta,0)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,49.0)
            umv(nnp3,58.0)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.07)
        umv(nnp2,49.0)
        umv(nnp3,58.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2,200,0.07)
        mv(Theta,0)
        

        sc()
    finally:
        sc()
        sc()
        sc()

def ev394_night3():
    try:
        so()
        newdataset('s62b_nanodiff_a')
        stp = 0.1
        steps = 150
        theta_start = -5.5
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,152)
            umv(nnp3,154)
            kmap.dkmap(nnp2,0,-12,80,nnp3,0,12,80,0.005)
            kmap.dkmap(nnp2,0,-12,80,nnp3,-12,0,80,0.005)
            kmap.dkmap(nnp2,12,0,80,nnp3,0,12,80,0.005)
            kmap.dkmap(nnp2,12,0,80,nnp3,-12,0,80,0.005)

        sc()
    finally:
        sc()
        sc()
        sc()
        
def ev394_afternoon():
    try:
        so()
        newdataset('62B_300C_braggptycho_a')
        stp = 0.01
        steps = 501
        theta_start = -1.25
        theta_back = 1.0
        umv(nnp2,114.75)
        umv(nnp3,140.5)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,5,500,0.1)
        mv(Theta,0)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,114.75)
            umv(nnp3,140.5)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.1)
        umv(nnp2,114.75)
        umv(nnp3,140.5)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,5,500,0.1)
        mv(Theta,0)
        

        sc()
    finally:
        sc()
        sc()
        sc()
        
def ev394_afternoon_bis():
    try:
        so()
        newdataset('62B_300C_braggptycho_bis_a')
        stp = 0.015
        steps = 401
        theta_start = -1.5
        theta_back = 1.0
        umv(nnp2,90.0)
        umv(nnp3,153.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,6,400,0.04)
        # mv(Theta,0)
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,90.0)
            umv(nnp3,153.0)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.04)
        umv(nnp2,90.0)
        umv(nnp3,153.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,6,400,0.04)
        mv(Theta,0)
        

        sc()
    finally:
        sc()
        sc()
        sc()
        
def ev394_afternoon_bisou():
    try:
        so()
        newdataset('62B_300C_braggptycho_disc4b_a')
        stp = 0.015
        steps = 75
        theta_start = -0.85
        theta_back = 1.0
        umv(nnp2,90.5)
        umv(nnp3,154.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.1,75,0.02)
        # mv(Theta,0)
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,90.5)
            umv(nnp3,154.0)
            kmap.dkmap(nnp3,-0.6,0.6,20,nnp2,0.6,-0.6,20,0.02)
        umv(nnp2,90.5)
        umv(nnp3,154.0)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.1,75,0.02)
        mv(Theta,0)
        

        sc()
    finally:
        sc()
        sc()
        sc()
        
def ev394_night4():
    try:
        so()
        newdataset('s861_nanodiffroi2_a')
        stp = 0.13
        steps = 160
        theta_start = -4.9
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,114)
            umv(nnp3,127)
            kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80,0.005)
            kmap.dkmap(nnp2,0,-24,80,nnp3,-24,0,80,0.005)
            kmap.dkmap(nnp2,24,0,80,nnp3,0,24,80,0.005)
            kmap.dkmap(nnp2,24,0,80,nnp3,-24,0,80,0.005)

        sc()
    finally:
        sc()
        sc()
        sc()

