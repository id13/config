from bliss.setup_globals import *

def sc5008_klineelem(direc, stp=0.25, hleng=100.0, expt=0.05):
    print ("====@@ call sc5008_klineelem", direc, stp, hleng, expt)
    npts = int(2*(hleng/stp))
    if direc == 'h':
        print(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt)
        kmap.dkmap(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt) 
    elif direc == 'v':
        print(nnp3,hleng,-hleng,npts,nnp2,0,0,1,expt)
        kmap.dkmap(nnp3,-hleng,hleng,npts,nnp2,0,0,1,expt)

def sc5008_kline(direc, linleng, elstp=0.25, expt=0.05):
    print ("====@@ call sc5008_kline", direc, linleng, expt)
    hlf_el = 100.0
    el = 2*hlf_el
    ne = int(linleng/el)

    if direc == 'h':
        try:
            hx_spc = 0.001*el
            hx_of = 0.5*hx_spc
            hx_start = nny.position
            for i in range(ne):
                hx_curr = hx_start + hx_of + i*hx_spc
                print("LINE direc:", direc, "  el:", i, "  hx_curr:", hx_curr)
                mv(nny,hx_curr)
                sc5008_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nny,hx_start)
    elif direc == 'v':
        try:
            vx_spc = 0.001*el
            vx_of = 0.5*vx_spc
            vx_start = nnz.position
            for i in range(ne):
                vx_curr = vx_start + vx_of + i*vx_spc
                print("LINE direc:", direc, "  el:", i, "  vx_curr:", vx_curr)
                mv(nnz,vx_curr)
                sc5008_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nnz,vx_start)
    else:
        raise ValueError

def sc5008_klinegrid(direc, leng, elstp, nlines, linestp, expt=0.05):
    print ("====@@ call sc5008_klinegrid", direc, leng, elstp, nlines, linestp, expt)
    if direc == 'h':
        gapmot = nnz
    elif direc == 'v':
        gapmot = nny

    assert gapmot in (nny, nnz)

    gapmot_start = gapmot.position
    try:
        for j in range(nlines):
            gapmot_curr = gapmot_start + j*linestp
            print("GAP direc:", direc, "  el:", j, "  gapmot_curr:", gapmot_curr)
            mv(gapmot,gapmot_curr)
            sc5008_kline(direc, leng, elstp=elstp, expt=expt)
    finally:
        mv(gapmot,gapmot_start)

def sc5008_kpatch(expt=0.05):
    print ("====@@ call sc5008_kpatch", expt)
    #sleep(2)
    #print('kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80, expt)')
    kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80, expt)

def sc5008_kpatchmatrix(h,v, hstp=0.025, vstp=0.025, expt=0.05):
    print ("====@@ call sc5008_kpatchmatrix", h, v, hstp, vstp, expt)
    try:
        h_pos = nny.position
        v_pos = nnz.position
        for j in range(v):
            v_curr = v_pos + j*vstp
            mv(nnz,v_curr)
            for i in range(h):
                h_curr = h_pos + i*hstp
                mv(nny,h_curr)
                sc5008_kpatch(expt=expt)
    finally:
        mv(nny, h_pos)
        mv(nnz, v_pos)

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    s.replace('-','_')
    gopos(x)
    newdataset('s01_%s' % s)
    fshtrigger()

def sc5008_main_E():

    the_expt = 0.05




    tgopos('E_c_w14_air02_0086.json')
    sc5008_kpatch(expt=the_expt)

    tgopos('E_c_w13_s01_p_0087.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('E_c_w13_s02_p_0088.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('E_c_w13_s03_p_0089.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)


    tgopos('E_c_w13_s05_v_0090.json')
    sc5008_klinegrid('v', 400, 0.25, 3, 0.001, the_expt)
    mvr(nnz, 0.20)
    mvr(nny, 0.40)
    newdataset('s01_E_c_w13_s04_added')
    sc5008_klinegrid('h', 1200, 0.25, 10, 0.1, the_expt)
    


    tgopos('E_c_w14_s01_p_0078.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('E_c_w14_s02_p_0079.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('E_c_w14_s03_p_0081.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('E_c_w14_s04_h_0082.json')
    sc5008_klinegrid('h', 1600, 0.25, 5, 0.08, the_expt)

    tgopos('E_c_w14_s05_v_0083.json')
    sc5008_klinegrid('v', 400, 0.25, 3, 0.001, the_expt)

    tgopos('E_c_w14_s06_j_0084.json')
    sc5008_klinegrid('h', 400, 2.0, 80, 0.005, the_expt)

    tgopos('E_c_w14_air02_0086.json')
    sc5008_kpatch(expt=the_expt)






# ################################

POOL = """
    sc5008_kpatch(expt=the_expt)

    sc5008_kpatchmatrix(2,2, expt=the_expt)

    sc5008_klinegrid('v', 200, 0.25, 3, 0.001, the_expt)

    sc5008_klinegrid('h', 1400, 0.25, 3, 0.001, the_expt)

    sc5008_klinegrid('h', 200, 2.0, 100, 0.002, the_expt)

"""
