PP_POS_STR = '''#
# initial guessed positions - wrong
#1 pp1 -9.992 1.3511
#2 pp2 -10.098 1.3511
#3 pp3 -9.992 1.4571
#4 pp4 -10.098 1.4571
#5 pp5 -10.045 1.4041
#6 empty -11.09 2.7089
# corners of the pp in the middle of the opening of the slits
101 A -10.0020 1.3411
102 B -10.0020 1.4911
103 C -10.1520 1.4911
104 D -10.1520 1.3411
# corners of the pp to the edges of the slits
201 AA -10.0395 1.3786
202 BB -10.0395 1.4536
203 CC -10.1145 1.4536
204 DD -10.1145 1.3786
205 ZZ -10.0770 1.4161
# slightly shifted corners of the pp with respect to the slits. TO BE USED FOR MEASUREMENTS
1 AM -10.0395-0.015 1.3786+0.015
2 BM -10.0395-0.015 1.4536-0.015
3 CM -10.1145+0.015 1.4536-0.015
4 DM -10.1145+0.015 1.3786+0.015
5 ZZ -10.0770 1.4161
6 empty -11.09 2.7089
#'''

PP_NUM_POS_DC = {}
PP_STR_POS_DC = {}
def es1244_static_setup():
    ll = PP_POS_STR.split('\n')
    for l in ll:
        print(f'l: [{l}]')
        if not l or l.startswith('#'):
            pass
        else:
            num, thestr, posy, posz = l.split()
            posy = float(eval(posy))
            posz = float(eval(posz))
            num = int(num)
            PP_NUM_POS_DC[num] = (posy, posz)
            PP_STR_POS_DC[thestr] = (posy, posz)
    print('PP_NUM_POS_DC: ==========================')
    pprint(PP_NUM_POS_DC)
    print('PP_STR_POS_DC: ==========================')
    pprint(PP_STR_POS_DC)

def mv_num_pp(num):
    (posy, posz) = PP_NUM_POS_DC[num]
    umv(nfzpbsy, posy, nfzpbsz, posz)
    
def mv_str_pp(thestr):
    (posy, posz) = PP_STR_POS_DC[thestr]
    umv(nfzpbsy, posy, nfzpbsz, posz)

def wpp():
    wm(nfzpbsy, nfzpbsz)
    
def mgeigmpx_x():
    mgd()
    MG_EH3a.enable('*ger:imag*')
    MG_EH3a.enable('*ger:roi*')
    MG_EH3a.enable('*mpxeh3:imag*')
    MG_EH3a.enable('*mpxeh3:roi*')
    MG_EH3a.enable('xmap3:*')
