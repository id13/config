COUNTER_DICT = dict(
    diode = 'p201_eh3_0:ct2_counters_controller:ct32',
    eiger_roitpl = 'eiger:roi_counters:%s_avg',
)
def get_last_scan_keydata(counter_key):
    sc = SCANS[-1]
    data = sc.get_data()
    arr = data[COUNTER_DICT[counter_key]]
    return arr

def get_last_scandata():
    sc = SCANS[-1]
    data = sc.get_data()
    return data

def get_last_scanmotpos(idx):
    data = get_last_scandata()
    ax_keys = [k for k in data.keys() if k.startswith("axis")]
    scanmotpos = []
    for k in ax_keys:
        a,nam = k.split(":")
        mot = getattr(SG, nam)
        pos = data[k][idx]
        scanmotpos.append((mot, pos))
    return scanmotpos

def get_last_maxidx(counter_key):
    arr = get_last_scan_keydata(counter_key)
    return arr.argmax()

def ggg_gopt(i):
    scanmotpos = get_last_scanmotpos(i)
    print ("\nnow we will be moving things ...")
    for (m,p) in scanmotpos:
        print ("\n    %s.move(%f)" % (m.name, p))
        m.move(p)

def ggg_alignmax(counter_key):
    i = get_last_maxidx(counter_key)
    print(i)
    ggg_gopt(i)
