
def do_fishing_lunch1():
	so()
	rstp('zone5_50x_fishing2')
	newdataset('zone5_fishing2')
	fshtrigger()
	#theta backlash corr
	theta_start = -5
	theta_incr = 0.333
	max_steps = 21
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,60,nnp3,-30,30,60,exp_time)
	rstp('zone5_50x_fishing3')
	newdataset('zone5_fishing3')
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,60,nnp3,-30,30,60,exp_time)
	rstp('zone5_50x_fishing4')
	newdataset('zone5_fishing4')
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,60,nnp3,-30,30,60,exp_time)
	fshtrigger()
	sc()
	
	
def do_fishing_afternoon1():
	so()
	rstp('zone5_50x_nd')
	newdataset('zone5_nd_fishing')
	fshtrigger()
	#theta backlash corr
	theta_start = -5
	theta_incr = 0.333
	max_steps = 21
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,60,nnp3,-30,30,60,exp_time)
		
def do_bragg_rin1():
	so()
	newdataset('ring1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -6.8
	theta_incr = 0.005
	max_steps = 80
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,15,nnp2,-0.375,0.375,15,exp_time)
	
		
def do_bragg_rin3():
	so()
	newdataset('ring3_bragg_b')
	fshtrigger()
	#theta backlash corr
	theta_start = -6.4
	theta_incr = 0.005
	max_steps = 80
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)

def do_bragg_edge1():
	so()
	newdataset('edge1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -5.3
	theta_incr = 0.005
	max_steps = 240
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
		
def do_bragg_ring4():
	so()
	newdataset('ring4_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -9.3
	theta_incr = 0.005
	max_steps = 460
	exp_time = 0.03
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
def do_bragg_ref1():
	so()
	newdataset('ref1_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -6
	theta_incr = 0.005
	max_steps = 450
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)


def do_bragg_ref3():
	so()
	newdataset('ref3_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -6
	theta_incr = 0.005
	max_steps = 400
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)

def do_bragg_edge2():
	so()
	newdataset('edge2_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -5.3
	theta_incr = 0.005
	max_steps = 220
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.375,0.375,16,nnp2,-0.375,0.375,16,exp_time)
def do_bragg_ring2_pp():
	so()
	newdataset('ring2_pp_bragg_a')
	fshtrigger()
	#theta backlash corr
	theta_start = -6.8
	theta_incr = 0.005
	max_steps = 200
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp3,-0.75,0.75,25,nnp2,-0.75,0.75,25,exp_time)


def do_fishing_night1():
	so()
	rstp('zone5_50x_nd')
	newdataset('zone5_nd_nanodiffraction_c')
	fshtrigger()
	#theta backlash corr
	theta_start = -7
	theta_incr = 0.1
	max_steps = 71
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
		
		
def do_fishing_night2():
	so()
	umv(nnp2,55.5,nnp3,127.64)
	newdataset('bp_roi_nd_c')
	fshtrigger()
	#theta backlash corr
	theta_start = -7.9
	theta_incr = 0.1
	max_steps = 37
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
		so()
	umv(nnp2,166.8,nnp3,166)
	umvr(nnp3,20)
	newdataset('side_view_nd')
	fshtrigger()
	#theta backlash corr
	theta_start = -13.0
	theta_incr = 0.1
	max_steps = 71
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
		
def do_fishing_night1_catchup():
	so()
	rstp('zone5_50x_nd')
	newdataset('zone5_nd_nanodiffraction_e')
	fshtrigger()
	#theta backlash corr
	theta_start = -5.3
	theta_incr = 0.1
	max_steps = 4
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
	theta_start = -3.6
	theta_incr = 0.1
	max_steps = 4
	exp_time = 0.005
	theta_back = theta_start -1
	umv(Theta, theta_back)
	umv(Theta, theta_start)
	for ii in range(0,max_steps):
		theta_go = ii*theta_incr+theta_start
		print(theta_go)
		umv(Theta, theta_go)
		kmap.dkmap(nnp2,-30,30,300,nnp3,-30,30,300,exp_time)
