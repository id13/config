import numpy as np

def make_rois():
    try:
        MG_EH3a.enable("*roi*")
    except ValueError:
        pass

def mgm():
    mgd()
    MG_EH3a.enable("*mpxeh3he:image*")
    make_rois()

def mgd():
    MG_EH3a.set_active()
    MG_EH3a.disable("*")
    MG_EH3a.enable("*ct32*")
    MG_EH3a.enable("*acq_time_3")
    

def whelp():
    print('''
wpco  whx      wcors          wnnp         wss6   wpin     wmic
pco   hexapod  coarse_sample  fine_sample  slits  pinhole  microscope
    
def wpco():
    # pco
    wm(npcox, npcoy, ndetz, mot_pco)

def wmpx():
    # pco
    wm(narmxx, narmz, ndetz, mot_pco)

def whx():
    # optics
    wm(nnx, nny, nnz, nnu, nnv, nnw)

def wcors():
    # coarse sample movements
    wm(norx, nory, norz, smza)

def wpin():
    # pinhole
    wm(smxb, smyb, smzb)

def wmic():
    # side microscope
    wm(nfluoax, nfluoay, nfluoaz)

    ''')
def wpco():
    # pco
    wm(npcox, npcoy, ndetz, mot_pco)

def wmpx():
    # pco
    wm(narmxx, narmz, ndetz, mot_pco)

def whx():
    # optics
    wm(nnx, nny, nnz, nnu, nnv, nnw)

def wcors():
    # coarse sample movements
    wm(norx, nory, norz)

def wpin():
    # pinhole
    wm(smxb, smyb, smzb)

def wmic():
    # side microscope
    wm(nfluoax, nfluoay, nfluoaz)

def fshopen():
    raise NotImplemented('use ufsh_open ...')

def fshclose():
    raise NotImplemented('use ufsh_close ...')

def flux_ct(expt, n=1, ringcurrent='auto'):
    if 'auto' == ringcurrent:
        machinfo = tango.DeviceProxy("acs.esrf.fr:10000/fe/master/id13")
        ringcurrent = machinfo.SR_Current
    # measure background
    ufsh_close()
    sct(expt)
    background = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
    print(f'Background: {background}')

    ufsh_open()()
    if n > 1:
        loopscan(n, expt)
        sven_getflux(n, background, ringcurrent)
    else:
        sct(expt)
        sven_getflux(background=background, ringcurrent=ringcurrent)

def pct(expt):
    # is also in nt2 simple - cleanup needed ...
    try: 
        ufsh_open() 
        time.sleep(0.05)
        sct(expt, pco, mpxeh3he)
        ufsh_close() 
        elog_print(f'pco image: #S {SCANS[-1].scan_number}')
    finally: 
        ufsh_close()

def pdscan(*p):
    ll = list(p)
    ll.append(pco)
    ll.append(mpxeh3he)

    try: 
        ufsh_open() 
        time.sleep(0.05)
        dscan(*ll)
        elog_print(f'pco scan: #S {SCANS[-1].scan_number}')
    finally: 
        ufsh_close()


def mct(expt):
    # is also in nt2 simple - cleanup needed ...
    try: 
        ufsh_open() 
        time.sleep(0.05)
        sct(expt, mpxeh3he)
        ufsh_close() 
        elog_print(f'mpceh3he image: #S {SCANS[-1].scan_number}')
    finally: 
        ufsh_close()

def mdscan(*p):
    ll = list(p)
    mgm()
    #ll.append(mpxeh3he)

    try: 
        ufsh_open() 
        time.sleep(0.05)
        dscan(*ll)
        elog_print(f'mpceh3he dscan: #S {SCANS[-1].scan_number}')
    finally: 
        ufsh_close()

def mdmesh(*p):
    ll = list(p)
    #ll.append(mpxeh3he)
    mgm()

    try: 
        ufsh_open() 
        time.sleep(0.05)
        dmesh(*ll)
        elog_print(f'mpceh3he dmesh: #S {SCANS[-1].scan_number}')
    finally: 
        ufsh_close()

def load_posarr(fname):
    posarr = np.load(fname)
    return posarr

def file_lup_dscan2d(fname, m1, m2, expt, *detectors):
    posarr = load_posarr(fname)
    posarr1 = posarr[1]
    posarr2 = posarr[0]
    lup_dscan2d(m1, m2, posarr1, posarr2, expt, *detectors)
    import shutil
    alliterms=os.listdir()
    chdir = [iterm for iterm in alliterms if os.path.isdir(iterm)][0] 
    shutil.copy(fname,f'./{chdir}/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')

def file_lup_dscan3d(fname, m1, m2, expt, *detectors):
    posarr = np.load(fname)
    posarr1 = posarr[0]
    posarr2 = posarr[1]
    posarr_dummy = posarr[2]
    #print(len(posarr_dummy),len(posarr1),len(posarr2))
    lup_dscan3d(m1, m2, posarr1, posarr2, posarr_dummy, expt, *detectors)
    import shutil
    alliterms=os.listdir()
    chdir = [iterm for iterm in alliterms if os.path.isdir(iterm)][0] 
    shutil.copy(fname,f'./{chdir}/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')

def check_pz(m):
    if m.name not in ('nnp1', 'nnp2', 'nnp3'):
        raise ValueError(f'illegal motor {m.name}')

def lup_dscan2d(m1, m2, posarr1, posarr2, expt, *detectors):
    check_pz(m1)
    check_pz(m2)
    start_pos_m1 = m1.position
    start_pos_m2 = m2.position
    a_posarr1 = posarr1 + start_pos_m1
    a_posarr2 = posarr2 + start_pos_m2

    pos_args = [(m1, a_posarr1),(m2, a_posarr2)]
    print(detectors)
    try:
        ufsh_open()
        time.sleep(0.05)
        lookupscan(pos_args, expt, *detectors)
        elog_print(f'mpceh3he lookupscan: #S {SCANS[-1].scan_number}')
        ufsh_close()
    finally:
        mv(m1, start_pos_m1)
        mv(m2, start_pos_m2)

def lup_dscan3d(m1, m2, posarr1, posarr2, posarr_dummy, expt, *detectors):
    check_pz(m1)
    check_pz(m2)
    start_pos_m1 = m1.position
    start_pos_m2 = m2.position
    a_posarr1 = posarr1 + start_pos_m1
    a_posarr2 = posarr2 + start_pos_m2

    mgm()
    NNN=None
    print(posarr_dummy.dtype, a_posarr1.dtype, a_posarr2.dtype)
    if None is NNN:
        pos_args = [(dummy, posarr_dummy),(m1, a_posarr1),(m2, a_posarr2)]
    else:
        pos_args = [(dummy, posarr_dummy[:NNN]),(m1, a_posarr1[:NNN]),(m2, a_posarr2[:NNN])]
    print('alive 0')
    print(pos_args)
    print(detectors)
    try:
        ufsh_open()
        time.sleep(0.05)
        print('alive 1')
        sys.stdout.flush()
        lookupscan(pos_args, expt, *detectors)
        elog_print(f'mpceh3he lookupscan: #S {SCANS[-1].scan_number}')
        ufsh_close()
    finally:
        mv(dummy, 0)
        mv(m1, start_pos_m1)
        mv(m2, start_pos_m2)

def flyscan(*p):
    try:
        mgm()
        ufsh_open()
        time.sleep(0.05)
        kmap.dkmap(*p)
        ufsh_close()
        try:
            elog_print(f'mpceh3he scan: #S {SCANS[-1].scan_number}')
        except:
            print('elog failure ...')
    finally:
       ufsh_close()

def single_lens_characterization_lookupscans():
    expt = 0.6
    file_lup_dscan2d('../../NOTES/preread_pos/scan00051_jitter_pos.npy',nnp2,nnp3,expt,p201_eh3_0,mpxeh3he)



def single_lens_characterization_scans():
    w = 15
    expt = 0.6
    n = 30
    p = (nnp2, 0, w, n, nnp3, -w, 0, n, expt)
    mdmesh(*p)
    mdmesh(*p)
    mdmesh(*p)

def skew_array_scans():
    
    inds = (
        (0, 1),
        (1, 1),
        (2, 1),
        (3, 1),
        (1, 2),
        (2, 2),
    )
    M = np.array([[ 2.95985326e-02,  3.09289032e-02, -2.50240006e+00],
       [ 1.49628850e-06, -5.79113987e-02,  2.44999996e+00],
       [ 6.18020275e-07, -1.20867007e-02,  1.00000000e+00]])
    inds = np.array(inds)
    y_inds = inds[:, 0]
    z_inds = inds[:, 1]
    ij_hom = np.vstack([y_inds.flatten(), z_inds.flatten(), np.ones((z_inds.size ,))])
    yz_hom = M @ ij_hom
    yz = yz_hom[:2] / yz_hom[-1]
    ys = yz[0, :]
    zs = yz[1, :]
    ns = np.arange(ys.size)
    for y, z, n in zip(ys, zs, ns):
        print('skew array scan {} out of {}'.format(n+1, ns.size))    
        umv(nny, y, nnz, z)
        #single_lens_characterization_scans()
        single_lens_characterization_lookupscans()

def get_skew_pos(i,j,M):
    ij_hom = np.vstack([[i], [j], 1])
    yz_hom = M @ ij_hom
    yz = yz_hom[:2] / yz_hom[-1]
    y = yz[0, 0]
    z = yz[1, 0]

    return y, z

def skew_array_scans_with_pinhole():
    inds = (
        (0, 0),
        (0, 1),
        (0, 2),
        (1, 0),
        (1, 1),
        (1, 2),
        (2, 0),
        (2, 1),
        (2, 2),
        (3, 0),
        (3, 1),
        (3, 2),
    )
    M = np.array([[-2.02543200e+01,  7.22675307e+00,  4.53386635e+02],
       [ 3.33186678e+00,  3.32698268e+01,  8.57312998e+01],
       [ 2.68033799e-02,  1.52846541e-02,  1.00000000e+00]])
    ref_pos = get_skew_pos(0, 0, M)
    #star_pos_00 = (135, 153.5)  # (nnp2, nnp3)
    star_pos_00 = np.array([-4.4645,   5.8750])


    inds = np.array(inds)
    y_inds = inds[:, 0]
    z_inds = inds[:, 1]
    ij_hom = np.vstack([y_inds.flatten(), z_inds.flatten(), np.ones((z_inds.size ,))])
    yz_hom = M @ ij_hom
    yz = yz_hom[:2] / yz_hom[-1]
    ys = yz[0, :]
    zs = yz[1, :]
    ns = np.arange(ys.size)
    for y, z, n in zip(ys, zs, ns):   
        print('skew array scan {} out of {}'.format(n+1, ns.size))     
        offset_in_mm = np.array([(y-ref_pos[0])*1e-6/1e-3, (z-ref_pos[1])*1e-6/1e-3])
        star_pos = star_pos_00 + offset_in_mm
        print(offset_in_mm, star_pos)
        umv(smyb, y, smzb, z)
        umv(nory, star_pos[0], norz, star_pos[1])
        single_lens_characterization_scans()

def skew_array_lensscans_with_pinhole_and_slits():
    #lens_1_pos=np.array([-2.443,2.390])
    #lens_4_pos=np.array([-2.353,2.390])    
    #lens_12_pos=np.array([-2.353,2.450])
    lens_12_pos=np.array([-2.191,2.710])
    lens_9_pos=np.array([-2.101,2.711])    
    lens_1_pos=np.array([-2.104,2.771])
    origin=lens_12_pos
    vi=(lens_9_pos-lens_12_pos)/3
    vj=(lens_1_pos-lens_9_pos)/2
    pos_array=np.zeros([12,2])
    kk=0
    for ii in range(4):
        for jj in range(3):
            pos_lens=origin+np.array(vi*ii)+np.array(vj*jj)
            pos_array[kk]=pos_lens
            print(pos_lens)
            kk=kk+1
    # Scan for each lens
    for ii in range(12):
        umv(nny,pos_array[ii,0],nnz,pos_array[ii,1])
        #pct(2)
        #mct(0.2)
        #time.sleep(1)
        import shutil
        elog_print(f'mpceh3he file_lup_dscan2d: #S {SCANS[-1].scan_number}')
        ufsh_open()
        time.sleep(0.05)
        file_lup_dscan2d('../../NOTES/preread_pos/L3_singlelens_0730_fermat_pos.npy',nnp2,nnp3,0.2,p201_eh3_0,mpxeh3he)      
        ufsh_close()
        shutil.copy('../../NOTES/preread_pos/L3_singlelens_0730_fermat_pos.npy',f'./L3_characterization_0001/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')
    
    # Move back to lens12 position
    umv(nny,origin[0],nnz,origin[1])


#def incremental_multi_beam_scans_L3():
#    12_beam_pin = np.array([-6395.944, 357.496])  # smyb, smzb
#    2_beam_pin = 12_beam_pin + np.array([90, -30])
#    3_beam_pin = 12_beam_pin + np.array([90, 0])
#    9_beam
#    6_beam_pin = 12_beam_pin + np.array([60, 0])

#    2_beam_star = np.array([?, ?])  # nory, norz
#    3_beam_star = 2_beam_star + np.array([0, 0.015])
#    6_beam_star = 3_beam_star + np.array([-0.015, 0])
#    12_beam_star = 3_beam_star + np.array([-0.045, 0])
 
def mb_lookupscans_sunday_night():
    import shutil
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_35um_com_pos.npy',nnp2,nnp3,0.09,p201_eh3_0,mpxeh3he)
    shutil.copy('../../NOTES/preread_pos/L3_multi_35um_com_pos.npy',f'./L3_characterization_0001/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_40um_com_pos.npy',nnp2,nnp3,0.09,p201_eh3_0,mpxeh3he)
    shutil.copy('../../NOTES/preread_pos/L3_multi_40um_com_pos.npy',f'./L3_characterization_0001/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_50um_com_pos.npy',nnp2,nnp3,0.09,p201_eh3_0,mpxeh3he)
    shutil.copy('../../NOTES/preread_pos/L3_multi_50um_com_pos.npy',f'./L3_characterization_0001/scan{SCANS[-1].scan_number}/scan{SCANS[-1].scan_number}_jitter_pos.npy')  
    ufsh_close()

def afternoon_monday_scan():
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan3d('../../NOTES/preread_pos/3dscan_intega_35um_com_pos.npy', nnp2, nnp3, 0.2)
    ufsh_close()
    umvr(norz,-0.045)
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan3d('../../NOTES/preread_pos/3dscan_intega_35um_com_pos.npy', nnp2, nnp3, 0.2)
    ufsh_close()

def mb_lookupscans_monday_night():
    ufsh_open()
    time.sleep(0.05)
    file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_35um_com_pos.npy',nnp2,nnp3,0.09,p201_eh3_0,mpxeh3he)
    umvr()

    

def patch_scans_set_monday_night():
    import shutil
    jump_size_z = 0.09  # distance to jump to start the next scan
    jump_size_y = 0.06  # distance to jump to start the next scan

    nor_origin = np.array([-4.9550,   7.7300])  # nory, norz in middle of sample window (scan 21)
    #origin_nnp = np.array([133, 156])  # nnp2, nnp3 in middle of sample window (scan 21)

    z_jumps = np.arange(2)
    y_jumps = np.arange(4)
    z_jumps = z_jumps - np.mean(z_jumps)
    y_jumps = y_jumps - np.mean(y_jumps) + 0.5
    print('y_jumps', y_jumps)
    print('z_jumps', z_jumps)
    n_scans = y_jumps.size * z_jumps.size
    print(f'about to make {n_scans} scans')
    for y_jump in y_jumps:
        for z_jump in z_jumps:
            nor_pos_rel = np.array([y_jump * jump_size_y, z_jump * jump_size_z])
            nor_pos_abs = nor_pos_rel + nor_origin
            print('nor_pos_abs', nor_pos_abs)
            
            # move motors
            umv(nory, nor_pos_abs[0], norz, nor_pos_abs[1])

            # do the scan
            ufsh_open()
            time.sleep(0.05)
            file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_35um_com_pos.npy',nnp2,nnp3,0.1,p201_eh3_0,mpxeh3he)
    ufsh_close()
            


def repeated_patch_sets_monday_night():
    for i in range(3):
        patch_scans_set_monday_night()


def patch_scans_set_tuesday_morning():
    import shutil
    jump_size_z = 0.09  # distance to jump to start the next scan
    jump_size_y = 0.120  # distance to jump to start the next scan

    nor_origin = np.array([-4.9550,   7.685])  # nory, norz in middle of sample window (scan 21)
    #origin_nnp = np.array([133, 156])  # nnp2, nnp3 in middle of sample window (scan 21)

    z_jumps = np.arange(2)
    y_jumps = np.arange(2)
    z_jumps = z_jumps
    y_jumps = y_jumps
    print('y_jumps', y_jumps)
    print('z_jumps', z_jumps)
    n_scans = y_jumps.size * z_jumps.size
    print(f'about to make {n_scans} scans')
    for y_jump in y_jumps:
        for z_jump in z_jumps:
            nor_pos_rel = np.array([y_jump * jump_size_y, z_jump * jump_size_z])
            nor_pos_abs = nor_pos_rel + nor_origin
            print('nor_pos_abs', nor_pos_abs)
            
            # move motors
            umv(nory, nor_pos_abs[0], norz, nor_pos_abs[1])
 
            # do the scan
            ufsh_open()
            time.sleep(0.05)
            file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_40um_com_pos.npy',nnp2,nnp3,0.1,p201_eh3_0,mpxeh3he)
    ufsh_close()

def patpatch_scans_set_tuesday_noon():
    import shutil
    jump_size_z = 0.09  # distance to jump to start the next scan
    jump_size_y = 0.120  # distance to jump to start the next scan

    nor_origin = np.array([-3.0300,   6.2000])  # center: nory, norz microchip 12 beams
    #nor_origin = np.array([-4.9550,   7.685])  # nory, norz in middle of sample window (scan 21)
    #origin_nnp = np.array([133, 156])  # nnp2, nnp3 in middle of sample window (scan 21)
    z_jumps = np.arange(4)
    y_jumps = np.arange(4)
    z_jumps = z_jumps-np.mean(z_jumps)
    y_jumps = y_jumps-np.mean(y_jumps)
    print('y_jumps', y_jumps)
    print('z_jumps', z_jumps)
    n_scans = y_jumps.size * z_jumps.size
    print(f'about to make {n_scans} scans')
    for y_jump in y_jumps:
        for z_jump in z_jumps:
            nor_pos_rel = np.array([y_jump * jump_size_y, z_jump * jump_size_z])
            nor_pos_abs = nor_pos_rel + nor_origin
            print('nor_pos_abs', nor_pos_abs)
            
            # move motors
            umv(nory, nor_pos_abs[0], norz, nor_pos_abs[1])

            # do the scan
            ufsh_open()
            time.sleep(0.05)
            file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_40um_com_pos.npy',nnp2,nnp3,0.1,p201_eh3_0,mpxeh3he)
            ufsh_close()


def patch_scans_set_tuesday_night():
    import shutil
    jump_size_z = 0.09  # distance to jump to start the next scan
    jump_size_y = 0.120  # distance to jump to start the next scan

    nor_origin = np.array([-3.900, 3.850])  # center: nory, norz FeMgAl2O3_12beam 12 beams

    z_jumps = np.arange(4)
    y_jumps = np.arange(4)
    z_jumps = z_jumps-np.mean(z_jumps)
    y_jumps = y_jumps-np.mean(y_jumps)
    print('y_jumps', y_jumps)
    print('z_jumps', z_jumps)
    n_scans = y_jumps.size * z_jumps.size
    print(f'about to make {n_scans} scans')
    for y_jump in y_jumps:
        for z_jump in z_jumps:
            nor_pos_rel = np.array([y_jump * jump_size_y, z_jump * jump_size_z])
            nor_pos_abs = nor_pos_rel + nor_origin
            print('nor_pos_abs', nor_pos_abs)
            
            # move motors
            umv(nory, nor_pos_abs[0], norz, nor_pos_abs[1])

            # do the scan
            ufsh_open()
            time.sleep(0.05)
            file_lup_dscan2d('../../NOTES/preread_pos/L3_multi_40um_com_pos.npy',nnp2,nnp3,0.1,p201_eh3_0,mpxeh3he)
            ufsh_close()


    



   

    

