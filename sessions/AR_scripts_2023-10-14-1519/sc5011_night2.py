import os 
print ('armed rl1...')

DS_KEY = 'b'

def dummy(*p,**kw):
    print('dummy', p, kw)

#newdataset = dummy
#enddataset = dummy
#dmesh = dummy

def sc5011_night2():
    gopos('B_x2a')
    newdataset('B_x2a_%s' % DS_KEY)
    dmesh(nnp3,-12.5,12.5,100,nnp2,100,-100,200,0.5) 
    enddataset()


    gopos('C_x1a')
    newdataset('C_x1a_%s' % DS_KEY)
    dmesh(nnp3,-12.5,12.5,100,nnp2,100,-100,200,0.5) 
    enddataset()


    gopos('C_x2a')
    newdataset('C_x2a_%s' % DS_KEY)
    dmesh(nnp3,-12.5,12.5,100,nnp2,100,-100,200,0.5) 
    enddataset()


    gopos('D_x1a')
    newdataset('D_x1a_%s' % DS_KEY)
    dmesh(nnp3,-12.5,12.5,200,nnp2,50,-50,100,0.5) 
    enddataset()


    gopos('E_x1a')
    newdataset('E_x1a_%s' % DS_KEY)
    dmesh(nnp3,-12.5,12.5,200,nnp2,50,-50,100,0.5) 
    enddataset()
