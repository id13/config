print('ihsc1796 - Yin load 1')
def ihsc1796_night():
    mgeig_x()
    eigerhws(True)

    START_KEY = 'a'
    newdataset(f'{START_KEY}_night_DS_9')
    rstp('tripmount_DS_9_0033.json')
    kmap.dkmap(nnp2, -80,80,320, nnp3, -80,80,320,0.01, frames_per_file=320*8)

    newdataset(f'{START_KEY}_night_DS_15')
    rstp('tripmount_DS_15_0039.json')                                                                                     
    kmap.dkmap(nnp2, -80,80,320, nnp3, -80,80,320,0.02, frames_per_file=320*8)

    newdataset(f'{START_KEY}_night_DS_20')
    rstp('tripmount_DS_20_0044.json')                                                                                     
    kmap.dkmap(nnp2, -80,80,320, nnp3, -80,80,320,0.03, frames_per_file=320*8)

    newdataset(f'{START_KEY}_night_DS_33')
    rstp('tripmount_DS_33_0058.json')
    kmap.dkmap(nnp2, -80,80,320, nnp3, -80,80,320,0.02, frames_per_file=320*8)

    newdataset(f'{START_KEY}_night_DS_30')
    rstp('tripmount_DS_30_0055.json')
    kmap.dkmap(nnp2, -80,80,320, nnp3, -80,80,320,0.04, frames_per_file=320*8)

def ihsc1796_night_main():
    try:
        ihsc1796_night()
    finally:
        sc()
        sc()
        sc()
