# beam size should be less 100 nm, this is 30 mA 16 bunch mode experiments
print('ihls3548 load 2')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t,frames_per_file):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #zeronnp()
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,
               nnp3,-1*zrange,zrange,zstep,
               exp_t,frames_per_file=frames_per_file)
    #zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,defocus=False):
    so()
    fshtrigger()
    mgeig_x()
    eigerhws(True)
    zeronnp()
    gopos(pos)
    pos_matrix = [[95,95],[155,95],[95,155],[155,155]]
    for i in range(4):
        umv(nnp2,pos_matrix[i][0],nnp3,pos_matrix[i][1])
        name = f'{pos[:-5]}_patch{i+1}_{start_key}'
        newdataset(name)
        kmap_scan(yrange/2,zrange/2,ystep_size,zstep_size,exp_t,
                  frames_per_file=2000)
        enddataset()


def September_24th_night1():
    try:
        umv(ndetx,-200)
        start_key = 'd'
        gopos_kmap_scan('D39_mOrange_iN_wo_iMG_s24_pos1_new_0006.json',start_key,
                        60,60,0.15,0.15,0.01)
        gopos_kmap_scan('D39_mOrange_iN_wo_iMG_s24_pos2_0001.json',start_key,
                        60,60,0.15,0.15,0.01)
        gopos_kmap_scan('D39_mOrange_iN_wo_iMG_s24_pos3_0002.json',start_key,
                        60,60,0.15,0.15,0.01)
        
        gopos('D39_mOrange_iN_wo_iMG_s24_pos4_paraffin_0003.json')
        mgeig_x()
        eigerhws(True)
        zeronnp()
        newdataset('paraffin_bkgd')
        kmap_scan(15,15,0.15,0.15,0.01,
                  frames_per_file=2000)
        sc()
    finally:
        sc()
        sc()

