print('md1388_c2_ov2 - load 2')
PREFIX = 'roi1_ov1_lac'
PIEZO_POS = '''
#no   p2    p3    npts   hwidth   expt  fpf 
#1     119.1  55.1  160    20       0.01  320 # ok
#2     146.9  44.5  160    20       0.01  320 # ok
#3     133.4  94.6  160    20       0.01  320 # eiger crashed
4     180    80.5  160    20       0.01  320
5     211.0  73.7  160    20       0.01  320
6     52.0   157.0 160    20       0.01  320
'''
  	

DRY_RUN = False
START_KEY = 'b'
def doo_scan(p2, p3, npts, hw, expt, fpf):
    mv(nnp2, p2)
    mv(nnp3, p3)
    if DRY_RUN:
        print(f'dry loff_kmap: nnp2, {-hw}, {hw},{npts}, nnp3, {-hw}, {hw}, {npts}, {expt}, {fpf}')
    else:
        loff_kmap(nnp2, -hw,hw,npts,nnp3,-hw,hw,npts,expt,
            frames_per_file=fpf)

def prepscan(prefix, piezo_pos_no):
    dsname = f'{START_KEY}_{prefix}_{piezo_pos_no:1d}'
    if DRY_RUN:
        print(f'dry newdataset: {dsname}')
    else:
        newdataset(dsname)
    
def run_piezo_positions():
    ll = PIEZO_POS.split('\n')
    for l in ll:
        l = l.strip()
        if l.startswith('#') or not l:
            continue
        pno, p2, p3, npts, hw, expt, fpf = l.split()
        pno  = int(pno)
        p2   = float(p2)
        p3   = float(p3)
        npts = int(npts)
        hw   = float(hw)
        expt = float(expt)
        fpf  = int(fpf)
        prepscan(PREFIX, pno)
        doo_scan(p2, p3, npts, hw, expt, fpf)
