print('load blc13300 ver 2')
prefix = 'b'
def blc13300_overnight():
    mgeig()
    fshtrigger()
    so()
    try:
        #gopos('PLA_sample_sub1_0000.json')
        #dn = f"sample_sub1_{prefix}"
        #print(dn)
        #newdataset(dn)
        #dkpatchmesh(1,199,0.5,99,0.04,1,1,5)
        #enddataset()

        gopos('PLA_sample_sub2_0001.json')
        dn = f"sample_sub2_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(2,399,1.5,299,0.04,1,1,5)
        enddataset()
        
        gopos('PLA_sample2_sub1_0002.json')
        dn = f"sample2_sub1_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(2,399,1.5,299,0.04,1,1,5)
        enddataset()

        gopos('PLA_sample2_sub2_0003.json')
        dn = f"sample2_sub2_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(1,199,0.5,99,0.04,1,1,5)
        enddataset()

        gopos('PLA_sample3_sub1_0004.json')
        dn = f"sample3_sub1_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(2,399,1.5,299,0.04,1,1,5)
        enddataset()

        gopos('PLA_sample4_sub1_0005.json')
        dn = f"sample4_sub1_{prefix}"
        print(dn)
        newdataset(dn)
        dkpatchmesh(2,399,1.5,299,0.04,1,1,5)
        enddataset()
            
        print('\n','run completed')
        sc()
    finally:
        enddataset()
        sc()
        sc()


