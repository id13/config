print('ls3045 night1 load: 3')

LAUNCH = 'c'

def make_dsname(posname, remov):
    if posname.endswith('.json'):
        pn = posname[:-5]
    elif posname.endswith('._pos'):
        pn = posname[:-5]
    else:
        pn = posname

    dsname = f'ds_{LAUNCH}_{pn}'
    dsname = dsname.replace(remov,'')
    return dsname

def do_one(posname, remov):
    dsname = make_dsname(posname, remov)
    newdataset(dsname)
    gopos(posname)
    kmap.dkmap(nnp2, 100 , 0    , 200   , nnp3, -100, 0   , 200, 0.02)
    kmap.dkmap(nnp2, 0   , -100 , 200   , nnp3, -100, 0   , 200, 0.02)
    kmap.dkmap(nnp2, 100 , 0    , 200   , nnp3, 0   , 100 , 200, 0.02)
    kmap.dkmap(nnp2, 0   , -100 , 200   , nnp3, 0   , 100 , 200, 0.02)
    enddataset()

def ls3045_night1_main():
#analf1_h_p11_ROI_1_0025.json

    poslist = """
tile03_h_p11_ROI_1_0025_0_0
tile03_h_p11_ROI_1_0025_0_1
tile03_h_p11_ROI_1_0025_0_2
tile03_h_p11_ROI_1_0025_1_0
tile03_h_p11_ROI_1_0025_1_1
tile03_h_p11_ROI_1_0025_1_2
tile03_h_p11_ROI_1_0025_2_0
tile03_h_p11_ROI_1_0025_2_1
tile03_h_p11_ROI_1_0025_2_2
analf1_h_p11_ROI_2_0026.json
analf1_h_p7_Cr_Ir_0021.json
analf1_h_p3_cr_0015.json
analf1_h_p5_cr_0017.json
analf1_h_p7_Cr_0020.json
    """.split()
    print (poslist)
    for pn in poslist:
        do_one(pn, 'analf1_')


def storethem():
    gopos('analf1_h_p11_ROI_1_0025.json')
    mvr(nny, -0.2, nnz, -0.2)
    start_y = nny.position
    start_z = nnz.position
    for j in range(3):
        curr_z = start_z + j*0.2
        mv(nnz, curr_z)
        for i in range(3):
            curr_y = start_y + i*0.2
            mv(nny, curr_y)
            print(f'{i},{j}: curr_y = {curr_y} curr_z = {curr_z}')
            stp(f'tile03_h_p11_ROI_1_0025_{i}_{j}')
