print('version ev480_pearl2_cube_ttomo_001 load 9')
import numpy as np, math
import time
import traceback

class Bailout(Exception): pass

ORI_INSTRUCT = """
# kappa,omega
0, 0.0
0, 3.444976076555024
0, 6.889952153110048
0, 10.334928229665072
0, 13.779904306220097
0, 17.22488038277512
0, 20.669856459330145
0, 24.11483253588517
0, 27.559808612440193
0, 31.004784688995215
0, 34.44976076555024
0, 37.89473684210527
0, 41.33971291866029
0, 44.78468899521531
0, 48.22966507177034
0, 51.674641148325364
0, 55.119617224880386
0, 58.56459330143541
0, 62.00956937799043
0, 65.45454545454545
0, 68.89952153110048
0, 72.34449760765551
0, 75.78947368421053
0, 79.23444976076556
0, 82.67942583732058
0, 86.1244019138756
0, 89.56937799043062
0, 93.01435406698565
0, 96.45933014354068
0, 99.9043062200957
0, 103.34928229665073
0, 106.79425837320575
0, 110.23923444976077
0, 113.6842105263158
0, 117.12918660287082
0, 120.57416267942584
0, 124.01913875598086
0, 127.4641148325359
0, 130.9090909090909
0, 134.35406698564594
0, 137.79904306220095
0, 141.244019138756
0, 144.68899521531102
0, 148.13397129186603
0, 151.57894736842107
0, 155.02392344497608
0, 158.4688995215311
0, 161.91387559808612
0, 165.35885167464116
0, 168.8038277511962
0, 172.2488038277512
0, 175.69377990430624
0, 179.13875598086125
0, 180.0
0, 176.555023923445
0, 173.11004784688996
0, 169.66507177033495
0, 166.2200956937799
0, 162.7751196172249
0, 159.33014354066987
0, 155.88516746411483
0, 152.44019138755982
0, 148.99521531100478
0, 145.55023923444978
0, 142.10526315789474
0, 138.66028708133973
0, 135.2153110047847
0, 131.7703349282297
0, 128.32535885167465
0, 124.88038277511963
0, 121.4354066985646
0, 117.99043062200958
0, 114.54545454545455
0, 111.10047846889952
0, 107.6555023923445
0, 104.21052631578948
0, 100.76555023923446
0, 97.32057416267943
0, 93.87559808612441
0, 90.43062200956939
0, 86.98564593301435
0, 83.54066985645933
0, 80.09569377990431
0, 76.65071770334929
0, 73.20574162679426
0, 69.76076555023924
0, 66.31578947368422
0, 62.87081339712919
0, 59.42583732057417
0, 55.980861244019145
0, 52.535885167464116
0, 49.09090909090909
0, 45.64593301435407
0, 42.20095693779905
0, 38.75598086124402
0, 35.311004784689
0, 31.866028708133975
0, 28.42105263157895
0, 24.976076555023926
0, 21.5311004784689
0, 18.086124401913878
0, 14.641148325358852
0, 11.196172248803828
0, 7.751196172248804
0, 4.30622009569378
0, 0.861244019138756
0, 0.0
0, 1.722488038277512
0, 5.167464114832536
0, 8.61244019138756
0, 12.057416267942585
0, 15.502392344497608
0, 18.947368421052634
0, 22.392344497607656
0, 25.837320574162682
0, 29.282296650717704
0, 32.72727272727273
0, 36.172248803827756
0, 39.61722488038278
0, 43.0622009569378
0, 46.50717703349282
0, 49.95215311004785
0, 53.397129186602875
0, 56.8421052631579
0, 60.28708133971292
0, 63.73205741626795
0, 67.17703349282297
0, 70.622009569378
0, 74.06698564593302
0, 77.51196172248804
0, 80.95693779904306
0, 84.4019138755981
0, 87.84688995215312
0, 91.29186602870814
0, 94.73684210526316
0, 98.18181818181819
0, 101.62679425837321
0, 105.07177033492823
0, 108.51674641148325
0, 111.96172248803829
0, 115.40669856459331
0, 118.85167464114834
0, 122.29665071770336
0, 125.74162679425838
0, 129.1866028708134
0, 132.63157894736844
0, 136.07655502392345
0, 139.52153110047848
0, 142.9665071770335
0, 146.41148325358853
0, 149.85645933014354
0, 153.30143540669857
0, 156.7464114832536
0, 160.19138755980862
0, 163.63636363636365
0, 167.08133971291866
0, 170.5263157894737
0, 173.9712918660287
0, 177.41626794258374
0, 180.0
0, 178.2775119617225
0, 174.8325358851675
0, 171.38755980861245
0, 167.94258373205741
0, 164.4976076555024
0, 161.05263157894737
0, 157.60765550239236
0, 154.16267942583733
0, 150.71770334928232
0, 147.27272727272728
0, 143.82775119617224
0, 140.38277511961724
0, 136.9377990430622
0, 133.4928229665072
0, 130.04784688995215
0, 126.60287081339713
0, 123.15789473684211
0, 119.71291866028709
0, 116.26794258373207
0, 112.82296650717704
0, 109.37799043062202
0, 105.933014354067
0, 102.48803827751196
0, 99.04306220095694
0, 95.59808612440192
0, 92.1531100478469
0, 88.70813397129187
0, 85.26315789473685
0, 81.81818181818183
0, 78.3732057416268
0, 74.92822966507177
0, 71.48325358851675
0, 68.03827751196172
0, 64.5933014354067
0, 61.14832535885168
0, 57.70334928229666
0, 54.25837320574163
0, 50.813397129186605
0, 47.36842105263158
0, 43.92344497607656
0, 40.47846889952153
0, 37.03349282296651
0, 33.588516746411486
0, 30.14354066985646
0, 26.698564593301437
0, 23.25358851674641
0, 19.80861244019139
0, 16.363636363636363
0, 12.918660287081341
0, 9.473684210526317
0, 6.028708133971293
0, 2.583732057416268
0, 0.0
"""

TEST_ORI_INSTRUCT = """
# kappa,omega
0, 0.0
0, 3.444976076555024
0, 6.889952153110048
0, 10.334928229665072
0, 13.779904306220097
0, 17.22488038277512
0, 20.669856459330145
0, 24.11483253588517
0, 27.559808612440193
"""

def generate_instruct_string(start_ome, stop_ome, nproj, nstrands):
    eps = 0.0001
    omegall = list(np.linspace(start_ome,stop_ome,nproj))
    print(omegall)
    llll = []
    for i in range(nstrands):
        sl = omegall[i::nstrands]
        if i % 2:
            sl.reverse()
        llll.append(sl)

    print(f'llll = {llll}')
    for i in range(nstrands):
        if i % 2:
            if math.fabs(llll[i][-1] - start_ome) < eps:
                pass
            else:
                llll[i].append(start_ome)
        else:
            if math.fabs(llll[i][-1] - stop_ome) < eps:
                pass
            else:
                llll[i].append(stop_ome)
    full = []
    for ll in llll:
        full.extend(ll)
    full_indexed = []
    for tp in enumerate(full):
        i,ome = tp
        full_indexed.append(tp)
    sll = []
    sll.append(f'')
    sll.append(f'# kappa,omega')
    sent = -1
    for tp in full_indexed:
        i, ome = tp
        print(sent, tp)
        if sent == ome:
            pass
        else:
            sll.append(f'0, {ome}')
        sent = tp[1]
    sll.append(f'')
    s = '\n'.join(sll)
    return s
        


def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (kap,ome) = l.split(',')
            ko = (int(kap), float(ome))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll


def dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=None):
    kmap.dkmap(nnp2,lly,uly,nitvy,nnp3,llz,ulz,nitvz,expt)

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo_eh3.log'):
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(kap,ome)) = instll_item
        self.log('... dummy ko trajectory')
        #self.zkap.trajectory_goto(ome, kap, stp=4)
        self.zkap.goto_okpos(ome)
        self.perform_scan()


    def perform_scan(self): 
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        lly *= 1.000
        uly *= 1.000
        llz *= 1.000
        ulz *= 1.000
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
        time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        print('++++++++++++++++++++++')
        print(f'instruct {instruct}')
        print('++++++++++++++++++++++')
        a , v = instruct[0]
        print('++++++++++++++++++++++')
        print(f'processing instruction: {a} {v}')
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t_bds = (lly, uly, llz, ulz) = tuple(map(int,w[1:5]))
                        fov_t_stps = (stpy, stpz) = tuple(map(float,w[5:]))
                        fov_t = fov_t_bds + fov_t_stps
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_corr(c_corr_t)
                    elif 'nnpcor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_nnpcorr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_c_nnpcorr(self, c_corr_t):
        print('applying nnp correction! ======================================== NNP!')
        print('pos before:')
        wnnp()
        (c_corrx, c_corry, c_corrz) = c_corr_t
        x = nnp1.position + c_corrx
        y = nnp2.position + c_corry
        z = nnp3.position + c_corrz
        mv(nnp1, x)
        mv(nnp2, y)
        mv(nnp3, z)
        print('pos after:')
        wnnp()
        self.log(f'nnpcorrection: {c_corr_t}')

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz, stpy, stpz) = fov_t

        lly = stpy*int(lly/stpy)
        uly = stpy*int(uly/stpy)
        llz = stpz*int(llz/stpz)
        ulz = stpz*int(ulz/stpz)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy/stpy)
        nitvz = int(dz/stpz)

        print('d', dy, dz)
        print('st',stpy,stpz)
        print('ni',nitvy,nitvz)
        print('ll',lly, uly, llz, ulz)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def ev480_test_main(zkap):
    print('Hi!')

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    # 117
    # 170
    scanparams = dict(
        lly = -12,
        uly = 12,
        nitvy = 240,
        llz = -12,
        ulz = 12,
        nitvz = 240,
        expt = 0.005,
        retveloc = 0.0
    )
    ttm = TTomo( 'asy.com', zkap, instll, scanparams)



    # loop over projections
    ttm.mainloop()
