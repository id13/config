import time, sys
print('cell16_run1 load 7')
START_KEY='d'


class Timeout(object):
    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()

def z_scan():
    dscan(ustrz,0,0.4,200,0.02)

def do_z_scan_set(cycle):
    newdataset(f'cell16_{cycle:03}_{START_KEY}')

    ref_ypos =  -11.55200
    ref_zpos =  1.39409-0.07
    delta_y_incr = 0.0025
    delta_y_space = 0.325
    for i in range(3):
        new_y_targetpos = ref_ypos + i*delta_y_space + cycle*delta_y_incr
        mv(ustry, new_y_targetpos)
        z_scan()
    
def do_cell16_run():

    ncycles            = 200
    clock_period       = 0.5
    upd_period         = 20.0
    cycle_period       = 360.0


    
    glob_to = Timeout(0)

    for i in range(ncycles):
        print(f'========================================== cycle {i}');sys.stdout.flush()
        try:
            local_to = Timeout(cycle_period)
            do_z_scan_set(i)

            to_detected = False
            upd_to = Timeout(upd_period)
            while not to_detected:
                (to_detected, spent_time) = local_to.check_timeout()
                (upd_flg, _tmp) = upd_to.check_timeout()

                if upd_flg:
                    print(f'time spent in cycle {i}: {spent_time}');sys.stdout.flush()
                    print(f'total time spent: {glob_to.get_delta()}');sys.stdout.flush()
                    print('\n');sys.stdout.flush()
                    upd_to.reset()
                time.sleep(clock_period)

        except KeyboardInterrupt:
            break

def cell16_run():
    try:
        do_cell16_run()
    finally:
        sc()
        sc()
