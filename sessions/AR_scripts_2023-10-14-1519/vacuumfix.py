# 2020/09/23
# copied from old id13ctrl / setup of vacuum-fix
#
import datetime
import time


def save_vacuum(_rv, _pir1, _pen1=None, _pir2=None, _pen2=None):
     
  st_short = 2
  st_long = 30
  
  cyc = 0
  while (1):
    dt = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    state_rv = _rv._tango_state
    print(f"{dt}  cycle: {cyc}    {_rv.name}: {state_rv}")
    
    if state_rv == "FAULT":
      print(f"{_rv._tango_status}")
      print(f"... {_rv.name}: RESETING")
      _rv.reset()

    else:
      if state_rv != "CLOSE":

        _vg = _pir1
        if _vg and (_vg._tango_state == "OFF"):
          print(f"... {_vg.name}: turning ON")
          _vg.setOn()
          time.sleep(st_short)
            
        _vg = _pen1
        if _vg and (_vg._tango_state == "OFF"):
          print(f"... {_vg.name}: turning ON")
          _vg.setOn()
          time.sleep(st_short)
          
        _vg = _pir2
        if _vg and (_vg._tango_state == "OFF"):
          print(f"... {_vg.name}: turning ON")
          _vg.setOn()
          time.sleep(st_short)
          
        _vg = _pen2
        if _vg and (_vg._tango_state == "OFF"):
          print(f"... {_vg.name}: turning ON")
          _vg.setOn()
          time.sleep(st_short)
            
        _rv.open()
        print(f"... {_rv.name}: OPENING")

      else:
        if state_rv != "OPEN":
          print(f"... {_rv.name}: UNKNOWN STATE: {state_rv}")
    

    sleep(st_long)
    cyc += 1
    


