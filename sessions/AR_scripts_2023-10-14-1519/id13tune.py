import sys
sys.path.insert(0, '/users/opid13/mcb/test_pypath')

from os import path
import numpy as np

import baseinfra

import metadata
import datah5ta
import sres
import xprofiles as xp

from bliss.setup_globals import *

def rgran(x,g):
    return round(x/g)*g

def rangstp_to_limitv(pos, reinge, stp, contained=False):
    ll = ul = 0.5*reinge
    return limstp_to_limitv(pos, ll, ul, stp, contained=contained)

def limstp_to_limitv(pos, ll, ul, stp, contained=False):
        if stp <= 0:
            raise ValueError("step size <= 0")
        absll = pos + ll
        absul = pos + ul
        
        g_pos = rgran(pos, stp)
        g_ll  = rgran(ll, stp)
        g_ul  = rgran(ul, stp)
        g_absll = g_pos + g_ll
        g_absul = g_pos + g_ul
        nitv = int(g_ldist/stp + g_udist/stp)
        if contained:
            if g_absll < absll:
                g_ll += stp
                nitv -= 1
            if g_absul > absul:
                g_ul -= stp
                nitv -= 1

        if nitv < 0:
            raise ValueError("no scan points")
        return (g_pos, g_ll, g_ul, nitv)


class Benv(object):

    def __init__(self):
        inimdata = dict(
            dname = path.normpath(path.join(
                    SCAN_SAVING.base_path,
                    SCAN_SAVING.template.format(session=SCAN_SAVING.session)
                ),
                fname = "%.h5" % SCAN_SAVING.data_filename
            )
        self.metad = metadata.BlissScanFile1(inimdata=inimdata)
        meta = metadata.make_mdview(self.metad)


class StdMaxTune(object):

    def __init__(self,
            mot = None,
            reinge = None,
            stp = None,
            expt = None,
            
        )
        self.mot = mot
        self.reinge = reinge
        self.stp = stp
        self.expt = expt

    def clone(self):
        return self.__class__(self.mot, self.reinge, self.stp, self.expt)

    def run(self):
        mot = self.mot
        pos = mot.position
        (g_pos, g_ll, g_ul, nitv) = rangstp_to_limitv(pos, self.reinge, self.stp, contained=False)
        mv(mot, g_pos)
        dscan(mot, g_ll, u_ll, nitv, self.expt)
        
        


