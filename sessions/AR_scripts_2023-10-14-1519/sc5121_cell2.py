import time
print('cell2_run1 load 11')
START_KEY='b'

def z_scan():
    dscan(ustrz,0,0.5,100,0.05)

def do_z_scan_set(cycle):
    newdataset(f'cell2_{cycle:03}_{START_KEY}')

    ref_ypos = -17.08000
    ref_zpos = -6.52781
    delta_y_incr = 0.03
    delta_y_space = 3.33333
    for i in range(3):
        new_y_targetpos = ref_ypos + i*delta_y_space + cycle*delta_y_incr
        mv(ustry, new_y_targetpos)
        z_scan()
    
def do_cell2_run():
    ncycles            = 100
    clock_period       = 120.0
    cycle_period       = 3600


    abs_start_t = time.time()
    print(f'abs start time: {abs_start_t}')
    for i in range(ncycles):
        try:
            print(f'cycle: {i} =====================================')
            rel_start_t = time.time()
            print(f'relative start time: {rel_start_t}')
            do_z_scan_set(i)
            while(True):
                time.sleep(clock_period)
                rel_report_t = time.time()-rel_start_t
                abs_report_t = time.time()-abs_start_t
                print(f'time: total    = {abs_report_t} {abs_report_t/3600}h')
                print(f'time: in cycle = {rel_report_t} {rel_report_t/3600}h')
                eval_time = time.time()-rel_start_t
                if eval_time > cycle_period:
                    break
                else:
                    print(f'{cycle_period-eval_time} left in this cycle')
                    print('type <CTRL><C> to interrupt ...')
                print('\n')
        except KeyboardInterrupt:
            break

def cell2_run():
    try:
        do_cell2_run() 
    finally:
        sc()
        sc()
