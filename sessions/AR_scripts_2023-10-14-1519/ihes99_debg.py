print('ihes99 - load 3')

start_key = 'dbg4'

def dooone(posname, resol=None):
    rstp(posname)
    dsname = posname[:-5]
    dsname = dsname + '_' + resol + '_' + start_key
    print('='*40)
    print('doing position', dsname)
    newdataset(dsname)
    zeronnp()
    umvr(nnp2, -10, nnp3, -10)
    if 'low' == resol:
        print(resol)
        kmap.dkmap(nnp2,-50,50,200,nnp3,-50,50,200,.02)
    elif 'high' == resol:
        print(resol)
        kmap.dkmap(nnp2,-30,30,600,nnp3,-30,30,600,.02)

BUFFER = """
OG50_mt2_1100C_pos2_50x_0030.json
OG50_mt2_1200C_pos1_50x_0031.json
OG50_mt2_1200C_pos2_50x_0032.json
OG50_mt2_1250C_pos1_50x_0033.json
OG50_mt2_1250C_pos2_50x_0034.json
"""
poslist = """
OG50_mt2_1100C_pos1_50x_0029.json
"""

def ihes99_prenight():
    try:
        so()
        x = poslist.strip()
        x = x.replace('\n', ' ')
        the_list = x.split()
        for the_item in the_list:
            dooone(the_item, resol='low')
            print('5 sec to interrupt ...')
            sleep(5)
            print("dont't interrupt any more")
        if 0:
            for the_item in the_list:
                dooone(the_item, resol='high')
                print('5 sec to interrupt ...')
                sleep(5)
                print("dont't interrupt any more")
    finally:
        sc()
        sc()
        sc()
