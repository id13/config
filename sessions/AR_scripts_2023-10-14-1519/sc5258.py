print('load 7')

def rot_scan_1():
    for int_angle in range(-35, 5, 5):
        print('='*40, int_angle)
        newdataset(f'rottest_1_th_{int_angle}p0')
        umv(usrotz, int_angle)
        print(usrotz.position, int_angle, 'as srot')
        dkmapyz_2(-0.02, 0.02, 40, -0.04, 0.04, 80, 0.02, retveloc=5)
        sync()
        elog_plot(scatter = True)
        sync()


def calibration_det():
    for det_pos in range(-930, -800, 10):
        print('='*40, det_pos)
        newdataset(f'series_calibration_Al2O3_detx{det_pos}p0_a')
        umv(udetx, det_pos)
        print(udetx.position, det_pos, 'as srot')
        dkmapyz_2(-0.05, 0.05, 50, -0.2, 0.2, 100, 0.01, retveloc=5)

def kmaps():
    dkmapyz_2(-0.075, 0.075, 300, -0.075, 0.075, 300, 0.02, retveloc=5)
    sync()
    elog_plot(scatter = True)
    sync()


def do_long_rotscan():
    start_key = 'a'
    rot_ll, rot_ul, rot_step = -40, 45, 5
    
    
    for samp in range(1,5):
        for pos_num in range(1,6):
            newdataset(f'r1_mem_{samp}_pos_{pos_num}_{start_key}')
            print('='*40)
            print(f'measuring sample # {samp} at position {pos_num}')
            pos_key = f'r_mem_{samp}_pos_{pos_num}'
            ggg.ggopos(pos_key, 'all')
            umvr(ustrz, 0.004, ustry, 0.004)
            print('pos_key = ', pos_key)
            for int_angle in range(rot_ll, rot_ul, rot_step):
                pass
                umv(usrotz, float(int_angle))
                sleep(1)
                dkmapyz_2(-0.02, 0.02, 80, -0.02, 0.02, 80, 0.02, retveloc = 5)
                sync()
                elog_plot(scatter = True)
                sync()


def kmaps_calc6():
    start_key = 'a'
    for pos in range(1, 3):
        pos_key = f'hec6_p_{pos}'
        newdataset(f'kmap_{pos_key}_{start_key}')
        gopos(pos_key)
        kmaps()
    for pos in range(1, 3):
        pos_key = f'hpc6_p_{pos}'
        newdataset(f'kmap_{pos_key}_{start_key}')
        gopos(pos_key)
        kmaps()

def the_real_thing():
    try:
        do_long_rotscan()
    finally:
        sc()
        sc()
        sc()

def do_long_rotscan_2():
    start_key = 'a'
    rot_ll, rot_ul, rot_step = -40, 45, 5
    
    
    for samp in range(1,5):
        for pos_num in range(1,6):
            newdataset(f'r2_mem_{samp}_pos_{pos_num}_{start_key}')
            print('='*40)
            print(f'measuring sample # {samp} at position {pos_num}')
            pos_key = f'r2_mem_{samp}_pos_{pos_num}'
            ggg.ggopos(pos_key, 'all')
            umvr(ustrz, 0.005, ustry, 0.001)
            print('pos_key = ', pos_key)
            for int_angle in range(rot_ll, rot_ul, rot_step):
                pass
                umv(usrotz, float(int_angle))
                sleep(1)
                dkmapyz_2(-0.015, 0.015, 60, -0.01, 0.01, 40, 0.02, retveloc = 5)
                sync()
                elog_plot(scatter = True)
                sync()



def hd_rotscan_friday():
    start_key = 'e'
    rot_ll, rot_ul, rot_step = -400, 401, 5

    for pos_num in range(1,3):
        newdataset(f'rot_hc2b_pos_{pos_num}_{start_key}')
        print('='*40)
        print(f'measuring at position {pos_num}')
        pos_key = f'hc2b_pos_{pos_num}'
        ggg.ggopos(pos_key, 'all')
        umvr(ustrz, 0.005)
        print('pos_key = ', pos_key)
        for i in range(rot_ll, rot_ul, rot_step):
            int_angle = i/10
            umv(usrotz, float(int_angle))
            sleep(1)
            dkmapyz_2(-0.015, 0.015, 60, -0.01, 0.01, 40, 0.02, retveloc = 5)
            sync()
            elog_plot(scatter = True)
            dooc(f'this is position {pos_num} at angle {int_angle}')
            sync()
                   
def hd_rotscan_weekend():
    try:
        start_key = 'b'
        rot_ll, rot_ul, rot_step = -400, 401, 5

        for pos_num in range(3,11):
            newdataset(f'rot_hc2b_pos_{pos_num}_{start_key}')
            print('='*40)
            print(f'measuring at position {pos_num}')
            pos_key = f'hc2b_pos_{pos_num}'
            ggg.ggopos(pos_key, 'all')
            umvr(ustrz, 0.007)
            print('pos_key = ', pos_key)
            for i in range(rot_ll, rot_ul, rot_step):
                int_angle = i/10
                umv(usrotz, float(int_angle))
                sleep(1)
                dkmapyz_2(-0.015, 0.015, 60, -0.01, 0.01, 40, 0.02, retveloc = 5)
                sync()
                elog_plot(scatter = True)
                dooc(f'this is position {pos_num} at angle {int_angle}')
                sync()
    finally:
        sc()
        sc()
        sc()


def thursday_night_rot():
    try:
        do_long_rotscan_2()
    finally:
        sc()
        sc()
        sc()

BUFF = '''
load_script('genstp')
xyzmxy_allmotnames = make_xyzmxy_allmotnames()
xyzmxy_motdc = make_xyzmxy_motdc()
xyzmxy_subsets = make_xyzmxy_subsets()
ggg = GenStorepos(xyzmxy_motdc, xyzmxy_allmotnames, xyzmxy_subsets)
'''
    
def kmaps_on_rotation1():
    pos_key = 'r_mem_2_pos_2'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps()
    
    pos_key = 'r_mem_2_pos_4'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps()
    
    pos_key = 'r_mem_3_pos_2'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps()
    
    pos_key = 'r_mem_3_pos_4'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps()
    

def kmaps_on_rotation2():
    for mem in range(1, 3):
        pos_key = f'r2_mem_{mem}_pos_3'
        newdataset(f'kmap_{pos_key}')
        ggg.ggopos(pos_key, 'all')
        kmaps()
    pos_key = 'r2_mem_3_pos_5'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps()   
    pos_key = 'r2_mem_4_pos_4'
    newdataset(f'kmap_{pos_key}')
    ggg.ggopos(pos_key, 'all')
    kmaps() 
    
    
def monday_night():
    pos = 1
    newdataset(f'oversampling_pos1_d')
    print(f'going to measure at position pos_1')
    umv(ustrx, -78.3269, ustry, 28.903 , ustrz, -9.2093)
    kmaps()
    
    pos = 2
    newdataset(f'oversampling_pos2_d')
    print(f'going to measure at position pos_2')
    umv(ustrx, -78.325, ustry, 29.119 , ustrz, -8.967)
    kmaps()
    
    pos = 3
    newdataset(f'oversampling_pos3_d')
    print(f'going to measure at position pos_3')
    umv(ustrx, -78.327, ustry, 28.044, ustrz, -9.339)
    kmaps()
    
    pos = 4
    newdataset(f'oversampling_pos4_d')
    print(f'going to measure at position pos_4')
    umv(ustrx, -78.326 , ustry, 27.688, ustrz, -9.078)
    kmaps()
    
    pos = 5
    newdataset(f'oversampling_pos5_d')
    print(f'going to measure at position pos_5')
    umv(ustrx, -78.323 , ustry, 27.4025, ustrz, -8.811)
    kmaps()
    
    pos = 6
    newdataset(f'oversampling_pos6_d')
    print(f'going to measure at position pos_6')
    umv(ustrx,  -78.326, ustry, 27.3175 , ustrz, -9.105)
    kmaps()
    
    pos = 7
    newdataset(f'oversampling_pos7_d')
    print(f'going to measure at position pos_7')
    umv(ustrx, -78.322, ustry, 27.0295, ustrz, -8.518)
    kmaps()
    
    pos = 8
    newdataset(f'oversampling_pos8_d')
    print(f'going to measure at position pos_8')
    umv(ustrx, -78.327, ustry, 27.015, ustrz, -9.214)
    kmaps()
    
    pos = 9
    newdataset(f'oversampling_pos9_d')
    print(f'going to measure at position pos_9')
    umv(ustrx, -78.318, ustry, 24.2005, ustrz, -8.736)
    kmaps()
    
    pos = 10
    newdataset(f'oversampling_pos10_d')
    print(f'going to measure at position pos_10')
    umv(ustrx, -78.324, ustry, 22.9025, ustrz, -8.972)
    kmaps()
    
    pos = 11
    newdataset(f'oversampling_pos11_d')
    print(f'going to measure at position pos_11')
    umv(ustrx, -78.317, ustry, 26.999, ustrz, -7.943)
    kmaps()
    
    sc()
    sc()
    sc()
