starting positions, only vy corrections, in focus

EH2 [673]: wm(ustrz, ustry, ustrx, umceny, umcenx)

              ustrz      ustry       ustrx     umceny    umcenx
--------  ---------  ---------  ----------  ---------  --------
User
 High       2.48429   69.32950   -41.71960   -1.20601   0.27499
 Current   -6.88200   26.87500  -122.72000   -1.57309   0.19338
 Low      -12.37740  -10.87050  -125.53030   -3.20601   0.07499
Offset    -12.00000  -11.90550   -44.99971   12.55565   1.26846

Dial
 High      14.48429   81.23500    -3.28011  -13.76167  -0.99347
 Current    5.11800   38.78050    77.72029  -14.12874  -1.07508
 Low       -0.37740    1.03500    80.53059  -15.76167  -1.19347


after 300micron ustrz down translation, umcenx corrections to be focus

              ustrz      ustry       ustrx     umceny    umcenx
--------  ---------  ---------  ----------  ---------  --------
User
 High       2.48429   69.32950   -41.71960   -1.20601   0.27499
 Current   -6.58200   26.87500  -122.72000   -1.57309   0.19839
 Low      -12.37740  -10.87050  -125.53030   -3.20601   0.07499
Offset    -12.00000  -11.90550   -44.99971   12.55565   1.26846

Dial
 High      14.48429   81.23500    -3.28011  -13.76167  -0.99347
 Current    5.41800   38.78050    77.72029  -14.12874  -1.07007
 Low       -0.37740    1.03500    80.53059  -15.76167  -1.19347


300 micron translation in ustrz needed a 5 micron correction in umcenx. z slope = 5/300 = 0.0167


for perov_day_burnscan (dmesh), position of the cross in microscope images (on micro_px):
y 375
z 186
(do not forget that images on micro_px and the ones saved are mirrored: left micro_px = right saved image. 
Apply correction to the readout of the Y values in px)
