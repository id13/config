print("macro ihsc1656.py")

def the_scan1():
    dkpatchmesh(0.4,199,0.4,199,0.02,1,1)
    
def the_scan2():
    dkpatchmesh(1.2,599,1.2,599,0.02,1,1)
    
def ihsc1656_submac():
    newdataset('Aref')
    gopos('Aref')
    the_scan1()
    
    newdataset('A50kc')
    gopos('A50kc')
    the_scan1()

    newdataset('A100kc')
    gopos('A100kc')
    the_scan1()

    newdataset('empty')
    gopos('empty')
    the_scan1()

    newdataset('Bref')
    gopos('Bref')
    the_scan1()

    newdataset('B50kc')
    gopos('B50kc')
    the_scan1()

    newdataset('B100kc')
    gopos('B100kc')
    the_scan1()

    newdataset('B200kc')
    gopos('B200kc')
    the_scan1()


def ihsc1656_mac():
    so()
    umv(udetx,-160)
    ihsc1656_submac()
    umv(udetx,-660)
    ihsc1656_submac()
    sc()
    sc()
    
def ihsc1656_mac2():
    so()
    umv(udetx,-660)
    ihsc1656_submac()

    newdataset('A100kc_2')
    gopos('A100kc')
    the_scan2()

    newdataset('B100kc_2')
    gopos('B100kc')
    the_scan2()

    sc()
    sc()

def ihsc1656_mac3():
    so()
    umv(udetx,-160)
    ihsc1656_submac()
    
    newdataset('A100kc_l')
    gopos('A100kc')
    the_scan2()

    newdataset('B100kc_l')
    gopos('B100kc')
    the_scan2()

    sc()
    sc()

