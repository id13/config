print('load md1288 ver 8')
prefix = 'b'

def md1288_july_23_day():
    mgeig()
    fshtrigger()
    so()
    try:
        gopos('mount04_M2_test_scan_start_2_0015.json')
        umv(ustry,-16.144)
        umv(ustrz,0.387)
        dn = f"M2_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(9.2/2,115*4,6.24/2,78*4,0.01,2,2,5)
        enddataset()
        
        #gopos('I_scan_start_pos')
        #dn = f"I_scan_{prefix}"
        ##print(dn)
        #newdataset(dn)
        #dkpatchmesh(5.28/2,66*4,8.96/2,112*4,0.01,2,2,5)
        #enddataset()
        #
        #gopos('M3_scan_start_pos')
        #dn = f"M3_scan_{prefix}"
        ##print(dn)
        #newdataset(dn)
        #dkpatchmesh(5.44/2,68*4,6.24/2,78*4,0.01,2,2,5)
        #enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()

step_size = 0.005   
ROI_scan = [
#'UD_ROI1','UD_ROI2',
'UD_ROI3',
'M2_ROI1','M2_ROI2','M2_ROI3',
'UE_ROI1','UE_ROI2','UE_ROI3',
]

ROI_start_pos_coord = [
#[12.316,2.268],[17.256,2.548],
[10.712,3.72],
[-15.502,1.507],[-16.143,3.387],[-10.372,0.867],
[-43.867,-9.601],[-45.32,-7.481],[-38.74,-8.8]
]

ROI_ninterval = [
#[200,208],[375,200],
[289,200],
[200,208],[225,200],[561,288],
[289,256],[323,200],[528,560],
]

roi_prefix = 'c' 

def md1288_july_23_high_resolution_scan():
    mgeig()
    fshtrigger()
    so()
    try:
        for num,_ in enumerate(ROI_scan):
            if 'UD_' in _:
                gopos('mount04_UD_test_scan_start_3_0021.json')
            elif 'M2_' in _:
                gopos('mount04_M2_test_scan_start_2_0015.json')
            elif 'UE_' in _:
                gopos('mount04_UE_test_scan_start_3_0022.json')
            else:
                print('focus of sample is not aligned')
                return
             
            umv(ustry,ROI_start_pos_coord[num][0])
            umv(ustrz,ROI_start_pos_coord[num][1])
            dn = f"{_}_{roi_prefix}"
            distance_y = step_size*ROI_ninterval[num][0]
            distance_z = step_size*ROI_ninterval[num][1]
            num_interval1 = ROI_ninterval[num][0]
            num_interval2 = ROI_ninterval[num][1]
                        
            newdataset(dn)
            dkpatchmesh(distance_y,num_interval1,
                        distance_z,num_interval2,
                        0.01,1,1,5)
            enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()

def md1288_july_23_night():
    #md1288_july_23_day()
    md1288_july_23_high_resolution_scan()
