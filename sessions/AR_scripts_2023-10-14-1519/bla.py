def ystp(stp):
    mvr(nny, stp)
    mv(Theta,-15)
    mv(Theta,15)
    mv(Theta,0)

def xstp(stp):
    mvr(nnx, stp)
    mv(Theta,-15)
    mv(Theta,15)
    mv(Theta,0)

def wnp():
    print(f' {nnp1.name:8s}  {nnp2.name:8s}  {nnp3.name:8s}')
    print(f'{nnp1.position:8.3f}  {nnp2.position:8.3f}  {nnp3.position:8.3f}')

def night1():
    try:
        kmap.dkmap(nnp2, 0, 20, 20*20, nnp3, 0, 20, 20*20, 0.05)
        kmap.dkmap(nnp2, -10, 30, 40*10, nnp3, -10, 30, 40*10, 0.05)
    finally:
        sc()
        sc()
        sc()

def spet_10th():
    try:
        gopos('mount01_2_a4_0002.json')
        umv(Theta,1.0)
        newdataset('mount01_2_a4_large_scan_1')
        kmap.dkmap(nnp2,-12,-2,100,nnp3,-20,0,200,0.05)
        
        gopos('mount01_2_a1_0003.json')
        umv(Theta,1.6)
        newdataset('mount01_2_a1_large_scan_1')
        kmap.dkmap(nnp2,-10,0,100,nnp3,-10,10,200,0.05)
        
        sc()
    finally:
        sc()
        sc()

def spet_10th_afternoon():
    try:
        gopos('mount03_km_top_0005.json')
        umv(Theta,-1)
        newdataset('mount03_top_large_scan')
        kmap.dkmap(nnp2,-7,3,100,nnp3,-20,20,400,0.05)
        
        gopos('mount03_km_bot_0004.json')
        umv(Theta,-4.4)
        newdataset('mount03_bot_large_scan')
        kmap.dkmap(nnp2,-7,3,100,nnp3,-10,30,400,0.05)
        
        sc()
    finally:
        sc()
        sc()

def spet_10th_night():
    try:
        gopos('mount04_90top_0000.json')
        umv(Theta,1.8)
        newdataset('mount04_90top_large_scan')
        kmap.dkmap(nnp2,-15,5,200,nnp3,-20,80,1000,0.05)
        
        gopos('mount04_90bot_0001.json')
        umv(Theta,3.1)
        newdataset('mount04_90bot_large_scan')
        kmap.dkmap(nnp2,-8,12,200,nnp3,-20,100,1200,0.05)
        
        sc()
    finally:
        sc()
        sc()
