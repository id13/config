from os import path
from math import pi, fabs
from collections import OrderedDict as Odict
import shutil
import json


RPD = pi/180.0
DPR = 180.0/pi

def mv_safe(ax, pos):
    for i in range(5):
        try:
            print(f'safe mv {ax.name} to {pos}')
            umv(ax, pos)
            return
        except:
            print(f'mv_safe err: {i}')
            sync()

class PMot(object):

    MOTDC = dict()

    def __init__(self, basemot, sign=1, scale=0.0, offset=0.0, tolerance=0.001):
        if basemot.name in self.MOTDC:
            raise KeyError('name already in use')
        else:
            self.basemot = basemot
            self.MOTDC = basemot
        self.sign = sign
        self.scale = scale
        self.offset = offset
        self.tolerance = tolerance

    def set_zero(self, tgt, src=None):
        if None is src:
            src = self.position()
        self.offset = self.offset + tgt - src

    @property
    def position(self):
        bpos = self.basemot.position
        vpos = self.sign*self.scale*bpos + self.offset
        return vpos

    @position.setter
    def position(self, vpos):
        bpos = (vpos - self.offset)/(self.sign*self.scale)
        mv_safe(self.basemot, bpos)

    def make_axis(self, name):
        return SoftAxis(name, self, position='position', move=f'position', tolerance=self.tolerance)

def make_motdc():
    motdc = Odict(
        phi = PMot(smxb, sign=1, scale=0.001, offset=0.0, tolerance=0.1),
        kap = PMot(smyb, sign=1, scale=0.001, offset=0.0, tolerance=0.1),
        x = PMot(nnx, sign=1, scale=1.0, offset=0.0, tolerance=0.002),
        y = PMot(nny, sign=1, scale=1.0, offset=0.0, tolerance=0.002),
        z = PMot(nnz, sign=1, scale=1.0, offset=0.0, tolerance=0.002)
    )
    return motdc


class Zkap(object):

    THEID = 0
    ORDER = 'phi kap x y z'.split()

    def __init__(self, mot_dc, arm=True):
        self.mot_dc = mot_dc
        self.ax_dc = Odict()
        if arm == True:
            print('creating axis set ...')
            for k, v in self.mot_dc.items():
                self.ax_dc[k] = v.make_axis(k)
        else:
            print('explicit axis initialization ...')
            self.ax_dc['phi'] = phi
            self.ax_dc['kap'] = kap
            self.ax_dc['x'] = x
            self.ax_dc['y'] = y
            self.ax_dc['z'] = z

        self.order = self.ORDER

        self.c_amp = self.ampx = self.ampy = None
        self.c_corrx = self.c_corry = self.c_corrz = 0.0
        self.set_phi_offset(0)
        self.reset_kap_korr()
        self.kap_corr_table[0] = (0.0, 0.0, 0.0)


    # geo
    # c_corrx, c_corry, corrz  outer correction to be applied e.g. during scan series
    # ref_XL, ref_XH  sampcen at phi = 0 (positive most x pos)
    # kap_corr_table = 
    #      kap_key : (kap_val, x_val, z_val)
    #      ...
    # phi_offset
    #
    # ampx, ampy, c_amp = 0.5*(ampx + ampy)
    # 
    #

    def xget_kap_corr(self, kap):
        key = round(kap)
        flk = float(key)
        if fabs(key - flk) > 0.1:
            raise ValueError('illegal kappa value: {kap}')
        return self.kap_corr_table[key]

    def tgt_xyz(self, ome, kap):
        rad_ome = RPD*ome
        rad_kap = RPD*kap
        (kap_val, kap_corrx, kap_corrz) = self.xget_kap_corr(kap)
        x = self.c_amp*cos(rad_ome)*cos(rad_kap) + self.c_cenx_k0 + kap_corrx + self.c_corrx
        y = self.c_amp*sin(rad_ome) + self.c_ceny_k0 + self.c_corry
        # print(f'cs = {self.c_amp*cos(rad_ome)*sin(-rad_kap)}')
        # print(f'self.c_cenz_k0 = {self.c_cenz_k0}')
        # print(f'kap_corrz = {kap_corrz}')
        # print(f'self.c_corrz = {self.c_corrz}')

        z = self.c_amp*cos(rad_ome)*sin(-rad_kap) + self.c_cenz_k0 + kap_corrz + self.c_corrz
        # print(f'z = {z}')
        return (x,y,z)

    def correct_xyz(self, ome=None, kap=0):
        if None == ome:
            ome = self.get_ome()
        xyz = self.tgt_xyz(ome, kap)
        self.mv_trans(xyz)

    def goto_okpos(self, ome, kap=0):
        self.mv_ome(ome)
        self.mv_kap(kap)
        self.correct_xyz(ome, kap)

    def nearest_kappa(self, req_kap):
        diff = 10000.0
        for k in self.kap_corr_table:
            cur_diff = fabs(k-req_kap)
            if cur_diff < diff:
                nearest_k = k
                diff = cur_diff
        return nearest_k

    def __str__(self):
        ll = []
        ll.append('# zkap: ########')
        ll.append(f'zkap.c_amp = {self.c_amp}')
        ll.append(f'zkap.phi_offset = {self.phi_offset}')
        ll.append(f'zkap.c_cenx_k0  = {self.c_cenx_k0}')
        ll.append(f'zkap.c_ceny_k0  = {self.c_ceny_k0}')
        ll.append(f'zkap.c_cenz_k0  = {self.c_cenz_k0}')
        ll.append(f'zkap.c_corrx    = {self.c_corrx}')
        ll.append(f'zkap.c_corry    = {self.c_corry}')
        ll.append(f'zkap.c_corrz    = {self.c_corrz}')
        ll.append(f'zkap.kap_corr_table = {self.kap_corr_table}')
        return '\n'.join(ll)

    def show(self):
        print(self)


    def trajectory_goto(self, tgt_ome, tgt_kap=0, stp=10):
        cur_ome = self.get_ome()
        posdc = self.get_pos_dict()
        cur_kap = posdc['kap']
        dome = tgt_ome - cur_ome
        dkap = tgt_kap - cur_kap
        npts = int(fabs(dome)/stp + 2)
        npts_kap = int(2*fabs(dkap)/stp + 2)
        npts = max(npts, npts_kap)
        lsp = np.linspace(cur_ome, tgt_ome, npts)
        lsp_kap = np.linspace(cur_kap, tgt_kap, npts)
        print(f'npts = {npts}')
        for i, (ome, fl_kap) in enumerate(zip(lsp, lsp_kap)):
            kap = float(self.nearest_kappa(fl_kap))
            print(f'\nwaypoint: {i}')
            print(f'ome = {ome}')
            print(f'fl_kap = {fl_kap}')
            print(f'kap = {kap}')
            self.goto_okpos(ome, kap)

    def zero_ome(self, val=0):
        posdc = self.get_pos_dict()
        self.phi_offset = posdc['phi'] - val

    def get_ome(self, phi_pos=None):
        if None is phi_pos:
            posdc = self.get_pos_dict()
            phi_pos = posdc['phi']
        return phi_pos - self.phi_offset

    def mv_ome(self, ome_pos):
        phi_pos = ome_pos + self.phi_offset
        self.mv_phi(phi_pos)

    def mvr_ome(self, delta_ome_pos):
        ome_pos = self.get_ome()
        self.mv_ome(ome_pos + delta_ome_pos)

    def mv_phi(self, phi_pos):
        mv_safe(self.ax_dc['phi'], phi_pos)

    def mv_kap(self, kap_pos):
        mv_safe(self.ax_dc['kap'], kap_pos)

    def mvr_phi(self, delta_phi_pos):
        posdc = self.get_pos_dict()
        phi_pos = posdc['phi']
        mv_safe(self.ax_dc['phi'], phi_pos + delta_phi_pos)

    def mv_trans(self, xyz):
        x,y,z = xyz
        mv_safe(self.ax_dc['x'], x)
        mv_safe(self.ax_dc['y'], y)
        mv_safe(self.ax_dc['z'], z)

    def set_x_cen(self, lposkey, hposkey):
        lpos = self.load_posdc(lposkey)
        hpos = self.load_posdc(hposkey)
        self.c_cenx_k0 = 0.5*(hpos['x'] + lpos['x'])
        print(f'c_cenx_k0 = {self.c_cenx_k0}')

    def set_y_cen(self, lposkey, hposkey):
        lpos = self.load_posdc(lposkey)
        hpos = self.load_posdc(hposkey)
        self.c_ceny_k0 = 0.5*(hpos['y'] + lpos['y'])

    def set_x_amp(self, lposkey, hposkey):
        lpos = self.load_posdc(lposkey)
        hpos = self.load_posdc(hposkey)
        self.ampx = 0.5*(hpos['x'] - lpos['x'])
        print(f'ampx = {self.ampx}')

    def set_y_amp(self, lposkey, hposkey):
        lpos = self.load_posdc(lposkey)
        hpos = self.load_posdc(hposkey)
        self.ampy = 0.5*(hpos['y'] - lpos['y'])
        print(f'ampy = {self.ampy}')

    def set_c_amp(self, mode='both'):
        self.amp_mode = None
        if 'both' == mode:
            self.c_amp = 0.5*(self.ampx + self.ampy)
        elif 'x' == mode:
            self.c_amp = self.ampx
        elif 'y' == mode:
            self.c_amp = self.ampy
        else:
            raise ValueError
        self.amp_mode = mode
        print(f'ampy = {self.c_amp}')

    def set_z_cen(self, lposkey, hposkey):
        lpos = self.load_posdc(lposkey)
        hpos = self.load_posdc(hposkey)
        self.c_cenz_k0 = 0.5*(hpos['z'] + lpos['z'])

    def set_phi_offset(self, phi_offset):
        self.phi_offset = phi_offset

    def reset_kap_korr(self):
        self.kap_corr_table =Odict()

    def add_kap_corr(self, kapkey, baseposkey, poskey=None):
        basedc = self.load_posdc(baseposkey)
        if None is poskey:
            posdc = self.get_pos_dict()
        else:
            posdc = self.load_posdc(poskey)
        dx = posdc['x'] - basedc['x']
        dz = posdc['z'] - basedc['z']
        self.kap_corr_table[kapkey] = (posdc['kap'], dx, dz)
        


    # pos logistics

    def zvsti(self):
        new_id = self.make_id()
        new_fname = self.make_idfname(new_id)
        print(new_fname)
        posdc = self.get_pos_dict()
        if path.exists(new_fname):
            raise IOError(f'illegal attempt to overwrite position {new_fname}')
        else:
            with open(new_fname, 'w') as f:
                json.dump(posdc, f)

    def stp(self):
        self.zvsti()

    def rstpall(self,x):
        self.zvrst(x, 'all')

    def make_idfname(self, someid):
        fname = f'zk_{someid:04d}.jsn'
        return fname

    def make_fname(self, x):
        try:
            x.endswith
            fname = f'{x}.jsn'
        except AttributeError:
            fname = self.make_idfname(x)
        return fname

    def make_id(self):
        new_id = self.THEID + 1
        self.THEID = new_id
        return new_id

    def get_pos_dict(self):
        motdc = self.mot_dc
    
        positems = [(vmname,motdc[vmname].position) for vmname in motdc.keys()]
        return dict(positems)

    def load_posdc(self, x):
        fname = self.make_fname(x)
        with open(fname, 'r') as f:
            posdc = json.load(f)
        return posdc

    def print_pos(self, posdc, x='<default>'):
        print(f'zvpos: {x}')
        for vmname in self.order:
            print(f'    {vmname:<5} = {posdc[vmname]}')
        
    def show_pos(self, x):
        posdc = self.load_posdc(x)
        self.print_pos(posdc, x)

    def zvrst(self, x, rstkeys):
        if 'all' == rstkeys:
            rstkeys = 'phi kap x y z'
        posdc = self.load_posdc(x)
        try:
            rstkeys.upper
            rks = rstkeys.split()
        except AttributeError:
            rks = rstkeys
        self.show_pos(x)
        for k in rks:
            ax = self.ax_dc[k]
            print(type(posdc[k]))
            print(f'mv_safe({k}, {posdc[k]})')
            mv_safe(ax, posdc[k])

    def assign(self, sk, tk):
        sk_fname = self.make_fname(sk)
        tk_fname = self.make_fname(tk)
        shutil.copyfile(sk_fname, tk_fname)
