import numpy as np
print('knife_series 06')

def detect_edges(knife_mot,knife_hrange=100,knife_hstep=0.5,expt=0.02,corner='fe'):
    
    motorname = knife_mot.name
    if knife_mot.name=='nnp2':
        sign = -1
    elif knife_mot.name in ['nnp3','nny','nnz']:
        sign = 1
    else:
        raise ValueError('illegal motor for knife edge scan %s' % motorname)
        
    knife_nstep = int(np.ceil(knife_hrange / knife_hstep))
    ll = sign*(-knife_nstep*knife_hstep)
    ul = sign*knife_nstep*knife_hstep
    start_knife = knife_mot.position
    success = False
    try:
        dscan(knife_mot,ll,ul,2*knife_nstep,expt)
        gofe()
        fe_pos = knife_mot.position
        gore()
        re_pos = knife_mot.position
        diff = sign * (re_pos-fe_pos)
        assert diff > 0
        gocorner(corner)
        success = True
    finally:
        if success:
            print('success position adapted', knife_mot.position)
        else:
            mv(knife_mot,start_knife)
            print('failure returning original position',knife_mot.position)
            
def detect_edge(knife_mot,knife_hrange=100,knife_hstep=0.5,expt=0.02,corner='fe',ofst=False):
    
    motorname = knife_mot.name
    if knife_mot.name=='nnp2':
        sign = -1
    elif knife_mot.name in ['nnp3','nny','nnz']:
        sign = 1
    else:
        raise ValueError('illegal motor for knife edge scan %s' % motorname)
        
    knife_nstep = int(np.ceil(knife_hrange / knife_hstep))
    if ofst:
        ll = sign*(-knife_nstep*knife_hstep) + sign*knife_nstep*knife_hstep
        ul = sign*knife_nstep*knife_hstep + sign*knife_nstep*knife_hstep
    else:
        ll = sign*(-knife_nstep*knife_hstep)
        ul = sign*knife_nstep*knife_hstep

    start_knife = knife_mot.position
    success = False
    try:
        plotinit(ct32)
        dscan(knife_mot,ll,ul,2*knife_nstep,expt)
        gocorner(corner)
        success = True
    finally:
        if success:
            print('success position adapted', knife_mot.position)
            return(knife_mot.position)
            mv(knife_mot,start_knife)
        else:
            print('failure returning original position',knife_mot.position)
            return(1000)
            mv(knife_mot,start_knife)

def hexapod_find_cross(des_pos_y=0.005,des_pos_z=0.005,corner='fe',ofst=True):
  
    zeronnp()
    
    edgepos = []
    alsteps = []
    '''
    coarse step
    '''
    hxpedge_y = detect_edge(nny,knife_hrange=0.5,knife_hstep=0.01,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_y==1000:
        raise ValueError('did not find the falling edge along hexapod y at coarse step')
    else:
        mv(nny,hxpedge_y-0.05)
    
    hxpedge_z = detect_edge(nnz,knife_hrange=0.5,knife_hstep=0.01,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_z==1000:
        raise ValueError('did not find the falling edge along hexapod z at coarse step')
    else:
        mv(nnz,hxpedge_z-0.05)
    edgepos.append([hxpedge_y,hxpedge_z])
    alsteps.append(['coarse step', '10um'])
    
    '''
    medium step
    '''
    mv(nny,hxpedge_y-0.1)
    hxpedge_y = detect_edge(nny,knife_hrange=0.1,knife_hstep=0.002,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_y==1000:
        raise ValueError('did not find the falling edge along hexapod y at medium step')
    else:
        mv(nny,hxpedge_y-0.005)
        
    mv(nnz,hxpedge_z-0.1)
    hxpedge_z = detect_edge(nnz,knife_hrange=0.1,knife_hstep=0.002,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_z==1000:
        raise ValueError('did not find the falling edge along hexapod z at mdeium step')
    else:
        mv(nnz,hxpedge_z-0.005)
    edgepos.append([hxpedge_y,hxpedge_z])
    alsteps.append(['medium step', '2um'])
    
    '''
    small step
    '''
    mv(nny,hxpedge_y-0.01)
    hxpedge_y = detect_edge(nny,knife_hrange=0.01,knife_hstep=0.0002,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_y==1000:
        raise ValueError('did not find the falling edge along hexapod y at small step')
    else:
        mv(nny,hxpedge_y-0.005)
        
    mv(nnz,hxpedge_z-0.01)
    hxpedge_z = detect_edge(nnz,knife_hrange=0.01,knife_hstep=0.0002,expt=0.02,corner=corner,ofst=ofst)
    if hxpedge_z==1000:
        raise ValueError('did not find the falling edge along hexapod z at small step')
    else:
        mv(nnz,hxpedge_z-0.005)
    edgepos.append([hxpedge_y,hxpedge_z])
    alsteps.append(['small step', '200nm'])
    
    mv(nny,hxpedge_y-des_pos_y)
    mv(nnz,hxpedge_z-des_pos_z)
    edgepos.append([nny.position,nnz.position])
    
    for i in range(len(alsteps)):
        print('the used %s of %s and the found edges positions are (nny,nnz): ' % (alsteps[i][0], alsteps[i][1]), tuple(edgepos[i]))
    print('finally moved to position (nny,nnz): ', tuple(edgepos[-1]))

def gocorner(corner):
    if corner == 'fe':
        gofe()
    elif corner =='re':
        gore()
    else:
        raise ValueError

def knife_series(xmot,xstep,nsteps,knife_mot,knife_hrange,knife_hstep,expt,corner='fe'):
    
    xmotname = xmot.name
    if xmot.name not in ('nnx','Theta'):
        raise ValueError('illegal motor for knife edge scan %s' % xmotname) 
    motorname = knife_mot.name
    if knife_mot.name=='nnp2':
        sign = -1
    elif knife_mot.name=='nnp3':
        sign = 1
    else:
        raise ValueError('illegal motor for knife edge scan %s' % motorname)
        
    knife_nstep = int(np.ceil(knife_hrange / knife_hstep))
    ll = sign*(-knife_nstep*knife_hstep)
    ul = sign*knife_nstep*knife_hstep
    start_x = xmot.position
    start_knife = knife_mot.position
    scannos = []
    xpos = []
    try:
        for i in range(nsteps):
            new_x = start_x + i*xstep
            print('new_x=',new_x,' cycle=', i)
            mv(xmot,new_x)
            dscan(knife_mot,ll,ul,2*knife_nstep,expt)
            scannos.append(SCANS[-1].scan_number)
            xpos.append(new_x)
            gocorner(corner)
    finally:
        mv(xmot,start_x)
        mv(knife_mot,start_knife)
        for i, (sn, xp) in enumerate(zip(scannos,xpos)):
            print(i, sn, xp)

