print('load 2')
START_KEY = 'a'
def sc5189_cc262():
    try:
        so()
        newdataset(f'cc262_5x_roi01_0054_{START_KEY}')
        gopos('cc262_5x_roi01_0054.json')
        dkmapyz_2(-0.135,0.135,540,-0.11,0.11,440,0.005)

        newdataset(f'cc262_5x_roi02_0055_{START_KEY}')
        gopos('cc262_5x_roi02_0055.json')
        dkmapyz_2(-0.15,0.15,600,-0.14,0.14,280,0.01)

        newdataset(f'cc262_5x_roi03_0056_{START_KEY}')
        gopos('cc262_5x_roi03_0056.json')
        dkmapyz_2(-0.075,0.075,300,-0.075,0.075,300,0.015)

        newdataset(f'cc262_5x_roi04_0057_{START_KEY}')
        gopos('cc262_5x_roi04_0057.json')
        dkmapyz_2(-0.07,0.07,280,-0.075,0.075,300,0.02)
    finally:
        sc()
        sc()
        sc()
