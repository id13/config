def safety_net():
    print('===============')
    for i in range(5):
        interrupt = 5 - i
        print(f'{interrupt} sec to interrupt kmap')
        sleep(1)
    print('TOO LATE - Wait for the next kmap')
    print('===============')

# macro for dinner 25/01, ptycho scans at different phase plate positions
def do_fp_pp_scan():
    fshtrigger()
    spiral_dscan(nnp2, nnp3, 0.04, 10000, 0.05)
    safety_net
    kmap.dkmap(nnp3, -3, 3, 100, nnp2, -3, 3, 100, 0.05)
    safety_net
    kmap.dkmap(nnp3, -2, 2, 100, nnp2, -2, 2, 100, 0.05)
    safety_net
def do_fp_pp_scan_quick():
    fshtrigger()
    kmap.dkmap(nnp3, -3, 3, 100, nnp2, -3, 3, 100, 0.05)
    fshtrigger()
    spiral_dscan(nnp2, nnp3, 0.04, 10000, 0.05)
    safety_net
def do_fp_nopp_scan_quick():
    fshtrigger()
    kmap.dkmap(nnp3, -1, 1, 100, nnp2, -1, 1, 100, 0.05)
    fshtrigger()
    spiral_dscan(nnp2, nnp3, 0.04, 10000, 0.05)
    safety_net

# example
def do_dinner_25jan():
    newdataset('pp_comp')
    #quick ptycho for all positions on the modulator
    i=6
    mv_num_pp(i)
    do_fp_nopp_scan_quick()
    for i in range(1,6):
        mv_num_pp(i)
        print('=========')
        print(f'doing ptycho scans with pp in position {i}')
        do_fp_pp_scan_quick()
    i=6
    mv_num_pp(i)
    do_fp_nopp_scan_quick()
    #now reproducibility check
    newdataset('pp_rep')
    mv_num_pp(5)
    print('=========')
    print(f'doing ptycho scans with pp in position {i}')
    do_fp_pp_scan_quick()
    do_fp_pp_scan_quick()
    do_fp_pp_scan_quick()
    do_fp_pp_scan_quick()
    mv_num_pp(6)
    print('=========')
    print(f'doing ptycho scans with pp in position {i}')
    mv_num_pp(5)
    do_fp_pp_scan_quick()
    mv_num_pp(6)
    print('=========')
    print(f'doing ptycho scans with pp in position {i}')
    mv_num_pp(5)
    do_fp_pp_scan_quick()
    mv_num_pp(6)
    print('=========')
    print(f'doing ptycho scans with pp in position {i}')
    mv_num_pp(5)
    do_fp_pp_scan_quick()
    sc()


def do_night_29jan():
    newdataset('pp_comp_2')
    so()
    for i in range(1,6):
        mv_num_pp(i)
        print('=========')
        print(f'doing ptycho scans with pp in position {i}')
        do_fp_pp_scan_quick()
    i=6
    mv_num_pp(i)
    do_fp_nopp_scan_quick()
    sc()
    


def do_magic():
    try:
        so()
        newdataset('pp_bcdi_run1')
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        
        
    finally:
        sc()
        sc()
        sc()


def do_magic_fine():
    try:
        so()
        newdataset('pp_bcdi_second_e')
        fshtrigger()
        #theta backlash corr
        theta_start = 108.1
        theta_incr = 0.002
        bp_pos = 108.204
        max_steps = 101
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        #umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,0.005)
        
        
    finally:
        sc()
        sc()
        sc()
        
        
        
def do_magic_fast():
    try:
        so()
        newdataset('pp_bcdi_second_f')
        fshtrigger()
        #theta backlash corr
        theta_start = 108.1
        theta_incr = 0.002
        bp_pos = 108.204
        max_steps = 101
        exp_time_pp = 0.0035
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        #umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,0.005)
        
        
    finally:
        sc()
        sc()
        sc()

def do_magic_loopscan_big():
    try:
        so()
        loop_repeat = 100
	#iterater through time points
        for uu in range (0,loop_repeat):
            print('loop cycle %03d' %uu) 
        # create dataset name based on time point
            newdataset('pp_bcdi_rad_exp_a_%03d' %uu)
            fshtrigger()
        #theta backlash corr
            theta_start = 108.114
            theta_incr = 0.008
            bp_pos = 108.21
            max_steps = 24
            exp_time_pp = 0.04
            theta_back = theta_start -1
            umv(Theta, theta_back)
            umv(Theta, theta_start)
            #umv(nnp2,116.686,nnp3,116.901)
            print('Now for some PP BCDI')
            for ii in range(0,max_steps):
                theta_go = ii*theta_incr+theta_start
                print(theta_go)
                umv(Theta, theta_go)
                dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
                ## BP 2D
                #outside of the loop
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-2.5,2.5,50,nnp3,-0.5,0.5,50,0.005)
        fshtrigger()

        
        
    finally:
        sc()
        sc()
        sc()
        

def do_magic_loopscan_small():
    try:
        so()
        loop_repeat = 70
	#iterater through time points
        for uu in range (0,loop_repeat):
            print('loop cycle %03d' %uu) 
        # create dataset name based on time point
            newdataset('pp_bcdi_rad_exp_c_%03d' %uu)
            fshtrigger()
        #theta backlash corr
            theta_start = 108.114
            theta_incr = 0.008
            bp_pos = 108.21
            max_steps = 24
            exp_time_pp = 0.04
            theta_back = theta_start -1
            umv(Theta, theta_back)
            umv(Theta, theta_start)
            #umv(nnp2,116.686,nnp3,116.901)
            print('Now for some PP BCDI')
            for ii in range(0,max_steps):
                theta_go = ii*theta_incr+theta_start
                print(theta_go)
                umv(Theta, theta_go)
                dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
                ## BP 2D
                #outside of the loop
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-2.5,2.5,50,nnp3,-0.5,0.5,50,0.005)
        fshtrigger()

        
        
    finally:
        sc()
        sc()
        sc()

def do_long_magic():
    try:
        so()
        newdataset('pp_bcdi_run_repeat')
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #theta backlash corr
        theta_start = 108.0
        theta_incr = 0.003
        bp_pos = 108.204
        max_steps = 151
        exp_time_pp = 0.03
        theta_back = theta_start -1
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        umv(nnp2,116.686,nnp3,116.901)
        print('Now for some PP BCDI')
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()
        #move to the small structure
        umv(nnp2,114.945,nnp3,113.845)
        umv(Theta, theta_back)
        umv(Theta, theta_start)
        for ii in range(0,max_steps):
            theta_go = ii*theta_incr+theta_start
            print(theta_go)
            umv(Theta, theta_go)
            dmesh(nnp2,-0.14,0.14,1,nnp3,-0.035,0.035,1,exp_time_pp)
        ## BP 2D
        umv(Theta, theta_back)
        umv(Theta, bp_pos)
        print('Now for some 2D bragg ptycho')
        kmap.dkmap(nnp2,-3.75,3.75,75,nnp3,-0.75,0.75,75,exp_time_pp-0.005)
        fshtrigger()

        
        
        
    finally:
        sc()
        sc()
        sc()
    
        
    
