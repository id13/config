from prompt_toolkit import PromptSession
from prompt_toolkit.key_binding import KeyBindings
import aiogevent

class DummyMotor(object):

    def __init__(self, inipos=0.0):


class Dummy(object):

    STEP_SIZES = {
        '1' : 0.1,
        '2' : 0.05,
        '3' : 0.02,
        '4' : 0.01,
        '5' : 0.005,
        '6' : 0.002,
        '7' : 0.001,
        '8' : 0.0005
    }

    def prn_error(k, event, errmsg):
        event.app.

    def set_stepsize(self, k, event):
        try:

    def get_stepsize
        

    

def my_prompt_app_focus1(dummy):
    bindings = KeyBindings()

    

    @bindings.add("i")
    def _(event):
        event.app.current_buffer.insert_text("Hello, world !")

    @bindings.add("1")
    def _(event):
        dummy.step_size = dummy.STEP_SIZES["1"]

    @bindings.add("2")
    def _(event):
        dummy.step_size = dummy.STEP_SIZES["2"]

    @bindings.add("i")
    def _(event):
        mvr(nnz, dummy.step_size)

    @bindings.add("k")
    def _(event):
        mvr(nnz, -dummy.step_size)

    @bindings.add("j")
    def _(event):
        mvr(nny, dummy.step_size)

    @bindings.add("l")
    def _(event):
        mvr(nny, -dummy.step_size)

    @bindings.add("e")
    def _(event):
        event.app.exit()

    session = PromptSession()

    return aiogevent.yield_future(session.prompt_async("> ", key_bindings=bindings))

# END
