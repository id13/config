print('karl_safe_scan_example load - 1')
import gevent,bliss, time

def simple_safe_kmap(pmot1, ll1, uli1, n1, pmot2, ll2, ul2, n2, expt, frames_per_file=200, timeout=None):
    if None is timeout:
        timeout = n1*n2*expt*1.15 + 40
    print(f'simple_safe_kmap: timeout={timeout}')
    try:
        t0 = time.time()
        with gevent.Timeout(seconds=timeout):
            kmap.dkmap(pmot1, ll1, uli1, n1, pmot2, ll2, ul2, n2, expt,
                       frames_per_file=frames_per_file)
            fshtrigger()
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print(f'caught hanging scan after {time.time() - t0} seconds timeout')
        return 1
    return 0

def safe_kmap(pmot1, ll1, uli1, n1, pmot2, ll2, ul2, n2, expt,
    frames_per_file=200, timeout=None, probeonly=False):
    if None is timeout:
        timeout = n1*n2*expt*1.5 + 60
    print(f'safe_kmap: timeout={timeout}')
    try:
        t0 = time.time()
        with gevent.Timeout(seconds=timeout):
            eig_hws_kmap(pmot1, ll1, uli1, n1, pmot2, ll2, ul2, n2, expt,
                       frames_per_file=frames_per_file, probeonly=probeonly)
            fshtrigger()
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print(f'caught hanging scan after {time.time() - t0} seconds timeout')
        return 1
    return 0

def safe_meig():
    mgeig()
    eigerhws(True)

def safe_meig_x():
    mgeig()
    eigerhws(True)
    MG_EH3a.enable('*xmap*')
