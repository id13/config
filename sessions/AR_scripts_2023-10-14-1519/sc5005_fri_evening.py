print ("sc5005_fri_evening loading ... 5")

from bliss.setup_globals import *

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    gopos(x)
    newdataset('DS_a_%s' % s)
    fshtrigger()

def WAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 0.0
    dist2 = 15.0

    mvr(nnp2,-dist1)
    fshtrigger()
    kmap.dkmap(nnp3, -38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2,-dist2)
    fshtrigger()
    dmesh(nnp3, -38, 38, 760 , nnp2, 0, -8, 16, 0.2)
    
def SAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 760 , nnp2, 0, -4, 8, 0.05)
    
def WAXS_nephila(posstr):
    zeronnp()

    mv(nnp3, 146)

    tgopos(posstr)

    dist1 = 0.0
    dist2 = 17.0

    mvr(nnp2, -dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, -dist2)
    fshtrigger()
    dmesh(nnp3,-30, 30, 600 , nnp2, 0, -10, 20, 0.2)
    
def SAXS_nephila(posstr):
    zeronnp()

    mv(nnp3, 146)

    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0
    
    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 600 , nnp2, 0, -5, 10, 0.05)


POS = """
"""

def sc5005_main_eve2_pos3():



    so()
    try:

        WAXS_nephila('mount4_x50_M23Ar_pos3_0029.json')


        umv(ndetx, 300)

        SAXS_nephila('mount4_x50_M23Ar_pos3_0029.json')


        
        sc()
    finally:
        sc()
        sc()
        sc()
