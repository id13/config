print('ihsc1654 ver 12')
prefix = 'g'
def dry_sample_scan(scan_list):
    try:
        for i,_ in enumerate(scan_list): 
            gopos(_)
            dn = f"{_[:-10]}"
            fshtrigger()
            so()
            newdataset(dn)
            dkmapyz(-0.05,0.05,29,-0.05,0.05,29,0.05,frames_per_file=900)
            enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()

#scan_list = [
#'dry_sample_HT_cross_section_a_pos1_0012.json',
#'dry_sample_HT_cross_section_a_pos2_0013.json',
#'dry_sample_HT_cross_section_a_pos3_0014.json',
#'dry_sample_HT_cross_section_a_pos4_0015.json',
#'dry_sample_HT_cross_section_a_pos5_0016.json',
#'dry_sample_HT_cross_section_a_pos6_0017.json',
#'dry_sample_HT_fiber_2__a_pos1_0002.json',
#'dry_sample_HT_fiber_2__a_pos2_0003.json',
#'dry_sample_Ion_fiber_1__a_pos1_0006.json',
#'dry_sample_Ion_fiber_1__a_pos2_0007.json',
#'dry_sample_Vis_fiber_1__a_pos1_0005.json',
#'dry_sample_Vis_fiber_1__a_pos2_0004.json',
#]

#######################
hum_sp_ls_new = [30,40,50,60,70,80]

#pos_list = [
#'hum_sample_HT_fiber1_a_p01_0min_0034.json',
#'hum_sample_HT_fiber1_a_p01_10min_0035.json',
#'hum_sample_HT_fiber1_a_p01_30_min_0033.json',
#'hum_sample_HT_fiber1_a_p01_60min_0036.json',
#'hum_sample_HT_fiber1_a_p02_0min_0037.json',
#'hum_sample_HT_fiber1_a_p02_10min_0038.json',
#'hum_sample_HT_fiber1_a_p02_30min_0039.json',
#'hum_sample_HT_fiber1_a_p02_60min_0040.json',
#'hum_sample_HT_fiber1_a_p03_0min_0041.json',
#'hum_sample_HT_fiber1_a_p03_10min_0042.json',
#'hum_sample_HT_fiber1_a_p03_30min_0043.json',
#'hum_sample_HT_fiber1_a_p03_60min_0044.json',
#]
pos_list = [
'hum_sample_Vis_fiber2_a_p01_0min_0045.json',
'hum_sample_Vis_fiber2_a_p01_10min_0046.json',
'hum_sample_Vis_fiber2_a_p01_30min_0047.json',
'hum_sample_Vis_fiber2_a_p01_60min_0048.json',
'hum_sample_Vis_fiber2_a_p02_0min_0049.json',
'hum_sample_Vis_fiber2_a_p02_10min_0050.json',
'hum_sample_Vis_fiber2_a_p02_30min_0051.json',
'hum_sample_Vis_fiber2_a_p02_60min_0052.json',
'hum_sample_Vis_fiber2_a_p03_0min_0053.json',
'hum_sample_Vis_fiber2_a_p03_10min_0054.json',
'hum_sample_Vis_fiber2_a_p03_30min_0055.json',
'hum_sample_Vis_fiber2_a_p03_60min_0056.json',
'hum_sample_Vis_fiber2_a_p04_0min_0057.json',
'hum_sample_Vis_fiber2_a_p04_10min_0058.json',
'hum_sample_Vis_fiber2_a_p04_30min_0059.json',
'hum_sample_Vis_fiber2_a_p04_60min_0060.json',
'hum_sample_Vis_fiber2_a_p05_0min_0064.json',
'hum_sample_Vis_fiber2_a_p05_10min_0063.json',
'hum_sample_Vis_fiber2_a_p05_30min_0062.json',
'hum_sample_Vis_fiber2_a_p05_60min_0061.json',
'hum_sample_Vis_fiber2_a_p06_0min_0065.json',
'hum_sample_Vis_fiber2_a_p06_10min_0066.json',
'hum_sample_Vis_fiber2_a_p06_30min_0067.json',
'hum_sample_Vis_fiber2_a_p06_60min_0068.json',

]
def hum_scan_new(hum_sp_ls_new):
    try:
        for (i,_) in enumerate(hum_sp_ls_new):
            humobj.set_hum(_)
            check_hum = humobj.hum_is_reached()
            while not check_hum:
                humobj.set_hum(_)
                check_hum = humobj.hum_is_reached()
            print('wait for sample to adapt to humidity environment')
            p = "p{:02d}".format(i+1)
            print(p)
            pl  = []
            for m in pos_list:
                if (p in m) & ('Vis_fiber2' in m):
                    pl.append(m)
            print(pl)
            print('\n\n\n')
            for n in ['_0min_','_10min_','_30min_','_60min_']:
                if n == '_0min_':
                    #pass
                    sleep(100)
                elif n == '_10min_':
                    sleep(600)
                elif n == '_30min_':
                    sleep(1200)
                elif n == '_60min_':
                    sleep(1800)
                for __ in pl:
                    if n in __:
                        dn = f"{__[:-10]}_hum_{_}_{prefix}"
                        gopos(__)
                        print('\n',dn,'\n')
                        mgeig()
                        fshtrigger()
                        so()
                        newdataset(dn)
                        dkmapyz(-0.05,0.05,29,-0.05,0.05,29,0.05,frames_per_file=900)
                        enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()



def july_18_afternoon():
    hum_scan_new(hum_sp_ls_new)

def july_18_night():
    hum_scan_new(hum_sp_ls_new)

'''
#scan_list = [
#'hum_sample_fiber1_c_p01_0023.json',
#'hum_sample_fiber2_l5_p01_0024.json',
#'hum_sample_fiber3_l3_p01_0025.json',
#'hum_sample_fiber4_l1_p01_0026.json',
#]

#hum_sp_ls = ['85_1','85_2','85_3']
#hum_sp_ls = [80,30]
#pos_list = [
#'hum_sample_Vis_fiber1_a_p01_0027.json', 
#'hum_sample_Vis_fiber1_a_p02_0028.json',
#'hum_sample_Vis_fiber1_a_p03_0029.json',
#'hum_sample_Vis_fiber1_b_p01_0030.json',
#'hum_sample_Vis_fiber1_b_p02_0031.json',
#'hum_sample_Vis_fiber1_b_p03_0032.json',
#]

def hum_scan(hum_sp_ls):
    try:
        for (i,_) in enumerate(hum_sp_ls):
            humobj.set_hum(_)
            check_hum = humobj.hum_is_reached()
            while not check_hum:
                humobj.set_hum(_)
                check_hum = humobj.hum_is_reached()
            print('wait for sample to adapt to humidity environment')
            #if i == 0:
            #    pass
            #else:
            sleep(3600)
            #gopos('empty')
            #newdataset(f"empty_hum_{_}_{prefix}")
            #dkmapyz(-0.02,0.02,19,-0.02,0.02,19,0.1,frames_per_file=400)
            #enddataset()
            p = "p{:02d}".format(i+2)
            print(p)
            pl  = []
            for __ in pos_list:
                if p in __:
                    pl.append(__)
            print(pl)
            print('\n\n\n')
            for __ in pl:
                dn = f"{__[:-10]}_hum_{_}_{prefix}"
                gopos(__)
                print('\n',dn,'\n')
                mgeig()
                fshtrigger()
                so()
                newdataset(dn)
                dkmapyz(-0.05,0.05,29,-0.05,0.05,29,0.05,frames_per_file=900)
                enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()
def july_17_afternoon_run():
    #dry_sample_scan(scan_list)
    hum_scan(hum_sp_ls)

def july_17_night_run():
    #dry_sample_scan(scan_list)
    hum_scan(hum_sp_ls)
'''
