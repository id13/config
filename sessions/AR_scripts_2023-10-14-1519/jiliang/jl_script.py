def uct_log(t=None):
    #diode counting
    if isinstance(t,type(None)):
        t = 1
    else:
        t = t
    INGAIN  = float(umoco.comm("?INBEAM").split()[3])
    OUTGAIN = float(umoco.comm("?OUTBEAM").split()[3])
    sct(t)
    uct32 = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
    uct34 = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct34'][0]
    OUTBEAM = uct32*OUTGAIN*215*1e7/1e11*1/t
    INBEAM  = uct34*INGAIN*215*1e7/1e11*1/t
    print(OUTBEAM,INBEAM)
    inbeam  = "counting intensity of inbeam %6.3f 1E11" %INBEAM
    outbeam = "counting intensity of outbeam %6.3f 1E11" %OUTBEAM
    lotoo.elog_info("ID13log:\n"+inbeam+"\n"+outbeam)
  
def nct_log(t=None):
    if isinstance(t,type(None)):
        t = 1
    else:
        t = t
    INGAIN  = float(nmoco.comm("?INBEAM").split()[3])
    OUTGAIN = float(nmoco.comm("?OUTBEAM").split()[3]) 
    sct(t)
    nct32 = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
    nct34 = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct34'][0]
    OUTBEAM = nct32*OUTGAIN*215*1e7/1e11*1/t
    INBEAM  = nct34*INGAIN*215*1e7/1e11*1/t
    print(OUTBEAM,INBEAM)
    inbeam  = "counting intensity of inbeam %6.3f 1E11" %INBEAM
    outbeam = "counting intensity of outbeam %6.3f 1E11" %OUTBEAM
    lotoo.elog_info("ID13log:\n"+inbeam+"\n"+outbeam)

