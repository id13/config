print('sc5404 ver 12')
prefix = 'g'
def sample_scan():
    try:
        sp_list = [30,35,40,45,50,55,60,65,70,75,80]
        for i in range(len(sp_list)): 
            umv(uhum_sp,sp_list[i])
            #if i == 0:
            #    pass
            #else:
            sleep(600)
            so()
            gopos(HT_list[i])
            dn = f"{HT_list[i]}"
            fshtrigger()
            dqmgeig()
            newdataset(dn)
            dkmapyz_2(-0.25,0.25,199,-0.125,0.125,99,0.03,
                      frames_per_file=200,retveloc=5)
            enddataset()
            gopos(ION_list[i])
            dn = f"{ION_list[i]}"
            fshtrigger()
            newdataset(dn)
            dkmapyz_2(-0.25,0.25,199,-0.125,0.125,99,0.03,
                      frames_per_file=200,retveloc=5)
            enddataset()
            gopos(VIS_list[i])
            dn = f"{VIS_list[i]}"
            fshtrigger()
            newdataset(dn)
            dkmapyz_2(-0.25,0.25,199,-0.125,0.125,99,0.03,
                      frames_per_file=200,retveloc=5)
            enddataset()
            #gopos('empty')
            #dn = f"empty_new_{i+1}"
            #fshtrigger()
            #newdataset(dn)
            #dkmapyz(-0.01,0.01,7,-0.01,0.01,7,0.03,frames_per_file=200)
            #enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()

def March_4th_night_run2():
    sample_scan()

HT_list = [
'DEacetylated_pos1',
'DEacetylated_pos2',
'DEacetylated_pos3',
'DEacetylated_pos4',
'DEacetylated_pos5',
'DEacetylated_pos6',
'DEacetylated_pos7',
'DEacetylated_pos8',
'DEacetylated_pos9',
'DEacetylated_pos10',
'DEacetylated_pos11',
]

ION_list = [
'Holocellulose_pos1',
'Holocellulose_pos2',
'Holocellulose_pos3',
'Holocellulose_pos4',
'Holocellulose_pos5',
'Holocellulose_pos6',
'Holocellulose_pos7',
'Holocellulose_pos8',
'Holocellulose_pos9',
'Holocellulose_pos10',
'Holocellulose_pos11',
]

VIS_list = [
'true_acetylate_pos1',
'true_acetylate_pos2',
'true_acetylate_pos3',
'true_acetylate_pos4',
'true_acetylate_pos5',
'true_acetylate_pos6',
'true_acetylate_pos7',
'true_acetylate_pos8',
'true_acetylate_pos9',
'true_acetylate_pos10',
'true_acetylate_pos11',
]

def March_6th_night_run2():
    sample_scan()

def simple_scan():
    try:
        for i in range(2): 
            so()
            gopos(HT_list[i])
            dn = f"{HT_list[i]}"
            fshtrigger()
            dqmgeig()
            newdataset(dn)
            dkmapyz(-0.1,0.1,79,-0.1,0.1,79,0.03,frames_per_file=200)
            enddataset()
            gopos(ION_list[i])
            dn = f"{ION_list[i]}"
            fshtrigger()
            newdataset(dn)
            dkmapyz(-0.1,0.1,79,-0.1,0.1,79,0.03,frames_per_file=200)
            enddataset()
            gopos(VIS_list[i])
            dn = f"{VIS_list[i]}"
            fshtrigger()
            newdataset(dn)
            dkmapyz(-0.1,0.1,79,-0.1,0.1,79,0.03,frames_per_file=200)
            enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()



def March_5th_day_run2():
    simple_scan()

scan_list = [
'HT_pos1',
'HT_pos2',
'ION_pos1',
'ION_pos2',
'VIS_pos1',
'VIS_pos2',
]

def serries_scan():
    try:
        for i in range(len(scan_list)): 
            so()
            gopos(scan_list[i])
            dn = f"{scan_list[i]}_{prefix}"
            fshtrigger()
            dqmgeig()
            newdataset(dn)
            dkmapyz_2(-0.1,0.1,79,-0.1,0.1,79,0.03,
                    frames_per_file=200,retveloc=5)
            enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()
