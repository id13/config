print('load v9')

test_pos1 = [
#'mount02_D1_test_pos_0010.json',
#'#mount02_D1_test_pos_0010.json',
'mount05_F7_test1_0011.json',
'mount05_G3_test1_0003.json',
]

test_pos2 = [
'mount05_F7_test2_0012.json',
'mount05_G3_test2_0004.json',
]


pos = [
#'ALAC_10min_SONIC_pos1_0000.json',
#'ALAC_10min_SONIC_pos2_0001.json',
#'ALAC_10min_SONIC_pos3_0002.json',
#'ALAC_10min_SONIC_pos4_0003.json',

#'mount02_D1_pos1_0006.json',
#'mount02_D1_pos2_0007.json',
#'mount02_D1_pos3_0008.json',
#'mount02_D5_pos1_0009.json',
#'mount02_D5_pos2_0004.json',
#'mount02_D5_pos3_0005.json',

#'mount03_A5_pos1_0007.json',
#'mount03_A5_pos2_0008.json',
#'mount03_A5_pos3_0009.json',
#'mount03_G1_pos1_0004.json',
#'mount03_G1_pos2_0005.json',
#'mount03_G1_pos3_0006.json',
#'mount03_G2_pos1_0010.json',
#'mount03_G2_pos2_0011.json',
#'mount03_G2_pos3_0012.json',

#'mount04_MYX1_pos1_0023.json',
#'mount04_MYX1_pos2_0024.json',
#'mount04_MYX1_pos3_0025.json',
#'mount04_MYX2_pos1_0020.json',
#'mount04_MYX2_pos2_0021.json',
#'mount04_MYX2_pos3_0022.json',
#'mount04_MYX3_pos1_0026.json',
#'mount04_MYX3_pos2_0027.json',
#'mount04_MYX3_pos3_0028.json',
#'mount04_MYX4_pos1_0029.json',
#'mount04_MYX4_pos2_0030.json',
#'mount04_MYX4_pos3_0031.json',


'mount05_F6_pos1_0005.json',
'mount05_F6_pos2_0006.json',
'mount05_F6_pos3_0007.json',
'mount05_F7_pos1_0008.json',
'mount05_F7_pos2_0009.json',
'mount05_F7_pos3_0010.json',
'mount05_G3_pos1_0000.json',
'mount05_G3_pos2_0001.json',
'mount05_G3_pos3_0002.json',
]
keys = 'j'
#hum_sp = [45,52.5,60,65,70,75,80]
#hum_sp = [70,75,80]
#hum_sp = [75,80]
#hum_sp = [80,75,70,65,60,52.5,45]

def run_hum_increase_scan():
    try:
        so()
        mgeig()
        #hum_sp = [45,52.5,60,65,70,75,80]
        hum_sp = [65,70,75,80]
        for _ in range(len(hum_sp)):
            umv(uhum_sp,hum_sp[_])
            if _ == 0:
                pass
            else:
                sleep(600)
            for __ in range(len(pos)):
                gopos(pos[__])
                fn = pos[__][:-10]
                if 'MYX1' in fn:
                    lm = 0.55
                else:
                    lm = 0.5
                invt = int(2*lm/0.002)
                newdataset(f'{fn}_hum{int(hum_sp[_])}_increase_{keys}')
                umvr(ustrz,0.005*(_+3))
                dkmapyz_2(-1*lm,lm,invt,-0.0075,0.0075,0,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_decrease_scan():
    try:
        so()
        mgeig()
        hum_sp = [80,75,70,65,60,52.5,45]
        #hum_sp = [70,65,60,52.5,45]
        for _ in range(len(hum_sp)):
            umv(uhum_sp,hum_sp[_])
            if _ == 0:
                pass
            else:
                sleep(1800)
            for __ in range(len(pos)):
                gopos(pos[__])
                fn = pos[__][:-10]
                if 'MYX1' in fn:
                    lm = 0.55
                else:
                    lm = 0.5
                invt = int(2*lm/0.002)
                newdataset(f'{fn}_hum{int(hum_sp[_])}_decrease_{keys}')
                umvr(ustrz,0.005*(_+7))
                dkmapyz_2(-1*lm,lm,invt,-0.0075,0.0075,0,0.01,retveloc=5)
                #dkmapyz_2(-1*lm,lm,invt,-0.2,0.2,0,0.01,retveloc=5)
                #dkmapyz_2(-0.4,0.4,400,-0.2,0.2,0,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan_test1():
    try:
        so()
        mgeig()
        hum_sp = [45]
        for _ in range(2):
            umv(uhum_sp,hum_sp[0])
            if _ == 0:
                pass
            else:
                sleep(1800)
            for __ in range(len(test_pos1)):
                gopos(test_pos1[__])
                fn = test_pos1[__][:-10]
                newdataset(f'{fn}_hum{_}_{keys}')
                dkmapyz_2(-0.4,0.4,400,-0.1,0.1,100,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan_test2():
    try:
        so()
        mgeig()
        hum_sp = [80]
        for _ in range(2):
            umv(uhum_sp,hum_sp[0])
            if _ == 0:
                pass
            else:
                sleep(1800)
            for __ in range(len(test_pos2)):
                gopos(test_pos2[__])
                fn = test_pos2[__][:-10]
                newdataset(f'{fn}_hum{_}_{keys}')
                dkmapyz_2(-0.4,0.4,400,-0.1,0.1,100,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan_23rd_night():
    run_hum_increase_scan()
    run_hum_decrease_scan()
    run_hum_scan_test1()
    run_hum_scan_test2()
    
def run_hum_scan_22nd_night_test():
    try:
        so()
        mgeig()
        for _ in range(2):
            umv(uhum_sp,hum_sp[_])
            if _ == 0:
                pass
            else:
                sleep(300)
            for __ in range(len(test_pos)):
                gopos(pos[__])
                fn = pos[__][:-10]
                newdataset(f'{fn}_hum{_}_increase_{keys}')
                umvr(ustrz,0.005*(_))
                dkmapyz_2(-0.4,0.4,400,-0.2,0.2,0,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan_22nd_night1():
    try:
        so()
        mgeig()
        for _ in range(len(hum_sp)):
            umv(uhum_sp,hum_sp[_])
            #if _ == 0:
            #    pass
            #else:
            sleep(600)
            for __ in range(len(pos)):
                gopos(pos[__])
                fn = pos[__][:-10]
                newdataset(f'{fn}_hum{hum_sp[_]}_increase_{keys}')
                umvr(ustrz,0.005*(_+3))
                dkmapyz_2(-0.4,0.4,400,-0.2,0.2,0,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan_22nd_night2():
    try:
        so()
        mgeig()
        for _ in range(len(hum_sp)):
            umv(uhum_sp,hum_sp[_])
            if _ == 0:
                pass
            else:
                sleep(3600)
            for __ in range(len(pos)):
                gopos(pos[__])
                fn = pos[__][:-10]
                newdataset(f'{fn}_hum{hum_sp[_]}_increase_{keys}')
                umvr(ustrz,0.005*(_+7))
                dkmapyz_2(-0.4,0.4,400,-0.2,0.2,0,0.01,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def run_hum_scan():
    try:
        #print(1)
        so()
        mgeig()
        for _ in hum_sp:
            if _ == 60:
                pass
            else:
                umv(uhum_sp,_)
                if _ == 65:
                    pass
                else:
                    sleep(400)
                #print(2)
                for __ in range(len(pos)):
                    gopos(pos[__])
                    newdataset(f'hum{_}_pos{int(__+1)}_{keys}')
                    if __ < 2:
                        umvr(ustry,0.015*(__+1))
                        umvr(ustrz,0.2)
                        dscan(ustrz,-0.25,0.25,200,0.02)
                        umvr(ustry,0.005)
                        dscan(ustrz,-0.25,0.25,200,0.02)
                    else:
                        umvr(ustry,0.015*(__+2))
                        umvr(ustrz,0.15)
                        dscan(ustrz,-0.2,0.2,160,0.02)
                        umvr(ustry,0.005)
                        dscan(ustrz,-0.2,0.2,160,0.02)
        sc()
    finally:
        sc()
        sc()
