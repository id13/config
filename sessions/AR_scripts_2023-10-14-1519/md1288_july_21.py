print('load md1288 ver 4')
prefix = 'c'
def md1288_july_21_day():
    mgeig()
    fshtrigger()
    so()
    try:
        gopos('K2_scan_start_pos')
        dn = f"K2_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(10.4/2,130*4,7.68/2,96*4,0.01,2,2,5)
        enddataset()
        
        gopos('UE_scan_start_pos')
        dn = f"UE_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(9.6/2,120*4,6.72/2,84*4,0.01,2,2,5)
        enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()
    
def md1288_july_21_night():
    mgeig()
    fshtrigger()
    so()
    try:
        gopos('K1_scan_start_pos')
        dn = f"K1_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(5.76/2,72*4,6.72/2,84*4,0.01,2,2,5)
        enddataset()
        
        gopos('LC_scan_start_pos')
        dn = f"LC_scan_{prefix}"
        #print(dn)
        newdataset(dn)
        dkpatchmesh(5.44/2,68*4,6.4/2,80*4,0.01,2,2,5)
        enddataset()
        
        print('\n','run completed')
        sc()
    finally:
        sc()
        sc()
