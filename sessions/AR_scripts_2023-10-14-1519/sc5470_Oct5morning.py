import bliss
import gevent

START_KEY = 'm'

print('sc5470 - Manar load 14')
l_bb = [
#'PNinterlock_scalefine_0002.json',
#'PNinterlock_scale_0001.json'
]


l_b = [
'PNinterlock_lock_0000.json'
]

l_s = [
]

def kmap_with_timeout(
    start_p2,  end_p2, npoints2, start_p3, end_p3, npoints3, expt, timeout="auto",
    frames_per_file=200*10,
):
    # change start_p2 and end_p2 to relative position instead of absolute position
    #mv(nnp2, start_p2)
    #mv(nnp3, start_p3)
    if "auto" == timeout:
        timeout = npoints2 * npoints3 * (expt + 0.0007) * 1.2 + 40
    print(f"patch kmap: timeout={timeout}")
    t0 = time.time()
    try:
        with gevent.Timeout(seconds=timeout):
            kmap.dkmap(
                nnp2, start_p2, end_p2, npoints2, nnp3, start_p3, end_p3, npoints3, expt, 
                frames_per_file=frames_per_file
            )
            fshtrigger()

    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print(f"caught hanging scan after {time.time() - t0} seconds timeout")

def timeout_test():
    sync()
    zeronnp()
    mgeig_x()
    eigerhws(True)
    newdataset(f'{START_KEY}_timeout_test')
    for _ in range(2):
        #kmap.dkmap(nnp2, -100,100,200, nnp3, -100,100,200,0.01, frames_per_file=200*10)
        kmap_with_timeout(-10,10,40,-10,10,40,0.01, 
                          frames_per_file=1600)

def sc5470_night():
    so()
    #sync()
    mgeig_x()
    eigerhws(True)

    START_KEY = 'm'
    #print('\n test \n')
    for _ in l_bb:
        #print(f'name for bb is {_[:-5]}')
        sync()
        zeronnp()
        newdataset(f'{START_KEY}_{_[:-5]}_bb')
        rstp(_)
        #umvr(nny,0.06,nnz,-0.1)
        #kmap.dkmap(nnp2, -100,100,200, nnp3, -100,100,200,0.01, frames_per_file=200*10)
        kmap_with_timeout(-60,60,480, -60,60,480,0.02, 
                          frames_per_file=480*10)
    #print('\n test \n')
    for _ in l_b:
        #print(f'name for 5 ms is {_[:-5]}')
        sync()
        zeronnp()
        newdataset(f'{START_KEY}_{_[:-5]}_b')
        rstp(_)
        #umvr(nny,0.06,nnz,-0.1)
        #kmap.dkmap(nnp2, -100,100,200, nnp3, -100,100,200,0.02, frames_per_file=200*10)
        kmap_with_timeout(-100,100,400, -100,100,400,0.02, 
                          frames_per_file=400*10)
#print('\n test \n')
    for _ in l_s:
        #print(f'name for 5 ms is {_[:-5]}')
        sync()
        zeronnp()
        newdataset(f'{START_KEY}_{_[:-5]}_s')
        rstp(_)
        #umvr(nny,0.06,nnz,-0.1)
        #kmap.dkmap(nnp2, -100,100,200, nnp3, -100,100,200,0.02, frames_per_file=200*10)
        kmap_with_timeout(-40,40,160, -40,40,160,0.02,
                          frames_per_file=160*10)

def sc5470_night_main():
    try:
        sc5470_night()
        sc()
    finally:
        sc()
        sc()
