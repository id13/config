print ("mac15 start")
def the_scan():
    dscan(ustrz,-0.08,0.08,80,0.1)

def the_mesh1():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.4)
    
def the_mesh2():
    dmesh(ustrz,-0.1,0.1,80,ustry,-0.2,0.2,4,0.4)
    
def the_mesh4():
    dmesh(ustrz,-0.5,0.5,100,ustry,-0.5,0.5,2,0.4)
    
def the_mesh3():
    dmesh(ustrz,-1,1,10,ustry,-0.4,0.4,10,0.1)
    
def in1127_mac15():
    so()
    fshtrigger()
    mgeig()

        
    for i in range(43,85):
        pos_name = "NOV2205_f02_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[8:])      
        the_scan()
        
    for i in range(38,43):
        pos_name = "NOV2205_f02_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[8:])      
        the_mesh3()
        

print ("mac15")
