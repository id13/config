#from bliss.setup_globals import *
print("v5 load successfully")
def run_test():
    try:
        fshtrigger()
        so()
        kmap.dkmap(nnp2,60,-60,600,nnp3,-60,60,600,0.01,frames_per_file=2000)
        sc()
        sc()
    finally:
        sc()
        sc() 
        sc()
        sc() 

def BS_calib():
    start_pos = ndety.position
    print('ndety position: %f' %start_pos)
    pos_step = 0.15
    mvr(ndety,-4.05)
    fshtrigger()
    try:
        so()
        for i in range(55):
            mvr(ndety,pos_step)
            time.sleep(1)
            loopscan(50,0.1)
        sc()
        umv(ndety,start_pos)
    finally:
        umv(ndety,start_pos)
        sc()
        sc()
        sc()
        
def lunch_marco():
    try:
        so()
        
        fshtrigger()
        kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,0.05)
        
        fshtrigger()
        kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,0.05)
        
        fshtrigger()
        dmesh(nnp3,-3,3,120,nnp2,3,-3,40,0.05)
        
        fshtrigger()
        dmesh(nnp3,-3,3,120,nnp2,3,-3,40,0.05)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.15,5000,0.05)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.15,5000,0.05)
        
        sc()
        sc()
        
    finally:
        sc()
        sc()
        sc()

def infocus_marco(t=0.02):
    try:
        so()
        
        fshtrigger()
        kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
        fshtrigger()
        kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
        fshtrigger()
        dmesh(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
        fshtrigger()
        dmesh(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.05,5000,t)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.05,5000,t)
        
        sc()
        sc()
        
    finally:
        sc()
        sc()
        sc()
        
        
def defocus_marco(t=0.02):
    try:
        so()
        
        fshtrigger()
        kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
        fshtrigger()
        kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
        fshtrigger()
        dmesh(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
        fshtrigger()
        dmesh(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.15,5000,t)
        
        fshtrigger()
        spiral_dscan(nnp2,nnp3,0.15,5000,t)
        
        sc()
        sc()
        
    finally:
        sc()
        sc()
        sc()

def infocus_scan(t=0.02):
    fshtrigger()
    kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
    fshtrigger()
    kmap.dkmap(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
    fshtrigger()
    dmesh(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
    fshtrigger()
    dmesh(nnp3,-2,2,80,nnp2,2,-2,80,t)
        
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.05,5000,t)
        
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.05,5000,t)
    
def defocus_scan(t=0.02):
    fshtrigger()
    kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
    fshtrigger()
    kmap.dkmap(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
    fshtrigger()
    dmesh(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
    fshtrigger()
    dmesh(nnp3,-3,3,120,nnp2,3,-3,40,t)
        
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.15,5000,t)
        
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.15,5000,t)

def monday_night_macro():
    try:
        so()
        defocus_scan(0.02)
        defocus_scan(0.05)
        defocus_scan(0.1)
        defocus_scan(0.2)
        
        rstp('infocus_noBS_sample_position')
        
        infocus_scan(0.05)
        infocus_scan(0.1)
        infocus_scan(0.2)
        sc()
        sc()
    finally:
        sc()
        sc()
        
# leg_d_correct (vertical)
# leg_e_correct
# leg_joint_correct
# chip_correct
def H_scan(t):
    fshtrigger()
    kmap.dkmap(nnp3,-3,3,120,nnp2,9,-9,120,t)
    
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.15,10000,t)

def V_scan(t):
    fshtrigger()
    kmap.dkmap(nnp3,-6,6,240,nnp2,6,-6,80,t)
    
    fshtrigger()
    spiral_dscan(nnp2,nnp3,0.15,10000,t)
    
    
def wednesday_night_macro():
    try:
        so()
        rstp('leg_joint_correct')
        H_scan(0.02)
        H_scan(0.1)

        rstp('leg_e_correct')
        H_scan(0.02)
        H_scan(0.1)
        
        rstp('chip_correct')
        H_scan(0.02)
        H_scan(0.1)
        H_scan(0.02)
        H_scan(0.1)
                
        rstp('leg_d_correct')
        V_scan(0.02)
        V_scan(0.1)
        
        sc()
        sc()
        
    finally:
        sc()
        sc() 
