print('ihma272_v2 load 24')

POS_POOL = '''
v2_alignment_0000.json
v2_col1-p1_0002.json
v2_empty_0001.json
v2_mymz_corner_0000.json
v2_x50_col1-p1_0003.json
v2_x50_col1-p2_0004.json
v2_x50_col1-p3_0005.json
v2_x50_col1-p4_0006.json
v2_x50_col1-p5_0007.json
v2_x50_col1-p6_0008.json
v2_x50_col1-p7_0009.json

v2_x50_col1-p7_0010.json
v2_x50_col1-p8_0011.json
'''
def doo_columns(the_range, the_step, the_expt, ncols, the_ystep):
    npts = int(1.0*the_range/the_step)
    the_used_range = npts*the_step
    yrange = (ncols)*the_ystep
    ynpts = ncols
    print('kmap parameters:')
    print((nnp3.name, -100.0, -100.0+the_used_range, npts, nnp2.name, 0,yrange,ynpts,the_expt))
    kmap.dkmap(nnp3, -100.0, -100.0+the_used_range, npts, nnp2, 0,yrange,ynpts,the_expt)

def doo_gopos(ps):
    print(f'\n\n\n======================================================================================\ndoing position: {ps}')
    gopos(ps)
    mvr(nnx, 2.5*0.0465)

def ihma272_v2_main2():

    try:
        so()
        doo_gopos('v2_x50_col1-p7_0009.json')
        doo_columns(200.0, 0.2, 0.005, 11, 10.0)
    finally:
        sc()
        sc()
        sc()

def ihma272_v2_main():

    try:
        so()
        doo_gopos('v2_x50_col1-p1_0003.json')
        doo_columns(200.0, 0.2, 0.005, 2, 10.0)
    
        doo_gopos('v2_x50_col1-p2_0004.json')
        doo_columns(200.0, 0.2, 0.005, 2, 10.0)
    
        doo_gopos('v2_x50_col1-p3_0005.json')
        doo_columns(200.0, 0.2, 0.005, 2, 10.0)
    
        doo_gopos('v2_x50_col1-p4_0006.json')
        doo_columns(200.0, 0.2, 0.005, 3, 10.0)
    
        doo_gopos('v2_x50_col1-p5_0007.json')
        doo_columns(200.0, 0.2, 0.005, 5, 10.0)
    
        doo_gopos('v2_x50_col1-p6_0008.json')
        doo_columns(200.0, 0.2, 0.005, 11, 10.0)
    
        doo_gopos('v2_x50_col1-p7_0009.json')
        doo_columns(200.0, 0.2, 0.005, 11, 10.0)
    finally:
        sc()
        sc()
        sc()
