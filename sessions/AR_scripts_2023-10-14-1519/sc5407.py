print('sc5407.py - load 1')

from scipy import ndimage as ndi


BUFFER = '''
ROTATION 

Hp1b_mem2_region5_p2: -63.16783756686527

'''


CEN_1 = -63.16783756686527 #Hp1b_mem2_region5_p2


def rot_hp1b_5_2():
	for angle in range(0, 61, 5): 
		cmg.move_to_cen(-63.16783756686527 + angle)
		print('================================')
		print(f'moving to angle {angle}')
		print('================================')
		align_particle(13, 0.4, 0.02)
		kmap.dkmap(nnp2, -3, 3, 60, nnp3, -3.5, 3.5, 70, 0.02)
		safety_net()

def Hpc6_mem_K():
    try:
        start_key = 'a'
        so()
        for i in range(1,6):
            pos_key = f'p{i}_good'
            ggg.grstp(f'pos_key', 'xyz')
            newdataset(f'{pos_key}_{start_key}')
            print('=========================')
            print(f'Going to {pos_key}')
            print('=========================')
            if i ==3:
                kmap.dkmap(nnp2, -7, 7, 280, nnp3, -6, 6, 240, 0.02)
            else:
                kmap.dkmap(nnp2, -6, 6, 240, nnp3, -6, 6, 240, 0.02)
            sync()
            elog_plot(scatter = True)
            dooc(f'this is position {pos_key}')
            sync()
            safety_net()
            

def Hc3b_memB():
    try:
        start_key = 'a'
        so()
        for i in range(1,4):
            pos_key = f'rot{i}'
            ggg.grstp(f'rot{i}', 'xyz')
            umvr(nnz, -0.004, nny, -0.004)
            newdataset(f'{pos_key}_{start_key}')
            print('=========================')
            print(f'Going to {pos_key}')
            print('=========================')
            kmap.dkmap(nnp2, -5, 5, 200, nnp3, -5, 5, 200, 0.02)
            sync()
            elog_plot(scatter = True)
            dooc(f'this is position {pos_key}')
            sync()
            safety_net()
            

        ggg.grstp('p4')
        umvr(nnz, -0.004, nny, -0.004)
        print('=========================')
        print(f'Going to p4')
        print('=========================')       
        newdataset(f'p4_{start_key}')
        kmap.dkmap(nnp2, -5, 5, 200, nnp3, -5, 5, 200, 0.02)
        sync()
        elog_plot(scatter = True)
        dooc(f'this is position this is position p4')
        sync()   
        safety_net()  
        
        ggg.grstp('p2')
        newdataset(f'p2_{start_key}')
        print('=========================')
        print(f'Going to p2')
        print('=========================')       
        kmap.dkmap(nnp2, -10, 12, 440, nnp3, -15, 5, 400, 0.02)
        sync()
        elog_plot(scatter = True)
        dooc(f'this is position this is position p2')
        sync()   
        safety_net()   
        
        ggg.grstp('p3')
        newdataset(f'p3_{start_key}')
        print('=========================')
        print(f'Going to p3')
        print('=========================')       
        kmap.dkmap(nnp2, -10, 10, 400, nnp3, -20, 10, 600, 0.02)
        sync()
        elog_plot(scatter = True)
        dooc(f'this is position this is position p3')
        sync()   
        safety_net()

        ggg.grstp('p1')
        newdataset(f'p1_{start_key}')
        print('=========================')
        print(f'Going to p1')
        print('=========================')       
        kmap.dkmap(nnp2, -20, 5, 500, nnp3, -20, 20, 800, 0.02)
        sync()
        elog_plot(scatter = True)
        dooc(f'this is position this is position p1')
        sync()          
        safety_net()

    finally:
        sc()
        sc()
        sc()
        
            
def doo_kmaps():
    start_key = 'a'
   
    for reg in range(2, 4):
        for part in range(1, 4):
            pos_key = f'region{reg}_p{part}'
            newdataset(f'{pos_key}_{start_key}')
            rstp(pos_key)
            print(f'going to {pos_key}')
            kmap.dkmap(nnp2, -10, 10, 200, nnp3, -10, 10, 200, 0.02)
            safety_net()


def safety_net():
    for i in range(1,7):
        timer = 7 - i
        print(f'{timer} sec to interrupt')
        sleep(1)
    print('TOO LATE - wait for next kmap')
    print('='*40)

def cen_mass_pix(thedata, shp, excessv=0.03):
	
	#data = sc.get_data('eiger:roi_counters:roi1_avg')
	#data = sc.get_data(ctr_string)
	data = thedata.copy()
	data = data.reshape(shp)
	medv = np.median(data)
	crit = medv + excessv
	thd_data = np.where(data > crit, 1, 0)
	cog = ndi.measurements.center_of_mass(thd_data)
	return cog

def get_last_scandata():
    sc = SCANS[-1]
    data = sc.get_data('eiger:roi_counters:roi1_avg')
    return data
	
def posinscan(shp, scan_cen_pix, scan_center, stepwidth, cog):
	print(shp, scan_cen_pix, scan_center, stepwidth, cog)
	pxv, pxh = scan_cen_pix
	shp_v, shp_h = shp
	cv, ch = scan_center
	stp_v, stp_h = stepwidth
	com_v, com_h = cog
	
	llv = (pxv - shp_v)*stp_v
	respos_v = cv + llv + com_v*stp_v
	llh = (pxh - shp_h)*stp_h
	respos_h = ch + llh + com_h*stp_h
	return respos_v, respos_h

def compute_particle_cen(scan_cen, scan_pix_cen, scan_shp, scan_stp, excessv=0.03):
    thedata = get_last_scandata()
    scan_cen = (nnp3.position, nnp2.position)
    theshape = scan_shp
    thecenpix= scan_pix_cen
    thecog = cen_mass_pix(thedata, theshape)
    target_pos = posinscan(theshape, thecenpix, scan_cen, scan_stp, thecog)
    return target_pos

def goto_nnp_pos(target_pos):
    nnp3_pos, nnp2_pos = target_pos
    umv(nnp3, nnp3_pos, nnp2, nnp2_pos)


def align_particle(nhw, stp, expt, excessv=0.03):
    cen_pos = (nnp3.position, nnp2.position)
    npts = nhw*2
    thelim = nhw*stp
    theshp = (npts, npts)
    thestp = (stp, stp)
    cen_pix = (nhw, nhw)
    kmap.dkmap(nnp2, -thelim, thelim, npts, nnp3, -thelim, thelim, npts, expt)
    target_pos = compute_particle_cen(cen_pos, cen_pix,  theshp, thestp, excessv=excessv)
    goto_nnp_pos(target_pos)
