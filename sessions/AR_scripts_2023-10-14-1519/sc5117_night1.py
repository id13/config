import math

print("sc5117_test asdf")

def test_1():
    print('iamtest')
    
def sc5117_night1():
    try:
        so()
        newdataset('roi1_nanodiff_2')
        stp = 0.1
        steps = 150
        theta_start = -10.3
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        rstp('roi1_50x._pos')
        umv(nnp2,169.83)
        umv(nnp3,121)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.007)
        sc()
    finally:
        sc()
        sc()
        sc()  

def sc5117_night2():
    try:
        so()
        newdataset('roi_patch2_nanodiff_1')
        stp = 0.1
        steps = 150
        theta_start = -10.3
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        umv(nnp2,128.338)
        umv(nnp3,87.3413)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.007)
        sc()
    finally:
        sc()
        sc()
        sc()  

def sc5117_morning2():
    try:
        so()
        newdataset('roi1_peakhunting_patch1_2')
        stp = 1.0
        steps = 15
        theta_start = -10.3
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        umv(nny,-2.1064)
        umv(nnz,-3.2524)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
            
        newdataset('roi1_peakhunting_patch2_2')
        mv(Theta,theta_start - theta_back)
        umv(nny,-1.8564)
        umv(nnz,-3.2524)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        newdataset('roi1_peakhunting_patch3_2')
        mv(Theta,theta_start - theta_back)
        umv(nny,-2.1064)
        umv(nnz,-3.0024)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        newdataset('roi1_peakhunting_patch4_2')
        mv(Theta,theta_start - theta_back)
        umv(nny,-1.8564)
        umv(nnz,-3.0024)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()  

def sc5117_day2_braggptycho():
    try:
        so()
        newdataset('cc116_braggptycho_a')
        stp = 0.01
        steps = 101
        theta_start = -7.0
        theta_back = 1.0
        umv(nnp2,136.365)
        umv(nnp3,95.3364)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1,100,0.02)
        mv(Theta,-6)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,136.365)
            umv(nnp3,95.3364)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.02)
        umv(nnp2,136.365)
        umv(nnp3,95.3364)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1,100,0.02)
        mv(Theta,0)
        sc()
    finally:
        sc()
        sc()
        sc()  
        
def sc5117_day2_braggptycho_b():
    try:
        so()
        newdataset('cc116_braggptycho_b')
        stp = 0.01
        steps = 101
        pos_nnp2 = 118.08
        pos_nnp3 = 99.2584
        theta_start = -7.2
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1,100,0.02)
        mv(Theta,-7.2)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1,100,0.02)
        mv(Theta,7.2)
        sc()
    finally:
        sc()
        sc()
        sc()  
def sc5117_day2_braggptycho_c():
    try:
        so()
        newdataset('cc116_braggptycho_c')
        stp = 0.015
        steps = 30
        pos_nnp2 = 131.371
        pos_nnp3 = 102.242
        theta_start = -6.4
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.45,30,0.005)
        mv(Theta,-6.4)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.45,40,0.005)
        mv(Theta,-6.4)
        sc()
    finally:
        sc()
        sc()
        sc()
        
      

def sc5117_dinner3():
    try:
        so()
        newdataset('peakhunting_patch1_1')
        stp = 1.0
        steps = 15
        theta_start = -7.5
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        gopos('patch1_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
            
        newdataset('peakhunting_patch2_1')
        mv(Theta,theta_start - theta_back)
        gopos('patch2_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        newdataset('peakhunting_patch3_1')
        mv(Theta,theta_start - theta_back)
        gopos('patch3_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        newdataset('peakhunting_patch4_1')
        mv(Theta,theta_start - theta_back)
        gopos('patch4_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()  


def sc5117_dinner3_afterdump():
    try:
        so()
        stp = 1.0
        steps = 15
        theta_start = -7.5
        theta_back = 1
        newdataset('peakhunting_patch3_2')
        mv(Theta,theta_start - theta_back)
        gopos('patch3_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        newdataset('peakhunting_patch4_2')
        mv(Theta,theta_start - theta_back)
        gopos('patch4_50x')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,115,-115,60,nnp3,-115,115,60,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()  
def sc5117_night3():
    try:
        so()
        newdataset('roi2_nanodiff_1')
        stp = 0.2
        steps = 50
        theta_start = -5
        theta_back = 1
        mv(Theta,theta_start - theta_back)
        rstp('roi2_50x._pos')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,20,-20,120,nnp3,-30,30,180,0.007)
        newdataset('roi3_nanodiff_1')    
        rstp('roi3_50x._pos')
        mv(Theta,theta_start - theta_back)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.007)
        newdataset('roi4_nanodiff_1')    
        rstp('roi4_50x._pos')
        mv(Theta,theta_start - theta_back)
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.007)
        sc()
    finally:
        sc()
        sc()
        sc() 
def sc5117_day4_braggptycho_a():
    try:
        so()
        newdataset('cc159_braggptycho_a')
        stp = 0.015
        steps = 46
        pos_nnp2 = 18.0
        pos_nnp3 = 118.6
        theta_start = 6.4
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.69,46,0.005)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.69,46,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()

def sc5117_day4_braggptycho_b():
    try:
        so()
        newdataset('cc159_braggptycho_b')
        stp = 0.015
        steps = 91
        pos_nnp2 = 18.5
        pos_nnp3 = 115.63
        theta_start = 5.2
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.35,90,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.35,90,0.02)
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_day4_braggptycho_b_retry():
    try:
        so()
        newdataset('cc159_braggptycho_b_retry')
        stp = 0.015
        steps = 32
        pos_nnp2 = 18.5
        pos_nnp3 = 115.63
        theta_start = 6.17
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.48,32,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.48,32,0.02)
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_day4_braggptycho_c():
    try:
        so()
        newdataset('cc159_braggptycho_c')
        stp = 0.015
        steps = 77
        pos_nnp2 = 19.5
        pos_nnp3 = 116.7
        theta_start = 6.04
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.14,76,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,21,nnp2,0.5,-0.5,21,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,1.14,76,0.02)
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_day4_braggptycho_f():
    try:
        so()
        newdataset('cc159_braggptycho_f')
        stp = 0.015
        steps = 41
        pos_nnp2 = 17.21
        pos_nnp3 = 119.78
        theta_start = 5.88
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.6,40,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.35,0.35,15,nnp2,0.35,-0.35,15,0.003)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.6,40,0.02)
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_day4_peakhunting():
    try:
        so()
        stp = 1.0
        steps = 15
        theta_start = -7.5
        theta_back = 1
        newdataset('peakhunting_mono_2')
        mv(Theta,theta_start - theta_back)
        gopos('mono_2')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_mono_3')
        mv(Theta,theta_start - theta_back)
        gopos('mono_3')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_mono_5')
        mv(Theta,theta_start - theta_back)
        gopos('mono_5')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_poly_1')
        mv(Theta,theta_start - theta_back)
        gopos('poly_1')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_poly_3')
        mv(Theta,theta_start - theta_back)
        gopos('poly_3')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_poly_4')
        mv(Theta,theta_start - theta_back)
        gopos('poly_4')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        newdataset('peakhunting_poly_5')
        mv(Theta,theta_start - theta_back)
        gopos('poly_5')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,50,nnp3,-25,25,50,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()  
        
def sc5117_day4_nanodiff_a():
    try:
        so()
        stp = 0.2
        steps = 60
        theta_start = -6.0
        theta_back = 1
        newdataset('nanodiff_mono_2_a')
        mv(Theta,theta_start - theta_back)
        gopos('mono_2')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_mono_3_a')
        mv(Theta,theta_start - theta_back)
        gopos('mono_3')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_mono_5_a')
        mv(Theta,theta_start - theta_back)
        gopos('mono_5')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_poly_1_a')
        mv(Theta,theta_start - theta_back)
        gopos('poly_1')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_poly_3_a')
        mv(Theta,theta_start - theta_back)
        gopos('poly_3')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_poly_5_a')
        mv(Theta,theta_start - theta_back)
        gopos('poly_5')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        sc()
    finally:
        sc()
        sc()
        sc()  



def sc5117_day4_nanodiff_b():
    try:
        so()
        stp = 0.2
        steps = 60
        theta_start = -6.0
        theta_back = 1
 
        newdataset('nanodiff_poly_1_b')
        mv(Theta,theta_start - theta_back)
        gopos('poly_1')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(55,steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)
        newdataset('nanodiff_poly_3_b')
        mv(Theta,theta_start - theta_back)
        gopos('poly_3')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(0,12):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)

        sc()
    finally:
        sc()
        sc()
        sc() 


def sc5117_day5_braggptycho_a():
    try:
        so()
        newdataset('cc161_braggptycho_a')
        stp = 0.025
        steps = 97
        pos_nnp2 = 133.8
        pos_nnp3 = 125.4
        theta_start = -0.1
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.4,96,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.35,0.35,15,nnp2,0.35,-0.35,15,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.4,96,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()


def sc5117_day5_braggptycho_b():
    try:
        so()
        newdataset('cc161_braggptycho_b')
        stp = 0.025
        steps = 97
        pos_nnp2 = 134.8
        pos_nnp3 = 124.4
        theta_start = -0.8
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.4,96,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.4,96,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
def sc5117_day5_braggptycho_c():
    try:
        so()
        newdataset('cc161_braggptycho_c')
        stp = 0.025
        steps = 91
        pos_nnp2 = 135.8
        pos_nnp3 = 122.9
        theta_start = 0.35
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.25,90,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.35,0.35,15,nnp2,0.35,-0.35,15,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,2.25,90,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
def sc5117_day5_mono_braggptycho_a():
    try:
        so()
        newdataset('cc161_mono_braggptycho_a')
        stp = 0.015
        steps = 47
        pos_nnp2 = 139.7
        pos_nnp3 = 129.45
        theta_start = -6.0
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.69,46,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.35,0.35,15,nnp2,0.35,-0.35,15,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.69,46,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()

def sc5117_day5_mono_braggptycho_c():
    try:
        so()
        newdataset('cc161_mono_braggptycho_c')
        stp = 0.015
        steps = 21
        pos_nnp2 = 139.7
        pos_nnp3 = 134.6
        theta_start = -5.75
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.3,20,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.3,20,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()

def sc5117_day5_mono_braggptycho_d():
    try:
        so()
        newdataset('cc161_mono_braggptycho_d')
        stp = 0.015
        steps = 41
        pos_nnp2 = 129.7
        pos_nnp3 = 134.6
        theta_start = -5.45
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.6,40,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.6,40,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()

def sc5117_day5_dinner():
    try:
        so()
        stp = 0.1
        steps = 100
        theta_start = -4.0
        theta_back = 1
 
        newdataset('mono_1_nanodiff_a')
        mv(Theta,theta_start - theta_back)
        gopos('mono_1')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)


        sc()
    finally:
        sc()
        sc()
        sc() 
        
        
def sc5117_evening5_mono_braggptycho_a():
    try:
        so()
        newdataset('cc158_mono1bis_braggptycho_')
        stp = 0.015
        steps = 25
        pos_nnp2 = 130.0
        pos_nnp3 = 120.0
        theta_start = 7.8
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()


def sc5117_evening5_mono_braggptycho_d():
    try:
        so()
        newdataset('cc158_mono1bis_braggptycho_d')
        stp = 0.015
        steps = 25
        pos_nnp2 = 130.0
        pos_nnp3 = 124.0
        theta_start = 7.75
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_evening5_mono_braggptycho_e():
    try:
        so()
        newdataset('cc158_mono1bis_braggptycho_e')
        stp = 0.015
        steps = 25
        pos_nnp2 = 130.0
        pos_nnp3 = 114.0
        theta_start = 7.75
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.02)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.36,24,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_evening5_mono_braggptycho_f():
    try:
        so()
        newdataset('cc158_mono1_braggptycho_f')
        stp = 0.015
        steps = 46
        pos_nnp2 = 203.1
        pos_nnp3 = 133.8
        theta_start =7.75
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.675,45,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.675,45,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_evening5_mono_braggptycho_g():
    try:
        so()
        newdataset('cc158_mono1_braggptycho_g')
        stp = 0.015
        steps = 46
        pos_nnp2 = 203.1
        pos_nnp3 = 133.8
        theta_start = 6.75
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.675,45,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.5,0.5,20,nnp2,0.5,-0.5,20,0.005)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.675,45,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()

def sc5117_evening5_mono_braggptycho_h():
    try:
        so()
        newdataset('cc158_mono1_braggptycho_h')
        stp = 0.025
        steps = 23
        pos_nnp2 = 200.1
        pos_nnp3 = 133.8
        theta_start = 6.5
        theta_back = 1.0
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.55,22,0.02)
        mv(Theta,theta_start - theta_back)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            umv(nnp2,pos_nnp2)
            umv(nnp3,pos_nnp3)
            kmap.dkmap(nnp3,-0.35,0.35,15,nnp2,0.35,-0.35,15,0.003)
        umv(nnp2,pos_nnp2)
        umv(nnp3,pos_nnp3)
        fshtrigger()
        mv(Theta,theta_start - theta_back)
        mv(Theta,theta_start)
        dscan(Theta,0,0.55,45,0.02) 
        sc()
    finally:
        sc()
        sc()
        sc()
        
def sc5117_day5_night():
    try:
        so()
        stp = 0.05
        steps = 200
        theta_start = -4.0
        theta_back = 1
 
        newdataset('mono_1_nanodiff_finesteps_a')
        mv(Theta,theta_start - theta_back)
        gopos('mono_1')
        umv(nnp2,125)
        umv(nnp3,125)
        for i in range(steps):
            theta_curr = theta_start +i*stp
            print("Current theta", theta_curr,'cycle = ',i)
            mv(Theta, theta_curr)
            kmap.dkmap(nnp2,25,-25,150,nnp3,-25,25,150,0.005)


        sc()
    finally:
        sc()
        sc()
        sc() 

