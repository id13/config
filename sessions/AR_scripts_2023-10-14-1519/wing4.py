import time
print ("version wing4-1")

def do_pty_patch():
    kmap.dkmap(nnp3,-4,4,160,nnp2,4,-4, 40,0.050, frames_per_file=200)

def the_wing4patch():
    nnp2_start = 145.0
    nnp3_start = 105.0

    mv(nnp2, nnp2_start)
    mv(nnp3, nnp3_start)

    for i in range(5):
        print ("===========================------- i cycle:", i)
        nnp3_pos = nnp3_start + i*8.0
        print("nnp3_pos =", nnp3_pos)
        mv(nnp3, nnp3_pos)

        for j in range(5):
            print ("==== j cycle:", j, "  (i=%1d)" % i)
            nnp2_pos = nnp2_start - j*8.0
            print("nnp2_pos =", nnp2_pos)
            mv(nnp2, nnp2_pos)
            do_pty_patch()

def wing4():
    mgeig()
    fshtrigger()
    so()
    sh1.open()
    time.sleep(5)
    print("running wing4patch ...")
    try:
        the_wing4patch()
    finally:
        try:
            sh1.close()
        finally:
            sc()
