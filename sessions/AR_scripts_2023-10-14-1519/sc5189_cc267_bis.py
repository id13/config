print('sc5189_cc267_bis load 15')
START_KEY = 'e'

THE_LIST = '''
cc267_5x_roi2_0065.json 120 100 0.5 0.01
#cc267_5x_roi3_0066.json 190 180 0.5 0.01
cc267_5x_roi4_0067.json 110 80  0.5 0.01
cc267_5x_roi5_0068.json 100 100 0.5 0.01
cc267_5x_roi6_0069.json 150 120 0.5 0.01
'''

def make_posdc(poslst):
    posll = poslst.split('\n')
    dc = dict()
    for l in posll:
        #print (f'l = {l}')
        l = l.strip()
        if not l or l.startswith('#'):
            continue
        w1 = l.split()
        w2 = w1[0].split('_')
        for e in w2:
            #print (f'e = {e}')
            if e.startswith('roi'):
                dc[e] = l
    return dc


def dooone(psdc, roi):
    dsname = f'{roi}_{START_KEY}'
    posname, hw, vw, stp, expt = psdc[roi].split()
    hw, vw, stp, expt = map(float, (hw, vw, stp, expt))
    hll = -0.5*0.001*hw
    hul = 0.5*0.001*hw
    hitv = int(hw/stp)
    vll = -0.5*0.001*vw
    vul = 0.5*0.001*vw
    vitv = int(vw/stp)
    print('\n\n\n                                                                  #####')
    print(' ============================================================-----#####')
    print('                                                                  #####\n')
    print(f'ROI: {roi} - What should happen ...')
    print(f"newdataset('{dsname}')")
    print(f"gopos('{posname}')")
    print(f'dkmapyz_2({hll}, {hul}, {hitv}, {vll}, {vul}, {vitv}, {expt})')
    print('')
    print('What is happening ... Good Luck!')
    newdataset(dsname)
    gopos(posname)
    dkmapyz_2(hll, hul, hitv, vll, vul, vitv, expt)


def sc5189_cc267_bis():
    try:
        posdc = make_posdc(THE_LIST)
        so()
        mgeig()
        dooone(posdc, 'roi2')
        dooone(posdc, 'roi4')
        dooone(posdc, 'roi5')
        dooone(posdc, 'roi6')

    finally:
        pass
        sc()
        sc()
        sc()
