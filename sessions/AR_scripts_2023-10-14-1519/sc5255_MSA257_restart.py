print('sc5255_MSA257.py load 1')

#from math import *
import math

def shortmesh(theta,lsr,hsr):
  
  theta_rad = theta * math.pi/180 
  wf = math.cos(math.fabs(theta_rad))
  
  c_lsr = lsr * wf
  c_hsr = hsr * wf
  
  start = lsr - c_lsr
  end = start + c_lsr + c_hsr
  
  images = 700#600
  
  exptime = 0.01 * wf
  
  print('scanning from %f to %f with %f exptime' %(start,end,exptime))
  dkmapyz_2(start,end,images-1,0,0.05,9,exptime,frames_per_file=images, retveloc=5.0)


def partial_theta_scan(bunch,lsr,hsr):

  theta = -30
  umv(usrotz,theta)
  print('bb0%s_thm%02d' %(bunch,-theta))
  newdataset('bb0%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  theta = -45
  umv(usrotz,theta)
  print('bb0%s_thm%02d' %(bunch,-theta))
  newdataset('bb0%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  
  umv(usrotz,0)

  
def theta_scan(bunch,lsr,hsr):

  theta = 0
  umv(usrotz,theta)
  print('bb0%s_th%02d' %(bunch,theta))
  newdataset('bb0%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  
  #theta = 15
  #umv(usrotz,theta)
  #print('bb0%s_th%02d' %(bunch,theta))
  #newdataset('bb0%s_th%02d' %(bunch,theta))
  #shortmesh(theta,lsr,hsr)

  theta = 20
  umv(usrotz,theta)
  print('bb0%s_th%02d' %(bunch,theta))
  newdataset('bb0%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  theta = 45
  umv(usrotz,theta)
  print('bb0%s_th%02d' %(bunch,theta))
  newdataset('bb0%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  
  #theta = -15
  #umv(usrotz,theta)
  #print('bb0%s_thm%02d' %(bunch,-theta))
  #newdataset('bb0%s_thm%02d' %(bunch,-theta))
  #shortmesh(theta,lsr,hsr)

  theta = -20
  umv(usrotz,theta)
  print('bb0%s_thm%02d' %(bunch,-theta))
  newdataset('bb0%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  theta = -45
  umv(usrotz,theta)
  print('bb0%s_thm%02d' %(bunch,-theta))
  newdataset('bb0%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  
  if bunch == 40:
    theta = 0
    umv(usrotz,theta)
    print('bb0_chk%s_th%02d' %(bunch,theta))
    newdataset('bb0%s_th%02d' %(bunch,theta))
    shortmesh(theta,lsr,hsr)
  
  umv(usrotz,0)

  
def sample_mesh(startcoord,lsr,hsr,steps):
  
  try:
    so()
    #eigerhws(True)
    mgeig()
    rstp(startcoord)

    #print('partially scanning bunch 5')
    #partial_theta_scan(20,lsr,hsr)
    #umvr(ustrz,0.22)

    for i in range(58,101):
      print('scanning bunch %i' %i)
      
      print('===============')
      print(ustrz.tolerance)
      wm(ustrz)
      print('===============')
      
      theta_scan(i,lsr,hsr)
      umvr(ustrz,0.055)
    enddataset()
    sc()
        
  finally:
    sc()
    sc()
    sc()
  
  
  
