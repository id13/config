print('version ttomo test ihmi1531 - 1')
import time
import gevent,bliss
import traceback


class Bailout(Exception): pass

#index,kappa,omega,absorption
ORI_INSTRUCT = """
0, 0, 0.000, 0
1, 0, 2.571, 0
2, 0, 5.143, 0
3, 0, 7.714, 0
4, 0, 10.286, 0
5, 0, 12.857, 0
6, 0, 15.429, 0
7, 0, 18.000, 0
8, 0, 20.571, 0
9, 0, 23.143, 0
10, 0, 25.714, 0
11, 0, 28.286, 0
12, 0, 30.857, 0
13, 0, 33.429, 0
14, 0, 36.000, 0
15, 0, 38.571, 0
16, 0, 41.143, 0
17, 0, 43.714, 0
18, 0, 46.286, 0
19, 0, 48.857, 0
20, 0, 51.429, 0
21, 0, 54.000, 0
22, 0, 56.571, 0
23, 0, 59.143, 0
24, 0, 61.714, 0
25, 0, 64.286, 0
26, 0, 66.857, 0
27, 0, 69.429, 0
28, 0, 72.000, 0
29, 0, 74.571, 0
30, 0, 77.143, 0
31, 0, 79.714, 0
32, 0, 82.286, 0
33, 0, 84.857, 0
34, 0, 87.429, 0
35, 0, 90.000, 0
36, 0, 92.571, 0
37, 0, 95.143, 0
38, 0, 97.714, 0
39, 0, 100.286, 0
40, 0, 102.857, 0
41, 0, 105.429, 0
42, 0, 108.000, 0
43, 0, 110.571, 0
44, 0, 113.143, 0
45, 0, 115.714, 0
46, 0, 118.286, 0
47, 0, 120.857, 0
48, 0, 123.429, 0
49, 0, 126.000, 0
50, 0, 128.571, 0
51, 0, 131.143, 0
52, 0, 133.714, 0
53, 0, 136.286, 0
54, 0, 138.857, 0
55, 0, 141.429, 0
56, 0, 144.000, 0
57, 0, 146.571, 0
58, 0, 149.143, 0
59, 0, 151.714, 0
60, 0, 154.286, 0
61, 0, 156.857, 0
62, 0, 159.429, 0
63, 0, 162.000, 0
64, 0, 164.571, 0
65, 0, 167.143, 0
66, 0, 169.714, 0
67, 0, 172.286, 0
68, 0, 174.857, 0
69, 0, 177.429, 0
70, 0, 180.000, 0
71, -10, 1.286, 0
72, -10, 10.759, 0
73, -10, 20.233, 0
74, -10, 29.707, 0
75, -10, 39.180, 0
76, -10, 48.654, 0
77, -10, 58.128, 0
78, -10, 67.602, 0
79, -10, 77.075, 0
80, -10, 86.549, 0
81, -10, 96.023, 0
82, -10, 105.496, 0
83, -10, 114.970, 0
84, -10, 124.444, 0
85, -10, 133.917, 0
86, -10, 143.391, 0
87, -10, 152.865, 0
88, -10, 162.338, 0
89, -10, 171.812, 0
90, -10, 181.286, 0
91, -10, 190.759, 0
92, -10, 200.233, 0
93, -10, 209.707, 0
94, -10, 219.180, 0
95, -10, 228.654, 0
96, -10, 238.128, 0
97, -10, 247.602, 0
98, -10, 257.075, 0
99, -10, 266.549, 0
100, -10, 276.023, 0
101, -10, 285.496, 0
102, -10, 294.970, 0
103, -10, 304.444, 0
104, -10, 313.917, 0
105, -10, 323.391, 0
106, -10, 332.865, 0
107, -10, 342.338, 0
108, -10, 351.812, 0
109, -10, 361.286, 0
110, -20, 0.000, 0
111, -20, 10.000, 0
112, -20, 20.000, 0
113, -20, 30.000, 0
114, -20, 40.000, 0
115, -20, 50.000, 0
116, -20, 60.000, 0
117, -20, 70.000, 0
118, -20, 80.000, 0
119, -20, 90.000, 0
120, -20, 100.000, 0
121, -20, 110.000, 0
122, -20, 120.000, 0
123, -20, 130.000, 0
124, -20, 140.000, 0
125, -20, 150.000, 0
126, -20, 160.000, 0
127, -20, 170.000, 0
128, -20, 180.000, 0
129, -20, 190.000, 0
130, -20, 200.000, 0
131, -20, 210.000, 0
132, -20, 220.000, 0
133, -20, 230.000, 0
134, -20, 240.000, 0
135, -20, 250.000, 0
136, -20, 260.000, 0
137, -20, 270.000, 0
138, -20, 280.000, 0
139, -20, 290.000, 0
140, -20, 300.000, 0
141, -20, 310.000, 0
142, -20, 320.000, 0
143, -20, 330.000, 0
144, -20, 340.000, 0
145, -20, 350.000, 0
146, -20, 360.000, 0
147, -30, 5.000, 0
148, -30, 15.588, 0
149, -30, 26.176, 0
150, -30, 36.765, 0
151, -30, 47.353, 0
152, -30, 57.941, 0
153, -30, 68.529, 0
154, -30, 79.118, 0
155, -30, 89.706, 0
156, -30, 100.294, 0
157, -30, 110.882, 0
158, -30, 121.471, 0
159, -30, 132.059, 0
160, -30, 142.647, 0
161, -30, 153.235, 0
162, -30, 163.824, 0
163, -30, 174.412, 0
164, -30, 185.000, 0
165, -30, 195.588, 0
166, -30, 206.176, 0
167, -30, 216.765, 0
168, -30, 227.353, 0
169, -30, 237.941, 0
170, -30, 248.529, 0
171, -30, 259.118, 0
172, -30, 269.706, 0
173, -30, 280.294, 0
174, -30, 290.882, 0
175, -30, 301.471, 0
176, -30, 312.059, 0
177, -30, 322.647, 0
178, -30, 333.235, 0
179, -30, 343.824, 0
180, -30, 354.412, 0
181, -30, 365.000, 0
182, -40, 0.000, 0
183, -40, 12.000, 0
184, -40, 24.000, 0
185, -40, 36.000, 0
186, -40, 48.000, 0
187, -40, 60.000, 0
188, -40, 72.000, 0
189, -40, 84.000, 0
190, -40, 96.000, 0
191, -40, 108.000, 0
192, -40, 120.000, 0
193, -40, 132.000, 0
194, -40, 144.000, 0
195, -40, 156.000, 0
196, -40, 168.000, 0
197, -40, 180.000, 0
198, -40, 192.000, 0
199, -40, 204.000, 0
200, -40, 216.000, 0
201, -40, 228.000, 0
202, -40, 240.000, 0
203, -40, 252.000, 0
204, -40, 264.000, 0
205, -40, 276.000, 0
206, -40, 288.000, 0
207, -40, 300.000, 0
208, -40, 312.000, 0
209, -40, 324.000, 0
210, -40, 336.000, 0
211, -40, 348.000, 0
212, -40, 360.000, 0
213, -45, 6.000, 0
214, -45, 18.857, 0
215, -45, 31.714, 0
216, -45, 44.571, 0
217, -45, 57.429, 0
218, -45, 70.286, 0
219, -45, 83.143, 0
220, -45, 96.000, 0
221, -45, 108.857, 0
222, -45, 121.714, 0
223, -45, 134.571, 0
224, -45, 147.429, 0
225, -45, 160.286, 0
226, -45, 173.143, 0
227, -45, 186.000, 0
228, -45, 198.857, 0
229, -45, 211.714, 0
230, -45, 224.571, 0
231, -45, 237.429, 0
232, -45, 250.286, 0
233, -45, 263.143, 0
234, -45, 276.000, 0
235, -45, 288.857, 0
236, -45, 301.714, 0
237, -45, 314.571, 0
238, -45, 327.429, 0
239, -45, 340.286, 0
240, -45, 353.143, 0
241, -45, 366.000, 0
242, -35, 0.000, 0
243, -35, 11.250, 0
244, -35, 22.500, 0
245, -35, 33.750, 0
246, -35, 45.000, 0
247, -35, 56.250, 0
248, -35, 67.500, 0
249, -35, 78.750, 0
250, -35, 90.000, 0
251, -35, 101.250, 0
252, -35, 112.500, 0
253, -35, 123.750, 0
254, -35, 135.000, 0
255, -35, 146.250, 0
256, -35, 157.500, 0
257, -35, 168.750, 0
258, -35, 180.000, 0
259, -35, 191.250, 0
260, -35, 202.500, 0
261, -35, 213.750, 0
262, -35, 225.000, 0
263, -35, 236.250, 0
264, -35, 247.500, 0
265, -35, 258.750, 0
266, -35, 270.000, 0
267, -35, 281.250, 0
268, -35, 292.500, 0
269, -35, 303.750, 0
270, -35, 315.000, 0
271, -35, 326.250, 0
272, -35, 337.500, 0
273, -35, 348.750, 0
274, -35, 360.000, 0
275, -25, 5.625, 0
276, -25, 15.625, 0
277, -25, 25.625, 0
278, -25, 35.625, 0
279, -25, 45.625, 0
280, -25, 55.625, 0
281, -25, 65.625, 0
282, -25, 75.625, 0
283, -25, 85.625, 0
284, -25, 95.625, 0
285, -25, 105.625, 0
286, -25, 115.625, 0
287, -25, 125.625, 0
288, -25, 135.625, 0
289, -25, 145.625, 0
290, -25, 155.625, 0
291, -25, 165.625, 0
292, -25, 175.625, 0
293, -25, 185.625, 0
294, -25, 195.625, 0
295, -25, 205.625, 0
296, -25, 215.625, 0
297, -25, 225.625, 0
298, -25, 235.625, 0
299, -25, 245.625, 0
300, -25, 255.625, 0
301, -25, 265.625, 0
302, -25, 275.625, 0
303, -25, 285.625, 0
304, -25, 295.625, 0
305, -25, 305.625, 0
306, -25, 315.625, 0
307, -25, 325.625, 0
308, -25, 335.625, 0
309, -25, 345.625, 0
310, -25, 355.625, 0
311, -25, 365.625, 0
312, -15, 0.000, 0
313, -15, 9.474, 0
314, -15, 18.947, 0
315, -15, 28.421, 0
316, -15, 37.895, 0
317, -15, 47.368, 0
318, -15, 56.842, 0
319, -15, 66.316, 0
320, -15, 75.789, 0
321, -15, 85.263, 0
322, -15, 94.737, 0
323, -15, 104.211, 0
324, -15, 113.684, 0
325, -15, 123.158, 0
326, -15, 132.632, 0
327, -15, 142.105, 0
328, -15, 151.579, 0
329, -15, 161.053, 0
330, -15, 170.526, 0
331, -15, 180.000, 0
332, -15, 189.474, 0
333, -15, 198.947, 0
334, -15, 208.421, 0
335, -15, 217.895, 0
336, -15, 227.368, 0
337, -15, 236.842, 0
338, -15, 246.316, 0
339, -15, 255.789, 0
340, -15, 265.263, 0
341, -15, 274.737, 0
342, -15, 284.211, 0
343, -15, 293.684, 0
344, -15, 303.158, 0
345, -15, 312.632, 0
346, -15, 322.105, 0
347, -15, 331.579, 0
348, -15, 341.053, 0
349, -15, 350.526, 0
350, -15, 360.000, 0
351, -5, 4.737, 0
352, -5, 14.211, 0
353, -5, 23.684, 0
354, -5, 33.158, 0
355, -5, 42.632, 0
356, -5, 52.105, 0
357, -5, 61.579, 0
358, -5, 71.053, 0
359, -5, 80.526, 0
360, -5, 90.000, 0
361, -5, 99.474, 0
362, -5, 108.947, 0
363, -5, 118.421, 0
364, -5, 127.895, 0
365, -5, 137.368, 0
366, -5, 146.842, 0
367, -5, 156.316, 0
368, -5, 165.789, 0
369, -5, 175.263, 0
370, -5, 184.737, 0
371, -5, 194.211, 0
372, -5, 203.684, 0
373, -5, 213.158, 0
374, -5, 222.632, 0
375, -5, 232.105, 0
376, -5, 241.579, 0
377, -5, 251.053, 0
378, -5, 260.526, 0
379, -5, 270.000, 0
380, -5, 279.474, 0
381, -5, 288.947, 0
382, -5, 298.421, 0
383, -5, 307.895, 0
384, -5, 317.368, 0
385, -5, 326.842, 0
386, -5, 336.316, 0
387, -5, 345.789, 0
388, -5, 355.263, 0
389, -5, 364.737, 0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (ext_idx, kap,ome, absorb) = l.split(',')
            ko = (int(ext_idx), int(kap), float(ome), int(absorb))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log', resume=-1, start_key="zzzz"):
        self.start_key = start_key
        self.resume = resume
        self.asyfn = asyfn
        self.logfn = logfn
        self.aux_logfn = 'aux_ttomo.log'
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0
        self.log('\n\n\n\n\n################################################################\n\n                          NEW TTomo starting ...\n\n')

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(ext_idx,kap,ome, absorb)) = instll_item
        if ext_idx < self.resume:
            self.log(f'resume - clause: skipping item {instll_item}')
            return
        print('========================>>> doo_projection', instll_item)
        print(absorb, type(absorb))
        if not absorb:
            print ('diff!')
        else:
            print ('absorb - which is illegal for this version!')

        #raise RuntimeError('test')
        str_ome = f'{ome:08.2f}'
        str_ome = str_ome.replace('.','p')
        str_ome = str_ome.replace('-','m')
        str_kap = f'{kap:1d}'
        str_kap = str_kap.replace('-','m')
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=6)
        newdataset_base = f'tt_{self.start_key}_{i:03d}_{ext_idx:03d}_{str_kap}_{str_ome}'
        if absorb:
            # no absorption sacns
            raise ValueError('no absorption scans !!!!!!')
            self.log('no absorption scans')
            #dsname = newdataset_base + '_absorb'
            #newdataset(dsname)
            # sc5408_fltube_to_fltdiode()
            #self.perform_scan()
        else:
            
            sp = self.scanparams
            self.auxlog('\nstart %s %1d %1d %1d %1d %f' % (
                self.start_key, i, ext_idx,
                int(sp['nitvy']), int(sp['nitvz']), time.time()))
            dsname = newdataset_base + '_diff'
            newdataset(dsname)
            self.perform_scan()
            self.auxlog(' done')

    def perform_scan(self):
		
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
        )
        # for the EH2 stepper motors
        #lly *= 0.001
        #uly *= 0.001
        #llz *= 0.001
        #ulz *= 0.001

        # for pi piezos
        lly *= 1.0
        uly *= 1.0
        llz *= 1.0
        ulz *= 1.0
        cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
        self.log(cmd)
        t0 = time.time()
        # put a reasonable time out here ...
        with gevent.Timeout(seconds=600):
            try:
                # put the nano scan ...
                
                kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt,frames_per_file=nitvy*20)
                # kmap.dkmap(nnp5,lly,uly,nitvy,nnp6,llz,ulz,nitvz,expt, frames_per_file=nitvy)
                #cmd = f'kmap.dkmap(nnp5, {lly},{uly},{nitvy}, nnp6, {llz},{ulz},{nitvz},{expt})'
                self.log('scan successful')
            except bliss.common.greenlet_utils.killmask.BlissTimeout:
                msg = f'caught hanging scan after {time.time() - t0} seconds timeout'
                print(msg)
                self.log(msg)
            
        #time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)
            
    def auxlog(self, s):
        s = str(s)
        with open(self.aux_logfn, 'a') as f:
            print('aux: [%s]' % s)
            f.write(s)
 
    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        print('adapting translational corr table:', c_corr_t)
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 0.5*(lly//0.5)
        uly = 0.5*(uly//0.5)
        llz = 0.5*(llz//0.5)
        ulz = 0.5*(ulz//0.5)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy//0.5)
        nitvz = int(dz//0.5)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz


def ihmi1531_ttomo_main(zkap, start_key, resume=-1):

    
    print('Hi IH-MI_1531!')

    # verify zkap
    print(zkap)

    

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    scanparams = dict(
        lly = -40,
        uly = 40,
        nitvy = 160,
        llz = -50,
        ulz = 50,
        nitvz = 200,
        expt = 0.002
    )
    # resume = -1 >>> does all the list
    # resume=n .... starts it tem from  nth external index (PSI matlab script)
    ttm = TTomo( 'asy.com', zkap, instll, scanparams, resume=resume, start_key=start_key)



    # loop over projections
    ttm.mainloop()
