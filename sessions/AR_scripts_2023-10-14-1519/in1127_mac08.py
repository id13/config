print ("mac08 start")
def the_scan():
    dscan(ustrz,-0.1,0.1,100,0.05)

def the_mesh1():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.05)
    
def the_mesh2():
    dmesh(ustrz,-0.1,0.1,50,ustry,-0.2,0.2,100,0.02)
    
def the_mesh4():
    dmesh(ustrz,-0.3,0.3,10,ustry,-0.1,0.1,10,0.05)
    
def in1127_mac08():
    so()
    fshtrigger()
    mgeig()
    for i in range(115,116):
        pos_name = "s_fr06_01_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.3,100,0.06,retveloc=3)
        
    for i in range(116,117):
        pos_name = "s_fr06_01_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.3,100,0.06,retveloc=3)
        
    for i in range(117,118):
        pos_name = "s_fr06_02_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.33,110,0.06,retveloc=3)
        
    for i in range(118,119):
        pos_name = "s_fr06_02_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.36,120,0.06,retveloc=3)
        
    for i in range(119,120):
        pos_name = "s_fr06_03_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.33,110,0.06,retveloc=3)
        
    for i in range(120,121):
        pos_name = "s_fr06_03_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        dkmapyz_2(0,0.6,200,0,0.36,120,0.06,retveloc=3)
        
    sc()
    sc()
    sc()
        
        
print ("mac08 end")
