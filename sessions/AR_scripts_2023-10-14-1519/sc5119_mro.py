import math

print("sc5119_mro load 7")

START_KEY = 'a'
EXP_T     = 0.02
NITV      = 40

SZ        = 125 #center of nnp3
DZ        = 3
STh       = -50
ETh       = 50
DTh       = 20
mov_mot   = nnp3

def theta_mesh(
                EXP_T    , 
                NITV     , 
                SZ       , 
                DZ       , 
                STh      , 
                ETh      , 
                DTh      , 
                mov_mot  , 
                ):
    NUM_ZSTP = int((ETh-STh)/DTh) 
    for i in range(NUM_ZSTP):
        umv(mov_mot, SZ  + i*DZ)
        if i == 0:
           umv(Theta,STh + i*DTh -1)
        umv(Theta,STh + i*DTh)
        frames_per_file = int(NITV + 1)
        eiger.saving.frames_per_file = frames_per_file
        #dscan(Theta,0,DTh,NITV,EXP_T)
        ascan(Theta,STh + i*DTh, STh + (i+1)*DTh,NITV,EXP_T)

def do_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV      = NITV,
                SZ        = SZ, #center of nnp3
                DZ        = DZ,
                STh       = STh,
                ETh       = ETh,
                DTh       = DTh,
                mov_mot   = mov_mot,
                ):
    try:
        dsname = f'{pname}_{START_KEY}'
        newdataset(dsname)
        zeronnp()
        fshtrigger()
        so()
        mgeig()
        theta_mesh(
                  EXP_T  ,   
                  NITV   ,   
                  SZ     ,   
                  DZ     ,   
                  STh    ,   
                  ETh    ,   
                  DTh    ,   
                  mov_mot,)
        zeronnp()
        sc()    
    finally:
        eiger.saving.frames_per_file = 200
        sc()
        sc()
        
def do_scan_mro_pos(pos,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV      = NITV,
                SZ        = SZ, #center of nnp3
                DZ        = DZ,
                STh       = STh,
                ETh       = ETh,
                DTh       = DTh,
                mov_mot   = mov_mot,
                ):
    # this will go to pos and run the scan
    umv(Theta,-1)
    umv(Theta,0)
    gopos(pos) 
    pname = pos[:-5]
    do_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV      = NITV,
                SZ        = SZ, #center of nnp3
                DZ        = DZ,
                STh       = STh,
                ETh       = ETh,
                DTh       = DTh,
                mov_mot   = mov_mot,
                )
    umv(Theta,-1)
    umv(Theta,0)
