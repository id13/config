print('ihsc1774 ver5')
import traceback as ttbb
key = 'a'
ang = np.arange(0,17,2)

def test_run():
    newdataset(f'tip_nnp3_fast_{key}')
    for _ in ang:
        umv(Theta,_)
        kmap.dkmap(nnp3,-20,20,400,nnp2,-5,5,10,0.02)
    enddataset()

initial_angle = -60
angle_start_step  = 4
num_scan = 30
#gopos('sagitta1_tip_th0_test_c_0011.json')
def sagitta_rotate1():
    gopos('sagitta1_tip_th0_test_c_0011.json')
    for _ in range(30):
        if _ == 0:
            pass
        else:
            umvr(nnz,0.01)
        start_angle = initial_angle+angle_start_step*_
        umv(Theta,start_angle)
        newdataset(f'sagitta_spine_pos{_:03d}_start_angle_{start_angle:04d}_{key}')
        for __ in range(10):
            if __ == 0:
                pass
            else:
                umvr(Theta,1)
            kmap.dkmap(nnp2,-50,50,1000,nnp3,0,5,5,0.005)
        enddataset()


def doocalib():
    try:
        so()
        sleep(3)
        for i, ndetx_pos in enumerate(np.linspace(-335.0, -235, 21)):
            print('='*40)
            print('='*40)
            print('cycle:', i, 'target ndetx position:', ndetx_pos)
            newdataset_name = f'al2o3_ndetx_m{-1*int(ndetx_pos)}_{key}'
            print(ndetx_pos, newdataset_name)
            newdataset(newdataset_name)
            umv(ndetx, ndetx_pos)
            print('detector position:', ndetx.position)
            kmap.dkmap(nnp2, -40, 40, 80, nnp3, -40, 40, 40, 0.01)
            print(2*'\n')
    finally:
        sc()

#
# sagitta2 mem
#
M50_LIST = '''#
sagitta2_mem_M50_p1_0025.json -70 70 1400 -70 50 10 0.01        0 2 20    1.0
sagitta2_mem_M50_p2_tip_0034.json -10 20 150 -10 20 60 0.01     0 16 32   0
sagitta2_mem_M50_p3_0027.json -60 60 1200 -70 70 20 0.01        0 4 20    0.35
sagitta2_mem_M50_p4_0028.json -60 60 600 -60 60 24 0.01         0 16 32   0.2
sagitta2_mem_M50_p5_0029.json -80 80 800 -80 80 80 0.01         0 16 16   0
sagitta2_mem_M50_p6_0030.json -100 100 2000 -100 100 100 0.01   0 6 3     0.5
sagitta2_mem_M50_p7_0031.json -100 100 2000 -100 100 2000 0.01  0 1 1     0
#'''
REAL = True

def doo_map(params):
    params = list(params)
    kparams = [nnp2] + params[:3] + [nnp3] + params[3:]
    print('dkmap:', kparams)
    if REAL:
        try:
            kmap.dkmap(*kparams)
        except:
            ttbb.format_exc()

def parse_list(poslist):
    ll = poslist.split('\n')
    todo = []
    for l in ll:
        if l.startswith('#'):
            continue
        (pn, ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3) = lv = l.strip().split()
        (ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3) = map(float, lv[1:])
        n2 = int(n2)
        n3 = int(n3)
        th_interv = int(th_interv)
        flv = (pn, ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3)
        pprint(flv)
        pprint(map(type, flv))
        todo.append(flv)
    return todo

def make_dsname(pn):
    dsname = f'{pn}_{key}'
    dsname = dsname.replace('.','_')
    return dsname

def process_th_series(list_index, ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3):
    zeronnp()
    curr_nnp3 = nnp3.position
    th_vals = np.linspace(th_start, th_stop, th_interv+1)
    nnp3_vals = np.linspace(curr_nnp3, curr_nnp3 + th_interv*delta_nnp3, th_interv+1)
    print('th_vals:')
    pprint(th_vals)
    print('nnp3_vals:')
    pprint(nnp3_vals)
    for j in range(len(th_vals)):
        print(f'**** th cycle: {list_index}.{j} **********\n#')
        print(f'targets:  th={th_vals[j]}  nnp3={nnp3_vals[j]}')
        if REAL or 1:
            mv(Theta, th_vals[j])
            mv(nnp3, nnp3_vals[j])
        print(f'actual:  th={Theta.position}  nnp3={nnp3.position}')
        doo_map((ll2, ul2, n2, ll3, ul3, n3, expt))

def process_list(idxl, todo):
    for i, t in zip(idxl,todo):
        print(f'#\n#\n# list cycle: {i} ==================================\n#\n#')
        pprint(t)
        (pn, ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3) =\
            t
        if REAL or 1:
            rstp(pn)
            diffp(pn)
        dsname = make_dsname(pn)
        print('dsname =', dsname)
        if REAL:
            newdataset(dsname)
        process_th_series(i, ll2, ul2, n2, ll3, ul3, n3, expt, th_start, th_stop, th_interv, delta_nnp3)

def may1_main():
    try:
        if REAL:
            so()
            sleep(3)
        doo = list(range(1,7))
        todo = parse_list(M50_LIST)
        flt_todo = [todo[i] for i in doo]
        process_list(doo, flt_todo)
    finally:
        sc()
        sc()
        sc()
        fshtrigger()
