print('loading eigfix interface')

import gevent

def eig_hws():
    eigerhws(True)

def eig_sows():
    eigerhws(False)

'''
def mgeig_hws_x():
    eiger.saving.file_format='HDF5BS'
    eiger.camera.dynamic_pixel_depth=True
    mgeig()
    eig_sows()
    eiger.proxy.saving_use_hw_comp=True
    eiger.camera.compression_type='BSLZ4'
    eig_hws()
    MG_EH3a.enable("*xmap3*")
'''

def eig_hws_kmap(mot1, ll1, ul1, n1, mot2, ll2, ul2, n2, expt, frames_per_file=200, probeonly=False):
    print("="*40, '[eiger hardweare saving scan]', '==========')
    try:
        try:
            msg = f'NEW_HWS_SCAN: This is scan number #{(int(SCANS[-1].scan_number))+1}'
            print(msg)
            dooc(msg)
        except:
            msg = f'NEW_HWS_SCAN: cannot determine scan number (probably due to fresh start ...)'
            print(msg)
            dooc(msg)
    except:
        # We never want it to fail because of fancy ...
        pass

    nn = n1*n2
    (nfiles, nfrac) = divmod(nn, frames_per_file)
    if probeonly:
        if not 0 == nfrac:
            print(f"Warning this scan would fail (nframes/frames_per_file={nn/frames_per_file} is non integer)")
        else:
            print(f"frames_per_file is okay (multiple of {frames_per_file})")
        return
    else:
        if not 0 == nfrac:
            raise ValueError('illegal number of frames per file (nframes/{frames_per_file} is non integer)!')
        else:
            kmap.dkmap(mot1, ll1, ul1, n1, mot2, ll2, ul2, n2, expt,
                frames_per_file=frames_per_file)



SOME_DOC = """
server:
latest eiger dev with hw compression (compiles in the afternoon)
mitigation=off
maxcpu=32

test1:
with bench(): kmap.dkmap(nnp2,-20, 20, 900, nnp3, 0, 0, 1, 0.02, frames_per_file=300)

# default start

eiger.saving.file_format='HDF5BS'
eiger.camera.dynamic_pixel_depth=True
mgeig()
eiger.proxy.saving_use_hw_comp=True
eiger.camera.compression_type='BSLZ4'
eig_hws()
MG_EH3a.enable("*xmap3*")

# example:
# kmap.dkmap(nnp2,-20, 20, 400, nnp3, -20, 20, 400, 0.005, frames_per_file=2000)

common:
initialization: webmin
eiger.saving.file_format='HDF5BS'
eig_sows()
eiger.camera.dynamic_pixel_depth=True
mgeig()

worked 1:
eiger.proxy.saving_use_hw_comp=True
eiger.camera.compression_type='BSLZ4'
worked 2:
eiger.proxy.saving_use_hw_comp=False
eiger.camera.compression_type='BSLZ4'
failed 1:
eiger.camera.dynamic_pixel_depth=True
eiger.proxy.saving_use_hw_comp=False
eiger.camera.compression_type='LZ4'


"""
