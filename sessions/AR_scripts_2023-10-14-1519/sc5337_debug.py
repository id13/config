import gevent, bliss

def debug_once():
    try:
        with gevent.Timeout(seconds=60):
            eig_hws_kmap(nnp2,-20, 20,100 , nnp3, -20, 20, 100, 0.02,
                probeonly=False)    
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print('caught the timeout ...')
    try:
        with gevent.Timeout(seconds=300):
            eig_hws_kmap(nnp2,-20, 20,100 , nnp3, -20, 20, 100, 0.02,
                probeonly=False)    
    except bliss.common.greenlet_utils.killmask.BlissTimeout:
        print('caught the timeout ...')

    

