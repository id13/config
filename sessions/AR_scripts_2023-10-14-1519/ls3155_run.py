print('ls3155 load 5')


start_key = 'c'


ALL_WAXS_list = [
'OR_1L_2R_pos3_50x_0022.json',
'OR_1L_2R_pos3_5x_0010.json',
'OR_1L_2R_pos4_50x_0023.json',
'OR_1L_2R_pos4_5x_0011.json',
'OR_1L_2R_pos5_50x_0024.json',
'OR_1L_2R_pos5_5x_0012.json',
'OR_1L_2R_pos6_50x_0025.json',
'OR_1L_2R_pos6_5x_0014.json',
]
WAXS_list = [x for x in ALL_WAXS_list if '50x' in x]

ALL_WAXS_list_oversampling = [
'OR_1L_2R_pos8_50x_0029.json',
'OR_1L_2R_pos8_5x_0030.json',
'OR_1L_2R_pos9_50x_0032.json',
'OR_1L_2R_pos9_5x_0031.json',
'OR_1L_2R_pos10_50x_0034.json',
'OR_1L_2R_pos10_5x_0033.json',
'OR_1L_2R_pos11_50x_0036.json',
'OR_1L_2R_pos11_5x_0035.json',
'OR_1L_2R_pos12_50x_0038.json',
'OR_1L_2R_pos12_5x_0037.json',
'OR_1L_2R_pos13_50x_0040.json',
'OR_1L_2R_pos13_5x_0039.json',
]
WAXS_list_oversampling = [x for x in ALL_WAXS_list_oversampling if '50x' in x]

WAXS_list_tubuli = [
'tubuli_M1_pos1_50x_0003.json',
'tubuli_M1_pos1_5x_0004.json',
'tubuli_M1_pos2_50x_0006.json',
'tubuli_M1_pos2_5x_0005.json',
'tubuli_M1_pos3_50x_0008.json',
'tubuli_M1_pos3_5x_0007.json',
'tubuli_M1_pos4_50x_0011.json',
'tubuli_M1_pos4_5x_0009.json'
]
WAXS_list_tubuli = [x for x in WAXS_list_tubuli if '50x' in x]

def verify():
    pprint(WAXS_list)


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,-1*yrange,yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    #mgroieiger()
    name = f'{pos}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def run_Feb17_evening():
    try:
        #umv(ndetx,-275)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,40,40,0.5,0.1,0.02)
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)
	    
        for i in WAXS_list_oversampling:
            gopos_kmap_scan(i,start_key,0,20,20,0.5,0.05,0.02)	
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)
    
    finally:
        sc()
        sc()
        sc()

def run_Feb18_noon():
    try:
        for i in WAXS_list_tubuli:
            gopos_kmap_scan(i,start_key,0,15,15,0.1,0.5,0.01)
            print('10 sec left to interrupt ...')
            sleep(10)
            print('interrupt time over - do not interrupt ...')
            sleep(1)        
    finally:
        sc()
        sc()
        sc()
