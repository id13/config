def gc_focus(xmot, xstart, xstep, xnpts, scanmot, *p):
    doos('gc_focus')
    for i in range(xnpts):
        print('3 sec to interrupt ...')
        sleep(3)
        next_xpos = xstart + i * xstep
        if xmot.name in ['nnp1', 'nnp5', 'nnx']:
            mv(xmot, next_xpos)
        else:
            raise ValueError(f'illegal motor {xmot} ({xmot.name})')
            return
        try:
            scanno = SCANS[-1].scan_number
            scanno = int(scanno)

        except IndexError:
            scanno = -1
        except:
            scanno = -2
        s = f'xindex: {i}    nominal_xpos {next_xpos}    xpos = {xmot.position}   #S{scanno} ========='
        print(s)
        doos(s)
        doos(s)
        dscan(scanmot, *p)
        goc(scanmot)
        sync()
        elog_plot(scatter=False)
        sync()
