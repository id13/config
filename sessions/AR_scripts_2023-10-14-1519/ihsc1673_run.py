# this is the script for inhouse experiment 1673
print('ihsc1673 is loaded (7)')
start_key = 'a'

def getpos(pos):
    '''
    go to the top of scanning range
    '''
    pre_name = f'{pos[:-10]}'
    gopos(pos)
    umvr(ustry,0.007)
    return pre_name

def raster_scan(yrange,zrange,ystep_size,zstep_size,exp_t,retveloc,frames_per_file):
    '''
    the scan separat to multiple blocks in Z direction
    each block size is defined by yrange and zrange
    interval size is defined by ystep_size and zstep_size
    '''
    ypts = int(yrange/ystep_size)
    zpts = int(zrange/zstep_size)
    y_low  = -yrange/2
    y_high = yrange/2
    z_low  = -zrange/2
    z_high = zrange/2 
    #in the eh2 dkmap does not exist
    #kmap.dkmap(ustry,y_low,y_high,ypts,ustrz,z_low,z_high,zpts,exp_t)
    dkmapyz_2(y_low,y_high,ypts,z_low,z_high,zpts,exp_t,
              retveloc=retveloc,frames_per_file=frames_per_file)

def check_interrupt_scan(yrange,zrange,
                   ystep_size,zstep_size,exp_t,
                   retveloc,frames_per_file):
    '''
    for each block, there will be a rotation to collect the projection at different angle
    the rotation always start from -180
    rot_step is the angle resolution of rotation unit is degree
    '''
    print(f'............................ recording angle: {_}')
    try:
        f = open('stop_it')
        enddataset()
        return 'stopit'
    except:
        pass
    raster_scan(yrange,zrange,ystep_size,zstep_size,exp_t,
               retveloc=retveloc,frames_per_file=frames_per_file)

def total_scan(pos_list,yrange_list,zrange_list,
               ystep_size_list,zstep_size_list,
               exp_t,
               start_key=start_key,
               retveloc=5,
               frames_per_file=2000, 
               already_done=0):
    '''
    all the transition motors have unit of millimeter
    go to the center of total scan range, yrange and zrange is the size of scan meshgrid
    zblock_size is a list including the information of size of each block in Z direction
    zblock_space is a list including the information of distance between each blocks, length of the list 
    should 1 less than the length of the list of zblock_size
    '''
    try:
        so()
        fshtrigger()
        mgeig()
        for _ in range(len(pos_list)):
            print(f'====================================== processing scan: {_}')
            if already_done > _:
                print(f'====================================== skipping scan: {_}')
            else:
                print(f'====================================== recording scan: {_}')
                prename = getpos(pos_list[_])
                dataset_name = f'{prename}_{start_key}'
                yrange = yrange_list[_]
                zrange = zrange_list[_]
                ystep_size = ystep_size_list[_]
                zstep_size = zstep_size_list[_]
                newdataset(dataset_name)
                if check_interrupt_scan(yrange,zrange,ystep_size,zstep_size,exp_t,
                               retveloc=retveloc,frames_per_file=frames_per_file) == 'stopit':
                    print('okay - bailing out ...')
                    break
                enddataset()
        sc()
    finally:
        sc()
        sc()

#basing the radiation damage test, 0.02 sec exposure should be torelantable for sample
def run_scan_layer():
    
    pos_list = [
    'CNF_MX_layer_CNF_pos2_LR_0008.json',
    'CNF_MX_layer_MX_pos2_LR_0006.json',
    'CNF_MX_layer_CvM_8t2_pos2_LR_0004.json',
    'CNF_MX_layer_CvM_4t6_pos2_LR_0001.json',
    ]
    
    umv(udetx,-870) 
    
    already_done = 0
    yrange_list  = [0.15 for x in range(4)]
    zrange_list  = [0.12 for x in range(4)]
    ystep_size_list   = [0.0025 for x in range(4)]
    zstep_size_list   = [0.0025 for x in range(4)]
    exp_t        = 0.02
    retveloc     = 5
    frames_per_file = 2000
    total_scan(pos_list,yrange_list,zrange_list,
               ystep_size_list,zstep_size_list,
               exp_t,start_key=start_key,
               retveloc=retveloc,frames_per_file=frames_per_file,
               already_done=already_done)
    
    
    pos_list = [
    'CNF_MX_layer_CvM_4t6_pos3_LR_0003.json',
    'CNF_MX_layer_CvM_8t2_pos3_LR_0005.json',
    'CNF_MX_layer_MX_pos3_LR_0007.json',
    'CNF_MX_layer_CNF_pos3_LR_0009.json',
    ]
   
    umv(udetx,-65) 
    already_done = 0
    yrange_list  = [0.15 for x in range(4)]
    zrange_list  = [0.12 for x in range(4)]
    ystep_size_list   = [0.0025 for x in range(4)]
    zstep_size_list   = [0.0025 for x in range(4)]
    exp_t        = 0.02
    retveloc     = 5
    frames_per_file = 2000
    total_scan(pos_list,yrange_list,zrange_list,
               ystep_size_list,zstep_size_list,
               exp_t,start_key=start_key,
               retveloc=retveloc,frames_per_file=frames_per_file,
               already_done=already_done)

def run_scan_fiber():
    #pos_list = ['cellulose_fiber_sample_lignin50_pos1_0000.json']
    #pos_list = ['cellulose_fiber_sample_lignin50_pos2_0001.json']
    #pos_list = ['cellulose_fiber_sample_lignin50_pos3_0002.json']
    #pos_list = [
    #'cellulose_fiber_sample_lignin30_pos1_0007.json',
    #'cellulose_fiber_sample_lignin10_pos1_0014.json',
    #'cellulose_fiber_sample_cell100_pos1_0020.json',
    #]
    
    #pos_list = [
    #'cellulose_fiber_sample_A1_pos1_0026.json',
    #'cellulose_fiber_sample_A2_pos1_0032.json',
    #'cellulose_fiber_sample_A3_pos1_0038.json',
    #]

    #pos_list = [
    #'cellulose_fiber_sample_A1_pos3_0028.json',
    #'cellulose_fiber_sample_A2_pos3_0034.json',
    #'cellulose_fiber_sample_A3_pos3_0040.json',
    #]
    
    #pos_list = [
    #'cellulose_fiber_sample_cell100_pos4_0023.json',
    #'cellulose_fiber_sample_lignin10_pos4_0017.json',
    #'cellulose_fiber_sample_lignin30_pos4_0010.json',
    #'cellulose_fiber_sample_lignin50_pos5_0004.json',
    #]
    
    pos_list = [
    'cellulose_fiber_sample_lignin50_SAXS_pos3_0083.json'
    ]
    
    already_done = 0
    yrange_list  = [0.06 for x in range(4)]
    zrange_list  = [0.06 for x in range(4)]
    ystep_size_list   = [0.0025 for x in range(4)]
    zstep_size_list   = [0.0025 for x in range(4)]
    #yrange_list  = [0.045 for x in range(3)]
    #zrange_list  = [0.04 for x in range(3)]
    #ystep_size_list   = [0.0025 for x in range(3)]
    #zstep_size_list   = [0.0025 for x in range(3)]
    exp_t        = 0.02
    retveloc     = 5
    frames_per_file = 2000
    total_scan(pos_list,yrange_list,zrange_list,
               ystep_size_list,zstep_size_list,
               exp_t,start_key=start_key,
               retveloc=retveloc,frames_per_file=frames_per_file,
               already_done=already_done)
    
    #pos_list = [
    #'cellulose_fiber_sample_cell100_pos2_0021.json',
    #'cellulose_fiber_sample_lignin10_pos2_0015.json',
    #'cellulose_fiber_sample_lignin30_pos2_0008.json',
    #]
    
    #pos_list = [
    #'cellulose_fiber_sample_A1_pos2_0027.json',
    #'cellulose_fiber_sample_A2_pos2_0033.json',
    #'cellulose_fiber_sample_A3_pos2_0039.json',
    #]
    
    #pos_list = [
    #'cellulose_fiber_sample_A1_pos4_0030.json',
    #'cellulose_fiber_sample_A2_pos4_0035.json',
    #'cellulose_fiber_sample_A3_pos4_0041.json',
    #]
    
    #pos_list = [
    #'cellulose_fiber_sample_cell100_pos5_0024.json',
    #'cellulose_fiber_sample_lignin10_pos5_0018.json',
    #'cellulose_fiber_sample_lignin30_pos5_0011.json',
    #'cellulose_fiber_sample_lignin50_pos6_0005.json',
    #]
    
    pos_list = [
    'cellulose_fiber_sample_lignin50_SAXS_pos2_0082.json'
    ]
    
    already_done = 0
    yrange_list  = [0.06 for x in range(4)]
    zrange_list  = [0.06 for x in range(4)]
    ystep_size_list   = [0.001 for x in range(4)]
    zstep_size_list   = [0.0025 for x in range(4)]
    #yrange_list  = [0.045 for x in range(3)]
    #zrange_list  = [0.04 for x in range(3)]
    #ystep_size_list   = [0.001 for x in range(3)]
    #zstep_size_list   = [0.0025 for x in range(3)]
    exp_t        = 0.02
    retveloc     = 5
    frames_per_file = 2000
    total_scan(pos_list,yrange_list,zrange_list,
               ystep_size_list,zstep_size_list,
               exp_t,start_key=start_key,
               retveloc=retveloc,frames_per_file=frames_per_file,
               already_done=already_done)
