fromm scipy.optimize import leastsq

def rfsin(p, y, x):
    return y - fsin(x, p)

def fsin(x, p):
    print("fsin x arrg:")
    print(x)
    A,ph,o = tuple(p)
    print("A,Ph,o=", A,ph,o)
    return A*np.sin(x-ph) + o

def sinfit(xarr, yarr, p=np.array):
    pl = leastsq(rfsin, p, args=(yarr, xarr))
    amp = pl[0][0]
    phase = pl[0][1]
    offset = pl[0][2]
    return (amp, phase, offset)

def plotfit(x, y1, y2):
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.show()

