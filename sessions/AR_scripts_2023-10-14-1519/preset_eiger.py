from bliss.scanning.chain import ChainPreset

class Preset_eiger(ChainPreset):
  def prepare(self,acq_chain):
    print("... Preset_eiger - prepare")
    pass

  def start(self,acq_chain):
    print("... Preset_eiger - start")
    pass

  def stop(self,acq_chain):
    print("... Preset_eiger - stop")
    pass

