print('sc5122_ver_cb_823_5.py load 3')

from math import *

def shortmesh(theta,lsr,hsr):
  
  theta_rad = theta * pi/180 
  wf = cos(fabs(theta_rad))
  
  c_lsr = lsr * wf
  c_hsr = hsr * wf
  
  start = lsr - c_lsr
  end = start + c_lsr + c_hsr
  
  images = 397
  
  exptime = 0.01 * wf
  
  print('scanning from %f to %f with %f exptime' %(start,end,exptime))
  dkmapyz_2(start,end,images-1,0,0.2,9,exptime,frames_per_file=397, retveloc=2.0)


def partial_theta_scan(bunch,lsr,hsr):

  theta = -30
  umv(usrotz,theta)
  print('dd%s_thm%02d' %(bunch,-theta))
  newdataset('dd%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  theta = -45
  umv(usrotz,theta)
  print('dd%s_thm%02d' %(bunch,-theta))
  newdataset('dd%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  
  umv(usrotz,0)

  
def theta_scan(bunch,lsr,hsr):

  theta = 0
  umv(usrotz,theta)
  print('dd%s_th%02d' %(bunch,theta))
  newdataset('dd%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  
  theta = 15
  umv(usrotz,theta)
  print('dd%s_th%02d' %(bunch,theta))
  newdataset('dd%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  theta = 30
  umv(usrotz,theta)
  print('dd%s_th%02d' %(bunch,theta))
  newdataset('dd%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  theta = 45
  umv(usrotz,theta)
  print('dd%s_th%02d' %(bunch,theta))
  newdataset('dd%s_th%02d' %(bunch,theta))
  shortmesh(theta,lsr,hsr)
  
  theta = -15
  umv(usrotz,theta)
  print('dd%s_thm%02d' %(bunch,-theta))
  newdataset('dd%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  theta = -30
  umv(usrotz,theta)
  print('dd%s_thm%02d' %(bunch,-theta))
  newdataset('dd%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  theta = -45
  umv(usrotz,theta)
  print('dd%s_thm%02d' %(bunch,-theta))
  newdataset('dd%s_thm%02d' %(bunch,-theta))
  shortmesh(theta,lsr,hsr)
  
  if bunch == 10:
    theta = 0
    umv(usrotz,theta)
    print('dd_chk%s_th%02d' %(bunch,theta))
    newdataset('dd%s_th%02d' %(bunch,theta))
    shortmesh(theta,lsr,hsr)
  
  umv(usrotz,0)

  
def sample_mesh(startcoord,lsr,hsr,steps):
  
  try:
    so()
    eigerhws(True)
    mgeig()
    #rstp(startcoord)

    print('partially scanning bunch 5')
    partial_theta_scan(20,lsr,hsr)
    umvr(ustrz,0.22)

    for i in range(21, steps):
      print('scanning bunch %i' %i)
      theta_scan(i,lsr,hsr)
      umvr(ustrz,0.22)
    enddataset()
    sc()
        
  finally:
    sc()
    sc()
    sc()
  
  
  
