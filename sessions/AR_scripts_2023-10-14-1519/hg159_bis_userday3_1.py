import time

class EXPO:

    expt = 0.02

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_bis_userday3_1.py ARMED ===")
print('RESUME1')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 1.0

    #if expt < 0.01:
    #    expt = 0.01
    #if expt < 0.02 and nitv < 401:
    #    retveloc = 1.0
    #else:
    #    retveloc = 0.5

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='c' # to be incremented if there is a crash

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_bis_userday3_1():


    EXPO.expt = 0.06

    dooul('Pa_thin_OV_PaDrLi_ul02_1x1_150x150_300x300_60_0047.json')
    dooul('Pa_thin_OV_PaDr_ul03_1x1_250x175_500x350_60_0048.json')
    dooul('Pa_thin_OV_PaWe_ul01_1x1_150x100_300x200_60_0046.json')



  






   
    




def hg159_day5_1_check_pos():
    pass
    #gopos('')
