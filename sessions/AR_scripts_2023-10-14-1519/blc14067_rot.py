print('load tomo scan 2')

def tomoscan_mgd(pos,yrange,zrange,exp_t,ystep,zstep,ang_range,ang_step):
    try:
        so()
        mgd()
        gopos(pos)
        for _ in np.arange(ang_range[0],ang_range[1],ang_step):
            umv(usrotz,_)
            newdataset('absoprtion_tomo2_th%02d' %_)
            dkmapyz_2(-yrange,yrange,int(2*(yrange/ystep)-1),
                      -zrange,zrange,int(2*(zrange/zstep)-1),exp_t,retveloc=5)
        sc()
    finally:
        sc()
        sc()

def tomoscan_mgeig(pos,yrange,zrange,exp_t,ystep,zstep,ang_range,ang_step):
    try:
        so()
        mgeig()
        gopos(pos)
        for _ in np.arange(ang_range[0],ang_range[1],ang_step):
            umv(usrotz,_)
            gopos(pos)
            newdataset('scattering_tomo_th%02d' %_)
            dkmapyz_2(-yrange,yrange,int(2*(yrange/ystep)-1),
                      -zrange,zrange,int(2*(zrange/zstep)-1),exp_t,retveloc=5)
        sc()
    finally:
        sc()
        sc()
