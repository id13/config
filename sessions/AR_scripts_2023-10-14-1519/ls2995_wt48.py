print('ls2995 v.38')
launch_prefix = 'a'
def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-10]
    s.replace('-','_')
    nl  = s.split('_')
    sn  = launch_prefix + '_' + nl[1]+'_'+nl[2] #x[:-17]#sample name
    rsl = nl[-2] #resolution
    if rsl == 'lr':
        patch_size = 1 
    if rsl == 'hr':
        patch_size = 0.4
    nn = nl[-1].split('x')
    pr  = int(nn[0])  #patch_row
    pc  = int(nn[1]) #patch_col
    print('\n\n',sn,patch_size,pr,pc,'\n\n')
    gopos(x)
    sleep(1.0)
    
    newdataset(sn)
    fshtrigger()
    return patch_size,pr,pc


def run_patch_scan(json_file):
    try:
    #pc is column and pr is row of scan, which correlate step at y and z in path scan
        so()
        patch_size,pr,pc = tgopos(json_file)
        mgeig()
        dkpatchmesh(patch_size,200,patch_size,200,0.02,pc,pr,retveloc=0.5)
        enddataset()
        
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()


def ls2995_wt48():

    kapton_pos = 'empty'
    gopos(kapton_pos)
    newdataset('kapton_bkg')
    mgeig()
    loopscan(200,0.02)
    enddataset()    
    
    dam_pos = 'WT48_dam1_wt48_0040.json'
    gopos(dam_pos)
    mgeig()
    newdataset('dam1')
    for i in range(4):
        loopscan(200,0.02)
        umvr(ustry,0.03)
    enddataset()
    
    l = [
    'WT48_mac1_wt48hr1_hr_1x1_0032.json',
    'WT48_mac1_wt48hr2_hr_1x1_0038.json',
    'WT48_mac1_wt48hr3_hr_1x1_0039.json',
    'WT48_mac1_wt48lr1_lr_2x4_0015.json',
    'WT48_mac1_wt48lr2_lr_1x1_0033.json',
    ]
    for i in l:
        run_patch_scan(i)
