################
# for theta and y mesh scan

print("sc5119_mro june 8 night load 7")

START_KEY = 'f'
EXP_T     = 0.02
NITV1     = 500
NITV2     = 20
#NITV3     = 1
SZ        = 125 
DZ        = 20
SY        = 125
DY        = 5
STh       = -50
ETh       = 50
mov_mot1  = nnp3
mov_mot2  = nnp2
    
def theta_y_mesh(
                EXP_T    , 
                NITV1    ,
                NITV2    ,
                SZ       , 
                DZ       ,
                SY       ,
                DY       , 
                STh      , 
                ETh      , 
                mov_mot1 , 
                mov_mot2 ,
                ):
    DZ_N = DZ/NITV1
    DTh  = np.abs(STh-ETh)/NITV1
    umv(Theta,STh-1)
    umv(Theta,STh)
    for i in range(NITV1):
        umv(mov_mot1, SZ  + i*DZ_N)
        umv(Theta, STh + i*DTh)
        frames_per_file = int(NITV2 + 1)
        eiger.saving.frames_per_file = frames_per_file
        
        umv(mov_mot2,SY)
        if mov_mot2.name == 'nnp2':
          LY = DY
          HY = -1*DY
        else:
          LY = -1*DY
          HY = DY
        dscan(mov_mot2,LY,HY,NITV2,EXP_T)
        
def do_theta_y_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                ):
    try:
        dsname = f'{pname}_{START_KEY}'
        newdataset(dsname)
        sync()
        zeronnp()
        fshtrigger()
        so()
        mgeig()
        theta_y_mesh(
                  EXP_T     = EXP_T,
                  NITV1     = NITV1,
                  NITV2     = NITV2,
                  SZ        = SZ, 
                  DZ        = DZ,
                  SY        = SY,
                  DY        = DY,
                  STh       = STh,
                  ETh       = ETh,
                  mov_mot1  = mov_mot1,
                  mov_mot2  = mov_mot2,)
        zeronnp()
        enddataset()
        sc()    
    finally:
        eiger.saving.frames_per_file = 200
        enddataset()
        sc()
        sc()
        
def do_theta_y_scan_mro_pos(pos,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                ):
    # this will go to pos and run the scan
    umv(Theta,-1)
    umv(Theta,0)
    gopos(pos) 
    pname = pos[:-5]
    do_theta_y_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                )
    umv(Theta,-1)
    umv(Theta,0)
    
    
vpos1 = [
'ib334a_THM_p04_0004.json',
'ib334a_THM_p05_0005.json',
'ib334a_THM_p07_0007.json',
'ib334a_THM_p08_0008.json',
'ib334a_THM_p09_0009.json',
'ib334a_THM_p10_0010.json',
'ib334a_THM_p11_0011.json',
'ib334a_THM_p01_0001.json',
]

hpos1 = [
'ib334a_THM_p02_0002.json',
'ib334a_THM_p03_0003.json',
'ib334a_THM_p06_0006.json',
'ib334a_THM_p12_0012.json',
]


def night_run():
  for p1 in hpos1:
    do_theta_y_scan_mro_pos(p1,mov_mot1=nnp2,mov_mot2=nnp3,SZ=120,SY=130)
    
  for p2 in vpos1:
    do_theta_y_scan_mro_pos(p2)

# the night run failed at p10 125+380*0.02 Theta 25.8 degree
vpos2 = [
'ib334a_THM_p11_0011.json',
'ib334a_THM_p01_0001.json',
'ib334a_THM_p10_0010.json',
]

vpos3 = [
'ib334a_THM_p10_0010.json',
]

vpos4 = [
'ib334a_THM_21_0021.json',
]    

def morning_run():
    
  for p2 in vpos4:
    do_theta_y_scan_mro_pos(p2)
  
'''
!!! === RuntimeError: nnp2: discrepancy between dial (120.000000) and controller position (121.040939), aborting === !!! ( for more details type cmd 'last_error' )
'''
