import math
import time
#from bliss.setup_globals import *

class Timeout(object):
    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()


class DetError(Exception): pass

class EH3DetTable(object):

    def __init__(self, nweigermot=None, nwarmmot=None, nwpcomot=None):
        assert nweigermpot.name == 'nweiger'
        assert nweigermpot.name == 'nwarm'
        assert nweigermpot.name == 'nwpco'
        
        self.ymot_dc = dict((x.name[2:],x) for x in (nweigermot, nwarmmot, nwpcomot))
        self.timeout = 5.0
        self.mv_timeout = 20.0
        self.eps = 0.04
        self.to = Timeout(timeout=self.timeout)
        self.mv_to = Timeout(timeout=self.timeout)

    def _check_table_key(self, k):
        return k in ('eiger', 'pco', 'arm')

    def x_check_table_key(self, k):
        if k not in ('eiger', 'pco', 'arm'):
            raise DetError('illegal table key')

    def bstatus(self, k):
        return int(wcid13f.get(f'pb{k}'))

    def wstatus(self, k):
        return int(wcid13f.get(f'pw{k}'))

    def expect_wstatus(self, k, expectv):
        to = self.to
        to.reset()
        i = 0
        while not to.check_timeout()[0]:
            print(f'wcyc {i}')
            new_stat = self.wstatus(k)
            if new_stat == expectv:
                return 1
            time.sleep(0.1)
            i += 1
        return 0
    
    def expect_bstatus(self, k, expectv):
        to = self.to
        to.reset()
        i = 0
        while not to.check_timeout()[0]:
            print(f'bcyc {i}')
            new_stat = self.bstatus(k)
            if new_stat == expectv:
                return 1
            time.sleep(0.1)
            i += 1
        return 0
    
    def break_on(self, k):
        wcid13f.set(f"b{k}", 1)
        return self.expect_bstatus(k, 1)

    def wheel_on(self, k):
        wcid13f.set(f"w{k}", 1)
        return self.expect_wstatus(k, 1)

    def break_off(self, k):
        wcid13f.set(f"b{k}", 0)
        return int(self.expect_bstatus(k, 0))

    def wheel_off(self, k):
        wcid13f.set(f"w{k}", 0)
        return int(self.expect_wstatus(k, 0))

    def yon(self,k):
        self.x_check_table_key(k)
        if self.wheel_on(k):
            if self.break_off(k):
                return 1
            else:
                print(f'break off failed for {k}')
                return 0
        else:
            print(f'wheel on failed for {k}')
            return 0
        
    def yoff(self, k):
        self.x_check_table_key(k)
        if self.break_on(k):
            if self.wheel_off(k):
                return 1
            else:
                print(f'wheel off failed for {k}')
                return 0
        else:
            print(f'break on failed for {k}')
            return 0

    def ison(self, k):
        b_stat = self.bstatus(k)
        w_stat = self.wstatus(k)
        return (0, 1) == (b_stat, w_stat)

    def isoff(self, k):
        b_stat = self.bstatus(k)
        w_stat = self.wstatus(k)
        return (1, 0) == (b_stat, w_stat)

    def ymvr(self, k, rel_tgt):
        mot = self.ymot_dc[k]
        syncwheels()
        curr_pos = mot.position
        tgt = curr_pos + rel_tgt
        self.ymv(k, tgt)
        syncwheels()

    def ywm(self, k, verbose=True):
        mot = self.ymot_dc[k]
        syncwheels()
        curr_pos = mot.position
        if verbose:
            print(f'current {k} position: {curr_pos}')
        return curr_pos

    def ymv(self, k, tgt):
        if self.ison(k):
            syncwheels()
            mot = self.ymot_dc[k]
            print(f'mv({mot} {tgt}]')
            for i in range(3):
                syncwheels()
                mv(mot, tgt)
                syncwheels()
                print(f'mv cyc {i} target {tgt} current_pos {mot.position:10.4f}')
            mv_to = self.mv_to
            mv_to.reset()
            while not mv_to.check_timeout()[0]:
                syncwheels()
                mv(mot, tgt)
                syncwheels()
                current_pos = mot.position
                discrepancy = np.fabs(current_pos - tgt)
                if discrepancy < self.eps:
                    print(f'timeout loop - done discrepancy={discrepancy:10.4f}: mv cyc {i} target {tgt} current_pos {current_pos:10.4f}')
                    return 1
            if discrepancy > 1.0:
                print(f'failed - discrepancy={discrepancy:10.4f}: mv cyc {i} target {tgt} current_pos {current_pos:10.4f}')
                return 0
            print(f'Warning - discrepancy={discrepancy:10.4f}: mv cyc {i} target {tgt} current_pos {current_pos:10.4f}')
            return 1
        else:
            print(f'ymv {k} {tgt} failed (axis isnot "on")')
            return 0

    @property
    def pco_position(self):
        return self.ywm('pco')

    @pco_position.setter
    def pco_position(self, pos):
        self.ymv('pco', pos)

    @property
    def eiger_position(self):
        return self.ywm('eiger')

    @eiger_position.setter
    def eiger_position(self, pos):
        self.ymv('eiger', pos)

    @property
    def arm_position(self):
        return self.ywm('arm')

    @arm_position.setter
    def arm_position(self, pos):
        self.ymv('arm', pos)

    def create_softaxis(self, k):
        return SoftAxis(f'mot_{k}', self, position=f'{k}_position', move=f'{k}_position', tolerance=0.05)
