print('ls2952_fib_mount_01 load 2')

STARTKEY = 'a'


def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_fib_mount_01():

    gop_newds('fib_mount_01_A2_scanstart_main_0013.json')
    dkpatchmesh(0.8, 400, 1.7, 170, 0.02, 1,1)
    enddataset()

    gop_newds('fib_mount_01_A3_scanstart_main_0014.json')
    dkpatchmesh(0.8, 400, 1.4, 140, 0.02, 1,1)
    enddataset()

    gop_newds('fib_mount_01_A4_scanstart_main_0018.json')
    dkpatchmesh(0.8, 400, 1.8, 180, 0.02, 1,1)
    enddataset()

    gop_newds('fib_mount_01_C1_scanstart_main_0021.json')
    dkpatchmesh(0.8, 400, 1.6, 160, 0.02, 1,1)
    enddataset()

    gop_newds('fib_mount_01_C2_scanstart_main_0024.json')
    dkpatchmesh(0.8, 400, 1.6, 160, 0.02, 2,1)
    enddataset()
    

