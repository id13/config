def night_macro():
	try:
		so()
		startkey = 'a'
		
		rstp('pos1_x50')
		newdataset(f'pos1_u18_7p96_ct0p02_{startkey}')
		umv(u18, 7.96)
		dkmapyz_2(-0.1,0.1,200,-0.1, 0.1, 200,0.02)
		
		newdataset(f'pos1_u18_8p04_ct0p02_{startkey}')
		umv(u18, 8.04)
		dkmapyz_2(-0.1,0.1,200,-0.1, 0.1, 200,0.02)
		
		rstp('pos2_x50')
		newdataset(f'pos2_u18_8p04_ct0p2_{startkey}')
		dkmapyz_2(-0.1,0.1,200,-0.1, 0.1, 200,0.2)
		
		newdataset(f'pos2_u18_7p96_ct0p2_{startkey}')
		umv(u18, 7.96)
		dkmapyz_2(-0.1,0.1,200,-0.1, 0.1, 200,0.2)
		
	finally:
		sc()
		sc()
		sc()


def ihma371():
	try:
		so()
		startkey = 'a'
		
		newdataset(f'pos_a_u18_8p04_ct0p02_{startkey}')
		umv(u18, 8.04)
		dkmapyz_2(-0.05,0.05,100,-0.05, 0.05, 100,0.02)
		
		umvr(ustrz, 0.2)
		newdataset(f'pos_a_u18_8p04_ct0p2_{startkey}')
		dkmapyz_2(-0.05,0.05,100,-0.05, 0.05, 100,0.2)
		
		newdataset(f'pos_a_u18_7p96_ct0p2_{startkey}')
		umv(u18, 7.96)
		dkmapyz_2(-0.05,0.05,100,-0.05, 0.05, 100,0.2)
		
	finally:
		sc()
		sc()
		sc()
