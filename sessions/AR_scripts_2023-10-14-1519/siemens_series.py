print('siemens series load - 5')
import numpy as np
import tango

def siemens_series(xmot, xstart, xstep, xnpts, *p):
    doos('siemens_series')
    for i in range(xnpts):
        print('3 sec to interrupt ...')
        sleep(3)
        next_xpos = xstart + i * xstep
        if xmot.name in ['nnp1', 'nnp5']:
            mv(xmot, next_xpos)
        else:
            raise ValueError(f'illegal motor {xmot} ({xmot.name})')
            return
        try:
            scanno = SCANS[-1].scan_number
            scanno = int(scanno)

        except IndexError:
            scanno = -1
        except:
            scanno = -2
        s = f'xindex: {i}    nominal_xpos {next_xpos}    xpos = {xmot.position}   #S{scanno} ========='
        print(s)
        doos(s)
        doos(s)
        kmap.dkmap(*p)
        sync()
        elog_plot(scatter=True)
        sync()

#def doop(*args, **kw):
#    print(*args, **kw)
#    dooc(*args, **kw)

def sven_getflux(nmaxvalues=10, background=0, ringcurrent=0):
    acq_time = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:acq_time_3']
    the_data = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32']
    the_data = np.array(the_data) - background
    
    s = nmoco.comm_ack('?outbeam')
    w = s.split()
    gain = float(w[3])
    wbz = wbetz.position
    ll = []
    ll.append(f'==== FLUX (#{SCANS[-1].scan_number})')
    ll.append('nmoco gain: {0:f},  acq_time: {1:.2f} s,  energy: {2:.2f} keV'.format(gain, acq_time[0], ccmo.get_energy()))
    ll.append(f'ns6vg: {ns6vg.position:.3f}, ns6hg: {ns6hg.position:.3f}')

    if wbz > 3:
      ll.append('no prefocusing lens')
    elif wbz > -12:
      ll.append('1 prefocusing lens')
    elif wbz > -30:
      ll.append('2 prefocusing lenses')
    else:
      ll.append('3 prefocusing lenses')
    
    if ringcurrent > 1:
        ll.append('ring current: {0:.2f} mA'.format(ringcurrent))
    gain_fac = gain*1000.0*2.15e+6/acq_time
    sven_flux_arr = the_data*gain_fac
    ll.append(f'Total factor: {np.mean(gain_fac):g},  background: {background}')
    #ll.append(f'printing max {nmaxvalues} values ...')
    #for v in sven_flux_arr[:nmaxvalues]:
    #    ll.append("%g" % v)
    if len(the_data) > 1:
        ll.append(f'mean det  [cps]:  {np.mean(the_data):g}')
        ll.append(f'mean flux [ph/s]: {np.mean(sven_flux_arr):g}')
        if ringcurrent > 1:
            ll.append(f'mean flux [ph/s]: {np.mean(sven_flux_arr)*200./ringcurrent:g} (at 200 mA)')
    ll.append('\n')
    res = '\n'.join(ll)
    print(res)
    dooc(res)
    

def sven_ct(expt, n=1, ringcurrent='auto'):
    if 'auto' == ringcurrent:
        machinfo = tango.DeviceProxy("acs.esrf.fr:10000/fe/master/id13")
        ringcurrent = machinfo.SR_Current
    # measure background
    fshclose()
    sct(expt)
    background = SCANS[-1].get_data()['p201_eh3_0:ct2_counters_controller:ct32'][0]
    print(f'Background: {background}')
    
    fshtrigger()
    if n > 1:
        loopscan(n, expt)
        sven_getflux(n, background, ringcurrent)
    else:
        sct(expt)
        sven_getflux(background=background, ringcurrent=ringcurrent)
    
