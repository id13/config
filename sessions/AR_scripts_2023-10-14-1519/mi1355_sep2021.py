# #################### Alignment drops #9

def gos6tile(i, tsiz, basiz, offh, offv):
    mv(ns6vg, tsiz)
    mv(ns6hg, tsiz)
    mv(ns6ho, offh-0.5*basiz+tsiz*(0.5+i*0.5))
    mv(ns6vo, offv-0.5*basiz+tsiz*(0.5+i*0.5))
    print ('tile = i*0.5')

OFFH = 0.03250
OFFV = -0.30211
BASIZ = 0.1
def go_tile025(i):
    gos6tile(i, 0.025, BASIZ, OFFH, OFFV)
    
def go_voll(i):
    gos6tile(i, 0.1, BASIZ, OFFH, OFFV)
    

def drops9_al1_mvs6(x):
    dc = dict()
    dc['100mu'] = '0.10000   0.03250   0.10000  -0.30211'
    dc['35mu_cen'] = '0.03496  -0.28749   0.03504   0.22289'
    dc['35mu_dick'] = '0.03496  -0.32250   0.03504   0.18791'
    dc['35mu_duenn'] = '0.03496  -0.25248   0.03504   0.25790'
    dc['35mu_cen_duenn'] = '0.03496  -0.27048   0.03504   0.23989'
    dc['35mu_cen_dick'] = '0.03496  -0.30550   0.03504   0.20488'
    dc['initialopt'] = '0.10000  -0.28749   0.10000   0.22289'
    d9s6mv(dc[x])

def drops9_initialopt_nt2():
    """
    EH3 [95]: wm(nt2x, nt2j, nt2t)

               nt2x       nt2j      nt2t
--------  ---------  ---------  --------
User
 High       9.97436   10.00000   2.52210
 Current  -10.69919   -2.90002  -0.66300
 Low      -31.91391  -90.83672  -3.47790
Offset      0.00000   -0.64818   1.00032

Dial
 High       9.97436  -10.64818  -1.52178
 Current  -10.69919    2.25184   1.66331
 Low      -31.91391   90.18854   4.47822
"""

    j = -2.90002 
    t = -0.66300
    mv(nt2j, j)
    mv(nt2t, t)

def d9s6mv(x):
    p = x.split()
    hg, ho, vg, vo = tuple(float(x) for x in p)
    mv(ns6ho, ho)
    mv(ns6vo, vo)
    mv(ns6hg, hg)
    mv(ns6vg, vg)
