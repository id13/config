# sc5407_center.py alignment + ggg (external centering and ggg infra structure)
# sc5407_notb.py particle align (kmap align on data)
# ihma_perov_mcb.py (gap management)

# ###########################3
# new kar developments: karl_scananalysis_a.py

''' initial nt2
* initial nt2 = -33.5399 -2.1885   1.5556
* goto 14 keV

EH3 [108]: wnt2()

              nt2x      nt2j     nt2t
--------  --------  --------  -------
User    
 High     -21.0000   15.0000   3.2000
 Current  -29.5399   -2.1885   1.5556
 Low      -47.5399   -7.0000  -5.0000
Offset      0.0000   -0.6482   1.0003

Dial    
 High     -21.0000  -15.6482  -2.1997
 Current  -29.5399    1.5403  -0.5553
 Low      -47.5399    6.3518   6.0003

'''
'''
load_script('karl_scananalysis_a')
load_script("karl_mlltools_a")
emll=MLLEnv()
'''

def qgoto_pt(idx):
    global SCANS

    s = SCANS[-1]
    d = s.get_data()
    ax_keys = [k for k in d.keys() if k.startswith("axis")]


    todo = []

    print ("\npreview:")
    for k in ax_keys:
        a,nam = k.split(":")
        mot = getattr(SG, nam)
        pos = d[k][idx]
        todo.append((mot, pos))
        print ("    %s.move(%f)" % (mot.name, pos))

    print ("\nnow we will be moving things ...")
    for (m,p) in todo:
        print ("\n    %s.move(%f)" % (m.name, p))
        m.move(p)

class MLLEnv(object):

    POSITIONER_POOL = dict(
        mll_tilty  = smxa,
        mll_tiltz  = smya,
        ugap      = u18,
        osa_x     = nt2x,
        osa_sym   = nt2j,
        osa_asym  = nt2t,
    )
    HW_POOL = dict(
        mono = ccmo
    )

    def align_ugap(self):
        ugap = self.POSITIONER_POOL['ugap']
        plotinit(COUNTER_DICT['diode'])
        ascan(ugap, 6, 8, 80, 0.1)
        gop(ugap)

    def tune_gap(self, target='here', therange=0.6):
        ugap = self.POSITIONER_POOL['ugap']
        if 'here' == target:
            target = ugap.position
        npts=int(therange*4/0.1)
        plotinit(COUNTER_DICT['diode'])
        ascan(ugap, -0.5*therange+target, +0.5*therange+target, npts, 0.1)
        gop(ugap)

    def tune_tilt(self):
        pp = self.POSITIONER_POOL
        mll_tilty = pp['mll_tilty']
        plotinit(COUNTER_DICT['diode'])
        dscan(mll_tilty, -30, 30, 120, 0.1)
        gop(mll_tilty)
        mll_tiltz = pp['mll_tiltz']
        plotinit(COUNTER_DICT['diode'])
        dscan(mll_tiltz, -200, 200, 100, 0.1)
        gop(mll_tiltz)

    def find_osa(self):
        dmesh(nt2j,-0.1,0.1, 10, nt2t, -0.1, 0.1, 10, 0.1)
        ggg_alignmax('diode')

    def find_osa_big(self):
        dmesh(nt2j,-0.7,0.7, 20, nt2t, -0.7, 0.7, 20, 0.1)
        ggg_alignmax('diode')

    def tune_osa(self):
        pp = self.POSITIONER_POOL
        osa_sym = pp['osa_sym']
        plotinit(COUNTER_DICT['diode'])
        dscan(osa_sym, -0.04, 0.04, 40, 0.1)
        goc(osa_sym)
        osa_asym = pp['osa_asym']
        plotinit(COUNTER_DICT['diode'])
        dscan(osa_asym, -0.04, 0.04, 40, 0.1)
        goc(osa_asym)
        
    def tune_osa_coarse(self):
        pp = self.POSITIONER_POOL
        osa_sym = pp['osa_sym']
        plotinit(COUNTER_DICT['diode'])
        dscan(osa_sym, -0.08, 0.08, 20, 0.1)
        goc(osa_sym)
        osa_asym = pp['osa_asym']
        plotinit(COUNTER_DICT['diode'])
        dscan(osa_asym, -0.08, 0.08, 20, 0.1)
        goc(osa_asym)
        

    def align_osa(self):
        pp = self.POSITIONER_POOL

        osa_sym = pp['osa_sym']
        dscan(osa_sym, -0.04, 0.04, 80, 0.1)
        goc(osa_sym)
        osa_asym = pp['osa_asym']
        dscan(osa_asym, -0.04, 0.04, 80, 0.1)
        goc(osa_asym)

        mll_tilty = pp['mll_tilty']
        dscan(mll_tilty, -40, 40, 200, 0.1)
        goo(mll_tilty)
        mll_tiltz = pp['mll_tiltz']
        dscan(mll_tiltz, -200, 200, 200, 0.1)
        goo(mll_tiltz)
        
        osa_sym = pp['osa_sym']
        dscan(osa_sym, -0.04, 0.04, 80, 0.1)
        goc(osa_sym)
        osa_asym = pp['osa_asym']
        dscan(osa_asym, -0.04, 0.04, 80, 0.1)
        goc(osa_asym)
