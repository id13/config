# beam size should be less 100 nm, this is 30 mA 16 bunch mode experiments
print('ihls3461 load 6')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    zeronnp()
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,defocus=False):
    so()
    fshtrigger()
    mgeig_x()
    name = f'{pos[:-5]}_{start_key}'
    umv(nnz,0,nny,0)
    gopos(pos)
    if defocus:
        umvr(nnx,1)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

EC_list = [
'EC_1948_pos1_0019.json',
'EC_1948_pos2_0020.json',
'EC_1898_pos1_0013.json',
'EC_1898_pos2_0014.json',
'EC_2191_pos1_0016.json',
'EC_2191_pos2_0017.json',

]

EC_bkgd_list = [
'EC_2191_bkgd_0018.json',
'EC_1898_bkgd_0015.json',
'EC_1948_bkgd_0021.json',
]

HC_list = [
#'HC_1948_pos1_0030.json',
#'HC_1948_pos2_0031.json',
#'HC_1948_pos3_0032.json',
#'HC_1898_pos1_0026.json',
#'HC_1898_pos2_0027.json',
#'HC_1898_pos3_0028.json',
#'HC_2191_pos1_0022.json',
'HC_2191_pos2_0023.json',
'HC_2191_pos3_0024.json',
]

HC_bkgd_list = [
'HC_2191_bkgd_0025.json',
'HC_1898_bkgd_0029.json',
'HC_1948_bkgd_0033.json',
]

def Feb_6th_day_run1():
    try:
        umv(ndetx,-300)
        start_key = 'e'
        for _ in HC_list:
            gopos_kmap_scan(_,start_key,
                            75,75,0.5,0.5,0.04,defocus=True)
        for _ in HC_bkgd_list:
            gopos_kmap_scan(_,start_key,
                            5,5,0.5,0.5,0.04,defocus=True)
        
        sc()
    finally:
        sc()
        sc()

def Feb_5th_night_run1():
    try:
        umv(ndetx,-300)
        start_key = 'b'
        for _ in EC_list:
            gopos_kmap_scan(_,start_key,
                            75,75,0.5,0.5,0.04,defocus=True)
        for _ in EC_bkgd_list:
            gopos_kmap_scan(_,start_key,
                            5,5,0.5,0.5,0.04,defocus=True)
        
        sc()
    finally:
        sc()
        sc()
