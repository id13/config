print('ls2952_fib_mount_02 load 1')

STARTKEY = 'a'


def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_fib_mount_02():

    gop_newds('fib_mount_02_C3_3_0032.json')
    umvr(ustry,-0.3)
    umvr(ustrz,-0.5)
    dkpatchmesh(0.6, 300, 1., 100, 0.02, 1,1)
    enddataset()

    gop_newds('fib_mount_02_C4_0030.json')
    umvr(ustry,-0.4)
    umvr(ustrz,-0.4)
    dkpatchmesh(0.8, 400, 0.8, 80, 0.02, 1,1)
    enddataset()
    
    sc()
    sc()
