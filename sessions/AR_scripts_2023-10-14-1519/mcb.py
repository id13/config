import numpy as np
from matplotlib import pyplot as plt

FWHM_FAC = 2.*np.sqrt(2.*np.log(2.))

def s2fw(sig):
  return FWHM_FAC*sig

def fw2s(fwhm):
  return fwhm/FWHM_FAC

def gaussian(x, mu, sig):
  return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def defaut_lnsp(ll, ul, n):
  return np.linspace(ll, ul, n+1)

class Simulator(object):
  
  def __init__(self, **kw):
    pass

class KnifeEdge(Simulator):
  
  def __init__(self, ):
    pass

def _test1():
  x = Simulator()
  x = defaut_lnsp(0.01, 0.04, 30)
  y = gaussian(x, 0.2, 0.05)
  plt.plot(x, y)
  plt.plot(x, np.cumsum(y))
  plt.show()
    
if __name__ == '__main__':
  _test1()
  
    
