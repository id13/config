def measure_piezo_voltages():
    musst = getattr(current_session.setup_globals, "nmusst")
    for motor_name, musst_channel in {"nnp2": 4, "nnp3": 5}.items():
        motor = getattr(current_session.setup_globals, motor_name)
        channel = musst.get_channel(musst_channel)
        motor.move(125)
        motor.move(125)
        motor.move(125)
        for position in [0, 250]:
            motor.move(position)
            sleep(1)
            motor.move(position)
            sleep(1)
            motor.move(position)
            print(motor_name, motor.position, "voltage: ", channel.value)
