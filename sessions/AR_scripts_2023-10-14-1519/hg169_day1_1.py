print('hg169_day1_1 load 4')
import time

print("=== hg169_day1_1 ===")

DSKEY='d' # to be incremented if there is a crash

class EXPO:

    expt = 0.02

class FLUX(object):

    tag = 'noneFX'

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 2.0

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



def hi_flux():
    FLUX.tag = 'hi6p31FX'
    umv(u18, 6.31)

def lo_flux():
    FLUX.tag = 'lo6p13FX'
    umv(u18, 6.13)

def doloop(s, npts, expt):
    print("\n\n\n======================== doing loop:", s, ' n=', npts, '  expt =', expt)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)

    print("\n[loopscan]:")
    fshtrigger()
    loopscan(npts, expt)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)


def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\ntnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    dsname = f'{dsname}_{FLUX.tag}'
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def raw_hg169_day1_1():

  
    
    
    # choose your exposuretime
    EXPO.expt = 0.010

    # prefix_ul01_<patch-hor>x<patchr-vert>_<intervals-hor>x<intervals-vert>_<range-hor>x<range-vert>_<exptime-suggestion>_<eport-number>.json
    # example: dooul('papyrus13_Pap13_ul01_5x1_520x450_1040x900_60_0049.json')

    # historical samples
    lo_flux()
    dooul('leti1_S1_ul04_1x1_115x170_230x340_10_0013.json')
    dooul('leti1_S1_ul40_1x1_450x235_900x470_10_0048.json')
    dooul('leti1_S3_ul41_1x1_150x150_300x300_10_0049.json')
    dooul('leti1_S5_ul42_1x1_420x225_840x550_10_0050.json')
    dooul('leti1_S6_ul43_1x1_210x100_420x200_10_0051.json')
    dooul('leti1_S10_ul02_1x1_195x105_390x210_10_0010.json')
    dooul('leti1_S11_ul44_1x1_190x170_380x340_10_0052.json')
    
    
    # damage prussian blue and cadmium yellow
    
    lo_flux()
    EXPO.expt = 0.010
    dooul('leti1_CdS_mapt1_ul14_1x1_25x50_50x100_10_0034.json')
    dooul('leti1_PB_mapt1_ul08_1x1_25x50_50x100_10_0028.json')
    
    lo_flux()
    EXPO.expt = 0.10
    dooul('leti1_CdS_mapt2_ul15_1x1_25x50_50x100_100_0035.json')
    dooul('leti1_PB_mapt2_ul08_1x1_25x50_50x100_100_0029.json')
    
    lo_flux()
    EXPO.expt = 1
    dooul('leti1_CdS_mapt3_ul16_1x1_25x50_50x100_1000_0036.json')
    dooul('leti1_PB_mapt3_ul08_1x1_25x50_50x100_1000_0030.json')
    
    hi_flux()
    EXPO.expt = 0.010 
    dooul('leti1_CdS_mapt4_ul17_1x1_25x50_50x100_10_0037.json')
    dooul('leti1_PB_mapt4_ul08_1x1_25x50_50x100_10_0031.json')
    
    hi_flux()
    EXPO.expt = 0.10 
    dooul('leti1_CdS_mapt5_ul18_1x1_25x50_50x100_100_0038.json')
    dooul('leti1_PB_mapt5_ul08_1x1_25x50_50x100_100_0032.json')
    
    hi_flux()
    EXPO.expt = 1
    dooul('leti1_CdS_mapt6_ul19_1x1_25x50_50x100_1000_0039.json')
    dooul('leti1_PB_mapt6_ul08_1x1_25x50_50x100_1000_0033.json')
    
         
         
    lo_flux()
    doloop('leti1_CdS_point1_0040.json',10000, 0.01)
    doloop('leti1_PB_point1_0022.json',10000, 0.01)
    
    
    hi_flux() 
    doloop('leti1_CdS_point2_0043.json',10000, 0.01)
    doloop('leti1_PB_point2_0023.json',10000, 0.01)
    
    
    
    

def hg169_day1_1():
    try:
        so()
        time.sleep(1)
        raw_hg169_day1_1()
        sc()
    finally:
        sc()
        sc()
        sc()








    
    
    
    
    
    
    
