
def perov_night2():
    start_key = 'c'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_night2_{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.5, 100, -0.2, 0.2, 40, 0.02, -2.5, 2.5, 10)
    finally:
        sc()
        sc()
        sc()

def perov_night2_visual():
    start_key = 'f'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_night2_{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.5, 5, -0.2, 0.2, 4, 0.02, -2.5, 2.5, 2)
    finally:
        sc()
        sc()
        sc()


def perov_day_scan1_previsual():
    start_key = 'j'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_day_scan1_previsual_{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.03, 1, -0.1, 0.1, 2, 0.02, -0.5, 0.5, 2)
    finally:
        sc()
        sc()
        sc()


def perov_day_scan1_xray():
    start_key = 'k'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_day_scan1_xray_{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.03, 6, -0.1, 0.1, 20, 0.02, -0.5, 0.5, 10)
    finally:
        sc()
        sc()
        sc()


def perov_day_scan1_postvisual():
    start_key = 'l'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_day_scan1_postvisual_{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.03, 1, -0.1, 0.1, 2, 0.02, -0.5, 0.5, 2)
    finally:
        sc()
        sc()
        sc()


def perov_day_burnscan():
    start_key = 'm'
    try:
        for i in range(1):
            newdataset(f'{start_key}_perov_day_burnscan{i:04d}')
            vrg.dmesh_z_rot_vy(0, 0.03, 1, -0.1, 0.1, 2, 30, 0, 0, 0)
    finally:
        sc()
        sc()
        sc()
        
