def mgallmpx(rois=True):
    MG_EH2.set_active()
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:acq_time_2')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct22')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct24')
    MG_EH2.enable('mpxeh2he:im*')
    MG_EH2.enable('mpxeh2cdte:im*')
    mpxeh2he.camera.energy_threshold=0.5*23.5
    mpxeh2cdte.camera.energy_threshold=0.5*23.5
    mpxeh2he.proxy.saving_index_format="%06d"
    mpxeh2he.proxy.saving_format="HDF5BS"
    mpxeh2cdte.proxy.saving_index_format="%06d"
    mpxeh2cdte.proxy.saving_format="HDF5BS"

    if rois:
        MG_EH2.enable('mpxeh2*:roi_counters:roi1_*')


def raddam(expt):
	expt_text = '_'.join(str(expt).split('.'))
	POS = f'rad_dam_{expt_text}'
	stp(POS)

	mvr (ustry ,-0.1)
	for i in range(1,21):
		loopscan(i, expt, sleep_time=3)
		mvr(ustry, 0.01)
	rstp(POS)
	dkmapyz_2(-0.2,0.2,200, -0.025,0.025,25, expt, retveloc=5)


def do_beam_damage():
	try:
		raddam(0.01)
		umvr(ustrz, 0.1)
		raddam(0.02)
		umvr(ustrz, -0.2)
		raddam(0.05)
	finally:
		sc()
		sc()
		sc()
