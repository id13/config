
from bliss.setup_globals import *

load_script("nanocal_session.py")

print("")
print("Welcome to your new 'nanocal_session' BLISS session !! ")
print("")
print("You can now customize your 'nanocal_session' session by changing files:")
print("   * /nanocal_session_setup.py ")
print("   * /nanocal_session.yml ")
print("   * /scripts/nanocal_session.py ")
print("")

