import os 
print ('rl2...')

def the_sc5011_scan():
    print ("the_sc5011_scan")
    dmesh(nnp3,-20,20,320,nnp2,0,-14.5,29,0.2)
    pass

DS_KEY = 'c'

def sc5011_night1():
    newdataset('S1_night1_%s' % DS_KEY)
    gopos('x2_l100mu_nny')
    y_start = nny.position
    for i in range(20):
        print('='*40)
        print('cycle %d' % i)
        y_pos = y_start + 0.015*i
        print('nny_pos %10.5f' % y_pos)
        mv(nny,y_pos)
        wm(nny)
        the_sc5011_scan()
    enddataset()
