import tango
import socket
import requests

def make_nanocontrol():
    return NanoControl()

class NanoControl():
    def __init__(self):
        self.scan_saving_path = '/data/id13/inhouse12/alexey/nanocal_bliss_test/'
        print('NanoControl data will be saved in: {}'.format(self.scan_saving_path))
        self._http_server_url = 'http://id13tmp0.esrf.fr:8000/data/raw_data/raw_data.h5'
        self._set_connection()
        self.apply_default_calibration()

    def _set_connection(self):
        self._device = tango.DeviceProxy('id13/nanocontrol/1')
        self._device.set_timeout_millis(10000000)
        self._device.set_connection()
    
    def apply_default_calibration(self):
        self._device.apply_default_calibration()

    def apply_calibration(self):
        self._device.apply_calibration()

    def get_calibration_info(self):
        return(self._device.get_calibration[1][0]['value'])

    def arm_fast_heat(self, time_profile: list, temp_profile:list):
        self._device.set_fh_time_profile(time_profile)
        self._device.set_fh_temp_profile(temp_profile)
        self._device.arm_fast_heat()
        print('Calibration to be used: {}'.format(self.get_calibration_info()))
        print("Heating program armed: ")
        for i, time_point in enumerate(time_profile):
            print(time_point, '\t', temp_profile[i])

    def run_fast_heat(self):
        self._device.run_fast_heat()
        print('Fast heating finised!')

    def download_data(self, file_name: str):
        URL = self._http_server_url
        response = requests.get(URL, verify=False)
        full_file_path = self.scan_saving_path + file_name + '.h5'
        with open(full_file_path, 'wb') as f:
            f.write(response.content)



from silx.gui import qt
from silx.gui.plot import Plot1D, PlotWindow
from silx.gui import icons
import sys
sys.path.append("/data/id13/inhouse12/alexey/nanocal_front")
from mainWindow import *
from mainWindowUi import *
import threading

def make_NanoControlGUI():
    return NanoControlGUI()

class NanoControlGUI():
    def _main(self):
        app = qt.QApplication(sys.argv)
        window = mainWindowUi()
        window.show()
        app.exec()
    def show(self):
        t = threading.Thread(target=self._main)
        t.daemon = True
        t.start()
