print ("sc5005_night3 loading ... 1")

from bliss.setup_globals import *

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    gopos(x)
    newdataset('DS_a_%s' % s)
    fshtrigger()

def WAXS_long_bombyx(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)

    tgopos(posstr)

    dist1 = 0.0
    dist2 = 15.0

    mvr(nnp2,-dist1)
    fshtrigger()
    kmap.dkmap(nnp3, -80, 80, 800, nnp2, 0, -4, 8, 0.05)
    kmap.dkmap(nnp3, -80, 80, 800, nnp2, -4, -8, 8, 0.05)

    mvr(nnp2,-dist2)
    fshtrigger()
    dmesh(nnp3, -80, 80, 1600 , nnp2, 0, -8, 16, 0.2)
    
def SAXS_long_bombyx(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)
    
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-80, 80, 800 , nnp2, 0, -4, 8, 0.05)
    kmap.dkmap(nnp3,-80, 80, 800 , nnp2, -4, -8, 8, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-80, 80, 1600 , nnp2, 0, -2, 4, 0.05)
    kmap.dkmap(nnp3,-80, 80, 1600 , nnp2, -2, -4, 4, 0.05)
    
def WAXS_bombyx(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)

    tgopos(posstr)

    dist1 = 0.0
    dist2 = 15.0

    mvr(nnp2,-dist1)
    fshtrigger()
    kmap.dkmap(nnp3, -38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2,-dist2)
    fshtrigger()
    dmesh(nnp3, -38, 38, 760 , nnp2, 0, -8, 16, 0.2)
    
def SAXS_bombyx(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)

    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 760 , nnp2, 0, -4, 8, 0.05)
    
def WAXS_nephila(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)

    tgopos(posstr)

    dist1 = 0.0
    dist2 = 17.0

    mvr(nnp2, -dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, -dist2)
    fshtrigger()
    dmesh(nnp3,-30, 30, 600 , nnp2, 0, -10, 20, 0.2)
    
def SAXS_nephila(posstr, deltaz=0.0):
    zeronnp()
    mvr(nnp3, deltaz)

    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0
    
    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 600 , nnp2, 0, -5, 10, 0.05)


POS = """
mount05_x50_BM2D_pos1new_0040.json
mount05_x50_BM2D_pos2_0041.json
mount05_x50_BM2N_pos1_0046.json
mount05_x50_BM2N_pos2_0045.json
mount05_x50_NEC_pos1_0031.json
mount05_x50_NEC_pos2_0034.json
mount05_x50_NIDL_pos1_0036.json
mount05_x50_NIDL_pos2_0037.json
"""

POS_X = """
B  mount05_x50_BM2D_pos1new_0040.json
B  mount05_x50_BM2D_pos2_0041.json
L  mount05_x50_BM2N_pos1_0046.json
L  mount05_x50_BM2N_pos2_0045.json
N  mount05_x50_NEC_pos1_0031.json
N  mount05_x50_NEC_pos2_0034.json
N  mount05_x50_NIDL_pos1_0036.json
N  mount05_x50_NIDL_pos2_0037.json
"""

def sc5005_main_night3():



    so()
    try:

        WAXS_nephila('mount05_x50_NEC_pos1_0031.json', deltaz=-6.0)
        WAXS_nephila('mount05_x50_NEC_pos2_0034.json', deltaz=-11.0)
        WAXS_nephila('mount05_x50_NIDL_pos1_0036.json')
        WAXS_nephila('mount05_x50_NIDL_pos2_0037.json')
        WAXS_bombyx('mount05_x50_BM2D_pos1new_0040.json')
        WAXS_bombyx('mount05_x50_BM2D_pos2_0041.json')
        WAXS_long_bombyx('mount05_x50_BM2N_pos1_0046.json')
        WAXS_long_bombyx('mount05_x50_BM2N_pos2_0045.json')

        umv(ndetx, 300)

        SAXS_nephila('mount05_x50_NEC_pos1_0031.json', deltaz=-6.0)
        SAXS_nephila('mount05_x50_NEC_pos2_0034.json', deltaz=-11.0)
        SAXS_nephila('mount05_x50_NIDL_pos1_0036.json')
        SAXS_nephila('mount05_x50_NIDL_pos2_0037.json')
        SAXS_bombyx('mount05_x50_BM2D_pos1new_0040.json')
        SAXS_bombyx('mount05_x50_BM2D_pos2_0041.json')
        SAXS_long_bombyx('mount05_x50_BM2N_pos1_0046.json')
        SAXS_long_bombyx('mount05_x50_BM2N_pos2_0045.json')

        sc()
    finally:
        sc()
        sc()
        sc()
