from math import fabs
def nt2u(v):
    x = v*0.707106
    mvr (nt2j, x, nt2t, x)

def nt2d(v):
    x = v*0.707106
    mvr (nt2j, -x, nt2t, -x)

def nt2p(v):
    x = v*0.707106
    mvr (nt2j, x, nt2t, -x)

def nt2m(v):
    x = v*0.707106
    mvr (nt2j, -x, nt2t, x)

def pcoct(t):
    sct(t, mpxeh3, pco)

def wnt2(): wm(nt2x,nt2j,nt2t)

def gox2(x, n2):
    mv(nnp2, 250-x, nnp3, n2)

def search_nt4align():
    wm(nt4t, nt4j)
    dscan(nt4j, -0.1,0.1,50,0.1)
    goc(nt4j)
    where()
    dscan(nt4t, -0.1,0.1,50,0.1)
    goc(nt4t)
    where()
    wm(nt4t, nt4j)

def nt3align():
    wm(nt1t, nt3j)
    dscan(nt3j, -0.04,0.04,40,0.1)
    goc(nt3j)
    where()
    dscan(nt1t, -0.04,0.04,40,0.1)
    goc(nt1t)
    where()
    wm(nt1t, nt3j)

def nt2align():
    wm(nt2t, nt2j)
    dscan(nt2j, -0.04,0.04,40,0.1)
    goc(nt2j)
    where()
    dscan(nt2t, -0.04,0.04,40,0.1)
    goc(nt2t)
    where()
    wm(nt2t, nt2j)

def nt4align():
    wm(nt4t, nt4j)
    dscan(nt4j, -0.02,0.02,50,0.1)
    goc(nt4j)
    where()
    dscan(nt4t, -0.02,0.02,50,0.1)
    goc(nt4t)
    where()
    wm(nt4t, nt4j)

def wnt():
    wm(nt2j,nt2t)
    wm(nt3j,nt1t)
    wm(nt4j,nt4t)

def prntrecov():
    t4 = nt4t.position
    j4 = nt4j.position
    t1 = nt1t.position
    j3 = nt3j.position
    t2 = nt2t.position
    j2 = nt2j.position
    print(f'mv(nt2j, {j2}, nt2t, {t2}, nt3j, {j3}, nt1t, {t1}, nt4j, {j4}, nt4t, {t4})')

def largebsalign():
    yp = nbsy.position
    zp = nbsz.position
    wm(nbsz, nbsy)
    dscan(nbsy, -0.6,0.6,50,0.1)
    goc(nbsy)
    where()
    dscan(nbsz, -0.6,0.6,50,0.1)
    goc(nbsz)
    where()
    wm(nbsz, nbsy)
    new_yp = nbsy.position
    new_zp = nbsz.position
    if fabs(yp-new_yp) < 0.11:
        nbsy.position = 0
    else:
        print('error: nbsy alignment discrepancy!')
    if fabs(zp-new_zp) < 0.11:
        nbsz.position = 0
    else:
        print('error: nbsz alignment discrepancy!')

# mv(nt2j, -2.8192050071373664, nt2t, 1.7814164150425473, nt3j, 5.766059075436477, nt1t, 2.0860005489980784, nt4j, -233.63444639718804, nt4t, -0.831677189129838)

