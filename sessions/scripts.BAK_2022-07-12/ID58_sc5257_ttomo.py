print('version ttomo_b')
import time
import traceback

class Bailout(Exception): pass

ORI_INSTRUCT = """
# kappa,omega
16,124.2
16,129.6
16,135
16,140.4
16,145.8
16,151.2
16,156.6
16,162
16,167.4
16,172.8
16,178.2
16,183.6
16,189
16,194.4
16,199.8
16,205.2
16,210.6
16,216
16,221.4
16,226.8
16,232.2
16,237.6
16,243
16,248.4
16,253.8
16,259.2
16,264.6
16,270
16,275.4
16,280.8
16,286.2
16,291.6
16,297
16,302.4
16,307.8
16,313.2
16,318.6
16,324
16,329.4
16,334.8
16,340.2
16,345.6
16,351
16,356.4
0,0
24,0
24,5.6
24,11.2
24,16.8
24,22.4
24,28
24,33.6
24,39.2
24,44.8
24,50.4
24,56
24,61.6
24,67.2
24,72.8
24,78.4
24,84
24,89.6
24,95.2
24,100.8
24,106.4
24,112
24,117.6
24,123.2
24,128.8
24,134.4
24,140
24,145.6
24,151.2
24,156.8
24,162.4
24,168
24,173.6
24,179.2
24,184.8
24,190.4
24,196
24,201.6
24,207.2
24,212.8
24,218.4
24,224
24,229.6
24,235.2
24,240.8
24,246.4
24,252
24,257.6
24,263.2
24,268.8
24,274.4
24,280
24,285.6
24,291.2
24,296.8
24,302.4
24,308
24,313.6
24,319.2
24,324.8
24,330.4
24,336
24,341.6
24,347.2
0,0
"""

VERY_ORI_INSTRUCT = """
# kappa,omega
20,0
0,0
0,2.2
0,4.4
0,6.6
0,8.8
0,11
0,13.2
0,15.4
0,17.6
0,19.8
0,22
0,24.2
0,26.4
0,28.6
0,30.8
0,33
0,35.2
0,37.4
0,39.6
0,41.8
0,44
0,46.2
0,48.4
0,50.6
0,52.8
0,55
0,57.2
0,59.4
0,61.6
0,63.8
0,66
0,68.2
0,70.4
0,72.6
0,74.8
0,77
0,79.2
0,81.4
0,83.6
0,85.8
0,88
0,90.2
0,92.4
0,94.6
0,96.8
0,99
0,101.2
0,103.4
0,105.6
0,107.8
0,110
0,112.2
0,114.4
0,116.6
0,118.8
0,121
0,123.2
0,125.4
0,127.6
0,129.8
0,132
0,134.2
0,136.4
0,138.6
0,140.8
0,143
0,145.2
0,147.4
0,149.6
0,151.8
0,154
0,156.2
0,158.4
0,160.6
0,162.8
0,165
0,167.2
0,169.4
0,171.6
0,173.8
0,176
0,178.2
0,180.4
0,0
40,0
40,6.7
40,13.4
40,20.1
40,26.8
40,33.5
40,40.2
40,46.9
40,53.6
40,60.3
40,67
40,73.7
40,80.4
40,87.1
40,93.8
40,100.5
40,107.2
40,113.9
40,120.6
40,127.3
40,134
40,140.7
40,147.4
40,154.1
40,160.8
40,167.5
40,174.2
40,180.9
40,187.6
40,194.3
40,201
40,207.7
40,214.4
40,221.1
40,227.8
40,234.5
40,241.2
40,247.9
40,254.6
40,261.3
40,268
40,274.7
40,281.4
40,288.1
40,294.8
40,301.5
40,308.2
40,314.9
40,321.6
40,328.3
40,335
40,341.7
40,348.4
0,0
8,0
8,5.2
8,10.4
8,15.6
8,20.8
8,26
8,31.2
8,36.4
8,41.6
8,46.8
8,52
8,57.2
8,62.4
8,67.6
8,72.8
8,78
8,83.2
8,88.4
8,93.6
8,98.8
8,104
8,109.2
8,114.4
8,119.6
8,124.8
8,130
8,135.2
8,140.4
8,145.6
8,150.8
8,156
8,161.2
8,166.4
8,171.6
8,176.8
8,182
8,187.2
8,192.4
8,197.6
8,202.8
8,208
8,213.2
8,218.4
8,223.6
8,228.8
8,234
8,239.2
8,244.4
8,249.6
8,254.8
8,260
8,265.2
8,270.4
8,275.6
8,280.8
8,286
8,291.2
8,296.4
8,301.6
8,306.8
8,312
8,317.2
8,322.4
8,327.6
8,332.8
8,338
8,343.2
8,348.4
8,353.6
0,0
32,0
32,6.1
32,12.2
32,18.3
32,24.4
32,30.5
32,36.6
32,42.7
32,48.8
32,54.9
32,61
32,67.1
32,73.2
32,79.3
32,85.4
32,91.5
32,97.6
32,103.7
32,109.8
32,115.9
32,122
32,128.1
32,134.2
32,140.3
32,146.4
32,152.5
32,158.6
32,164.7
32,170.8
32,176.9
32,183
32,189.1
32,195.2
32,201.3
32,207.4
32,213.5
32,219.6
32,225.7
32,231.8
32,237.9
32,244
32,250.1
32,256.2
32,262.3
32,268.4
32,274.5
32,280.6
32,286.7
32,292.8
32,298.9
32,305
32,311.1
32,317.2
32,323.3
32,329.4
32,335.5
32,341.6
32,347.7
32,353.8
0,0
16,0
16,5.4
16,10.8
16,16.2
16,21.6
16,27
16,32.4
16,37.8
16,43.2
16,48.6
16,54
16,59.4
16,64.8
16,70.2
16,75.6
16,81
16,86.4
16,91.8
16,97.2
16,102.6
16,108
16,113.4
16,118.8
16,124.2
16,129.6
16,135
16,140.4
16,145.8
16,151.2
16,156.6
16,162
16,167.4
16,172.8
16,178.2
16,183.6
16,189
16,194.4
16,199.8
16,205.2
16,210.6
16,216
16,221.4
16,226.8
16,232.2
16,237.6
16,243
16,248.4
16,253.8
16,259.2
16,264.6
16,270
16,275.4
16,280.8
16,286.2
16,291.6
16,297
16,302.4
16,307.8
16,313.2
16,318.6
16,324
16,329.4
16,334.8
16,340.2
16,345.6
16,351
16,356.4
0,0
24,0
24,5.6
24,11.2
24,16.8
24,22.4
24,28
24,33.6
24,39.2
24,44.8
24,50.4
24,56
24,61.6
24,67.2
24,72.8
24,78.4
24,84
24,89.6
24,95.2
24,100.8
24,106.4
24,112
24,117.6
24,123.2
24,128.8
24,134.4
24,140
24,145.6
24,151.2
24,156.8
24,162.4
24,168
24,173.6
24,179.2
24,184.8
24,190.4
24,196
24,201.6
24,207.2
24,212.8
24,218.4
24,224
24,229.6
24,235.2
24,240.8
24,246.4
24,252
24,257.6
24,263.2
24,268.8
24,274.4
24,280
24,285.6
24,291.2
24,296.8
24,302.4
24,308
24,313.6
24,319.2
24,324.8
24,330.4
24,336
24,341.6
24,347.2
0,0
"""

def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (kap,ome) = l.split(',')
            ko = (int(kap), float(ome))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log'):
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(kap,ome)) = instll_item
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=4)
        self.perform_scan()


    def perform_scan(self): 
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        lly *= 0.001
        uly *= 0.001
        llz *= 0.001
        ulz *= 0.001
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
        time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 3*(lly//3)
        uly = 3*(uly//3)
        llz = 3*(llz//3)
        ulz = 3*(ulz//3)

        dy = uly - lly
        dz = ulz - llz

        nitvy = dy//3
        nitvz = dz//3

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def sc5257_main(zkap):
    print('Hi!')

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    # 117
    # 170
    scanparams = dict(
        lly = -175,
        uly = 176,
        nitvy = 117,
        llz = -269,
        ulz = 241,
        nitvz = 170,
        expt = 0.005,
        retveloc = 5.0
    )
    ttm = TTomo( 'asy.com', zkap, instll, scanparams)



    # loop over projections
    ttm.mainloop()
