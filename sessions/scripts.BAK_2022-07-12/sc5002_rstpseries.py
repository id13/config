def dmy_dkmap(*p):
    print ("dmy_dkmap args:")
    print ("    lly        :", p[0])
    print ("    uly        :", p[1])
    print ("    nitvy      :", p[2])
    print ("    llz        :", p[3])
    print ("    ulz        :", p[4])
    print ("    nitvz      :", p[5])
    print ("    exptime    :", p[6])
    print ("")

#dkmapyz_2(ll1,ul1,nitv1,ll2,ul2,nitv2,exptime, frames_per_file=2000)

def pos_kfunc(s):
    w = s.split('_')
    return int(w[0][1:])


def get_relevant_pos():
    newll = []
    ll = lspos()
    for x in ll:
        w = x[:-5]
        if '_ul_' in w:
            newll.append(w)
    return newll

EXPTIME = 0.05
def sc5002_scan_series():
    lll = get_relevant_pos()
    llll = sorted(lll, key = pos_kfunc)
    repe = llll[0]
    ll = llll[:]
    ll.append(repe)

    
    for i,s in enumerate(ll):
        print("===================================================")
        print('itemindex:', i)
        (p,k,scinf) = s.split('_')
        posidx = int(p[1:])
        print('positionindex:', posidx)
        print('scaninf:', scinf)
        (h,v) = scinf.split('x')
        hn = int(h)
        hr = 0.001 *float(h)
        vn = int(v)
        vr = 0.001 *float(v)
        gopos(s)
        sleep(3)
        dmy_dkmap(0,hr,hn,0,vr,vn,EXPTIME)
        dkmapyz_2(0,hr,hn,0,vr,vn,EXPTIME)
        sleep(0.01)
