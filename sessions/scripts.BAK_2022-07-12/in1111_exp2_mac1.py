print ("mac1,2,3,4")

def the_scan1():
    dscan(ustrz,-0.05,0.05,50,0.05)

def the_scan2():
    dscan(ustrz,-0.1,0.1,100,0.1)

def the_scan3():
    dscan(ustrz,-0.1,0.1,100,0.05)

def the_scan4():
    dmesh(ustry,-0.2,0.2,20,ustrz,-0.1,0.1,10,0.05)

def in1111_exp2_mac1():
    newdataset('frame1')
    for i in range(1,10):
        pos_name = "frame1_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan1()

    newdataset('frame2')
    for i in range(1,30):
        pos_name = "frame2_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan2()

    newdataset('frame3')
    for i in range(1,10):
        pos_name = "frame3_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan2()

def in1111_exp2_mac2():
    newdataset('frame4')
    for i in range(1,10):
        pos_name = "frame4_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan3()

    newdataset('frame5')
    for i in range(1,11):
        pos_name = "frame5_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan3()

def in1111_exp2_mac3():
    newdataset('frame6')
    for i in range(1,8):
        pos_name = "frame6_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)        
        the_scan4()
        
def in1111_exp2_mac4():
    newdataset('frame6')
    for i in range(8,14):
        pos_name = "frame6_pos%1d" % i
        print("="*30,pos_name)
        gopos(pos_name)
        umvr(ustry,-0.01)        
        the_scan4()


print ("mac1,2,3,4 end")
