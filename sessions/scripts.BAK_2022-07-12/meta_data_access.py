import os
from os import path
import numpy as np
import h5py



POSSET_KEY_SETS = dict(
    UHEX = "uhextx      uhexty      uhextz      uhexrx      uhexry      uhexrz",
    USTR = "ustrx       ustry        ustrz",
    UDET = "udetx       udety        udetz",
    USAP = "usapx       usapy        usapz"
)
    

def pos_print(x):
    t = tuple(x)
    tpl1 = "%10.5f,"
    tpl2 = "%10.5f"
    tpl = (tpl1*(len(t)-1)) + tpl2
    return tpl % t

class PosSet(object):

    def __init__(possetI_ks=None, ps_keyt=None, ps_post=None):
        pass


class DataH5(object):

    def __init__(self, fname, adpth="."):
        self.afpth = path.normpath(path.join(adpth, fname))

    def open_rb(self):
        f = open(self.afpth, "rb")
        self.fh5 = h5py.File(f, 'r')

    def close(self):
        self.fh5.close()

    def get_scankeys(self):
        return list(self.fh5.keys())

    def find_scankey(self, scanno, scankeys=None):
        if None is scankeys:
            scankeys = self.get_scankeys()
        for sk in scankeys:
            i,tp = sk.split('_')
            if int(i) == scanno:
                return sk
        return None

    def get_scanpositions(self, scankey):
        fh5 = self.fh5
        pos_collec = fh5[scankey]['scan_meta']['positioners']['positioners_start']
        #print ("Hi",(pos_collec.keys()))
        #for k in pos_collec.keys():
        #    print(pos_collec[k], float(np.array(pos_collec[k], dtype=np.float64)))
        dc = dict()
        for k in pos_collec.keys():
            #print(k,pos_collec[k])
            try:
                dc[k] = float(np.array(pos_collec[k]))
            except ValueError:
                print("ValueError on reading motor pos of axis: %s", (k,))
                try:
                    s = str(np.array(pos_collec[k]))
                    print("Value found: %s" % (s,))
                    
                except:
                    print("not the string (ERR) problem ...")
        return dc

    def get_possetpos(self, sgenk, pk, dc=None):
        #if isinstance(sgenk, basestring):
        if hasattr(sgenk, "upper"):
            sk = sgenk
        else:
            sk = self.find_scankey(sgenk)
        if None is dc:
            dc = self.get_scanpositions(sk)
    
        ps = POSSET_KEY_SETS[pk]
        w = ps.split()
        t = tuple([float(np.array(dc[k])) for k in w])
        return (ps, w, t)

        
def _test2():
    scanno = 274
    d = '/data/id13/inhouse12/THEDATA_I12_1/d_2020-03-1_commi_EBS_RESTART/DATA/TEST/ebsmar1'
    fn = 'd3.h5'
    dh5 = DataH5(fn, d)
    dh5.open_rb()

    (ps, w, t) = dh5.get_possetpos(274, "UHEX")
    print (ps)
    print(pos_print(t))
    print('')
    
    (ps, w, t) = dh5.get_possetpos(274, "UDET")
    print (ps)
    print(pos_print(t))
    print('')

    (ps, w, t) = dh5.get_possetpos(274, "USTR")
    print (ps)
    print(pos_print(t))
    print('')

            
def _test1():
    scanno = 274
    d = '/data/id13/inhouse12/THEDATA_I12_1/d_2020-03-1_commi_EBS_RESTART/DATA/TEST/ebsmar1'
    fn = 'd3.h5'
    dh5 = DataH5(fn, d)
    dh5.open_rb()
    sk = dh5.find_scankey(scanno)
    print(sk)
    dc = dh5.get_scanpositions(sk)
    hexa =  ("uhextx","uhexty","uhextz","uhexrx","uhexry","uhexrz")
    print ("(%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,)" % tuple([float(np.array(dc[k])) for k in hexa]))
    dh5.close()

        
if __name__ == '__main__':
    _test2()
