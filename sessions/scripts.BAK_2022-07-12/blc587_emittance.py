def emit_test(key, prim=True):
    newdataset('emit_test_%s' % key)

    gopos('out')
    if prim:
        plotinit(ct32)
        dscan(pho, -0.5,0.5,50,0.05)
        goc(pho)
        plotinit(ct32)
        dscan(pvo, -0.5,0.5,50,0.05)
        goc(pvo)
        apalign()

    gopos('he')
    plotinit(ct32)
    detect_edge(nnp2,50)
    plotinit(ct32)
    detect_edge(nnp2,20)
    plotinit(ct32)
    dscan(nnp2,5,-5,200,0.1)
    plotinit(ct32)
    dscan(nnp2,5,-5,400,0.1)

    gopos('ve')
    plotinit(ct32)
    detect_edge(nnp3,50)
    plotinit(ct32)
    detect_edge(nnp3,20)
    plotinit(ct32)
    dscan(nnp3,-5,5,200,0.1)
    plotinit(ct32)
    dscan(nnp3,-5,5,400,0.1)

    gopos('out')
    loopscan(20,1)
    enddataset()

def doowbet():
    
    plotinit(ct32)
    dscan(wbetz,-0.1,0.1,50,0.05)
    goc(wbetz)
    plotinit(ct32)
    dscan(wbetz,-0.03,0.03,120,0.05)
    goc(wbetz)
    plotinit(ct32)
    dscan(wbety,-0.05,0.05,50,0.05)
    goc(wbety)
    plotinit(ct32)
    dscan(wbetz,-0.03,0.03,120,0.05)
    goc(wbetz)

def goto_wbetz3():
    set_outgain(9.9e-5)
    set_ingain(1e-6)

def goto_wbetz0():
    set_outgain(1e-6)
    set_ingain(1e-8)

def mvwbetzin():
    fe.close()
    umv(wbetz, -40.02760)

def mvwbetzout():
    fe.close()
    umv(wbetz, 6.0)

def dooall_w3():
    fe.close()
    goto_wbetz3()
    mvwbetzin()
    fe.open()
    doowbet()

def dooall_w0():
    fe.close()
    goto_wbetz0()
    mvwbetzout()
    fe.open()

def w3_emit_test(key):
    newdataset('emit_test_%s' % key)

    gopos('out')
    apalign()

    gopos('he')
    plotinit(ct32)
    detect_edge(nnp2,50)
    plotinit(ct32)
    detect_edge(nnp2,20)
    plotinit(ct32)
    dscan(nnp2,5,-5,200,0.1)
    plotinit(ct32)
    dscan(nnp2,5,-5,400,0.1)

    gopos('ve')
    plotinit(ct32)
    detect_edge(nnp3,50)
    plotinit(ct32)
    detect_edge(nnp3,20)
    plotinit(ct32)
    dscan(nnp3,-5,5,200,0.1)
    plotinit(ct32)
    dscan(nnp3,-5,5,400,0.1)
    gopos('out')
    loopscan(20,1)

    enddataset()
