print('eh3detarm load 8')
from pprint import pprint
from collections import namedtuple
import numpy as np
pi = float(np.pi)
RPD = float(pi/180.0)
DPR = float(180.0/pi)

Armco = namedtuple('Armco', ['armx','armrm','armrd'])
ArmcoSet = namedtuple('ArmcoSet', ['low','high'])


class EH3Arm(object):

    DEFAULTS = dict(
        t1     = 1580.0,
        E      = 0.0,
        A      = 113.0,
        B      = 1272.0,
        t0_offset = 0.0,
        t1_MAX  = 1580.0
    )

    def __init__(self, defaults=None):
        if None == defaults:
            defaults = self.DEFAULTS
        self.defaults = defaults

    def make_vector(self, x0, x1):
        return np.array((x0,x1), dtype=np.float64)

    def diffco_to_armco_demo(self, tth, D):
        dfc = self.defaults
        t0_offset, t1, E, A, B = dfc['t0_offset'], dfc['t1'], dfc['E'], dfc['A'], dfc['B']
        tth_rad = RPD*tth
        vD = self.make_vector(
            D*np.cos(tth_rad),
            D*np.sin(tth_rad),
        )
        vA = self.make_vector(
            A*np.cos(tth_rad),
            A*np.sin(tth_rad),
        )
        vE = self.make_vector(
            E*np.cos(tth_rad-0.5*pi),
            E*np.sin(tth_rad-0.5*pi),
        )

        vP = vD + vA + vE

        b1 = t1 - vP[1]

        sin_beta_l = b1/B
        beta_rad_l = np.arcsin(b1/B)

        vB_l = self.make_vector(
            B*np.cos(beta_rad_l),
            B*np.sin(beta_rad_l),
        )
        t0_l = vP[0] + vB_l[0]

        beta_rad_h = pi - beta_rad_l

        vB_h = self.make_vector(
            B*np.cos(beta_rad_h),
            B*np.sin(beta_rad_h),
        )
        t0_h = vP[0] + vB_h[0]
        alpha_rad_l = tth_rad - beta_rad_l
        alpha_rad_h = tth_rad - beta_rad_h
        print(f'''\
low P setting:
    alpha     = {DPR*alpha_rad_l:8.4f}
    beta      = {DPR*beta_rad_l:8.4f}
    armx_theo = {t0_l:10.4f}
high P setting:
    alpha     = {DPR*alpha_rad_h:8.4f}
    beta      = {DPR*beta_rad_h:8.4f}
    armx_theo = {t0_h:10.4f}
''')


    def arm_goto(self, tgt_armx_pos=None, tgt_armrm_pos=None, tgt_armrd_pos=None):
        curr_armx_pos  = narmx.position
        curr_armrm_pos = narmrm.position
        curr_armrd_pos = narmrd.position
        drd = tgt_armrd_pos - curr_armrd_pos
        drm = tgt_armrm_pos - curr_armrm_pos
        nsteps = max(2, int(0.5*max(drd,drm)))
        cmd = f'ascan(narmx, {curr_armx_pos}, {tgt_armx_pos}, narmrm, {curr_armrm_pos}, {tgt_armrm_pos},\
              narmrd, {curr_armrd_pos}, {tgt_armrd_pos}, {nsteps}, 0.1)'
        print(cmd)
        ans = yesno('go there (y/n)?')
        if ans:
            a3scan(narmx, curr_armx_pos, tgt_armx_pos, narmrm, curr_armrm_pos, tgt_armrm_pos,
                 narmrd, curr_armrd_pos, tgt_armrd_pos, nsteps, 0.1)

    def diff_goto(self, tgt_tth, tgt_D):
        res = self.diffco_to_armco(tgt_tth, tgt_D)
        print(f'arm pos required:\n    {res.low}')
        ans = yesno('okay - go there (y/n)?')
        if ans:
            self.arm_goto(tgt_armx_pos=res.low.armx, tgt_armrm_pos=res.low.armrm, tgt_armrd_pos=res.low.armrd)
            

    def diffco_to_armco(self, tth, D):
        dfc = self.defaults
        t0_offset, t1, E, A, B = dfc['t0_offset'], dfc['t1'], dfc['E'], dfc['A'], dfc['B']
        tth_rad = RPD*tth
        vD = self.make_vector(
            D*np.cos(tth_rad),
            D*np.sin(tth_rad),
        )
        vA = self.make_vector(
            A*np.cos(tth_rad),
            A*np.sin(tth_rad),
        )
        vE = self.make_vector(
            E*np.cos(tth_rad-0.5*pi),
            E*np.sin(tth_rad-0.5*pi),
        )

        vP = vD + vA + vE

        b1 = t1 - vP[1]

        sin_beta_l = b1/B
        beta_rad_l = np.arcsin(b1/B)

        vB_l = self.make_vector(
            B*np.cos(beta_rad_l),
            B*np.sin(beta_rad_l),
        )
        t0_l = vP[0] + vB_l[0]

        beta_rad_h = pi - beta_rad_l

        vB_h = self.make_vector(
            B*np.cos(beta_rad_h),
            B*np.sin(beta_rad_h),
        )
        t0_h = vP[0] + vB_h[0]
        alpha_rad_l = tth_rad - beta_rad_l
        alpha_rad_h = tth_rad - beta_rad_h
        alpha_deg_l = DPR * alpha_rad_l
        alpha_deg_h = DPR * alpha_rad_h
        beta_deg_l = DPR * beta_rad_l
        beta_deg_h = DPR * beta_rad_h
        armx_l = t0_offset + t0_l
        armx_h = t0_offset + t0_h
        armco_l = Armco(armx=armx_l, armrm=beta_deg_l, armrd=alpha_deg_l)
        armco_h = Armco(armx=armx_h, armrm=beta_deg_h, armrd=alpha_deg_h)
        armco_set = ArmcoSet(low=armco_l, high=armco_h)
        return armco_set

    def diff_armco(self, p0, p1):
        x0,b0,a0 = p0
        x1,b1,a1 = p1
        print(f'dx = {x0-x1}\ndb = {b0-b1}\nda = {a0-a1}')
        return (x0-x1, b0-b1, a0-a1)

    def interactive_goto(self, tth, D, armx_setting='high'):
        src_armco = Armco(narmx.position, narmrm.position, narmrd.position)
        res = self.diffco_to_armco(tth, D)
        tgt_armco = getattr(res, armx_setting)
        print(f'source armco setting={armx_setting}:\n    {src_armco}\n')
        print(f'target armco setting={armx_setting}:\n    {tgt_armco}\n')
        print(f'mvr diff: \n{self.diff_armco(tgt_armco, src_armco)}')

    def armco_print(self, aset):
        print (aset)

def _test():
    ag = EH3ArmGeometry()
    ag.diffco_to_armco(-70.0,300.0)
