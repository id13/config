print('hg172_nov21_raddam - load 3')

import traceback

START_KEY = 'd'  # change each time

def goflux(x):
    if not x in ('low', 'high'):
        raise ValueError('illegal flux specification!')
    if 'low' == x:
        umv(u18, 6.25)
    else:
        umv(u18, 6.4)
    return x

def dooraddam(posname, flux, n, expt):
    try:
        if posname.endswith('._pos'):
            posname = posname[:-5]
        newdataset(f'{posname}_{START_KEY}')
        goflux(flux)
        gopos(posname)
        loopscan(n, expt, eiger, ct24, sleep_time=0.01)
    except:
        print('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')
        print('E')
        print('E')
        print(f'Raddam Error:\n{traceback.format_exc()}')
        


INP = """
test1           high   20   0.02
test2           high   20   0.03
test3           high   20   0.04
"""

def raddam_main():
    ll = [x for x in INP.split('\n') if x.strip()]
    for line in ll:
        print(f'[{line}]')
        (pos, flux, n, expt) = line.split()
        n = int(n)
        expt = float(expt)
        dooraddam(pos, flux, n, expt)

    #dooraddam('Louis')
    #dooraddam('Carole')
    #ooraddam('Vercingetorix')
    # ...

