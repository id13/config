print('ls3044_P1003_S4_overviews: load 7')
POS_COMMENTS = '''
============== stp:
test_pos1
    scan: kmap.dkmap(nnp2, 20,-20,100, nnp3, -30,30,150,0.01)
============== stp:
P1003_S4_test_pos2_0000.json
P1003_S4_test_pos3_0001.json

'''

def overviewMap(pos):
    gopos(pos)
    mv(nnp2,225,nnp3,25)
    kmap.dkmap(nnp2,0,-200,400,nnp3,0,200,400,0.01,frames_per_file=400)


def makeoverviews():
    try:
        eigerhws(True)
        overviewPositions = ['P1003_S4_Osteon_1_ul_0002.json',
        'P1003_S4_Osteon_2_ul_0005.json',
        'P1003_S4_PrimaryBone_1_ul_0010.json']
        for i in overviewPositions:
            print('Now scanning {0}'.format(i))
            overviewMap(i)
    finally:
        sc()
        sc()
        sc()

def kstripes(prefix, nlines_tot, nlines_per_stripe, nlines_step, ncols, ncols_step, exptime, frames_per_file=None):
    if None is frames_per_file:
        frames_per_file = ncols
    (nstripes, fracs) = divmod(nlines_tot, nlines_per_stripe)
    if fracs != 0:
        raise ValueError('illegal lines_per_stripe')

    start_nnp3 = nnp3.position

    for i in range(0,nstripes):
        print(f'scanning stripe # {i} ============================================================')
        newdataset(f'{prefix}_{i:03}')
        curr_nnp3 = start_nnp3 + i*nlines_per_stripe*nlines_step
        print(f'moving to "curr_nnp3" {curr_nnp3}')
        mv(nnp3, curr_nnp3)
        print(f'kmap_args: {nnp2.name, 0, -ncols*ncols_step, ncols, nnp3.name, 0, nlines_per_stripe*nlines_step, nlines_per_stripe, exptime, frames_per_file}')
        kmap.dkmap(nnp2, 0, -ncols*ncols_step, ncols, nnp3, 0, nlines_per_stripe*nlines_step, nlines_per_stripe,
            exptime, frames_per_file=frames_per_file)
        enddataset()
