print('version ID51 ttomo_d')
import time
import traceback

class Bailout(Exception): pass

ORI_ORI_INSTRUCT = """
# kappa,omega
0,0
0,1.9
0,3.8
0,5.7
0,7.6
0,9.5
0,11.4
0,13.3
0,15.2
0,17.1
0,19
0,20.9
0,22.8
0,24.7
0,26.6
0,28.5
0,30.4
0,32.3
0,34.2
0,36.1
0,38
0,39.9
0,41.8
0,43.7
0,45.6
0,47.5
0,49.4
0,51.3
0,53.2
0,55.1
0,57
0,58.9
0,60.8
0,62.7
0,64.6
0,66.5
0,68.4
0,70.3
0,72.2
0,74.1
0,76
0,77.9
0,79.8
0,81.7
0,83.6
0,85.5
0,87.4
0,89.3
0,91.2
0,93.1
0,95
0,96.9
0,98.8
0,100.7
0,102.6
0,104.5
0,106.4
0,108.3
0,110.2
0,112.1
0,114
0,115.9
0,117.8
0,119.7
0,121.6
0,123.5
0,125.4
0,127.3
0,129.2
0,131.1
0,133
0,134.9
0,136.8
0,138.7
0,140.6
0,142.5
0,144.4
0,146.3
0,148.2
0,150.1
0,152
0,153.9
0,155.8
0,157.7
0,159.6
0,161.5
0,163.4
0,165.3
0,167.2
0,169.1
0,171
0,172.9
0,174.8
0,176.7
0,178.6
0,180.5
0,0
40,0
40,11.7
40,23.4
40,35.1
40,46.8
40,58.5
40,70.2
40,81.9
40,93.6
40,105.3
40,117
40,128.7
40,140.4
40,152.1
40,163.8
40,175.5
40,187.2
40,198.9
40,210.6
40,222.3
40,234
40,245.7
40,257.4
40,269.1
40,280.8
40,292.5
40,304.2
40,315.9
40,327.6
40,339.3
0,0
10,0
10,9.1
10,18.2
10,27.3
10,36.4
10,45.5
10,54.6
10,63.7
10,72.8
10,81.9
10,91
10,100.1
10,109.2
10,118.3
10,127.4
10,136.5
10,145.6
10,154.7
10,163.8
10,172.9
10,182
10,191.1
10,200.2
10,209.3
10,218.4
10,227.5
10,236.6
10,245.7
10,254.8
10,263.9
10,273
10,282.1
10,291.2
10,300.3
10,309.4
10,318.5
10,327.6
10,336.7
10,345.8
0,0
30,0
30,10.4
30,20.8
30,31.2
30,41.6
30,52
30,62.4
30,72.8
30,83.2
30,93.6
30,104
30,114.4
30,124.8
30,135.2
30,145.6
30,156
30,166.4
30,176.8
30,187.2
30,197.6
30,208
30,218.4
30,228.8
30,239.2
30,249.6
30,260
30,270.4
30,280.8
30,291.2
30,301.6
30,312
30,322.4
30,332.8
30,343.2
0,0
20,0
20,9.6
20,19.2
20,28.8
20,38.4
20,48
20,57.6
20,67.2
20,76.8
20,86.4
20,96
20,105.6
20,115.2
20,124.8
20,134.4
20,144
20,153.6
20,163.2
20,172.8
20,182.4
20,192
20,201.6
20,211.2
20,220.8
20,230.4
20,240
20,249.6
20,259.2
20,268.8
20,278.4
20,288
20,297.6
20,307.2
20,316.8
20,326.4
20,336
20,345.6
0,0
"""


ORI_INSTRUCT = """
# kappa,omega
20,153.6
20,163.2
20,172.8
20,182.4
20,192
20,201.6
20,211.2
20,220.8
20,230.4
20,240
20,249.6
20,259.2
20,268.8
20,278.4
20,288
20,297.6
20,307.2
20,316.8
20,326.4
20,336
20,345.6
0,0
"""



def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (kap,ome) = l.split(',')
            ko = (int(kap), float(ome))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo.log'):
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(kap,ome)) = instll_item
        self.log('... dummy ko trajectory')
        self.zkap.trajectory_goto(ome, kap, stp=4)
        self.perform_scan()


    def perform_scan(self): 
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        lly *= 0.001
        uly *= 0.001
        llz *= 0.001
        ulz *= 0.001
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
        time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        instll.reverse()
        while(True):
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            instll_item = instll.pop()
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        a , v = instruct[0]
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t = (lly, uly, llz, ulz) = tuple(map(int, w[1:]))
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_corr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz) = fov_t
        lly = 3*(lly//3)
        uly = 3*(uly//3)
        llz = 3*(llz//3)
        ulz = 3*(ulz//3)

        dy = uly - lly
        dz = ulz - llz

        nitvy = dy//3
        nitvz = dz//3

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def sc5257_main(zkap):
    print('Hi!')

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    # 117
    # 150
    scanparams = dict(
        lly = -175,
        uly = 176,
        nitvy = 117,
        llz = -225,
        ulz = 225,
        nitvz = 150,
        expt = 0.005,
        retveloc = 5.0
    )
    ttm = TTomo( 'asy.com', zkap, instll, scanparams)



    # loop over projections
    ttm.mainloop()
