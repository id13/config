from matplotlib import pyplot as plt
from scipy.optimize import leastsq
#from o8x3.io import xyio

import numpy as np

def readxy(fname, dname=None, skip=0):
    if dname:
        fname = os.path.join(dname, fname)
    f = open(fname, "r")
    ll = f.readlines()
    f.close()
    ll = ll[skip:]
    x = np.zeros((len(ll),), dtype=np.float64)
    y = np.zeros((len(ll),), dtype=np.float64)
    for (i,l) in enumerate(ll):
        (x[i],y[i]) = list(map(float, l.split()))
    return (x,y)


def f(x, p):
    _x2 = x*x
    return p[0] + p[1]*x + p[2]*_x2

def rf(p, y, x):
    _x2 = x*x
    print(p, y, x)
    return y - (p[0] + p[1]*x + p[2]*_x2)

def rfsin(p, y, x):
    return y - fsin(x, p)
def fsin(x, p):
    print("fsin x arrg:")
    print(x)
    A,ph,o = tuple(p)
    print("A,Ph,o=", A,ph,o)
    return A*np.sin(x-ph) + o

class SinFit(object):

    def __init__(self, ps, pstep, npoints):
        self.ps = ps
        self.pe = ps + (npoints-1)*pstep
        self.xrang = (pe-ps)

XPP = np.linspace(-180.0,180.0,37)
XP = XPP*np.pi/180.0

    


class MContainer(object):

    def __init__(self, mot, offset, sign):
        # ipos = offset + (sign)*mot.position
        self.mot = mot
        self.offset = offset
        self.sign = sign

    def b2i_pos(self, bpos):
        ipos = self.offset+self.sign*bpos
        return ipos
        
    def i2b_pos(self, bpos):
        bpos = self.offset+self.sign*bpos
        return bpos

    def __str__(self):
        return f'''{self.mot.name}
    pos     =  {self.mot.position}
    ioffset =  {self.offset}
    isign   =  {self.sign}'''

    def __repr__(self):
        return str(self)

def make_motdc_FLUOB_DRIVE():
    cage(nfluoby, -5,5)
    cage(nfluobx, -3,3)
    cage(nny, -3,3)
    cage(nnx, -3,3)
    cage(Theta, -15,15)

    motdc = dict(
        oymot = MContainer(nfluoby, 0, -1),
        oxmot = MContainer(nfluobx, 0, 1),
        cymot = MContainer(nny, 0, 1),
        cxmot = MContainer(nnx, 0, 1),
        thmot = MContainer(Theta, 0, 1)
    )
    return motdc
        

class BlissCenteringHelper(object):

    def __init__(self, motdc, pref=None):
        self.motdc = motdc
        if not None is pref:
            self.reset_all(pref=pref)

    def open(self, mode='w'):
        self.cen_fob = open(self.cen_fn, mode)

    def close(self):
        try:
            self.cen_fn.close()
        except:
            print('close error')

    def reset(self, mode='w'):
        if mode == 'w':
            self.b_cen_ll = []
            self.i_cen_ll = []

    def reset_all(self, pref='cen_default', mode='w'):
        self.close()
        self.reset(mode)
        self.set_outprefix(pref)
        self.open(mode)

    def read_bposset(self):
        bp = {}
        for inm, mc in self.motdc.items():
            bp[inm] = bpos = mc.mot.position
        return bp

    def record_pos(self):
        bp = self.read_bposset()
        self.b_cen_ll.append(bp)
        self.i_cen_ll.append(self.posset_b2i(bp))

    def set_outprefix(self, pref):
        self.cen_fn = f'{pref}_cen.xy'

    def posset_b2i(self, bposset):
        bp = bposset
        ip = {}
        
        for inm, mc in self.motdc.items():
            bpos = bp[inm]
            ipos = mc.b2i_pos(bpos)
            ip[inm] = ipos
        return ip

    def posset_b2i(self, iposset):
        ip = iposset
        bp = {}
        
        for inm, mc in self.motdc.items():
            ipos = ip[inm]
            bpos = mc.i2b_pos(ipos)
            bp[inm] = bpos
        return bp

    def extract_cen_i_thoy(self):
        x = []
        y = []
        for ip in self.i_cen_ll:
           x.append(ip['thmot'])
           y.append(ip['oymot'])
        thrr = np.array(x)*np.pi/180.0
        oyrr = np.array(y)
        return (thrr, oyrr)

    def fit_params(self):
        thrr, oyrr = self.extract_cen_i_thoy()
        p = np.array([5.0,-10.0,0.0], np.float64)
        pl = leastsq(rfsin, p, args=(oyrr, thrr))
        print("pl=", pl)
        yl = fsin(XP, pl[0])
        A = pl[0][0]
        ph = pl[0][1]
        o = pl[0][2]
        print("phase=",  divmod(pl[0][1]*180.0/np.pi, 360.0))
        print("y-comp =", -A*np.sin(ph))
        print("x-comp =", A*np.cos(ph))
        print("That means: umvr nnx " +  str(A*np.cos(ph)) +" umvr X " + str(-A*np.cos(ph)) + " umvr  nny " +  str(-A*np.sin(ph)) + " umvr Y " + str(A*np.sin(ph)))
        xplot = thrr*180.0/np.pi
        #plt.ion()
        plt.plot(XPP,yl)
        plt.plot(xplot,oyrr)
        plt.show()


def _test():

    (x,y) = readxy("cen_tomo2.dat") 
    
    x = np.array(x)*np.pi/180.0
    y = np.array(y)
    p = np.array([5.0,-10.0,0.0], np.float64)
    pl = leastsq(rfsin, p, args=(y, x))
    print("pl=", pl)
    yl = fsin(XP, pl[0])
    A = pl[0][0]
    ph = pl[0][1]
    o = pl[0][2]
    print("phase=",  divmod(pl[0][1]*180.0/np.pi, 360.0))
    print("y-comp =", -A*np.sin(ph))
    print("x-comp =", A*np.cos(ph))
    print("That means: umvr nnx " +  str(A*np.cos(ph)) +" umvr X " + str(-A*np.cos(ph)) + " umvr  nny " +  str(-A*np.sin(ph)) + " umvr Y " + str(A*np.sin(ph)))

    xplot = x*180.0/np.pi
    #plt.ion()
    plt.plot(XPP,yl)
    plt.plot(xplot,y)
    plt.show()
    input()

if __name__ == '__main__':
    _test()
