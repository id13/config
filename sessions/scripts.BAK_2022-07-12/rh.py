print("+ loading scripts/rh.py ...")

def server_print(msg):
    print(msg)
    return 0

def testmicosx(nr):
  time.sleep(30)
  umv(strx,0)
  time.sleep(3)
  for i in range(nr):
    time.sleep(3)
    umvr(strx,1)
    time.sleep(3)
    umvr(strx,-1)
    
def testmicosz(nr):
  time.sleep(30)
  umv(strz,0)
  time.sleep(3)
  for i in range(nr):
    time.sleep(3)
    umvr(strz,1)
    time.sleep(3)
    umvr(strz,-1)
    
def opiomdelay(ch, delay, width, exptime):
  freq = 8
  tick = 1.0/(freq * 1.0e6)
  
  ch = int(ch)
  if ((ch < 1) or (ch > 1) ):
    print(f"Error in channel[{ch}] must be 1 ... 4")
    exit
        
  if( (delay + width) > exptime):
    print(f"Error delay[{delay}] + width[{width}] > exptime[{exptime}]")
    exit
    

  if( (delay < 0) or  (width <= 0) or (exptime <= 0) ):
    print(f"Error delay[{delay}] (<0)  width[{width}] (<=0)exptime[{exptime}](<=0)")
    exit


  ticks_delay = int(delay / tick)
  if ticks_delay == 0:
    ticks_delay = 1
    
  ticks_width = int(width / tick)
  if ticks_width == 0:
    ticks_width = 1
  
  cmd = "CNT %d CLK%d RISE D32 START RISE I8 PULSE %d %d 1" % (ch, freq, ticks_width, ticks_delay) 
  uopiom2.comm(cmd)

  print(cmd)





# MG and CHAIN management
#
#

# measurement groups and chains
#

def mgd():
    # diode (p201) only
    #DEFAULT_CHAIN.set_settings(default_chain_eh3a['chain_config'])
    MG_EH3a.set_active()

def mgdc():
    # diode (p201) only + sim_cam
    DEFAULT_CHAIN.set_settings(default_chain_eh3b['chain_config'])
    MG_EH3b.set_active()

def mgpco():
    # pco camera
    DEFAULT_CHAIN.set_settings(chain_pco['chain_config'])
    MG_PCO.set_active()

def mgeig():
    # eiger detector and diode
    DEFAULT_CHAIN.set_settings(default_chain_eh3['chain_config'])
    MG_EH3.set_active()

def mgvlm():
    # vlm2 microscope camera
    DEFAULT_CHAIN.set_settings(default_chain_eh3vlm2['chain_config'])
    MG_VLM.set_active()    
