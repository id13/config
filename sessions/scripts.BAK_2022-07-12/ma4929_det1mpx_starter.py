print('load 1')
import traceback
import time
def set_condition(idx, attempt):
    # newdataset
    print('\n\n\n======================== setting condition:')
    print(f'    condition index = {idx}')
    print(f'    condition attempt = {attempt}')


def x_check_dets():
    mgallmpx()
    th1 = mpxeh2he.camera.energy_threshold
    th2 = mpxeh2cdte.camera.energy_threshold
    if th1 < 0.5*23.4:
        raise RuntimeError()
    if th2 < 0.5*23.4:
        raise RuntimeError()


def exit_door():
    print('Finished kmap. You can control+c within 5s.')
    sleep(1)
    print('Finished kmap. You can control+c within 4s.')
    sleep(1)
    print('Finished kmap. You can control+c within 3s.')
    sleep(1)
    print('Finished kmap. You can control+c within 2s.')
    sleep(1)
    print('Finished kmap. You can control+c within 1s.')
    sleep(1)
    print('Too late! Wait for after the next map, in 1min 4s...')    




def doo_scan(idx, attempt):
    print('\n\n\n======================== setting condition:')
    print(f'    condition index = {idx}')
    print(f'    condition attempt = {attempt}')
    
    #umv(ustry, 56.55200, ustrz,   -7.46600)
    #dkmapyz_2(-0.1,0.1,10,-0.1,0.1,10, 0.01, 5.0)
    
    
    prefix = "a"
    # POSITIONS GENERALES
    # center en ustry
    ustry_center = 57.3975

    #LFP
    ustrz_surfAl = -11.87
    usrotx_LFP = 0.375
    usroty_LFP = 3.295

    #graphite
    ustrz_surfCu = -5.247
    usrotx_graphite = 1.195
    usroty_graphite = 0.55
    
    #graphite
    #ustrz_surfSep = -7.416-.100
    
    #map parameters
    y_halfwidth = 1
    
    if True : #idx%2 == 0 :
        # Even index : we map the graphite
        print("going to GRAPHITE")
        z_margin = 0.009      
        umv(usrotx, usrotx_graphite, \
            usroty, usroty_graphite, \
            ustry, ustry_center-y_halfwidth, \
            ustrz, ustrz_surfCu+z_margin)     
        newdataset(f'{prefix}_graphite_{idx:04d}_{attempt}')
        #dkmapyz_2(0, 2*y_halfwidth, 20, 0, -0.087-z_margin*2, 37, 0.05, retveloc=5.0)
        dkmapyz_2(0, 2*y_halfwidth, 20, 0, -0.081-z_margin*2, 32, 0.05, retveloc=5.0)
    else:
        # Odd index : we map the LFP
        print("going to NMC")
        z_margin = 0.0
        umv(usrotx, usrotx_LFP, \
            usroty, usroty_LFP, \
            ustry,  ustry_center-y_halfwidth, \
            ustrz,  ustrz_surfAl-z_margin)
        newdataset(f'{prefix}_NMC_{idx:04d}_{attempt}')
        #dkmapyz_2(0, 2*y_halfwidth, 20, 0, 0.108+z_margin*2, 38, 0.05, retveloc=5.0)
        dkmapyz_2(0, 2, 20, 0, 0.087, 29, 0.05, retveloc=5.0)
        
        print("going to separator")
        newdataset(f'{prefix}_Sep_{idx:04d}_{attempt}')
        dkmapyz_2(0, 2, 20, .090, 0.270, 10, 0.05, retveloc=5.0)
        #z_margin = 0.
        #umv(ustry, ustry_center-y_halfwidth, \
        #    ustrz, ustrz_surfSep)
        #newdataset(f'{prefix}_Sep_{idx:04d}_{attempt}')
        #dkmapyz_2(0, 2*y_halfwidth, 20, 0, 0.300, 10, 0.05, retveloc=5.0)


class THEID(object):

    CMD_ID = 580

def det1starter_macro(theidobj, idx, attempt, maxattempts=3):
    if attempt > maxattempts:
        raise RuntimeError(f'maxattempts reached (attempt {attempt} maxattempts {maxattempts}')
    else:
        try:
            doo_scan(idx, attempt)
        except:
            exit_door()
            print(traceback.format_exc())
            print(f'failure {idx} attempt {attempt}')
            THEID.CMD_ID = THEID.CMD_ID+1
            com_sender(THEID.CMD_ID, 'restart_both')
            try:
                time.sleep(20)
                x_check_dets()
            except:
                time.sleep(40)
                x_check_dets()
            det1starter_macro(theidobj, idx, attempt+1, maxattempts=maxattempts)

def det1starter_test_macro():

    for i in range(20000):
        det1starter_macro(THEID, i, 1, maxattempts=5)
        exit_door()
