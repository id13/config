import time

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_day4_2 ARMED ===")
print('RESUME1')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    if expt < 0.01:
        expt = 0.01
    if expt < 0.02 and nitv < 401:
        retveloc = 0.5
    else:
        retveloc = 0.25
    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='d'

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_day4_2():


    dooul('cena2_26RL_ul01_1x1_400x400_400x400_25_0053.json')
    dooul('cena2_27RL_ul01_2x2_200x200_400x400_25_0054.json')
    dooul('cena2_H_ul01_1x1_400x400_400x400_25_0052.json')
    dooul('cena2_J_ul01_1x1_400x400_400x400_25_0048.json')
    dooul('cena2_K_ul01_1x2_400x400_400x400_25_0050.json')
    dooul('cena2_R_ul01_1x1_400x400_400x400_25_0051.json')
    dooul('cena2_S_ul01_1x1_300x400_300x400_25_0049.json')


def hg159_day4_2_check_pos():
    pass
    #gopos('')
