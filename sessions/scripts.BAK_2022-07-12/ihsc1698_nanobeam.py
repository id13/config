# beam size should be less 100 nm, this is 30 mA 16 bunch mode experiments
print('insc1698 load 9')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    zeronnp()
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig()
    name = f'{pos[:-5]}_{start_key}'
    umv(nnz,0,nny,0)
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

straight_film_pos = [
'MX_films_pos1_0000.json',
'MX_films_pos2_0001.json',
]
bend_film_pos = [
#'MX_films_pos4_0003.json',
##'MX_films_pos5_0006.json',
#a collision with aperture happend, sample also shift
##'MX_films_pos6_0007.json',
##'MX_films_pos7_0008.json',
'MX_films_pos8_0009.json',
'MX_films_pos9_0010.json',
]

fiber_waxs_pos = [
'cellulose_fibers_ht_pos1_0017.json',
'cellulose_fibers_ht_pos4_0020.json',
'cellulose_fibers_ion_pos1_0014.json',
'cellulose_fibers_vis_pos1_0011.json',
]

fiber_saxs_pos = [
'cellulose_fibers_ht_pos2_0018.json',
'cellulose_fibers_ion_pos2_0015.json',
'cellulose_fibers_vis_pos2_0012.json',
]

mount03_waxs_pos = [
'mount03_ion_cross_pos1_0027.json',
'mount03_ion_cross_pos3_0028.json',
'mount03_tw_bkgd_pos1_0025.json',
'mount03_tw_pos1_0021.json',
'mount03_tw_pos2_0022.json',
]

mount03_saxs_pos = [
'mount03_ion_cross_pos2_0029.json',
'mount03_ion_cross_pos4_0030.json',
'mount03_tw_bkgd_pos2_0026.json',
'mount03_tw_pos3_0023.json',
'mount03_tw_pos4_0024.json',
]

def march9_night_run2():
    try:
        umv(ndetx,-300)
        start_key = 'd'
        for _ in mount03_waxs_pos:
            gopos_kmap_scan(_,start_key,
                            30,30,0.25,0.25,0.08)
        
        umv(ndetx,0)
        for _ in mount03_saxs_pos:
            gopos_kmap_scan(_,start_key,
                            30,30,0.25,0.25,0.08)
        sc()
    finally:
        sc()
        sc()
        enddataset()

def march9_night_run1():
    try:
        umv(ndetx,-300)
        start_key = 'd'
        for _ in fiber_waxs_pos:
            gopos_kmap_scan(_,start_key,
                            15,20,0.25,0.25,0.05)
        
        umv(ndetx,0)
        for _ in fiber_saxs_pos:
            gopos_kmap_scan(_,start_key,
                            15,20,0.25,0.25,0.05)
        sc()
    finally:
        sc()
        sc()
        enddataset()

def march9_daytime_run2():
    try:
        so()
        mgeig()
        fshtrigger()
        #umv(ndetx,0)
        start_key = 'd'
        for _ in bend_film_pos:
            #gopos_kmap_scan(_,start_key,
            #                60,50,0.25,0.5,0.05)
            # new scan for bend film only use width of 50 microns instead 120
            gopos_kmap_scan(_,start_key,
                            25,50,0.25,0.5,0.05)
        
        #umv(ndetx,-360)
        #for _ in clay_waxs_pos:
        #    gopos_kmap_scan(_,start_key,
        #                    100,5,0.25,0.25,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()

def march9_daytime_run():
    try:
        so()
        mgeig()
        fshtrigger()
        #umv(ndetx,0)
        start_key = 'a'
        for _ in straight_film_pos:
            gopos_kmap_scan(_,start_key,
                            60,50,0.25,0.5,0.05)
        
        #umv(ndetx,-360)
        #for _ in clay_waxs_pos:
        #    gopos_kmap_scan(_,start_key,
        #                    100,5,0.25,0.25,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()
