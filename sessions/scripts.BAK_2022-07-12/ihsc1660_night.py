print('running ihsc1660 v3')
def ihsc1660_night():
    try:
        newdataset('silk_fiber_c_0')
        #gopos('CR_silk_scan_center_0004')
        gopos('CR_silk_scan_center_0004.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp3,-15,15,600,nnp2,25,-25,100,0.05)
        enddataset()
        sc()
    finally:
        sc()
        sc()

def ihsc1660_overnigt_1():
    try:
        umv(ndetx,360)
        newdataset('JL_cross_pos1_a_0')
        gopos('JL_cellulose_HT_cross_pos1_0000.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp2,7,-7,140,nnp3,-7,7,140,0.05)
        enddataset()
        
        umv(ndetx,-300)
        newdataset('JL_cross_pos2_a_0')
        gopos('JL_cellulose_HT_cross_pos2_0001.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp2,8,-8,160,nnp3,-6,6,120,0.05)
        enddataset()
        
        newdataset('JL_Thick_pos1_a_0')
        gopos('JL_cellulose_Thick_pos1_0005.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp2,10,-10,200,nnp3,-10,10,200,0.05)
        enddataset()
        
        newdataset('JL_Thick_pos2_a_0')
        gopos('JL_cellulose_Thick_pos2_0006.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp2,10,-10,200,nnp3,-10,10,200,0.05)
        enddataset()

        sc()
    finally:
        sc()
        sc()
    
    
def ihsc1660_overnight_2():
    try:
        newdataset('JL_Thick_pos3_a_0')
        gopos('JL_cellulose_Thick_pos3_0007.json')
        so()
        fshtrigger()
        kmap.dkmap(nnp2,10,-10,200,nnp3,-10,10,200,0.05)
        enddataset()
        
        sc()
    finally:
        sc()
        sc()
