import sys
sys.path.insert(0,'/data/visitor/ma4929/id13/XXPROCESS')
import os
import time
import pprint
import traceback
from collections import OrderedDict as Odict
from filecommunication import BaseHandler, FileCommunication



class Timeout(object):

    def __init__(self, timeout=0.0):
        self.timeout = timeout
        self.start_t = time.time()

    def get_delta(self):
        return time.time()-self.start_t

    def check_timeout(self):
        delta_t = time.time()-self.start_t
        return (delta_t > self.timeout, delta_t)

    def reset(self):
        self.start_t = time.time()

def mgallmpx(rois=True, usexmap=True):
    MG_EH2.set_active()
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:acq_time_2')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct22')
    MG_EH2.enable('p201_eh2_0:ct2_counters_controller:ct24')
    MG_EH2.enable('mpxeh2he:im*')
    MG_EH2.enable('mpxeh2cdte:im*')
    mpxeh2he.camera.energy_threshold=0.5*23.5
    mpxeh2cdte.camera.energy_threshold=0.5*23.5
    mpxeh2he.proxy.saving_index_format="%06d"
    mpxeh2he.proxy.saving_format="HDF5BS"
    mpxeh2cdte.proxy.saving_index_format="%06d"
    mpxeh2cdte.proxy.saving_format="HDF5BS"

    if usexmap:
        MG_EH2.enable('*xmap*')
    else:
        MG_EH2.disable('*xmap*')

    if rois:
        MG_EH2.enable('mpxeh2*:roi_counters:roi1_*')

def get_detstatus(det):
    cmd = f'supervisorctl status {det} > .python3isamess'
    print(f'executing [{cmd}]')
    os.system(cmd)
    with open('.python3isamess', 'r') as f:
        s = f.read()
    w = s.split()
    return w[1]

def start_det(det):
    cmd = f'supervisorctl start {det} &'
    print(f'executing [{cmd}]')
    os.system(cmd)

def stop_det(det):
    cmd = f'supervisorctl stop {det} &'
    print(f'executing [{cmd}]')
    os.system(cmd)

def reset_both():
    stat = Odict()
    det1 = 'lid13det1_detectors:mpxeh2he'
    det2 = 'lid13det1_detectors:mpxeh2cdte'
    s1 = get_detstatus(det1)
    s2 = get_detstatus(det2)
    stat['initial'] = (-1, s1,s2)
    stop_det(det1)
    stop_det(det2)

    to = Timeout(timeout=30.0)
    to.reset()
    while True:
        exit_condition = 0
        s1 = get_detstatus(det1)
        s2 = get_detstatus(det2)
        stat['to_stop'] = (to.get_delta(), s1, s2)

        if 'STOPPED' == s1:
            exit_condition += 1
        if 'STOPPED' == s2:
            exit_condition += 1
        if 2 == exit_condition:
            break
        (to_flg, dt) = to.check_timeout()
        if to_flg:
            raise(RuntimeError('cannot stop detectors - giving up!'))
            pprint.pprint(stat)
    if exit_condition != 2:
        raise(RuntimeError('cannot stop detectors - giving up (on exit condition)!'))
        pprint.pprint(stat)

    s1 = get_detstatus(det1)
    s2 = get_detstatus(det2)
    stat['after_stop'] = (-1, s1, s2)
    
    start_det(det1)
    start_det(det2)

    to.reset()
    while True:
        exit_condition = 0
        s1 = get_detstatus(det1)
        s2 = get_detstatus(det2)
        stat['to_start'] = (to.get_delta(), s1, s2)

        if 'RUNNING' == s1:
            exit_condition += 1
        if 'RUNNING' == s2:
            exit_condition += 1
        if 2 == exit_condition:
            break
        (to_flg, dt) = to.check_timeout()
        if to_flg:
            raise(RuntimeError('cannot start detectors - giving up!'))
            pprint.pprint(stat)
    if exit_condition != 2:
        raise(RuntimeError('cannot start detectors - giving up (on exit condition)!'))
        pprint.pprint(stat)

    s1 = get_detstatus(det1)
    s2 = get_detstatus(det2)
    stat['after_start'] = (-1, s1, s2)
    
    print('looks successful:')
    pprint.pprint(stat)


class Det1MpxStarter(BaseHandler):

    def __init__(self):
        super().__init__()

    def action(self, instruct):
        for (a,v) in instruct:
            if 'noop' == a:
                pass
            elif 'restart_both' == a:
                reset_both()
            else:
                print(f'instruction: {(a,v)} ignored.')

COM_FNAME = '/data/visitor/ma4929/id13/XXPROCESS/mpx.asy'

def com_receiver():
    hdlr = Det1MpxStarter()
    fcom = FileCommunication(COM_FNAME)
    fcom.set_receive_handler(hdlr)
    fcom.run_receive_server(5.0)

def com_sender(theid, cmd, value=True):
    fcom = FileCommunication(COM_FNAME)
    instructions = list()
    instructions.append(('id', int(theid)))
    instructions.append((cmd, value))
    print(f'sending instructions:\n{instructions}')
    fcom.send(theid, instructions)


if __name__ == '__main__':
    args = sys.argv[1:]
    if args[0] == 'send':
        com_sender(int(args[1]), args[2])
    elif args[0] == 'rec':
        com_receiver()
    else:
        print (args)

