from id13.scripts import kmap
from bliss import setup_globals

# remember        eiger._get_proxy('Eiger').deleteMemoryFiles()
def yzth_scan(thmot,thstart,thstop, thinterv, mot_1, start_1,end_1,steps_1,mot_2,start_2, end_2, steps_2, exp):
    thstep = 1.0*(thstop - thstart)/thinterv

    eiger = setup_globals.eiger
               
    for i in xrange(thinterv+1):
        curr_th = thstart + i*thstep
        print "current th:", curr_th
        ## Move theta
        Theta.move(curr_th)
        ## Do the kmap
        print(kmap.dkmap(mot_1,start_1,end_1,steps_1,mot_2,start_2,end_2,steps_2,exp))
        
def striped_yzth_scan(thmot,thstart,thstop, thinterv, pts_per_stripe,\
mot_1, start_1,end_1,steps_1,mot_2,start_2, end_2, steps_2, exp):
    thstep = 1.0*(thstop - thstart)/thinterv

    eiger = setup_globals.eiger
    n_stripes, modu = divmod(steps_2, pts_per_stripe)
    for i in xrange(thinterv+1):
        curr_th = thstart + i*thstep
        ## Move theta
        Theta.move(curr_th)
        print "current th:", curr_th
    	for i in xrange(n_stripes):
            ## Do the kmap
            print(kmap.dkmap(mot_1,start_1,end_1,steps_1,mot_2,start_2,end_2,pts_per_stripe, exp))
	if modu:
            print(kmap.dkmap(mot_1,start_1,end_1,steps_1,mot_2,start_2,end_2,modu,exp))
