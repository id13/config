print('SC_5259 load 12')


start_key = 'c'

WAXS_list1 = [
'B01_1_4_S8_a_A_0002.json',
'B01_1_4_S8_a_B_0003.json',

'B01_1_4_S8_b_A_0009.json',
'B01_1_4_S8_b_B_0010.json',

##'B01_I_5_SN4a_S5_empty_0002.json',
#'B01_I_5_SN4a_S5_A_0001.json',
#'B01_I_5_SN4a_S5_B_0003.json',
##'B01_I_5_SN4b_S5_empty_0008.json',
#'B01_I_5_SN4b_S5_A_0007.json',
#'B01_I_5_SN4b_S5_B_0009.json',
#'B01_I_5_SN4b_S5_C_0010.json',
#'B01_I_5_SN4b_S5_D_0011.json',
#'B01_I_5_SN4b_S5_E_0012.json',
]

WAXS_list2 = [

'B01_1_4_S8_a_C_0004.json',
'B01_1_4_S8_a_D_0007.json',
'B01_1_4_S8_a_E_0006.json',
'B01_1_4_S8_a_F_0008.json',

'B01_1_4_S8_b_C_0011.json',
'B01_1_4_S8_b_D_0012.json',
'B01_1_4_S8_b_E_0013.json',
'B01_1_4_S8_b_F_0014.json',

##'B01_I_5_SN4a_S5_empty_0002.json',
#'B01_I_5_SN4a_S5_C_0004.json',
#'B01_I_5_SN4a_S5_D_0005.json',
#'B01_I_5_SN4a_S5_E_0006.json',
##'B01_I_5_SN4b_S5_empty_0008.json',
#'B01_I_5_SN4b_S5_F_0013.json',
#'B01_I_5_SN4b_S5_G_0014.json',
#'B01_I_5_SN4b_S5_H_0015.json',
#'B01_I_5_SN4b_S5_I_0016.json',
]

SAXS_list1 = [
'B01_I_5_SN4b_S5_J_0017.json',
'B01_I_5_SN4b_S5_K_0018.json',
'B01_I_5_SN4b_S5_L_0019.json',
#'B01_I_5_SN4b_S5_empty_0008.json',
]


bkgd_list = [
'B01_1_4_S8_a_empty_0016.json',
'B01_1_4_S8_b_empty_0015.json',

#'B01_I_7_SN7_a_empty_0000.json',
#'B01_I_7_SN7_b_empty_0001.json',
]
WAXS_list = [
'B01_I_7_SN7_a_A_0003.json',
'B01_I_7_SN7_a_B_0004.json',
'B01_I_7_SN7_a_C_0005.json',
'B01_I_7_SN7_a_D_0006.json',
'B01_I_7_SN7_b_A_0007.json',
'B01_I_7_SN7_b_B_0008.json',

#'B01_8_S6b_PA_0001.json',
#'B01_8_S6b_PB_0002.json',
#'B01_8_S6b_PC_0003.json',
#'B01_8_S6b_PD_0005.json',
#'B01_8_S6b_PE_0006.json',
#
#'B04_III_1_S5_PF_0005.json',
#'B04_III_1_S5_PD_0003.json',
#'B04_III_1_S5_PB_0001.json',
#'B04_III_1_S5_PRF_0012.json',
#'B04_III_1_S5_PRD_0010.json',
#'B04_III_1_S5_PRB_0008.json',
#
#'B01_I_1_S3_Pos_I-1_S3_F_0005.json',
#'B01_I_1_S3_Pos_I-1_S3_D_0003.json',
#'B01_I_1_S3_Pos_I-1_S3_B_0001.json',
#'B01_II_1_SiN3_posB4_0023.json',
#'B01_II_1_SiN3_posB3_0022.json',

]

SAXS_list = [

#'B04_III_1_S5_PE_0004.json',
#'B04_III_1_S5_PC_0002.json',
#'B04_III_1_S5_PA_0000.json',
#'B04_III_1_S5_PRE_0011.json',
#'B04_III_1_S5_PRC_0009.json',
#'B04_III_1_S5_PRA_0007.json',
#
#'B01_I_1_S3_Pos_I-1_S3_E_0004.json',
#'B01_I_1_S3_Pos_I-1_S3_C_0002.json',
#'B01_I_1_S3_Pos_I-1_S3_A_0000.json',
#'B01_II_1_SiN3_posB2_0021.json',
#'B01_II_1_SiN3_posB1_0020.json',
]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t,scan_axis='h'):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    umv(nnp3,118) 
    #umvr(nnp3,5) 
    if scan_axis == 'h':
        kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,
        zrange,zstep,exp_t)
    if scan_axis == 'v':
        kmap.dkmap(nnp3,zrange,-1*zrange,zstep,nnp2,-1*yrange,
        yrange,ystep,exp_t)
        

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t,scan_axis='h'):
    so()
    fshtrigger()
    mgeig()
    MG_EH3a.enable('*roi3*')
    name = f'{pos[:-10]}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t,scan_axis=scan_axis)
    enddataset()

def run_july_3rd_evening():
    try:
        for i in range(2):
            gopos_kmap_scan(bkgd_list[i],
              '20ms',0,10,10,0.5,0.5,0.02)
        for i in range(2):
            gopos_kmap_scan(bkgd_list[i],
              '50ms',0,10,10,0.5,0.5,0.05)
        for i in range(len(WAXS_list2)):
            gopos_kmap_scan(WAXS_list2[i],start_key,0,15,15,0.1,0.5,0.02)
        for i in range(len(WAXS_list1)):
            gopos_kmap_scan(WAXS_list1[i],start_key,0,15,15,0.1,0.5,0.05)
        sc()
    except:
        sc()
        sc()

def run_july_3rd_afternoon():
    try:
        for i in range(2):
            gopos_kmap_scan(bkgd_list[i],
              '20ms',0,10,10,0.5,0.5,0.02)
        umv(ndetx,-250)
        for i in range(len(WAXS_list)):
            gopos_kmap_scan(WAXS_list[i],start_key,0,10,20,0.1,0.5,0.02)
        sc()
    except:
        sc()
        sc()
            
def bkgd_scan():
    try:
        gopos_kmap_scan('B01_I_5_SN4a_S5_empty_0002.json',
              '20ms',0,10,10,0.5,0.5,0.02)
        gopos_kmap_scan('B01_I_5_SN4a_S5_empty_0002.json',
              '50ms',1,10,10,0.5,0.5,0.05)
        gopos_kmap_scan('B01_I_5_SN4b_S5_empty_0008.json',
              '50ms',0,10,10,0.5,0.5,0.05)
        sc()
    except:
        sc()
        sc()
def run_july_2nd_night():
    try:
        #umv(ndetx,-250)
        #for i in WAXS_list1:
        #    gopos_kmap_scan(i,start_key,0,15,15,0.1,0.1,0.02)
        #for i in WAXS_list2:
        #    gopos_kmap_scan(i,start_key,0,15,15,0.1,0.1,0.05)
        umv(ndetx,300)
        for i in SAXS_list1:
            gopos_kmap_scan(i,start_key,0,15,15,0.1,0.1,0.05)
        sc()
    except:
        sc()
        sc()

def run_july_2nd_afternoon():
    try:
        umv(ndetx,-250)
        for i in WAXS_list:
            gopos_kmap_scan(i,start_key,0,15,25,0.1,0.5,0.05)
        sc()
    except:
        sc()
        sc()
            

def run_july_1st_evening(): 
    try:
        #umv(ndetx,-250)
        #gopos_kmap_scan(WAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
        #                scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        umv(ndetx,-250)		
        gopos_kmap_scan(WAXS_list[1],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[1],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[2],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[2],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')
        		
        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[3],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[3],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        umv(ndetx,-250)		
        gopos_kmap_scan(WAXS_list[4],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[4],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[5],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[5],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')		
        
        sc()
    except:
        sc()
        sc()

def run_june_30th_evening():
    try:
        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        umv(ndetx,-250)		
        gopos_kmap_scan(WAXS_list[1],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[1],start_key,0,25,15,0.5,0.1,0.02,
                        scan_axis='v')
        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[2],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')
        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[2],start_key,0,25,15,0.5,0.1,0.05,
                        scan_axis='v')
        
        sc()
    except:
        sc()
        sc()




def run_june_30th_afternoon():
    try:
        umv(ndetx,-250)

        gopos_kmap_scan(WAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        gopos_kmap_scan(WAXS_list[1],start_key,0,25,15,0.5,0.2,0.05,
                        scan_axis='v')
        #gopos_kmap_scan(WAXS_list[2],start_key,0,10,10,0.1,0.1,0.005,
        #                scan_axis='v')

        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[0],start_key,0,25,15,0.5,0.2,0.02,
                        scan_axis='v')
        gopos_kmap_scan(SAXS_list[1],start_key,0,25,15,0.5,0.2,0.05,
                        scan_axis='v')
        #gopos_kmap_scan(SAXS_list[2],start_key,1,10,30,0.1,0.5,0.005,
        #                scan_axis='v')

        sc()
    except:
        sc()
        sc()



