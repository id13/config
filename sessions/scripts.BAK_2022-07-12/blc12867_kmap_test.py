print('v.06')
launch_prefix = 'd'
def tgopos(x,step_size1=5,patch_size1=1,
             step_size2=1,patch_size2=0.3):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    s.replace('-','_')
    sn  = launch_prefix + '_' + s #x[:-17]#sample name
    print('\n\n',sn,' patch_size1 is {}mm step_size1 is {} um'.format(patch_size1,step_size1),'\n\n')
    print('\n\n',sn,' patch_size2 is {}mm step_size2 is {} um'.format(patch_size2,step_size2),'\n\n')
    gopos(x)
    sleep(1.0)
    
    newdataset(sn)
    fshtrigger()
    return int(patch_size1/(step_size1*1e-3)),int(patch_size2/(step_size2*1e-3))


def kmap_scan(pos_x,patch_size1=1,patch_size2=0.3,
                    step_size1=5,step_size2=1,
                    exptime=0.02):
    try:
        so()
        step_num1,step_num2 = tgopos(pos_x,
                        step_size1=step_size1,patch_size1=patch_size1,
                        step_size2=step_size2,patch_size2=patch_size2)
        sleep(5)
        mgeig()
        ll1 = -1*patch_size1/2
        hl1 = patch_size1/2
        ll2 = -1*patch_size2/2
        hl2 = patch_size2/2
        print('\n\n',ll2,hl2,step_num2,ll1,hl1,step_num1,'\n\n\n')
        dkmapyz_2(ll2,hl2,step_num2,ll1,hl1,step_num1,exptime,retveloc=0.5)
        enddataset()
                
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()


def kmap_test():
    sample     = ['kevlar_knot_0005.json',
                  ]
    patch_size1 = [0.3]
    step_size1  = [2.5]
    patch_size2 = [0.8]
    step_size2  = [2.5]
    for i in sample:
        for ii in patch_size1:
            for iii in step_size1:
                    for __ in step_size2:
                        try:
                            kmap_scan(i,patch_size1=ii,step_size1=iii,
                                        patch_size2=0.8,step_size2=__,
                                        exptime=0.02)    
                        except:
                            last_error
                            pass
    sample     = ['kevlar_twist_0006.json',
                  ]
    patch_size1 = [0.4]
    step_size1  = [2.5]
    patch_size2 = [1.0]
    step_size2  = [2.5]
    for i in sample:
        for ii in patch_size1:
            for iii in step_size1:
                    for __ in step_size2:
                        try:
                            kmap_scan(i,patch_size1=ii,step_size1=iii,
                                        patch_size2=1.0,step_size2=__,
                                        exptime=0.02)    
                        except:
                            last_error
                            pass
