print('blc12965_mcb load 7')

def ybeam_knife_prescan():
    dscan(ustry,-0.15,0.15,50,0.05)
    gofe()

def ybeam_knife_scan():
    dscan(ustry,-0.04,0.04,160,0.1, sleep_time=0.05)
    gofe()

def zbeam_knife_prescan():
    dscan(ustrz,-0.15,0.15,50,0.05)
    gofe()

def zbeam_knife_scan():
    dscan(ustrz,-0.04,0.04,160,0.1, sleep_time=0.05)
    gofe()

def knife_series_y(ds_name, psgap):
    mv(pvg,psgap,phg,psgap)
    newdataset('prealign')
    x_start = -113.8
    delta_x = 0.5
    n_pts = 121
    try:
        mv(ustrx, x_start)
        ybeam_knife_prescan()
        newdataset('may4_ybeam_series_ps50mu_002')
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(ustrx, x_pos)
            ybeam_knife_scan()
    finally:
        enddataset()

def knife_series_z(ds_name, psgap):
    mv(pvg,psgap,phg,psgap)
    newdataset('prealign')
    x_start = -113.8
    delta_x = 0.5
    n_pts = 121
    try:
        mv(ustrx, x_start)
        zbeam_knife_prescan()
        newdataset(ds_name)
        for i in range(n_pts):
            x_pos = x_start + i*delta_x
            print (f'cycle = {i}  x_pos = {x_pos}  ====================================')
            mv(ustrx, x_pos)
            zbeam_knife_scan()
    finally:
        enddataset()

def knife_ps_serseries():
    pos = 'mrostd_mtd_01_cross_initial_0000.json'

    psgap_tup = [(0.05, '50mu'), (0.1,'100mu'),(0.2,'200mu'),(0.3,'300mu')]
    START_KEY = 'a'

    try:
        gopos(pos)
        mvr(ustrz,-0.2)
        for psgap, sgp in psgap_tup[0:1]:
            knife_series_y(f'may4_repeat_y_serseries_ps{sgp}_{START_KEY}', psgap)
        gopos(pos)
        mvr(ustry,-0.2)
        for psgap, sgp in psgap_tup:
            knife_series_z(f'may4_z_serseries_ps{sgp}_{START_KEY}', psgap)
        gopos(pos)
        mvr(ustrz,-0.2)
        for psgap, sgp in psgap_tup:
            knife_series_y(f'may4_y_serseries_ps{sgp}_{START_KEY}', psgap)

    finally:
        sc()
        sc()
