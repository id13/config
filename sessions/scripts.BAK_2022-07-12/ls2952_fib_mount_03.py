print('ls2952_fib_mount_03 load 7')

STARTKEY = 'e'

[
'fib_mount_03_B1_4_0044.json',
'fib_mount_03_B1_2_0041.json',
'fib_mount_03_B2_1_0038.json',
'fib_mount_03_B3_1_0036.json',
'fib_mount_03_B3_2_0043.json',
'fib_mount_03_B4_1_0034.json',
]

def gop_newds(pos):
    print ('=======================')
    w = pos.split('_')
    ds_name = '_'.join(w[:4])
    ds_name = ds_name + '_{}'.format(STARTKEY)
    print(pos)
    gopos(pos)
    print(ds_name)
    newdataset(ds_name)


def ls2952_fib_mount_03():

    #gop_newds('fib_mount_03_B1_4_0044.json')
    #dkpatchmesh(1., 500, 1., 100, 0.02, 1,1)
    #enddataset()

    #gop_newds('fib_mount_03_B1_2_0041.json')
    #dkpatchmesh(1., 500, 2., 200, 0.02, 1,1)
    #enddataset()
    
    #gop_newds('fib_mount_03_B2_1_0038.json')
    #dkpatchmesh(1., 500, 2., 200, 0.02, 1,1)
    #enddataset()
    
    #gop_newds('fib_mount_03_B3_1_0036.json')
    #dkpatchmesh(1., 500, 1., 100, 0.02, 1,1)
    #enddataset()

    #gop_newds('fib_mount_03_B3_2_0043.json')
    #dkpatchmesh(1., 500, 1., 100, 0.02, 1,1)
    #enddataset()
    
    #gop_newds('fib_mount_03_B4_1_0034.json')
    #dkpatchmesh(1., 500, 1.6, 160, 0.02, 1,1)
    #enddataset()

    gop_newds('fib_mount_03_B1_BIS_scanstart_0047.json')
    dkpatchmesh(0.8, 400, 2.0, 200, 0.02, 2,1)
    enddataset()

    
    sc()
    sc()
