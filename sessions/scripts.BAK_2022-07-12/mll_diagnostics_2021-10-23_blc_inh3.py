print ("load 12")
from bliss.setup_globals import *

TRAJ_TABLE = '''#
#          3.2  -1.75
# nnx nnp2 nnp3
# 2.77500, 125.7, 123.35
2.77500, 125.15, 123.55
2.57500,  121.24, 125.27
2.375 ,   118.7, 127
2.175,    115  , 128.7
'''

dydx=(125.7-115)/0.6
dzdx=(123.35-128.7)/0.6

def mmitpmv(x):
    y = 115 + (x-2.175)*dydx
    z = 128.7 + (x-2.175)*dzdx
    print(f'y = {y}   z = {z}')
    elog_print(f'mmitpmv: y = {y}   z = {z}')
    mv(nnp2, y, nnp3, z)
    mv(nnx, x)

def mmpmv(y,z):
    mv(nnp2,y,nnp3,z)

LAUNCH='b'
DSPREFIX = 'auto_focsiem_wbet1_1'

HEADER_COMMENT = f"""

HEADER LAUNCH: {LAUNCH}


Starting of 1 wbet lens prefoc focusing series with astigmatic
long distance HH MLL (short test)

intended macro: mll_diag_blc13364()
dataset prefix: {DSPREFIX}
launch        : {LAUNCH}



"""
def doopl():
   elog_plot(scatter=True)

dooc = elog_print
doop = doopl
dooa = elog_add

def doomp(*p, cmt=None):
    if not None is cmt:
        dooc(f'position_comment {cmt}') 
    for m in p:
        dooc(f'position_log: {m.name} {m.position}')
    

def doointent(s):
   elog_print(f'==== INTENTION:\n{s}')
dooi = doointent

def doofail():
    elog_print(f'{DSPREFIX} - LAUNCH: {LAUNCH}: FAILED!!!')

def macro_start_line():
   l = '===================================================='
   elog_print('\n'.join(('BEGIN MACRO',l,l,l)))
    
def macro_end_line():
   l = '===================================================='
   elog_print('\n'.join(('l,l,l,END MACRO')))
    
def mll_diag_blc13364():
    macro_start_line()

    elog_print(f'{DSPREFIX} - LAUNCH: {LAUNCH}')
    elog_print(HEADER_COMMENT)
    
    sh3.open()
    sh2.open()
    try:
        fshtrigger()
        dsname = f'{DSPREFIX}_{LAUNCH}'
        newdataset(dsname)
        elog_print(f'dataset: {dsname}')
        nnx_start = 2.6
        for i in range(4):
            nnx_curr = nnx_start+i*0.1
            elog_print(f'cycle: {i}    nnx_curr: {nnx_curr}')
            mmitpmv(nnx_curr)
            
            elog_print('scancommand: kmap.dkmap(nnp2,10,-10,200,nnp3, -10,10,200,0.01)')
            kmap.dkmap(nnp2,4,-4,80,nnp3, -4,4,160,0.01)
            #elog_print('scanexecution: okay') 
            elog_plot(scatter=True)
            sleep(1)
            #elog_print('plotexport: okay') 
    finally:
        sh2.close()
        sh2.close()
        enddataset()
        macro_end_line()
