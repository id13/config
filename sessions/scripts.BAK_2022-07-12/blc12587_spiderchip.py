print ("spiderchip patch mesh load 2")
from bliss.setup_globals import *

def blc12587_klineelem(direc, stp=0.25, hleng=100.0, expt=0.05):
    print ("====@@ call blc12587_klineelem", direc, stp, hleng, expt)
    npts = int(2*(hleng/stp))
    if direc == 'h':
        print(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt)
        kmap.dkmap(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt) 
    elif direc == 'v':
        print(nnp3,hleng,-hleng,npts,nnp2,0,0,1,expt)
        kmap.dkmap(nnp3,-hleng,hleng,npts,nnp2,0,0,1,expt)

def blc12587_kline(direc, linleng, elstp=0.25, expt=0.05):
    print ("====@@ call blc12587_kline", direc, linleng, expt)
    hlf_el = 100.0
    el = 2*hlf_el
    ne = int(linleng/el)

    if direc == 'h':
        try:
            hx_spc = 0.001*el
            hx_of = 0.5*hx_spc
            hx_start = nny.position
            for i in range(ne):
                hx_curr = hx_start + hx_of + i*hx_spc
                print("LINE direc:", direc, "  el:", i, "  hx_curr:", hx_curr)
                mv(nny,hx_curr)
                blc12587_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nny,hx_start)
    elif direc == 'v':
        try:
            vx_spc = 0.001*el
            vx_of = 0.5*vx_spc
            vx_start = nnz.position
            for i in range(ne):
                vx_curr = vx_start + vx_of + i*vx_spc
                print("LINE direc:", direc, "  el:", i, "  vx_curr:", vx_curr)
                mv(nnz,vx_curr)
                blc12587_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nnz,vx_start)
    else:
        raise ValueError

def blc12587_klinegrid(direc, leng, elstp, nlines, linestp, expt=0.05):
    print ("====@@ call blc12587_klinegrid", direc, leng, elstp, nlines, linestp, expt)
    if direc == 'h':
        gapmot = nnz
    elif direc == 'v':
        gapmot = nny

    assert gapmot in (nny, nnz)

    gapmot_start = gapmot.position
    try:
        for j in range(nlines):
            gapmot_curr = gapmot_start + j*linestp
            print("GAP direc:", direc, "  el:", j, "  gapmot_curr:", gapmot_curr)
            mv(gapmot,gapmot_curr)
            blc12587_kline(direc, leng, elstp=elstp, expt=expt)
    finally:
        mv(gapmot,gapmot_start)

def blc12587_kpatch(expt=0.05):
    print ("====@@ call blc12587_kpatch", expt)
    #sleep(2)
    #print('kmap.dkmap(nnp2,0,-16,80,nnp3,0,16,80, expt)')
    kmap.dkmap(nnp2,0,-16,80,nnp3,0,16,80, expt)

def blc12587_kpatchmatrix(h,v, hstp=0.016, vstp=0.016, expt=0.05):
    print ("====@@ call blc12587_kpatchmatrix", h, v, hstp, vstp, expt)
    try:
        h_pos = nny.position
        v_pos = nnz.position
        for j in range(v):
            v_curr = v_pos + j*vstp
            mv(nnz,v_curr)
            for i in range(h):
                h_curr = h_pos + i*hstp
                mv(nny,h_curr)
                blc12587_kpatch(expt=expt)
    finally:
        mv(nny, h_pos)
        mv(nnz, v_pos)

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    s.replace('-','_')
    gopos(x)
    newdataset('s01_%s' % s)
    fshtrigger()

def blc12587_main():

    the_expt = 0.05

    so()
    tgopos('spiderchip_x50_ptyhair_p02_0050.json')
    mvr(nnp3,-11)
    try:
        blc12587_kpatchmatrix(5,8, expt=the_expt)   
        sc()
        fshtrigger()
    finally:
        fshtrigger()
        sc()
        sc()
        sc()
