import time

class EXPO:

    expt = 0.02

def dummy(*p):
    print("dummy:",p)

def emul(*p,**kw):
    (rng1,nitv2,rng2,nitv2,expt,ph,pv) = tuple(p)
    print("emul:",p,kw)
    mvr(ustry,ph*rng1,ustrz,pv*rng2)

#newdataset = dummy
#enddataset = dummy
#dkpatchmesh = emul
print("=== hg159_bis_userday2_3.py ARMED ===")
print('RESUME2')
def wate():
    time.sleep(3)
def wate2():
    time.sleep(1)

def dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv):
    expt = EXPO.expt
    print("using exposuretime:", expt)


    retveloc = 1.0

    #if expt < 0.01:
    #    expt = 0.01
    #if expt < 0.02 and nitv < 401:
    #    retveloc = 1.0
    #else:
    #    retveloc = 0.5

    dkpatchmesh(rng1,nitv1,rng2,nitv2,expt,ph,pv,retveloc=retveloc)



DSKEY='c' # to be incremented if there is a crash

def dooul(s):
    print("\n\n\n======================== doing:", s)

    s_pos = s
    if s.endswith('.json'):
        s_ds = s[:-5]
    else:
        s_ds = s
    ulindex = s.find('ul')
    s_an = s_ds[ulindex:]

    w = s_an.split('_')

    (ph,pv) = w[1].split('x')
    (ph,pv) = tp = tuple(map(int, (ph,pv)))

    (nitv1,nitv2) = w[2].split('x')

    nitv1 = int(nitv1)
    nitv2 = int(nitv2)
    rng1,rng2 = w[3].split('x')
    rng1 = float(rng1)/1000.0
    rng2 = float(rng2)/1000.0
    expt = float(w[4])/1000.0

    dsname = "%s_%s" % (s_ds, DSKEY)
    print("datasetname:", dsname)
    print("patch layout:", tp)

    print("\nnewdatset:")
    if dsname.endswith('.json'):
        dsname = dsname[:-5]
    print("modified dataset name:", dsname)
    newdataset(dsname)
    print("\ngopos:")
    gopos(s)
    wate()

    print("\ndooscan[patch mesh]:")
    dooscan(rng1,nitv1,rng2,nitv2,expt,ph,pv)
    wate2()

    print("\nenddataset:")
    enddataset()

    print('5sec to interrupt:')
    sleep(5)

    

def hg159_bis_userday2_3():


    EXPO.expt = 0.06

    dooul('C_plate1_LO_PbO_SO2_ul04_2x2_333x333_500x500_60_0038.json')
    dooul('plate1_s_1-37202-3C_a_ul02_1x1_200x250_200x250_60_0035.json')
    dooul('plate1_s_2-37202-3C_a_ul03_1x1_200x300_200x300_60_0036.json')
    dooul('plate1_s_2-37202-3C_f_ul01_1x1_200x400_200x400_60_0034.json')
    dooul('C_plate1_plastic_ul05_1x1_250x250_500x500_60_0039.json')
    dooul('plate1_BW_antler_ul11_1x1_150x150_150x150_60_0045.json')
    dooul('plate1_BW_cattle_ul08_1x1_150x150_150x150_60_0042.json')
    dooul('plate1_BW_chicken_ul09_1x1_150x150_150x150_60_0043.json')
    dooul('plate1_BW_ivory_ul10_1x1_150x150_150x150_60_0044.json')
    dooul('plate1_BW_pig_ul07_1x1_150x150_150x150_60_0041.json')
    dooul('plate1_BW_sheep_ul06_1x1_150x150_150x150_60_0040.json')














  






   
    




def hg159_day5_1_check_pos():
    pass
    #gopos('')
