print('loading las2950 tolman - 12')
from pprint import pprint
from matplotlib import pyplot as plt
from scipy.optimize import leastsq
#from o8x3.io import xyio
from collections import OrderedDict as Odict
#Odict = dict
import json

import numpy as np

def readxy(fname, dname=None, skip=0):
    if dname:
        fname = os.path.join(dname, fname)
    f = open(fname, "r")
    ll = f.readlines()
    f.close()
    ll = ll[skip:]
    x = np.zeros((len(ll),), dtype=np.float64)
    y = np.zeros((len(ll),), dtype=np.float64)
    for (i,l) in enumerate(ll):
        (x[i],y[i]) = list(map(float, l.split()))
    return (x,y)


def f(x, p):
    _x2 = x*x
    return p[0] + p[1]*x + p[2]*_x2

def rf(p, y, x):
    _x2 = x*x
    print(p, y, x)
    return y - (p[0] + p[1]*x + p[2]*_x2)

def rfsin(p, y, x):
    return y - fsin(x, p)
def fsin(x, p):
    print("fsin x arrg:")
    print(x)
    A,ph,o = tuple(p)
    print("A,Ph,o=", A,ph,o)
    return A*np.sin(x-ph) + o

class SinFit(object):

    def __init__(self, ps, pstep, npoints):
        self.ps = ps
        self.pe = ps + (npoints-1)*pstep
        self.xrang = (pe-ps)

XPP = np.linspace(-360.0,360.0,73)
XP = XPP*np.pi/180.0

    


class MContainer(object):

    def __init__(self, mot, offset, sign):
        # ipos = offset + (sign)*mot.position
        self.mot = mot
        self.offset = offset
        self.sign = sign

    def b2i_pos(self, bpos):
        ipos = self.offset+self.sign*bpos
        return ipos
        
    def i2b_pos(self, bpos):
        bpos = self.offset+self.sign*bpos
        return bpos

    def __str__(self):
        return f'''{self.mot.name}
    pos     =  {self.mot.position}
    ioffset =  {self.offset}
    isign   =  {self.sign}'''

    def __repr__(self):
        return str(self)

def make_motdc_FLUOB_DRIVE():
    cage(nfluoby, -5,5)
    cage(nfluobx, -3,3)
    cage(nny, -3,3)
    cage(nnx, -3,3)
    cage(Theta, -15,15)

    motdc = Odict(
        oymot = MContainer(nfluoby, 0, -1),
        oxmot = MContainer(nfluobx, 0, 1),
        cymot = MContainer(nny, 0, 1),
        cxmot = MContainer(nnx, 0, 1),
        thmot = MContainer(Theta, 0, 1)
    )
    return motdc

class DummyMotor(object):

    def __init__(self, name, position=42.0):
        self.name = name
        self.position  = position

    def move(self, tgt):
        self.position = position

def mod90(x, obj):
    try:
        ph = obj.ph
    except AttributeError:
        ph = float(obj)
    return float(1000*(ph*180/np.pi + x*90))

def mod180(x, obj):
    try:
        ph = obj.ph
    except AttributeError:
        ph = float(obj)
    return float(1000*(ph*180/np.pi + x*180))

def nearest_cos0(smya_pos=None, obj=None):
    try:
        ph = obj.ph
    except AttributeError:
        ph = float(obj)
    if None is smya_pos:
        smya_pos = smya.position
    ll = []
    ll_res = []
    for i in range(-101,101,2):
        c =mod90(i, ph)
        ll_res.append(c)
        dc = np.fabs(c-smya_pos)
        ll.append(dc)
    i = ll.index(min(ll))
    return ll_res[i]
        
def nearest_sin0(smya_pos=None, obj=None):
    try:
        ph = obj.ph
    except AttributeError:
        ph = float(obj)
    if None is smya_pos:
        smya_pos = smya.position
    ll = []
    ll_res = []
    for i in range(-100,102,2):
        c =mod90(i, ph)
        ll_res.append(c)
        dc = np.fabs(c-smya_pos)
        ll.append(dc)
    i = ll.index(min(ll))
    return ll_res[i]
        
def make_motdc_SMARACT_TOMO():
    cage(nny, -3,3)
    cage(nnx, -3,3)
    cage(nnz, -3,3)

    motdc = Odict(
        oymot = MContainer(nnx, 0, 1),
        oxmot = MContainer(nny, 0, 1),
        ozmot = MContainer(nnz, 0, 1),
        cymot = MContainer(DummyMotor('nixy'), 0, 1),
        cxmot = MContainer(DummyMotor('nixx'), 0, 1),
        kamot = MContainer(smxa, 0.0, 1),
        thmot = MContainer(smya, 0.0, 1)
    )
    return motdc
        
'''
load_script('ls2950_birkedal_tolman')
xdc=make_motdc_SMARACT_TOMO()
bch=BlissCenteringHelper(xdc,'tst_a')
bch.store_ortho_ref()
'''

def make_bch():
    xdc=make_motdc_SMARACT_TOMO()
    bch=BlissCenteringHelper(xdc,'tst_a')
    #bch.store_y_ref()
    return bch

class BlissCenteringHelper(object):

    def __init__(self, motdc, pref=None):
        self.motdc = motdc
        if not None is pref:
            self.reset_all(pref=pref)
        self.nny_sign = -1
        self.tilts = Odict(
            tilt00 =  0.0,
            tilt01 = -10000.0,
            tilt02 = -20000.0,
            tilt03 = -30000.0,
            tilt04 = -35000.0,
        )
        self.tilt_diffpos = {}
        self.gen_diffpos = dict(
                d_gen_x = 0,
                d_gen_y = 0,
                d_gen_z = 0
        )
        self.reset()
        self.x_ref = self.y_ref = self.z_ref = 0.0
        self.A     = self.ph    = self.o     = 0.0
        self.A_z   = self.ph_z  = self.o_z   = 0.0
        self.scale_z = 1.0
        self.offset_z = 0.0
        self.z_fit_mode = False

    def set_abs_gen_diffpos(self, gen_x=None, gen_y=None, gen_z=None):
        if not None is gen_x:
            old_gen_x = self.gen_diffpos['d_gen_x']
            self.gen_diffpos['d_gen_x'] = new_gen_x = gen_x
            print(f'changing d_gen_x from {old_gen_x} to {new_gen_x}')
        if not None is gen_y:
            old_gen_y = self.gen_diffpos['d_gen_y']
            self.gen_diffpos['d_gen_y'] = new_gen_y = gen_y
            print(f'changing d_gen_y from {old_gen_y} to {new_gen_y}')
        if not None is gen_z:
            old_gen_z = self.gen_diffpos['d_gen_z']
            self.gen_diffpos['d_gen_z'] = new_gen_z = gen_z
            print(f'changing d_gen_z from {old_gen_z} to {new_gen_z}')

    def set_rel_gen_diffpos(self, gen_x=None, gen_y=None, gen_z=None):
        if not None is gen_x:
            old_gen_x = self.gen_diffpos['d_gen_x']
            self.gen_diffpos['d_gen_x'] = new_gen_x = gen_x + old_gen_x
            print(f'shifting d_gen_x from {old_gen_x} to {new_gen_x} by {gen_x}')
        if not None is gen_y:
            old_gen_y = self.gen_diffpos['d_gen_y']
            self.gen_diffpos['d_gen_y'] = new_gen_y = gen_y + old_gen_y
            print(f'shifting d_gen_y from {old_gen_y} to {new_gen_y} by {gen_y}')
        if not None is gen_z:
            old_gen_z = self.gen_diffpos['d_gen_z']
            self.gen_diffpos['d_gen_z'] = new_gen_z = gen_z + old_gen_z
            print(f'shifting d_gen_z from {old_gen_z} to {new_gen_z} by {gen_z}')

    def reset_gen_diffpos(self):
        print(f'setting all d_gen values to 0 ...')
        self.gen_diffpos['d_gen_x'] = 0
        self.gen_diffpos['d_gen_y'] = 0
        self.gen_diffpos['d_gen_z'] = 0

    def show(self, no_cen_ll=True):
        print('BCH: ========================')
        for att in ('tilts tilt_diffpos gen_diffpos b_cen_ll i_cen_ll y_ref z_ref x_ref A ph o A_z ph_z o_z scale_z offset_z z_fit_mode gen_diffpos'.split()):
            v = getattr(self, att)
            if type(v) == list or type(v) == dict:
                if 'cen_ll' in att and no_cen_ll:
                    pass
                else:
                    print(f'ITEM: {att}: ---------')
                    pprint(v)
            else:
                print(f'ITEM: {att} = {v}')
        print('=====')

    def save_state(self, fn):
        state = dict(
            tilts = self.tilts,
            tilt_diffpos = self.tilt_diffpos,
            b_cen_ll = self.b_cen_ll,
            i_cen_ll = self.i_cen_ll,
            y_ref = self.y_ref,
            z_ref = self.z_ref,
            x_ref = self.x_ref,
            A = self.A,
            ph = self.ph,
            o = self.o,
            A_z = self.A_z,
            ph_z = self.ph_z,
            o_z = self.o_z,
            scale_z = self.scale_z,
            offset_z = self.offset_z,
            gen_diffpos = self.gen_diffpos
        )
        #print(state)
        with open(fn, 'w') as f:
            json.dump(state, f, indent=4)

    def load_state(self, fn):
        with open(fn, 'r') as f:
            state = json.load(f)
        self.tilts = state['tilts']
        self.tilt_diffpos = state['tilt_diffpos']
        self.b_cen_ll = state['b_cen_ll']
        self.i_cen_ll = state['i_cen_ll']
        self.y_ref = state['y_ref']
        self.z_ref = state['z_ref']
        self.x_ref = state['x_ref']
        self.A = state['A']
        self.ph = state['ph']
        self.o = state['o']
        try:
            self.A_z = state['A_z']
            self.ph_z = state['ph_z']
            self.o_z = state['o_z']
            self.scale_z = state['scale_z']
            self.offset_z = state['offset_z']
        except:
            print('WARNING: "_z" features not - not found')
        try:
            self.gen_diffpos = state['gen_diffpos']
        except:
            print('WARNING: gen_diffpos - not found')

    def tilt_goto(self, tiltkey):
        mv(smxa, self.tilts[tiltkey])

    #def open(self, mode='w'):
    #    self.cen_fob = open(self.cen_fn, mode)

    #def close(self):
    #    try:
    #        self.cen_fn.close()
    #    except:
    #        print('close error')

    def reset(self, mode='w'):
        if mode == 'w':
            self.b_cen_ll = []
            self.i_cen_ll = []

    def store_x_ref(self):
        self.o = nnx.position

    def store_y_ref(self):
        self.y_ref = nny.position

    def store_z_ref(self):
        self.z_ref = nnz.position

    def adj_y(self, ay):
        self.adj

    def reset_all(self, pref='cen_default', mode='w'):
        #try:
        #    self.close()
        #except:
        #    pass
        self.reset(mode)
        #self.set_outprefix(pref)
        #self.open(mode)

    def load_table(self, table_fn):
        with open(table_fn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        for l in ll:
            w = l.split()
            bp = Odict()
            for k,sv in zip(self.motdc.keys(), w):
                bp[k] = float(sv)
            ip = self.posset_b2i(bp)
            self.b_cen_ll.append(bp)
            self.i_cen_ll.append(ip)

    def save_table(self, table_fn, force=False):
        if not force and path.exists(table_fn):
            raise FileExistsError(f'not allowed to overwrite {table_fn}')
    
        with open(table_fn, 'w') as f:
            for bp in self.b_cen_ll:
                w = map(str, bp.items())
                l = ' '.join(w) + '\n'
                w = l.split()
                f.write(l)

    def read_bposset(self):
        bp = Odict()
        for inm, mc in self.motdc.items():
            bp[inm] = bpos = mc.mot.position
        return bp

    def record_pos(self):
        bp = self.read_bposset()
        self.b_cen_ll.append(bp)
        self.i_cen_ll.append(self.posset_b2i(bp))

    def store_tilt_ref(self, tiltkey):
        if 'tilt00' == tiltkey:
            self.tilt_diffpos[tiltkey] = dict(
                d_x_ref = 0,
                d_y_ref = 0,
                d_z_ref = 0
            )
        else:
            if np.fabs(smxa.position - self.tilts[tiltkey]) > 1:
                raise ValueError(f'illegal tiltkey {tiltkey}')
            self.tilt_diffpos[tiltkey] = dict(
                d_x_ref = nnx.position - self.o - self.gen_diffpos['d_gen_x'],
                d_y_ref = 0.0,
                d_z_ref = nnz.position - self.z_ref - self.gen_diffpos['d_gen_z']
            )

    def set_outprefix(self, pref):
        self.cen_fn = f'{pref}_cen.xy'

    def posset_b2i(self, bposset):
        bp = bposset
        ip = Odict()
        
        for inm, mc in self.motdc.items():
            bpos = bp[inm]
            ipos = mc.b2i_pos(bpos)
            ip[inm] = ipos
        return ip

    def posset_b2i(self, iposset):
        ip = iposset
        bp = Odict()
        
        for inm, mc in self.motdc.items():
            ipos = ip[inm]
            bpos = mc.i2b_pos(ipos)
            bp[inm] = bpos
        return bp

    def get_phi(self):
        return(smya.position/1000.0)

    def mv_phi(self, tgt):
        mv(smya, 1000.0*tgt)

    def mv_tilt(self, tgt):
        mv(smxa, 1000.0*tgt)

    def mvr_phi(self, dist):
        mvr(smya, 1000.0*dist)

    def mvr_tilt(self, dist):
        mvr(smxa, 1000.0*dist)

    def extract_cen_i_thoy(self):
        x = []
        y = []
        z = []
        for ip in self.i_cen_ll:
           x.append(ip['thmot'])
           y.append(ip['oymot'])
           z.append(ip['ozmot'])
        thrr = np.array(x)*np.pi/180000.0
        oyrr = np.array(y)
        ozrr = np.array(z)
        #ozrr = 50*(ozrr-np.mean(ozrr)) - 2
        return (thrr, oyrr, ozrr)

    def fit_params(self):
        '''
def a(): mvr(nnx,-0.005)                                                                                                                                                                  
def b(): mvr(nnx,-0.01)                                                                                                                                                                   
def c(): mvr(nnx,-0.02)                                                                                                                                                                   
def aa(): mvr(nnx,0.02)                                                                                                                                                                   
def aa(): mvr(nnx,0.005)                                                                                                                                                                  
def bb(): mvr(nnx,0.01)                                                                                                                                                                   
def cc(): mvr(nnx,0.02)      
        '''
        thrr, oyrr, ozrr = self.extract_cen_i_thoy()
        p = np.array([5.0,-10.0,0.0], np.float64)
        pl = leastsq(rfsin, p, args=(oyrr, thrr))
        print("pl=", pl)

        yl = fsin(XP, pl[0])
        A = self.A = pl[0][0]
        ph = self.ph = pl[0][1]
        o = self.o = pl[0][2]

        pl_z = leastsq(rfsin, p, args=(ozrr, thrr))
        print("pl_z=", pl_z)

        zl = fsin(XP, pl_z[0])
        A_z = self.A_z = pl_z[0][0]
        ph_z = self.ph_z = pl_z[0][1]
        o_z = self.o_z = pl_z[0][2]

        print("Y result:")
        print("phase=",  divmod(pl[0][1]*180.0/np.pi, 360.0))
        print("y-comp =", -A*np.sin(ph))
        print("x-comp =", A*np.cos(ph))
        print("That means: umvr nnx " +  str(A*np.cos(ph)) +" umvr X " + str(-A*np.cos(ph)) + " umvr  nny " +  str(-A*np.sin(ph)) + " umvr Y " + str(A*np.sin(ph)))
        xplot = thrr*180.0/np.pi
        #plt.ion()
        plt.plot(XPP,yl)
        plt.plot(xplot,oyrr)
        plt.plot(XPP,self.scale_z*(zl-o_z)+self.offset_z)
        plt.plot(xplot,self.scale_z*(ozrr-o_z)+self.offset_z)
        plt.show()

    def set_z_fit_mode(self, z_fit_mode=False):
        self.z_fit_mode = z_fit_mode

    def set_scale_z(self, scale_z=None, offset_z=None):
        if not None is scale_z:
            self.scale_z = scale_z
        if not None is offset_z:
            self.offset_z = offset_z

    def make_lut(tkk,tkv):
        lut = Odict()
        lut.tkk = tkk
        lut.tkv = tkv
        for bp in self.b_cen_ll:
            lut_k_ll = []
            for kk in tkk:
                kkx = bp[kk]
                codv = str(int(kkx*10))
                lut_k_ll.append(codv)
            lut_k = tuple(lut_k_ll)
            lut_v_ll = []
            for kv in tkv:
                kvx = bp[kv]
                lut_v_ll.append(kvx)
            lut[lut_k] = tuple(lut_v_ll)

    def set_lut(self, lut):
        self.lut = lut

    def lut_k_mv(self, lut_k):
        motdc = self.motdc
        bp = self.lut[lut_k]
        for kv in self.lut.tkv:
            mv(motdc.mot[kv], bp[kv])

    def print_lut(self, lut=None):
        if None is lut:
            lut = self.lut
        print('{:30} : {}'.format(lut.tkk, lut.tkv))
        for k,v in lut.items():
            print('{:30} : {}'.format(k,v))

    def theo(self, smya_pos):
        ismya = np.pi*smya_pos/180000.0
        #corry = self.A*np.sin(ismya-self.ph) + self.o
        corry = self.A*np.sin(ismya-self.ph)
        corrx = self.A*np.cos(ismya-self.ph)
        print(corry)
        print(corrx)
        return corry, corrx

    def apply_corr(self, smya_pos=None, tiltkey='tilt00', auto=False):
        if np.fabs(smxa.position - self.tilts[tiltkey]) > 1:
            raise ValueError(f'illegal tiltkey {tiltkey}')
        #    d_x_ref = nnx.position - self.o,
        #    d_y_ref = nny.position - self.y_ref
        #    d_z_ref = nnz.position - self.z_ref
        if None is smya_pos:
            smya_pos = smya.position
        ismya = np.pi*smya_pos/180000.0
        kappa = smxa.position*np.pi/180000.0

        tddc = self.tilt_diffpos[tiltkey]
        tdiff_x = tddc['d_x_ref']
        tdiff_y = tddc['d_y_ref']
        tdiff_z = tddc['d_z_ref']
        gddc = self.gen_diffpos
        gdiff_x = gddc['d_gen_x']
        gdiff_y = gddc['d_gen_y']
        gdiff_z = gddc['d_gen_z']
        toff_y = self.y_ref + tdiff_y


        if self.z_fit_mode:
            print('using z-fit mode ...')
            z_acmp = self.A_z*np.sin(ismya-self.ph_z)
            print (f'z_acmp = {z_acmp}')
            z_acmp_x = z_acmp*np.sin(-kappa)
            z_acmp_z = z_acmp*np.cos(-kappa)
            toff_x = self.o + z_acmp_x   + tdiff_x
            toff_z = self.o_z + z_acmp_z + tdiff_z
        else:
            print('using simple z mode ...')
            toff_x = self.o + tdiff_x
            toff_z = self.z_ref + tdiff_z
        
        curr_nnx = nnx.position
        curr_nny = nny.position
        curr_nnz = nnz.position

        
        acmp = self.A*np.sin(ismya-self.ph)
        acmp_x = acmp*np.cos(-kappa)
        acmp_y = self.nny_sign*self.A*np.cos(ismya-self.ph)
        acmp_z = acmp*np.sin(-kappa)

        tgt_nnx = toff_x + acmp_x + gdiff_x
        tgt_nny = toff_y + acmp_y + gdiff_y
        tgt_nnz = toff_z + acmp_z + gdiff_z

        print(f'o         = {self.o}')
        print(f'y_ref     = {self.y_ref}')
        print(f'z_ref     = {self.z_ref}')
        print('')
        print(f'tdiff_x   = {tdiff_x}')
        print(f'tdiff_y   = {tdiff_y}')
        print(f'tdiff_z   = {tdiff_z}')
        print('')
        print(f'gdiff_x   = {gdiff_x}')
        print(f'gdiff_y   = {gdiff_y}')
        print(f'gdiff_z   = {gdiff_z}')
        print('')
        print(f'toff_x    = {toff_x}')
        print(f'toff_y    = {toff_y}')
        print(f'toff_z    = {toff_z}')
        print('')
        print(f'acmp_x   = {acmp_x}')
        print(f'acmp_y   = {acmp_y}')
        print(f'acmp_z   = {acmp_z}')
        print('')
        print(f'curr_nnx  = {curr_nnx}')
        print(f'curr_nny  = {curr_nny}')
        print(f'curr_nnz  = {curr_nnz}')
        print('')
        print(f'tgt_nnx   = {tgt_nnx}')
        print(f'tgt_nny   = {tgt_nny}')
        print(f'tgt_nnz   = {tgt_nnz}')
        if auto:
            mv(nnx, tgt_nnx)
            mv(nny, tgt_nny)
            mv(nnz, tgt_nnz)
            return

        ans = yesno('apply nnx correction?')
        if ans or auto:
            mv(nnx, tgt_nnx)
        ans = yesno('apply nny correction?')
        if ans or auto:
            mv(nny, tgt_nny)
        ans = yesno('apply nnz correction?')
        if ans or auto:
            mv(nnz, tgt_nnz)
        ans = yesno('undo?')
        if ans or auto:
            mv(nnx, curr_nnx)
            mv(nny, curr_nny)
            mv(nnz, curr_nnz)

    def goto_nearest_cos0(self):
        print(f'current smya pos = {smya.position}')
        new_pos = nearest_cos0(smya_pos=None, obj=self.ph)
        print(f'new smya pos = {new_pos}')
        ans = yesno('go there?')
        if ans:
            umv(smya, new_pos)

    def goto_nearest_sin0(self):
        print(f'current smya pos = {smya.position}')
        new_pos = nearest_sin0(smya_pos=None, obj=self.ph)
        print(f'new smya pos = {new_pos}')
        ans = yesno('go there?')
        if ans:
            umv(smya, new_pos)

    def where(self):
        wm(smya, smxa)
        whx()
        wmars()


def _test():

    (x,y) = readxy("cen_tomo2.dat") 
    
    x = np.array(x)*np.pi/180000.0
    y = np.array(y)
    p = np.array([5.0,-10.0,0.0], np.float64)
    pl = leastsq(rfsin, p, args=(y, x))
    print("pl=", pl)
    yl = fsin(XP, pl[0])
    A = pl[0][0]
    ph = pl[0][1]
    o = pl[0][2]
    print("phase=",  divmod(pl[0][1]*180.0/np.pi, 360.0))
    print("y-comp =", -A*np.sin(ph))
    print("x-comp =", A*np.cos(ph))
    print("That means: umvr nnx " +  str(A*np.cos(ph)) +" umvr X " + str(-A*np.cos(ph)) + " umvr  nny " +  str(-A*np.sin(ph)) + " umvr Y " + str(A*np.sin(ph)))

    xplot = x*180.0/np.pi
    #plt.ion()
    plt.plot(XPP,yl)
    plt.plot(xplot,y)
    plt.show()
    input()

if __name__ == '__main__':
    _test()
