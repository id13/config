print('load sc5252_generic 5')
START_KEY = 'f'
START_KEY_POS = 'pa'

def dkmapyz_2(ll2,ul2,n2,ll3,ul3,n3,expt):
    print(ll2,ul2,n2,ll3,ul3,n3,expt)
    kmap.dkmap(nnp2,ll2,ul2,n2,nnp3,ll3,ul3,n3,expt)

PARAM_SETS = '''
set2b -50 50 400 -50 50 200 0.01
set3b -50 50 400 -50 50 200 0.005
set7b -50 50 500 -45 45 300 0.003
'''

POSITIONS = '''
cc360_5x_beforeScan_cell1_0001.json set7b
cc360_5x_beforeScan_cell2_0002.json set2b
cc360_5x_beforeScan_cell3_0003.json set3b
cc360_5x_beforeScan_cell4_0004.json set7b
cc360_5x_beforeScan_cell5_0005.json set2b
cc360_5x_beforeScan_cell6_0006.json set3b
cc360_5x_beforeScan_cell7_0007.json set7b
cc360_5x_beforeScan_cell8_0008.json set2b
cc360_5x_beforeScan_cell9_0009.json set3b
'''

def make_grid_posname(cellname, cycle, i, j):
    posname = f'gridpos_yz_{cycle:04d}_{i:02d}_{j:02d}_{cellname}'
    return posname

def generate_pos_grid(dy, dz, ny, nz, cellname):
    curr_y = nny.position
    curr_z = nnz.position
    cycle = 0
    for j in range(nz):
        next_z = curr_z + i*dz
        mv(nny, next_y)
        for i in range(ny):
            print('='*20)
            next_y = curr_y + i*dy
            print(f'cycle={cucle};next_y={next_y};next_z={next_z};i={i};j={j}')
            mv(nny, next_y)
            posname = make_grid_posname(cellname, cycle, i, j)
            stp(posname)
            cycle += 1

def make_positions_string(cellname, ny, nz):
    leng = ny*nz
    paramsets = PARAM_SETS*(ny*nz)
    paramsets = [x.split()[0] for x in paramsets]
    posnames = []
    cycle = 0
    for j in range(nz):
        for i in range(ny):
            cycle += 1
            pn = make_grid_posname(cellname, cycle, i, j)
            posnames.append(pn)
    positions_prep = list(zip(posnames, paramsets))
    lines = []
    for ps in positions_prep:
        p,s = ps
        lines.append(f'{p} {s}'
    lines.append('')
    positions = '\n'.join(lines)
    return positons
    
    



def extract_cellname(posname):
    w = posname.split('_')
    cellname = w[-2]
    return cellname

def sc5252_generic1():
    try:
        pdict = dict()
        ll = PARAM_SETS.split('\n')
        for l in ll:
            l = l.strip()
            if l:
                w = l.split()
                print(w)
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = w
                setk,ll2,ul2,n2,ll3,ul3,n3,expt = (
                    setk,int(ll2),int(ul2),int(n2),
                    int(ll3),int(ul3),int(n3),float(expt)
                )
                pdict[setk] = (ll2,ul2,n2,ll3,ul3,n3,expt)

        ll = POSITIONS.split('\n')
        measlist = []
        for l in ll:
            l = l.strip()
            if l:
                w = l.split()
                print(w)
                posname, setk = w
                measlist.append((posname, setk))
        so()
        mgeig()

        for i, (posname, setk) in enumerate(measlist):
            cellname = extract_cellname(posname)
            roiname = f'{cellname}_{setk}'

            newdataset(f'{roiname}_{START_KEY}')
            gopos(posname)
            dkmapyz_2(*pdict[setk])

    finally:
        sc()
        sc()
        sc()
