print('load - 4')
def sc5187_night1_main():
    try:
        gopos('night1_start')
        start_y = nny.position
        start_z = nnz.position
        for i in range(41):
            mv(nny, start_y + i*0.033)
            mv(nnz, start_z + i*0.040)
            kmap.dkmap(nnp2,-60,60, 120*5, nnp3, -30,0,50,0.02)
    finally:
        sc()
        sc()
        sc()
