import datetime
import os
import base64


#=====================================================
#=====================================================
def testNotifyImage():
  _mgr = current_session.scan_saving.metadata_manager
  fn = "/users/opid13/dont-forget.png"
    
  with open(fn, "rb") as f:
      data = f.read()
  data = b"data:image/png;base64," + base64.b64encode(data)
  
  _mgr.uploadBase64(data)
  

#=====================================================
#=====================================================
def fox(nr):
  bla = ""
  for i in range(nr):
    bla += "[%5d] The quick brown fox jumps over the lazy dog\n" % (i+1,)
  return bla
  
#=====================================================
#=====================================================
def getIsoTime():
   return datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

#=====================================================
#=====================================================
def testFiles():
  _mgr = current_session.scan_saving.metadata_manager
  root = _mgr.dataFolder

  dt = getIsoTime()
  
  p1 = root + "/" + dt + "/eiger"
  os.makedirs(p1)
  with open(p1 + "/dummy.txt", 'w+') as f:
    f.write(fox(500))

  p2 = root + "/" + dt + "/pco"
  os.makedirs(p2)
  with open(p2 + "/dummy.edf", 'w+') as f:
    f.write(fox(500))

  p3 = root + "/" + dt + "/frelon"
  os.makedirs(p3)
  with open(p3 + "/dummy.h5", 'w+') as f:
    f.write(fox(500))


#=====================================================
#=====================================================
def testNotify(msg="Hello World"):
  _mgr = current_session.scan_saving.metadata_manager

  dt = getIsoTime() + ":\n"
  
  _mgr.notifyCommand("CMD " + dt + msg)
  _mgr.notifyDebug("DBG " + dt  + msg)
  _mgr.notifyError("ERR " + dt  + msg)
  _mgr.notifyInfo("INF "  + dt  + fox(15))


#=====================================================
#=====================================================
def metainfo():
  _mgr = current_session.scan_saving.metadata_manager
  _exp = current_session.scan_saving.metadata_experiment
  
  try:
    proposal_mgr = _mgr.proposal
  except:
    proposal_mgr = "ERR"
    
  try:
    beamlineId_mgr        = _mgr.beamlineId
  except:
    beamlineId_mgr = "ERR"
    
  try:
    datafileList_mgr      = _mgr.datafileList
  except:
    datafileList_mgr = "ERR"
    
  try:
    dataFolder_mgr        = _mgr.dataFolder
  except:
    dataFolder_mgr = "ERR"
    
  try:
    datasetName_mgr       = _mgr.datasetName
  except:
    datasetName_mgr = "ERR"
    
  try:
    datasetNameList_mgr   = _mgr.datasetNameList
  except:
    datasetNameList_mgr = "ERR"
    
  try:
    datasetState_mgr      = _mgr.datasetState
  except:
    datasetState_mgr = "ERR"
    
  try:
    lastDatafile_mgr      = _mgr.lastDatafile
  except:
    lastDatafile_mgr = "ERR"
    
  try:
    messageList_mgr       = _mgr.messageList
  except:
    messageList_mgr = "ERR"
    
  try:
    metadataFile_mgr      = _mgr.metadataFile
  except:
    metadataFile_mgr = "ERR"
    
  try:
    sampleName_mgr        = _mgr.sampleName
  except:
    sampleName_mgr = "ERR"
    
  try:
    sampleParameters_mgr  = _mgr.sampleParameters
  except:
    sampleParameters_mgr = "ERR"
    
  try:
    State_mgr       = _mgr.State()
  except:
    State_mgr = "ERR"
    
  try:
    Status_mgr       = _mgr.Status()
  except:
    Status_mgr = "ERR"
    
    
  try:
    proposal_exp = _exp.proposal
  except:
    proposal_exp = "ERR"
    
  try:
    dataRoot_exp = _exp.dataRoot
  except:
    dataRoot_exp = "ERR"
    
  try:
    sample_exp = _exp.sample
  except:
    sample_exp = "ERR"
    
  try:
    State_exp = _exp.State()
  except:
    State_exp = "ERR"
    
  try:
    Status_exp = _exp.Status()
  except:
    Status_exp = "ERR"
    
  
  info = (
    f"        proposal_exp: [{proposal_exp}]  mgr:[{proposal_mgr}]\n"

    f"        dataRoot_exp: [{dataRoot_exp}]\n"
    f"      dataFolder_mgr: [{dataFolder_mgr}]\n"   

    f"          sample_exp: [{sample_exp}] mgr:[{sampleName_mgr}]\n"       

    f"sampleParameters_mgr: [{sampleParameters_mgr}]\n"

    f"      beamlineId_mgr: [{beamlineId_mgr}]\n" 
    f"    datafileList_mgr: [{datafileList_mgr}]\n"    
    f"     datasetName_mgr: [{datasetName_mgr}]\n"      
    f" datasetNameList_mgr: [{datasetNameList_mgr}]\n"  
    f"    lastDatafile_mgr: [{lastDatafile_mgr}]\n"     
    f"     messageList_mgr: [{messageList_mgr}]\n"    
    f"    metadataFile_mgr: [{metadataFile_mgr}]\n"    

    f"    datasetState_mgr: [{datasetState_mgr}] State_mgr:[{State_mgr}]\n" 
    f"           State_exp: [{State_exp}]\n" 
    )
    
  print(info)
