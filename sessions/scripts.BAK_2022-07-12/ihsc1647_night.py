# first test on sweden sample
# fiber about 20 microns, kmap with 30x5 map, step size 500nm and 250nm, exposure 0.02 sec fly scan
# beam was align with gold cross with estimate size of 200x200 nm**2
#fiber1_pos1_center 
#nny,-1.6478
#nnz,0.7893 
#four position very 7 microns along z shift 2 microns in y
#
#fiber 2 pos1_center
#nny -3.5944
#nnz 1.01

print('insc1647 load 4')

def radiation_damage_test(num_scan,yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    for i in range(num_scan):
        kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def radiation_damage_dataset(start_key,sample_key,
                             num_scan,yrange,zrange,
                             ystep_size,zstep_size,exp_t):
    umvr(nnz,0.001)
    so()
    fshtrigger()
    mgeig()
    newdataset(f'{sample_key}_{start_key}')
    radiation_damage_test(num_scan,yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

def june11_night_run():
    try:
        umv(ndetx,-360)
        start_key = 'd'
        # fiber 1
        y_pos = -1.6688#-1.6478
        z_pos = 0.8193#0.7893
        umv(nny,y_pos,nnz,z_pos)
        radiation_damage_dataset(start_key,'fiber1_500nm_detm360_20ms',5,20,2.5,0.5,0.5,0.02)
        
        umv(nny,y_pos,nnz,z_pos+0.007)
        radiation_damage_dataset(start_key,'fiber1_250nm_detm360_20ms',5,20,2.5,0.25,0.25,0.02)

        umv(nny,y_pos,nnz,z_pos+0.007*2)
        radiation_damage_dataset(start_key,'fiber1_500nm_detm360_50ms',5,20,2.5,0.5,0.5,0.05)
        
        umv(nny,y_pos,nnz,z_pos+0.007*3)
        radiation_damage_dataset(start_key,'fiber1_250nm_detm360_50ms',5,20,2.5,0.25,0.25,0.05)
        
        # fiber 2
        y_pos = -3.6084#-3.5944
        z_pos = 1.048#1.01
        umv(nny,y_pos,nnz,z_pos)
        radiation_damage_dataset(start_key,'fiber2_500nm_detm360_20ms',5,20,2.5,0.5,0.5,0.02)
        
        umv(nny,y_pos,nnz,z_pos+0.007)
        radiation_damage_dataset(start_key,'fiber2_250nm_detm360_20ms',5,20,2.5,0.25,0.25,0.02)
        
        umv(nny,y_pos,nnz,z_pos+0.007*2)
        radiation_damage_dataset(start_key,'fiber2_500nm_detm360_50ms',5,20,2.5,0.5,0.5,0.05)
        
        umv(nny,y_pos,nnz,z_pos+0.007*3)
        radiation_damage_dataset(start_key,'fiber2_250nm_detm360_50ms',5,20,2.5,0.25,0.25,0.05)
        
        umv(ndetx,300)
        
        # fiber 1
        y_pos = -1.6688#-1.6478
        z_pos = 0.8193#0.7893
        umv(nny,y_pos,nnz,z_pos+0.007*4)
        radiation_damage_dataset(start_key,'fiber1_500nm_detp300_20ms',5,20,2.5,0.5,0.5,0.02)
        
        umv(nny,y_pos,nnz,z_pos+0.007*5)
        radiation_damage_dataset(start_key,'fiber1_250nm_detp300_20ms',5,20,2.5,0.25,0.25,0.02)

        umv(nny,y_pos,nnz,z_pos+0.007*6)
        radiation_damage_dataset(start_key,'fiber1_500nm_detp300_50ms',5,20,2.5,0.5,0.5,0.05)
        
        umv(nny,y_pos,nnz,z_pos+0.007*7)
        radiation_damage_dataset(start_key,'fiber1_250nm_detp300_50ms',5,20,2.5,0.25,0.25,0.05)
            
        # fiber 2
        y_pos = -3.6084#-3.5944
        z_pos = 1.048#1.01
        umv(nny,y_pos,nnz,z_pos+0.007*4)
        radiation_damage_dataset(start_key,'fiber2_500nm_detp300_20ms',5,20,2.5,0.5,0.5,0.02)
        
        umv(nny,y_pos,nnz,z_pos+0.007*5)
        radiation_damage_dataset(start_key,'fiber2_250nm_detp300_20ms',5,20,2.5,0.25,0.25,0.02)

        umv(nny,y_pos,nnz,z_pos+0.007*6)
        radiation_damage_dataset(start_key,'fiber2_500nm_detp300_50ms',5,20,2.5,0.5,0.5,0.05)
        
        umv(nny,y_pos,nnz,z_pos+0.007*7)
        radiation_damage_dataset(start_key,'fiber2_250nm_detp300_50ms',5,20,2.5,0.25,0.25,0.05)
        
        sc()
    finally:
        sc()
        sc()
        enddataset()
