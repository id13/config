import numpy as np
import math
from time import sleep, time
import xmlrpc.server

from bliss.setup_globals import *

try:
  basestring
except NameError:
  basestring = str


#
#

def yesno(quetxt, genindent=""):
    ans = input("%s%s (y/n)? " % (genindent,quetxt))
    if "y" == ans:
        return True
    else:
        return False

#
# channel-cut mono
#

#
# eh2 esrf hexapod
#
# initial pos:
#uhextx    uhexty    uhextz    uhexrx    uhexry    uhexrz  
#--------  --------  --------  --------  --------  --------
#-0.03192  0.25205   0.52991   -0.06232  0.51470   0.31954
HEX_INIPOS = np.array([float(x) for x in "-0.03192  0.25205   0.52991   -0.06232  0.51470   0.31954".split()], dtype=np.float64)
def whx():
    wm(uhextx, uhexty, uhextz, uhexrx, uhexry, uhexrz)

HEX_FMT = "%10.5f %10.5f %10.5f %10.5f %10.5f %10.5f"

def mv_func(mname, pos):
    mot = bliss.setup_globals[mname]
    print ("mot =", mot.name)
    mv(mot, pos)

def mvr_func(mname, pos):
    mot = globals[manme]
    print ("mot =", mot.name)
    mvr(mot, pos)

def get_motorpos(mobj):
    if isinstance(mobj, basestring):
        mot = bliss.setup_globals[mobj]
    else:
        mot = mobj
    return raw_get_motorpos

def raw_get_motorpos(mot):
    return mot.position

def move_to_posset(possetkeys, post):
    bglb = bliss.setup_globals
    mott = tuple(bglb[x] for x in possetkeys)
    curr_pos = tuple(x.position for x in mott)
    print("%8s  %10s  %10s  %10s" % ("mname","current","target","delta"))
    for i in range(len(possetkeys)):
        print("%8s  %10.5f  %10.5f %10.5f" % (
            possetkeys[i],
            curr_pos[i].
            post[i],
            post[i] - curr_pos[i]
        ))
    ans = yesno("move there")
    if ans:
        print("moving ...")
        for (mk,mp) in zip(possetkeys,post):
            print(mk,"to",mp)
            mv_func(mk,mp)
        print("...done.")
    else:
        print("okay, okay - nothing done.")

class ID13_ServerFuncs(object):

    def __init__(self):
        self.mdict = dict(x=ustrx, y=ustry, z=ustrz)

    def get_pid(self):
        return os.getpid()

    def rmt_mv(self, a_mname, target_pos):
        motname = '<void>'
        ali_mname = '<void>'
        try:
            ali_mname = str(a_mname)
            target_pos = float(target_pos)
            mot = self.mdict(ali_mname)
            mname = mot.name
            mot.move(target_pos)
        except:
            return "error: moving %s (alias %s)" % (mmname, ali_mname)

def run_id13_server():
    return
    sfncs = ID13_ServerFuncs()
    with xmlrpc.server.SimpleXMLRPCServer(('lid13eh21.esrf.fr', 8020)) as server:
        server.register_introspection_functions()
        server.register_function(pow)
        server.register_instance(sfncs)
        server.serve_forever()

def get_hex_inipos():
    return HEX_INIPOS

def hex_compare(posarr=None):
    if None is posarr:
        return get_hex_posarr()-HEX_INIPOS
    else:
        return np.array(posarr)-HEX_INIPOS

def shex_cmp():
    print(HEX_FMT % tuple(hex_compare()))

def mshex_cmp():
    print(HEX_FMT % tuple(-hex_compare()))

def hsnc():
    sync(uhextx, uhexty, uhextz, uhexrx, uhexry, uhexrz)

def hex_move_to_ref():
    t = tuple(get_hex_inipos())
    for m,p in zip((uhextx, uhexty, uhextz, uhexrx, uhexry, uhexrz),t):
        hsnc()
        print (m,p)
        mv(m,p)

def wps():
    wm(pho, phg, pvo, pvg)
    

def get_hex_posarr():
    hexpos = [
        uhextx.position,
        uhexty.position,
        uhextz.position,
        uhexrx.position,
        uhexry.position,
        uhexrz.position
    ]
    return np.array(hexpos, dtype=np.float64)



#
# motors/stages
#
def set_micos_tolerance(mot, tolerance=0.001):
    if mot not in(ustrx, ustrz):
        print("error: illegal motor for set_micos_tolerance")
        print("nothing done ...")
        return
    mot.config.set("tolerance", tolerance)

def set_lm(mot, lolim=None, hilim=None):
    try:
        mot.position
    except AttributeError:
        printr("error: arg1 to set_lm probably not a valid motor/stage instance!")
        print("nothing done ...")
        return
    l,h = mot.limits
    if None is lolim:
        sl = l
    else:
        sl = float(lolim)
    if None is hilim:
        sh = h
    else:
        sh = float(hilim)
    fsl = min(sl, sh)
    fsh = max(sl, sh)
    mot.limits = (fsl, fsh)
    mot.settings_to_config(velocity=False, acceleration=False, limits=True)
    return

def wait_until(t):
    while 1:
        tn = t-time()
        if tn < 0:
            return tn

def wait_until_start(dt):
    print(wait_until(time()+dt))

