print('ihls3398 load 3')


start_key = 'f'


WAXS_list = [
'S8_EC2029_pos2_0004.json',
'S8_EC2029_pos3_0005.json',
'S8_EC2029_pos4_0001.json',
'S8_EC2029_pos6_0003.json',

]



def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig_x()
    MG_EH3a.enable('*:roi1*')
    name = f'{pos[:-10]}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

def run_jun_28th_night():
    try:
        gopos_kmap_scan(WAXS_list[0],start_key,0,50,50,0.5,0.5,0.03)
        gopos_kmap_scan(WAXS_list[1],start_key,0,50,50,0.5,0.5,0.05)
        gopos_kmap_scan(WAXS_list[2],start_key,0,50,50,0.5,0.5,0.06)
        gopos_kmap_scan(WAXS_list[3],start_key,0,50,50,0.5,0.5,0.07)
        sc()
    except:
        sc()
        sc()

#def run_june_11th_morning():
#    try:
#        umv(ndetx,-250)

#        gopos_kmap_scan(WAXS_list[0],start_key,0,20,30,0.5,0.1,0.01)
#        gopos_kmap_scan(WAXS_list[1],start_key,0, 5, 5,0.1,0.1,0.01)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,10,10,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[3],start_key,0,20,20,0.1,0.5,0.005)
#        gopos_kmap_scan(WAXS_list[4],start_key,0,20,15,0.1,0.1,0.005)


#        umv(ndetx,300)
 #       gopos_kmap_scan(SAXS_list[0],start_key,0, 5, 5,0.1,0.1,0.01)
 #       gopos_kmap_scan(SAXS_list[1],start_key,0,10,10,0.1,0.1,0.005)
 #       gopos_kmap_scan(SAXS_list[2],start_key,0,10,30,0.1,0.5,0.005)
 #       gopos_kmap_scan(SAXS_list[3],start_key,0,20,30,0.5,0.1,0.01)
#         gopos_kmap_scan(SAXS_list[3],start_key,0,20,30,0.1,0.1,0.01)
#
#        umv(ndetx,-250)
#        gopos_kmap_scan(WAXS_list[5],start_key,0,20,20,0.1,0.1,0.01)
#        gopos_kmap_scan(WAXS_list[5],start_key,0,20,20,0.07,0.07,0.01)
#
#
#        sc()
#    except:
#        sc()
#        sc()
#
#
#
#
#
#
#def run_june_10th_night():
#    try:
##        umv(ndetx,-250)
##
##        gopos_kmap_scan(WAXS_list[0],start_key,0,10,20,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[2],start_key,0,20,20,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[3],start_key,0,20,10,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[4],start_key,0, 5, 5,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[5],start_key,0,25,25,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[6],start_key,0,20,20,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[7],start_key,0,10,30,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[8],start_key,0, 5, 5,0.1,0.1,0.005)
#
#
#
#        umv(ndetx,300)
#        gopos_kmap_scan(SAXS_list[0],start_key,0,10,20,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[1],start_key,0,20,10,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[2],start_key,0, 5, 5,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[3],start_key,0,15,25,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[4],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[5],start_key,0, 5, 5,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[6],start_key,0,10,20,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[7],start_key,0,15,15,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[8],start_key,0,20,20,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[9],start_key,0,20,10,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[10],start_key,0,25,25,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[11],start_key,0,20,20,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[12],start_key,0,10,30,0.1,0.1,0.005)
#
#
#        sc()
#    except:
#        sc()
#        sc()
#
#
#
#def run_june_10th_evening():
#    try:
#        umv(ndetx,-250)
#
#        gopos_kmap_scan(WAXS_list[0],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[1],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[3],start_key,0, 5, 5,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[4],start_key,0,20,20,0.1,0.1,0.005)
##        gopos_kmap_scan(WAXS_list[5],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[6],start_key,0, 5, 5,0.1,0.1,0.005)
#
#        umv(ndetx,300)
#        gopos_kmap_scan(SAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[3],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[4],start_key,0, 5, 5,0.1,0.1,0.005)
##        gopos_kmap_scan(SAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
#        sc()
#    except:
#        sc()
#        sc()
#
#
#def run_june_9th_night():
#    try:
#        umv(ndetx,-250)
#
#        #gopos_kmap_scan(WAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[3],start_key,1,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[4],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
#        
#        umv(ndetx,300) 
#        gopos_kmap_scan(SAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[3],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[4],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
#        sc()
#    except:
#        sc()
#        sc()
#
#def run_june_9th_lunch():
#    try:
#        umv(ndetx,-250)
#
#        gopos_kmap_scan(WAXS_list[0],start_key,0,30,30,0.1,0.1,0.01)
#       
#         #gopos_kmap_scan(WAXS_list[1],start_key,0,25,25,0.1 ,1,0.01)
#        
#        gopos_kmap_scan(WAXS_list[2],start_key,0,5,5,0.1,0.1,0.01)
#        
#        umv(ndetx,300) 
#        gopos_kmap_scan(SAXS_list[0],start_key,0,25,25,0.1,0.1,0.01)
#        
#        #gopos_kmap_scan(SAXS_list[1],start_key,0,35,35,0.1,0.1,0.01)
#        
#        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5,0.1,0.1,0.01)
#        sc()
#    except:
#        sc()
#        sc()
#
#def run_june_8th():
#    try:
#        umv(ndetx,-250)
#
#        gopos_kmap_scan(WAXS_list[0],start_key,0,12.5,12.5,0.2 ,1,0.01)
#        gopos_kmap_scan(WAXS_list[1],start_key,0,12.5,12.5,0.1 ,1,0.01)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,12.5,12.5,0.05,1,0.01)
#        #for _ in range(10):
#        #    gopos_kmap_scan(WAXS_list[3],start_key,_,12.5,12.5,0.2,1,0.01)
#        
#        umv(ndetx,300) 
#        gopos_kmap_scan(SAXS_list[0],start_key,0,12.5,12.5,0.2 ,1,0.01)
#        gopos_kmap_scan(SAXS_list[1],start_key,0,12.5,12.5,0.1 ,1,0.01)
#        gopos_kmap_scan(SAXS_list[2],start_key,0,12.5,12.5,0.05,1,0.01)
#        for _ in range(10):
#            gopos_kmap_scan(SAXS_list[3],start_key,_,12.5,12.5,0.2,1,0.01)
#        sc()
#    except:
#        sc()
#        sc()
