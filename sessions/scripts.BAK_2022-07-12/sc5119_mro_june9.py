################
# for theta and y mesh scan

print("sc5119_mro june 9 load 4")

START_KEY = 'a'
EXP_T     = 0.005
NITV1     = 500
NITV2     = 100
NITV3     = 5
NWTCH     = 15

SZ        = 70
DZ        = 2.5
SY        = 125
DY        = 100

STh       = -50
ETh       = 50

mov_mot1  = nnp3
mov_mot2  = nnp2
    
def theta_kmap_mesh(
                EXP_T    , 
                NITV1    ,
                NITV2    ,
                NITV3    ,
                NWTCH    ,
                SZ       , 
                DZ       ,
                SY       ,
                DY       , 
                STh      , 
                ETh      , 
                mov_mot1 , 
                mov_mot2 ,
                ):
    #DZ_N = DZ/NITV1
    for j in range(NWTCH):
        SZ = SZ + DZ*2 + (DZ*2/NITV3) 
        DTh  = np.abs(STh-ETh)/NITV1
        umv(Theta,STh-1)
        umv(Theta,STh)
        #start_time = time.time()
        for i in range(NITV1):
            #umv(mov_mot1, SZ  + i*DZ_N)
            umv(Theta, STh + i*DTh)
            frames_per_file = int(NITV2 * NITV3)
            #eiger.saving.frames_per_file = frames_per_file
            umv(mov_mot1,SZ)
            umv(mov_mot2,SY)
            #zeronnp()
            if mov_mot2.name == 'nnp2':
              LY = DY
              HY = -1*DY
            else:
              LY = -1*DY
              HY = DY
            kmap.dkmap(mov_mot2,LY,HY,NITV2,mov_mot1, -1*DZ, DZ, NITV3, 
                       EXP_T, frames_per_file = frames_per_file)
          
        #print(f'one z pos costs {time.time()-start_time} sec')
        #time.sleep(5)
        
def do_theta_kmap_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                NITV3     = NITV3,
                NWTCH     = NWTCH,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                ):
    try:
        dsname = f'{pname}_{START_KEY}'
        newdataset(dsname)
        sync()
        zeronnp()
        fshtrigger()
        so()
        mgeig()
        theta_kmap_mesh(
                  EXP_T     = EXP_T,
                  NITV1     = NITV1,
                  NITV2     = NITV2,
                  NITV3     = NITV3,
                  NWTCH     = NWTCH,
                  SZ        = SZ, 
                  DZ        = DZ,
                  SY        = SY,
                  DY        = DY,
                  STh       = STh,
                  ETh       = ETh,
                  mov_mot1  = mov_mot1,
                  mov_mot2  = mov_mot2,)
        zeronnp()
        enddataset()
        sc()    
    finally:
        eiger.saving.frames_per_file = 200
        enddataset()
        sc()
        sc()
        
def do_theta_kmap_scan_mro_pos(pos,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                NITV3     = NITV3,
                NWTCH     = NWTCH,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                ):
    # this will go to pos and run the scan
    umv(Theta,-1)
    umv(Theta,0)
    gopos(pos) 
    pname = pos[:-5]
    do_theta_kmap_scan_mro(pname,
                START_KEY = START_KEY,
                EXP_T     = EXP_T,
                NITV1     = NITV1,
                NITV2     = NITV2,
                NITV3     = NITV3,
                NWTCH     = NWTCH,
                SZ        = SZ, 
                DZ        = DZ,
                SY        = SY,
                DY        = DY,
                STh       = STh,
                ETh       = ETh,
                mov_mot1   = mov_mot1,
                mov_mot2   = mov_mot2,
                )
    umv(Theta,-1)
    umv(Theta,0)
    
    

vpos = [
'ib334a_2_p03_0003.json',
]

    
def test_run():
    start_time = time.time()
    for p2 in vpos:
      do_theta_kmap_scan_mro_pos(p2,NITV1=5,STh=-1,ETh=0,NWTCH=2,START_KEY='test2')
    print(f'total time{time.time()-start_time}')
    
def june9_run():
    #start_time = time.time()
    for p2 in vpos:
      do_theta_kmap_scan_mro_pos(p2)
    #print(f'total time{time.time()-start_time}')
    
