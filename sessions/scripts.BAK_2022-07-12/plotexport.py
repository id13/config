
def elog_plot(controller=None, scatter=False):
    """Export the actual curve plot to the logbook

    Arguments:
        controller: If specified, a Lima or MCA controller can be specified
                    to export the relative specific plot
    """
    from bliss.common import plot as plot_module
    flint = plot_module.get_flint(creation_allowed=False, mandatory=False)
    if flint is None:
        print("Flint is not available or not reachable")
        return
    flint.wait_end_of_scans()
    if controller is None:
        if scatter:
            p = flint.get_live_plot(kind="default-scatter")
        else:
            p = flint.get_live_plot(kind="default-curve")
    elif isinstance(controller, Lima):
        p = flint.get_live_plot(image_detector=controller.name)
    elif isinstance(controller, BaseMCA):
        p = flint.get_live_plot(mca_detector=controller.name)
    else:
        raise RuntimeError(
            "Reaching plot from controller type {type(controller)} is not supported"
        )
    try:
        p.export_to_logbook()
    except RuntimeError as e:
        print(e.args[0])

