
def set_lm(mot, lolim=None, hilim=None):
    try:
        mot.position
    except AttributeError:
        printr("error: arg1 to set_lm probably not a valid motor/stage instance!")
        print("nothing done ...")
        return
    l,h = mot.limits
    if None is lolim:
        sl = l
    else:
        sl = float(lolim)
    if None is hilim:
        sh = h
    else:
        sh = float(hilim)
    fsl = min(sl, sh)
    fsh = max(sl, sh)
    mot.limits = (fsl, fsh)
    mot.settings_to_config(velocity=False, acceleration=False, limits=True)
    return

