from bliss.common.counter import SoftCounter
from msmb import hello_world
from msmb import set_lm
import numpy
import math

# intlck
# p201
# mono motors
# beamviewver && camera!!
# cameras as detecors


class SimpleSim1(object):

    def __init__(self, motobj, offset=0.0):
        self.motobj = motobj
        self.offset = offset

    @property
    def value(self):
        #print(self.i, self.x)
        v = self.motobj.position - self.offset
        v = math.sin(5.0*v)*(-10*v*v + 5.0)
        #v = 5.0+pow(self.x[self.i]-self.offset, 2)
        #self.i += 1
        return float(v)

    def sim_prep(self, ll, ul, n, expt):
        self.i = 0
        self.x = numpy.linspace(ll, 2*ul, 2*n+1)



def make_sftcnt(motobj, offset=0.0):
    sisi = SimpleSim1(motobj, offset=offset)
    return sisi, SoftCounter(sisi)
