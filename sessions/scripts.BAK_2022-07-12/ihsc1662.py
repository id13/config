print('load ihsc1662 v2')

kw = 'c'

def scan_pos(pos,scan_range):
    try:
        name = pos[:-10] + f"_{kw}"
        gopos(pos)
        newdataset(name)
        so()
        fshtrigger()
        zeronnp()
        npts = int(2*scan_range/0.5)
        #npts = int(2*scan_range/0.2)
        kmap.dkmap(nnp2,scan_range,-1*scan_range,npts,
                   nnp3,-1*scan_range,scan_range,npts,0.1)
        #kmap.dkmap(nnp2,scan_range,-1*scan_range,npts,nnp3,-10,10,100,0.05)
        #kmap.dkmap(nnp2,10,-10,100,nnp3,-10,10,100,0.05)
        enddataset()
        sc()
    finally:
        enddataset()
        sc()
        sc()


def scan_plan3():
    try: 
        umv(ndetx,360)
        scan_pos('beech_section_pos10_SAXS_0017.json',30)
        scan_pos('beech_section_pos6_SAXS_0013.json',30)
        scan_pos('beech_section_pos7_SAXS_0014.json',30)
         
        umv(ndetx,-300)
        scan_pos('beech_section_pos5_WAXS_0012.json',30)
        scan_pos('beech_section_pos8_WAXS_0015.json',30)
        scan_pos('beech_section_pos9_WAXS_0016.json',30)
        sc()
    finally:
        sc()
        sc()    

def scan_plan2():
    try: 
        umv(ndetx,360)
        scan_pos('RISE_fiber_cellulose_100_SAXS_0000.json',10)
        scan_pos('RISE_fiber_lignin_10_SAXS_0003.json',15)
        scan_pos('RISE_fiber_lignin_33_SAXS_0004.json',15)
        scan_pos('RISE_fiber_lignin_50_SAXS_0007.json',20)

         
        umv(ndetx,-300)
        scan_pos('RISE_fiber_cellulose_100_WAXS_0001.json',10)
        scan_pos('RISE_fiber_lignin_10_WAXS_0002.json',15)
        scan_pos('RISE_fiber_lignin_33_WAXS_0005.json',15)
        scan_pos('RISE_fiber_lignin_50_WAXS_0006.json',20)
        
        sc()
    finally:
        sc()
        sc()    


def scan_plan1():
    try: 
        umv(ndetx,-300)
        scan_waxs_list = [
        'Aalto_fiber_HT_WAXS_0004.json',
        'Aalto_fiber_Ion_WAXS_0002.json',
        'Aalto_fiber_Vis_WAXS_0000.json',
        ]
        for _ in scan_waxs_list:
            scan_pos(_)
        
        umv(ndetx,360)
        scan_saxs_list = [
        'Aalto_fiber_HT_SAXS_0005.json',
        'Aalto_fiber_Ion_SAXS_0003.json',
        'Aalto_fiber_Vis_SAXS_0001.json',
        ]
        for _ in scan_saxs_list:
            scan_pos(_)
        sc()
    finally:
        sc()
        sc()    
    
