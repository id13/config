class A(object):

    MOTDC = {}

    def __init__(self, x):
        if x in self.MOTDC:
            raise KeyError()
        else:
            self.MOTDC[x] = x
            self.x = x

    def show(self):
        print(self.MOTDC)


def _test1():
    e = A('e')
    e.show()
    f = A('f')
    f.show()
    e2 = A('e')
    e2.show()

if __name__ == '__main__':
    _test1()
