import time
def dummy(*p):
    print("dummy:",p)

# newdataset = dummy
# enddataset = dummy
# dkpatchmesh = dummy

def wate():
    time.sleep(0.1)
def wate2():
    time.sleep(0.1)

def emul4():
    return
    mvr(ustry,0.4)
    mvr(ustrz,0.4)
    wate2()

def emul8():
    return
    mvr(ustry,0.8)
    mvr(ustrz,0.8)
    wate2()

def emul24():
    return
    mvr(ustry,0.4)
    mvr(ustrz,0.8)
    wate2()

def ls2951_BSWTcr():

    

    newdataset('BWTScr_agar__a')
    gopos('agar')
    wate2()
    mvr(ustrz, -0.2)
    mvr(ustry, -0.2)
    wate()
    dkpatchmesh(0.4,80,0.4,80,0.02,1,1)
    emul24()
    enddataset()

    newdataset('BWTScr_loresA__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    wate()
    dkpatchmesh(0.4,80,0.4,80,0.02,1,2)
    emul8()
    enddataset()

    newdataset('BWTScr_loresB__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    mvr(ustry, 0.4)
    wate()
    dkpatchmesh(0.4,80,0.4,80,0.02,2,2)
    emul8()
    enddataset()

    newdataset('BWTScr_hiresA__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    mvr(ustry, 1.2)
    wate()
    dkpatchmesh(0.4,200,0.4,200,0.02,2,2)
    emul8()
    enddataset()

    newdataset('BWTScr_hiresB__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    mvr(ustry, 2.0)
    wate()
    dkpatchmesh(0.4,200,0.4,200,0.02,2,2)
    emul8()
    enddataset()

    newdataset('BWTScr_hiresC__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    mvr(ustry, 2.8)
    wate()
    dkpatchmesh(0.4,200,0.4,200,0.02,2,2)
    emul8()
    enddataset()

    return

    newdataset('BWTScr_hiresD__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.2)
    mvr(ustry, 3.6)
    wate()
    dkpatchmesh(0.4,200,0.4,200,0.02,2,2)
    emul8()
    enddataset()

    newdataset('BWTScr_loresC__a')
    gopos('OUL')
    wate2()
    mvr(ustrz, 0.1)
    mvr(ustry, 4.4)
    wate()
    dkpatchmesh(0.4,80,0.4,80,0.02,1,2)
    emul24()
    enddataset()
