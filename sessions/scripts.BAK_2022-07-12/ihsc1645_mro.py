import math

print('ihsc1645_mro load 7')

START_KEY = 'e'
EXP_T = 0.05
SQ2 = math.sqrt(2)
SCANR = 100.0
DIAGLIM = SCANR/SQ2
NITV = 400


def do_rad_scan():
    fshtrigger()
    zeronnp()
    dscan(nnp2, 100, -100, NITV, EXP_T)
    d2scan(nnp2, DIAGLIM, -DIAGLIM, nnp3, -DIAGLIM, DIAGLIM, NITV, EXP_T)
    dscan(nnp3, -100, 100, NITV, EXP_T)
    d2scan(nnp2, -DIAGLIM, DIAGLIM, nnp3, DIAGLIM, -DIAGLIM, NITV, EXP_T)
    zeronnp()

def do_mesh_scan():
    fshtrigger()
    zeronnp()
    mvr(nnp2,50)
    mvr(nnp3,-50)
    kmap.dkmap(nnp2,0,100,200,nnp3,0,100,200, EXP_T)
    #print('kmap.dkmap(nnp2,0,100,2,nnp3,0,100,2, EXP_T')
    zeronnp()

def do_one_pos(pname):
    gopos(pname)
    dsname = f'{pname[:-5]}_{START_KEY}'
    newdataset(dsname)
    if 'MSH' in pname:
        do_mesh_scan()
    else:
        do_rad_scan()
    enddataset()

PLL_FULL = '''
mount01_P950_p01_0001.json
mount01_BKG_MSH_p01_0027.json
mount01_P950_p02_0002.json
mount01_P950_p03_0003.json
mount01_P950_p08_0008.json
mount01_P950_p09_0009.json
mount01_P950_MSH_p04_0004.json
mount01_P1350_MSH_p02_0019.json
mount01_P1350_p01_0018.json
mount01_P1350_p03_0020.json
mount01_P1350_p04_0021.json
mount01_P1350_p06_0023.json
mount01_P1350_p08_0025.json
mount01_P1350_p09_0026.json
mount01_P9100_MSH_p01_0010.json
mount01_P9200_p01_0012.json
mount01_P9200_p02_0013.json
mount01_P9200_p04_0015.json
mount01_P9200_p05_0016.json
mount01_P950_MSH_p05_0005.json
mount01_P950_MSH_p06_0006.json
mount01_P950_MSH_p07_0007.json
mount01_P1350_MSH_p05_0022.json
mount01_P1350_MSH_p07_0024.json
mount01_P9100_MSH_p02_0011.json
mount01_P9200_MSH_p03_0014.json
mount01_P9200_MSH_p06_0017.json
'''

PLL_TEST = '''
mount01_P950_p01_0001.json
mount01_BKG_MSH_p01_0027.json
'''

PLL = PLL_FULL

def ihsc1645_mro():
    print(f'DIAGLIM = {DIAGLIM}')
    print(f'NITV = {NITV}')
    print(f'EXP_T = {EXP_T}')

    poslist = PLL.split()
    print(poslist)
    for pname in poslist:
        do_one_pos(pname)
