import time
print ("version 4")

def do_pty_patch():
    kmap.dkmap(nnp3,-4,4,160,nnp2,4,-4, 20,0.0014, frames_per_file=200)

def the_pty8micronpatch():
    nnp2_start = 175.20000+4
    nnp3_start = 78.30000-4

    mv(nnp2, nnp2_start)
    mv(nnp3, nnp3_start)

    for i in range(15):
        print ("===========================------- i cycle:", i)
        nnp3_pos = nnp3_start + i*8.0
        print("nnp3_pos =", nnp3_pos)
        mv(nnp3, nnp3_pos)

        for j in range(15):
            print ("==== j cycle:", j)
            nnp2_pos = nnp2_start - j*8.0
            print("nnp2_pos =", nnp2_pos)
            mv(nnp2, nnp2_pos)
            do_pty_patch()

def pty8micronpatch():
    mgeig()
    fshtrigger()
    so()
    sh1.open()
    time.sleep(5)
    print("running pty8micronpatch ...")
    try:
        the_pty8micronpatch()
    finally:
        try:
            sh1.close()
        finally:
            sc()


DOC = """
center position
                nnp1       nnp2       nnp3
--------  ----------  ---------  ---------
User
 High      250.00000  250.00000  250.00000
 Current   125.00000  123.20000  130.30000
 Low      -100.00000    0.00000    0.00000
Offset       0.00000    0.00000    0.00000

Dial
 High      250.00000  250.00000  250.00000
 Current   125.00000  123.20000  130.30000
 Low      -100.00000    0.00000    0.00000


start position: 

               nnp1       nnp2       nnp3
--------  ----------  ---------  ---------
User
 High      250.00000  250.00000  250.00000
 Current   125.00000  175.20000   78.30000
                       + 4mu       -4mu
 Low      -100.00000    0.00000    0.00000
Offset       0.00000    0.00000    0.00000

Dial
 High      250.00000  250.00000  250.00000
 Current   125.00000  175.20000   78.30000
 Low      -100.00000    0.00000    0.00000




"""
