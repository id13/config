print("macro ihsc1653.py")

def the_scan1():
    dkpatchmesh(0.4,200,0.4,200,0.01,1,1)
    
def the_scan2():
    dkpatchmesh(0.8,400,0.2,100,0.01,1,1)
    
def ihsc1653_submac():
    newdataset('Q-9142')
    gopos('2_13072021_Q-9142_0007.json')
    the_scan1()
    
    newdataset('Q-9143')
    gopos('2_13072021_Q-9143_0005.json')
    the_scan1()

    newdataset('Q-9144')
    gopos('2_13072021_Q-9144_0006.json')
    the_scan1()

    newdataset('Q-9145')
    gopos('2_13072021_Q-9145_0000.json')
    the_scan1()

    newdataset('Q-9146')
    gopos('2_13072021_Q-9146_0001.json')
    the_scan1()

    newdataset('Q-9147')
    gopos('2_13072021_Q-9147_0003.json')
    the_scan1()

    newdataset('Q-9148')
    gopos('2_13072021_Q-9148_0002.json')
    the_scan2()

    newdataset('Q-9149')
    gopos('2_13072021_Q-9149_0004.json')
    the_scan1()

    newdataset('empty')
    gopos('2_13072021_empty_0008.json')
    the_scan1()

def ihsc1653_mac():
    so()
    umv(udetx,-160)
    ihsc1653_submac()
    umv(udetx,-660)
    ihsc1653_submac()
    sc()
    sc()
    
    
