print('load ver 06')
prefix   = 'a'
pos_list = [
'rise_cellulose_fiber_cellulose_100_5x_p01_0000.json',
'rise_cellulose_fiber_cellulose_100_5x_p02_0023.json',
'rise_cellulose_fiber_cellulose_100_5x_p03_0001.json',
'rise_cellulose_fiber_cellulose_100_5x_p04_0002.json',
'rise_cellulose_fiber_cellulose_100_5x_p05_0003.json',
'rise_cellulose_fiber_cellulose_100_5x_p06_0004.json',
'rise_cellulose_fiber_lignin_10_5x_p01_0017.json',
'rise_cellulose_fiber_lignin_10_5x_p02_0018.json',
'rise_cellulose_fiber_lignin_10_5x_p03_0019.json',
'rise_cellulose_fiber_lignin_10_5x_p04_0020.json',
'rise_cellulose_fiber_lignin_10_5x_p05_0021.json',
'rise_cellulose_fiber_lignin_10_5x_p06_0022.json',
'rise_cellulose_fiber_lignin_33_5x_p01_0011.json',
'rise_cellulose_fiber_lignin_33_5x_p02_0012.json',
'rise_cellulose_fiber_lignin_33_5x_p03_0013.json',
'rise_cellulose_fiber_lignin_33_5x_p04_0014.json',
'rise_cellulose_fiber_lignin_33_5x_p05_0015.json',
'rise_cellulose_fiber_lignin_33_5x_p06_0016.json',
'rise_cellulose_fiber_lignin_50_5x_p01_0005.json',
'rise_cellulose_fiber_lignin_50_5x_p02_0006.json',
'rise_cellulose_fiber_lignin_50_5x_p03_0007.json',
'rise_cellulose_fiber_lignin_50_5x_p04_0008.json',
'rise_cellulose_fiber_lignin_50_5x_p05_0009.json',
'rise_cellulose_fiber_lignin_50_5x_p06_0010.json',
]

hum_sp_ls = [30,40,50,60,70,80]

def hum_scan(hum_sp_ls):
    try:
        for (i,_) in enumerate(hum_sp_ls):
            humobj.set_hum(_)
            check_hum = humobj.hum_is_reached()
            while not check_hum:
                humobj.set_hum(_)
                check_hum = humobj.hum_is_reached()
            print('wait for sample to adapt to humidity environment')
            if i == 0:
                 pass
            else:
                 sleep(4800)
            gopos('empty')
            newdataset(f"empty_hum_{_}_{prefix}")
            dkmapyz(-0.02,0.02,19,-0.02,0.02,19,0.1,frames_per_file=400)
            enddataset()
            p = "p{:02d}".format(i+1)
            print(p)
            pl  = []
            for __ in pos_list:
                if p in __:
                    pl.append(__)
            print(pl)
            print('\n\n\n')
            for __ in pl:
                dn = f"{__[:-10]}_hum_{_}_{prefix}"
                gopos(__)
                print('\n',dn,'\n')
                newdataset(dn)
                dkmapyz(-0.02,0.02,19,-0.02,0.02,19,0.1,frames_per_file=400)
                enddataset()
        print('run complete')
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()
