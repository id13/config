print('ls2995 v.21')
launch_prefix = 'a'
def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-10]
    s.replace('-','_')
    nl  = s.split('_')
    sn  = launch_prefix + '_' + nl[1]+'_'+nl[2] #x[:-17]#sample name
    rsl = nl[-2] #resolution
    if rsl == 'lr':
        patch_size = 1 
    if rsl == 'hr':
        patch_size = 0.4
    nn = nl[-1].split('x')
    pr  = int(nn[0])  #patch_row
    pc  = int(nn[1]) #patch_col
    print('\n\n',sn,patch_size,pr,pc,'\n\n')
    gopos(x)
    sleep(1.0)
    
    newdataset(sn)
    fshtrigger()
    return patch_size,pr,pc

def strip1lr1_scan2():
    try:
        so()
        path_size,pr,pc = tgopos('WTCS1_mac1_strip1lr1_lr_2x4_0031.json')
        sleep(5)
        umvr(ustry,1)
        sleep(5)
        umvr(ustry,1)
        mgeig()
        dkpatchmesh(1,200,1,200,0.02,2,2,retveloc=0.5)
        enddataset()
                
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()


def run_patch_scan(json_file):
    try:
        #pc is column and pr is row of scan, which correlate step at y and z in path scan
        so()
        patch_size,pr,pc = tgopos(json_file)
        mgeig()
        dkpatchmesh(patch_size,200,patch_size,200,0.02,pc,pr,retveloc=0.5)
        enddataset()
        
        sc()
        sc()
    finally:
        sc()
        sc()
        sc()

def feb_26_day():
    l = [
 
]
    for i in l:
        run_patch_scan(i)

def ls2995_protein1():
    l = [
        'protein_mac1_supmnlr1_lr_1x1_0000.json',
        'protein_mac1_actasdlr1_lr_1x1_0001.json',
        'protein_mac1_sltasdr1_lr_1x1_0003.json',
    ]
    for i in l:
        run_patch_scan(i)
