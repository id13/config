print ("sc5005_fir_afternoon loading ... 1")

from bliss.setup_globals import *

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    gopos(x)
    newdataset('DS_a_%s' % s)
    fshtrigger()

def WAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 0.0
    dist2 = 15.0

    mvr(nnp2,-dist1)
    fshtrigger()
    kmap.dkmap(nnp3, -38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2,-dist2)
    fshtrigger()
    dmesh(nnp3, -38, 38, 760 , nnp2, 0, -8, 16, 0.2)
    
def SAXS_bombyx(posstr):
    zeronnp()
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0

    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 380 , nnp2, 0, -8, 16, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-38, 38, 760 , nnp2, 0, -4, 8, 0.05)
    
def WAXS_nephila(posstr):
    zeronnp()
    mv(nnp3,118)
    tgopos(posstr)

    dist1 = 0.0
    dist2 = 17.0

    mvr(nnp2, -dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, -dist2)
    fshtrigger()
    dmesh(nnp3,-30, 30, 600 , nnp2, 0, -10, 20, 0.2)
    
def SAXS_nephila(posstr):
    zeronnp()
    mv(nnp3,118)
    tgopos(posstr)

    dist1 = 18.0
    dist2 = 14.0
    
    mvr(nnp2, dist1)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 300 , nnp2, 0, -10, 20, 0.05)

    mvr(nnp2, dist2)
    fshtrigger()
    kmap.dkmap(nnp3,-30, 30, 600 , nnp2, 0, -5, 10, 0.05)


POS = """
"""


def sc5005_main_aftn():



    so()
    try:

        WAXS_nephila('mount03_x50_M11UV_pos1_0017.json')
        #WAXS_nephila('mount03_x50_M11UV_pos2_0019.json')


        umv(ndetx, 300)


        SAXS_nephila('mount03_x50_M11UV_pos1_0017.json')
        #SAXS_nephila('mount03_x50_M11UV_pos2_0019.json')
        
        sc()
    finally:
        sc()
        sc()
        sc()
