print ('load - 31')

def search_scan(search_params):
    dscan(nny, *search_params)

def do_kmap(kmap_params):
    try:
        kmap.dkmap(*kmap_params)
    finally:
        fshtrigger()

def track_fibre_y_up(start_pos, search_params,
    kmap_params, roi_counter, nsegments, start_segment, dmargin, dheight, start_y = None):

    gopos(start_pos)
    start_z = nnz.position
    if not None is start_y:
        print(f'moving to previously aligne nny position: {start_y}')
        mv(nny, start_y)
        

    for i in range(start_segment, nsegments):
        print('===========================================================================:)')
        print(f'segment: {i}')
        curr_z = start_z + i*dheight
        print(f'search: curr_z = {curr_z}')
        mv(nnz, curr_z)
        search_scan(search_params)
        #goto_cen(eiger.counters.roi2_avg, nny)
        nny_before = nny.position
        goto_cen(roi_counter, nny)
        nny_after = nny.position
        print(f'nny: before = {nny_before}  after: {nny_after}')
        curr_z = curr_z + dmargin
        print(f'kmap: curr_z = {curr_z}')
        mv(nnz, curr_z)
        if not i % 1:
            do_kmap(kmap_params)

def fibretracking_main():
# scarr=sc.get_data('p201_eh3_0:ct2_counters_controller:ct34')
    activate_all()
    fshtrigger()
    newdataset('Au_12_I_base_a')
    start_pos = '12_I_base_a'
    start_segment = 0
    start_y = None      
    nsegments = 4 
    search_params = [-0.07,0.07,140,0.02]
    kmap_params = [nnp2, -50,50,100*10, nnp3, 0,20 , 40 , 0.005]
    dheight = -0.02
    dmargin = -0.01
    roi_counter = eiger.counters.roi2_avg
    track_fibre_y_up(start_pos, search_params,
        kmap_params, roi_counter, nsegments, start_segment, dmargin, dheight, start_y=start_y)

def activate_all():
    mgeig_x()
    MG_EH3a.enable('*r:r*')
