def sc5010_afternoon4():

    # 1 ###########################################

    newdataset('BB1750_30')

#          measure_BB1750
    gopos('measure_BB1750')
    dkpatchmesh(0.4,200,0.7,350,0.04,3,2) 

    enddataset()

    # 2 ###########################################

    newdataset('BB1753_30')

#          measure_BB1753
    gopos('measure_BB1753')
    dkpatchmesh(0.334,167,0.268,134,0.04,3,3)

    enddataset()


    # 3 ###########################################

    newdataset('BB1754_30')

#          measure_BB1754
    gopos('measure_BB1754')
    dkpatchmesh(0.34,170,0.334,167,0.04,3,3) 

    enddataset()

    # 4 ###########################################

    newdataset('BB1760_30')

#          measure_BB1760
    gopos('measure_BB1760')
    dkpatchmesh(0.4,200,0.4,200,0.04,3,3) 

    enddataset()
