import sys, os
from time import sleep


class FileCommunication(object):

    def __init__(self, asynfn):
        self.asynfn = asynfn

    def set_receive_handler(self, handler):
        self.receive_handler = handler

    def send(self, theid, instruct):
        ll = [f'{a} = {v}' for (a,v) in instruct]
        ll.append('')
        s = '\n'.join(ll)
        with open(self.asynfn, 'w') as f:
            f.write(s)

    def receive(self):
        instruct = []
        with open(self.asynfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            l = l.strip()
            if not l.startswith('#'):
                if '=' in l:
                    a,v = l.split('=',1)
                    (action, value) = a.strip(), v.strip()
                    instruct.append((action, value))
        return instruct

    def run_receive_server(self, sleep_time):
        stop_flg = True
        while(stop_flg):
            try:
                instruct = self.receive()
                stop_flg = self.receive_handler(instruct)
            except OSError:
                pass
            sleep(sleep_time)
        return

class BaseHandler(object):

    def __init__(self):
        self._theid = -1

    def action(self, instruct):
        print('action!')

    def __call__(self, instruct):
        action, value = instruct[0]
        if 'id' == action:
            print(f'==== found id {int(value)} ==== own id {self._theid}')
            print('==== instruction list:')
            print(instruct)
            iaction = int(value)
            if iaction > self._theid:
                self._theid = iaction
                self.action(instruct)
            else:
                pass
            
        else:
            print('==== malformed instruction list:')
            print(instruct)
        return True

COM_FNAME = 'delme.asy'

def test_com_receiver():
    th = BaseHandler()
    fcom = FileCommunication(COM_FNAME)
    fcom.set_receive_handler(th)
    fcom.run_receive_server(5.0)

def test_com_sender(theid):
    fcom = FileCommunication(COM_FNAME)
    instructions = list()
    instructions.append(('id', int(theid)))
    bla = 'bla'
    instructions.append(('bla', bla*theid))
    print(f'sending instructions:\n{instructions}')
    fcom.send(theid, instructions)

if __name__ == '__main__':
    args = sys.argv[1:]
    if args[0] == 'send':
        test_com_sender(int(args[1]))
    elif args[0] == 'rec':
        test_com_receiver()
    else:
        print (args)
