import math
from bliss.setup_globals import *
SQ2H = math.sqrt(2.0)*0.5

XXX = '''
load_script('nt2virtual')
nt2fac=EH3Nt2Virtual_B()
mot_nt2z = nt2fac.create_softaxis('z')
mot_nt2y = nt2fac.create_softaxis('y')


test_x_series(mot_nt2y, mot_nt2z)
'''

def test_x_series(mot_y, mot_z, tag_float=0.1234):
    ini_x = nt2x.position
    ini_t = nt2t.position
    ini_j = nt2j.position
    stp = -0.02
    loopscan(1,tag_float)
    try:
        for i in range(20):
            new_x = ini_x + stp*i
            mv(nt2x, new_x)
            plotinit(ct32)
            dscan(mot_y, -0.04,0.04, 20, 0.05)
            goc(mot_y)
            where()
            sleep(1)
            plotinit(ct32)
            dscan(mot_z, -0.04,0.04, 20, 0.05)
            goc(mot_z)
            where()
            sleep(1)
            #if not i % 4:
            plotinit(ct32)
            dscan(mot_y, -0.04,0.04, 320, 0.05)
    finally:
        sc()
        sc()
        sc()
        mv(nt2x, ini_x)
        mv(nt2t, ini_t)
        mv(nt2j, ini_j)
        

class EH3Nt2Virtual_B(object):

    def __init__(self):
        self.jmot = nt2j
        self.tmot = nt2t
        
        self.z_pos = 0
        self.y_pos = 0

    def get_positions(self):
        t = self.tmot.position
        j = self.jmot.position
        y = SQ2H*(j-t)
        z = SQ2H*(j+t)
        return (t,j,y,z)

    @property
    def y_position(self):
        t,j,y,z = self.get_positions()
        return y

    @y_position.setter
    def y_position(self, newy):
        t,j,y,z = self.get_positions()
        dy = newy - y
        dt = -0.5*dy/SQ2H
        dj = 0.5*dy/SQ2H
        newt = t + dt
        newj = j + dj
        mv(self.tmot, newt)
        mv(self.jmot, newj)

    @property
    def z_position(self):
        t,j,y,z = self.get_positions()
        return z

    @z_position.setter
    def z_position(self, newz):
        t,j,y,z = self.get_positions()
        dz = newz - z
        dt = 0.5*dz/SQ2H
        dj = 0.5*dz/SQ2H
        newt = t + dt
        newj = j + dj
        mv(self.tmot, newt)
        mv(self.jmot, newj)

    def create_softaxis(self, k):
        if k not in ('y', 'z'):
            raise ValueError()
        return SoftAxis(f'mot_nt2{k}', self, position=f'{k}_position', move=f'{k}_position', tolerance=0.002)

class EH3Nt2Virtual(object):

    def __init__(self):
        self.jmot = nt2j
        self.tmot = nt2t
        
        self.z_pos = 0
        self.z_off = 0
        self.y_pos = 0
        self.y_off = 0

    def get_raw_y(self):
        t = self.tmot.position
        j = self.jmot.position
        return t+j

    def get_raw_z(self):
        t = self.tmot.position
        j = self.jmot.position
        return j-t

    #def set_nt2virtual(self, y=None, z=None):
    #    if not None is y:
    #        old_y = self.y_position
    #        dy = y - old_y
    #        self.y_off += dy
    #    if not None is z:
    #        old_y = self.z_position
    #        dz = z - old_z
    #        self.z_off += dz

    @property
    def y_position(self):
        return self.get_raw_y() + self.y_off

    @y_position.setter
    def y_position(self, pos):
        old_pos = self.y_position
        delta = pos - old_pos
        dt, dj = self.get_y_delta_tj(delta)
        t = self.tmot.position
        j = self.jmot.position
        mv(self.jmot, j + dj)
        mv(self.tmot, t + dt)

    @property
    def z_position(self):
        return self.get_raw_z() + self.z_off

    @z_position.setter
    def z_position(self, pos):
        old_pos = self.z_position
        delta = pos - old_pos
        dt, dj = self.get_z_delta_tj(delta)
        t = self.tmot.position
        j = self.jmot.position
        mv(self.jmot, j + dj)
        mv(self.tmot, t + dt)

    def get_y_delta_tj(self, delta_y):
        dt = delta_y*SQ2H
        dj = delta_y*SQ2H
        return dt, dj

    def get_z_delta_tj(self, delta_y):
        dt = -delta_y*SQ2H
        dj = delta_y*SQ2H
        return dt, dj

    def create_softaxis(self, k):
        if k not in ('y', 'z'):
            raise ValueError()
        return SoftAxis(f'mot_nt2{k}', self, position=f'{k}_position', move=f'{k}_position', tolerance=0.002)

