from bliss.setup_globals import *

# this is trying to consist with new akmap function, which automatically log the info in eh3 setup
def dkmap(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time, frames_per_file=2000):
    try:
      kmap.dkmap(fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time, frames_per_file=frames_per_file)
    finally:
      s1 = ('kmap.dkmap(%s ,%f ,%f ,%d ,%s ,%f ,%f ,%d ,%f ,%d)' %(fast_mot.name, xmin, xmax, x_nb_points, slow_mot.name, ymin, ymax, y_nb_points, expo_time, frames_per_file))
      s2 = ('start time: %s' %SCANS[-1].scan_info['start_time_str']) 
      s3 = ('metafile name: %s' %SCANS[-1].scan_info['filename']) 
      s4 = ('scan number: %d' %(SCANS[-1].scan_info['scan_nb'])) 
      lotoo.elog_info('ID13log: \n' + s1 +  '\n' + s2 + '\n' + s3 + '\n' + s4)

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    s.replace('-','_')
    gopos(x)
    newdataset('s01_%s' % s)
    fshtrigger()

def klineelem(direc, stp=0.25, hleng=100.0, expt=0.05, frames_per_file=2000):
    print ("====@@ call klineelem", direc, stp, hleng, expt)
    npts = int(2*(hleng/stp))
    if direc == 'h':
        print(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt)
        #kmap.dkmap(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt) 
        dkmap(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt, frames_per_file=2000)
    elif direc == 'v':
        print(nnp3,hleng,-hleng,npts,nnp2,0,0,1,expt)
        #kmap.dkmap(nnp3,-hleng,hleng,npts,nnp2,0,0,1,expt)
        dkmap(nnp3,-hleng,hleng,npts,nnp2,0,0,1,expt, frames_per_file=2000)

def kline(direc, linleng, elstp=0.25, expt=0.05,
          hlf_el=100, frames_per_file=2000):
    # linleng: total line length, of that unit is micron, composed by several parts-"el"    
    # el is line scan conducted by dkmap, and is central symetical scan
    # movement to the center of each el is conduct by hex, which has unit of millimeter
    # elstp could set to 1 maybe
    print ("====@@ call kline", direc, linleng, expt)
    #hlf_el = 100.0
    el = 2*hlf_el
    ne = int(linleng/el)

    if direc == 'h':
        try:
            hx_spc = 0.001*el
            hx_of = 0.5*hx_spc
            hx_start = nny.position
            for i in range(ne):
                hx_curr = hx_start + hx_of + i*hx_spc
                print("LINE direc:", direc, "  el:", i, "  hx_curr:", hx_curr)
                mv(nny,hx_curr)
                klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt, frames_per_file=2000)
        finally:
            mv(nny,hx_start)
    elif direc == 'v':
        try:
            vx_spc = 0.001*el
            vx_of = 0.5*vx_spc
            vx_start = nnz.position
            for i in range(ne):
                vx_curr = vx_start + vx_of + i*vx_spc
                print("LINE direc:", direc, "  el:", i, "  vx_curr:", vx_curr)
                mv(nnz,vx_curr)
                klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt, frames_per_file=2000)
        finally:
            mv(nnz,vx_start)
    else:
        raise ValueError


def klinegrid(direc, leng, elstp, nlines, linestp, expt=0.05, frames_per_file=2000):
    # this is a function move perpendicular to kline scan,
    # nlines determine how many kline scan will perform, and step size is linestp
    # the unit of leng, elstp and linestp is micron
    print ("====@@ call klinegrid", direc, leng, elstp, nlines, linestp, expt)
    if direc == 'h':
        gapmot = nnz
    elif direc == 'v':
        gapmot = nny

    assert gapmot in (nny, nnz)

    gapmot_start = gapmot.position

    # to make linestp have same unit as leng and elstp
    linestp /= 1000
    try:
        for j in range(nlines):
            gapmot_curr = gapmot_start + j*linestp
            print("GAP direc:", direc, "  el:", j, "  gapmot_curr:", gapmot_curr)
            mv(gapmot,gapmot_curr)
            kline(direc, leng, elstp=elstp, expt=expt, frames_per_file=2000)
    finally:
        mv(gapmot,gapmot_start)



def kpatch(el=-150,eln=150,vl=150,vln=150,expt=0.05, frames_per_file=2000):
    print ("====@@ call kpatch", expt)
    #sleep(2)
    #print('kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80, expt)')
    #kmap.dkmap(nnp2,0,el,eln,nnp3,0,vl,vln, expt)
    dkmap(nnp2,0,el,eln,nnp3,0,vl,vln, expt, frames_per_file=2000)

def kpatchmatrix(h,v, hstp=151, vstp=151, expt=0.05, frames_per_file=2000):
    # h,v is number of patches at equator and vertical direction
    print ("====@@ call kpatchmatrix", h, v, hstp, vstp, expt)
    # enable the input hstp and vstp have same unit of micron
    hstp /= 1000
    vstp /= 1000
    try:
        h_pos = nny.position
        v_pos = nnz.position
        for j in range(v):
            v_curr = v_pos + j*vstp
            mv(nnz,v_curr)
            for i in range(h):
                h_curr = h_pos + i*hstp
                mv(nny,h_curr)
                # el and other inputs of kpatch using default could be revise from script
                kpatch(expt=expt, frames_per_file=2000)
    finally:
        mv(nny, h_pos)
        mv(nnz, v_pos)
        
        

def sc5009_thu_run():
    try:
      so()
      
      #gopos('F_591__0001.json')
      #newdataset('F_591_distal_0001')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,155,95,180,nnp3,65,105,80,0.2,360)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,155,95,180,nnp3,105,145,80,0.2,360)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,155,95,180,nnp3,145,185,80,0.2,360)
      
      #tgopos('F_591_proxi1_0002.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,170,80,270,nnp3,50,100,100,0.2,540)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,170,80,270,nnp3,100,150,100,0.2,540)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,170,80,270,nnp3,150,175,50,0.2,540)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,170,80,270,nnp3,175,200,50,0.2,540)
      
      tgopos('F_591_proxi2_0003.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,50,75,50,0.2,600)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,75,100,50,0.2,600)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,100,125,50,0.2,600)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,125,150,50,0.2,600)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,150,175,50,0.2,600)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,175,75,300,nnp3,175,200,50,0.2,600)
      
      sc()
      sc()
      sc()
    finally:
      sc()
      sc()
      sc()
      sc()
      
def sc5009_fri_run():
  try:
    so()

    #tgopos('gm03_opis_1_0001.json')
    #fshtrigger()
    #mgeig_x()
    #zeronnp()
    #akmap(nnp2,150,100,100,nnp3,110,140,60,0.5,300)

    #tgopos('gm03_tibia_3_0007.json')
    #fshtrigger()
    #mgeig_x()
    #zeronnp()
    #akmap(nnp2,145,105,80,nnp3,105,145,80,0.5,240)

    #tgopos('gm03_tibia_4_0008.json')
    #fshtrigger()
    #mgeig_x()
    #zeronnp()
    #akmap(nnp2,145,105,80,nnp3,100,150,100,0.5,240)

    #tgopos('gm03_blank_0000.json')
    #fshtrigger()
    #mgeig_x()
    #zeronnp()
    #akmap(nnp2,145,105,40,nnp3,105,145,40,0.5,120)
    
    tgopos('sh_1_0002.json')
    fshtrigger()
    mgeig_x()
    zeronnp()
    akmap(nnp3,100,150,100,nnp2,165,85,40,1,200)
    
    tgopos('sh_2_0003.json')
    fshtrigger()
    mgeig_x()
    zeronnp()
    akmap(nnp3,105,145,80,nnp2,135,115,10,1,160)
    
    #tgopos('sh_3_0004.json')
    #fshtrigger()
    #mgeig_x()
    #zeronnp()
    #akmap(nnp3,100,150,100,nnp2,135,115,10,1,200)
    
    tgopos('sh_4_0005.json')
    fshtrigger()
    mgeig_x()
    zeronnp()
    akmap(nnp3,105,145,80,nnp2,135,115,10,1,160)
    
    

    sc()
    sc()
    sc()
  finally:
    sc()
    sc()
    sc()
    sc()
    
def sc5009_fri_night_run():
    try:
      so()
      
      tgopos('E_592_empty_0006.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,125,105,20,nnp3,95,115,20,0.4,400)
      
      tgopos('E_592_empty_oct_0007.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,125,105,20,nnp3,95,115,20,0.4,400)
      
      tgopos('E_592_CS_tenc_0000.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,155,75,80,nnp3,65,145,80,0.3,400)
      
      tgopos('E_592_LP_tenc1_0001.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,85,125,40,0.4,400)
      
      tgopos('E_592_LP_tenc2_0002.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,85,125,40,0.4,400)
      
      tgopos('E_592_LP_tenl1_0003.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,85,125,40,0.4,400)
      
      tgopos('E_592_LP_tenl2_0004.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,85,125,40,0.4,400)
      
      
      tgopos('E_592_LP_corl_0005.json')
      
      umvr(nnz,-0.2)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,5,55,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,55,105,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,105,155,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,155,205,50,0.2,400)
      
      umvr(nnz,0.2)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,5,55,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,55,105,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,105,155,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,155,205,50,0.2,400)
      
      umvr(nnz,0.2)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,5,55,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,55,105,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,105,155,50,0.2,400)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,15,200,nnp3,155,205,50,0.2,400)

      sc()
      sc()
      sc()
    finally:
      sc()
      sc()
      sc()
      sc()


def sc5009_sat_run():
    try:
      so()
      
      tgopos('L_L1_distal_0000.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,90,160,35,nnp2,150,100,50,1,350)
      
      tgopos('L_L2_distal_0005.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,90,160,35,nnp2,150,100,50,1,350)
      
      tgopos('L_L1_proxi_0001.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,100,150,25,nnp2,175,75,100,1,250)
      
      tgopos('L_L2_proxi_0004.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,100,150,25,nnp2,175,75,100,1,250)
      
      tgopos('L_LP1_0007.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,100,150,50,nnp2,150,100,50,0.5,250)
      
      tgopos('L_LP2_0008.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,100,150,50,nnp2,150,100,50,0.5,250)
      
      tgopos('L_blank_0003.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,115,135,20,nnp2,115,135,20,0.5)
      
      sc()
      sc()
      sc()
    finally:
      sc()
      sc()
      sc()
      sc()


def sc5009_sat_aft_run():
    try:
      so()
      
      tgopos('L_LP_depro_0010.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,110,140,30,nnp2,140,110,30,0.5,300)
      
      tgopos('L_S_tendon_intact_0011.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,100,150,50,nnp2,150,100,50,0.5,200)
      
      tgopos('L_glue_0012.json')
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp3,120,130,10,nnp2,130,120,10,0.5,100)
      
      sc()
      sc()
      sc()
    finally:
      sc()
      sc()
      sc()
      sc()
      
def sc5009_sat_night_run():
    try:
      so()
      
      #tgopos('LP_C_4_cross1_0002.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,25,75,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,75,125,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,125,175,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,175,225,50,0.3,320)
      
      # start of repeat1
      #tgopos('LP_C_4_cross2_0003.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      # vacuum problem during this scan in first run
      #akmap(nnp2,205,45,160,nnp3,25,75,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,75,125,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,125,175,50,0.3,320)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,205,45,160,nnp3,175,225,50,0.3,320)
      
      #tgopos('LP_C_4_long1_0004.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,195,55,140,nnp3,25,75,50,0.6,280)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,195,55,140,nnp3,75,125,50,0.6,280)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,195,55,140,nnp3,125,175,50,0.6,280)
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,195,55,140,nnp3,175,225,50,0.6,280)
      
      # repeat3 with only 0.2sec exptime
      tgopos('LP_C_4_long2_0005.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,215,35,180,nnp3,45,85,40,0.6,360)
      # scan failed after this scan with 0.6s exposure time
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,35,180,nnp3,85,125,40,0.2,360)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,35,180,nnp3,125,165,40,0.2,360)
      fshtrigger()
      mgeig_x()
      zeronnp()
      akmap(nnp2,215,35,180,nnp3,165,205,40,0.2,360)
      
      # repeat2 (half exposure time as originally)
      #tgopos('LP_C_4_pigment2_0007.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,165,85,80,nnp3,85,165,80,.5,320)


      sc()
      sc()
      sc()
    finally:
      newdataset()
      sc()
      sc()
      sc()
      sc()
            
def sc5009_run():
    print('macro runned')
    try:
      so()
      #tgopos('B_c_w10_s01_p_0020.json')
      #sc5008_kpatchmatrix(2,2, expt=the_expt)

      #tgopos('B_c_w10_s02_p_0021.json')
      #sc5008_kpatchmatrix(2,2, expt=the_expt)
      
      #tgopos('D_590_cone_in_cone_0000.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #dmeshlog(nnp2,45,-45,300,nnp3,-35,35,140,0.3)

      #tgopos('D_590_center_0002.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #dmeshlog(nnp2,45,-45,300,nnp3,-35,35,140,0.3)    
      
      #tgopos('D_590_tip_0001.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #dmeshlog(nnp2,45,-45,300,nnp3,-35,35,140,0.3)
      
      #tgopos('ChBP_A2_0001.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_A3_0000.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_B1_0003.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_B2_0004.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_B3_0005.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_C1_0008.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_C2_0007.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_C3_0006.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      #tgopos('ChBP_empty_0009.json')
      #fshtrigger()
      #mgeig_x()
      #zeronnp()
      #akmap(nnp2,145,105,20,nnp3,105,145,20,0.5)
      
      sc()
      sc()
      sc()
    finally:
      sc()
      sc()
      sc()
      sc()
     


'''
#ChBP_A2_0001.json
#ChBP_A3_0000.json
#ChBP_B1_0003.json
#ChBP_B2_0004.json
#ChBP_B3_0005.json
#ChBP_C1_0008.json
#ChBP_C2_0007.json
#ChBP_C3_0006.json

'''
# the accurate parameter should be available after beam aligned
#def sample_moveout():
#    #nfluobx=-9. nbx = 8.
#    #nfluoax=-32.6225 nfluoay=-81.
#    umv(ndetx,400)
#    umv(nbsx,10)
#    umv(nfluoby,150)
#    umv(nbsy,-100)
    
#def sample_movein():
#    umv(nbsy,0)
#    umv(nfluoby,0)
#    umv(nbsx,8)
#    umv(ndex,-300)
