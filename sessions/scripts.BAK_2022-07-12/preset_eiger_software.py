import time
from bliss.scanning.chain import ChainPreset


def eiger_software_scan(nb_frames, expo_time, run=True):
    ##loop = loopscan(1, nb_frames * expo_time * 1.3, eiger, run=False)
    set_no_data()
    loop = loopscan(1, 1, eiger, run=False)
    esp = EigerSoftwarePreset()
    esp.set_params(nb_frames, expo_time)
    print(esp)
    loop.acq_chain.add_preset(esp)

    if run:
        loop.run()

    return loop

def set_no_data():
    print(SCAN_SAVING.images_prefix)
    SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}"
    print(SCAN_SAVING.images_prefix)
    


class EigerSoftwarePreset(ChainPreset):
    _nb_frames = 15000
    _expo_time = 0.0014

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._ufsh = config.get("ufsh")
        self._umux = config.get("umux")
        self._eiger = config.get("eiger")
        self._uopiom2 = config.get("uopiom2")

    def __repr__(self):
        return f"<{self.__class__.__name__} nb_frames={self._nb_frames} expo_time={self._expo_time} guard={self._guard}>"

    def set_params(self, nb_frames, expo_time, guard=0.1):
        """Set parameters for preset"""
        self._nb_frames = nb_frames
        self._expo_time = expo_time
        self._guard = guard

    def prepare(self, acq_chain):
        print(SCAN_SAVING.images_prefix)
        print(self.__class__.__name__, "Prepare")
        # Put shutter in external
        self._ufsh.mode = self._ufsh.EXTERNAL
        self._umux.switch("FS_ENABLE", "ENABLE")
        
        # use single trigger from loop scan, not USER ttl
        self._umux.switch("FSH_MODE", "EXT_TRIG")
        self._umux.switch("TRIG_MODE", "EXT_TRIG")


    def start(self, acq_chain):
        # Configure opiom2 to gate fast shutter
        #self.configure_gate()
        
        # Put eiger in external single trigger
        # Have to use start so it overwrites bliss settings
        self._eiger._proxy.acq_trigger_mode = "EXTERNAL_TRIGGER"
        self._eiger._proxy.acq_expo_time = self._expo_time
        self._eiger._proxy.acq_nb_frames = self._nb_frames
        #self._eiger._proxy.saving_format = 'HDF5BS'
        self._eiger._proxy.saving_managed_mode = 'HARDWARE'
        self._eiger._proxy.saving_format = 'HDF5'
        self._eiger._proxy.video_active = False
        self._eiger.stopAcq()
        self._eiger.prepareAcq()
        print(SCAN_SAVING.images_prefix)

        print(self.__class__.__name__, "Waiting for user trigger...")

    def stop(self, acq_chain):
        print(self.__class__.__name__, "Finished")
        # Close shutter
        #self._ufsh.mode = self._ufsh.MANUAL
        self._ufsh.mode = self._ufsh.EXTERNAL
        self._ufsh.close()

        # Put trig_mode back
        self._umux.switch("TRIG_MODE", "P201")
        self._umux.switch("FSH_MODE", "P201")

        # Put trigger mode back
        self._eiger._proxy.acq_trigger_mode = "EXTERNAL_TRIGGER_MULTI"
        self._eiger._proxy.saving_managed_mode = 'SOFTWARE'
        
        print(SCAN_SAVING.images_prefix)
        SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"
        print(SCAN_SAVING.images_prefix)

    def configure_gate(self):
        """Configure fast shutter gate
        Use opiom2 to generate a gate signal for fast shutter from trigger
        This will overwrite the Preset_opiom2 configuration
        """
        freq = 8
        tick = 1 / 8 * 1e6

        ch = 1
        inchannel = 5
        ticks_width = int((self._expo_time * self._nb_frames + self._guard) / tick)
        ticks_delay = 1

        cmd = "CNT %d CLK%d RISE D32 START RISE I%d PULSE %d %d 1" % (
            ch,
            freq,
            inchannel,
            ticks_width,
            ticks_delay,
        )
        print("Eiger gate cmd", cmd)
        self._uopiom2.comm(cmd)

    def send_rising(self):
        """Trigger User Input
        Use O8 opiom2 to send rising TTL to IN4 of opiom1 to
        simulate user trigger
        """
        self._uopiom2.comm("IMA 0x80")
        val = self._uopiom2.comm("?IMA")
        assert val == "0x80"

        time.sleep(0.01)

        self._uopiom2.comm("IMA 0x0")
        val = self._uopiom2.comm("?IMA")
        assert val == "0x00"

