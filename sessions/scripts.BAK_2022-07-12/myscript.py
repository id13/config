def mycommands():
    
    umv(udetx,-160)
    
    rstp('rubber_up_0001.json')  
    newdataset('rubber_up_0001_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.02,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_up_0002.json')  
    newdataset('rubber_up_0002_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0001.json')  
    newdataset('rubber_mid_0001_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0002.json')  
    newdataset('rubber_mid_0002_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.10,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0001.json')  
    newdataset('rubber_down_0001_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.1,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0002.json')  
    newdataset('rubber_down_0002_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.2,frames_per_file=300,retveloc=5.0)
    
    
    rstp('rubber_up_0003.json')  
    newdataset('rubber_up_0003_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.02,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_up_0004.json')  
    newdataset('rubber_up_0004_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0003.json')  
    newdataset('rubber_mid_0003_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0004.json')  
    newdataset('rubber_mid_0004_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.1,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0003.json')  
    newdataset('rubber_down_0003_saxs')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.1,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0004.json')  
    newdataset('rubber_down_0004_saxs')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.20,frames_per_file=300,retveloc=5.0)
    
    umv(udetx,-660)
    
    rstp('rubber_up_0003.json')  
    newdataset('rubber_up_0003')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.02,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_up_0004.json')  
    newdataset('rubber_up_0004')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0003.json')  
    newdataset('rubber_mid_0003')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.02,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_mid_0004.json')  
    newdataset('rubber_mid_0004')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0003.json')  
    newdataset('rubber_down_0003')   
    dkmapyz_2(-0.15,0.15,150,-0.15,0.15,150,0.05,frames_per_file=300,retveloc=5.0)
    
    rstp('rubber_down_0004.json')  
    newdataset('rubber_down_0004')   
    dkmapyz_2(-0.15,0.15,75,-0.15,0.15,75,0.10,frames_per_file=300,retveloc=5.0)
    
    sc()
    sc()
    sc()
    sc()
    
print ('loaded')

