print('sc5189_fancy load 5')
START_KEY = 'a'

TEST_LIST = '''
cc267_5x_roi1_0064.json 150 240 0.5 0.005
cc267_5x_roi2_0065.json 200 220 1.0 0.010
cc267_5x_roi3_0066.json 100 200 2.0 0.02
'''

REAL_LIST = '''
cc267_5x_roi4_0067.json
cc267_5x_roi5_0068.json
cc267_5x_roi6_0069.json
'''

def make_posdc(poslst):
    posll = poslst.split('\n')
    dc = dict()
    for l in posll:
        #print (f'l = {l}')
        l = l.strip()
        if not l:
            continue
        w1 = l.split()
        w2 = w1[0].split('_')
        for e in w2:
            #print (f'e = {e}')
            if e.startswith('roi'):
                dc[e] = l
    return dc


def dooone(psdc, roi):
    dsname = roi
    posname, hw, vw, stp, expt = psdc[roi].split()
    hw, vw, stp, expt = map(float, (hw, vw, stp, expt))
    hll = -0.5*hw
    hul = 0.5*hw
    hitv = int(hw/stp)
    vll = -0.5*vw
    vul = 0.5*vw
    vitv = int(vw/stp)
    print(f"newdatset('{dsname}')")
    print(f"gopos('{posname}')")
    print(f'dkmapyz_2({hll}, {hul}, {hitv}, {vll}, {vul}, {vitv}, {expt})')
    #newdatset(dsname)
    #gopos(posname)
    #dkmapyz_2(hll, hul, hitv, vll, vul, vitv, expt)


def sc5189_fancy():
    try:
        posdc = make_posdc(TEST_LIST)
        #so()
        #mgeig()
        dooone(posdc, 'roi1')
        dooone(posdc, 'roi2')
        dooone(posdc, 'roi3')

    finally:
        pass
        #sc()
        #sc()
        #sc()
