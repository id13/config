def hello_world():
    print ("Hello world (msmb)")
    return 

def set_lm(mot, lolim=None, hilim=None): 
# needed: something like ismotor(mot)
# eg: assert ismotor(mot) == True
    l,h = mot.limits 
    if None is lolim: 
        sl = l 
    else: 
        sl = float(lolim) 
    if None is hilim: 
        sh = h 
    else: 
        sh = float(hilim) 
    fsl = min(sl, sh)
    fsh = max(sl, sh)
    mot.limits = (sl, sh) 
    mot.settings_to_config(velocity=False, acceleration=False, limits=True) 
    return
