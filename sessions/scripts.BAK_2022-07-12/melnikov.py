# ihsc1687 29/01/22-30/01/22 eh3
# pcl bottlebrush from IDA

def do_meshes():
    fshtrigger()
    rstp('1150_1_50x_1')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)
    
    newdataset('1150_2')
    fshtrigger()
    rstp('1150_1_50x_2')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1150_3')
    fshtrigger()
    rstp('1150_1_50x_3')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_2')
    fshtrigger()
    rstp('1350_2_50x_1')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_2')
    fshtrigger()
    rstp('1350_2_50x_12')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_2')
    fshtrigger()
    rstp('1350_2_50x_3')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_4')
    fshtrigger()
    rstp('1350_4_50x_1')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_4')
    fshtrigger()
    rstp('1350_4_50x_2')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    newdataset('1350_4')
    fshtrigger()
    rstp('1350_4_50x_3')
    kmap.dkmap(nnp2, -60, 60, 240, nnp3, -60, 60, 240, 0.05)

    sc()
    sc()
