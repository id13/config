print('hello virginie, version ev480_pillar1_ttomo_004 load 1')
import numpy as np, math
import time
import traceback

class Bailout(Exception): pass

ORI_INSTRUCT = """
# kappa,omega
0, 0.0
0, 2.175226586102719
0, 4.350453172205438
0, 6.525679758308158
0, 8.700906344410877
0, 10.876132930513595
0, 13.051359516616316
0, 15.226586102719034
0, 17.401812688821753
0, 19.577039274924473
0, 21.75226586102719
0, 23.92749244712991
0, 26.10271903323263
0, 28.27794561933535
0, 30.45317220543807
0, 32.62839879154079
0, 34.803625377643506
0, 36.97885196374622
0, 39.15407854984895
0, 41.329305135951664
0, 43.50453172205438
0, 45.679758308157105
0, 47.85498489425982
0, 50.03021148036254
0, 52.20543806646526
0, 54.38066465256798
0, 56.5558912386707
0, 58.73111782477341
0, 60.90634441087614
0, 63.081570996978854
0, 65.25679758308158
0, 67.43202416918429
0, 69.60725075528701
0, 71.78247734138974
0, 73.95770392749245
0, 76.13293051359517
0, 78.3081570996979
0, 80.4833836858006
0, 82.65861027190333
0, 84.83383685800605
0, 87.00906344410876
0, 89.18429003021149
0, 91.35951661631421
0, 93.53474320241692
0, 95.70996978851964
0, 97.88519637462237
0, 100.06042296072508
0, 102.2356495468278
0, 104.41087613293053
0, 106.58610271903324
0, 108.76132930513596
0, 110.93655589123867
0, 113.1117824773414
0, 115.28700906344412
0, 117.46223564954683
0, 119.63746223564955
0, 121.81268882175227
0, 123.98791540785498
0, 126.16314199395771
0, 128.33836858006043
0, 130.51359516616316
0, 132.68882175226588
0, 134.86404833836858
0, 137.0392749244713
0, 139.21450151057402
0, 141.38972809667675
0, 143.56495468277947
0, 145.74018126888217
0, 147.9154078549849
0, 150.09063444108762
0, 152.26586102719034
0, 154.44108761329306
0, 156.6163141993958
0, 158.79154078549848
0, 160.9667673716012
0, 163.14199395770393
0, 165.31722054380666
0, 167.49244712990938
0, 169.6676737160121
0, 171.8429003021148
0, 174.01812688821752
0, 176.19335347432025
0, 178.36858006042297
0, 180
0, 178.91238670694864
0, 176.73716012084594
0, 174.56193353474322
0, 172.3867069486405
0, 170.21148036253777
0, 168.03625377643505
0, 165.86102719033232
0, 163.68580060422963
0, 161.5105740181269
0, 159.33534743202418
0, 157.16012084592145
0, 154.98489425981873
0, 152.809667673716
0, 150.6344410876133
0, 148.4592145015106
0, 146.28398791540786
0, 144.10876132930514
0, 141.93353474320242
0, 139.7583081570997
0, 137.583081570997
0, 135.40785498489427
0, 133.23262839879155
0, 131.05740181268882
0, 128.8821752265861
0, 126.70694864048339
0, 124.53172205438067
0, 122.35649546827796
0, 120.18126888217523
0, 118.00604229607251
0, 115.8308157099698
0, 113.65558912386707
0, 111.48036253776435
0, 109.30513595166164
0, 107.12990936555892
0, 104.95468277945619
0, 102.77945619335348
0, 100.60422960725076
0, 98.42900302114803
0, 96.25377643504532
0, 94.0785498489426
0, 91.90332326283988
0, 89.72809667673717
0, 87.55287009063444
0, 85.37764350453173
0, 83.20241691842901
0, 81.02719033232628
0, 78.85196374622357
0, 76.67673716012085
0, 74.50151057401813
0, 72.32628398791542
0, 70.1510574018127
0, 67.97583081570997
0, 65.80060422960726
0, 63.625377643504535
0, 61.45015105740182
0, 59.274924471299094
0, 57.09969788519638
0, 54.92447129909366
0, 52.74924471299094
0, 50.57401812688822
0, 48.3987915407855
0, 46.22356495468278
0, 44.04833836858006
0, 41.873111782477345
0, 39.69788519637462
0, 37.522658610271904
0, 35.34743202416919
0, 33.17220543806647
0, 30.996978851963746
0, 28.82175226586103
0, 26.64652567975831
0, 24.471299093655592
0, 22.29607250755287
0, 20.12084592145015
0, 17.945619335347434
0, 15.770392749244714
0, 13.595166163141995
0, 11.419939577039276
0, 9.244712990936556
0, 7.069486404833837
0, 4.894259818731118
0, 2.719033232628399
0, 0.5438066465256798
0, 0
0, 1.0876132930513596
0, 3.262839879154079
0, 5.438066465256798
0, 7.613293051359517
0, 9.788519637462237
0, 11.963746223564955
0, 14.138972809667674
0, 16.314199395770395
0, 18.48942598187311
0, 20.664652567975832
0, 22.839879154078552
0, 25.01510574018127
0, 27.19033232628399
0, 29.365558912386707
0, 31.540785498489427
0, 33.716012084592144
0, 35.89123867069487
0, 38.066465256797585
0, 40.2416918429003
0, 42.416918429003026
0, 44.59214501510574
0, 46.76737160120846
0, 48.942598187311184
0, 51.1178247734139
0, 53.29305135951662
0, 55.468277945619334
0, 57.64350453172206
0, 59.818731117824775
0, 61.99395770392749
0, 64.16918429003022
0, 66.34441087613294
0, 68.51963746223565
0, 70.69486404833837
0, 72.87009063444108
0, 75.04531722054381
0, 77.22054380664653
0, 79.39577039274924
0, 81.57099697885197
0, 83.74622356495469
0, 85.9214501510574
0, 88.09667673716012
0, 90.27190332326285
0, 92.44712990936556
0, 94.62235649546828
0, 96.797583081571
0, 98.97280966767372
0, 101.14803625377644
0, 103.32326283987916
0, 105.49848942598187
0, 107.6737160120846
0, 109.84894259818732
0, 112.02416918429003
0, 114.19939577039275
0, 116.37462235649548
0, 118.54984894259819
0, 120.72507552870091
0, 122.90030211480364
0, 125.07552870090635
0, 127.25075528700907
0, 129.4259818731118
0, 131.60120845921452
0, 133.7764350453172
0, 135.95166163141994
0, 138.12688821752266
0, 140.3021148036254
0, 142.4773413897281
0, 144.65256797583083
0, 146.82779456193353
0, 149.00302114803625
0, 151.17824773413898
0, 153.3534743202417
0, 155.52870090634443
0, 157.70392749244715
0, 159.87915407854985
0, 162.05438066465257
0, 164.2296072507553
0, 166.40483383685802
0, 168.58006042296074
0, 170.75528700906347
0, 172.93051359516616
0, 175.10574018126889
0, 177.2809667673716
0, 179.45619335347433
0, 180
0, 177.82477341389728
0, 175.64954682779458
0, 173.47432024169186
0, 171.29909365558913
0, 169.1238670694864
0, 166.94864048338368
0, 164.77341389728096
0, 162.59818731117826
0, 160.42296072507554
0, 158.24773413897282
0, 156.0725075528701
0, 153.89728096676737
0, 151.72205438066467
0, 149.54682779456195
0, 147.37160120845923
0, 145.1963746223565
0, 143.02114803625378
0, 140.84592145015105
0, 138.67069486404836
0, 136.49546827794563
0, 134.3202416918429
0, 132.14501510574019
0, 129.96978851963746
0, 127.79456193353475
0, 125.61933534743203
0, 123.4441087613293
0, 121.2688821752266
0, 119.09365558912387
0, 116.91842900302116
0, 114.74320241691844
0, 112.56797583081571
0, 110.392749244713
0, 108.21752265861028
0, 106.04229607250755
0, 103.86706948640484
0, 101.69184290030212
0, 99.5166163141994
0, 97.34138972809669
0, 95.16616314199396
0, 92.99093655589124
0, 90.81570996978853
0, 88.6404833836858
0, 86.46525679758308
0, 84.29003021148037
0, 82.11480362537765
0, 79.93957703927492
0, 77.76435045317221
0, 75.58912386706949
0, 73.41389728096676
0, 71.23867069486406
0, 69.06344410876133
0, 66.8882175226586
0, 64.7129909365559
0, 62.53776435045317
0, 60.362537764350456
0, 58.18731117824774
0, 56.012084592145015
0, 53.8368580060423
0, 51.66163141993958
0, 49.48640483383686
0, 47.31117824773414
0, 45.135951661631424
0, 42.9607250755287
0, 40.78549848942598
0, 38.610271903323266
0, 36.43504531722054
0, 34.259818731117825
0, 32.08459214501511
0, 29.909365558912388
0, 27.734138972809667
0, 25.55891238670695
0, 23.38368580060423
0, 21.208459214501513
0, 19.033232628398792
0, 16.858006042296072
0, 14.682779456193353
0, 12.507552870090635
0, 10.332326283987916
0, 8.157099697885197
0, 5.981873111782478
0, 3.8066465256797586
0, 1.6314199395770395
0, 0
"""

TEST_ORI_INSTRUCT = """
# kappa,omega
0, 0.0
0, 3.444976076555024
0, 6.889952153110048
0, 10.334928229665072
0, 13.779904306220097
0, 17.22488038277512
0, 20.669856459330145
0, 24.11483253588517
0, 27.559808612440193
"""

def generate_instruct_string(start_ome, stop_ome, nproj, nstrands):
    eps = 0.0001
    omegall = list(np.linspace(start_ome,stop_ome,nproj))
    #print(omegall)
    llll = []
    for i in range(nstrands):
        sl = omegall[i::nstrands]
        if i % 2:
            sl.reverse()
        llll.append(sl)

    #print(f'llll = {llll}')
    for i in range(nstrands):
        if i % 2:
            if math.fabs(llll[i][-1] - start_ome) < eps:
                pass
            else:
                llll[i].append(start_ome)
        else:
            if math.fabs(llll[i][-1] - stop_ome) < eps:
                pass
            else:
                llll[i].append(stop_ome)
    full = []
    for ll in llll:
        full.extend(ll)
    full_indexed = []
    for tp in enumerate(full):
        i,ome = tp
        full_indexed.append(tp)
    sll = []
    sll.append(f'')
    sll.append(f'# kappa,omega')
    sent = -1
    for tp in full_indexed:
        i, ome = tp
        #print(sent, tp)
        if sent == ome:
            pass
        else:
            sll.append(f'0, {ome}')
        sent = tp[1]
    sll.append(f'')
    s = '\n'.join(sll)
    return s
        


def make_instruct_list(s):
    ll = s.split('\n')
    instll = []
    for l in ll:
        l = l.strip()
        if l.startswith('#'):
            print (l)
        elif not l:
            pass
        else:
            (kap,ome) = l.split(',')
            ko = (int(kap), float(ome))
            instll.append(ko)
    instll = list(enumerate(instll))
    return instll


def dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=None):
    kmap.dkmap(nnp2,lly,uly,nitvy,nnp3,llz,ulz,nitvz,expt)

class TTomo(object):

    def __init__(self, asyfn, zkap, instll, scanparams, stepwidth=3, logfn='ttomo_eh3.log'):
        self.asyfn = asyfn
        self.logfn = logfn
        self.zkap = zkap
        self.instll = instll
        self.scanparams = scanparams
        self.stepwidth=stepwidth
        self._id = 0

    def read_async_inp(self):
        instruct = []
        with open(self.asyfn, 'r') as f:
            s = f.read()
        ll = s.split('\n')
        ll = [l.strip() for l in ll]
        for l in ll:
            print(f'[{l}]')
            if '=' in l:
                a,v = l.split('=',1)
                (action, value) = a.strip(), v.strip()
                instruct.append((action, value))
        self.log(s)
        return instruct

    def doo_projection(self, instll_item):
        self.log(instll_item)
        (i,(kap,ome)) = instll_item
        self.log('... dummy ko trajectory')
        #self.zkap.trajectory_goto(ome, kap, stp=4)
        self.zkap.goto_okpos(ome)
        self.perform_scan()


    def perform_scan(self): 
        sp = self.scanparams
        sp_t = (lly,uly,nitvy,llz,ulz,nitvz,expt,retveloc) = (
            sp['lly'],
            sp['uly'],
            sp['nitvy'],
            sp['llz'],
            sp['ulz'],
            sp['nitvz'],
            sp['expt'],
            sp['retveloc']
        )
        lly *= 1.000
        uly *= 1.000
        llz *= 1.000
        ulz *= 1.000
        cmd = f'dk..({lly},{uly},{nitvy},{llz},{ulz},{nitvz},{expt}, retveloc={retveloc})'
        self.log(cmd)
        dkmapyz_2(lly,uly,nitvy,llz,ulz,nitvz,expt, retveloc=retveloc)
        time.sleep(5)

    def oo_correct(self, c_corrx, c_corry, c_corrz):
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def fov_correct(self,lly,uly,llz,ulz):
        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz

    def to_grid(self, lx):
        lx = int(lx)
        (n,f) = divmod(lx, self.stepwidth)

    def mainloop(self, inst_idx=0):
        instll = list(self.instll)
        indexed_instll = list(enumerate(instll))
        indexed_instll.reverse()
        while(True):
            full_item = indexed_instll.pop()
            cycle, instll_item = full_item
            print(f'\n\n\n\n\n\n\n ============================================ CYCLE {cycle}')
            print(f'KAPPA = {instll_item[0]}  OMEGA = {instll_item[1]}')
            instruct = self.read_async_inp()
            self.process_instructions(instruct)
            self.doo_projection(instll_item)

    def log(self, s):
        s = str(s)
        with open(self.logfn, 'a') as f:
            msg = f'\nCOM ID: {self._id} | TIME: {time.time()} | DATE: {time.asctime()} | ===============================\n'
            print(msg)
            f.write(msg)
            print(s)
            f.write(s)

    def process_instructions(self, instruct):
        print('++++++++++++++++++++++')
        print(f'instruct {instruct}')
        print('++++++++++++++++++++++')
        a , v = instruct[0]
        print('++++++++++++++++++++++')
        print(f'processing instruction: {a} {v}')
        if 'id' == a:
            theid = int(v)
            if theid > self._id:
                self._id = theid
                msg = f'new instruction set found - processing id= {theid} ...'
                self.log(msg)
            else:
                self.log('only old instruction set found - continuing ...')
                return
        else:
            self.log('missing instruction set id - continuing ...')
            return

        for a,v in instruct:
            if 'end' == a:
                return
            elif 'stop' == a:
                print('bailing out ...')
                raise Bailout()

            elif 'tweak' == a:
                try:
                    self.log(f'dummy tweak: found {v}')
                    w = v.split()
                    mode = w[0]
                    if 'fov' == mode:
                        fov_t_bds = (lly, uly, llz, ulz) = tuple(map(int,w[1:5]))
                        fov_t_stps = (stpy, stpz) = tuple(map(float,w[5:]))
                        fov_t = fov_t_bds + fov_t_stps
                        self.adapt_fov_scanparams(fov_t)
                    elif 'cor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_corr(c_corr_t)
                    elif 'nnpcor' == mode:
                        c_corr_t = (c_corrx, c_corry, c_corrz) = tuple(map(float, w[1:]))
                        self.adapt_c_nnpcorr(c_corr_t)
                    else:
                        raise ValueError(v)
                except:
                    self.log(f'error processing: {v}\n{traceback.format_exc()}')
                        
            else:
                print(f'WARNING: instruction {a} ignored')

    def adapt_c_corr(self, c_corr_t):
        (c_corrx, c_corry, c_corrz) = c_corr_t
        self.zkap.c_corrx = c_corrx
        self.zkap.c_corry = c_corry
        self.zkap.c_corrz = c_corrz

    def adapt_c_nnpcorr(self, c_corr_t):
        print('applying nnp correction! ======================================== NNP!')
        print('pos before:')
        wnnp()
        (c_corrx, c_corry, c_corrz) = c_corr_t
        x = nnp1.position + c_corrx
        y = nnp2.position + c_corry
        z = nnp3.position + c_corrz
        mv(nnp1, x)
        mv(nnp2, y)
        mv(nnp3, z)
        print('pos after:')
        wnnp()
        self.log(f'nnpcorrection: {c_corr_t}')

    def adapt_fov_scanparams(self, fov_t):
        (lly, uly, llz, ulz, stpy, stpz) = fov_t

        lly = stpy*int(lly/stpy)
        uly = stpy*int(uly/stpy)
        llz = stpz*int(llz/stpz)
        ulz = stpz*int(ulz/stpz)

        dy = uly - lly
        dz = ulz - llz

        nitvy = int(dy/stpy)
        nitvz = int(dz/stpz)

        print('d', dy, dz)
        print('st',stpy,stpz)
        print('ni',nitvy,nitvz)
        print('ll',lly, uly, llz, ulz)

        sp = self.scanparams
        sp['lly'] = lly
        sp['uly'] = uly
        sp['llz'] = llz
        sp['ulz'] = ulz
        sp['nitvy'] = nitvy
        sp['nitvz'] = nitvz

def ev480_test_main(zkap):
    print('Hi!')

    # verify zkap
    print(zkap)

    # read table
    instll = make_instruct_list(ORI_INSTRUCT)
    print(instll)
    
    # setup params
    # 117
    # 170
    scanparams = dict(
        lly = -16,
        uly = 16,
        nitvy = 320,
        llz = -10,
        ulz = 10,
        nitvz = 200,
        expt = 0.002,
        retveloc = 0.0
    )
    ttm = TTomo( 'asy.com', zkap, instll, scanparams)



    # loop over projections
    ttm.mainloop()
