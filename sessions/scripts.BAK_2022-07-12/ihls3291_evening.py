LOAD = 'a'
print (f'ihls3291_evening_main: load {LOAD}')
def ihls3291_evening_main():

    gopos('cap_1_drop_02_0002.json')
    mvr(ustry,-0.3,ustrz,-0.3)
    newdataset(f'drop_02_{LOAD}')
    dkpatchmesh(0.6,120,0.8,40,0.005,1,1, retveloc=5.0)

    gopos('cap_1_drop_03_0003.json')
    mvr(ustry,-0.3,ustrz,-0.3)
    newdataset(f'drop_03_{LOAD}')
    dkpatchmesh(0.6,120,0.8,40,0.005,1,1, retveloc=5.0)

    gopos('cap_1_drop_04_0006.json')
    mvr(ustry,-0.3,ustrz,-0.3)
    newdataset(f'drop_04_{LOAD}')
    dkpatchmesh(0.6,120,0.8,40,0.005,1,1, retveloc=5.0)

    gopos('cap_1_drop_05_0007.json')
    mvr(ustry,-0.3,ustrz,-0.3)
    newdataset(f'drop_05_{LOAD}')
    dkpatchmesh(0.6,120,0.8,40,0.005,1,1, retveloc=5.0)

    gopos('cap_1_parafilm_01_0008.json')
    mvr(ustry,-0.3,ustrz,-0.3)
    newdataset(f'parafilm_{LOAD}')
    dkpatchmesh(0.6,120,0.8,40,0.005,1,1, retveloc=5.0)

