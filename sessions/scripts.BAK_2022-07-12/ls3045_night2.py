print('ls3045 night2 load: 2')

LAUNCH = 'c'

def make_dsname(posname, remov):
    if posname.endswith('.json'):
        pn = posname[:-5]
    elif posname.endswith('._pos'):
        pn = posname[:-5]
    else:
        pn = posname

    dsname = f'ds_{LAUNCH}_{pn}'
    dsname = dsname.replace(remov,'')
    return dsname

def do_one(posname, remov):
    dsname = make_dsname(posname, remov)
    newdataset(dsname)
    gopos(posname)
    kmap.dkmap(nnp2, 100 , 0    , 100   , nnp3, -100, 0   , 100, 0.02)
    kmap.dkmap(nnp2, 0   , -100 , 100   , nnp3, -100, 0   , 100, 0.02)
    kmap.dkmap(nnp2, 100 , 0    , 100   , nnp3, 0   , 100 , 100, 0.02)
    kmap.dkmap(nnp2, 0   , -100 , 100   , nnp3, 0   , 100 , 100, 0.02)
    enddataset()

def ls3045_night2_main():
    poslist = """
zf_72_1_l_cen_pos_1_0002.json
zf_72_1_l_cen_pos_2_0003.json
zf_72_1_l_cen_pos_3_0004.json
zf_72_1_l_cen_pos_4_0005.json
    """.split()
    print (poslist)
    for pn in poslist:
        do_one(pn, 'zf_72_1_') # to be changed

