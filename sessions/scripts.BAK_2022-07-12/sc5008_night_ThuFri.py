from bliss.setup_globals import *

def sc5008_klineelem(direc, stp=0.25, hleng=100.0, expt=0.05):
    print ("====@@ call sc5008_klineelem", direc, stp, hleng, expt)
    npts = int(2*(hleng/stp))
    if direc == 'h':
        print(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt)
        kmap.dkmap(nnp2,hleng,-hleng,npts,nnp3,0,0,1,expt) 
    elif direc == 'v':
        print(nnp3,hleng,-hleng,npts,nnp2,0,0,1,expt)
        kmap.dkmap(nnp3,-hleng,hleng,npts,nnp2,0,0,1,expt)

def sc5008_kline(direc, linleng, elstp=0.25, expt=0.05):
    print ("====@@ call sc5008_kline", direc, linleng, expt)
    hlf_el = 100.0
    el = 2*hlf_el
    ne = int(linleng/el)

    if direc == 'h':
        try:
            hx_spc = 0.001*el
            hx_of = 0.5*hx_spc
            hx_start = nny.position
            for i in range(ne):
                hx_curr = hx_start + hx_of + i*hx_spc
                print("LINE direc:", direc, "  el:", i, "  hx_curr:", hx_curr)
                mv(nny,hx_curr)
                sc5008_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nny,hx_start)
    elif direc == 'v':
        try:
            vx_spc = 0.001*el
            vx_of = 0.5*vx_spc
            vx_start = nnz.position
            for i in range(ne):
                vx_curr = vx_start + vx_of + i*vx_spc
                print("LINE direc:", direc, "  el:", i, "  vx_curr:", vx_curr)
                mv(nnz,vx_curr)
                sc5008_klineelem(direc, hleng=hlf_el, stp=elstp, expt=expt)
        finally:
            mv(nnz,vx_start)
    else:
        raise ValueError

def sc5008_klinegrid(direc, leng, elstp, nlines, linestp, expt=0.05):
    print ("====@@ call sc5008_klinegrid", direc, leng, elstp, nlines, linestp, expt)
    if direc == 'h':
        gapmot = nnz
    elif direc == 'v':
        gapmot = nny

    assert gapmot in (nny, nnz)

    gapmot_start = gapmot.position
    try:
        for j in range(nlines):
            gapmot_curr = gapmot_start + j*linestp
            print("GAP direc:", direc, "  el:", j, "  gapmot_curr:", gapmot_curr)
            mv(gapmot,gapmot_curr)
            sc5008_kline(direc, leng, elstp=elstp, expt=expt)
    finally:
        mv(gapmot,gapmot_start)

def sc5008_kpatch(expt=0.05):
    print ("====@@ call sc5008_kpatch", expt)
    #sleep(2)
    #print('kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80, expt)')
    kmap.dkmap(nnp2,0,-24,80,nnp3,0,24,80, expt)

def sc5008_kpatchmatrix(h,v, hstp=0.025, vstp=0.025, expt=0.05):
    print ("====@@ call sc5008_kpatchmatrix", h, v, hstp, vstp, expt)
    try:
        h_pos = nny.position
        v_pos = nnz.position
        for j in range(v):
            v_curr = v_pos + j*vstp
            mv(nnz,v_curr)
            for i in range(h):
                h_curr = h_pos + i*hstp
                mv(nny,h_curr)
                sc5008_kpatch(expt=expt)
    finally:
        mv(nny, h_pos)
        mv(nnz, v_pos)

def tgopos(x):
    print ("====@@ call tgopos", x)
    s = x[:-5]
    s.replace('-','_')
    gopos(x)
    newdataset('s01_%s' % s)
    fshtrigger()

def sc5008_ain():

    the_expt = 0.05

    tgopos('b_c_air_0001.json')
    sc5008_kpatch(expt=the_expt)

    tgopos('B_c_w07_s14_-h_0017.json')
    mvr(nny,-0.6)
    sc5008_klinegrid('h', 600, 0.25, 3, 0.001, the_expt)

    tgopos('B_c_w07_s02_p_0004.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w07_s06_p_0008.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w07_s11_p_0014.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w07_s13_v_0016.json')
    sc5008_klinegrid('v', 600, 0.25, 3, 0.001, the_expt)

    tgopos('B_c_w07_s15_j_0018.json')
    sc5008_klinegrid('h', 400, 2.0, 80, 0.005, the_expt)

    tgopos('B_c_w07_s16_j_0019.json')
    sc5008_klinegrid('h', 400, 2.0, 80, 0.005, the_expt)

    tgopos('B_c_w10_s01_p_0020.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w10_s02_p_0021.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w10_s03_p_0022.json')
    sc5008_kpatchmatrix(2,2, expt=the_expt)

    tgopos('B_c_w10_s04_v_0023.json')
    sc5008_klinegrid('v', 1400, 0.25, 3, 0.001, the_expt)

    tgopos('B_c_w10_s05_h_0024.json')
    sc5008_klinegrid('h', 1600, 0.25, 3, 0.001, the_expt)

    tgopos('B_c_w10_s06_j_0026.json')
    sc5008_klinegrid('h', 400, 2.0, 80, 0.005, the_expt)

    tgopos('B_c_w10_s07_j_0027.json')
    sc5008_klinegrid('h', 400, 2.0, 80, 0.005, the_expt)

    tgopos('b_c_air_0001.json')
    sc5008_kpatch(expt=the_expt)




DOC = """ b_c_air_0001.json
# B_c_w07_s01_p_0003.json
B_c_w07_s02_p_0004.json
# B_c_w07_s03_p_0005.json
# B_c_w07_s04_p_0006.json
# B_c_w07_s05_p_0007.json
B_c_w07_s06_p_0008.json
# B_c_w07_s07_p_0009.json
# B_c_w07_s08_p_0010.json
# B_c_w07_s08_p_0011.json
# B_c_w07_s09_p_0012.json
# B_c_w07_s10_p_0013.json
B_c_w07_s11_p_0014.json
# B_c_w07_s12_p_0015.json
B_c_w07_s13_v_0016.json
B_c_w07_s14_-h_0017.json
B_c_w07_s15_j_0018.json
B_c_w07_s16_j_0019.json
B_c_w10_s01_p_0020.json
B_c_w10_s02_p_0021.json
B_c_w10_s03_p_0022.json
B_c_w10_s04_v_0023.json
B_c_w10_s05_h_0024.json
B_c_w10_s05_h_0025.json
B_c_w10_s06_j_0026.json
B_c_w10_s07_j_0027.json
# b_c_air2_0002.json
"""

