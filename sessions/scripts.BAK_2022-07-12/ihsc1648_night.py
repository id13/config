# there is -0.052 mm difference at y direction
print('insc1648 load 6')

def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)
    zeronnp()

def gopos_kmap_scan(pos,start_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig()
    name = f'{pos[:-5]}_{start_key}'
    umv(nnz,0,nny,0)
    gopos(pos)
    umvr(nny,-0.052)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()

clay_saxs_pos = [
'june112_night_scan_caly_cellulose_center_0013.json',
'june112_night_scan_caly_cellulose_edge_0010.json',
]
saxs_pos = [
'june112_night_scan_HT_Ion_fiber_0003.json',
'june112_night_scan_HT_Ion_top_0002.json',
'june112_night_scan_Ion_fiber_0001.json',
'june112_night_scan_Ion_top_0000.json',
'june112_night_scan_Vis_fiber_0005.json',
'june112_night_scan_Vis_top_0004.json',
'june112_night_scan_beech_cross1_0017.json',
'june112_night_scan_beech_cross_2_0020.json',
'june112_night_scan_beech_fiber1_0016.json',
'june112_night_scan_rise_cellulose_100_0006.json',
'june112_night_scan_rise_lignin_50_0009.json',
]
waxs_pos = [
'june112_night_scan_HT_Ion_fiber_waxs_0027.json',
'june112_night_scan_HT_Ion_top_waxs_0026.json',
'june112_night_scan_Ion_fiber_waxs_0025.json',
'june112_night_scan_Ion_top_waxs_0024.json',
'june112_night_scan_Vis_fiber_waxs_0023.json',
'june112_night_scan_Vis_top_waxs_0022.json',
'june112_night_scan_beech_cross1_waxs_0018.json',
'june112_night_scan_beech_cross_2_waxs_0021.json',
'june112_night_scan_beech_fiber2_waxs_0019.json',
'june112_night_scan_caly_cellulose_center_waxs_0014.json',
'june112_night_scan_caly_cellulose_edge_waxs_0011.json',
'june112_night_scan_rise_cellulose_100_waxs_0007.json',
'june112_night_scan_rise_lignin_50_waxs_0008.json',
]

clay_waxs_pos = [
'june112_night_scan_caly_cellulose_center_waxs_0014.json',
'june112_night_scan_caly_cellulose_edge_waxs_0011.json',
]

def june12_night_run():
    try:
        umv(ndetx,0)
        start_key = 'd'
        #for _ in clay_saxs_pos:
        #    gopos_kmap_scan(_,start_key,
        #                    100,5,0.25,0.25,0.03)
        for _ in saxs_pos:
            gopos_kmap_scan(_,start_key,
                            20,20,0.25,0.25,0.03)
        
        umv(ndetx,-360)
        for _ in clay_waxs_pos:
            gopos_kmap_scan(_,start_key,
                            100,5,0.25,0.25,0.03)
        for _ in waxs_pos:
            gopos_kmap_scan(_,start_key,
                            20,20,0.25,0.25,0.03)
        sc()
    finally:
        sc()
        sc()
        enddataset()
