print ("mac07 start")
def the_scan():
    dscan(ustrz,-0.1,0.1,100,0.05)

def the_mesh1():
    dmesh(ustrz,-0.03,0.03,30,ustry,-0.025,0.025,10,0.05)
    
def the_mesh2():
    dmesh(ustrz,-0.1,0.1,50,ustry,-0.2,0.2,100,0.02)
    
def the_mesh4():
    dmesh(ustrz,-0.3,0.3,10,ustry,-0.1,0.1,10,0.05)
    
def in1127_mac07():
    so()
    fshtrigger()
    mgeig()
    for i in range(104,115):
        pos_name = "s_fr05_01_%04d" % i
        print("="*30,pos_name)
        gopos(f'{pos_name}.json')  
        newdataset(pos_name[3:])      
        the_mesh4()
        
        
print ("mac07 end")
