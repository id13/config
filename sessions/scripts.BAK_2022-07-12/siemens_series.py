print('siemens series load - 5')
def siemens_series(xmot, xstart, xstep, xnpts, *p):
    doos('siemens_series')
    for i in range(xnpts):
        print('3 sec to interrupt ...')
        sleep(3)
        next_xpos = xstart + i * xstep
        if xmot.name in ['nnp1', 'nnp5']:
            mv(xmot, next_xpos)
        else:
            raise ValueError(f'illegal motor {xmot} ({xmot.name})')
            return
        s = f'xindex: {i}    nominal_xpos {next_xpos}    xpos = {xmot.position}   ========='
        print(s)
        doos(s)
        kmap.dkmap(*p)
        elog_plot(scatter=True)
