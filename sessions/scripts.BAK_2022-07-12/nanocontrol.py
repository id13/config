import tango

def make_nanocontrol():
    return NanoControl()

class NanoControl():
    def __init__(self):
        try:
            self.set_connection()

    def set_connection(self):
        self.device = tango.DeviceProxy('id13/nanocontrol/1')
        self.device.set_connection() 
        
