'''
mono fit example
import numpy as np
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

def func(x, kc, co):
    return kc/np.sin(np.pi*x/180-co)

def main():
    y = np.array([13.273,11.919,15.2,17.998])
    x = np.array([-8.03054,-9.01014,-6.9324,-5.76244])
    popt, cov = curve_fit(func, x, y, bounds=([-3,-1],[-0.8,0.8]))
    print (180.0*popt[1]/np.pi)
    print (popt, cov)

if __name__ == '__main__':
    main()
'''
import math
from bliss.setup_globals import *

RPD = math.pi/180.0
DPR = 180.0/math.pi
E2L = 12.3985

CLASSICAL_EYBERT_OUTPUT = """
274.EYBERT> koz_show
Serial device "id13/serialcomlid131/com1" isn't open.
Serial device "id13/serialcomlid131/com1" isn't open.

ID13  MONOCHROMATOR:
====================

Theory:
E[keV] = 1keV * (KOHZU constant)/sin(GLOB_MONO[rad] - (KOHZU offset))

Current KOHZU calibration:
    KOHZU offset   =  0.55146
    KOHZU constant =  -1.98066

Current undulator gap settings:
    gap u35 =  100
    gap u18 =  7.026

Current Monochromator setting:
    GLOB_MONO =  -8.21515

KOHZU energy for set angle -8.21515 = 12.9959 keV
KOHZU lambda for set angle -8.21515 = 0.954034 Angstroem


"""
# OLD VALUES (before June 1, 2021)
# CC_OFF   = 0.55146
# CC_CONST = -1.98066
# NEW VALUES
CC_OFF   =  0.556904534
CC_CONST = -1.98135728
GAP_MOT_1 = u18
GAP_MOT_2 = u35
MIN_GAP_1 = 6.0
MIN_GAP_2 = 11.0
MAX_GAP_1 = 25.0
MAX_GAP_2 = 40.0
CC_MONO_MOT = ccth
PHG = phg
PHO = pho
PVG = pvg
PVO = pvo

def yesno(quetxt, genindent=""):
    ans = input("%s%s (y/n)? " % (genindent,quetxt))
    if "y" == ans:
        return True
    else:
        return False


class CCMono(object):

    def __init__(self, calib_off=CC_OFF, calib_const=CC_CONST,
        gap_mot_1=GAP_MOT_1, gap_mot_2=GAP_MOT_2, mono_mot=CC_MONO_MOT,
        phg_mot = PHG, pho_mot = PHO, pvg_mot = PVG, pvo_mot = PVO
        ):
        self._calib_off  = calib_off
        self._calib_const = calib_const
        self._gap_mot_1 = gap_mot_1
        self._gap_mot_2 = gap_mot_2
        self._mono_mot = mono_mot
        self._pvg_mot = pvg_mot
        self._pvo_mot = pvo_mot
        self._phg_mot = phg_mot
        self._pho_mot = pho_mot

    def goto_energy(self, energy):
        mmot = self._mono_mot
        print ("current mono position:")
        wm(mmot)
        new_angle = self.energy_to_angle(energy)
        print ("%s would go to angle =" % mmot.name, new_angle)
        if yesno("go there"):
            mv(mmot, new_angle)
            wm(mmot)
        else:
            print("okay - nothing done.")

    def get_energy(self):
        energy = self.angle_to_energy(self._mono_mot.position)
        return energy

    def angle_to_energy(self, angle):
        return self._calib_const/math.sin(RPD*(angle - self._calib_off))

    def energy_to_angle(self, energy):
        return DPR*math.asin(self._calib_const/energy) + self._calib_off

    def show(self):
        ccoff = self._calib_off
        ccc   = self._calib_const
        gm1   = self._gap_mot_1
        gm2   = self._gap_mot_2
        mmot  = self._mono_mot
        energy = self.angle_to_energy(mmot.position)
        lam    = E2L/energy

        print("""ID13 channel-cut monochromator:
    calibration:
        calibrated offset:    %f
        calibrated constant:  %f
    insertion devices:
        undulator gap1:       %s = %f
        undulator gap2:       %s = %f
    monochromator:
        angle:                %5s = %f
        energy[keV]:          %f
        lambda[A]:            %f
""" % (
    ccoff, ccc, gm1.name, gm1.position, gm2.name, gm2.position,
    mmot.name, mmot.position, energy, lam
))
