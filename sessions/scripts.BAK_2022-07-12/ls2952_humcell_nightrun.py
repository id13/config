print('ls2952_humcell_nightrun load 5')

import time

STARTKEY = 'c'

def ls2952_humcell_nightrun():

    try:
        start_key = STARTKEY
        so()
        time.sleep(3)
        newdataset(f'humdity_nightrun_{start_key}')
        gopos('humcell_B1_bis_scanstart_0123.json')
        mvr(ustrz, 0.03)

        for i in range(20):
            print(f'cycle: {i} ========================================')
            dkpatchmesh(1.0, 500, 0.01, 1, 0.02, 1, 1)
            time.sleep(1440)
            mvr(ustrz, 0.085)
        dkpatchmesh(1.0, 500, 0.01, 1, 0.02, 1, 1)

    finally:
        sc()
        sc()
