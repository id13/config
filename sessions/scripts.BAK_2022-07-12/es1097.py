LOAD = 12
print(f'es1097 - {LOAD}')

START_LETT = 'e'
STEPSIZE=2

REGIONS_1 = '''
14573 470 730
14575 320 690
14576 810 810
14578 560 680
'''

REGIONS_2 = '''
14746 860 480
14747 940 840
14748 910 1000
14750 950 860
'''

REGIONS = REGIONS_2

def dooline(l):
    l = l.strip()
    w = l.split()
    regno, dy,dz = w
    regno = int(regno)
    dy = int(dy)
    dz = int(dz)

    nstpy = int(dy/STEPSIZE)
    nstpz = int(dz/STEPSIZE)
    dy = 0.001 * nstpy*STEPSIZE
    dz = 0.001 * nstpz*STEPSIZE
    
    newdataset(f'roi__{regno}_{START_LETT}')
    gopos(f'roi__{regno}')
    print(f'makesomemap_2(0, {dy}, {nstpy}, 0, {dz}, {nstpz}, 0.05, retveloc=2')
    dkmapyz_2(0, dy, nstpy, 0, dz, nstpz, 0.05, retveloc=2)

def es1097_main_night3():
    try:
        so()
        fshtrigger()
        sleep(3)
        for l in REGIONS.split('\n'):
            l = l.strip()
            if l:
                print('dooing {l} ================================================')
                print('    5 sec to interrupt')
                sleep(3)
                dooline(l)
    finally:
        sc()
        sc()
        sc()

