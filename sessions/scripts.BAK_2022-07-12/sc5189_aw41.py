print('sc5189_aw41 load 1')
START_KEY = 'a'

THE_LIST = '''
aw41_5x_roi1_0076.json 150 150 0.5 0.002
aw41_5x_roi2_0080.json 150 150 0.5 0.003
aw41_5x_roi3_0081.json 150 150 0.5 0.005
aw41_5x_roi4_0082.json 150 150 0.5 0.01
aw41_5x_roi5_0083.json 150 150 0.5 0.015
aw41_5x_roi6_0084.json 150 150 0.5 0.02
aw41_5x_roi7_0085.json 150 150 0.2 0.003
aw41_5x_roi8_0086.json 150 150 0.5 0.003
aw41_5x_roi9_0087.json 150 150 0.7 0.003
aw41_5x_roi10_0088.json 150 150 1.0 0.003
aw41_5x_roi11_0089.json 150 150 1.5 0.003
aw41_5x_roi12_0090.json 150 150 2.0 0.003
aw41_5x_roi13_0091.json 150 150 0.2 0.01
aw41_5x_roi14_0092.json 150 150 0.5 0.01
aw41_5x_roi15_0093.json 150 150 1.0 0.01
aw41_5x_roi16_0094.json 150 150 2.0 0.01
aw41_5x_roi17_0095.json 150 150 0.5 0.003
'''

def make_posdc(poslst):
    posll = poslst.split('\n')
    dc = dict()
    for l in posll:
        #print (f'l = {l}')
        l = l.strip()
        if not l or l.startswith('#'):
            continue
        w1 = l.split()
        w2 = w1[0].split('_')
        for e in w2:
            #print (f'e = {e}')
            if e.startswith('roi'):
                dc[e] = l
    return dc


def dooone(psdc, roi):
    dsname = f'{roi}_{START_KEY}'
    posname, hw, vw, stp, expt = psdc[roi].split()
    hw, vw, stp, expt = map(float, (hw, vw, stp, expt))
    hll = -0.5*0.001*hw
    hul = 0.5*0.001*hw
    hitv = int(hw/stp)
    vll = -0.5*0.001*vw
    vul = 0.5*0.001*vw
    vitv = int(vw/stp)
    print('\n\n\n                                                                  #####')
    print(' ============================================================-----#####')
    print('                                                                  #####\n')
    print(f'ROI: {roi} - What should happen ...')
    print(f"newdataset('{dsname}')")
    print(f"gopos('{posname}')")
    print(f'dkmapyz_2({hll}, {hul}, {hitv}, {vll}, {vul}, {vitv}, {expt})')
    print('')
    print('What is happening ... Good Luck!')
    newdataset(dsname)
    gopos(posname)
    dkmapyz_2(hll, hul, hitv, vll, vul, vitv, expt)


def sc5189_aw41():
    try:
        posdc = make_posdc(THE_LIST)
        so()
        mgeig()
        dooone(posdc, 'roi1')
        dooone(posdc, 'roi2')
        dooone(posdc, 'roi3')
        dooone(posdc, 'roi4')
        dooone(posdc, 'roi5')
        dooone(posdc, 'roi6')
        dooone(posdc, 'roi7')
        dooone(posdc, 'roi8')
        dooone(posdc, 'roi9')
        dooone(posdc, 'roi10')
        dooone(posdc, 'roi11')
        dooone(posdc, 'roi12')
        dooone(posdc, 'roi13')
        dooone(posdc, 'roi14')
        dooone(posdc, 'roi15')
        dooone(posdc, 'roi16')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')
        dooone(posdc, 'roi17')

    finally:
        pass
        sc()
        sc()
        sc()
