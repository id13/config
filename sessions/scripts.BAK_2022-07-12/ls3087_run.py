print('ls3087 load 19')


start_key = 'f'


WAXS_list = [

'J11_04_B8_03_EGG_pos1_0021.json',
'J11_04_B8_03_EGG_pos4_0024.json',
'J11_04_B1_04_PEU_pos1_0028.json',
'J11_04_B5_01_NIDlong_pos1_0025.json',
'J11_04_B5_01_NIDlong_pos3_0027.json',
'J11_04_B8_03_EGG_pos2_0022.json',



#'J11_03_B8_03_EGG_pos3_0000.json',
#'J11_03_B8_03_EGG_pos5_0005.json',
#'J11_03_B8_03_EGG_pos7_0008.json',
#'J11_03_B8_04_PEU_pos1_0009.json',
#'J11_03_B8_04_PEU_pos3_0011.json',
#'J11_03_B8_04_PEU_pos5_0013.json',
#'J11_03_B7_05_EGGlong_pos1_0018.json',


#'J11_01_B5_05_NIDlong_pos1_0038.json',
#'J11_01_B5_05_NIDlong_pos2_0039.json',
#'J11_01_B5_05_NIDlong_pos3_0040.json',
#'J11_01_B5_05_NIDlong_pos6_0043.json',
#'J11_01_B5_05_NIDlong_pos7_0044.json',
#'J11_01_B6_02_ARlong_pos1_0032.json',
#'J11_01_B6_02_ARlong_pos4_0035.json',
#'J11_01_B6_02_ARlong_pos6_0037.json',



#'J10_04_B6_03_ARlong_pos1_0018.json',
#'J10_04_B6_03_ARlong_pos2_0019.json',
#'J10_04_B6_03_ARlong_pos3_0020.json',
#'J10_04_B6_03_ARlong_pos4_0021.json',
#'J10_04_B6_03_ARlong_pos7_0024.json',
#'J10_04_B5_04_NIDlong_pos1_0025.json',
#'J10_04_B5_04_NIDlong_pos2_0026.json',
#'J10_04_B5_04_NIDlong_pos4_0028.json',
#'J10_04_B5_04_NIDlong_pos6_0030.json',
#'J10_03_B2_EGG_PH_pos1_0007.json',
#'J10_03_B2_EGG_PH_pos2_0008.json',
#'J10_03_B2_EGG_PH_pos3_0009.json',
#'J10_03_B2_EGG_PH_pos7_0013.json',
#'J10_03_B6_06_NID_pos1_0014.json',
#'J10_03_B6_06_NID_pos3_0016.json',
#'J10_03_B6_06_NID_pos4_0017.json',


#'J09_B1_01_PEU_3um_pos1_0048.json',
#'J09_B1_01_PEU_3um_pos4_0051.json',
#'J09_B1_01_PEU_3um_pos5_0052.json',
#'J09_B4_09_AR_3um_pos1_0043.json',
#'J09B4_09_AR_3um_pos4_0046.json',
#'J09_B4_09_AR_3um_pos5_0047.json',

#'B2_07_EGG_3um_pos1_0038.json',
#'B2_07_EGG_3um_pos2_0040.json',
#'B2_07_EGG_3um_pos5_0042.json',
#'NID_whole_mount02_up_pos3_0029.json',
#'NID_whole_mount02_up_pos4_0030.json',
#'NID_whole_mount02_up_pos5_0031.json',

]

SAXS_list = [

'J11_04_B8_03_EGG_pos4_0024.json',
'J11_04_B1_04_PEU_pos2_0029.json',
'J11_04_B5_01_NIDlong_pos2_0026.json',
'J11_04_B8_03_EGG_pos3_0023.json',



#'J11_03_B8_03_EGG_pos4_0001.json',
#'J11_03_B8_03_EGG_pos6_0006.json',
#'J11_03_B8_03_EGG_pos7_0008.json',
#'J11_03_B8_04_PEU_pos2_0010.json',
#'J11_03_B8_04_PEU_pos4_0012.json',
#'J11_03_B8_04_PEU_pos5_0013.json',
#'J11_03_B7_05_EGGlong_pos2_0019.json',


#'J11_01_B5_05_NIDlong_pos4_0041.json',
#'J11_01_B5_05_NIDlong_pos5_0042.json',
#'J11_01_B5_05_NIDlong_pos7_0044.json',
#'J11_01_B6_02_ARlong_pos2_0033.json',
#'J11_01_B6_02_ARlong_pos3_0034.json',
#'J11_01_B6_02_ARlong_pos6_0037.json',

#'J10_04_B6_03_ARlong_pos5_0022.json',
#'J10_04_B6_03_ARlong_pos6_0023.json',
#'J10_04_B6_03_ARlong_pos7_0024.json',
#'J10_04_B5_04_NIDlong_pos3_0027.json',
#'J10_04_B5_04_NIDlong_pos5_0029.json',
#'J10_04_B5_04_NIDlong_pos6_0030.json',


#'J10_03_B2_EGG_PH_pos4_0010.json',
#'J10_03_B2_EGG_PH_pos5_0011.json',
#'J10_03_B2_EGG_PH_pos7_0013.json',
#'J10_03_B6_06_NID_pos2_0015.json',
#'J10_03_B6_06_NID_pos4_0017.json',


#'J09_B1_01_PEU_3um_pos2_0049.json',
#'J09_B1_01_PEU_3um_pos3_0050.json',
#'J09_B1_01_PEU_3um_pos5_0052.json',
#'J09_B4_09_AR_3um_pos2_0044.json',
#'J09_B4_09_AR_3um_pos3_0045.json',
#'J09_B4_09_AR_3um_pos5_0047.json',

#'B2_07_EGG_3um_pos3_0039.json',
#'B2_07_EGG_3um_pos4_0041.json',
#'B2_07_EGG_3um_pos5_0042.json',
#'NID_whole_mount03_pos1_0019.json',
#'NID_whole_mount03_pos2_0020.json',
#'NID_whole_mount03_pos3_0021.json',
#'NID_whole_mount03_pos4_0022.json',
#'NID_whole_mount03_pos5_0023.json',
]


def kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t):
    ystep = int(2*yrange/ystep_size)
    zstep = int(2*zrange/zstep_size)
    #umvr(nnp2,#,nnp3,#)
    kmap.dkmap(nnp2,yrange,-1*yrange,ystep,nnp3,-1*zrange,zrange,zstep,exp_t)

def gopos_kmap_scan(pos,start_key,run_key,
                    yrange,zrange,ystep_size,
                    zstep_size,exp_t):
    so()
    fshtrigger()
    mgeig()
    name = f'{pos[:-10]}_{start_key}_{run_key}'
    gopos(pos)
    newdataset(name)
    kmap_scan(yrange,zrange,ystep_size,zstep_size,exp_t)
    enddataset()


def run_june_11th_morning():
    try:
#        umv(ndetx,-250)

#        gopos_kmap_scan(WAXS_list[0],start_key,0,20,30,0.5,0.1,0.01)
#        gopos_kmap_scan(WAXS_list[1],start_key,0, 5, 5,0.1,0.1,0.01)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,10,10,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[3],start_key,0,20,20,0.1,0.5,0.005)
#        gopos_kmap_scan(WAXS_list[4],start_key,0,20,15,0.1,0.1,0.005)


        umv(ndetx,300)
 #       gopos_kmap_scan(SAXS_list[0],start_key,0, 5, 5,0.1,0.1,0.01)
 #       gopos_kmap_scan(SAXS_list[1],start_key,0,10,10,0.1,0.1,0.005)
 #       gopos_kmap_scan(SAXS_list[2],start_key,0,10,30,0.1,0.5,0.005)
 #       gopos_kmap_scan(SAXS_list[3],start_key,0,20,30,0.5,0.1,0.01)
        gopos_kmap_scan(SAXS_list[3],start_key,0,20,30,0.1,0.1,0.01)

        umv(ndetx,-250)
        gopos_kmap_scan(WAXS_list[5],start_key,0,20,20,0.1,0.1,0.01)
        gopos_kmap_scan(WAXS_list[5],start_key,0,20,20,0.07,0.07,0.01)


        sc()
    except:
        sc()
        sc()






def run_june_10th_night():
    try:
#        umv(ndetx,-250)
#
#        gopos_kmap_scan(WAXS_list[0],start_key,0,10,20,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[2],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[3],start_key,0,20,10,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[4],start_key,0, 5, 5,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[5],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[6],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[7],start_key,0,10,30,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[8],start_key,0, 5, 5,0.1,0.1,0.005)



        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[0],start_key,0,10,20,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[1],start_key,0,20,10,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[2],start_key,0, 5, 5,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[3],start_key,0,15,25,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[4],start_key,0,25,25,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[5],start_key,0, 5, 5,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[6],start_key,0,10,20,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[7],start_key,0,15,15,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[8],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[9],start_key,0,20,10,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[10],start_key,0,25,25,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[11],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[12],start_key,0,10,30,0.1,0.1,0.005)


        sc()
    except:
        sc()
        sc()



def run_june_10th_evening():
    try:
        umv(ndetx,-250)

        gopos_kmap_scan(WAXS_list[0],start_key,0,25,25,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[1],start_key,0,25,25,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[2],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[3],start_key,0, 5, 5,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[4],start_key,0,20,20,0.1,0.1,0.005)
#        gopos_kmap_scan(WAXS_list[5],start_key,0,25,25,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[6],start_key,0, 5, 5,0.1,0.1,0.005)

        umv(ndetx,300)
        gopos_kmap_scan(SAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[3],start_key,0,20,20,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[4],start_key,0, 5, 5,0.1,0.1,0.005)
#        gopos_kmap_scan(SAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
        sc()
    except:
        sc()
        sc()


def run_june_9th_night():
    try:
        umv(ndetx,-250)

        #gopos_kmap_scan(WAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[3],start_key,1,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[4],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(WAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
        
        umv(ndetx,300) 
        gopos_kmap_scan(SAXS_list[0],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[1],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5  ,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[3],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[4],start_key,0,15,15,0.1,0.1,0.005)
        gopos_kmap_scan(SAXS_list[5],start_key,0,5,5  ,0.1,0.1,0.005)
        sc()
    except:
        sc()
        sc()

def run_june_9th_lunch():
    try:
        umv(ndetx,-250)

        gopos_kmap_scan(WAXS_list[0],start_key,0,30,30,0.1,0.1,0.01)
       
         #gopos_kmap_scan(WAXS_list[1],start_key,0,25,25,0.1 ,1,0.01)
        
        gopos_kmap_scan(WAXS_list[2],start_key,0,5,5,0.1,0.1,0.01)
        
        umv(ndetx,300) 
        gopos_kmap_scan(SAXS_list[0],start_key,0,25,25,0.1,0.1,0.01)
        
        #gopos_kmap_scan(SAXS_list[1],start_key,0,35,35,0.1,0.1,0.01)
        
        gopos_kmap_scan(SAXS_list[2],start_key,0,5,5,0.1,0.1,0.01)
        sc()
    except:
        sc()
        sc()

def run_june_8th():
    try:
        umv(ndetx,-250)

        gopos_kmap_scan(WAXS_list[0],start_key,0,12.5,12.5,0.2 ,1,0.01)
        gopos_kmap_scan(WAXS_list[1],start_key,0,12.5,12.5,0.1 ,1,0.01)
        gopos_kmap_scan(WAXS_list[2],start_key,0,12.5,12.5,0.05,1,0.01)
        #for _ in range(10):
        #    gopos_kmap_scan(WAXS_list[3],start_key,_,12.5,12.5,0.2,1,0.01)
        
        umv(ndetx,300) 
        gopos_kmap_scan(SAXS_list[0],start_key,0,12.5,12.5,0.2 ,1,0.01)
        gopos_kmap_scan(SAXS_list[1],start_key,0,12.5,12.5,0.1 ,1,0.01)
        gopos_kmap_scan(SAXS_list[2],start_key,0,12.5,12.5,0.05,1,0.01)
        for _ in range(10):
            gopos_kmap_scan(SAXS_list[3],start_key,_,12.5,12.5,0.2,1,0.01)
        sc()
    except:
        sc()
        sc()
