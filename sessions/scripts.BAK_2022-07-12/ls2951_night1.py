def ls2951_night1():
    newdataset('night1hires_a')
    gopos('night1_ul_1')
    dkpatchmesh(0.5,250,0.5,250,0.025,3,3)
    enddataset()

    newdataset('night1lores_a')
    gopos('night1_ul_2')
    dkpatchmesh(1.0,200,1.0,200,0.025,4,4)
    enddataset()
