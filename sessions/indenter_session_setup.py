
from bliss.setup_globals import *

load_script("indenter_session.py")

print("")
print("Welcome to your new 'indenter_session' BLISS session !! ")
print("")
print("You can now customize your 'indenter_session' session by changing files:")
print("   * /indenter_session_setup.py ")
print("   * /indenter_session.yml ")
print("   * /scripts/indenter_session.py ")
print("")

load_script("indenter_a")
inda = make_inda()
inda_pos_motor = inda.create_posaxis('inda_pos_motor')
inda_pos_counter = inda.create_positioncounter('inda_pos_counter')
inda_force = inda.create_forcecounter('inda_force')
inda_raw_force = inda.create_raw_forcecounter('inda_raw_force')

