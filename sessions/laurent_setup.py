
from bliss.setup_globals import *

load_script("laurent.py")

print("")
print("Welcome to your new 'laurent' BLISS session !! ")
print("")
print("You can now customize your 'laurent' session by changing files:")
print("   * /laurent_setup.py ")
print("   * /laurent.yml ")
print("   * /scripts/laurent.py ")
print("")