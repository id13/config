from bliss.setup_globals import *
from bliss import current_session
import time
import pprint
from bliss.scanning.chain import ChainPreset

load_script("rh.py")


load_script("vacuumfix.py")
#load_script("rhmeta.py")
#load_script("preset_fsh.py")
load_script("preset_eiger.py")

#from id13.scripts.user_script import *
from id13.scripts.fastmon import *


print("")
print("Welcome to your new 'rh' BLISS session !! ")
print("")
print("You can now customize your 'rh' session by changing files:")
print("   * /rh_setup.py ")
print("   * /rh.yml ")
print("   * /scripts/rh.py ")
print("")

from bliss import setup_globals

print("+ set the default chain ...")
#default_chain = setup_globals.default_chain_rh
#DEFAULT_CHAIN.set_settings(chain_rh['chain_config'])
#DEFAULT_CHAIN.set_settings(chain_sim_cam1['chain_config'])
#DEFAULT_CHAIN.set_settings(chain_rh3_mpx['chain_config'])


#====== metadata
SCAN_SAVING.scan_number_format = '%05d'
SCAN_SAVING.images_prefix = '{img_acq_device}_{scan_number}_'
SCAN_SAVING.date_format = '%Y-%m-%d'
#SCAN_SAVING.technique = 'KMAP'

current_session.enable_esrf_data_policy()
#current_session.disable_esrf_data_policy()

cam = "eiger"
if hasattr(setup_globals, cam):
  print("... [%s] is USED" % (cam,))
  try:
    eiger_preset = Preset_eiger()
    DEFAULT_CHAIN.add_preset(eiger_preset)
  except:
    print("... [%s] EXCEPT ERROR" % (cam,))
    pass
else:
  print("... [%s] is NOT USED" % (cam,))
  

cam = "mpxeh3"
if hasattr(setup_globals, cam):
  print("... [%s] is USED" % (cam,))
  try:
    mpxeh3
    mpxthleh3 = SoftAxis(
        "mpxthleh3", mpxeh3.camera, position="energy_threshold", move="energy_threshold"
    )
    DEFAULT_CHAIN.set_settings(chain_rh3_mpx['chain_config'])
  except:
    print("... [%s] EXCEPT ERROR" % (cam,))
    pass
else:
  print("... [%s] is NOT USED" % (cam,))

