
from bliss.setup_globals import *

load_script("manfred.py")

print("")
print("Welcome to your new 'manfred' BLISS session !! ")
print("")
print("You can now customize your 'manfred' session by changing files:")
print("   * /manfred_setup.py ")
print("   * /manfred.yml ")
print("   * /scripts/manfred.py ")
print("")

