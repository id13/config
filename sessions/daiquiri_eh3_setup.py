
from bliss.setup_globals import *
from bliss import setup_globals

load_script("daiquiri_eh3.py")

print("")
print("Welcome to your new 'daiquiri_eh3' BLISS session !! ")
print("")
print("You can now customize your 'daiquiri_eh3' session by changing files:")
print("   * /daiquiri_eh3_setup.py ")
print("   * /daiquiri_eh3.yml ")
print("   * /scripts/daiquiri_eh3.py ")
print("")


#SCAN_SAVING.images_prefix = "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"

from blissoda.id13.daiquiri_xrpd_processor import DaiquiriXrpdProcessor as _XrpdProcessor
xrpd_processor = _XrpdProcessor(enable_plotter=False)
