from numpy import *
from matplotlib import pyplot as plt

class Circle

def _test():
    x = linspace(0.0, 360.0, 180.0)
    xr = x*pi/180.0
    print(len(x))
    y = 0.473**sin(x)*(1.0+0.002*x)
    e = random.random(len(y))*0.5
    y = y+e
    yy = y*0
    for i in range(len(x)-4):
        fit = polyfit(x[i:i+3], y[i:i+3],2)
        f = poly1d(fit)
        print(fit, f(x[i+3]))
        yy[i+3] = f(x[i+3])
    print(x,y,yy)
    plt.plot(x,y)
    plt.plot(x,yy, 'red')
    plt.show()

if __name__ == '__main__':
    _test()
