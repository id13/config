print('load script ch6158 1st night ver 9')
# so()
#sct(0.01)
#load_script('ch6158_1st_night')
#load_script('ch6158_tolman')
#bch2 = setitup()
#ch6518_19_night('name')
#ucorrsrotz = make_corrsrotz(bch2)

prefix = 'e'


#def set_bch_class():
    #load_script('ch6158_tolman')
#bch2 = setitup()
#ucorrsrotz = make_corrsrotz(bch2)
    
def set_lm_sample_running():
    set_lm(ustrx,-61.000,-59.347)
    set_lm(ustry,-10.000,-13.410)
    set_lm(usrotz,29.5,135.5)

def set_lm_sample_change():
    sc()
    umv(udetx,-64)
    umv(ubsy,-100)
    set_lm(ustrx,-61.000,10.000)
    #set_lm(ustry,-10.000,-13.410)
    #set_lm(usrotz,29.5,135.5)

class CorrectedUsrotz(object):

    def __init__(self, bch):
        self.bch = bch

    @property
    def cr_position(self):
        return usrotz.position

    @cr_position.setter
    def cr_position(self, pos):
        mv(usrotz, pos)
        self.bch.correct(pos, ask=False)

def make_corrsrotz(bch):
    cr = CorrectedUsrotz(bch)
    sftax = SoftAxis('corrsrotz', cr, position='cr_position', move='cr_position', tolerance=0.02)
    return sftax


def go_rot_cent(deg):
    umv(usrotz,deg)
    if np.abs(usrotz.position-deg)<0.1:
        bch2.correct(deg,ask=False)
    else:
        raise RuntimeError(f'user interrupt, want to {deg}, go to {usrotz.position}')

def angular_raster_scan(deg,ll1,ul1,nitv1,ll2,ul2,nitv2,exp_t,retveloc=5):
    try:
        f = open('stopit')
        enddataset()
        return 'stopit'
    except:
        pass
    
    go_rot_cent(deg)
    dkmapyz_2(ll1,ul1,nitv1,ll2,ul2,nitv2,exp_t,retveloc=retveloc)

def ch6158_19_night(sample_name):
    mgeig()
    fshtrigger()
    so()
    ll1 = -0.4
    ul1 =  0.4
    nitv1 = int(0.8/0.005)
    ll2 = -0.2
    ul2 =  0.2
    nitv2 = 80
    exp_t = 0.002

    once_flg = True
    try:
        ang1 = np.arange(50,135.5,1)
        print(f'ang1 = {ang1}\n')
        ans = yesno('okay? continue?')
        if not ans:
            raise RuntimeError('user interrupt')
        # dataset inlucde 50 to 135 degree with step size of 1 degree
        newdataset(f'{sample_name}_{prefix}')
        for deg in ang1:
            if (deg>70) & (deg<74):
                print(f'------skip-----{deg}')
                pass
            elif (deg>126) & (deg<133):
                print(f'------skip-----{deg}')
                pass
            else:
                print(f'ang1 series: thets = {deg}')
                if once_flg:
                    ans = yesno('okay? continue?')
                    if not ans:
                        raise RuntimeError('user interrupt')
                    once_flg = False
                # fly scan for each angle   
                #go_rot_cent(deg) 
                angular_raster_scan(deg,ll1,ul1,nitv1,
                                    ll2,ul2,nitv2,exp_t)
        enddataset()
            
        sc()
    finally:
        sc()
        sc()

def ch6158_18_night():
    mgeig()
    fshtrigger()
    so()
    ll1 = -0.4
    ul1 =  0.4
    nitv1 = 80*2
    ll2 = -1.1
    ul2 =  1.1
    nitv2 = 220*2
    exp_t = 0.002

    once_flg = True
    try:
        ang1 = np.arange(0,360.5,2)
        print(f'ang1 = {ang1}\n')
        #ang2 = np.flipud(np.arange(1,360.5,2))
        print(f'ang2 = {ang2}\n')
        ans = yesno('okay? continue?')
        if not ans:
            raise RuntimeError('user interrupt')
        # dataset inlucde 0 to 360 degree with step size of 2 degree
        newdataset(f'rot_ang_000_to_360_{prefix}')
        for deg in ang1:
            print(f'ang1 series: thets = {deg}')
            if once_flg:
                ans = yesno('okay? continue?')
                if not ans:
                    raise RuntimeError('user interrupt')
                once_flg = False
            # fly scan for each angle    
            angular_raster_scan(deg,ll1,ul1,nitv1,
                                ll2,ul2,nitv2,exp_t)
        enddataset()
            
        # dataset inlucde 359 to 001 degree with step size of 2 degree
        newdataset(f'rot_ang_359_to_001_{prefix}')
        for deg in ang2:
            print(f'ang2 series: thets = {deg}')
            # fly scan for each angle    
            angular_raster_scan(deg,ll1,ul1,nitv1,
                                ll2,ul2,nitv2,exp_t)
        enddataset()
        sc()
    finally:
        sc()
        sc()
